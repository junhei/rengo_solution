<?php
class Usermanager_model extends CI_Model {
 

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		// $this->output->enable_profiler(TRUE);
	}

	function get_user_information($company_serial)
	{
		$sql="
		SELECT 
			*
		FROM
			user_information
		WHERE
			flag = 'Y'";

		if($company_serial==0){
			$wherecon='';
		} else {
			$wherecon="
			AND
				company_serial='".$company_serial."'";
		}

		// echo $sql;
		// die();
		$result = $this->db->fReadSql($sql.$wherecon);
		return $result;
	}

	function get_user_count($company_serial, $user_status)
	{	
		$sql1 = "
		SELECT
			user_status
		FROM 
			user_information 		
		";

		switch($user_status){
			case "whole":
				if($company_serial==0) $wherecon='';
				else $wherecon = " WHERE company_serial='".$company_serial."'";
				break;
			case "normal":
				$wherecon = " WHERE user_status='일반'";
				break;
			case "bad":
				$wherecon = " WHERE user_status='불량'";
				break;
			case "special":
				$wherecon = " WHERE user_status='특별'";
				break;
		}

		if($company_serial==0) {
			$sql2='';	
		} else if($wherecon != ''){
			$sql2="
			AND
				company_serial='".$company_serial."'";	
		}

		$query = $this->db->query($sql1.$wherecon.$sql2);
		return $query->num_rows();
	}

	function get_order_information($user_serial)
	{
		$sql="
		SELECT 
			O.period_start,O.period_finish,CM.car_type,CM.car_name_detail,CL.car_number,CL.gear_type,CL.fuel_option,O.rental_price
		FROM 
			order_list O
		INNER JOIN 
			car_list CL
		ON 
			O.car_serial = CL.serial 
		INNER JOIN 
			car_master CM
		ON 
			CL.car_index = CM.car_index
		WHERE 
			O.user_serial='".$user_serial."' AND (O.flag = 'P' OR O.flag = 'F')
		";

		$result =$this->db->fReadSql($sql);
		return $result;
	}

	function get_receipt_information($user_serial)
	{
		$sql="
		SELECT 
			deposite_date, type, deposite_way, deposite_amount, memo, handler, deposite_registration_date
		FROM 
			receipt_history
		WHERE 
			user_serial='".$user_serial."' AND flag = 'Y'";

		$result =$this->db->fReadSql($sql);
		return $result;
	}

	
	// function user_info_insert($data)
	// {
	// 	//중복처리
	// 	$result=$this->db->insert('user_information',$data);
	// 	if($result){
	// 		$response['code']='S01';
	// 	}else{
	// 		$response['code'] ="E01";
	// 		$error = $this->db->error();
	// 		$response['message'] = $error['message'];
	// 	}
	// 	return $response;
	// }

	function user_info_update($user_serial,$user_information_data)
	{
		$this->db->where('user_serial',$user_serial);
		$result=$this->db->update('user_information',$user_information_data);

		if($result){
			$response['code'] ="S01";
		} else {
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	// function company_user_grade_update($user_serial,$company_serial,$company_user_grade_data)
	// {
	// 	$this->db->where('user_serial',$user_serial);
	// 	$this->db->where('company_serial',$company_serial);
	// 	$result=$this->db->update('company_user_grade',$company_user_grade_data);

	// 	if($result){
	// 		$response['code'] ="S01";
	// 		// $response['a'] =$company_user_grade_data['user_status'];
	// 	} else {
	// 		$response['code'] ="E01";
	// 		$error = $this->db->error();
	// 		$response['message'] = $error['message'];
	// 	}
	// 	return $response;
	// }

	function user_delete($user_serial)
	{
		$result = $this->db->delete('company_user_grade', array('user_serial' => $user_serial));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	function get_license_image($user_serial) 
	{
		$sql="SELECT license_image FROM user_information WHERE user_serial=".$user_serial;
		$result=$this->db->fReadSql($sql);
		return $result;
	}


	function m_get_user($company_serial, $user_name)
	{
		$sql="
		SELECT 
			user_serial, user_name, birthday, gender, user_email, phone_number1, license_type, license_number, published_date, expiration_date, phone_number2, postcode, address
		FROM
			user_information
		WHERE
			flag = 'Y'";

		if($company_serial==0){
			$wherecon='';
		} else {
			$wherecon="
			AND
				company_serial='".$company_serial."'";
		}

		if($user_name!=''){
			$wherecon.=" AND user_name='".$user_name."'";
		}

		// echo $sql;
		// die();
		$result = $this->db->fReadSql($sql.$wherecon);
		return $result;
	}


	function m_change_pw($id, $pw, $auth){
		

		$sql = "UPDATE
				admin_information
			SET
				admin_pw = PASSWORD('".$pw."')
			WHERE
				admin_id = '".$id."'
			AND 
				auth_code = '".$auth."'";

		$result = $this->db->query($sql);
		$result_row_count = $this->db->affected_rows();
		if($result_row_count > 0){
			$response['code'] = 'S01';
			$response['value'] = array();
			
		}else{
			$response['code'] = 'E01';
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}
}