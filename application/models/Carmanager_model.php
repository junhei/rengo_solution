<?php
class Carmanager_model extends CI_Model {
 

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	//전체 차종 얻기
	function get_list($company_serial, $type)
	{

		$sql = "SELECT maker_master.maker_name, maker_master.country, car_list.*, car_master.* FROM maker_master, car_list, car_master WHERE car_list.flag != 'D' AND car_list.car_index = car_master.car_index AND car_master.maker_index = maker_master.maker_index";

		if($company_serial!='' && $company_serial!=0){
			$sql = $sql." AND car_list.company_serial = ".$company_serial;
		}
		
		if($type != ''){
			$sql = $sql." AND car_master.car_type = '".urldecode($type)."'";
		}
		$result = $this->db->fReadSql($sql, '');
		return $result;
	}

	//특정기간 전체 차종 얻기
	function get_list_period($company_serial, $type, $period)
	{

		if(strlen($period) == "25"){
			$temp_period = explode("_", $period);
			$period_start = $temp_period['0'];
			$period_finish = $temp_period['1'];
			$예약간시간간격 = 4;

			if($company_serial >= '1'){
				$sql_company_serial = "
				AND car_list.company_serial = '".intval($company_serial)."'
				";
			}

			$sql = "
			SELECT
				order_list.car_serial
			FROM
				order_list,
				car_list
			WHERE
				car_list.flag != 'D'
				and car_list.flag != 'M'
				and order_list.period_finish > '".date('YmdHi',strtotime($period_start.' -'.$예약간시간간격.' hours'))."' and order_list.period_start < '".date('YmdHi',strtotime($period_finish.' +'.$예약간시간간격.' hours'))."'
				and car_list.serial = order_list.car_serial 
			".$sql_company_serial."
			GROUP BY
				order_list.car_serial
			ORDER BY
				order_list.car_serial
			";
			$result_order_list = $this->db->fReadSql($sql, '');
			$count_order_list = count($result_order_list);
			for($i=0;$i<$count_order_list;$i++){
				$order_list[$result_order_list[$i]['car_serial']] = "Y";
			}
		}


		if($company_serial >= '1'){
			$sql_company_serial = " AND car_list.company_serial = ".$company_serial;
		}
		


		if(urldecode($type) != "전체"){
			$sql_type = " AND car_master.car_type = '".urldecode($type)."'";
		}

		$sql = "
		SELECT 
			maker_master.maker_name,
			maker_master.country,
			car_list.*,
			car_master.*,
			car_list.serial
		FROM
			maker_master,
			car_list,
			car_master
		WHERE
			car_list.car_index = car_master.car_index 
			AND car_master.maker_index = maker_master.maker_index
			".$sql_company_serial."
			".$sql_type."
		ORDER BY
			car_master.car_name_detail ASC";
		$result = $this->db->fReadSql($sql, '');
		$count_result = count($result);
		for($i=0;$i<$count_result;$i++){
			if($order_list[$result[$i]['serial']] == "Y"){
				continue;
			}
			else $result_new[] = $result[$i];
		}
		return $result_new;
	}


	//확인내역
	function get_check_list($car_serial)
	{
		$sql = "SELECT * FROM car_check_history where car_serial =".$car_serial;
		$result = $this->db->fReadSql($sql, '');
		return $result;
	}
	//소모품 교환내역
	function get_supply_list($car_serial)
	{
		$sql = "SELECT * FROM car_supply_history where car_serial =".$car_serial;
		$result = $this->db->fReadSql($sql, '');
		return $result;
	}

	//응급추동
	function get_emergency_list($car_serial)
	{
		$sql = "SELECT * FROM car_emergency_history where car_serial =".$car_serial;
		$result = $this->db->fReadSql($sql, '');
		return $result;
	}

	//수리내역
	function get_repair_list($car_serial)
	{
		$sql = "SELECT * FROM car_repair_history where car_serial =".$car_serial;
		$result = $this->db->fReadSql($sql, '');
		return $result;
	}

	//사고내역
	function get_accident_list($car_serial)
	{
		$sql = "SELECT * FROM car_accident_history where car_serial =".$car_serial;
		$result = $this->db->fReadSql($sql, '');
		return $result;
	}

	//렌고이용차량 댓수
	function get_rengo_count($company_serial = '')
	{
		if($company_serial >= '1'){
			$sql_company_serial = "
			AND company_serial = '".$company_serial."'
";
		}

		$sql = "SELECT count(*) as cnt FROM car_list where flag = 'Y'
		".$sql_company_serial."
		";
		$result = $this->db->fReadSql($sql, '');
		return $result['0']['cnt'];
	}

	function get_count($company_serial = 0, $type)
	{
		// $type = urldecode($input_type);
		$sql = "SELECT car_list.serial FROM car_list, maker_master, car_master WHERE car_list.car_index = car_master.car_index AND car_master.maker_index = maker_master.maker_index";

		if($company_serial!=0 && $company_serial!=''){
			$sql = $sql." AND company_serial =".$company_serial;
		}
		

		switch($type){
			case "전체":
					
					break;
			case "미운행":
			case "정상":
			case "검사중":
			case "수리중":
			case "폐차":
			case "매각":
					$sql = $sql." AND car_status='".$type."'";
					break;
			case "자차포함":
					$sql = $sql." AND insurance_self_flag='Y'";
					break;
			case "자차없음":
					$sql = $sql." AND insurance_self_flag='N'";
					break;
			case "age21":
					$sql = $sql." AND requirement_age=21";
					break;
			case "age26":
					$sql = $sql." AND requirement_age=26";
					break;
			case "allage":
					$sql = $sql." AND requirement_age!=21 AND requirement_age!=26";
					break;
			// case "본사":
			// 		$sql = "SELECT car_list.serial FROM car_list, maker_master, car_master WHERE car_list.car_index = car_master.car_index AND car_master.maker_index = maker_master.maker_index";
			// 		if($company_serial!=0 && $company_serial!=''){
			// 				$sql = $sql." AND company_serial =".$company_serial;
			// 		}
			// 		$sql = $sql." AND branch_serial = 0";
			// 		break;
			// case "지점":
			// 		$sql = "SELECT car_list.serial FROM car_list, maker_master, car_master WHERE car_list.car_index = car_master.car_index AND car_master.maker_index = maker_master.maker_index";
			// 		if($company_serial!=0 && $company_serial!=''){
			// 				$sql = $sql." AND company_serial =".$company_serial;
			// 		}
			// 		$sql = $sql." AND branch_serial != 0";
					break;
		}
		
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	//차량 등록
	function add_car($data){
		$sql = "SELECT * FROM car_list WHERE company_serial=".$data['company_serial']." AND car_number='".$data['car_number']."' ";
		$query = $this->db->query($sql);
		$result_count = $query->num_rows();

		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록 되어 있습니다.";		
		}else{

			$this->db->flush_cache();
			//등록 날짜 및 사용자 저장

			$this->db->set('registered_date', 'now()', FALSE);
			// $this->db->set('registered_ip', $_SERVER['REMOTE_ADDR'] , FALSE);
			

			$result = $this->db->insert('car_list', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$error = $this->db->error();
				$response['code'] ="E01";
				$response['message'] =$error['message'];
			}
		}
		return $response;
	}

	//차량 등록
	function update_car($data){


		$this->db->flush_cache();
			//등록 날짜 및 사용자 저장

		$this->db->set('edited_date', 'now()', FALSE);
			// $this->db->set('registered_ip', $_SERVER['REMOTE_ADDR'] , FALSE);
			
		$this->db->where('serial', $data['serial']);
		$result = $this->db->update('car_list', $data);
		if($result){
				$response['code'] ="S01";
		}else{
				$error = $this->db->error();
				$response['code'] ="E01";
				$response['message'] =$error['message'];
		}
		
		return $response;
	}


	function delete_car($id){

		$this->db->flush_cache();
		$this->db->where('serial', $id);
		$this->db->set('edited_date', 'now()', FALSE);
		$data = array(
        'flag' => 'D',
		);
		$result = $this->db->update('car_list', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	//점검 내역

	function add_check($data){
		$sql = "SELECT * FROM car_check_history WHERE car_serial=".$data['car_serial']." AND type='".$data['type']."' AND check_date='".$data['check_date']."'";
		$query = $this->db->query($sql);
		$result_count = $query->num_rows();

		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록 되어 있습니다.";		
		}else{
			$result = $this->db->insert('car_check_history', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;
	}

	function update_check($search_id, $data){

		$this->db->where('serial', $search_id);
		$result = $this->db->update('car_check_history', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
		
	}


	function delete_check($id){

		$result = $this->db->delete('car_check_history', array('serial' => $id));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	//소모품교환

	function add_supply($data){
		$sql = "SELECT * FROM car_supply_history WHERE car_serial=".$data['car_serial']." AND type='".$data['type']."' AND check_date='".$data['check_date']."'";
		$query = $this->db->query($sql);
		$result_count = $query->num_rows();

		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록 되어 있습니다.";		
		}else{
			$result = $this->db->insert('car_supply_history', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;
	}

	function update_supply($search_id, $data){

		$this->db->where('serial', $search_id);
		$result = $this->db->update('car_supply_history', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
		
	}


	function delete_supply($id){

		$result = $this->db->delete('car_supply_history', array('serial' => $id));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	// 사고 

	function add_accident($data){
		$sql = "SELECT * FROM car_accident_history WHERE car_serial=".$data['car_serial']." AND accident_date='".$data['accident_date']."'";
		$query = $this->db->query($sql);
		$result_count = $query->num_rows();

		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록 되어 있습니다.";		
		}else{
			$result = $this->db->insert('car_accident_history', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;
	}

	function update_accident($search_id, $data){

		$this->db->where('serial', $search_id);
		$result = $this->db->update('car_accident_history', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
		
	}


	function delete_accident($id){

		$result = $this->db->delete('car_accident_history', array('serial' => $id));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	// 정비

	function add_repair($data){
		$sql = "SELECT * FROM car_repair_history WHERE car_serial=".$data['car_serial']." AND type='".$data['type']."' AND repair_start_date='".$data['repair_start_date']."' AND repair_end_date='".$data['repair_end_date']."'";
		$query = $this->db->query($sql);
		$result_count = $query->num_rows();

		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록 되어 있습니다.";		
		}else{
			$result = $this->db->insert('car_repair_history', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;
	}

	function update_repair($search_id, $data){

		$this->db->where('serial', $search_id);
		$result = $this->db->update('car_repair_history', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
		
	}


	function delete_repair($id){

		$result = $this->db->delete('car_repair_history', array('serial' => $id));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	//출동
		function add_emergency($data){
		
		$result = $this->db->insert('car_emergency_history', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	function update_emergency($search_id, $data){

		$this->db->where('serial', $search_id);
		$result = $this->db->update('car_emergency_history', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
		
	}


	function delete_emergency($id){

		$result = $this->db->delete('car_emergency_history', array('serial' => $id));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}



	function get_car_information($company_serial, $car_serial){
		if($company_serial >= '1' && $car_serial >= '1'){
			$sql = "select 
					*
					from car_list 
					where
					company_serial = '".$company_serial."'  
					and serial <= '".$car_serial."' 
			";

			$result = $this->db->fReadSql($sql, '');
			return $result;
		} else {
			return false;
		}

	}

}