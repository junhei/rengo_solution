<?php
class Car_schedule_model extends CI_Model {
 

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

	}
	//company의 배차, 회차 정보를 start, end기간내에 가져옴
	function get_car_schedule($start, $end, $company){
		$sql = "select 
				order_list.delivery_place, order_list.username, order_list.period_start, car_list.car_number
				from order_list, car_list 
				where (order_list.flag = 'F' or order_list.flag = 'P')
				and order_list.period_start >= '".$start."'  
				and order_list.period_start <= '".$end."' 
				and order_list.car_serial = car_list.serial";

		//company!=0 추가하면 모든 회사볼수 있음
		if($company!=''){
			$sql = $sql." AND order_list.company_serial = ".$company;
		}

		$result = $this->db->fReadSql($sql, '');
		$delivery_count = 0;
		foreach($result as $data){
			//시작날짜를 모두 0분으로 설정
			$delivery_start_date = date('Ymd', strtotime($data['period_start']));
			$start_date = new DateTime($delivery_start_date);
			$start_date->setTime(intval(date('H', strtotime($data['period_start']))), 0);
			$start_str = $start_date->format('Y-m-d\TH:i:sO');
			$tok = explode('/', $data['delivery_place']);
			if($delivery_result[$start_str] != null){
				$delivery_count++;
				// $title = $send_result[$start_str]['title'];
				// $title = $title."<br/>&nbsp;<배차>".$data['car_number']."-".$tok[count($tok)-1];
			}else{
				$delivery_count = 1;
				// $title = "<배차>".$data['car_number']."-".$tok[count($tok)-1];
				// $title = "배차";
			}
			$delivery_result[$start_str] = array(
				'type' => 1,
				'start' => date("Y-m-d\TH:i:sO", strtotime($start_str)) ,
				// 'StartTimezone' => "Asia/Seoul",
				'end' => date("Y-m-d\TH:i:sO", strtotime($start_str.'+58 minutes') ),
				// 'EndTimezone' => "Asia/Seoul",
				'title' => "배차 ".$delivery_count." 건"
				// 'description' => "(배)".$data['username']."(".$data['tel'].")"
			);

		}
		// var_dump($send_result);

		$sql = "select 
				order_list.pickup_place, order_list.username, order_list.period_finish, car_list.car_number
				from order_list, car_list 
				where (order_list.flag = 'F' or order_list.flag = 'P')
				and order_list.period_finish >= '".$start."'  
				and order_list.period_finish <= '".$end."' 
				and order_list.car_serial = car_list.serial";
		//company!=0 추가하면 모든 회사볼수 있음
		if($company!=''){
			$sql = $sql." AND order_list.company_serial = ".$company; 
		}
		$result = $this->db->fReadSql($sql, '');
		$pickup_count = 0;
		foreach($result as $data){
			//시작날짜를 모두 0분으로 설정
			$delivery_start_date = date('Ymd', strtotime($data['period_finish']));
			$start_date = new DateTime($delivery_start_date);
			$start_date->setTime(intval(date('H', strtotime($data['period_finish']))), 0);
			$start_str = $start_date->format('Y-m-d\TH:i:sO');
			$tok = explode('/', $data['pickup_place']);
			

			if($pickup_result[$start_str] != null){
				$pickup_count++;
				// $title = $send_result[$start_str]['title'];
				// $title = $title."<br/>&nbsp;<회차>".$data['car_number']."-".$tok[count($tok)-1];
			}
			else{
				$pickup_count = 1;
				// $title = "<회차>".$data['car_number']."-".$tok[count($tok)-1];
			}
			$pickup_result[$start_str] = array(
				'type' => 2,
				'start' => date("Y-m-d\TH:i:sO", strtotime($start_str)) ,
				// 'StartTimezone' => "Asia/Seoul",
				'end' => date("Y-m-d\TH:i:sO", strtotime($start_str.'+59 minutes') ),
				// 'EndTimezone' => "Asia/Seoul",
				'title' => "회차 ".$pickup_count." 건"
				// 'description' => $data['username']."(".$data['tel'].")"
			);
		}



		if($delivery_result==null){
			
		}else{
			foreach($delivery_result as $data){
				$result_array[] = array(
				'type' => $data['type'],
				'start' => $data['start'],
				'end' => $data['end'],
				'title' => $data['title']
				);

			}
		}

		if($pickup_result==null){
			$result_array = array();
		}else{
			foreach($pickup_result as $data){
				$result_array[] = array(
				'type' => $data['type'],
				'start' => $data['start'],
				'end' => $data['end'],
				'title' => $data['title']
				);

			}
		}

		return $result_array;

	}

	//특정 시간대의 company 배차, 회차 정보를 자세히 가져옴
	function get_car_schedule_detail($start, $end, $company){

		$sql = " 
				select 
				order_list.serial, car_list.car_number, order_list.delivery_place, order_list.username, order_list.tel, order_list.period_start, order_list.rental_staff_name, car_master.car_name_detail
				from order_list , car_list, car_master
				where (order_list.flag = 'F' or order_list.flag = 'P')
				and order_list.period_start >= '".$start."'  
				and order_list.period_start <= '".$end."' 
        		and order_list.car_serial = car_list.serial
        		and car_list.car_index = car_master.car_index";

        if($company!='' && $company!=0){
			$sql = $sql." AND order_list.company_serial = ".$company;
		}

		$result = $this->db->fReadSql($sql, '');
		foreach($result as $data){
			if($data['rental_staff_name']=='' || $data['rental_staff_name'] == null){
				$handler = "담당자 미지정";
			}else{
				$handler = $data['rental_staff_name'];
			}
			$send_result[] = array(
				'type' => '대여',
				'serial' => $data['serial'],
				'car_name_detail' => $data['car_name_detail'],
 				'car_number' => $data['car_number'],
				'place' => $data['delivery_place'],
				'name' => $data['username'],
				'phone' => $data['tel'],
				'date' => date('Y-m-d H:i', strtotime($data['period_start'])),
				'handler' => $handler
				);
		}

		$sql = " 
				select 
				order_list.serial, car_list.car_number, order_list.pickup_place, order_list.username, order_list.tel, order_list.period_finish, order_list.return_staff_name, car_master.car_name_detail
				from order_list , car_list, car_master
				where (order_list.flag = 'F' or order_list.flag = 'P')
				and order_list.period_finish >= '".$start."'  
				and order_list.period_finish <= '".$end."' 
        		and order_list.car_serial = car_list.serial
        		and car_list.car_index = car_master.car_index";

        if($company!='' && $company!=0){
			$sql = $sql." AND order_list.company_serial = ".$company;
		}

		$result = $this->db->fReadSql($sql, '');
		foreach($result as $data){
			if($data['return_staff_name']=='' || $data['return_staff_name'] == null){
				$handler = "담당자 미지정";
			}else{
				$handler = $data['return_staff_name'];
			}
			$send_result[] = array(
				'type' => '반납',
				'serial' => $data['serial'],
				'car_name_detail' => $data['car_name_detail'],
				'car_number' => $data['car_number'],
				'place' => $data['pickup_place'],
				'name' => $data['username'],
				'phone' => $data['tel'],
				'date' => date('Y-m-d H:i', strtotime($data['period_finish'])),
				'handler' => $handler
				);
		}

		if($send_result==null){
			$send_result = array();
		}

		//날짜순으로 데이터 정렬. 재혁 추가
		function dateComp($a,$b){
			return strtotime($a['date'])-strtotime($b['date']);
		}
		usort($send_result,'dateComp');
		
		return $send_result;
	}

	function get_count_delivery_pickup($start, $end, $company){
		//대여 구하기
		$sql = "select 
				*
				from order_list, car_list 
				where (order_list.flag = 'F' or order_list.flag = 'P')
				and order_list.period_start >= '".$start."'  
				and order_list.period_start <= '".$end."' 
				and order_list.car_serial = car_list.serial";

		//company!=0 추가하면 모든 회사볼수 있음
		if($company!=''){
			$sql = $sql." AND order_list.company_serial = ".$company;
		}

		$result = $this->db->fReadSql($sql);
		$delivery_count = count($result);

		$sql = "select 
				*
				from order_list, car_list 
				where (order_list.flag = 'F' or order_list.flag = 'P')
				and order_list.period_finish >= '".$start."'  
				and order_list.period_finish <= '".$end."' 
				and order_list.car_serial = car_list.serial";
		//company!=0 추가하면 모든 회사볼수 있음
		if($company!=''){
			$sql = $sql." AND order_list.company_serial = ".$company; 
		}
		$result = $this->db->fReadSql($sql);
		$pickup_count = count($result);
		$response = array(
			'delivery' => $delivery_count,
			'pickup' => $pickup_count
		);

		return $response;
	}

}