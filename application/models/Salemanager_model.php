<?php
class Salemanager_model extends CI_Model {
 

	function __construct()
	{
		parent::__construct();
	}

	function orderday_period_sales($company_serial,$category,$year,$month=0)
	{
		$sql1="
		SELECT
			MONTH(registered_date) AS classify,
			COUNT(serial) AS count,
			SUM(total_price) AS sales
		FROM
			order_list
		WHERE 
			YEAR(registered_date) = '".$year."'
		";
		
		$wherecon = $company_serial == 0? '' : " AND company_serial='".$company_serial."'";
		
		$categorycon = $category == ''? '' : " AND order_kind = '".$category."'";

		$sql2="
		GROUP BY
			classify
		";

		// echo $sql1.$wherecon.$categorycon.$sql2;die();
		$result = $this->db->fReadSql($sql1.$wherecon.$categorycon.$sql2);
		return $result;
	}

	function orderday_period_sales_per_month($company_serial,$category,$year,$month)
	{
		$sql1="
		SELECT
			DAY(registered_date) AS classify,
			COUNT(serial) AS count,
			SUM(total_price) AS sales
		FROM
			order_list
		WHERE 
			YEAR(registered_date) = '".$year."'
		";
		
		$wherecon = $company_serial == 0? '' : " AND company_serial='".$company_serial."'";
		
		$categorycon = $category == ''? '' : " AND order_kind = '".$category."'";
		
		$monthcon = $month == 0? '' : " AND MONTH(registered_date)='".$month."'";

		$sql2="
		GROUP BY
			classify
		";

		// echo $sql1.$wherecon.$categorycon.$monthcon.$sql2;die();
		$result = $this->db->fReadSql($sql1.$wherecon.$categorycon.$monthcon.$sql2);
		return $result;
	}

	function orderday_carnumber_sales($company_serial,$category,$year,$month)
	{
		$sql1="
		SELECT
			CM.car_name_detail AS car_name_detail,
			C.car_number AS classify,
			COUNT(OL.serial) AS count,
			SUM(OL.total_price) AS sales
		FROM
			order_list OL
		INNER JOIN
			car_list C
		ON
			OL.car_serial = C.serial
		INNER JOIN 
			car_master CM
		ON
			CM.car_index = C.car_index	
		WHERE 
			YEAR(OL.registered_date) = '".$year."'
		";
		
		$wherecon = $company_serial == 0? '' : " AND OL.company_serial='".$company_serial."'";
		
		$categorycon = $category == ''? '' : " AND OL.order_kind = '".$category."'";
		
		$monthcon = $month == 0? '' : " AND MONTH(OL.registered_date)='".$month."'";

		$sql2="
		GROUP BY
			classify
		";

		// echo $sql1.$wherecon.$categorycon.$monthcon.$sql2;die();
		$result = $this->db->fReadSql($sql1.$wherecon.$categorycon.$monthcon.$sql2);
		return $result;
	}

	function orderday_acceptance_sales($company_serial,$category,$year,$month)
	{
		$sql1="
		SELECT 
			deposite_way AS classify, 
			COUNT(user_serial) AS count, 
			SUM(deposite_amount) AS sales
		FROM 
			receipt_history
		WHERE
			YEAR(deposite_date) ='".$year."'
		";
	
		$wherecon = $company_serial == 0? '' : " AND OL.company_serial='".$company_serial."'";
		
		$categorycon = $category == ''? '' : " AND OL.order_kind = '".$category."'";
		
		$monthcon = $month == 0? '' : " AND MONTH(OL.registered_date)='".$month."'";

		$sql2="
		GROUP BY
			classify
		";
		// echo $sql1.$wherecon.$categorycon.$monthcon.$sql2;die();
		$result=$this->db->fReadSql($sql1.$wherecon.$categorycon.$monthcon.$sql2);

		return $result;
	}

	function period_sales($company_serial,$category,$year,$month=0)
	{
		$sql1="
		SELECT 
			MONTH(deposite_date) AS classify,
			#COUNT(order_serial) AS count,
			SUM(deposite_amount) AS sales
		FROM
			receipt_history
		WHERE 
			YEAR(deposite_date) = '".$year."'	
		";
		
		if($company_serial==0){
			$wherecon='';
		} else {
			$wherecon="
			AND
				company_serial='".$company_serial."'";
		}

		if($category == '') {
			$categorycon="
			";
		} else {
			$categorycon=" 
			AND
				type='대여금액'
			AND 
				".$category."='Y'";
		}	
		
		$sql2="
		GROUP BY
			classify
		";
		
		// echo $sql1.$wherecon.$categorycon.$sql2;die();
		$result=$this->db->fReadSql($sql1.$wherecon.$categorycon.$sql2);
		$period_count = $this->period_count($company_serial,$category,$year,$month=0);
		
		for($i=0;$i<count($result);++$i){
			$result[$i] = array_merge($result[$i],$period_count[$i]);
		}

		return $result;
	}

	function period_count($company_serial,$category,$year,$month=0) {
		$sql1="
		SELECT 
			MONTH(deposite_date) AS classify,
			COUNT(order_serial) AS count
		FROM
			receipt_history
		WHERE 
			YEAR(deposite_date) = '".$year."'
		AND 
			type = '대여금액'
		";
		
		if($company_serial==0){
			$wherecon='';
		} else {
			$wherecon="
			AND
				company_serial='".$company_serial."'";
		}

		if($category == '') {
			$categorycon="
			";
		} else {
			$categorycon=" 
			AND 
				".$category."='Y'";
		}	
		
		$sql2="
		GROUP BY
			classify
		";
		
		// echo $sql1.$wherecon.$categorycon.$sql2;die();
		$result=$this->db->fReadSql($sql1.$wherecon.$categorycon.$sql2);
		return $result;
	}

	function period_sales_per_month($company_serial,$category,$year,$month)
	{
		$sql1="
		SELECT 
			DAY(deposite_date) AS classify,
			#COUNT(user_serial) AS count,
			SUM(deposite_amount) AS sales
		FROM
			receipt_history
		WHERE 	
			YEAR(deposite_date) = '".$year."'
		";
		
		if($company_serial==0){
			$wherecon='';
		} else {
			$wherecon="
			AND
				company_serial='".$company_serial."'";
		}

		if($category == '') {
			$categorycon="
			";
		} else {
			$categorycon=" 
			AND
				type='대여금액'
			AND 
				".$category."='Y'";
		}	
		
		if($month == 0) {
			$monthcon='';
		} else {
			$monthcon=" AND MONTH(deposite_date)='".$month."'";
		}

		$sql2="
		GROUP BY
			classify
		";

		// echo $sql1.$wherecon.$categorycon.$monthcon.$sql2; die();
		$result=$this->db->fReadSql($sql1.$wherecon.$categorycon.$monthcon.$sql2);
		$period_per_month_count=$this->period_per_month_count($company_serial,$category,$year,$month);

		for($i=0;$i<count($result);++$i){
			$result[$i] = array_merge($result[$i],$period_per_month_count[$i]);
		}

		return $result;
	}

	function period_per_month_count($company_serial,$category,$year,$month) 
	{
		$sql1="
		SELECT 
			DAY(deposite_date) AS classify,
			COUNT(user_serial) AS count
		FROM
			receipt_history
		WHERE 	
			YEAR(deposite_date) = '".$year."'
		AND 
			type = '대여금액'
		";
		
		if($company_serial==0){
			$wherecon='';
		} else {
			$wherecon="
			AND
				company_serial='".$company_serial."'";
		}

		if($category == '') {
			$categorycon="
			";
		} else {
			$categorycon=" 
			AND 
				".$category."='Y'";
		}	
		
		if($month == 0) {
			$monthcon='';
		} else {
			$monthcon=" AND MONTH(deposite_date)='".$month."'";
		}

		$sql2="
		GROUP BY
			classify
		";

		// echo $sql1.$wherecon.$categorycon.$monthcon.$sql2; die();
		$result=$this->db->fReadSql($sql1.$wherecon.$categorycon.$monthcon.$sql2);
		return $result;
	}

	function carnumber_sales($company_serial,$category,$year,$month) 
	{
		$sql1="
		SELECT
			CM.car_name_detail AS car_name_detail,
			C.car_number AS classify,
			#COUNT(R.user_serial) AS count,
			SUM(R.deposite_amount) AS sales
		FROM
			receipt_history R
		INNER JOIN
			car_list C
		ON
			R.car_serial = C.serial
		INNER JOIN 
			car_master CM
		ON
			CM.car_index = C.car_index
		WHERE
			YEAR(deposite_date) = '".$year."'
		";
		

		if($company_serial==0){
			$wherecon='';
		} else {
			$wherecon="
			AND
				R.company_serial='".$company_serial."'";
		}

		if($category == '') {
			$categorycon="
			";
		} else {
			$categorycon=" 
			AND
				type='대여금액'
			AND 
				".$category."='Y'";
		}

		if($month == 0) {
			$monthcon='';
		} else {
			$monthcon=" AND MONTH(R.deposite_date)='".$month."'";
		}

		$sql2="
		GROUP BY
			classify
		";
		// echo $sql1.$wherecon.$categorycon.$monthcon.$sql2;die();
		$result=$this->db->fReadSql($sql1.$wherecon.$categorycon.$monthcon.$sql2);
		$carnumber_count = $this->carnumber_count($company_serial,$category,$year,$month);

		for($i=0;$i<count($result);++$i){
			$result[$i] = array_merge($result[$i],$carnumber_count[$i]);
		}

		return $result;
	}

	function carnumber_count($company_serial,$category,$year,$month) 
	{
		$sql1="
		SELECT
			CM.car_name_detail AS car_name_detail,
			C.car_number AS classify,
			COUNT(R.user_serial) AS count
		FROM
			receipt_history R
		INNER JOIN
			car_list C
		ON
			R.car_serial = C.serial
		INNER JOIN 
			car_master CM
		ON
			CM.car_index = C.car_index
		WHERE
			YEAR(deposite_date) = '".$year."'
		AND 
			type = '대여금액'
		";
		
		if($company_serial==0){
			$wherecon='';
		} else {
			$wherecon="
			AND
				R.company_serial='".$company_serial."'";
		}

		if($category == '') {
			$categorycon="
			";
		} else {
			$categorycon=" 
			AND 
				".$category."='Y'";
		}

		if($month == 0) {
			$monthcon='';
		} else {
			$monthcon=" AND MONTH(R.deposite_date)='".$month."'";
		}

		$sql2="
		GROUP BY
			classify
		";
		// echo $sql1.$wherecon.$categorycon.$monthcon.$sql2;die();
		$result=$this->db->fReadSql($sql1.$wherecon.$categorycon.$monthcon.$sql2);
		return $result;
	}

	function acceptance_sales($company_serial,$category,$year,$month)
	{
		$sql1="
		SELECT 
			deposite_way AS classify, 
			#COUNT(user_serial) AS count, 
			SUM(deposite_amount) AS sales
		FROM 
			receipt_history
		WHERE
			YEAR(deposite_date) ='".$year."'
		";

		if($company_serial==0){
			$wherecon='';
		} else {
			$wherecon="
			AND
				company_serial='".$company_serial."'";
		}

		if($category == '') {
			$categorycon="
			";
		} else {
			$categorycon=" 
			AND
				type='대여금액'
			AND 
				".$category."='Y'";
		}

		if($month == 0) {
			$monthcon='';
		} else {
			$monthcon=" AND MONTH(deposite_date)='".$month."'";
		}	

		$sql2="
		GROUP BY
			classify
		";
		// echo $sql1.$wherecon.$categorycon.$monthcon.$sql2;die();
		$result=$this->db->fReadSql($sql1.$wherecon.$categorycon.$monthcon.$sql2);
		$acceptance_count=$this->acceptance_count($company_serial,$category,$year,$month);

		for($i=0;$i<count($result);++$i){
			$result[$i] = array_merge($result[$i],$acceptance_count[$i]);
		}

		return $result;
	}

	function acceptance_count($company_serial,$category,$year,$month)
	{
		$sql1="
		SELECT 
			deposite_way AS classify, 
			COUNT(user_serial) AS count
		FROM 
			receipt_history
		WHERE
			YEAR(deposite_date) ='".$year."'
		AND 
			type = '대여금액'
		";

		if($company_serial==0){
			$wherecon='';
		} else {
			$wherecon="
			AND
				company_serial='".$company_serial."'";
		}

		if($category == '') {
			$categorycon="
			
			";
		} else {
			$categorycon=" 
			AND 
				".$category."='Y'";
		}

		if($month == 0) {
			$monthcon='';
		} else {
			$monthcon=" AND MONTH(deposite_date)='".$month."'";
		}	

		$sql2="
		GROUP BY
			classify
		";
		// echo $sql1.$wherecon.$categorycon.$monthcon.$sql2;die();
		$result=$this->db->fReadSql($sql1.$wherecon.$categorycon.$monthcon.$sql2);
		return $result;
	}

	function get_carnumbers($company_serial)
	{
		$sql1="
		SELECT 
			CM.car_name_detail AS car_name_detail,
			C.car_number AS classify
		FROM 
			receipt_history R 
		INNER JOIN 
			car_list C 
		ON
			R.car_serial = C.serial 
		INNER JOIN
			car_master CM
		ON
			CM.car_index = C.car_index
		";

		if($company_serial==0){
			$wherecon='';	
		} else {
			$wherecon="
			WHERE 
				R.company_serial='".$company_serial."'";	
		}

		$sql2="
 		GROUP BY 
 			classify
		";

		// echo $sql1.$wherecon.$sql2; die();
		$result=$this->db->fReadSql($sql1.$wherecon.$sql2);
		return $result;
	}

	function get_acceptances($company_serial)
	{
		$sql1="
		SELECT 
			deposite_way AS classify
		FROM 
			receipt_history R 
		INNER JOIN 
			car_list C 
		ON
			R.car_serial = C.serial 
		";

		if($company_serial==0){
			$wherecon='';	
		} else {
			$wherecon="
			WHERE 
				R.company_serial='".$company_serial."'";	
		}

		$sql2="
 		GROUP BY 
 			classify
		";

		$result=$this->db->fReadSql($sql1.$wherecon.$sql2);
		return $result;
	}

	function type_data($company_serial,$type,$by_way,$year,$month)
	{		
		if($by_way=='period_sales' || $by_way=='orderday_period_sales') {
			$classify='MONTH(R.deposite_date)';
			$joincon='';
		} else if($by_way=='carnumber_sales' || $by_way=='orderday_carnumber_sales') {
			$classify='C.car_number';
			$joincon='
			INNER JOIN
				car_list C
			ON
				R.car_serial=C.serial
			';
		} else if($by_way=='acceptance_sales') {
			$classify='R.deposite_way';
			$joincon='';
		} else if ($by_way == 'period_sales_per_month' || $by_way == 'orderday_period_sales_per_month') {
			$classify='DAY(R.deposite_date)';
			$joincon='';
		}

		$type_price_key = array(
			'대여금액'=>'rental_charge',
			'대여기간할인'=>'period_discount',
			'특정기간할인'=>'special_period_discount',
			'기타할인'=>'other_discount',
			'배차'=>'car_allocation',
			'회차'=>'car_return',
			'배회차'=>'car_allocation_return',
			'자차1'=>'insurance_self1',
			'자차2'=>'insurance_self2',
			'자차3'=>'insurance_self3',
			'초과'=>'excess_charge',
			'환불'=>'refund_charge',
			'카시트'=>'car_seat',
			'스노우체인'=>'snow_chain',
			'기타유료옵션'=>'other_paid_option',
			'수리비'=>'repair_cost',
			'청구손실비'=>'loss_cost_billing',
			'사고면책금'=>'accident_exemption_cost',
			'사고보상금'=>'accident_compensation',
			'유류비용'=>'fuel_cost',
			'범칙금'=>'fine',
			'보증금'=>'security_deposit',
			'기타'=>'others',
			'총미수액'=>'outstanding_amount'
		);


		$sql1="
		SELECT
			".$classify." AS classify,
			SUM(R.deposite_amount) AS '".$type_price_key[$type]."'
		FROM
			receipt_history R
		";
		
		$wherecon1="
		WHERE
			YEAR(deposite_date) ='".$year."'
		AND
			R.type='".$type."'
		";

		if($company_serial==0){
			$wherecon2='';
		} else {
			$wherecon2="
			AND	
				R.company_serial='".$company_serial."'";	
		}
		
		if($month == 0) {
			$monthcon='';
		} else {
			$monthcon=" AND MONTH(R.deposite_date)='".$month."'";
		}
		
		$sql2="
		GROUP BY
			classify
		";
		// echo $sql1.$joincon.$wherecon1.$wherecon2.$monthcon.$sql2; die();
		$result=$this->db->fReadSql($sql1.$joincon.$wherecon1.$wherecon2.$monthcon.$sql2);

		return $result;
	}

	function detail_reservaion($company_serial, $start_day, $end_day){

		$sql = "SELECT 
		    O.serial, COM.company_name, CAR.car_number, O.period_start, O.period_finish, U.user_name, U.birthday, U.phone_number1, O.pickup_place, O.delivery_place, O.rental_price, O.insurance_price, O.price_off, R.deposite_amount, R.deposite_date, M.car_name_detail
			FROM
			    company_information AS COM, order_list AS O, receipt_history AS R, car_list AS CAR, user_information AS U, car_master AS M
			WHERE
			     R.deposite_date >= '".$start_day."'
			AND
				R.deposite_date <= '".$end_day."' 
			AND
			   R.order_serial = O.serial 
			AND
			   O.car_serial = CAR.serial 
			AND
			  O.company_serial = COM.serial 
			AND
			  O.user_serial = U.user_serial 
			AND
			  CAR.car_index = M.car_index
			AND
			  COM.serial=".$company_serial;


		$result = $this->db->fReadSql($sql);



		if(count($result)==0){
			return array();
		}else{
			for($i=0; $i<count($result); $i++){

				$place = $result[$i]['delivery_place']." => ".$result[$i]['pickup_place'];
				$period = substr($result[$i]['period_start'], 0, 4)."-".substr($result[$i]['period_start'], 4, 2)."-".substr($result[$i]['period_start'], 6, 2)." ".substr($result[$i]['period_start'], 8, 2).":".substr($result[$i]['period_start'], 10, 2)." ~ ".substr($result[$i]['period_finish'], 0, 4)."-".substr($result[$i]['period_finish'], 4, 2)."-".substr($result[$i]['period_finish'], 6, 2)." ".substr($result[$i]['period_finish'], 8, 2).":".substr($result[$i]['period_finish'], 10, 2);

				$send_array[] = array(
					'serial' => $result[$i]['serial'],
					'company_name' => $result[$i]['company_name'],
					'car_number' => $result[$i]['car_number'],
					'period' => $period,
					'user_name' => $result[$i]['user_name'],
					'birthday' => $result[$i]['birthday'],
					'phone_number1' => $result[$i]['phone_number1'],
					'place' => $place,
					'rental_price' => $result[$i]['rental_price'],
					'insurance_price' => $result[$i]['insurance_price'],
					'price_off' => $result[$i]['price_off'],
					'deposite_amount' => $result[$i]['deposite_amount'],
					'deposite_date' => $result[$i]['deposite_date']
				);

			}
			

			return $send_array;
			
		}
		
	}

	function get_settlementstatement($company_serial, $start_date, $end_date){
		$sql = "select
			  R.serial, M.car_name_detail, O.period_start, O.period_finish, U.user_name, U.phone_number1, R.deposite_amount 
			from
			  receipt_history as R, order_list as O, car_list as C, user_information as U, car_master as M
			Where
			  O.flag = 'F'
			AND
			  R.order_serial = O.serial
			AND
			  R.user_serial = U.user_serial
			AND
			  R.car_serial = C.serial
			AND
			  C.car_index = M.car_index
			AND 
			  R.deposite_amount > 0
			AND
			  R.company_serial = '".$company_serial."'
			AND
			  O.period_start > '".$start_date."'
			AND
			  O.period_finish < '".$end_date."'
			ORDER BY O.period_start asc";

		$result=$this->db->fReadSql($sql);
		$size = count($result);
		$week = array('일', '월', '화', '수', '목', '금', '토');

		if($size==0){
			return array();
		}else{
			$total_money = 0;
			for($i=0;$i<$size; $i++){
				$result[$i]['order'] = ($i +1);
				$select_start_time = mktime(substr($result[$i]['period_start'], 8, 2), substr($result[$i]['period_start'], 10, 2), 0, substr($result[$i]['period_start'], 4, 2), substr($result[$i]['period_start'], 6, 2), substr($result[$i]['period_start'], 0, 4));
				$week_num1 = date("w",$select_start_time);

				$select_end_time = mktime(substr($result[$i]['period_finish'], 8, 2), substr($result[$i]['period_finish'], 10, 2), 0, substr($result[$i]['period_finish'], 4, 2), substr($result[$i]['period_finish'], 6, 2), substr($result[$i]['period_finish'], 0, 4));
				$week_num2 = date("w",$select_end_time);


				$result[$i]['rent_date']  = date("m/d($week[$week_num1])h시i분부터 ", $select_start_time)."\n".date("m/d($week[$week_num2])h시i분까지", $select_end_time);
				// $result[$i]['rent_date']  = substr($result[$i]['period_start'], 0, 4)."-".substr($result[$i]['period_start'], 4, 2)."-".substr($result[$i]['period_start'], 6, 2)." ".substr($result[$i]['period_start'], 8, 2).":".substr($result[$i]['period_start'], 10, 2)." ~ ".substr($result[$i]['period_finish'], 0, 4)."-".substr($result[$i]['period_finish'], 4, 2)."-".substr($result[$i]['period_finish'], 6, 2)." ".substr($result[$i]['period_finish'], 8, 2).":".substr($result[$i]['period_finish'], 10, 2);
				$result[$i]['user'] = $result[$i]['user_name']."(".$result[$i]['phone_number1'].")";

				$total_money += intval($result[$i]['deposite_amount']);
				// $result[$i]['deposite_amount'] = 
			}
			$servie_fee = $total_money*0.1; //수수료(=공급대가)
			$profit = $servie_fee/1.1; //공급가액
			$result[0]['total_price'] = $total_money;
			$result[0]['service_fee'] = $servie_fee;
			$result[0]['calcuration_fee'] = $total_money*0.9;
			$result[0]['profit'] = $profit;
			$result[0]['add_price'] = $servie_fee - $profit;

			return $result;
		}
		


	}



	function m_get_rengo_money($company_serial, $start_day, $end_day){

		$sql = "SELECT 
		   		R.deposite_amount, R.type
			FROM
			    receipt_history AS R, order_list AS O
			WHERE
			   O.period_start > '".$start_day."'
			AND
			   O.period_finish < '".$end_day."'
			AND
			   R.order_serial = O.serial 
			AND
			   R.rengo_flag = 'Y'
			AND
			   R.company_serial=".$company_serial;


		$receipt_array = $this->db->fReadSql($sql);



		if(count($receipt_array)==0){
			$send_array = array(
					'rental_price' => 0,
					'insurance_price' => 0,
					'total_rental_price' => 0,
					'service_price' => 0,
					'payback_price' => 0
			
			);

			return $send_array;
		}else{
			$rental_price = 0;
			$insurance_price = 0;
			for($i=0; $i<count($receipt_array); $i++){
				if($receipt_array[$i]['type'] == "대여금액"){
					$rental_price += intval($receipt_array[$i]['deposite_amount']);
				}else if($receipt_array[$i]['type'] == "할인요금"){
					$rental_price -= intval($receipt_array[$i]['deposite_amount']);
				}else if($receipt_array[$i]['type'] == "보험요금"){
					$insurance_price += intval($receipt_array[$i]['deposite_amount']);
				}
			}
			$total_price = $rental_price + $insurance_price;
			$service_price = $total_price*0.1;
			$payback_price = $total_price - $service_price;

			$send_array = array(
					'rental_price' => $rental_price,
					'insurance_price' => $insurance_price,
					'total_rental_price' => $total_price,
					'service_price' => $service_price,
					'payback_price' => $payback_price
			
			);
			

			return $send_array;
			
		}
		
	}

}	
