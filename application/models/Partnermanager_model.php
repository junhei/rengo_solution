<?php
class Partnermanager_model extends CI_Model {
 

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		// $this->output->enable_profiler(TRUE);
	}

	function partner_information($partner_serial) {
		$sql="
		SELECT
		  	*
	 	FROM
	 		company_information
		";

		if($partner_serial != ''){
			$wherecon=" WHERE serial='".$partner_serial."'";
		} else {
			$wherecon='';
		}

		// echo $sql.$wherecon;
		// die();
		$result=$this->db->fReadSql($sql.$wherecon);
		return $result;
	}

	function service_area($partner_serial) {
		$sql1 = "
		SELECT
		   b.company_serial,
		   a.location_name_kor,
		   a.area_kind,
		   a.city_name
		FROM
		   service_area_information a,
		   service_area_list b
		WHERE
		   a.serial = b.service_area_serial
		";
		
		if($partner_serial != ''){
			$wherecon=" AND b.company_serial='".$partner_serial."'";
		} else {
			$wherecon='';
		}
	  	
	  	$sql2="
		ORDER BY
		   b.company_serial ASC,
		   b.service_area_serial ASC,
		   a.serial ASC
		";
		// echo $sql1.$wherecon.$sql2;
		// die();
		$result=$this->db->fReadSql($sql1.$wherecon.$sql2, '', '', '');
		return $result;
	}

	function register_partner_information($partner_serial,$data) 
	{
		$this->db->where('serial',$partner_serial);
		$result=$this->db->update('company_information',$data);
		if($result){
			$response['code']='S01';
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	// function register_btn_flag($id)
	// {
	// 	$sql="
	// 	SELECT
	// 		flag
	// 	FROM
	// 		company_information
	// 	WHERE
	// 		id='".$id."'	
	// 	";

	// 	$result=$this->db->fReadSql($sql);
	// 	return $result;
	// }

	function update_flag($id){
		$this->db->where('id',$id);
		$flag=array('flag'=>'Y');
		$result=$this->db->update('company_information',$flag);
	}

	function get_company_info($id)
	{
		$sql="
			SELECT 
				*
			FROM
				company_information
			WHERE
				serial='".$id."'
			";
		
		$company_info=$this->db->fReadSql($sql);
		return $company_info;
	}

	function write_admin_info($data)
	{
		$result=$this->db->insert('admin_information',$data);

		if($result){
			$response['code']='S01';
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

}