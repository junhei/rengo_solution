<?php
class Area_model extends CI_Model {
 

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

	}

	function get_count($company_serial){
		$sql = "SELECT 
				count(*) as count 
				FROM service_area_list 
				where company_serial = '".$company_serial."'
				AND is_subway = 'Y'";
		$result1 =  $this->db->fReadSql($sql);

		$sql2 = "SELECT 
				count(*) as count 
				FROM service_area_list 
				where company_serial = '".$company_serial."'
				AND is_subway = 'N'";
		$result2 =  $this->db->fReadSql($sql2);

		$response = array(
			'count_subway' => $result1[0]['count'],
			'count_spot' =>  $result2[0]['count']
		);
		return $response;

	}
	//모든 도시명 얻기(spot 추가에서 도시명 얻을 때)
	function get_all_city(){
		$sql = "select
      				sido
				from
    				area
				group by sido";

		$result_array =  $this->db->fReadSql($sql, '');

		return $result_array;
	}

	//지하철역이 등록 되어 있는 도시들만 얻기
	function get_subway_city(){
		$sql = "select
      				substring_index(substring_index(area_kind,' ', 2),' ' ,-1) as city
				from
    				service_area_information
    			where
    				flag = 'Y'
    			AND
    				substring_index(substring_index(area_kind,' ', 1),' ' ,-1) = 'subway'
				group by city 
				order by city desc";

		$result_array =  $this->db->fReadSql($sql, '');

		return $result_array;

	}
	//spot이 등록되어 있는 도시만 얻기
	function get_spot_city(){
		$sql = "select
      				substring_index(substring_index(area_kind,' ', 2),' ' ,-1) as city
				from
    				service_area_information
    			where
    				flag = 'Y'
    			AND
    				substring_index(substring_index(area_kind,' ', 1),' ' ,-1) = 'spot'
				group by city 
				order by city desc";

		$result_array =  $this->db->fReadSql($sql, '');

		return $result_array;

	}
	//도시의 모든 구/군 정보 얻기
	function get_gu_by_city($city){
		$sql = "select 
						distinct gugun 
				from 
						area 
				where sido='".$city."'";
		$result_array =  $this->db->fReadSql($sql, '');
		return $result_array;	
	}
	//회사가 배달 가능한 지하철 정보 얻기
	function get_delivery_subway_by_city($city, $company_serial){


			$sql = "SELECT
								serial, location_name_kor, area_kind
					FROM
								service_area_information
					WHERE 
							flag = 'Y' 
					AND
						substring_index(substring_index(area_kind,' ', 2),' ' ,-1) ='".urldecode($city)."'
					AND
						left(area_kind, position(' ' in area_kind)) = 'subway'
					ORDER BY area_kind asc";

			$area_information_list =  $this->db->fReadSql($sql, '');

			//회사가 선택한 모든 area serial 가져오기



			$sql = "SELECT 
					    area.*, list.service_area_serial
					FROM
					    service_area_information as area, service_area_list as list
					WHERE
					     flag = 'Y'
					AND
					     substring_index(substring_index(area.area_kind,' ', 2),' ' ,-1) = '".urldecode($city)."'
					AND 
					     area.serial = list.service_area_serial
					AND
					     list.company_serial = ".$company_serial."";
					   
			$select_area_list = $this->db->fReadSql($sql, '');
			//전체 지하철역 중에서 company가 선택한 지하철역은 selected 추가
			for($i=0;$i<count($area_information_list);$i++){
					for($j=0; $j<count($select_area_list); $j++){
						if($select_area_list[$j]['service_area_serial'] == $area_information_list[$i]['serial']){
							$area_information_list[$i]['selected'] = true;
						}
					}
			}

		return $area_information_list;
	}
	//도시에 존재하는 spot들 가져오기(마스터 설정에서 사용)
	function get_place_by_city($city){

			//area_kind 중 지하철역이 아닌 것 가져오기
			$sql = "SELECT
						serial, location_name_kor, area_name
					FROM
						service_area_information
					WHERE 
						flag = 'Y' 
					AND
						left(area_kind, position(' ' in area_kind)) = 'spot'
					AND
						substring_index(substring_index(area_kind,' ', 2),' ' ,-1) ='".urldecode($city)."'
					ORDER BY area_kind asc";

			$area_information_list =  $this->db->fReadSql($sql, '');

			if($area_information_list == null || count($area_information_list) == 0){
				return array();
			}else{
				return $area_information_list;
			}
	}
	//회사가 설정한 spot들을 도시에 따라 가져오기 
	function get_selected_spot_by_city($city, $company_serial){

			//area_kind 중 지하철역이 아닌 것 가져오기
			$sql = "SELECT
						serial, location_name_kor, area_name
					FROM
						service_area_information
					WHERE 
						flag = 'Y' 
					AND
						left(area_kind, position(' ' in area_kind)) = 'spot'
					AND
						substring_index(substring_index(area_kind,' ', 2),' ' ,-1) ='".urldecode($city)."'
					ORDER BY area_kind asc";

			$area_information_list =  $this->db->fReadSql($sql, '');

			$sql = "SELECT 
					    area.*, list.service_area_serial
					FROM
					    service_area_information as area, service_area_list as list
					WHERE
					     area.flag = 'Y'
					AND
					     list.city_name = '".urldecode($city)."'
					AND 
					     area.serial = list.service_area_serial
					AND
					     list.company_serial = ".$company_serial."";
					   
			$select_area_list = $this->db->fReadSql($sql, '');
			//전체 지하철역 중에서 company가 선택한 지하철역은 selected 추가
			for($i=0;$i<count($area_information_list);$i++){
					for($j=0; $j<count($select_area_list); $j++){
						if($select_area_list[$j]['service_area_serial'] == $area_information_list[$i]['serial']){
							$area_information_list[$i]['selected'] = true;
						}
					}
			}

		return $area_information_list;
	}
	//
	function get_sub_area($city){
		$sql = "SELECT
					city_name, area_name
				FROM
					service_area_information
				WHERE
					flag = 'Y'
				AND 
					substring_index(substring_index(area_kind,' ', 2),' ' ,-1) ='".urldecode($city)."'
				AND
					area_name != ''
				group by area_name";
		$result_array = $this->db->fReadSql($sql, '');

		if($result_array == null || count($result_array) == 0){
				return array();
		}else{
				return $result_array;
		}
	}

	
	//회사의 subway 설정 저장
	function save_subway($company, $city, $array){
		//city 이름으로 등록된 지하철역 모두 지우고 새로 저장
		$result = $this->db->delete('service_area_list', array('city_name' => $city , 'is_subway' => 'Y', 'company_serial' => $company));
		if($result){
			$this->db->trans_start();
			for($i=0; $i<count($array); $i++){
				$this->db->query("INSERT INTO service_area_list (city_name, service_area_serial, company_serial, is_subway) VALUES ('".$city."', '".$array[$i]."' , '".$company."', 'Y')");
			}
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
			    $response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}else{
				$response['code'] ="S01";
			}

		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}

		return $response;
	}
	//회사의 spot 설정 저장
	function save_spot($company, $city, $array){
		//city 이름으로 등록된 지하철역 모두 지우고 새로 저장
		$result = $this->db->delete('service_area_list', array('city_name' => $city , 'is_subway' => 'N', 'company_serial' => $company));
		if($result){
			$this->db->trans_start();
			for($i=0; $i<count($array); $i++){
				$this->db->query("INSERT INTO service_area_list (city_name, service_area_serial, company_serial, is_subway) VALUES ('".$city."', '".$array[$i]."' , '".$company."', 'N')");
			}
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
			    $response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}else{
				$response['code'] ="S01";
			}

		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}

		return $response;
	}

	//spot 추가(마스터 전용)
	function save_etc_place($data){

		$sql = "SELECT 
					* 
				FROM 
					service_area_information 
				WHERE 
					flag = 'Y' 
				AND
					city_name ='".$data['city_name']."'
				AND 
					area_name = '".$data['area_name']."'
				AND
					location_name_kor = '".$data['location_name_kor']."'";

		$result = $this->db->fReadSql($sql, '');
		if(count($result)>0){
			$response['code'] ="E02";
			// $error = $this->db->error();
			$response['message'] = "이미 등록되어 있습니다.";
		}else{
			$result = $this->db->insert('service_area_information', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$error = $this->db->error();
				$response['code'] ="E01";
				$response['message'] =$error['message'];
			}
		}

		return $response;

	}
	//spot 삭제(마스터 전용)
	function delete_etc_place($array){
		$this->db->trans_start();
		for($i=0; $i<count($array); $i++){
			$this->db->query("UPDATE service_area_information SET flag='N' WHERE serial='".$array[$i]."'");
		}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			    $response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
		}else{
				$response['code'] ="S01";
		}
		return $response;
	}

}