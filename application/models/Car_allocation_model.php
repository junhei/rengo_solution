<?php
class Car_allocation_model extends CI_Model {
 

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

	}

	function get_car_schedule($start, $end, $company){
		$sql = "select 
				order_list.delivery_place, order_list.username, order_list.period_start, car_list.car_number
				from order_list, car_list 
				where (order_list.flag = 'F' or order_list.flag = 'P')
				and order_list.period_start >= '".$start."'  
				and order_list.period_start <= '".$end."' 
				and order_list.delivery_place != ''
				and order_list.car_serial = car_list.serial";

		//company!=0 추가하면 모든 회사볼수 있음
		if($company!=''){
			$sql = $sql." AND order_list.company_serial = ".$company;
		}

		$result = $this->db->fReadSql($sql, '');
		$delivery_count = 0;
		foreach($result as $data){
			//시작날짜를 모두 0분으로 설정
			$delivery_start_date = date('Ymd', strtotime($data['period_start']));
			$start_date = new DateTime($delivery_start_date);
			$start_date->setTime(intval(date('H', strtotime($data['period_start']))), 0);
			$start_str = $start_date->format('Y-m-d\TH:i:sO');
			$tok = explode('/', $data['delivery_place']);
			if($delivery_result[$start_str] != null){
				$delivery_count++;
				// $title = $send_result[$start_str]['title'];
				// $title = $title."<br/>&nbsp;<배차>".$data['car_number']."-".$tok[count($tok)-1];
			}else{
				$delivery_count = 1;
				// $title = "<배차>".$data['car_number']."-".$tok[count($tok)-1];
				// $title = "배차";
			}
			$delivery_result[$start_str] = array(
				'type' => 1,
				'start' => date("Y-m-d\TH:i:sO", strtotime($start_str)) ,
				// 'StartTimezone' => "Asia/Seoul",
				'end' => date("Y-m-d\TH:i:sO", strtotime($start_str.'+58 minutes') ),
				// 'EndTimezone' => "Asia/Seoul",
				'title' => "배차 ".$delivery_count." 건"
				// 'description' => "(배)".$data['username']."(".$data['tel'].")"
			);

		}
		// var_dump($send_result);

		$sql = "select 
				order_list.pickup_place, order_list.username, order_list.period_finish, car_list.car_number
				from order_list, car_list 
				where (order_list.flag = 'F' or order_list.flag = 'P')
				and order_list.period_finish >= '".$start."'  
				and order_list.period_finish <= '".$end."' 
				and order_list.pickup_place != ''
				and order_list.car_serial = car_list.serial";
		//company!=0 추가하면 모든 회사볼수 있음
		if($company!=''){
			$sql = $sql." AND order_list.company_serial = ".$company; 
		}
		$result = $this->db->fReadSql($sql, '');
		$pickup_count = 0;
		foreach($result as $data){
			//시작날짜를 모두 0분으로 설정
			$delivery_start_date = date('Ymd', strtotime($data['period_finish']));
			$start_date = new DateTime($delivery_start_date);
			$start_date->setTime(intval(date('H', strtotime($data['period_finish']))), 0);
			$start_str = $start_date->format('Y-m-d\TH:i:sO');
			$tok = explode('/', $data['pickup_place']);
			

			if($pickup_result[$start_str] != null){
				$pickup_count++;
				// $title = $send_result[$start_str]['title'];
				// $title = $title."<br/>&nbsp;<회차>".$data['car_number']."-".$tok[count($tok)-1];
			}
			else{
				$pickup_count = 1;
				// $title = "<회차>".$data['car_number']."-".$tok[count($tok)-1];
			}
			$pickup_result[$start_str] = array(
				'type' => 2,
				'start' => date("Y-m-d\TH:i:sO", strtotime($start_str)) ,
				// 'StartTimezone' => "Asia/Seoul",
				'end' => date("Y-m-d\TH:i:sO", strtotime($start_str.'+59 minutes') ),
				// 'EndTimezone' => "Asia/Seoul",
				'title' => "회차 ".$pickup_count." 건"
				// 'description' => $data['username']."(".$data['tel'].")"
			);
		}



		if($delivery_result==null){
			
		}else{
			foreach($delivery_result as $data){
				$result_array[] = array(
				'type' => $data['type'],
				'start' => $data['start'],
				'end' => $data['end'],
				'title' => $data['title']
				);

			}
		}

		if($pickup_result==null){
			$result_array = array();
		}else{
			foreach($pickup_result as $data){
				$result_array[] = array(
				'type' => $data['type'],
				'start' => $data['start'],
				'end' => $data['end'],
				'title' => $data['title']
				);

			}
		}

		return $result_array;

	}

	function get_count_delivery_pickup($start, $end, $company){
		//대여 구하기
		$sql = "select 
				*
				from order_list, car_list 
				where (order_list.flag = 'F' or order_list.flag = 'P')
				and order_list.period_start >= '".$start."'  
				and order_list.period_start <= '".$end."' 
				and order_list.delivery_place != ''
				and order_list.car_serial = car_list.serial";

		//company!=0 추가하면 모든 회사볼수 있음
		if($company!=''){
			$sql = $sql." AND order_list.company_serial = ".$company;
		}

		$result = $this->db->fReadSql($sql);
		$delivery_count = count($result);

		$sql = "select 
				*
				from order_list, car_list 
				where (order_list.flag = 'F' or order_list.flag = 'P')
				and order_list.period_finish >= '".$start."'  
				and order_list.period_finish <= '".$end."' 
				and order_list.pickup_place != ''
				and order_list.car_serial = car_list.serial";
		//company!=0 추가하면 모든 회사볼수 있음
		if($company!=''){
			$sql = $sql." AND order_list.company_serial = ".$company; 
		}
		$result = $this->db->fReadSql($sql);
		$pickup_count = count($result);
		$response = array(
			'delivery' => $delivery_count,
			'pickup' => $pickup_count
		);

		return $response;
	}


	function get_car_schedule_detail($start, $end, $company){

		$sql = " 
				select 
				order_list.serial, car_list.car_number, order_list.delivery_place, order_list.username, order_list.tel, order_list.period_start
				from order_list , car_list
				where (order_list.flag = 'F' or order_list.flag = 'P')
				and order_list.period_start >= '".$start."'  
				and order_list.period_start <= '".$end."' 
				and order_list.delivery_place != '' 
        		and order_list.car_serial = car_list.serial";

        if($company!='' && $company!=0){
			$sql = $sql." AND order_list.company_serial = ".$company;
		}

		$result = $this->db->fReadSql($sql, '');
		foreach($result as $data){
			$send_result[] = array(
				'type' => '배차',
				'serial' => $data['serial'],
 				'car_number' => $data['car_number'],
				'place' => $data['delivery_place'],
				'name' => $data['username'],
				'phone' => $data['tel'],
				'date' => date('Y-m-d H:i', strtotime($data['period_start']))
				);
		}

		$sql = " 
				select 
				order_list.serial, car_list.car_number, order_list.pickup_place, order_list.username, order_list.tel, order_list.period_finish
				from order_list , car_list
				where (order_list.flag = 'F' or order_list.flag = 'P')
				and order_list.period_finish >= '".$start."'  
				and order_list.period_finish <= '".$end."' 
				and order_list.pickup_place != ''
        		and order_list.car_serial = car_list.serial";

        if($company!='' && $company!=0){
			$sql = $sql." AND order_list.company_serial = ".$company;
		}

		$result = $this->db->fReadSql($sql, '');
		foreach($result as $data){
			$send_result[] = array(
				'type' => '회차',
				'serial' => $data['serial'],
				'car_number' => $data['car_number'],
				'place' => $data['pickup_place'],
				'name' => $data['username'],
				'phone' => $data['tel'],
				'date' => date('Y-m-d H:i', strtotime($data['period_finish']))
				);
		}



		if($send_result==null){
			return array();
		}else{
			$this->_sksort($send_result, 'date', true);
			return $send_result;
		}

	}
	//array 안의 array의 특정 key로 sort
	function _sksort(&$array, $subkey="id", $sort_ascending=false) {

	    if (count($array))
	        $temp_array[key($array)] = array_shift($array);

	    foreach($array as $key => $val){
	        $offset = 0;
	        $found = false;
	        foreach($temp_array as $tmp_key => $tmp_val)
	        {
	            if(!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey]))
	            {
	                $temp_array = array_merge(    (array)array_slice($temp_array,0,$offset),
	                                            array($key => $val),
	                                            array_slice($temp_array,$offset)
	                                          );
	                $found = true;
	            }
	            $offset++;
	        }
	        if(!$found) $temp_array = array_merge($temp_array, array($key => $val));
	    }

	    if ($sort_ascending) $array = array_reverse($temp_array);

	    else $array = $temp_array;
	}


	// function get_car_information($company_serial, $car_serial){
	// 	if($company_serial >= '1' && $car_serial >= '1'){
	// 		$sql = "select 
	// 				*
	// 				from car_list 
	// 				where
	// 				company_serial = '".$company_serial."'  
	// 				and serial <= '".$car_serial."' 
	// 		";

	// 		$result = $this->db->fReadSql($sql, '');
	// 		return $result;
	// 	} else {
	// 		return false;
	// 	}

	// }

}