<?php
class Api_base_model extends CI_Model {
 
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}
	
	function get_data_from_url($option)
	{
		$_url = $option['url'];
		$_method_type = $option['method_type']?$option['method_type']:"GET";
		$_function = $option['function']?$option['function']:"";
		$_data_format = (ISSET($option['data_format'])&&$option['data_format'])?$option['data_format']:'json';
		$_data = (ISSET($option['data'])&&$option['data'])?$option['data']:'';

		if($_url == ''){
			return "please confirm url address";
		} else if($_function == ''){
			return "please confirm data type";
		}

		$_url = $_url."/".$_function;

		if($_method_type == "GET" && $_data['keyword_name'] && $_data['keyword_value']){
			$_url = $_url."/".$_data['keyword_name']."/".urlencode($_data['keyword_value']);
		}

		$ch = curl_init();


		curl_setopt($ch, CURLOPT_URL, $_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); 
//		curl_setopt($ch, CURLOPT_USERAGENT, getenv(HTTP_USER_AGENT));
		
	//	curl_setopt($ch, CURLOPT_COOKIESESSION, 1 ); 
	//	curl_setopt($ch, CURLOPT_COOKIE, $strCookie ); 


		
		if($_method_type == "POST"){
			$_temp = '';
			if(is_array($_data)){
				while (list($_key, $_value) = each($_data)){
					$_temp .= "&".$_key."=".$_value;
				}
			}
		}

		$partner_id = $this->session->userdata('partner_id');
		$user_id = $this->session->userdata('user_id');

		if($partner_id >= '1'){
			$_temp .= "&partner_id=".$partner_id;
		}
		if($user_id >= '1'){
			$_temp .= "&user_id=".$user_id;
		}

		if($_temp)$_temp = substr($_temp, 1);


		curl_setopt($ch, CURLOPT_POST, count($_temp));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $_temp);   
		
		
//		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
//		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);

/*
		$saYear = $_GET['saYear']?$_GET['saYear']:2013;
		$saSer = $_GET['saSer'];

	//	$postData = "_NAVI_CMD=&_NAVI_SRNID=&_SRCH_SRNID=PNO102014&_CUR_CMD=InitMulSrch.laf&_CUR_SRNID=PNO102014&_NEXT_CMD=InitMulSrch.laf&_NEXT_SRNID=PNO102014&_PRE_SRNID=&_LOGOUT_CHK=&_FORM_YN=N";
	//	$postData = "_NAVI_CMD=&_NAVI_SRNID=&_SRCH_SRNID=PNO102014&_CUR_CMD=InitMulSrch.laf&_CUR_SRNID=PNO102014&_NEXT_CMD=RetrieveRealEstDetailInqSaList.laf&_NEXT_SRNID=PNO102014&_PRE_SRNID=&_LOGOUT_CHK=&_FORM_YN=Y&jiwonNm=�����߾��������&saYear=".$saYear."&saSer=".$saSer."&srnID=PNO102014";
	//$postData = "dongCode=1168010400&danjiCode=ALL&srhYear=2014&srhPeriod=2";

		curl_setopt($ch, CURLOPT_POST, count($postData));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);   
	*/

		$curl_result = curl_exec($ch);
		curl_close($ch);
		
		if($_data_format == "json"){
			$_temp1 = json_decode($curl_result);
			$_temp2 = json_encode($_temp1);
			$_result = $_temp2;
		} else {
			$_result = $curl_result;
		}
		return $_result;
	}

  	function _set_data_to_url($data)
	{
		$this->data = $data;
	}

	function _get_excel_file($file)
	{
		require_once '/home/apache/PHPExcel/Classes/PHPExcel.php';
		$objPHPExcel = new PHPExcel();
		$file = '/home/apache/uploads/차종관리20160713.xls';

		$objReader = PHPExcel_IOFactory::createReaderForFile($file);
		$objPHPExcel = $objReader->load($file);

		return $objPHPExcel;
	}
}
?>
