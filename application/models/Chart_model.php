<?php
class Chart_model extends CI_Model {


 
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

	}

	function get_daily_data($start, $end, $company_serial){

	$week = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
	$week_korea = array("월요일", "화요일", "수요일", "목요일", "금요일", "토요일", "일요일");

		$sql = "SELECT
				date_format(registered_date, '%W') category,
				SUM(total_price) total_price,
				COUNT(*) value
				FROM
				order_list
				WHERE
				(flag = 'F' or flag = 'Y' or flag = 'P')
				and date_format(registered_date, '%Y%m%d') >= '".$start."' and date_format(registered_date, '%Y%m%d') <= '".$end."'";

		if($company_serial!='' && $company_serial!=0){
			$sql = $sql." and company_serial=".$company_serial;


		}
		$sql = $sql." group by category;";


		$result = $this->db->fReadSql($sql, '');

		$colArray[] = array(
			'id' => "",
			'label' => "day",
			'pattern' => "",
			'type' => "string"
		);


		$colArray[] = array(
			'id' => "",
			'label' => "예약건수",
			'pattern' => "",
			'type' => "number"
		);

		$day1Count = 0;
		$day2Count = 0;
		$day3Count = 0;
		$day4Count = 0;
		$day5Count = 0;
		$day6Count = 0;
		$day7Count = 0;

		for($i=0; $i<count($result); $i++){
				if($result[$i]['category'] == "Monday"){
					$day1Count = $result[$i]['value'];
				}else if($result[$i]['category'] == "Tuesday"){
					$day2Count = $result[$i]['value'];
				}
				else if($result[$i]['category'] == "Wednesday"){
					$day3Count = $result[$i]['value'];
				}
				else if($result[$i]['category'] == "Thursday"){
					$day4Count = $result[$i]['value'];
				}
				else if($result[$i]['category'] == "Friday"){
					$day5Count = $result[$i]['value'];
				}
				else if($result[$i]['category'] == "Saturday"){
					$day6Count = $result[$i]['value'];
				}
				else if($result[$i]['category'] == "Sunday"){
					$day7Count = $result[$i]['value'];
				}
		}

		$vArray[] = array(
			'v' => "월요일",
		);

		$vArray[] = array(
			'v' => $day1Count,
		);

		$rowArray[] = array(
			'c' => $vArray
		);

		$vArray2[] = array(
			'v' => "화요일",
		);

		$vArray2[] = array(
			'v' => $day2Count,
		);

		$rowArray[] = array(
			'c' => $vArray2
		);

		$vArray3[] = array(
			'v' => "수요일",
		);

		$vArray3[] = array(
			'v' => $day3Count,
		);

		$rowArray[] = array(
			'c' => $vArray3
		);

		$vArray4[] = array(
			'v' => "목요일",
		);

		$vArray4[] = array(
			'v' => $day4Count,
		);

		$rowArray[] = array(
			'c' => $vArray4
		);

		$vArray5[] = array(
			'v' => "금요일",
		);

		$vArray5[] = array(
			'v' => $day5Count,
		);

		$rowArray[] = array(
			'c' => $vArray5
		);

		$vArray6[] = array(
			'v' => "토요일",
		);

		$vArray6[] = array(
			'v' => $day6Count,
		);

		$rowArray[] = array(
			'c' => $vArray6
		);

		$vArray7[] = array(
			'v' => "일요일",
		);

		$vArray7[] = array(
			'v' => $day7Count,
		);

		$rowArray[] = array(
			'c' => $vArray7
		);

		$send_result = array(
			'cols' => $colArray,
			'rows' => $rowArray
		);


		return $send_result;

	}

	function get_hourly_data($start, $end, $company_serial){


		$sql = "SELECT
				date_format(registered_date, '%H') category,
				SUM(total_price) total_price,
				COUNT(*) value
				FROM
				order_list
				WHERE
				(flag = 'F' or flag = 'Y' or flag = 'P')
				and date_format(registered_date, '%Y%m%d') >= '".$start."' and date_format(registered_date, '%Y%m%d') <= '".$end."'";

		if($company_serial!='' && $company_serial!=0){
			$sql = $sql." and company_serial=".$company_serial;
		}

		$sql = $sql." group by category;";

		$result = $this->db->fReadSql($sql, '');


		$colArray[] = array(
			'id' => "",
			'label' => "day",
			'pattern' => "",
			'type' => "string"
		);


		$colArray[] = array(
			'id' => "",
			'label' => "예약건수",
			'pattern' => "",
			'type' => "number"
		);
		
		for($i=1; $i<=24; $i++){
			unset($vArray);
			$vArray[] = array(
				'v' => $i."시",
			);
			$count = 0;
			for($j=0; $j<count($result); $j++){
				$index = intval($result[$j]['category']);
				if($i == $index){
					$count = intval($result[$j]['value']);
				}
			}

			$vArray[] = array(
				'v' => $count,
			);

			$rowArray[] = array(
				'c' => $vArray
			);
			
		}
		


		$send_result = array(
			'cols' => $colArray,
			'rows' => $rowArray
		);


		return $send_result;		

	}

	function get_cartype_data($start, $end, $company_serial){


		$sql = "SELECT
				c.car_type as category,
				SUM(a.total_price) total_price,
				COUNT(*) value
				FROM
				order_list as a, car_list as b, car_master as c
				WHERE
				( a.flag = 'F' or a.flag = 'P' )
				and left(a.period_start, 8) >= '".$start."' and left(a.period_start, 8) <= '".$end."' and a.car_serial = b.serial 
		        and b.car_index = c.car_index";

		if($company_serial!='' && $company_serial!=0){
			$sql = $sql." and a.company_serial=".$company_serial;

		}

		$sql = $sql." group by category;";
		$result = $this->db->fReadSql($sql, '');

		$colArray[] = array(
			'id' => "",
			'label' => "cartype",
			'pattern' => "",
			'type' => "string"
		);

		$colArray[] = array(
			'id' => "",
			'label' => "price",
			'pattern' => "",
			'type' => "number"
		);

		foreach($result as $data){
				$vArray = null;
				$vArray[] = array(
					'v' => $data['category'],
				);

				$vArray[] = array(
					'v' => intval($data['value']),
				);

 			
			$rowArray[] = array(
				'c' => $vArray
			);
		};

		$send_result = array(
			'cols' => $colArray,
			'rows' => $rowArray
		);

		
		return $send_result;		

	}



function get_place_data($start, $end, $company_serial){

		$sql = "SELECT
				delivery_place category,
				SUM(total_price) total_price,
				COUNT(*) value
				FROM
				order_list
				WHERE
				(flag = 'F' or flag = 'P')
				and
				delivery_place != ''
				and left(period_start, 8) >= '".$start."' and left(period_start, 8) <= '".$end."'";

		if($company_serial!='' && $company_serial!=0){
			$sql = $sql." and company_serial=".$company_serial;

		}

		$sql = $sql." group by category order by value desc limit 10;";

		$result = $this->db->fReadSql($sql, '');
		//지역에 '/' 가 없는경우 붙인다.(이전 DB와 호환을 위해)
		foreach($result as $data){
			if($data['category']!='' || $data['category']!=null){
				$tokenArray = explode('/', $data['category']);
				if(count($tokenArray)==1 || count($tokenArray)==0){
					$data['category'] = "부산/".$data['category'];
				}
			}
		}

		$colArray[] = array(
			'id' => "",
			'label' => "place",
			'pattern' => "",
			'type' => "string"
		);

		$colArray[] = array(
			'id' => "",
			'label' => "count",
			'pattern' => "",
			'type' => "number"
		);

		foreach($result as $data){
				$vArray = null;
				$vArray[] = array(
					'v' => $data['category'],
				);

				$vArray[] = array(
					'v' => intval($data['value']),
				);

 			
			$rowArray[] = array(
				'c' => $vArray
			);
		};

		$send_result = array(
			'cols' => $colArray,
			'rows' => $rowArray
		);

		return $send_result;		

	}

	function get_sex_data($start, $end, $company_serial){


		$sql = "SELECT
				gender,
				total_price
				FROM
				order_list
				WHERE
				(flag = 'F' or flag = 'P')
				and
				gender != ''
				and left(period_start, 8) >= '".$start."' and left(period_start, 8) <= '".$end."'";

		if($company_serial!='' && $company_serial!=0){
			$sql = $sql." and company_serial=".$company_serial;
		}
		$result = $this->db->fReadSql($sql, '');
		$girlCount = 0;
		$manCount = 0;


		foreach($result as $data){
				$sex = intval($data['gender'])%2; //2로 나누어서 홀수이면 남자, 짝수이면 여자
				if($sex == 0){
					$girlCount++;
				}else{
					$manCount++;
				}

		}




		$colArray[] = array(
			'id' => "",
			'label' => "sex",
			'pattern' => "",
			'type' => "string"
		);

		$colArray[] = array(
			'id' => "",
			'label' => "count",
			'pattern' => "",
			'type' => "number"
		);

		$vArrayMan[] = array(
			'v' => '남성'
		);

		$vArrayMan[] = array(
			'v' => $manCount
		);
		$rowArray[] = array(
			'c' => $vArrayMan
		);

		$vArrayWoMan[] = array(
			'v' => '여성'
		);

		$vArrayWoMan[] = array(
			'v' => $girlCount
		);
		$rowArray[] = array(
			'c' => $vArrayWoMan
		);

		$send_result = array(
			'cols' => $colArray,
			'rows' => $rowArray
		);

		return $send_result;

	}

	function get_age_data($start, $end, $company_serial){

		$sql = "SELECT
				birthday,
				total_price
				FROM
				order_list
				WHERE
				(flag = 'F' or flag = 'P')
				and left(period_start, 8) >= '".$start."' and left(period_start, 8) <= '".$end."'";

		if($company_serial!='' && $company_serial!=0){
			$sql = $sql." and company_serial=".$company_serial;

		}

		$result = $this->db->fReadSql($sql, '');


		$current_year = date("Y");



		$count20 = 0;
		$count30 = 0;
		$count40 = 0;
		$count50 = 0;
		$count60 = 0;

		foreach($result as $data){
			$birth_year = intval(substr($data['birthday'], 0, 4));
			if($birth_year==0){
				continue;
			}

			$age = $current_year - $birth_year + 1;

			if($age>=20 && $age<30){
				$count20++;
			}else if($age>=30 && $age<40){
				$count30++;
			}else if($age>=40 && $age<50){
				$count40++;
			}else if($age>=50 && $age<60){
				$count50++;
			}else{
				$count60++;
			}
		}

		$colArray[] = array(
			'id' => "",
			'label' => "age",
			'pattern' => "",
			'type' => "string"
		);

		$colArray[] = array(
			'id' => "",
			'label' => "count",
			'pattern' => "",
			'type' => "number"
		);

		$vArray20[] = array(
			'v' => '20대'
		);

		$vArray20[] = array(
			'v' => $count20
		);
		$rowArray[] = array(
			'c' => $vArray20
		);

		$vArray30[] = array(
			'v' => '30대'
		);

		$vArray30[] = array(
			'v' => $count30
		);
		$rowArray[] = array(
			'c' => $vArray30
		);

		$vArray40[] = array(
			'v' => '40대'
		);

		$vArray40[] = array(
			'v' => $count40
		);
		$rowArray[] = array(
			'c' => $vArray40
		);

		$vArray50[] = array(
			'v' => '50대'
		);

		$vArray50[] = array(
			'v' => $count50
		);
		$rowArray[] = array(
			'c' => $vArray50
		);

		$vArray60[] = array(
			'v' => '60대이상'
		);

		$vArray60[] = array(
			'v' => $count60
		);
		$rowArray[] = array(
			'c' => $vArray60
		);



		$send_result = array(
			'cols' => $colArray,
			'rows' => $rowArray
		);

		return $send_result;

	}

	function get_price_data($start, $end, $company_serial){

		$sql = "SELECT
				rental_price
				FROM
				order_list
				WHERE
				(flag = 'F' or flag = 'P')
		        -- and
		        -- rental_price > 0
				and left(period_start, 8) >= '".$start."' and left(period_start, 8) <= '".$end."'";




		if($company_serial!='' && $company_serial!=0){
			$sql = $sql." and company_serial=".$company_serial;

		}

		$sql = $sql." group by rental_price";

		$result = $this->db->fReadSql($sql, '');
		//지역에 '/' 가 없는경우 붙인다.(이전 DB와 호환을 위해)
		$price5count = 0;
		$price20count = 0;
		$price30count = 0;
		$price40count = 0;
		$price50count = 0;

		foreach($result as $data){
			if(intval($data['rental_price'])<50000){
				$price5count++;
			}else if(intval($data['rental_price'])<200000){
				$price20count++;
			}else if(intval($data['rental_price'])<300000){
				$price30count++;
			}else if(intval($data['rental_price'])<400000){
				$price40count++;
			}else{
				$price50count++;
			}
		}

		$colArray[] = array(
			'id' => "",
			'label' => "price",
			'pattern' => "",
			'type' => "string"
		);

		$colArray[] = array(
			'id' => "",
			'label' => "count",
			'pattern' => "",
			'type' => "number"
		);
		//5만원이하
		$vArray5[] = array(
			'v' => '5만원미만'
		);

		$vArray5[] = array(
			'v' => $price5count
		);
		$rowArray[] = array(
			'c' => $vArray5
		);
		//10만원이하
		$vArray20[] = array(
			'v' => '5만원~20만원미만'
		);

		$vArray20[] = array(
			'v' => $price20count
		);
		$rowArray[] = array(
			'c' => $vArray20
		);
		//15만원이하
		$vArray30[] = array(
			'v' => '20만원~30만원미만'
		);

		$vArray30[] = array(
			'v' => $price30count
		);
		$rowArray[] = array(
			'c' => $vArray30
		);
		//20만원이하
		$vArray40[] = array(
			'v' => '30만원~40만원미만'
		);

		$vArray40[] = array(
			'v' => $price40count
		);
		$rowArray[] = array(
			'c' => $vArray40
		);

		//25만원이하
		$vArray50[] = array(
			'v' => '40만원이상'
		);

		$vArray50[] = array(
			'v' => $price50count
		);
		$rowArray[] = array(
			'c' => $vArray50
		);



		$send_result = array(
			'cols' => $colArray,
			'rows' => $rowArray
		);

		return $send_result;		

	}

	function get_period_data($start, $end, $company_serial){

		$sql = "SELECT
				(period_finish - period_start) AS period
				FROM
				order_list
				WHERE
				(flag = 'F' or flag = 'P')
				and left(period_start, 8) >= '".$start."' and left(period_start, 8) <= '".$end."'";


		if($company_serial!='' && $company_serial!=0){
			$sql = $sql." and company_serial=".$company_serial;

		}

		$sql = $sql." group by (period_finish - period_start)";

		$result = $this->db->fReadSql($sql, '');
		$period_array = array();
		//period가 10000이면 1일(24시간)
		foreach($result as $data){
			$days = ((intval($data['period']) /10000) +1)+'';
			if($period_array[$days] == null){
				$period_array[$days] = array(
					'category' => ($days-1)."일~".$days."일 미만",
					'count' => 1);
			}else{
				$period_array[$days]['count']++;
			}
		}


		$colArray[] = array(
			'id' => "",
			'label' => "period",
			'pattern' => "",
			'type' => "string"
		);

		$colArray[] = array(
			'id' => "",
			'label' => "count",
			'pattern' => "",
			'type' => "number"
		);

		foreach($period_array as $data){
				$vArray = null;
				$vArray[] = array(
					'v' => $data['category'],
				);

				$vArray[] = array(
					'v' => intval($data['count']),
				);

 			
			$rowArray[] = array(
				'c' => $vArray
			);
		};
	

		

		$send_result = array(
			'cols' => $colArray,
			'rows' => $rowArray
		);

		return $send_result;		

	}


	function get_expense_data($start, $end, $company_serial){


		$sql = "SELECT
				date_format(deposite_date, '%Y%m') category,
				SUM(deposite_amount) total_price
				FROM
				receipt_history
				WHERE
				flag = 'Y'
				AND
				date_format(deposite_date, '%Y%m%d') >= '".$start."' and date_format(deposite_date, '%Y%m%d') <= '".$end."'";

		if($company_serial!='' && $company_serial!=0){
			$sql = $sql." and company_serial=".$company_serial;
		}

		$sql = $sql." group by category;";

		$total_result = $this->db->fReadSql($sql, '');

		$sql = "SELECT
				date_format(deposite_date, '%Y%m') category,
				SUM(deposite_amount) total_price
				FROM
				receipt_history
				WHERE
				flag = 'Y'
				AND
				handler = '렌고예약'
				AND
				date_format(deposite_date, '%Y%m%d') >= '".$start."' and date_format(deposite_date, '%Y%m%d') <= '".$end."'";

		if($company_serial!='' && $company_serial!=0){
			$sql = $sql." and company_serial=".$company_serial;
		}

		$sql = $sql." group by category;";

		$rengo_result = $this->db->fReadSql($sql, '');




		$colArray[] = array(
			'id' => "",
			'label' => "day",
			'pattern' => "",
			'type' => "string"
		);


		$colArray[] = array(
			'id' => "",
			'label' => "전체매출",
			'pattern' => "",
			'type' => "number"
		);

		$colArray[] = array(
			'id' => "",
			'label' => "렌고매출",
			'pattern' => "",
			'type' => "number"
		);
		
		for($i=0; $i<count($total_result); $i++){
			unset($vArray);
			$vArray[] = array(
				'v' => substr($total_result[$i]['category'], 0,4)."년 ".substr($total_result[$i]['category'], 4,2)."월"
			);
			
			$vArray[] = array(
				'v' => $total_result[$i]['total_price']
			);
			for($j=0; $j<count($rengo_result); $j++){
					//렌고 매출월이 있으면
				if($total_result[$i]['category'] == $rengo_result[$j]['category']){
					$vArray[] = array(
						'v' => $rengo_result[$i]['total_price']
					);
				}
			}

			$rowArray[] = array(
				'c' => $vArray
			);
			
		}
		


		$send_result = array(
			'cols' => $colArray,
			'rows' => $rowArray
		);


		return $send_result;		
	}

	function get_car_time_data($start, $end, $company_serial){


		$sql = "SELECT
				b.car_number as category, SUM(a.period_finish - a.period_start) as days
				FROM
				order_list as a, car_list as b
				WHERE
				( a.flag = 'F' or a.flag = 'P' )
				and left(a.period_start, 8) >= '".$start."' and left(a.period_start, 8) <= '".$end."'
        		and a.car_serial = b.serial";

		if($company_serial!='' && $company_serial!=0){
			$sql = $sql." and a.company_serial=".$company_serial;
		}

		$sql = $sql." group by category;";

		$total_result = $this->db->fReadSql($sql, '');

		
		$colArray[] = array(
			'id' => "",
			'label' => "차량번호",
			'pattern' => "",
			'type' => "string"
		);


		$colArray[] = array(
			'id' => "",
			'label' => "운행시간",
			'pattern' => "",
			'type' => "number"
		);
		
		for($i=0; $i<count($total_result); $i++){
			unset($vArray);
			$vArray[] = array(
				'v' => $total_result[$i]['category']
			);

			$days = 0;
			$hours = 0;
			//4자리 이상이면 5자리부터 몇일인지 알려줌
			if(strlen($total_result[$i]['days']) >4){
				$days = intval(substr($total_result[$i]['days'], 0, -4));
			}

			$hours = intval(substr(substr($total_result[$i]['days'],-4), 0, 2)) / 24;
			
			$days+= $hours;
			
			$vArray[] = array(
				'v' => $days
			);
			
			$rowArray[] = array(
				'c' => $vArray
			);
			
		}
		


		$send_result = array(
			'cols' => $colArray,
			'rows' => $rowArray
		);


		return $send_result;		
	}


}