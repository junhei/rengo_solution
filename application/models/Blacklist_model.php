<?php
class Blacklist_model extends CI_Model {
 

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

	}

	//전체 블랙리스트 얻기
	function get_list()
	{
		$sql = "SELECT * FROM bbs_blacklist";
		$result = $this->db->fReadSql($sql, '');
		return $result;
	}

	//블랙 리스트 추가
	function add($data){
		$sql = "SELECT * FROM bbs_blacklist WHERE name='".$data['name']."' AND birthday='".$data['birthday']."'";


		$query = $this->db->query($sql);
		$result_count = $query->num_rows();
		// return $result_count;
		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록 된 유저 입니다.";		
		}else{
			$result = $this->db->insert('bbs_blacklist', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;

		
	}
	//블랙 리스트 업데이트
	function update($data){

		$this->db->where('serial', $data['serial']);
		$result = $this->db->update('bbs_blacklist', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}
	//블랙 리스트 삭제
	function delete($id){

		$result = $this->db->delete('bbs_blacklist', array('serial' => $id));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	function count_black() {
		$sql="
		SELECT 
			COUNT(serial) as blacklist
		FROM 
			bbs_blacklist
		";
		$result=$this->db->fReadSql($sql);
		return $result;
	}


	//전체 블랙리스트 얻기
	function m_get_list()
	{
		$sql = "SELECT serial, name, tel, birthday, used_date, kind, writer, title, license, company_serial, company_tel, used_area, company_name FROM bbs_blacklist";
		$result = $this->db->fReadSql($sql);
		return $result;
	}

}