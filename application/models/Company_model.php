<?php
class Company_model extends CI_Model {
 

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

	}

	function m_check_login($email, $password){
		$sql = "SELECT 
				A.company_serial, C.company_name, A.admin_id, A.admin_name, A.admin_flag, A.permission 
				FROM 
				admin_information AS A, company_information AS C 
				WHERE 
				A.admin_id='".$email."' AND A.admin_pw = PASSWORD('".$password."') AND A.company_serial = C.serial";
		$result = $this->db->fReadSql($sql);

		if(count($result)>0){
			if($result[0]['admin_flag'] == 'Y'){
				$response['code'] = 'S01';
				$response['value'] = $result[0];
			}else{
				$response['code'] = 'E03';
				$response['message'] =  '정상 사용중이 아닌 유저입니다. 관리자에게 문의하세요.';
			}
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	//전체 차종 얻기
	function get_company($company_serial){
		if($company_serial=='' || $company_serial ==null || $company_serial == 0){
			$sql = "SELECT * FROM company_information where flag = 'Y' and company_name != ''";
		}else{
			$sql = "SELECT * FROM company_information WHERE serial = '".$company_serial."' and company_name != ''";
		}
		
		$result = $this->db->fReadSql($sql, '');
		//슈퍼 관리자이면 serial 0번 전체 메뉴 추가
		// if($company_serial=='' || $company_serial ==null || $company_serial == 0){
		// 	array_unshift($result, array( 
		// 		'serial' => 0,
		// 		'company_name' => '전체'));
		// }
		return $result;
	}

	// function get_branch($company_serial, $option =''){

	// 	$sql = "SELECT * FROM branch_information WHERE company_serial = '".$company_serial."' AND flag = 'Y'";
	// 	$result = $this->db->fReadSql($sql, '');
	// 	if($result==null){
	// 		$result = array();
	// 	}
	// 	if($option == ''){
	// 			array_unshift($result, array( 
	// 			'serial' => 0,
	// 			'branch_name' => '본사'));
	// 	}
	// 	return $result;

	// }
	
	//password 얻기
	function password($company_serial) {
		$sql="
		SELECT
			password
		FROM
			company_information
		WHERE
			serial='".$company_serial."'
		";
		$result=$this->db->fReadSql($sql);
		return $result;
	}
	//company 등록
	function register_company($data)
	{
		$this->db->flush_cache();
		$this->db->set('registered_date', 'now()', FALSE);
		$this->db->set('password', "PASSWORD('".$data['password']."')", FALSE);
		$company_data = array(
				'email' => $data['email'],
				'flag' => 'W',
				'company_name' => $data['company_name'],
				'tel' => $data['tel'],
				'fax' => $data['fax'],
				'postcode' => $data['postcode'],
				'company_address' => $data['company_address'],
				'open_time_start' => $data['open_time_start'],
				'open_time_finish' => $data['open_time_finish'],
				'delivery_max_per_hour' => $data['delivery_max_per_hour'],
				'owner_name' =>$data['owner_name'],
				'phone' => $data['phone'],
				'corporate_license_number' => $data['corporate_license_number'],
				'register_number' => $data['register_number'],
				// 'phone_option1' => $data['phone']				
		);

		$result=$this->db->insert('company_information',$company_data);

		if($result){
			$response['code']='S01';
			$response['serial'] = $this->db->insert_id();
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	//company 등록
	function register_company_file($data)
	{
		$this->db->flush_cache();
		if($data['file_business_regist']){
			$this->db->set('business_regist_filename', $data['file_business_regist']);
		}
		if($data['file_bankbook']){
			$this->db->set('bankbook_filename', $data['file_bankbook']);
		}
		$this->db->where('serial', $data['serial']);
		//1. company 정보 먼저 update
		$this->db->update('company_information');
	}



	function modify_company($company_serial, $data){

		$company_data = array(
				'company_name' => $data['company_name'],
				'corporate_license_number' => $data['corporate_license_number'],
				'register_number' => $data['register_number'],
				'owner_name' =>$data['owner_name'],
				'phone' => $data['phone'],
				'phone_option1' => $data['phone'],
				'tel' => $data['tel'],
				'fax' => $data['fax'],
				'postcode' => $data['postcode'],
				'company_address' => $data['company_address']
		);

		if($data['flag']!= ''){
			$company_data['flag'] = $data['flag'];
		}
		if($data['open_time_start']!= ''){
			$company_data['open_time_start'] = $data['open_time_start'];
		}
		if($data['open_time_finish']!= ''){
			$company_data['open_time_finish'] = $data['open_time_finish'];
		}
		if($data['delivery_max_per_hour']!= ''){
			$company_data['delivery_max_per_hour'] = $data['delivery_max_per_hour'];
		}

		$this->db->trans_start();

		$this->db->flush_cache();
		if($data['password'] != ''){
			$this->db->set('password', "PASSWORD('".$data['password']."')", FALSE);
		}

		$this->db->set('edited_date', 'now()', FALSE);
		$this->db->where('serial', $company_serial);
		//1. company 정보 먼저 update
		$this->db->update('company_information',$company_data);

		$this->db->flush_cache();
		$admin_data = array(
				'admin_name' => $data['owner_name'],
				'phone_number' => $data['phone']
		);

		if($data['password'] != ''){
			$this->db->set('admin_pw', "PASSWORD('".$data['password']."')", FALSE);
		}
		$this->db->set('edited_date', 'now()', FALSE);
		$this->db->where('admin_id', $data['email']);
		$this->db->update('admin_information',$admin_data);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
		    $response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];

		}else{
			$response['code'] ="S01";
			$responee['company_serial'] = $company_serial;
		}

		return $response;
	}

	
	function get_start_end_time($company_serial){
		$sql = "SELECT
								open_time_start,
								open_time_finish
				FROM
								company_information
				WHERE 
								serial=".$company_serial;

		return $this->db->fReadSql($sql, '');
	}

	function save_time_and_delivery($company_serial, $start_time, $end_time, $max_count, $min_period, $max_period){

		$data = array(
			'open_time_start' => $start_time,
			'open_time_finish' => $end_time,
			'delivery_max_per_hour' => $max_count,
			'min_reservation_period' => $min_period,
			'max_reservation_period' => $max_period
		);
		$this->db->flush_cache();
		$this->db->set('edited_date', 'now()', FALSE);
		$this->db->where('serial', $company_serial);
		$result = $this->db->update('company_information', $data);
		$result_count = $this->db->affected_rows();
		if($result_count > 0){
				$response['code'] ="S01";
		}else{
				$error = $this->db->error();
				$response['code'] ="E01";
				$response['message'] =$error['message'];
		}
		
		return $response;
	}

	function get_setting($company_serial){
		$sql = "
		select
			max_reservation_period, min_reservation_period, delivery_max_per_hour, open_time_start, open_time_finish
		from
			company_information
	    where
      		serial='".$company_serial."'";

      	$result = $this->db->fReadSql($sql);
      	return $result;
	}

	function get_city(){
		$sql = "
		select
					city_name
		from
					service_area_information
	    group by 
      				city_name";

      	$result = $this->db->fReadSql($sql, '');
      	return $result;
	}

	function add_new_admin($company_serial){
		$sql = "SELECT * FROM company_information WHERE serial='".$company_serial."'";
      	$company_info = $this->db->fReadSql($sql, '');

      	if(count($company_info)==0){
			$message = '등록된 회사가 없습니다.';
      	}else{
      		$sql = "SELECT * FROM admin_information WHERE admin_id='".$company_info[0]['email']."'";
      		$result = $this->db->fReadSql($sql, '');
      		if(count($result)==0){
      			$this->db->trans_start();
      			$sql = "INSERT INTO admin_information(admin_id, admin_pw, admin_level, admin_name, permission, registered_date, company_serial, admin_flag, phone_number) VALUES ('".$company_info[0]['email']."' , '".$company_info[0]['password']."' , 2, '".$company_info[0]['owner_name']."', 'YYYYYN' , now(), ".$company_serial." , 'Y', '".$company_info[0]['phone']."')";
      			$this->db->query($sql);
      			$sql2 = "UPDATE company_information SET flag='Y' WHERE serial='".$company_serial."'";
				$this->db->query($sql2);
      			$this->db->trans_complete();
      			if ($this->db->trans_status() === FALSE)
				{
					$message = $error['message'];
				}else{
					$message = "최종 승인 되었습니다! 렌고 파트너스 사이트에서 로그인하세요.</br><a href='http://solution.rengo.co.kr'>바로 이동하기</a>";
				}
      			
      		}else{
      			$message = "이미 승인 처리 되었습니다.";
      		}
      	}

      	return $message;
	}


	function duplicate_test($value,$column) 
	{
		$sql="
		SELECT 
			email
		FROM
			company_information
		WHERE 
			".$column."='".$value."'
		";
		// echo $sql;
		// die();
		$result=$this->db->fReadSql($sql);
		return $result;
	}
	//시간당 배차회수 가져오기
	function get_delivery_max_per_hour($company_serial){
		$sql = "SELECT delivery_max_per_hour FROM company_information WHERE serial='".$company_serial."'";
		$result=$this->db->fReadSql($sql);
		return $result;
	}

	function get_image_file($company_serial, $type){

		if($type=='bank'){
			$sql = "SELECT bankbook_filename AS file ";
		}else if($type=='business'){
			$sql = "SELECT business_regist_filename AS file ";
		}
		$sql.="FROM company_information WHERE serial='".$company_serial."'";

		$result=$this->db->fReadSql($sql);
		return $result[0]['file'];
	}
	//시간당 배차회수 저장
	// function save_delivery_max_per_hour($company_serial, $max){
	// 	$sql = "UPDATE company_information SET delivery_max_per_hour='".$max."' WHERE serial='".$company_serial."'";
	// 	$result=$this->db->query($sql);
	// 	if($this->db->affected_rows()>0){
	// 		$response['code'] = "S01";
	// 	}else{
	// 		$response['code'] = "E01";
	// 		$response['message'] = "저장 실패";
	// 	}
	// 	return $response;
	// }

}