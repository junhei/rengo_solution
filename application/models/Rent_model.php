<?php
class Rent_model extends CI_Model {

	private $최대예약가능시작날짜 = "14";
	private $최대예약가능날짜 = "30";
 
	function __construct(){
		// Call the Model constructor
		parent::__construct();

	}

	function get_car_flag($car_serial ){
		if(intval($car_serial) >= '1')
		$sql="
		SELECT 
			flag 
		FROM
			car_list
		WHERE 
			serial = '".intval($car_serial)."'
		";
		$result = $this->db->fReadSql($sql);
		return $result['0']['flag'];
	}


	function get_company_user($company_serial){
		$sql="
		SELECT 
			* 
		FROM
			user_information
		WHERE 
			company_serial=".$company_serial;
		$result=$this->db->fReadSql($sql);
		if(count($result)==0){
			return array();
		}else{
			return $result;
		}
		
	}
	
	function get_search_user_information($search_string){
		$search_string=urldecode($search_string);
		$sql="
		SELECT 
			* 
		FROM
			user_information
		WHERE 
			(user_name LIKE '%".$search_string."%' 
			OR 
			birthday LIKE '%".$search_string."%'
			OR 
			phone_number1 LIKE '%".$search_string."%'
			OR 
			address LIKE '%".$search_string."%' 
			OR 
			company LIKE '%".$search_string."%')";
		$result=$this->db->fReadSql($sql);
		return $result;
	}

	function get_special_price($company_serial = ''){
		if(intval($company_serial) >= '1'){
			$sql_company_serial = "
			and company_serial = '".intval($company_serial)."'
			";
		}
		$sql="
		SELECT 
			* 
		FROM
			special_price
		WHERE 
			flag = 'Y'
			".$sql_company_serial."
		ORDER BY
			serial ASC
		";
		$result = $this->db->fReadSql($sql);
		return $result;
	}

	function get_schedule_html($data){
//		$company_serial = $this->session->userdata('company_serial');
		$company_serial = $data['company_serial'];
		$branch_serial = $data['branch_serial'];
		$search_keyword = $data['search_keyword'];
		$order_kind = $data['order_kind'];
		$scheduler_car_type = $data['scheduler_car_type'];
		$special_price = $this->rent->get_special_price($company_serial);


		$year = $data['year'];
		$month = $data['month'];
		$day = $data['day'];
		$today = date("Ymd");
		$now_hour = date("H");

		$year = intval($year)?intval($year):date("Y");
		$month = intval($month)?$month:date("m");
		$day = intval($day)?$day:date("d");

		$start_date = strtotime($year.$month.$day. " -3 days");
		$finish_date = strtotime($year.$month.$day. " +36 days");

		$week = array("일", "월", "화", "수", "목", "금", "토");

		if($data['schedule_mode'] == "date"){
			if($company_serial){
				$sql_company = "
				and company_serial = '".$company_serial."'
				";
			}
			$sql = "
			SELECT
				*
			FROM
				holiday_list
			WHERE
				1=1
				".$sql_company."
			";
			$result_holiday = $this->db->fReadSql($sql, '');
			$count_holiday = count($result_holiday);
			for($i=0;$i<$count_holiday;$i++){
				$holiday[$result_holiday[$i]['company_serial']][$result_holiday[$i]['holiday']] = "rengo_off";
			}

			$sql = "
			SELECT
				*
			FROM
				holiday_list_car
			WHERE
				1=1
				".$sql_company."
			";
			$result_holiday_car = $this->db->fReadSql($sql, '');
			$count_holiday_car = count($result_holiday_car);
			for($i=0;$i<$count_holiday_car;$i++){
				$holiday_car[$company_serial][$result_holiday_car[$i]['car_serial']][$result_holiday_car[$i]['holiday']] = "rengo_off";
			}

			for($i=0;$i<40;$i++){
				$temp_date = strtotime($year.$month.$day. " +".($i-3)." days");
				$요일 = $week[date('w',$temp_date)];
				$오늘 = "";
				$오늘_txt = "";
				if($오늘카운트 < '1' && $today <= date("Ymd", $temp_date)){
					$diff_hour = (strtotime(date("Ymd", $temp_date)."0000") - strtotime($today."0000")) / (60*60) ;
					$count_day = intval($diff_hour / 24);
					$this->최대예약가능시작날짜 = $this->최대예약가능시작날짜 - $count_day;
					if($count_day < '1'){
						$오늘 = " 오늘";
						$오늘_txt = "<span class=\"오늘_txt\">".date("d", $temp_date)."</span><span class=\"오늘_circle\"></span>";
					}
					$오늘카운트=1;
				}

				$day_class = '';

				$close_day = '';
				if($오늘카운트 < '1' || $오늘카운트 > $this->최대예약가능시작날짜){
//					$렌고판매일정 = " class = \"렌고판매일정\" ";
					$rengo_day_on = " rengo_off";
				} else if($오늘카운트 >= '1' && $오늘카운트 <= $this->최대예약가능시작날짜){
					$렌고판매일정 = '';
					$rengo_day_on = " rengo_on";
					$close_day = " onclick=\"javascript:close_day('".date("Ymd", $temp_date)."');\"";
				} 
				if($오늘카운트 >= '1')$오늘카운트++;


				if($holiday[$company_serial][date("Ymd", $temp_date)]){
					$rengo_day_on = " rengo_off";
				}

				if($요일 == "토"){
					$day_class = " class=\"토요일";
				}
				else if($요일 == "일"){
					$day_class = " class=\"일요일";
				}
				$day_number_class = $day_class?($day_class.$rengo_day_on.$오늘."\""):(" class=\"".$rengo_day_on.$오늘."\"");
				$day_number_class_cell = $day_class?($day_class.$rengo_day_on."\""):(" class=\"".$rengo_day_on."\"");
				
				if($day_class)$day_class .= $rengo_day_on."\"";
				else $day_class = " class=\"".$rengo_day_on."\"";


				$this_month_day_name .= "<li".$day_class.$close_day.">".$요일."</li>
";
				if($오늘_txt){
					$this_month_day_number .= "<li".$day_number_class.">".$오늘_txt."</li>
";
					$this_month_day_cell .= "<li".$day_number_class_cell.$렌고판매일정." onclick=\"javascript:close_day_car('".date("Ymd", $temp_date)."', '#car_serial');\"></li>
";
				} else {
					$this_month_day_number .= "<li".$day_number_class.">".date("d", $temp_date)."</li>
";
					$this_month_day_cell .= "<li".$day_number_class.$렌고판매일정." onclick=\"javascript:close_day_car('".date("Ymd", $temp_date)."', '#car_serial');\"></li>
";
				}
			}
		} else {
			$start_date = strtotime($year.$month.$day);
			$finish_date = strtotime($year.$month.$day. " +1 days");

			if($company_serial >= '1'){
				$sql = "
				SELECT
					open_time_start,
					open_time_finish
				FROM
					company_information
				WHERE
					serial = '".$company_serial."'
";
				$result_company = $this->db->fReadSql($sql, '');
				$open_time_start = substr($result_company['0']['open_time_start'], 0, 2);
				$open_time_finish = substr($result_company['0']['open_time_finish'], 0, 2);
			} else {
				$open_time_start = 1;
				$open_time_finish = 24;
			}
			/*
			24시간으로 고정
			*/
			$open_time_start = 0;
			$open_time_finish = 23;

			for($i=0;$i<4;$i++){
				$temp_date = strtotime($year.$month.$day. " +".($i)." days");
				$요일 = $week[date('w',$temp_date)];
				$date = date("Y-m-d", $temp_date)."(".$요일.")";


				if($요일 == "토"){
					$day_class = " class=\"토요일\"";
				}
				else if($요일 == "일"){
					$day_class = " class=\"일요일\"";
				}

				$this_month_day_name .= "<li class=\"day_time ".$day_class."\">".$date."</li>
";
				for($j=$open_time_start;$j<=$open_time_finish;$j++){
					$오늘 = "";
					if($today.$now_hour == date("Ymd", $temp_date).sprintf("%02.0f",$j)){
//						$오늘 = " 오늘";
					}

					$this_month_day_number .= "<li".$오늘.">".sprintf("%02.0f",$j)."</li>
";
					$this_month_day_cell .= "<li".$오늘."></li>
";
				}
			}
		}


		if($company_serial >= '1'){
			$sql_company_serial = "
a.company_serial = '".$company_serial."' and 
";
		}
		if('a'.$branch_serial >= 'a'.'0'){
			$sql_branch_serial = "
a.branch_serial = '".$branch_serial."' and 
";
		}
		if($scheduler_car_type){
			$sql_scheduler_car_type = "
b.car_type = '".$scheduler_car_type."' and 
";
		}

		$sql = "
SELECT
	a.*,
	b.car_name_detail
FROM
	car_list a,
	car_master b,
	maker_master c
WHERE
	".$sql_company_serial."
	".$sql_branch_serial."
	".$sql_scheduler_car_type."
	a.car_index = b.car_index 
	and c.maker_index = b.maker_index
ORDER BY
	b.car_name_detail ASC,
	a.serial ASC
";
		$result_car = $this->db->fReadSql($sql, '');


		$start_date = date("Ymd", $start_date);
		$finish_date = date("Ymd", $finish_date);

		if($start_date && $finish_date){
			$sql_period = "
and
(
d.period_start <= '".$finish_date."2400' and
d.period_finish >= '".$start_date."0000'
)
";
		}

		if($company_serial >= '1'){
			$sql_partner = "
and e.serial = '".$company_serial."'
";
		}

		if(strlen(trim($search_keyword)) >= '1'){
			$sql_keyword = "
and 
(
b.car_number like '%".$search_keyword."%' or
e.company_name like '%".$search_keyword."%' or
d.username like '%".$search_keyword."%' or
d.tel like '%".$search_keyword."%' or
d.birthday like '%".$search_keyword."%' or
d.order_number = '".$search_keyword."'
)
";
		}

		if($order_kind){
			$sql_order_kind = "
and 
d.order_kind = '".$order_kind."'
";
		}



		$sql = "
select
	b.car_number,
	e.company_name,
	d.registered_date,
	d.*,
	e.phone,
	b.serial,
	d.serial as order_number

from
	car_list b,
	order_list d,
	company_information e
where
	b.serial = d.car_serial and
	e.serial = b.company_serial
	".$sql_period."
	".$sql_partner."
	".$sql_order_kind."
	".$sql_keyword."
	and (d.flag = 'F' or d.flag = 'P')
order by b.serial asc, d.serial asc
";
//echo $sql."<BR>";
// F : 결제완료
// P : 파트너앱 입력
// Y : 예약중(결제전)

		$result_order = $this->db->fReadSql($sql, '');

		for($i=0;$i<count($result_order);$i++){
			$order_list[$result_order[$i]['serial']][] = $result_order[$i];
/*
	echo $result_order[$i]['car_number']."a : ".$result_order[$i]['flag']." : ".$result_order[$i]['total_price']." : ".$result_order[$i]['username']." : ".$result_order[$i]['period_start']."~".$result_order[$i]['period_finish']."<BR>";
	echo count($order_list[$result_order[$i]['serial']])."<BR>
";
*/		}

		$count_car_list = count($result_car);
		$count_registered_car_number = $count_car_list;
		$date_now = date("YmdHi");

		$original_this_month_day_cell = $this_month_day_cell;
		for($i=0,$k=0;$i<$count_car_list;$i++){
			if($result_car[$i]['flag'] == "Y"){
				$this_month_day_cell = $original_this_month_day_cell;
				$available_car_number++;
			} else {
				$this_month_day_cell = str_replace("rengo_on", "rengo_off", $original_this_month_day_cell);
			}
			if($sql_keyword){
				$break_car_flag = "break";
			}

			$count_order_list = count($order_list[$result_car[$i]['serial']]);
			for($j=0;$j<$count_order_list;$j++){

				$position_left = 0;
				$position_top = 49;
				$cell_width = 48;
				$cell_height = 49;
				$ol = $order_list[$result_car[$i]['serial']][$j];
				
				if($sql_keyword){
					$break_car_flag = "";
				}
				$original_period_start = $ol['period_start'];
				$original_period_finish = $ol['period_finish'];
				if(intval(substr($ol['period_start'], 0, 8)) < $start_date){
					$ol['period_start'] = substr($start_date, 0, 8)."0000";
				} else if(intval(substr($ol['period_finish'], 0, 8)) > $finish_date){
					$ol['period_finish'] = substr($finish_date, 0, 8)."2400";
				}

				$diff_hour = (strtotime($ol['period_start']) - strtotime($start_date."0000")) / (60*60) ;
				$count_day = intval($diff_hour / 24);
				$count_hour = $diff_hour - ($count_day * 24);
				if(intval($count_hour) != $count_hour)$count_hour = intval($count_hour) + 1;

				if($ol['status'] == "반납"){
					$bg_class = "bg_info";
				} else if($ol['status'] == "예약"){
					$bg_class = "bg_primary";
				}  else if($ol['status'] == "운행") {
					$bg_class = "bg_success";
				}

				if($ol['order_kind'] == "장기"){
					$bg_class = "bg_danger";
				} else if($ol['order_kind'] == "중기"){
					$bg_class = "bg_중기";
				}else if($ol['order_kind'] == "보험"){
					$bg_class = "bg_warning";
				}

				if($data['schedule_mode'] == "date"){
					$left = ($count_day) * $cell_width;

					if(intval(substr($ol['period_start'], 8, 2)) >= '1'){
						$left = $left + substr($ol['period_start'], 8, 2) * 2;
					}
					if(intval(substr($ol['period_start'], 10, 2)) == '30'){
						$left = $left + 1;
					}

					$diff_hour = (strtotime($ol['period_finish']) - strtotime($ol['period_start'])) / (60*60) ;
					$count_day = intval($diff_hour / 24);
					$count_hour = $diff_hour - ($count_day * 24);

					if(intval($count_hour) != $count_hour)$count_hour = intval($count_hour) + 1;

					if($count_day >= '1'){
							$width = ($count_day * $cell_width) + ($count_hour * 2);
					} else {
						$width = ($count_hour * 2);
					}

					$top = $position_top + ($cell_height * ($k-1));
					if($width < 140){
						$order_text = $ol['username']?$ol['username']:(intval(substr($original_period_start, 4, 2))."월".intval(substr($original_period_start, 6, 2))."일".substr($original_period_start, 8, 2)."시".substr($original_period_start, 10, 2)."분");
					} else if($width < 200){
						$order_text = $ol['username']." ".intval(substr($original_period_start, 4, 2))."월".intval(substr($original_period_start, 6, 2))."일".substr($original_period_start, 8, 2)."시".substr($original_period_start, 10, 2)."분";
					} else {
						$order_text = $ol['username']." ".$original_period_start." ~ ".$original_period_finish." ".$ol['tel'];
					}
					$order_list_detail .= "<a class=\"schedule_list bg_primary ".$bg_class."\" style=\"top:".($top)."px; left:".$left."px; width: ".$width."px; height:48px;\" onclick=\"javascript:order_detail('".$ol['order_number']."');\"><span style=\"max-width:".($width)."px;\">".$order_text."</span></a>
";

				} else {
					$left = ($diff_hour) * $cell_width;
/*
day_time6
day_time12
day_time18
day_time
*/
					$diff_hour = (strtotime($ol['period_finish']) - strtotime($ol['period_start'])) / (60*60) ;

					$width = ($diff_hour * $cell_width);

					$top = $position_top + ($cell_height * ($k-1));
					if($width < 140){
						$order_text = $ol['username']?$ol['username']:(intval(substr($original_period_start, 4, 2))."월".intval(substr($original_period_start, 6, 2))."일".substr($original_period_start, 8, 2)."시".substr($original_period_start, 10, 2)."분");
					} else if($width < 200){
						$order_text = $ol['username']." ".intval(substr($original_period_start, 4, 2))."월".intval(substr($original_period_start, 6, 2))."일".substr($original_period_start, 8, 2)."시".substr($original_period_start, 10, 2)."분";
					} else {
						$order_text = $ol['username']." ".$original_period_start." ~ ".$original_period_finish." ".$ol['tel'];
					}
					$order_list_detail .= "<a class=\"schedule_list bg_primary ".$bg_class."\" style=\"top:".$top."px; left:".$left."px; width: ".$width."px;height:48px;\"><span style=\"max-width:".($width)."px;\">".$order_text."</span></a>
";
				}
			}
			if($result_car[$i]['flag'] == "Y"){
				$count_rengo_car_number++;
				$car_name_list_checked = " checked";
				$rengo_checked = " rengo_on";
				$update_car_flag = 'N';
			} else {
				$car_name_list_checked = "";
				$rengo_checked = " rengo_off";
				$update_car_flag = 'Y';
			}

			if($break_car_flag == "break")continue;

			if($car_name_list == '')$bt0_car_name_list = " bt0";
			else $bt0_car_name_list = "";

			if($result_car[$i]['flag'] == "Y" || $result_car[$i]['flag'] == "N"){
				$temp_time = time();
				$car_name_list .= "
												<div class=\"schedule_car_data_list row".$bt0_car_name_list.$rengo_checked."\">
													<!--div class=\"col-md-2 text-center br_ccc pdl0 pdr0\">
														<div class=\"tit_item\">
															<input type=\"checkbox\"".$car_name_list_checked." id=\"car_serial_".$result_car[$i]['serial'].$temp_time."\" onchange=\"javascript:change_car_flag($(this).get(0).name);\" value=\"".$result_car[$i]['serial']."\" name=\"car_serial_".$result_car[$i]['serial'].$temp_time."\">
															<label for=\"car_serial_".$result_car[$i]['serial'].$temp_time."\">
															</label>
														</div>
													</div-->
													<div class=\"col-md-7 text-center br_ccc\" onclick=\"javascript:change_car_flag('".$result_car[$i]['serial']."', '".$update_car_flag."');\"><!--input type=\"checkbox\"".$car_name_list_checked." id=\"car_serial_".$result_car[$i]['serial'].$temp_time."\" onchange=\"javascript:change_car_flag($(this).get(0).name);\" value=\"".$result_car[$i]['serial']."\" name=\"car_serial_".$result_car[$i]['serial'].$temp_time."\">
															<label for=\"car_serial_".$result_car[$i]['serial'].$temp_time."\">".$result_car[$i]['car_name_detail']."</label-->".$result_car[$i]['car_name_detail']."</div>
													<div class=\"col-md-5 text-center\">".$result_car[$i]['car_number']."</div>
												</div>
";
			} else if($result_car[$i]['flag'] == "D"){
				$car_name_list .= "
												<div class=\"schedule_car_data_list row".$bt0_car_name_list."\">
													<!--div class=\"col-md-2 text-center br_ccc pdl0 pdr0\">
														<div class=\"tit_item\">
															<input type=\"checkbox\" id=\"car_serial_".$result_car[$i]['serial'].$temp_time."\" name=\"car_serial_".$result_car[$i]['serial'].$temp_time."\" disabled>
															<label for=\"car_serial_".$result_car[$i]['serial'].$temp_time."\">
															</label>
														</div>
													</div-->
													<div class=\"col-md-7 text-center br_ccc\">".$result_car[$i]['car_name_detail']."</div>
													<div class=\"col-md-5 text-center\">".$result_car[$i]['car_number']."</div>
												</div>
";
			}

			if($this_month_day_cell_car == ''){
				$bto0_schedule_time = " bt0";
				$ul_schedule_time = "";
			} else {
				$bto0_schedule_time = "";
				$ul_schedule_time = " class=\"bt_ccc\"";
			}
			if($data['schedule_mode'] == "date"){
				$class_mode ="day ";
			} else {
				$class_mode ="time ";
			}
			$count_holiday_car_temp = count($holiday_car[$result_car[$i]['company_serial']][$result_car[$i]['serial']]);
			if($count_holiday_car_temp >= '1'){
				while (list($key, $value) = each($holiday_car[$result_car[$i]['company_serial']][$result_car[$i]['serial']])){
					$this_month_day_cell = str_replace("rengo_on\" onclick=\"javascript:close_day_car('".$key."'", "rengo_off\" onclick=\"javascript:close_day_car('".$key."'", $this_month_day_cell);
				}
			}
			$this_month_day_cell_car .= "
														<div class=\"row\">
															<div class=\"schedule_time_wrap ".$class_mode."".$bto0_schedule_time."\" ".$count_holiday_car_temp.">
																<div class=\"schedule_time\">
																	<ul".$ul_schedule_time.">
																	".str_replace("#car_serial", $result_car[$i]['serial'], $this_month_day_cell)."
																	</ul>
																</div>
															</div>
														</div>
";
			$k++;
			$this_month_day_cell = str_replace("#car_serial", '', $original_this_month_day_cell);
		}

													



		for($i=0;$i<count($special_price);$i++){
			$position_left = 0;
			$position_top = 87;
			$cell_width = 48;
			$cell_height = 42;
			$sp = $special_price[$i];
			if(intval(substr($sp['end_date'], 0, 8)) < $start_date){
				continue;
			}
	
			if(intval(substr($sp['start_date'], 0, 8)) < $start_date){
				$sp['start_date'] = substr($start_date, 0, 8)."0000";
			} else if(intval(substr($sp['end_date'], 0, 8)) > $finish_date){
				$sp['end_date'] = substr($finish_date, 0, 8)."2400";
			}

			$diff_hour = (strtotime($sp['start_date']) - strtotime($start_date."0000")) / (60*60) ;
			$count_day = intval($diff_hour / 24);
			$count_hour = $diff_hour - ($count_day * 24);

			if(intval($count_hour) != $count_hour)$count_hour = intval($count_hour) + 1;

			$left = ($count_day) * $cell_width;

			if(intval(substr($sp['start_date'], 8, 2)) >= '1'){
				$left = $left + substr($sp['start_date'], 8, 2) * 2;
			}
		
			if(intval(substr($sp['start_date'], 10, 2)) == '30'){
				$left = $left + 1;
			}

			$diff_hour = (strtotime($sp['end_date']) - strtotime($sp['start_date'])) / (60*60) ;
			$count_day = intval($diff_hour / 24);
			$count_hour = $diff_hour - ($count_day * 24);
		
			if(intval($count_hour) != $count_hour)$count_hour = intval($count_hour) + 1;

			if($count_day >= '1'){
				$width = ($count_day * $cell_width) + ($count_hour * 2);
			} else {
				$width = ($count_hour * 2);
			}

			$top = $position_top + ($cell_height * ($i-1));
			$special_price_list .= "
														<a class=\"sale bg_deep_purple\" style=\"top:".($position_top)."px; left:".$left."px; width:".$width."px;\"><span style=\"max-width:".($width)."px;\">".$special_price[$i]['memo']." (".($special_price[$i]['percent']-100>='1'?("+".number_format($special_price[$i]['percent']-100)):number_format($special_price[$i]['percent']-100))."%)</span></a>
";
		}

		unset($result);
		$result['count_car_list'] = $count_car_list;
		$result['count_registered_car_number'] = intval($count_car_list);
		$result['count_rengo_car_number'] = intval($count_rengo_car_number);
		$result['special_price_list'] = $special_price_list;
		$result['car_name_list'] = $car_name_list;
		$result['order_list_detail'] = $order_list_detail;
		$result['this_month_day_name'] = $this_month_day_name;
		$result['this_month_day_number'] = $this_month_day_number;
		$result['this_month_day_cell'] = $this_month_day_cell;
		$result['this_month_day_cell_car'] = $this_month_day_cell_car;
		$result['year'] = $year;
		$result['month'] = $month;
		$result['day'] = $day;

		return $result;
	}

	function save_order(){

		$order_serial = $this->input->post('serial');

		//트랜잭션 시작
		$this->db->trans_start();
		$birthday = $this->input->post('birthday');
		$second_birthday = $this->input->post('second_birthday');
		//1. 일단 유저 넣거나 찾아서 user_serial 얻어온다.
		if(strlen($this->input->post('birthday')) == "6"){
			$birthday = "19".$this->input->post('birthday');
		}
		if(strlen($this->input->post('second_birthday')) == "6"){
			$second_birthday = "19".$this->input->post('second_birthday');
		}
		$sql = "
		SELECT
			*
		FROM
			user_information
		WHERE
			company_serial = '".$this->input->post('company_serial')."' and
			user_name = '".$this->input->post('username')."' and
			birthday = '".$birthday."' and
			gender = '".$this->input->post('gender')."' and
			phone_number1 = '".$this->input->post('tel', TRUE)."'
		";
		$result_user_information = $this->db->fReadSql($sql);
		$user_serial = 0;
		//해당 업체의 동일 유저가 있으면
		if($result_user_information['0']['user_serial'] >= '1'){

			$user_serial = $result_user_information['0']['user_serial'];
			$sql = "
			UPDATE
				user_information
			SET
				user_email = '".$this->input->post('user_mail', TRUE)."',
				company_serial = '".$this->input->post('company_serial', TRUE)."',
				user_name = '".$this->input->post('username', TRUE)."',
				birthday = '".$birthday."',
				gender = '".$this->input->post('gender', TRUE)."',
				phone_number1 = '".$this->input->post('tel', TRUE)."',
				phone_number2 = '".$this->input->post('phone_number2', TRUE)."',
				license_type = '".$this->input->post('license_type', TRUE)."',
				license_number = '".$this->input->post('license_number', TRUE)."',
				published_date = '".$this->input->post('published_date', TRUE)."',
				expiration_date = '".$this->input->post('expiration_date', TRUE)."',
				postcode = '".$this->input->post('postcode', TRUE)."',
				address = '".$this->input->post('address', TRUE)."'
			WHERE
				user_serial = '".$result_user_information['0']['user_serial']."'
			";
			$this->db->query($sql);
		
		} else { // 동일 유저가 없으면 새로 넣는다.
			$sql = "
			INSERT INTO
				user_information
			SET
				user_email = '".$this->input->post('user_mail', TRUE)."',
				company_serial = '".$this->input->post('company_serial', TRUE)."',
				user_name = '".$this->input->post('username', TRUE)."',
				birthday = '".$birthday."',
				gender = '".$this->input->post('gender', TRUE)."',
				phone_number1 = '".$this->input->post('tel', TRUE)."',
				phone_number2 = '".$this->input->post('phone_number2', TRUE)."',
				license_type = '".$this->input->post('license_type', TRUE)."',
				license_number = '".$this->input->post('license_number', TRUE)."',
				published_date = '".$this->input->post('published_date', TRUE)."',
				expiration_date = '".$this->input->post('expiration_date', TRUE)."',
				postcode = '".$this->input->post('postcode', TRUE)."',
				address = '".$this->input->post('address', TRUE)."',
				user_status = '일반'
			";
			$this->db->query($sql);
			$user_serial = $this->db->insert_id();
		}


/*제2운전자정보*/
/*
		$sql = "
		SELECT
			*
		FROM
			user_information
		WHERE
			company_serial = '".$this->input->post('company_serial')."' and
			user_name = '".$this->input->post('second_username')."' and
			birthday = '".$this->input->post('second_birthday')."' and
			gender = '".$this->input->post('second_gender')."' and
			phone_number1 = '".$this->input->post('second_tel', TRUE)."'
		";
		$result_user_information = $this->db->fReadSql($sql);
		$second_user_serial = 0;
		//해당 업체의 동일 유저가 있으면
		if($result_user_information['0']['user_serial'] >= '1'){

			$second_user_serial = $result_user_information['0']['user_serial'];
			$sql = "
			UPDATE
				user_information
			SET
				company_serial = '".$this->input->post('company_serial', TRUE)."',
				user_name = '".$this->input->post('second_username', TRUE)."',
				birthday = '".$this->input->post('second_birthday', TRUE)."',
				gender = '".$this->input->post('second_gender', TRUE)."',
				phone_number1 = '".$this->input->post('second_tel', TRUE)."',
				phone_number2 = '".$this->input->post('second_phone_number2', TRUE)."',
				license_type = '".$this->input->post('second_license_type', TRUE)."',
				license_number = '".$this->input->post('second_license_number', TRUE)."',
				published_date = '".$this->input->post('second_published_date', TRUE)."',
				expiration_date = '".$this->input->post('second_expiration_date', TRUE)."',
				postcode = '".$this->input->post('second_postcode', TRUE)."',
				address = '".$this->input->post('second_address', TRUE)."'
			WHERE
				user_serial = '".$result_user_information['0']['user_serial']."'
			";
			$this->db->query($sql);
		
		} else { // 동일 유저가 없으면 새로 넣는다.
			$sql = "
			INSERT INTO
				user_information
			SET
				company_serial = '".$this->input->post('company_serial', TRUE)."',
				user_name = '".$this->input->post('second_username', TRUE)."',
				birthday = '".$this->input->post('second_birthday', TRUE)."',
				gender = '".$this->input->post('second_gender', TRUE)."',
				phone_number1 = '".$this->input->post('second_tel', TRUE)."',
				phone_number2 = '".$this->input->post('second_phone_number2', TRUE)."',
				license_type = '".$this->input->post('second_license_type', TRUE)."',
				license_number = '".$this->input->post('second_license_number', TRUE)."',
				published_date = '".$this->input->post('second_published_date', TRUE)."',
				expiration_date = '".$this->input->post('second_expiration_date', TRUE)."',
				postcode = '".$this->input->post('second_postcode', TRUE)."',
				address = '".$this->input->post('second_address', TRUE)."',
				user_status = '일반'
			";
			$this->db->query($sql);
			$user_serial = $this->db->insert_id();
		}
*/


			// 		$result['code'] = "E01";
			// $error = $this->db->error();
			// $result['message'] =$error['message'];

			// return $result;
		
		//2. 유저 serial 얻어서 order_list를 추가하거나 update한다. 만약 kind가 보험이면 보험정보도 넣는다.

		//status는 대여날짜가 있으면 운행, 반납날짜가 있으면 반납으로 자동 변경
		$status = '예약';
		if($this->input->post('period_start')!='' && $this->input->post('period_finish')!=''){
			$status = '반납';
		}else if($this->input->post('period_start')!='' && $this->input->post('period_finish')==''){
			$status = '운행';
		}
		//order_kind는 예약 기간에 30일 이상이면 중기, 365일 이상이면 장기로
		$order_kind = $this->input->post('order_kind');
		if($order_kind!='보험'){
			$period_start = $this->input->post('rental_plan');
			$period_finish = $this->input->post('return_plan');

			//이사님 날짜 구하는 로직 가지고 옴
			for($i=0;$period_finish>=date("YmdHi", strtotime($period_start." +".($i+1)." days"));$i++){
				$day = date("w", strtotime($period_start." +".$i." days"));

				if($day < '1' || $day > '5'){
					//평일
					$weekend_count ++;
				} else {
					//주말
					$week_count ++;
				}
			}



			$last_date = date("YmdHi", strtotime($period_start." +".$i." days"));

			$day_count = $week_count + $weekend_count;

			$period = number_format($week_count + $weekend_count);


			//365일 이상이면
			if($period>'365'){
				$order_kind = '장기';
			}else if($period>'31'){
				$order_kind = '중기';
			}

			// $result['code'] = "E01";
			// $result['message'] = $period;
			// return $result;

		}
		if(intval($order_serial) >= 1){


			$sql = "
			UPDATE
				order_list
			SET
				status = '".$status."',
				period_start = '".$this->input->post('rental_plan')."',
				period_finish = '".$this->input->post('return_plan')."',
				delivery_place = '".$this->input->post('delivery_place')."',
				pickup_place = '".$this->input->post('pickup_place')."',
				company_serial = '".$this->input->post('company_serial')."',
				branch_serial = '".$this->input->post('branch_serial')."',
				car_serial = '".$this->input->post('car_serial')."',
				contract_period = '".$this->input->post('contract_period')."',
				rental_price = '".$this->input->post('rental_price')."',
				period_price_serial = '".$this->input->post('period_price_serial')."',
				special_price_serial = '".$this->input->post('special_price_serial')."',
				price_off = '".$this->input->post('price_off')."',
				total_price = '".$this->input->post('total_price')."',
				period_start_order = '".$this->input->post('period_start')."',
				period_finish_order = '".$this->input->post('period_finish')."',
				rental_staff_serial = '".$this->input->post('rental_staff_serial')."',
				rental_staff_name = '".$this->input->post('rental_staff_name')."',
				return_staff_serial = '".$this->input->post('return_staff_serial')."',
				return_staff_name = '".$this->input->post('return_staff_name')."',
				delivery_place_order = '".$this->input->post('delivery_place_order')."',
				pickup_place_order = '".$this->input->post('pickup_place_order')."',
				start_fuel = '".$this->input->post('start_fuel')."',
				end_fuel = '".$this->input->post('end_fuel')."',
				start_km = '".$this->input->post('start_km')."',
				end_km = '".$this->input->post('end_km')."',
				drive_km = '".$this->input->post('drive_km')."',
				memo_text = '".$this->input->post('memo_text')."',
				order_kind = '".$order_kind."',

				username = '".$this->input->post('username')."',
				user_mail = '".$this->input->post('user_mail')."',
				user_serial = '".$user_serial."',
				tel = '".$this->input->post('tel')."',
				birthday = '".$birthday."',
				gender = '".$this->input->post('gender')."',
				phone_number2 = '".$this->input->post('phone_number2', TRUE)."',
				license_type = '".$this->input->post('license_type', TRUE)."',
				license_number = '".$this->input->post('license_number', TRUE)."',
				published_date = '".$this->input->post('published_date', TRUE)."',
				expiration_date = '".$this->input->post('expiration_date', TRUE)."',
				postcode = '".$this->input->post('postcode', TRUE)."',
				address = '".$this->input->post('address', TRUE)."',

				edited_date = now(),
				edited_ip = '".getenv(REMOTE_ADDR)."',


				second_username = '".$this->input->post('second_username')."',
				second_user_mail = '".$this->input->post('second_user_mail')."',
				second_user_serial = '".$this->input->post('second_user_serial')."',
				second_tel = '".$this->input->post('second_tel')."',
				second_birthday = '".$second_birthday."',
				second_gender = '".$this->input->post('second_gender')."',

				second_phone_number2 = '".$this->input->post('second_phone_number2', TRUE)."',
				second_license_type = '".$this->input->post('second_license_type', TRUE)."',
				second_license_number = '".$this->input->post('second_license_number', TRUE)."',
				second_published_date = '".$this->input->post('second_published_date', TRUE)."',
				second_expiration_date = '".$this->input->post('second_expiration_date', TRUE)."',
				second_postcode = '".$this->input->post('second_postcode', TRUE)."',
				second_address = '".$this->input->post('second_address', TRUE)."',

				etc_price_off = '".$this->input->post('etc_price_off', TRUE)."',
				insurance_name = '".$this->input->post('insurance_name', TRUE)."',
				insurance_price = '".$this->input->post('insurance_price', TRUE)."',
				delivery_type = '".$this->input->post('delivery_type', TRUE)."',
				delivery_price = '".$this->input->post('delivery_price', TRUE)."',
				option_type = '".$this->input->post('option_type', TRUE)."',
				option_price = '".$this->input->post('option_price', TRUE)."',
				accident_type = '".$this->input->post('accident_type', TRUE)."',
				accident_price = '".$this->input->post('accident_price', TRUE)."',
				overcharge_payback_type = '".$this->input->post('overcharge_payback_type', TRUE)."',
				overcharge_price = '".$this->input->post('overcharge_price', TRUE)."',
				payback_price = '".$this->input->post('payback_price', TRUE)."',
				etc_type = '".$this->input->post('etc_type', TRUE)."',
				etc_price = '".$this->input->post('etc_price', TRUE)."'
			WHERE
				serial = '".$order_serial."'
			";
			$this->db->query($sql);

			// $drive_distance = $this->input->post('end_km');
			// if( $drive_distance != null && $drive_distance != ''){
			// 	$sql = "
			// 	UPDATE car_list
			// 	SET drive_distance = '".$drive_distance."'
			// 	WHERE serial='".$this->input->post('car_serial')."'";
			// 	$this->db->query($sql);
			// }


			//보험대차
			if(urldecode($this->input->post('order_kind')) == "보험"){
				$sql = "
				UPDATE
					insurance_car
				SET
					damaged_car_type = '".$this->input->post('damaged_car_type')."',
					damaged_car_number = '".$this->input->post('damaged_car_number')."',
					insurance_company = '".$this->input->post('insurance_company')."',
					insurance_number = '".$this->input->post('insurance_number')."',
					insurance_manager = '".$this->input->post('insurance_manager')."',
					accident_car_number = '".$this->input->post('accident_car_number')."',
					accident_phone = '".$this->input->post('accident_phone')."',
					accident_fax = '".$this->input->post('accident_fax')."',
					repair_company = '".$this->input->post('repair_company')."',
					repair_phone = '".$this->input->post('repair_phone')."',
					accident_percent = '".$this->input->post('accident_percent')."',
					insurance_date = '".$this->input->post('insurance_date')."'
				WHERE
					order_serial = '".$order_serial."'
				";
				$this->db->query($sql);
			}

		} else {
			$sql = "
			INSERT INTO order_list
			SET

			flag = 'P',
			status = '".$status."',
			period_start = '".$this->input->post('rental_plan')."',
			period_finish = '".$this->input->post('return_plan')."',
			delivery_place = '".$this->input->post('delivery_place')."',
			pickup_place = '".$this->input->post('pickup_place')."',
			company_serial = '".$this->input->post('company_serial')."',
			branch_serial = '".$this->input->post('branch_serial')."',
			car_serial = '".$this->input->post('car_serial')."',
			contract_period = '".$this->input->post('contract_period')."',
			rental_price = '".$this->input->post('rental_price')."',
			period_price_serial = '".$this->input->post('period_price_serial')."',
			special_price_serial = '".$this->input->post('special_price_serial')."',
			price_off = '".$this->input->post('price_off')."',
			total_price = '".$this->input->post('total_price')."',
			period_start_order = '".$this->input->post('period_start')."',
			period_finish_order = '".$this->input->post('period_finish')."',
			rental_staff_serial = '".$this->input->post('rental_staff_serial')."',
			rental_staff_name = '".$this->input->post('rental_staff_name')."',
			return_staff_serial = '".$this->input->post('return_staff_serial')."',
			return_staff_name = '".$this->input->post('return_staff_name')."',
			delivery_place_order = '".$this->input->post('delivery_place_order')."',
			pickup_place_order = '".$this->input->post('pickup_place_order')."',
			start_fuel = '".$this->input->post('start_fuel')."',
			end_fuel = '".$this->input->post('end_fuel')."',
			start_km = '".$this->input->post('start_km')."',
			end_km = '".$this->input->post('end_km')."',
			drive_km = '".$this->input->post('drive_km')."',
			memo_text = '".$this->input->post('memo_text')."',
			order_kind = '".$order_kind."',
			
			username = '".$this->input->post('username')."',
			user_mail = '".$this->input->post('user_mail')."',
			user_serial = '".$user_serial."',
			tel = '".$this->input->post('tel')."',
			birthday = '".$birthday."',
			gender = '".$this->input->post('gender')."',

			registered_date = now(),
			registered_ip = '".getenv(REMOTE_ADDR)."',

			phone_number2 = '".$this->input->post('phone_number2', TRUE)."',
			license_type = '".$this->input->post('license_type', TRUE)."',
			license_number = '".$this->input->post('license_number', TRUE)."',
			published_date = '".$this->input->post('published_date', TRUE)."',
			expiration_date = '".$this->input->post('expiration_date', TRUE)."',
			postcode = '".$this->input->post('postcode', TRUE)."',
			address = '".$this->input->post('address', TRUE)."',



			second_username = '".$this->input->post('second_username')."',
			second_user_mail = '".$this->input->post('second_user_mail')."',
			second_user_serial = '".$this->input->post('second_user_serial')."',
			second_tel = '".$this->input->post('second_tel')."',
			second_birthday = '".$second_birthday."',
			second_gender = '".$this->input->post('second_gender')."',

			second_phone_number2 = '".$this->input->post('second_phone_number2', TRUE)."',
			second_license_type = '".$this->input->post('second_license_type', TRUE)."',
			second_license_number = '".$this->input->post('second_license_number', TRUE)."',
			second_published_date = '".$this->input->post('second_published_date', TRUE)."',
			second_expiration_date = '".$this->input->post('second_expiration_date', TRUE)."',
			second_postcode = '".$this->input->post('second_postcode', TRUE)."',
			second_address = '".$this->input->post('second_address', TRUE)."',

			etc_price_off = '".$this->input->post('etc_price_off', TRUE)."',
			insurance_name = '".$this->input->post('insurance_name', TRUE)."',
			insurance_price = '".$this->input->post('insurance_price', TRUE)."',
			delivery_type = '".$this->input->post('delivery_type', TRUE)."',
			delivery_price = '".$this->input->post('delivery_price', TRUE)."',
			option_type = '".$this->input->post('option_type', TRUE)."',
			option_price = '".$this->input->post('option_price', TRUE)."',
			accident_type = '".$this->input->post('accident_type', TRUE)."',
			accident_price = '".$this->input->post('accident_price', TRUE)."',
			overcharge_payback_type = '".$this->input->post('overcharge_payback_type', TRUE)."',
			overcharge_price = '".$this->input->post('overcharge_price', TRUE)."',
			payback_price = '".$this->input->post('payback_price', TRUE)."',
			etc_type = '".$this->input->post('etc_type', TRUE)."',
			etc_price = '".$this->input->post('etc_price', TRUE)."'
			";

			$this->db->query($sql);

			$order_serial = $this->db->insert_id();


			// $drive_distance = $this->input->post('end_km');
			// if( $drive_distance != null && $drive_distance != ''){
			// 	$sql = "
			// 	UPDATE car_list
			// 	SET drive_distance = '".$drive_distance."'
			// 	WHERE serial='".$this->input->post('car_serial')."'";
			// 	$this->db->query($sql);
			// }

			//보험대차
			if(urldecode($this->input->post('order_kind')) == "보험"){
				$sql = "
				INSERT INTO insurance_car
				SET
					order_serial = '".$order_serial."',
					damaged_car_type = '".$this->input->post('damaged_car_type')."',
					damaged_car_number = '".$this->input->post('damaged_car_number')."',
					insurance_company = '".$this->input->post('insurance_company')."',
					insurance_number = '".$this->input->post('insurance_number')."',
					insurance_manager = '".$this->input->post('insurance_manager')."',
					accident_car_number = '".$this->input->post('accident_car_number')."',
					accident_phone = '".$this->input->post('accident_phone')."',
					accident_fax = '".$this->input->post('accident_fax')."',
					repair_company = '".$this->input->post('repair_company')."',
					repair_phone = '".$this->input->post('repair_phone')."',
					accident_percent = '".$this->input->post('accident_percent')."',
					insurance_date = '".$this->input->post('insurance_date')."'
				";
				$this->db->query($sql);
			}
		}
		//트랜잭션 종료
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
		        // generate an error... or use the log_message() function to log your error
			$result['code'] = "E01";
			$error = $this->db->error();
			$result['message'] =$error['message'];
		}else{
			$result['code'] = "S01";
		}

		return $result;
		
	}

	function order_detail($serial){
		if(intval($serial) >= '1'){
			$sql = "
			select
				*
			from
				order_list
			where
				serial = '".intval($serial)."'
			";
			$result_order_detail = $this->db->fReadSql($sql);
			if($result_order_detail['0']['period_start_order'] != '' && strlen($result_order_detail['0']['period_start_order']) == '12'){
				$result_order_detail['0']['period_start_order'] = substr($result_order_detail['0']['period_start_order'], 4, 2)."/".substr($result_order_detail['0']['period_start_order'], 6, 2)."/".substr($result_order_detail['0']['period_start_order'], 0, 4)." ".substr($result_order_detail['0']['period_start_order'], 8, 2).":".substr($result_order_detail['0']['period_start_order'], 10, 2).":00";
			}
			if($result_order_detail['0']['period_finish_order'] != '' && strlen($result_order_detail['0']['period_finish_order']) == '12'){
				$result_order_detail['0']['period_finish_order'] = substr($result_order_detail['0']['period_finish_order'], 4, 2)."/".substr($result_order_detail['0']['period_finish_order'], 6, 2)."/".substr($result_order_detail['0']['period_finish_order'], 0, 4)." ".substr($result_order_detail['0']['period_finish_order'], 8, 2).":".substr($result_order_detail['0']['period_finish_order'], 10, 2).":00";
			}
			
			$result_order_detail['0']['period_start'] = substr($result_order_detail['0']['period_start'], 4, 2)."/".substr($result_order_detail['0']['period_start'], 6, 2)."/".substr($result_order_detail['0']['period_start'], 0, 4)." ".substr($result_order_detail['0']['period_start'], 8, 2).":".substr($result_order_detail['0']['period_start'], 10, 2).":00";
			$result_order_detail['0']['period_finish'] = substr($result_order_detail['0']['period_finish'], 4, 2)."/".substr($result_order_detail['0']['period_finish'], 6, 2)."/".substr($result_order_detail['0']['period_finish'], 0, 4)." ".substr($result_order_detail['0']['period_finish'], 8, 2).":".substr($result_order_detail['0']['period_finish'], 10, 2).":00";



		}
		if(strlen($result_order_detail['0']['birthday']) == '8')$result_order_detail['0']['birthday'] = substr($result_order_detail['0']['birthday'], 2);
		$result = $result_order_detail['0'];
		$sql = "
			SELECT
				a.*,
				b.car_name_detail,
				b.car_type,
				b.car_people,
				a.car_number
			FROM
				car_list a,
				car_master b
			WHERE
				a.car_index = b.car_index
				and a.serial = '".$result_order_detail['0']['car_serial']."'

		";
		$result_car = $this->db->fReadSql($sql);

		$result['car_type'] = $result_car['0']['car_type'];
		$result['car_number'] = $result_car['0']['car_number'];
		$result['car_name_detail'] = $result_car['0']['car_name_detail'];
		$result['car_people'] = $result_car['0']['car_people'];
		$result['gear_type'] = $result_car['0']['gear_type'];
		$result['fuel_option'] = $result_car['0']['fuel_option'];
		$result['smoking'] = $result_car['0']['smoking'];

		$result['option_navi'] = $result_car['0']['option_navi'];
		$result['option_bluetooth'] = $result_car['0']['option_bluetooth'];
		$result['option_automatic_back_mirror'] = $result_car['0']['option_automatic_back_mirror'];
		$result['option_hi_pass'] = $result_car['0']['option_hi_pass'];
		$result['option_aux'] = $result_car['0']['option_aux'];
		$result['option_heatseat'] = $result_car['0']['option_heatseat'];
		$result['option_gps'] = $result_car['0']['option_gps'];
		$result['option_rear_sensor'] = $result_car['0']['option_rear_sensor'];
		$result['option_blackbox'] = $result_car['0']['option_blackbox'];
		$result['option_cdplayer'] = $result_car['0']['option_cdplayer'];
		$result['option_backcamera'] = $result_car['0']['option_backcamera'];
		$result['option_sunroof'] = $result_car['0']['option_sunroof'];
		$result['option_lane_departure_warning'] = $result_car['0']['option_lane_departure_warning'];
		$result['option_automatic_seat'] = $result_car['0']['option_automatic_seat'];
		$result['option_front_sensor'] = $result_car['0']['option_front_sensor'];
		$result['option_smart_key'] = $result_car['0']['option_smart_key'];
		$result['option_av_system'] = $result_car['0']['option_av_system'];
		$result['option_ecm'] = $result_car['0']['option_ecm'];
		$result['option_license1'] = $result_car['0']['option_license1'];
		$result['option_usb'] = $result_car['0']['option_usb'];

		$result['code'] = "S01";
		$result['sql'] = $sql;

		return $result;
	}

	function insurance_detail($serial){
		$sql="
		SELECT 
			*
		FROM 
			insurance_car
		WHERE 
			order_serial='".$serial."'";

		$result=$this->db->fReadSql($sql);
		return $result;
	}

	function get_count($company_serial=0) {
		$sql="
		SELECT 
			COUNT(car_list.flag) AS booking
		FROM 
			car_list
		WHERE 
			flag='Y'
		";
		$wherecon = $company_serial == 0? '' : " AND company_serial='".$company_serial."'";

		$result=$this->db->fReadSql($sql.$wherecon);
		return $result[0]['booking'];
	}

	function set_holiday($holiday, $company_serial, $car_serial = '') {
		$company_serial = intval($company_serial);
		$holiday = intval($holiday);
		$car_serial = intval($car_serial);

		if($company_serial < '1' || strlen($holiday) != '8'){
			$result['message'] = $company_serial;
		}

		if($car_serial >= '1'){
			$sql = "
			SELECT
				*
			FROM
				holiday_list_car
			WHERE
				holiday = '".substr($holiday, 0, 8)."' and
				company_serial = '".$company_serial."' and
				car_serial = '".$car_serial."'
			";
			$result = $this->db->fReadSql($sql);
			if($result['0']['serial'] >= '1'){
				$sql = "
				DELETE
				FROM
					holiday_list
				WHERE
					holiday = '".substr($holiday, 0, 8)."' and
					company_serial = '".$company_serial."'
				";
				$this->db->query($sql);

				$sql = "
				DELETE
				FROM
					holiday_list_car
				WHERE
					holiday = '".substr($holiday, 0, 8)."' and
					company_serial = '".$company_serial."' and
					car_serial = '".$car_serial."'
				";
				$this->db->query($sql);

			} else {
				$sql = "
				SELECT
					*
				FROM
					holiday_list
				WHERE
					holiday = '".substr($holiday, 0, 8)."' and
					company_serial = '".$company_serial."'
				";
				$result = $this->db->fReadSql($sql);

				if($result['0']['serial'] >= '1'){
					$sql = "
					DELETE
					FROM
						holiday_list
					WHERE
						holiday = '".substr($holiday, 0, 8)."' and
						company_serial = '".$company_serial."'
					";
					$this->db->query($sql);

					$sql = "
					SELECT
						*
					FROM
						car_list
					WHERE
						company_serial = '".$company_serial."' and
						flag = 'Y' and
						car_status = '정상'
					";
					$result = $this->db->fReadSql($sql);

					for($i=0;$i<count($result);$i++){
						if($result[$i]['serial'] == $car_serial)continue;
						$sql = "
						INSERT INTO
							holiday_list_car
						SET
							holiday = '".substr($holiday, 0, 8)."',
							company_serial = '".$company_serial."',
							car_serial = '".$result[$i]['serial']."',
							registered_date = now(),
							registered_ip = '".getenv(REMOTE_ADDR)."'
						";
						$this->db->query($sql);
					}

				} else {
					$sql = "
					INSERT INTO
						holiday_list_car
					SET
						holiday = '".substr($holiday, 0, 8)."',
						company_serial = '".$company_serial."',
						car_serial = '".$car_serial."',
						registered_date = now(),
						registered_ip = '".getenv(REMOTE_ADDR)."'
					";
					$this->db->query($sql);
				}
			}
			$result['message'] = $sql;
			return $result;

		} else {
			$sql = "
			SELECT
				*
			FROM
				holiday_list
			WHERE
				holiday = '".substr($holiday, 0, 8)."' and
				company_serial = '".$company_serial."'
			";
			$result = $this->db->fReadSql($sql);
			if($result['0']['serial'] >= '1'){
				$sql = "
				DELETE
				FROM
					holiday_list
				WHERE
					holiday = '".substr($holiday, 0, 8)."' and
					company_serial = '".$company_serial."'
				";
				$this->db->query($sql);
			} else {
				$sql = "
				INSERT INTO
					holiday_list
				SET
					holiday = '".substr($holiday, 0, 8)."',
					company_serial = '".$company_serial."',
					registered_date = now(),
					registered_ip = '".getenv(REMOTE_ADDR)."'
				";
				$this->db->query($sql);
			}
			$sql = "
			DELETE
			FROM
				holiday_list_car
			WHERE
				holiday = '".substr($holiday, 0, 8)."' and
				company_serial = '".$company_serial."'
			";
			$this->db->query($sql);

			$result['message'] = $sql;
			return $result;
		}
	}


	function delete_order($serial){
		//결제 내역이 있는 경우 불가능하다고 리턴
		$sql = "
				SELECT * 
				FROM receipt_history
				WHERE flag != 'N' 
				AND order_serial = '".$serial."' 
				";
		$result = $this->db->fReadSql($sql);
		if(count($result)>0){
			$response['code'] = 'E02';
			$response['message'] = '수납 내역이 있는 경우 삭제가 불가능합니다.';
			return $response;
		}else{//실제 삭제는 하지 않고 flag만 N으로 설정한다. 해당 order에 해당하는 결제 내역도 flag N 한다.
			//트랜잭션 시작
			$this->db->trans_start();
			$sql = "
					UPDATE receipt_history
					SET flag = 'N'
					WHERE order_serial = '".$serial."' 
					";
			$this->db->query($sql);

			$sql = "
					UPDATE order_list
					SET flag = 'D'
					WHERE serial = '".$serial."' 
					";
			//트랜잭션 종료
			$this->db->query($sql);
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				$response['code'] = "E01";
				$error = $this->db->error();
				$response['message'] =$error['message'];
			}else{
				$response['code'] = "S01";
			}
			return $response;
		}
	}



	function m_get_all_order($company_serial, $start, $end){

		try{
			$sql = "
			SELECT 
			   O.serial, O.user_mail, O.username, O.birthday, O.gender, O.tel, O.period_start, O.period_finish, O.delivery_place, O.pickup_place, O.rental_price, O.total_price, O.option_price, O.insurance_price, O.price_off, 
			   C.car_number, C.gear_type, C.fuel_option, C.requirement_age, C.option_navi, C.option_bluetooth, C.option_automatic_back_mirror, C.option_hi_pass, C.option_aux, C.option_heatseat, C.option_gps, C.option_rear_sensor, C.option_blackbox, C.option_cdplayer, C.option_backcamera, C.option_sunroof, C.option_lane_departure_warning, C.option_automatic_seat, C.option_front_sensor, C.option_smart_key, C.option_av_system, C.option_ecm, C.option_license1, C.option_week_48, C.option_weekend_48,
			   M.car_name_detail
			 FROM
			   order_list as O, car_list as C, car_master as M
			 WHERE
			   (O.flag = 'F' or O.flag = 'P' or O.flag = 'Y')
			 AND
			   O.period_start <= '".$end."' 
			 AND
			   O.period_finish >= '".$start."'
			 AND
			   O.company_serial = '".$company_serial."'
			 AND
			 	O.car_serial = C.serial
			 AND
			 	C.car_index = M.car_index";


			$result=$this->db->fReadSql($sql);

			//결제 history찾아서 있으면 항목 더해준다.
			for($i=0; $i<count($result); $i++){
				$sql2 = "
				SELECT deposite_amount, deposite_way
				FROM receipt_history
				WHERE type = '대여요금' AND order_serial = '".$result[$i]['serial']."'
				";
				$receipt_result = $this->db->fReadSql($sql2);
				if(count($receipt_result)!=0){
					$result[$i]['deposite_amount'] = $receipt_result[0]['deposite_amount'];
					$result[$i]['deposite_way'] = $receipt_result[0]['deposite_way'];
				}else{
					$result[$i]['deposite_amount'] = '0';
					$result[$i]['deposite_way'] = '';
				}
			}
			if(count($result) == 0){
				$result = array();
			}
			$response['code'] = 'S01';
			$response['value'] = $result;
			// }else{
			// 	$response['code'] = 'S01';
			// 	$response['value'] = $result;
			// }
			return $response;
		}catch (Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
    		return $response;
		}
	}

	function m_get_order_detail($order_serial){

		try{
			$sql = "
			SELECT 
			     O.serial, O.user_mail, O.username, O.birthday, O.gender, O.tel, O.period_start, O.period_finish, O.delivery_place, O.pickup_place, O.rental_price, O.total_price, O.option_price, O.insurance_price, O.price_off AS discount_price, O.rental_staff_name, O.rental_staff_serial, O.return_staff_name, O.return_staff_serial,
			   C.car_number, C.gear_type, C.fuel_option, C.requirement_age, C.option_navi, C.option_bluetooth, C.option_automatic_back_mirror, C.option_hi_pass, C.option_aux, C.option_heatseat, C.option_gps, C.option_rear_sensor, C.option_blackbox, C.option_cdplayer, C.option_backcamera, C.option_sunroof, C.option_lane_departure_warning, C.option_automatic_seat, C.option_front_sensor, C.option_smart_key, C.option_av_system, C.option_ecm, C.option_license1, C.option_week_48, C.option_weekend_48,
			   M.car_name_detail
			 FROM
			   order_list as O, car_list as C, car_master as M
			 WHERE
			   O.serial = '".$order_serial."'
			 AND
			 	O.car_serial = C.serial
			 AND
			 	C.car_index = M.car_index";


			$result=$this->db->fReadSql($sql);

			//결제 history찾아서 있으면 항목 더해준다.
			for($i=0; $i<count($result); $i++){
				$sql2 = "
				SELECT deposite_amount, deposite_way
				FROM receipt_history
				WHERE type = '대여요금' AND order_serial = '".$result[$i]['serial']."'
				";
				$receipt_result = $this->db->fReadSql($sql2);
				if(count($receipt_result)!=0){
					$result[$i]['deposite_amount'] = $receipt_result[0]['deposite_amount'];
					$result[$i]['deposite_way'] = $receipt_result[0]['deposite_way'];
				}else{
					$result[$i]['deposite_amount'] = '0';
					$result[$i]['deposite_way'] = '';
				}
			}
			
			// if(count($result) == 0){
			if(count($result)==0){
				$result = array();
			}
			$response['code'] = 'S01';
			$response['value'] = $result;
			// }else{
			// 	$response['code'] = 'S01';
			// 	$response['value'] = $result;
			// }
			return $response;
		}catch (Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
    		return $response;
		}
	}

	function m_add_order($order_data, $pay_data){


		//해당 기간이 이미 예약이 되어 있는지 확인하여 이미 예약되어 있으면 error 리턴
		$order_sql = "
			SELECT
			*
			FROM
				order_list
			WHERE
				(flag = 'F' or flag = 'P' ) and
				(period_start < '".$order_data['period_finish']."' and period_finish > '".$order_data['period_start']."' ) and
				car_serial = '".$order_data['car_serial']."' and
				company_serial = '".$order_data['company_serial']."' 
	   		group by serial
		";
		$order_result = $this->db->fReadSql($order_sql);
		if($order_result[0]['serial'] >= '1'){
			$result['code'] = "E04";
			$error = $this->db->error();
			$result['message'] = '예약이 불가능한 기간입니다.';
			return $result;
		}
		//해당 차가 hold 상태(렌고에서 예약중이거 솔루션에서 누가 예약중)인지 확인하여 사용불가이면 error 리턴
		$car_sql = "
			SELECT
			*
			FROM
				car_list
			WHERE
				serial = '".$order_data['car_serial']."' and
				hold_date > now() and hold_date != '' and hold_date IS NOT NULL
		";

		$car_result = $this->db->fReadSql($car_sql);
		if($car_result[0]['serial'] >= '1'){
			$result['code'] = "E05";
			$result['message'] = '현재 예약 진행중인 차량입니다.';
			return $result;
		}



		//트랜잭션 시작
		$this->db->trans_start();
		$birthday = $order_data['birthday'];
		//1. 일단 유저 넣거나 찾아서 user_serial 얻어온다.
		if(strlen($order_data['birthday']) == "6"){
			$birthday = "19".$order_data['birthday'];
		}
		$sql = "
		SELECT
			*
		FROM
			user_information
		WHERE
			company_serial = '".$order_data['company_serial']."' and
			user_name = '".$order_data['username']."' and
			birthday = '".$birthday."' and
			phone_number1 = '".$order_data['tel']."'
		";
		$result_user_information = $this->db->fReadSql($sql);
		$user_serial = 0;

		//해당 업체의 동일 유저가 있으면 업데이트
		if($result_user_information['0']['user_serial'] >= '1'){

			$user_serial = $result_user_information['0']['user_serial'];
			$sql = "
			UPDATE
				user_information
			SET
				user_email = '".$order_data['user_mail']."',
				company_serial = '".$order_data['company_serial']."',
				user_name = '".$order_data['username']."',
				birthday = '".$birthday."',
				phone_number1 = '".$order_data['tel']."'
			WHERE
				user_serial = '".$result_user_information['0']['user_serial']."'
			";
			$this->db->query($sql);
		
		} else { // 동일 유저가 없으면 새로 넣는다.
			$sql = "
			INSERT INTO
				user_information
			SET
				user_email = '".$order_data['user_mail']."',
				company_serial = '".$order_data['company_serial']."',
				user_name = '".$order_data['username']."',
				birthday = '".$birthday."',
				phone_number1 = '".$order_data['tel']."',
				user_status = '일반'
			";
			$this->db->query($sql);
			$user_serial = $this->db->insert_id();
		}


		
		//2. 유저 serial 얻어서 order_list를 추가한다.
		for($i=0;$order_data['period_finish']>=date("YmdHi", strtotime($order_data['period_start']." +".($i+1)." days"));$i++){
			$day = date("w", strtotime($order_data['period_start']." +".$i." days"));

			if($day < '1' || $day > '5'){
				//평일
				$weekend_count ++;
			} else {
				//주말
				$week_count ++;
			}

		}



		$last_date = date("YmdHi", strtotime($order_data['period_start']." +".$i." days"));
		$day_count = $week_count + $weekend_count;
		$hour_count = ceil(gmdate('Hi', strtotime($order_data['period_finish']) - strtotime($last_date)) / 100);

		$order_data['contract_period'] = number_format($week_count + $weekend_count)."일 ".number_format($hour_count)."시간";


		$sql = "
			INSERT INTO order_list
			SET
			flag = 'P',
			period_start = '".$order_data['period_start']."',
			period_finish = '".$order_data['period_finish']."',
			delivery_place = '".$order_data['delivery_place']."',
			pickup_place = '".$order_data['pickup_place']."',
			company_serial = '".$order_data['company_serial']."',
			car_serial = '".$order_data['car_serial']."',
			contract_period = '".$order_data['contract_period']."',
			rental_price = '".$order_data['rental_price']."',
			price_off = '".$order_data['price_off']."',
			total_price = '".$order_data['total_price']."',
			order_kind = '일반',
			username = '".$order_data['username']."',
			user_mail = '".$order_data['user_mail']."',
			user_serial = '".$user_serial."',
			tel = '".$order_data['tel']."',
			birthday = '".$birthday."',
			registered_date = now(),
			registered_ip = '".getenv(REMOTE_ADDR)."',
			insurance_price = '".$order_data['insurance_price']."',
			option_price = '".$order_data['option_price']."'
			";
			// echo $sql;
			// die();
			$this->db->query($sql);
			$order_serial = $this->db->insert_id();

		// if($pay_data['deposite_amount']!=''){
		// 	$sql = "
		// 	INSERT INTO receipt_history
		// 	SET
		// 	flag = 'Y',
		// 	type = '대여요금',
		// 	handler = 'app',
		// 	normal_sale = 'Y',
		// 	company_serial = '".$order_data['company_serial']."',
		// 	car_serial = '".$pay_data['car_serial']."',
		// 	deposite_amount = '".$pay_data['deposite_amount']."',  
		// 	deposite_way = '".$pay_data['deposite_way']."',  
		// 	order_serial = '".$order_serial."',
		// 	user_serial = '".$user_serial."'
		// 	";
		// 	$this->db->query($sql);

		// }
		//트랜잭션 종료
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
		        // generate an error... or use the log_message() function to log your error
			$result['code'] = "E01";
			$error = $this->db->error();
			$result['message'] =$error['message'];
		}else{
			$result['code'] = "S01";
		}

		return $result;


	}


	function m_change_day_flag($company_serial, $holiday, $flag){


		if($flag == 'Y'){
			$sql = "
			SELECT *
			FROM holiday_list
			WHERE company_serial='".$company_serial."'
			AND holiday = '".$holiday."'";
			$result = $this->db->fReadSql($sql);
			if(count($result)>0){
				$response['code'] = "S01";
				$response['value'] = array();
			}else{
				$sql = "
				INSERT INTO holiday_list
				SET
					holiday = '".$holiday."',
					company_serial = '".$company_serial."',
					registered_date = now(),
					registered_ip = '".getenv(REMOTE_ADDR)."'";
				$result = $this->db->query($sql);
				if($result){
					$response['code'] = "S01";
					$response['value'] = array();
				}else{
					$response['code'] = "E01";
					$error = $this->db->error();
					$response['message'] =$error['message'];
				}
			}
			return $response;
		}else if($flag == 'N'){
			$sql = "
			SELECT *
			FROM holiday_list
			WHERE company_serial='".$company_serial."'
			AND holiday = '".$holiday."'";
			$result = $this->db->fReadSql($sql);
			if(count($result)>0){

				$sql = "
				DELETE
				FROM
					holiday_list
				WHERE
					holiday = '".$holiday."' and
					company_serial = '".$company_serial."'
				";
				$result = $this->db->query($sql);
				if($result){
					$response['code'] = "S01";
					$response['value'] = array();
				}else{
					$response['code'] = "E01";
					$error = $this->db->error();
					$response['message'] =$error['message'];
				}				
			}else{
				$response['code'] = "S01";
				$response['value'] = array();
			}
			return $response;
		}else{
			$response['code'] = 'E02';
			$response['message'] = 'input data error';
			return $response;
		}
	}

}