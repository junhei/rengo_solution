<?php
class Staffmanager_model extends CI_Model {
 

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

	}

	function get_count($company_serial){
		if($company_serial=='0'){
			$sql = "SELECT * FROM admin_information WHERE admin_flag='Y' AND admin_level<2" ;
		}else{
			$sql = "SELECT * FROM admin_information WHERE admin_flag='Y' and company_serial=".$company_serial." and admin_level<2" ;
		}

		$result = $this->db->fReadSql($sql);
		return count($result);
	}


	//해당 company의 직원(admin 가능한 사람) list 보여주기
	function get_list($company_serial)
	{
		if($company_serial=='0'){
			$sql = "SELECT * FROM admin_information WHERE admin_flag='Y' AND admin_level<2" ;
		}else{
			$sql = "SELECT * FROM admin_information WHERE admin_flag='Y' and company_serial=".$company_serial." and admin_level<2" ;
		}
		

		$result = $this->db->fReadSql($sql, '');
		return $result;
	}

	function get_list_for_rent($company_serial)
	{
		if($company_serial=='0'){
			$sql = "SELECT * FROM admin_information WHERE admin_flag='Y'" ;
		}else{
			$sql = "SELECT * FROM admin_information WHERE admin_flag='Y' and company_serial=".$company_serial." and admin_level<2" ;
		}

		$result = $this->db->fReadSql($sql, '');
		return $result;
	}




	function add($data, $admin_pw = ''){
		// $sql = "SELECT * FROM car_master WHERE car_name='".$data['car_name']."'";
		$this->db->set('registered_date', 'now()', FALSE);
		$this->db->set('registered_ip', getenv("REMOTE_ADDR"));
		$this->db->set('admin_level',"1");
		$this->db->set('admin_flag', 'Y');

		if($admin_pw){
			$this->db->set('admin_pw', "password('".$admin_pw."')", FALSE);
		}

		$sql = "SELECT * FROM admin_information WHERE admin_id='".$data['admin_id']."' AND admin_flag = 'Y'";
// echo $sql;
		$query = $this->db->query($sql);
		$result_count = $query->num_rows();
// return $result_count;
		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록 된 아이디입니다.";		
		}else{
			$result = $this->db->insert('admin_information', $data);
			if($result){
				$response['code'] ="S01";
				$response['serial'] = $this->db->insert_id();
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;
	}

	function update($data, $admin_pw = ''){
		if($data['serial'] < '1'){
			$response['code'] ="E01";
			$response['message'] = "직원아디를 선택해 주세요..";
			return $response;
		}
		$this->db->flush_cache();
		if($admin_pw){
			$this->db->set('admin_pw', "password('".$admin_pw."')", FALSE);
		}
		$this->db->set('edited_date', 'now()', FALSE);
		$this->db->set('edited_ip', getenv("REMOTE_ADDR"));
		$this->db->where('serial', $data['serial']);

		$result = $this->db->update('admin_information', $data);
		if($result){
			$response['code'] ="S01";
			
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	function delete($data){
		
		$this->db->flush_cache();
		// $this->db->where('company_serial', $data['company_serial']);
		$this->db->where('serial', $data['serial']);
		
		$this->db->set('edited_date', 'now()', FALSE);
		$this->db->set('edited_ip', getenv("REMOTE_ADDR"));
		$this->db->set('admin_flag', 'N');

		$result = $this->db->update('admin_information');
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
		}
		return $response;
	}


	//해당 company의 직원(admin 가능한 사람) list 보여주기
	function m_get_list($company_serial)
	{
		if($company_serial=='0'){
			$sql = "SELECT serial, admin_name FROM admin_information WHERE admin_flag='Y'" ;
		}else{
			$sql = "SELECT serial, admin_name FROM admin_information WHERE admin_flag='Y' and company_serial=".$company_serial." and admin_level<2" ;
		}
		

		$result = $this->db->fReadSql($sql, '');
		return $result;
	}


}