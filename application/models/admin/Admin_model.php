<?php
class Admin_model extends CI_Model {
 
	private $admin_level;
	private $admin_name;
	private $login_date;
	private $data;
	private $admin_function;
	private $menu_permission;


	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->admin_function = array("대여관리", "차량관리", "고객관리", "매출관리", "렌고 설정", "환경설정", "마스터 설정");
		$this->_check_admin_information();
	}

	function get_admin_function()
	{
		$admin_function = $this->admin_function;
		return $admin_function;
	}
	
	function get_admin_level()
	{
		$admin_level = $this->admin_level;
		return $admin_level;
	}

 	function set_admin_level($admin_level)
	{
		$this->admin_level = $this->admin_level;
	}

	function get_partner_list($flag)
	{
		if($flag){
			$sql = "
			SELECT 
				*
			FROM 
				company_information
			WHERE 
				flag = '".$flag."'
			ORDER BY
				company_name asc
			";
		} else {
			$sql = "
			SELECT 
				*
			FROM 
				company_information
			ORDER BY
				company_name asc
			";
		}

		$result = $this->db->fReadSql($sql, '');
		return $result;
	}


	function admin_logout(){ 
		$this->session->sess_destroy(); 
		redirect('/'); 
		die();
	} 


	function get_admin_name()
	{
		$admin_name = $this->admin_name;
		return $admin_name;
	}

 	function set_admin_name($admin_name)
	{
		$this->admin_name = $admin_name;
	}

	function get_data()
	{
		$data = $this->data;
		return $data;
	}

  	function set_data($data)
	{
		$this->data = $data;
	}

	function set_menu_permission($menu_permission)
	{
		$this->menu_permission = $menu_permission;
	}

	function get_menu_permission()
	{
		return $this->menu_permission;
	}

	function _view($data = ''){
		$this->load->view("admin/admin_layout_top", $data); 
//		$this->load->view("admin/admin_status", $data); 
//		$this->load->view("admin/layout_left", $data); 
		$this->load->view($this->template_file, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	// GET타입의 변수를 정리후 재배치
	function _check_request_uri($exception = '') {
		$get_value = $this->input->get();

		if(is_array($get_value)){
			while (list($key, $value) = each($get_value)){
				if(!isset($temp[$key])){
					$temp[$key] = $value;
				}
			}
			while (list($key, $value) = each($temp)){
				if($key != $exception)
					$this->request_uri .= "&".$key."=".$value;
			}
		}
		if($this->request_uri)$this->request_uri = substr($this->request_uri, 1);
	}

	// 메뉴별 관리자 접근 퍼미션 확인
	function _check_permission($menu_permission) {
		$this->set_menu_permission($menu_permission);
		$data = $this->get_data();

		return $data['permission'][$menu_permission];
	}

	// 1. 관리자 정보 체크
	function _check_admin_information() {

		$session_id = $this->session->session_id;

		// 세션에서 관리자 레벨 확인
		$admin_serial = $this->session->userdata('admin_serial');
		$admin_level = $this->session->userdata('admin_level');

		if($admin_serial == 'S'){
			// 슈퍼관리자
			$admin_level = 3;
			$admin_name = "슈퍼관리자";
			$admin_id = "슈퍼관리자";
			$data['admin_function'] = $this->admin_function;
			$permission_count = count($data['admin_function']);

			$permission = "";
			for($i=0;$i<$permission_count;$i++){
				$permission .= "Y";
			}

			$newdata = array(
				'admin_level' => $admin_level,
				'admin_name' => $admin_name,
				'admin_id' => $admin_id,
				'admin_permission' => $permission
			);

			$this->session->set_userdata($newdata);
//			$this->session->set_userdata('admin_level', $admin_level); 
//			$this->session->set_userdata('admin_name', $admin_name); 		
		}
		else if($admin_serial >= '1'){
			// 일반관리자
			$sql = "
			SELECT 
				admin_information.*, company_information.company_name
			FROM 
				admin_information, company_information
			WHERE 
				admin_information.serial = '".$admin_serial."'
			and 
				admin_information.company_serial = company_information.serial
			";

			$result = $this->db->fReadSql($sql, '');

			$admin_level = $result[0]['admin_level'];
			$admin_id = $result[0]['admin_id'];
			$admin_name = $result[0]['admin_name'];
			$company_serial = $result[0]['company_serial'];
			$permission = $result[0]['permission'];
			$company_name = $result[0]['company_name'];

			$newdata = array(
				'admin_level' => $admin_level,
				'admin_name' => $admin_name,
				'admin_id' => $admin_id,
				'company_serial' => $company_serial,
				'company_name' => $company_name ,
				'admin_permission' => $permission
			);

			$this->session->set_userdata($newdata);
//			$this->session->set_userdata('admin_level', $admin_level); 
//			$this->session->set_userdata('admin_name', $admin_name); 
		}
		

		if(@$data)$this->set_data($data);

		switch ($admin_level) {
			case "3":
				// 슈퍼관리자
			case "2":
			case "1":
				// 상급 관리자
				$this->set_admin_name($admin_name);
				$data['admin_name'] = $this->get_admin_name();

				// 접속 국가 표시
				$sql = "
				SELECT 
					c.country,
					s.ip_address,
					s.timestamp
				FROM 
					ip2nationCountries c,
					ip2nation i,
					ci_sessions s
				WHERE 
					i.ip <= INET_ATON(s.ip_address) AND 
					c.code = i.country AND
					s.id = '".$session_id."'
				ORDER BY 
					i.ip DESC 
				LIMIT 0,1
				";
				$result = $this->db->fReadSql($sql, '');

				$data['connect_country'] = $result[0]['country'];
				$data['connect_ip'] = $result[0]['ip_address'];
				$data['connect_date'] = $result[0]['timestamp'];

				$data['permission'] = $permission;

				$this->set_data($data);

				break;
				// 하급 관리자
				break;

			default:
				// 관리자 레벨 확인 불능
				// 로그인 페이지 호출
				$data['login_id'] = $this->input->post('login_id');
				$data['login_pw'] = $this->input->post('login_pw');
				$data['redirect_uri'] = $this->input->post('redirect_uri');
				
				// 입력한 아이디, 패스워드를 관리자 아이디, 패스워드와 비교
				if($data['login_id'] && $data['login_pw']){

					if($data['login_id'] == "admin" && $data['login_pw'] == "123456ab"){
						// 관리자 레벨 3으로 설정
						$admin_serial = "S";
						$admin_level = 3;
						$admin_name = '슈퍼관리자';
					} 
					else if($data['login_id'] && $data['login_pw']){
						$sql = "
						SELECT 
							*
						FROM 
							admin_information
						WHERE
							admin_id = '".$data['login_id']."' AND
							admin_pw = password('".$data['login_pw']."')
						";
						$result = $this->db->fReadSql($sql, '');

						if($result[0]['serial'] >= '1')	{
							$admin_serial = $result[0]['serial'];
							$admin_level = $result[0]['admin_level'];
							$admin_name = $result[0]['admin_name'];
						}
					}
					
					if(@$admin_level >= '1')
					{
						$this->session->set_userdata('admin_serial', $admin_serial); 

						// 코드이그나이터 엑티브레코드 저장된 아이템 제거 및 테이블 칼럼 할당
						$this->db->flush_cache();
						$this->db->set('admin_serial', $admin_serial);
						$this->db->set('admin_id', $data['login_id']);
						$this->db->set('session_id', $session_id);
						$this->db->set('admin_level', $admin_level);
						$this->db->set('user_agent', $_SERVER["HTTP_USER_AGENT"]);
						$this->db->set('registered_date', 'now()', FALSE);
						$this->db->set('registered_ip', $_SERVER['REMOTE_ADDR']);
						$this->db->insert('access_log_admin'); 

//						redirect(getenv(REQUEST_URI));
						echo "OK1|".getenv(REQUEST_URI);
						die();
					}
					// 코드이그나이터 엑티브레코드 저장된 아이템 제거 및 테이블 칼럼 할당
					$this->db->flush_cache();
					$this->db->set('admin_serial', @$admin_serial);
					$this->db->set('admin_id', $data['login_id']);
					$this->db->set('session_id', $session_id);
					$this->db->set('admin_level', $admin_level);
					$this->db->set('user_agent', $_SERVER["HTTP_USER_AGENT"]);
					$this->db->set('registered_date', 'now()', FALSE);
					$this->db->set('registered_ip', $_SERVER['REMOTE_ADDR']);
					$this->db->insert('access_log_admin');
				}
				echo "아이디 또는 비밀번호를 확인해 주세요.";
//				redirect('/login/?redirect_uri='.($redirect_uri?$redirect_uri:getenv(REQUEST_URI)));
//				$this->template_file = 'admin/login_view';
//				echo $this->load->view($this->template_file, $data, true);
				die();

		}
	}

	function _remove_directory($dir) {
		if ($handle = opendir("$dir")) {
			while (false !== ($item = readdir($handle))) {
				if ($item != "." && $item != "..") {
					if (is_dir($dir."/".$item)) {
						$this->_remove_directory($dir."/".$item);
					} else {
						unlink($dir."/".$item);
					}
				}
			}
			closedir($handle);
			rmdir($dir);
		}
	}

}
?>