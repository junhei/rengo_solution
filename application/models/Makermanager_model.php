<?php
class Makermanager_model extends CI_Model {
 

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	//전체 차종 얻기
	function get_list()
	{
		$sql = "SELECT * FROM maker_master order by maker_index ASC";
		$result = $this->db->fReadSql($sql, '');
		return $result;
	}

	//전체 차종 얻기
	function get_active_list()
	{
		$sql = "SELECT * FROM maker_master where flag = 'Y' order by maker_index ASC";
		$result = $this->db->fReadSql($sql, '');
		return $result;
	}


	function get_car_count($maker_index)
	{
		$sql = "SELECT * FROM car_list WHERE maker_index=".$maker_index;
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	function add($data){
		$sql = "SELECT * FROM maker_master WHERE maker_name='".$data['maker_name']."'";
		$query = $this->db->query($sql);
		$result_count = $query->num_rows();

		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="Already exist.";		
		}else{
			$result = $this->db->insert('maker_master', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;

		
	}

	function update($data){

		$this->db->where('maker_index', $data['maker_index']);
		$result = $this->db->update('maker_master', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	function delete($id){

		$result = $this->db->delete('maker_master', array('maker_index' => $id));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}


}