<?php
class Mastermanager_model extends CI_Model {
 

	function __construct()
	{
		parent::__construct();

	}

	function get_admin_information() {
		$sql = "
		SELECT 
			serial,
			company_serial,
			admin_id,
			-- admin_pw,
			admin_level,
			admin_name,
			permission,
			company_serial,
			registered_date,
			registered_ip,
			edited_date,
			edited_ip
		FROM 
			admin_information
		ORDER BY 
			admin_id 
		";
		$result = $this->db->fReadSql($sql);
		return $result;
	}	

	function update_admin_information($data){
		// $this->db->flush_cache();
		$this->db->set('admin_id', $data['admin_id']);
		if($data['admin_pw']!=null && $data['admin_pw']!=''){
			$this->db->set('admin_pw', "password('".$data['admin_pw']."')", FALSE);
		}
		$this->db->set('admin_name', $data['admin_name']);
		$this->db->set('permission', $data['permission']);
		$this->db->set('company_serial', $data['company_serial']);
		// $this->db->set('registered_date', 'now()', FALSE);
		// $this->db->set('registered_ip', $_SERVER['REMOTE_ADDR']);
		$this->db->set('edited_date', 'now()', FALSE);
		$this->db->set('edited_ip', $_SERVER['REMOTE_ADDR']);
		$this->db->where('serial', $data['serial']);
		$result = $this->db->update('admin_information');

		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}

		return $response;
	}

}