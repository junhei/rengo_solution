<?php
class Car_model extends CI_Model {
 

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	//전체 차량 얻기(company, type별로 구분 가능)
	function get_list($company_serial, $type)
	{

		$sql = "SELECT maker_master.maker_name, maker_master.country, car_list.*, car_master.* FROM maker_master, car_list, car_master WHERE car_list.flag != 'D' AND car_list.car_index = car_master.car_index AND car_master.maker_index = maker_master.maker_index";

		if($company_serial!='' && $company_serial!=0 ){
			$sql = $sql." AND car_list.company_serial = ".$company_serial;
		}
		
		if($type != ''){
			$sql = $sql." AND car_master.car_type = '".urldecode($type)."'";
		}
		$result = $this->db->fReadSql($sql, '');
		return $result;
	}

	//특정기간 전체 차종 얻기
	function get_list_period($company_serial, $type, $period)
	{

		if(strlen($period) == "25"){
			$temp_period = explode("_", $period);
			$period_start = $temp_period['0'];
			$period_finish = $temp_period['1'];
			$예약간시간간격 = 4;

			if($company_serial >= '1'){
				$sql_company_serial = "
				AND car_list.company_serial = '".intval($company_serial)."'
				";
			}
			//해당 시간의 선택된 회사의 예약된 차량 구하기 
			$sql = "
			SELECT
				order_list.car_serial
			FROM
				order_list,
				car_list
			WHERE
				car_list.flag != 'D'
				and car_list.flag != 'M'
				and order_list.period_finish > '".date('YmdHi',strtotime($period_start.' -'.$예약간시간간격.' hours'))."' and order_list.period_start < '".date('YmdHi',strtotime($period_finish.' +'.$예약간시간간격.' hours'))."'
				and car_list.serial = order_list.car_serial 
				and order_list.flag != 'C' 
				and order_list.flag != 'D' 
			".$sql_company_serial."
			GROUP BY
				order_list.car_serial
			ORDER BY
				order_list.car_serial
			";
			$result_order_list = $this->db->fReadSql($sql, '');
			$count_order_list = count($result_order_list);
			for($i=0;$i<$count_order_list;$i++){
				$order_list[$result_order_list[$i]['car_serial']] = "Y";
			}
		}


		if($company_serial >= '1'){
			$sql_company_serial = " AND car_list.company_serial = ".$company_serial;
		}
		


		if(urldecode($type) != "전체"){
			$sql_type = " AND car_master.car_type = '".urldecode($type)."'";
		}

		$sql = "
		SELECT 
			maker_master.maker_name,
			maker_master.country,
			car_list.*,
			car_master.*,
			car_list.serial
		FROM
			maker_master,
			car_list,
			car_master
		WHERE
			car_list.car_index = car_master.car_index
			AND car_list.car_status = '정상' 
			AND car_master.maker_index = maker_master.maker_index
			".$sql_company_serial."
			".$sql_type."
		ORDER BY
			car_master.car_name_detail ASC";
		$result = $this->db->fReadSql($sql, '');
		$count_result = count($result);
		for($i=0;$i<$count_result;$i++){
			if($order_list[$result[$i]['serial']] == "Y"){
				continue;
			}
			else $result_new[] = $result[$i];
		}
		return $result_new;
	}


	//확인내역
	function get_check_list($car_serial)
	{
		$sql = "SELECT * FROM car_check_history where car_serial =".$car_serial;
		$result = $this->db->fReadSql($sql, '');
		if(count($result)>0){
			return $result;
		}else{
			return array();
		}
	}
	//소모품 교환내역
	function get_supply_list($car_serial)
	{
		$sql = "SELECT * FROM car_supply_history where car_serial =".$car_serial;
		$result = $this->db->fReadSql($sql, '');
		if(count($result)>0){
			return $result;
		}else{
			return array();
		}
	}

	//응급추동
	function get_emergency_list($car_serial)
	{
		$sql = "SELECT * FROM car_emergency_history where car_serial =".$car_serial;
		$result = $this->db->fReadSql($sql, '');
		if(count($result)>0){
			return $result;
		}else{
			return array();
		}
	}

	//수리내역
	function get_repair_list($car_serial)
	{
		$sql = "SELECT * FROM car_repair_history where car_serial =".$car_serial;
		$result = $this->db->fReadSql($sql, '');
		if(count($result)>0){
			return $result;
		}else{
			return array();
		}
	}

	//사고내역
	function get_accident_list($car_serial)
	{
		$sql = "SELECT * FROM car_accident_history where car_serial =".$car_serial;
		$result = $this->db->fReadSql($sql, '');
		if(count($result)>0){
			return $result;
		}else{
			return array();
		}
	}

	//렌고이용차량 댓수
	function get_rengo_count($company_serial = '')
	{
		if($company_serial >= '1'){
			$sql_company_serial = "
			AND company_serial = '".$company_serial."'
";
		}

		$sql = "SELECT count(*) as cnt FROM car_list where flag = 'Y'
		".$sql_company_serial."
		";
		$result = $this->db->fReadSql($sql, '');
		return $result['0']['cnt'];
	}

	function get_count($company_serial = 0, $type)
	{


			// $type = urldecode($input_type);
			$sql = "SELECT car_list.serial FROM car_list, maker_master, car_master WHERE car_list.flag != 'D' AND car_list.car_index = car_master.car_index AND car_master.maker_index = maker_master.maker_index";

			if($company_serial!='' && $company_serial!=0 ){
				$sql = $sql." AND car_list.company_serial = ".$company_serial;
			}

			switch($type){
				case "전체":
						
						break;
				case "미운행":
				case "정상":
				case "검사중":
				case "수리중":
				case "폐차":
				case "매각":
						$sql = $sql." AND car_status='".$type."'";
						break;
				case "자차포함":
						$sql = $sql." AND insurance_self_flag='Y'";
						break;
				case "자차없음":
						$sql = $sql." AND insurance_self_flag='N'";
						break;
				case "age21":
						$sql = $sql." AND requirement_age=21";
						break;
				case "age26":
						$sql = $sql." AND requirement_age=26";
						break;
				case "allage":
						$sql = $sql." AND requirement_age!=21 AND requirement_age!=26";
						break;
				// case "본사":
				// 		$sql = "SELECT car_list.serial FROM car_list, maker_master, car_master WHERE car_list.car_index = car_master.car_index AND car_master.maker_index = maker_master.maker_index";
				// 		if($company_serial!=0 && $company_serial!=''){
				// 				$sql = $sql." AND company_serial =".$company_serial;
				// 		}
				// 		$sql = $sql." AND branch_serial = 0";
				// 		break;
				// case "지점":
				// 		$sql = "SELECT car_list.serial FROM car_list, maker_master, car_master WHERE car_list.car_index = car_master.car_index AND car_master.maker_index = maker_master.maker_index";
				// 		if($company_serial!=0 && $company_serial!=''){
				// 				$sql = $sql." AND company_serial =".$company_serial;
				// 		}
				// 		$sql = $sql." AND branch_serial != 0";
						break;
			}

			$query = $this->db->query($sql);
			return $query->num_rows();

		
		
	}
	//차량 등록
	function add_car($data){
		$sql = "SELECT * FROM car_list WHERE car_number='".$data['car_number']."' ";
		$query = $this->db->query($sql);
		$result_count = $query->num_rows();

		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록 되어 있는 차량 번호입니다.";		
		}else{

			$this->db->flush_cache();
			//등록 날짜 및 사용자 저장

			$this->db->set('registered_date', 'now()', FALSE);
			// $this->db->set('registered_ip', $_SERVER['REMOTE_ADDR'] , FALSE);
			
			$result = $this->db->insert('car_list', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$error = $this->db->error();
				$response['code'] ="E01";
				$response['message'] =$error['message'];
			}
		}
		return $response;
	}

	//차량 수정
	function update_car($data){


		$this->db->flush_cache();
			//등록 날짜 및 사용자 저장

		$this->db->set('edited_date', 'now()', FALSE);
			// $this->db->set('registered_ip', $_SERVER['REMOTE_ADDR'] , FALSE);
			
		$this->db->where('serial', $data['serial']);
		$result = $this->db->update('car_list', $data);
		if($result){
				$response['code'] ="S01";
		}else{
				$error = $this->db->error();
				$response['code'] ="E01";
				$response['message'] =$error['message'];
		}
		
		return $response;
	}

	//차량 삭제
	function delete_car($id){

		$this->db->flush_cache();
		$this->db->where('serial', $id);
		$this->db->set('edited_date', 'now()', FALSE);
		$data = array(
        'flag' => 'D',
		);
		$result = $this->db->update('car_list', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	//점검 내역 추가
	function add_check($data){
		$sql = "SELECT * FROM car_check_history WHERE car_serial=".$data['car_serial']." AND type='".$data['type']."' AND check_date='".$data['check_date']."'";
		$query = $this->db->query($sql);
		$result_count = $query->num_rows();

		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록 되어 있습니다.";		
		}else{
			$result = $this->db->insert('car_check_history', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;
	}
	//점검 내용 수정
	function update_check($search_id, $data){

		$this->db->where('serial', $search_id);
		$result = $this->db->update('car_check_history', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
		
	}
	//점검 내역 삭제
	function delete_check($id){

		$result = $this->db->delete('car_check_history', array('serial' => $id));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	//소모품교환 추가
	function add_supply($data){
		$sql = "SELECT * FROM car_supply_history WHERE car_serial=".$data['car_serial']." AND type='".$data['type']."' AND check_date='".$data['check_date']."'";
		$query = $this->db->query($sql);
		$result_count = $query->num_rows();

		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록 되어 있습니다.";		
		}else{
			$result = $this->db->insert('car_supply_history', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;
	}
	//소모품교환 수정
	function update_supply($search_id, $data){

		$this->db->where('serial', $search_id);
		$result = $this->db->update('car_supply_history', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
		
	}

	//소모품교환 삭제
	function delete_supply($id){

		$result = $this->db->delete('car_supply_history', array('serial' => $id));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	//사고내역 추가
	function add_accident($data){
		$sql = "SELECT * FROM car_accident_history WHERE car_serial=".$data['car_serial']." AND accident_date='".$data['accident_date']."'";
		$query = $this->db->query($sql);
		$result_count = $query->num_rows();

		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록 되어 있습니다.";		
		}else{
			$result = $this->db->insert('car_accident_history', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;
	}
	//사고내역 수정
	function update_accident($search_id, $data){

		$this->db->where('serial', $search_id);
		$result = $this->db->update('car_accident_history', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
		
	}

	//사고내역 삭제
	function delete_accident($id){

		$result = $this->db->delete('car_accident_history', array('serial' => $id));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	//정비내역 추가
	function add_repair($data){
		$sql = "SELECT * FROM car_repair_history WHERE car_serial=".$data['car_serial']." AND type='".$data['type']."' AND repair_start_date='".$data['repair_start_date']."' AND repair_end_date='".$data['repair_end_date']."'";
		$query = $this->db->query($sql);
		$result_count = $query->num_rows();

		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록 되어 있습니다.";		
		}else{
			$result = $this->db->insert('car_repair_history', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;
	}
	//정비내역 수정
	function update_repair($search_id, $data){

		$this->db->where('serial', $search_id);
		$result = $this->db->update('car_repair_history', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
		
	}

	//정비내역 삭제
	function delete_repair($id){

		$result = $this->db->delete('car_repair_history', array('serial' => $id));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}

	//출동 추가
		function add_emergency($data){
		
		$result = $this->db->insert('car_emergency_history', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}
	//출동 수정
	function update_emergency($search_id, $data){

		$this->db->where('serial', $search_id);
		$result = $this->db->update('car_emergency_history', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
		
	}

	//출동 삭제
	function delete_emergency($id){

		$result = $this->db->delete('car_emergency_history', array('serial' => $id));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}


	//rent에서 특정 차의 정보를 얻을때 사용
	function get_car_information($company_serial, $car_serial){
		if($company_serial >= '1' && $car_serial >= '1'){
			$sql = "select 
					*
					from car_list 
					where
					company_serial = '".$company_serial."'  
					and serial <= '".$car_serial."' 
			";

			$result = $this->db->fReadSql($sql, '');
			return $result;
		} else {
			return false;
		}

	}


	function m_get_list($company_serial, $type)
	{
		try{
			$sql = "
			SELECT 
			M.maker_name, M.country, 
			C.car_type, C.car_name_detail, C.car_people, C.car_name,
			L.company_serial, L.flag, L.car_status, L.car_index, L.smoking, L.car_number, L.color, L.gear_type, L.fuel_option, L.requirement_age, L.option_navi, L.option_bluetooth, L.option_automatic_back_mirror, L.option_hi_pass, L.option_aux, L.option_heatseat, L.option_gps, L.option_rear_sensor, L.option_blackbox, L.option_cdplayer, L.option_backcamera, L.option_sunroof, L.option_lane_departure_warning, L.option_automatic_seat, L.option_front_sensor, L.option_smart_key, L.option_av_system, L.option_ecm, L.option_license1, L.option_week_48, L.option_weekend_48, L.insurance_man1_flag, L.insurance_man2_flag, L.insurance_object_flag, L.insurance_children_flag, L.insurance_self_flag, L.insurance_self2_flag,
				L.insurance_self_price, L.insurance_self2_price, L.insurance_man1_compensation, L.insurance_man2_compensation, L.insurance_object_compensation, L.insurance_children_compensation, L.insurance_self_compensation, L.insurance_self2_compensation, L.insurance_man1_customer, L.insurance_man2_customer, L.insurance_object_customer, L.insurance_children_customer, L.insurance_self_customer, L.insurance_self2_customer, L.week_price, L.weekend_price   
			FROM maker_master AS M, car_list AS L, car_master AS C 
			WHERE 
			L.car_index = C.car_index 
			AND C.maker_index = M.maker_index";

			if($company_serial!='' && $company_serial!=0 ){
				$sql = $sql." AND L.company_serial = ".$company_serial;
			}
			
			if($type != ''){
				$sql = $sql." AND C.car_type = '".urldecode($type)."'";
			}

			$result = $this->db->fReadSql($sql);
			if(count($result)==0){
				$result = array();
				$response['code'] = 'S01';
				$response['value'] = $result;
				return $response;
			}else{
				//6시간, 1시간 가격 구해서 더해서 보내준다.
				$sql2 = "
				SELECT week_6, week_1, weekend_6, weekend_1
				FROM basic_price
				WHERE flag = 'Y' AND
				company_serial ='".$company_serial."'";
				$price_result = $this->db->fReadSql($sql2);
				if(count($price_result)>0){
					//차량마다 6시간, 1시간 요금을 구해서 항목에 더해준다.
					for($i=0; $i<count($result); $i++){
						$price_week_6 = intval($result[$i]['week_price']) * intval($price_result[0]['week_6']) / 100;
						$price_week_1 = intval($result[$i]['week_price']) * intval($price_result[0]['week_1']) / 100;
						$price_weekend_6 = intval($result[$i]['weekend_price']) * intval($price_result[0]['weekend_6']) / 100;
						$price_weekend_1 = intval($result[$i]['weekend_price']) * intval($price_result[0]['weekend_1']) / 100;

						$result[$i]['week_price_6'] = $price_week_6.'';
						$result[$i]['week_price_1'] = $price_week_1.'';
						$result[$i]['weekend_price_6'] = $price_weekend_6.'';
						$result[$i]['weekend_price_1'] = $price_weekend_1.'';
					}
				}else{
					for($i=0; $i<count($result); $i++){
						$result[$i]['week_price_6'] = '0';
						$result[$i]['week_price_1'] = '0';
						$result[$i]['weekend_price_6'] = '0';
						$result[$i]['weekend_price_1'] = '0';
					}

				}

				$response['code'] = 'S01';
				$response['value'] = $result;
				return $response;
			}
			
		}catch (Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
    		return $response;
		}
	}

}