<?php
class Carmaster_model extends CI_Model {
 

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

	}
	//차종 가져오기
	function get($id){
		$sql = "SELECT car_master.*, maker_master.maker_name FROM car_master, maker_master WHERE car_master.maker_index = maker_master.maker_index AND car_master.car_index=".$id;
		$result = $this->db->fReadSql($sql, '');
		if(count($result) > 0){
			return $result;
		}else{
			return array();
		}

	}

	//전체 차종 얻기
	function get_list()
	{
		$sql = "SELECT 
				car_master.*, maker_master.maker_name 
				FROM car_master, maker_master 
				WHERE car_master.maker_index = maker_master.maker_index
				AND car_master.flag = 'Y'";
		$result = $this->db->fReadSql($sql, '');
		if(count($result) > 0){
			return $result;
		}else{
			return array();
		}
	}

	//제조사에 해당하는 차량 모델 가져오기
	function get_model_list_by_maker($id)
	{
		$sql = "SELECT DISTINCT car_name FROM car_master WHERE maker_index = ".$id;
		$result = $this->db->fReadSql($sql, '');
		if(count($result) > 0){
			return $result;
		}else{
			return array();
		}
		
	}
	//차량 이름으로 가져오기
	function get_detail_list_by_name($name)
	{
		$sql = "SELECT * FROM car_master WHERE car_name = '".$name."'";
		$result = $this->db->fReadSql($sql, '');
		if(count($result) > 0){
			return $result;
		}else{
			return array();
		}
	}

	//해당 차종으로 동록된 차량의 수 가져오기
	function get_car_count($maset_index)
	{
		$sql = "SELECT * FROM car_list WHERE car_index=".$car_index;
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	//차종 등록
	function add($data){
		$sql = "SELECT * FROM car_master WHERE car_name='".$data['car_name']."'";


		$query = $this->db->query($sql);
		$result_count = $query->num_rows();
// return $result_count;
		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록 된 차 입니다.";		
		}else{
			$result = $this->db->insert('car_master', $data);
			if($result){
				$response['code'] ="S01";
				$response['car_index'] = $this->db->insert_id();

				// $response['car_index'] ="S01";
				
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;

		
	}
	//추종 수정
	function update($car_index, $data){

		$this->db->where('car_index', $car_index);
		$result = $this->db->update('car_master', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$response['message'] ="데이터 수정 중에 에러가 발생했습니다.";
		}
		return $response;
	}
	//차종 삭제
	function delete($id){

		$this->db->where('car_index', $id);
		$data = array('flag' => 'N');
		$result = $this->db->update('car_master', $data);

		// $result = $this->db->delete('car_master', array('car_index' => $id));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$response['message'] ="데이터 삭제 중에 에러가 발생했습니다.";
		}
		return $response;
	}

	function save_image_file($data){
		$this->db->flush_cache();
		if($data['car_list_image']){
			$this->db->set('car_list_image_file', $data['car_list_image']);
		}
		if($data['car_detail_image']){
			$this->db->set('car_detail_image_file', $data['car_detail_image']);
		}
		$this->db->where('car_index', $data['serial']);
		//1. company 정보 먼저 update
		$this->db->update('car_master');
	}


}