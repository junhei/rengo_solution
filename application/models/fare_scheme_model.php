<?php
class Fare_scheme_model extends CI_Model {
 

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

	}

	//전체 차종 얻기
	function get_basic_price($id)
	{
		$sql = "SELECT * FROM basic_price WHERE flag='Y' and company_serial=".$id;
		$result = $this->db->fReadSql($sql, '');
		return $result;
	}

	function get_period_price($id)
	{
		$sql = "SELECT * FROM period_price WHERE flag='Y' and company_serial=".$id;
		$result = $this->db->fReadSql($sql, '');
		return $result;
	}

	function get_special_price($id)
	{
		$sql = "SELECT * FROM special_price WHERE flag='Y' and company_serial=".$id;
		$result = $this->db->fReadSql($sql, '');
		return $result;
	}

	// function get_model_list_by_maker($id)
	// {
	// 	$sql = "SELECT DISTINCT car_name FROM car_master WHERE maker_index = ".$id;
	// 	$result = $this->db->fReadSql($sql, '');
	// 	return $result;
	// }

	// function get_detail_list_by_name($name)
	// {
	// 	$sql = "SELECT * FROM car_master WHERE car_name = '".$name."'";
	// 	$result = $this->db->fReadSql($sql, '');
	// 	return $result;
	// }


	// function get_car_count($maset_index)
	// {
	// 	$sql = "SELECT * FROM car_list WHERE car_index=".$car_index;
	// 	$query = $this->db->query($sql);
	// 	return $query->num_rows();
	// }


	function basic_price_add($data){
		$sql = "SELECT * FROM basic_price WHERE flag = 'Y' and company_serial='".$data['company_serial']."'";


		$query = $this->db->query($sql);
		$result_count = $query->num_rows();
// return $result_count;
		if($result_count>0){
			$this->db->trans_start();
			$sql = "UPDATE basic_price SET flag = 'N', deleted_date = now() WHERE flag = 'Y' AND company_serial='".$data['company_serial']."'";
			$this->db->query($sql);
			$sql = "INSERT INTO basic_price (company_serial, week_6, week_1, weekend_6, weekend_1, flag, registered_date) VALUES ('".$data['company_serial']."', '".$data['week_6']."' , '".$data['week_1']."' , '".$data['weekend_6']."' , '".$data['weekend_1']."' , 'Y', now() )";
			$this->db->query($sql);
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
			    $response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}else{
				$response['code'] ="S01";
			}
		}else{
			$result = $this->db->insert('basic_price', $data);
			if($result){
				$response['code'] ="S01";
				// $response['company_serial'] = $this->db->insert_id();
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;

		
	}
	function period_price_add($data){
		$sql = "SELECT * FROM period_price WHERE flag='Y' AND company_serial='".$data['company_serial']."' AND days = '".$data['days']."'";


		$query = $this->db->query($sql);
		$result_count = $query->num_rows();
		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록되어 있는 기간입니다.";		
		}else{
			$result = $this->db->insert('period_price', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;

		
	}
	function special_price_add($data){
		$sql = "SELECT * FROM special_price WHERE flag = 'Y' AND company_serial='".$data['company_serial']."' AND end_date >= '".$data['start_date']."' and start_date <= '".$data['end_date']."'";


		$query = $this->db->query($sql);
		$result_count = $query->num_rows();
		
		if($result_count > 0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록되어 있는 날짜 입니다.";		
		}else{
			$result = $this->db->insert('special_price', $data);
			if($result){
				$response['code'] ="S01";	
			}else{
				$response['code'] ="E01";
				$error = $this->db->error();
				$response['message'] = $error['message'];
			}
		}
		return $response;

		
	}

	function update($car_index, $data){

		$this->db->where('car_index', $car_index);
		$result = $this->db->update('car_master', $data);
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$response['message'] ="데이터 수정 중에 에러가 발생했습니다.";
		}
		return $response;
	}

	function basic_price_delete($id){

		$result = $this->db->delete('basic_price', array('company_serial' => $id));
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$response['message'] ="데이터 삭제 중에 에러가 발생했습니다.";
		}
		return $response;
	}

	function period_price_delete($data){
		
		
		$this->db->flush_cache();
		// $this->db->where('company_serial', $data['company_serial']);
		$this->db->where('serial', $data['serial']);

		$this->db->set('deleted_date', 'now()', FALSE);
		$this->db->set('flag', 'N');
		
		$result = $this->db->update('period_price');
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$response['message'] ="데이터 삭제 중에 에러가 발생했습니다.";
		}
		return $response;
	}

	function special_price_delete($data){
		$this->db->flush_cache();
		// $this->db->where('company_serial', $data['company_serial']);
		$this->db->where('serial', $data['serial']);
		
		$this->db->set('deleted_date', 'now()', FALSE);
		$this->db->set('flag', 'N');

		$result = $this->db->update('special_price');
		if($result){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$response['message'] ="데이터 삭제 중에 에러가 발생했습니다.";
		}
		return $response;
	}

	// function basic_price_update($data){

	// 	$this->db->trans_start();
	// 	$sql = "UPDATE basic_price SET flag = 'N', deleted_date = now() WHERE  company_serial=".$data['company_serial'];
	// 	$this->db->query($sql);
	// 	$sql = "INSERT INTO basic_price (company_serial, branch_serial, week_24, week_6, week_1, weekend_24, weekend_6, weekend_1, flag, registered_date) VALUES ('".$data['company_serial']."' , '".$data['branch_serial']."' , '".$data['week_24']."', '".$data['week_6']."' , '".$data['week_1']."' , '".$data['weekend_24']."' , '".$data['weekend_6']."' , '".$data['weekend_1']."' ,'Y', now() )";
	// 	$this->db->query($sql);
	// 	$this->db->trans_complete();

	// 	if ($this->db->trans_status() === FALSE)
	// 	{
	// 	    $response['code'] ="E01";
	// 		$error = $this->db->error();
	// 		$response['message'] = $error['message'];
	// 	}else{
	// 		$response['code'] ="S01";
	// 	}
	// 	return $response;
	// }

	function period_price_update($serial, $data){

		$this->db->trans_start();
		$sql = "UPDATE period_price SET flag = 'N', deleted_date = now() WHERE serial=".$serial;
		$this->db->query($sql);
		$sql = "INSERT INTO period_price (company_serial, days, percent, flag, registered_date) VALUES ('".$data['company_serial']."', '".$data['days']."' , '".$data['percent']."' , 'Y', now() )";
		$this->db->query($sql);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
		    $response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}else{
			$response['code'] ="S01";
		}
		return $response;
	}
	
	function special_price_update($serial, $data){

		$this->db->trans_start();
		$sql = "UPDATE special_price SET flag = 'N', deleted_date = now() WHERE serial=".$serial;
		$this->db->query($sql);
		$sql = "INSERT INTO special_price (company_serial, start_date, end_date, memo, percent, flag, registered_date) VALUES ('".$data['company_serial']."', '".$data['start_date']."' , '".$data['end_date']."' , '".$data['memo']."' , '".$data['percent']."' , 'Y', now() )";
		$this->db->query($sql);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
		    $response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}else{
			$response['code'] ="S01";
		}
		return $response;
	}
}
