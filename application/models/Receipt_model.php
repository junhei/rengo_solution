<?php

class Receipt_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function get_order_data($id){
		$sql = "SELECT * FROM order_list WHERE serial=".$id;
		$result = $this->db->fReadSql($sql, '');
		return $result;
	}

	function get_receipt_data($id){
		if($id==0){
			return array();
		}
		$sql = "SELECT * FROM receipt_history WHERE flag != 'N' AND order_serial=".$id;
		$result = $this->db->fReadSql($sql, '');
		if(count($result)==0){
			return array();
		}
		return $result;
	}

	//차량 등록
	function add_receipt($data){
		$sql = "SELECT * FROM receipt_history WHERE order_serial=".$data['order_serial']." AND flag='Y' AND type='".$data['type']."' AND deposite_way='".$data['deposite_way']."'";
		$query = $this->db->query($sql);
		$result_count = $query->num_rows();

		if($result_count>0){
				$response['code'] ="E02";
				$response['message'] ="이미 등록 되어 있습니다.";		
		}else{

			$this->db->flush_cache();
			//등록 날짜 및 사용자 저장

			$this->db->set('deposite_registration_date', 'now()', FALSE);
			// $this->db->set('registered_ip', $_SERVER['REMOTE_ADDR'] , FALSE);
			

			$result = $this->db->insert('receipt_history', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$error = $this->db->error();
				$response['code'] ="E01";
				$response['message'] =$error['message'];
			}
		}
		return $response;
	}

	//차량 등록
	function update_receipt($serial, $data){

		$this->db->where('serial', $serial);
		$result = $this->db->update('receipt_history', $data);
		$result_count = $this->db->affected_rows();
		if($result_count > 0){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}
	function delete_receipt($serial){

		$this->db->where('serial', $serial);
		$data = array(
			'flag' => 'N'
			);
		$result = $this->db->update('receipt_history', $data);
		$result_count = $this->db->affected_rows();
		if($result_count > 0){
			$response['code'] ="S01";
		}else{
			$response['code'] ="E01";
			$error = $this->db->error();
			$response['message'] = $error['message'];
		}
		return $response;
	}


	function get_deposite_ways($company_serial){
		$sql = "
		SELECT *
		FROM deposite_way
		WHERE flag = 'Y'
		AND ( company_serial = 0 or company_serial ='".$company_serial."' )";
		return $this->db->fReadSql($sql);
	}
	function add_deposite_way($data){
		$sql = "
		SELECT *
		FROM deposite_way
		WHERE flag = 'Y'
		AND name = '".$data['name']."' 
		AND company_serial = '".$data['company_serial']."'";
		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			$response['code'] ="E02";
			$response['message'] ="이미 등록 되어 있습니다.";	
			return $response;
		}else{
			$result = $this->db->insert('deposite_way', $data);
			if($result){
				$response['code'] ="S01";
			}else{
				$error = $this->db->error();
				$response['code'] ="E01";
				$response['message'] =$error['message'];
			}
			return $response;
		}


	}
	function delete_deposite_way($serial){
		$sql ="
			UPDATE
				deposite_way
			SET
				flag = 'N'
			WHERE
				serial = '".$serial."'
			";
			$result = $this->db->query($sql);
			if($result){
				$response['code'] ="S01";
			}else{
				$error = $this->db->error();
				$response['code'] ="E01";
				$response['message'] =$error['message'];
			}
			return $response;
	}
}

?>