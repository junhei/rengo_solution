<script>
 var btn_change_check_history = $("#btn_change_check_history"); //검사 내역 저장버튼

 
// 점검 내역 다이얼로그 저장 버튼
btn_change_check_history.click(function() {

	//선택된 차정보
	var carGrid = $("#grid").data("kendoGrid");
	var row = carGrid.select();
	var carData = carGrid.dataItem(row);

	//선택된 내역정보
	var historyGrid = $("#grid_history").data("kendoGrid");
	var historyRow = historyGrid.select();
	var historyData = historyGrid.dataItem(historyRow);

	//나머지 다이얼로그 정보
	var checkDate = $('#datetimepicker').data('date');

	var checkedYaer = checkDate.substring(0,4);
	var checkedMonth = checkDate.substring(6,8);
	var checkedDay = checkDate.substring(10,12);

	alert(checkedMonth);

	var selectDate = new Date(checkedYaer+"-"+checkedMonth+"-"+checkedDay); 
	alert(selectDate);
	var type = $('#check_select_type option:selected').val();
	var period = $('#check_select_period option:selected').val(); 

	//주기 글자 길이 구해서 길면 개월, 짧으면 년
	if(period.length==3){
		var month = parseInt(period.substring(0,1));
		selectDate.setMonth(selectDate.getMonth() + month);
	}else{
		var month = parseInt(period.substring(0,1))*12;
		selectDate.setMonth(selectDate.getMonth() + month);
	}


	
	var pay = $('#text_pay').val();
	var result = $('#text_result').val();
// (selectDate.getMonth() + 1) + "-" + selectDate.getDate() + "-" + selectDate.getFullYear()

	//등록이면
	var text_dialog_title = $('#check_history_dialog_title').text();

	if(text_dialog_title == "점검 내역 등록"){	

		if(checkDate==null){
			alert("점검 날짜는 꼭 입력해야 합니다.");
			return false;
		}	
		$.post("http://solution.rengo.co.kr/carmanager/register_check_history",
        	{
        			car_serial : carData.serial,
        			type : type,
					period : period,
					check_date : checkedYaer+"-"+checkedMonth+"-"+checkedDay ,
					next_check_date : selectDate.getFullYear()+"-"+zeroPad((selectDate.getMonth() + 1),2) +"-" + zeroPad(selectDate.getDate(),2),
					pay : pay,
					result :  result
			},
			function(data, status){
					var result = JSON.parse(data);
					if(result.code=="S01"){
						alert("저장되었습니다.");
						$("#check_history_close_btn").trigger({ type: "click" });
						//데이터 다시 읽어옴
						refresh_check_history();    
					}else if(result.code=="E02"){
						alert("이미 같은 내역이 같은 날짜에 등록되어 있습니다.");
					}else{
						alert("데이터 저장 중 에러가 발생했습니다.");
					}

		});
	}else if(text_dialog_title == "점검 내역 수정"){//수정이면
		alert(selectDate);
		alert(historyData.index+"\n"+carData.serial+"\n"+type+"\n"+period+"\n" + checkedYaer+"-"+checkedMonth+"-"+checkedDay+"\n"+  selectDate.getFullYear()+"-"+zeroPad((selectDate.getMonth() + 1),2) +"-" + selectDate.getDate());
		$.post("http://solution.rengo.co.kr/carmanager/update_check_history",
        	{
        			serial : historyData.index,
        			car_serial : carData.serial,
        			type : type,
					period : period,
					check_date : checkedYaer+"-"+checkedMonth+"-"+checkedDay,
					next_check_date : selectDate.getFullYear()+"-"+zeroPad((selectDate.getMonth() + 1),2) +"-" + zeroPad(selectDate.getDate(),2),
					pay : pay,
					result :  result
			},
			function(data, status){
					// alert(data);
					var result = JSON.parse(data);
					if(result.code=="S01"){
						alert("수정되었습니다.");
						$("#check_history_close_btn").trigger({ type: "click" });
						//데이터 다시 읽어옴
						refresh_check_history();    
					}else{
						alert(result.message);
					}

		});
	}else{
		alert('에러');
	}
	
});
</script>