<script>

$(document).ready(function() {

			//히스토리 

	 $("#grid_history").delegate("tbody>tr", "dblclick", 
	 	function(){

	 			//grid에서 id 얻어오기
			var grid = $("#grid_history").data("kendoGrid");
			var row = grid.select();
			var data = grid.dataItem(row);

	 		is_history_register = false;

	 		switch(selected_tab){
	 			case 1:
	 					
	 					$('#history_check_dialog').modal('toggle'); //점검 내역 팝업
	 					$('#check_history_delete_btn').show(); //삭제 버튼 보이기
	 					$('#check_history_dialog_title').text("점검 내역 수정");
	 					$('#check_history_save_btn').text("수정");

	 					//데이터 가져와서 값 대입
						// var checkDate = new Date(data.check_date);
						// var nextCheckDate = new Date(data.next_check_date);

						$('#check_date').datepicker().data('datepicker').selectDate(data.check_date);
						
						// $('#datetimepicker').attr('data-date', checkDate.getFullYear()+"년 "+ zeroPad((checkDate.getMonth() + 1),2) + "월 " + zeroPad(checkDate.getDate(),2) + "일");
						// $('#datetimepicker').val(checkDate.getFullYear()+"년 "+ zeroPad((checkDate.getMonth() + 1),2) + "월 " + zeroPad(checkDate.getDate(),2) + "일");
						// $('#datetimepicker2').attr('data-date', nextCheckDate.getFullYear()+"년 "+ zeroPad((nextCheckDate.getMonth() + 1),2) + "월 " + zeroPad(nextCheckDate.getDate(),2) + "일");
						// $('#datetimepicker2').val(nextCheckDate.getFullYear()+"년 "+zeroPad((nextCheckDate.getMonth() + 1),2) + "월 " + zeroPad(nextCheckDate.getDate(),2) + "일");
						$('#text_result').val(data.result);
						$('#text_pay').val(data.pay);
						// alert(data.type+","+data.period);
						$("#check_select_type").val(data.type).attr("selected", "selected");
						$("#check_select_period").val(data.period).attr("selected", "selected");
	 					break;
	 			case 2:
	 					$('#history_supply_dialog').modal('toggle'); //점검 내역 팝업
	 					$('#supply_history_delete_btn').show(); //삭제 버튼 보이기
	 					$('#supply_history_dialog_title').text("소모품교환 내역 수정");
	 					$('#supply_history_save_btn').text("수정");

	 					//데이터 가져와서 값 대임
	 					$('#supply_date').datepicker().data('datepicker').selectDate(data.check_date);
	 					$('#supply_input_name').val(data.type);
						// $('#supply_input_period').val(data.period);
						$('#supply_input_final').val(data.check_distance);
						// $('#supply_input_next').val(data.next_check_distance);
						$('#supply_input_pay').val(data.pay);
						$('#supply_input_memo').val(data.memo);	
						// $('#history_supply_dialog').modal();  
	 					break;
	 			case 3:
	 					$('#history_accident_dialog').modal('toggle'); //점검 내역 팝업
	 					$('#accident_delete_btn').show(); //삭제 버튼 보이기
	 					$('#accident_history_dialog_title').text("사고 내역 수정");
	 					$('#accident_save_btn').text("수정");

	 					var carGrid = $("#grid").data("kendoGrid");
						var carRow = carGrid.select();
						var carData = carGrid.dataItem(carRow);

	 					//값대입
						$('#car_accident_customer_car_number').val(carData.car_number);
						$('#car_accident_customer_car_model').val(carData.car_name_detail);

						if(data.status=="접수"){
								$('#accident_status_apply').prop("checked", true);
						}else if(data.status=="완결"){
								$('#accident_status_finish').prop("checked", true);
						}else if(data.status=="보류"){
								$('#accident_status_append').prop("checked", true);
						}
						$('#accident_time').datepicker().data('datepicker').selectDate(data.accident_date);
						// var accident_date = new Date(data.accident_date);
						// $('#accident_time').attr('data-date', accident_date.getFullYear()+"년 "+ zeroPad((accident_date.getMonth() + 1),2) + "월 " + zeroPad(accident_date.getDate(),2) + "일");
	 				// 	$('#car_accident_time_input').val(accident_date.getFullYear()+"년 "+ zeroPad((accident_date.getMonth() + 1),2) + "월 " + zeroPad(accident_date.getDate(),2) + "일 " + zeroPad(accident_date.getHours(),2)+"시 " + zeroPad(accident_date.getMinutes(),2))+"분";
						$('#car_accident_place_input').val(data.accident_place);
						$('#car_accident_customer_name').val(data.customer_name);
						$('#car_accident_customer_company').val(data.customer_company);

						$('#car_accident_customer_phnoe').val(data.customer_phone);
						$('#car_accident_customer_mobile').val(data.customer_mobile);
						$('#car_accident_customer_percent').val(data.customer_percent);
						$('#car_accident_customer_compensation').val(data.customer_compensation);
						$('#car_accident_customer_insurance_company').val(data.customer_insurance_company).prop("selected", true);
						$('#car_accident_customer_register_number').val(data.customer_register_number);
						$('#car_accident_customer_insurance_manager').val(data.customer_insurance_man);
						$('#car_accident_customer_insurance_phone').val(data.customer_insurance_phone);
						$('#car_accident_customer_insurance_fax').val(data.customer_insurance_fax);
						$('#car_accident_customer_insurance_repair').val(data.customer_repair);
						$('#car_accident_customer_pay').val(data.customer_pay);
						$('#accident_insurance_date').datepicker().data('datepicker').selectDate(data.customer_insurance_date);
						// var checkDate = new Date(data.customer_insurance_date);
						// $('#car_accident_customer_insurance_date').attr('data-date', checkDate.getFullYear()+"년 "+ zeroPad((checkDate.getMonth() + 1),2) + "월 " + zeroPad(checkDate.getDate(),2) + "일");
	 				// 	$('#car_accident_customer_insurance_date_text').val(checkDate.getFullYear()+"년 "+ zeroPad((checkDate.getMonth() + 1),2) + "월 " + zeroPad(checkDate.getDate(),2) + "일");
						$('#car_accident_customer_memo').val(data.customer_memo);

						$('#car_accident_target_car_number').val(data.target_car_number);
						$('#car_accident_target_car_model').val(data.target_car_model);
						$('#car_accident_target_name').val(data.target_name);
						$('#car_accident_target_company').val(data.target_company);
						$('#car_accident_target_phnoe').val(data.target_phone);
						$('#car_accident_target_mobile').val(data.target_mobile);
						$('#car_accident_target_percent').val(data.target_percent);
						$('#car_accident_target_compensation').val(data.target_compensation);
						$('#car_accident_target_insurance_company').val(data.target_insurance_company).prop("selected", true);
						$('#car_accident_target_register_number').val(data.target_register_number);
						$('#car_accident_target_insurance_manager').val(data.target_insurance_man);
						$('#car_accident_target_insurance_phone').val(data.target_insurance_phone);
						$('#car_accident_target_insurance_fax').val(data.target_insurance_fax);
						$('#car_accident_target_insurance_repair').val(data.target_repair);
						$('#car_accident_target_hospital').val(data.target_hospital);
						$('#car_accident_target_memo').val(data.target_memo);
	 					break;
	 			case 4:
	 					$('#history_repair_dialog').modal('toggle'); //점검 내역 팝업
	 					$('#repair_delete_btn').show(); //삭제 버튼 보이기
	 					$('#repair_history_dialog_title').text("정비 내역 수정");
	 					$('#repair_save_btn').text("수정");
	 					//데이터 가져와서 값 대입
	 					// var startDate = new Date(data.repair_start_date);
	 					// var endDate = new Date(data.repair_end_date);
	 					$('#repair_start_date').datepicker().data('datepicker').selectDate(data.repair_start_date);
	 					$('#repair_end_date').datepicker().data('datepicker').selectDate(data.repair_end_date);

	 					// $('#repair_start_date').attr('data-date', startDate.getFullYear()+"년 "+ zeroPad((startDate.getMonth() + 1),2) + "월 " + zeroPad(startDate.getDate(),2) + "일");
	 					// $('#repair_start_date_text').val(startDate.getFullYear()+"년 "+ zeroPad((startDate.getMonth() + 1),2) + "월 " + zeroPad(startDate.getDate(),2) + "일");
	 					// $('#repair_end_date').attr('data-date', endDate.getFullYear()+"년 "+ zeroPad((endDate.getMonth() + 1),2) + "월 " + zeroPad(endDate.getDate(),2) + "일");
	 					// $('#repair_end_date_text').val(endDate.getFullYear()+"년 "+ zeroPad((endDate.getMonth() + 1),2) + "월 " + zeroPad(endDate.getDate(),2) + "일");
	 					switch(data.type){
	 						case "일반정비":
	 								$('#repair_radio_type1').prop("checked", true);
									$('#repair_radio_type2').prop("checked", false);
									$('#repair_radio_type3').prop("checked", false);
	 								break;
	 						case "사고수리":
	 								$('#repair_radio_type1').prop("checked", false);
									$('#repair_radio_type2').prop("checked", true);
									$('#repair_radio_type3').prop("checked", false);
	 								break;
	 						case "보험수리":
	 								$('#repair_radio_type1').prop("checked", false);
									$('#repair_radio_type2').prop("checked", false);
									$('#repair_radio_type3').prop("checked", true);
	 								break;
	 					}
	 					$('#repair_input_company').val(data.repair_company);
						$('#repair_input_phone_number').val(data.repair_company_phone_number);
						$('#repair_input_text').val(data.repair_detail);
						// $('#repair_input_pay1').val(data.repair_pay1);
						// $('#repair_input_pay2').val(data.repair_pay2);
						// $('#repair_input_pay3').val(data.repair_pay3);
						$('#repair_input_total_pay').val(data.repair_total_pay);
						// $('#repair_input_distance').val(data.distance);
						$('#repair_input_customer_name').val(data.repair_customer_name);
						// $('#repair_input_charge1').val(data.repair_charge1);
						// $('#repair_input_charge2').val(data.repair_charge2);
						$('#repair_input_total_charge').val(data.repair_total_charge);
						$('#repair_input_memo').val(data.memo);
						// $('#history_repair_dialog').modal();  

	 					break;
	 			case 5:
	 					$('#history_emergency_dialog').modal('toggle'); //점검 내역 팝업
	 					$('#emergency_delete_btn').show(); //삭제 버튼 보이기
	 					$('#emergency_history_dialog_title').text("현장출동 내역 수정");
	 					$('#emergency_save_btn').text("수정");
	 					//데이터 가져와서 보여주기
	 					$('#emergency_time').datepicker().data('datepicker').selectDate(data.date);
	 					// var startDate = new Date(data.date);

	 					// $('#emergency_input_exchange_date').attr('data-date', startDate.getFullYear()+"년 "+ zeroPad((startDate.getMonth() + 1),2) + "월 " + zeroPad(startDate.getDate(),2) + "일");
	 					// $('#emergency_input_exchange_date_text').val(startDate.getFullYear()+"년 "+ zeroPad((startDate.getMonth() + 1),2) + "월 " + zeroPad(startDate.getDate(),2) + "일 " + zeroPad(startDate.getHours(),2)+"시 " + zeroPad(startDate.getMinutes(),2))+"분";
						$('#emergency_input_place').val(data.place);
						$('#emergency_input_detail').val(data.detail);

	 			 		break;
	 		}
	 		// is_history_register = filterablese;
	 		// register_modify_history();
	 	});





});



//점검 내역 등록 버튼 이벤트
$("#btn_history_register").click(function() {
	is_history_register = true;

	switch(selected_tab){
		case 1:
			$('#check_history_delete_btn').hide(); //삭제 버튼 없애기
			$("#check_history_save_btn").text("저장");
			dialog_title = $('#check_history_dialog_title');
			dialog_title.text("점검 내역 등록");
			//초기화
			$('#check_date').datepicker().data('datepicker').date = new Date();
 			$('#check_date').val('');
			$('#text_result').val('');
			$('#text_pay').val('');
			$('#check_select_type option:eq(0)').prop("selected", true);
			$('#check_select_period option:eq(0)').prop("selected", true);
			//다음 날짜 맨처음에는 사용 못하고 처음 날짜 선택하면 활성화 가능
			$('#datetimepicker2').prop('disabled', true);
			$('#history_check_dialog').modal();  
			break;
		case 2:
			$('#supply_history_delete_btn').hide(); //삭제 버튼 보이기
			$("#supply_history_save_btn").text("저장");
			dialog_title = $('#supply_history_dialog_title');
			dialog_title.text("소모품교환 내역 등록");
			// 초기화
			$('#supply_date').datepicker().data('datepicker').date = new Date();
 			$('#supply_date').val('');
			$('#supply_input_name').val('');
			$('#supply_input_period').val('');
			$('#supply_input_final').val('');
			$('#supply_input_next').val('');
			$('#supply_input_pay').val('');
			$('#supply_input_memo').val('');	
			$('#history_supply_dialog').modal();  
			break;
		case 3:

			var carGrid = $("#grid").data("kendoGrid");
			var row = carGrid.select();
			var data = carGrid.dataItem(row);

			$('#accident_delete_btn').hide(); //삭제 버튼 보이기
			$("#accident_save_btn").text("저장");
			dialog_title= $('#accident_history_dialog_title');
			dialog_title.text("사고 내역 등록");
			//초기화
			$('#car_accident_customer_car_number').val(data.car_number);
			$('#car_accident_customer_car_model').val(data.car_name_detail);
			$('#accident_status_apply').prop("checked", true);
			$('#accident_time').datepicker().data('datepicker').date = new Date();
			$('#accident_time').val('');
			$('#car_accident_place_input').val('');
			$('#car_accident_customer_name').val('');
			$('#car_accident_customer_company').val('');
			$('#car_accident_customer_phnoe').val('');
			$('#car_accident_customer_mobile').val('');
			$('#car_accident_customer_percent').val('');
			$('#car_accident_customer_compensation').val('');
			$('#car_accident_customer_insurance_company option:eq(0)').prop("selected", true);
			$('#car_accident_customer_register_number').val('');
			$('#car_accident_customer_insurance_manager').val('');
			$('#car_accident_customer_insurance_phone').val('');
			$('#car_accident_customer_insurance_fax').val('');
			$('#car_accident_customer_insurance_repair').val('');
			$('#car_accident_customer_pay').val('');
			$('#accident_insurance_date').datepicker().data('datepicker').date = new Date();
			$('#accident_insurance_date').val('');
			$('#car_accident_customer_memo').val('');

			$('#car_accident_target_car_number').val('');
			$('#car_accident_target_car_model').val('');
			$('#car_accident_target_name').val('');
			$('#car_accident_target_company').val('');
			$('#car_accident_target_phnoe').val('');
			$('#car_accident_target_mobile').val('');
			$('#car_accident_target_percent').val('');
			$('#car_accident_target_compensation').val('');
			$('#car_accident_target_insurance_company option:eq(0)').prop("selected", true);
			$('#car_accident_target_register_number').val('');
			$('#car_accident_target_insurance_manager').val('');
			$('#car_accident_target_insurance_phone').val('');
			$('#car_accident_target_insurance_fax').val('');
			$('#car_accident_target_insurance_repair').val('');
			$('#car_accident_target_hospital').val('');
			$('#car_accident_target_memo').val('');

			$('#history_accident_dialog').modal();  

			break;
		case 4:
			$('#repair_delete_btn').hide(); //삭제 버튼 보이기
			$("#repair_save_btn").text("저장");
			dialog_title = $('#repair_history_dialog_title');
			dialog_title.text("정비 내역 등록");
			//초기화
			$('#repair_radio_type1').prop("checked", true);
			$('#repair_radio_type2').prop("checked", false);
			$('#repair_radio_type3').prop("checked", false);
			$('#repair_start_date').datepicker().data('datepicker').date = new Date();
 			$('#repair_start_date').val('');
			$('#repair_end_date').datepicker().data('datepicker').date = new Date();
 			$('#repair_end_date').val('');
			$('#repair_input_company').val('');
			$('#repair_input_phone_number').val('');
			$('#repair_input_text').val('');
			// $('#repair_input_pay1').val('');
			// $('#repair_input_pay2').val('');
			// $('#repair_input_pay3').val('');
			$('#repair_input_total_pay').val('');
			// $('#repair_input_distance').val('');
			$('#repair_input_customer_name').val('');
			// $('#repair_input_charge1').val('');
			// $('#repair_input_charge2').val('');
			$('#repair_input_total_charge').val('');
			$('#repair_input_memo').val('');
			$('#history_repair_dialog').modal();  
			break;
		case 5:
			$('#emergency_delete_btn').hide(); //삭제 버튼 보이기
			$("#emergency_save_btn").text("저장");
			dialog_title = $('#emergency_history_dialog_title');
			dialog_title.text("현장출동 내역 등록");
			//초기화
			$('#emergency_time').datepicker().data('datepicker').date = new Date();
 			$('#emergency_time').val('');
			$('#emergency_input_place').val('');
			$('#emergency_input_detail').val('');
			$('#history_emergency_dialog').modal();  
			break;
	}

});

//////////////////////////////////////////////////////////////////////////////////////////// 탭 클릭 이벤트 
$("#tab_check_history").click(function() {
		selected_tab = 1;
		refresh_check_history();      
});

$("#tab_check_accident").click(function() {
		selected_tab = 3;
		refresh_accident_history();  
});
$("#tab_check_supply").click(function() {
		selected_tab = 2;
      	refresh_supply_history();
});

$("#tab_check_repair").click(function() {
		selected_tab = 4;
		refresh_repair_history();
});

$("#tab_check_go").click(function() {
		selected_tab = 5;
		refresh_emergency_history();		
 });


//////////////////////////////////////////////////////////////////////////////////////////// 내역 grid refresh
function refresh_check_history(){
		var carGrid = $("#grid").data("kendoGrid");
		var row = carGrid.select();
		var data = carGrid.dataItem(row);
				//기존 그리드 삭제
		if($('#grid_history').data().kendoGrid!=null){
				$('#grid_history').data().kendoGrid.destroy();
				$('#grid_history').empty();
		}

   		//check grid 초기화
   		$("#grid_history").kendoGrid({
					reorderable: true, //사용자가 목록 자기맘에드는대로 순서 바꿀수 있게
					resizable: true, //사용자가 컬럼 크기 맘대로 바꿀수 있게
					// selectable: "multiple, row", //선택할수 있도록
					allowCopy: true, //값 copyrksm
					height: 200, //높이????
					sortable: true, //정렬가능하도록
					// filterable: true, //필터(비교해서 정렬)가능하도록
					selectable: "row", //선택할수 있도록
					excel: {
						allPages: true,
       					fileName: historyExcelFilename, //excel로 저장할 이름,
       					filterable: true
       				},
       				pageable: false,
       				noRecords: {
       					template: "점검 내역이 없습니다."
       				},
       				dataSource: {
			        	transport: {
							    read: {
							      url: "http://solution.rengo.co.kr/carmanager/get_check_history/" + data.serial,
							      dataType: "json"
							    }
					   },
					   schema: {
					   	model: {
					   		fields: {
					   			index: { type: "number" },
					   			type: { type: "string" },
					   			period: { type: "string" },
					   			check_date: { type: "date" },
					   			next_check_date: { type: "date" },
					   			pay: { type: "number" },
					   			result: { type: "string" }
					   			}
					   		}		
					   	},
					 pageSize: 10
									 // serverPaging: true,
									 // serverFiltering: true,
									 // serverSorting: true
					},
					excelExport: function(e) {
					    e.workbook.fileName = historyExcelFilename;
					  },
					columns: [ 
						{
							field: "type",
							title: "점검종류",

						}, 
						// {
						// 	field: "period",
						// 	title: "주기",
						// },
						{
							field: "check_date",
							title: "검사일시",
							format: "{0:yyyy년 M월 dd일}",
						},
						// {
						// 	field: "next_check_date",
						// 	title: "다음검사일",
						// 	format: "{0:yyyy/M/dd}",
						// },
						{
							field: "pay",
							title: "검사비용",
							format: "{0:##,#}원",
						},
						{
							field: "result",
							title: "검사결과",
						}
           			]
         });

   		$('#grid_history').data('kendoGrid').refresh();
}


function refresh_supply_history(){

		var carGrid = $("#grid").data("kendoGrid");
		var row = carGrid.select();
		var data = carGrid.dataItem(row);
		//기존 그리드 삭제
		if($('#grid_history').data().kendoGrid!=null){
				$('#grid_history').data().kendoGrid.destroy();
				$('#grid_history').empty();
			}
   		//supply grid 초기화

   		$("#grid_history").kendoGrid({
					reorderable: true, //사용자가 목록 자기맘에드는대로 순서 바꿀수 있게
					resizable: true, //사용자가 컬럼 크기 맘대로 바꿀수 있게
					// selectable: "multiple, row", //선택할수 있도록
					allowCopy: true, //값 copyrksm
					height: 200, //높이????
					sortable: true, //정렬가능하도록
					// filterable: true, //필터(비교해서 정렬)가능하도록
					selectable: "row", //선택할수 있도록
					excel: {
						allPages: true,
       					fileName: historyExcelFilename, //excel로 저장할 이름,
       					filterable: true
       				},
       				// pageable: {
       				// 	messages: {
       				// 		display: "Showing {0}-{1} from {2} data items",
       				// 		empty: "No data"
       				// 	}
       				// },
       				noRecords: {
       					template: "소모품 교환 내역이 없습니다."
       				},
       				// columnMenu: {
       				// 	sortable: false,
       				// 	messages: {
       				// 		columns: "표시할 항목 선택",
       				// 		filter: "필터",
       				// 	}
       				// },
       				// filterable: {
       				// 	operators: {
       				// 		string: {
       				// 			eq: "같음",
       				// 			contains: "포함됨",
       				// 			startswith: "시작됨",
       				// 			endswith: "끝남"
       				// 		},
       				// 		number: {
       				// 			eq: "같음",
       				// 			gte: "크거나 같음",
       				// 			gt: "큼",
       				// 			lte: "작거타 같음",
       				// 			lt: "작음",
       				// 		},
       				// 		date: {
       				// 			eq: "같음",
       				// 			gte: "(포함)이후",
       				// 			lte: "(포함)이전"
       				// 		}
       				// 	}},
       				dataSource: {
			        	transport: {
							    read: {
							      url: "http://solution.rengo.co.kr/carmanager/get_supply_history/" + data.serial,
							      dataType: "json"
							    }
					   },
					   schema: {
					   	model: {
					   		fields: {
					   			index: { type: "number" },
					   			type: { type: "string" },
					   			period: { type: "number" },
					   			check_date: { type: "date" },
					   			check_distance: { type: "number" },
					   			next_check_distance: { type: "number" },
					   			pay: { type: "number" },
					   			memo: { type: "string" }
					   			}
					   		}		
					   	},
					 pageSize: 10
									 // serverPaging: true,
									 // serverFiltering: true,
									 // serverSorting: true
					},
					excelExport: function(e) {
					     e.workbook.fileName = historyExcelFilename;
					  },
					columns: [ 
						{
							field: "type",
							title: "소모품",

						}, 
						{
							field: "check_date",
							title: "교환일시",
							format: "{0:yyyy년 M월 dd일}",
						},
						{
							field: "check_distance",
							title: "최종교환",
							format: "{0:##,#}km",
						},
						// {
						// 	field: "next_check_distance",
						// 	title: "다음교환",
						// 	format: "{0:##,#}km",
						// },
						{
							field: "pay",
							title: "교환비용",
							format: "{0:##,#}원",
						},
						{
							field: "memo",
							title: "메모",
						}
           			]
         });
   		$('#grid_history').data('kendoGrid').refresh();

}

function refresh_accident_history(){
	var carGrid = $("#grid").data("kendoGrid");
		var row = carGrid.select();
		var data = carGrid.dataItem(row);
				//기존 그리드 삭제
		if($('#grid_history').data().kendoGrid!=null){
				$('#grid_history').data().kendoGrid.destroy();
				$('#grid_history').empty();
			}
   			//supply grid 초기화


   		$("#grid_history").kendoGrid({
					reorderable: true, //사용자가 목록 자기맘에드는대로 순서 바꿀수 있게
					resizable: true, //사용자가 컬럼 크기 맘대로 바꿀수 있게
					// selectable: "multiple, row", //선택할수 있도록
					allowCopy: true, //값 copyrksm
					height: 200, //높이????
					sortable: true, //정렬가능하도록
					// filterable: true, //필터(비교해서 정렬)가능하도록
					selectable: "row", //선택할수 있도록
					// excel: {
					// 	allPages: true,
     //   					fileName: "사고내역", //excel로 저장할 이름,
     //   					filterable: true
     //   				},
       				excelExport: function(e) {
					     e.workbook.fileName = historyExcelFilename;
					  },
       				// pageable: {
       				// 	messages: {
       				// 		display: "Showing {0}-{1} from {2} data items",
       				// 		empty: "No data"
       				// 	}
       				// },
       				noRecords: {
       					template: "사고 내역이 없습니다."
       				},
       				// columnMenu: {
       				// 	sortable: false,
       				// 	messages: {
       				// 		columns: "표시할 항목 선택",
       				// 		filter: "필터",
       				// 	}
       				// },
       				// filterable: {
       				// 	operators: {
       				// 		string: {
       				// 			eq: "같음",
       				// 			contains: "포함됨",
       				// 			startswith: "시작됨",
       				// 			endswith: "끝남"
       				// 		},
       				// 		number: {
       				// 			eq: "같음",
       				// 			gte: "크거나 같음",
       				// 			gt: "큼",
       				// 			lte: "작거타 같음",
       				// 			lt: "작음",
       				// 		},
       				// 		date: {
       				// 			eq: "같음",
       				// 			gte: "(포함)이후",
       				// 			lte: "(포함)이전"
       				// 		}
       				// 	}},
       					dataSource: {
       						transport: {
       							read: {
       								url: "http://solution.rengo.co.kr/carmanager/get_accident_history/" + data.serial,
       								dataType: "json"
       							}
       						},
       						schema: {
       							model: {
       								fields: {
       									serial: { type: "number" },
       									status: { type: "string" },
       									accident_date: { type: "date" },
       									accident_place: { type: "string" },
       									// accident_detail: { type: "string" },
       									customer_name: { type: "string" },
       									customer_company :  { type: "string" },
       									customer_phone: { type: "string" },
       									customer_mobile: { type: "string" },
       									// customer_human_damage: { type: "string" },
       									// customer_damage_measure: { type: "string" },
       									customer_percent: { type: "number" },
       									customer_compensation: { type: "number" },
       									customer_pay: { type: "number" },
       									customer_insurance_date: { type: "date" },
       									customer_insurance_company: { type: "string" },
       									customer_register_number: { type: "string" },
       									customer_insurance_man: { type: "string" },
       									customer_insurance_phone: { type: "string" },
       									customer_insurance_fax: { type: "string" },
       									customer_repair: { type: "string" },
       									customer_memo: { type: "string" },
       									target_car_number: { type: "string" },
       									target_car_model: { type: "string" },
       									target_name: { type: "string" },
       									target_phone: { type: "string" },
       									target_mobile: { type: "string" },
       									target_company :  { type: "string" },
       									// target_human_damage: { type: "string" },
       									// target_damage_measure: { type: "string" },
       									target_percent: { type: "number" },
       									target_compensation: { type: "number" },
       									target_insurance_company: { type: "string" },
       									target_register_number: { type: "string" },
       									target_insurance_man: { type: "string" },
       									target_insurance_phone: { type: "string" },
       									target_insurance_fax: { type: "string" },
       									target_repair: { type: "string" },
       									target_hospital: { type: "string" },
       									target_memo: { type: "string" },

       								}
       							}		
       						},
       						pageSize: 10
									 // serverPaging: true,
									 // serverFiltering: true,
									 // serverSorting: true
									},
									columns: [ 
									{
										field: "status",
										title: "상태",
										width: 80
									}, 
									{
										field: "accident_date",
										title: "사고일시",
										format: "{0: yyyy년 MM월 dd일 HH시mm분}",
										width: 150
									}, 
									{
										field: "accident_place",
										title: "사고장소",
										width: 100
									},
									// {
									// 	field: "accident_detail",
									// 	title: "사고내용",
									// 	width: 150
									// },
									{
										title: "당사 차량 정보",
										columns: [{
											field: "customer_car_number",
											title: "차량정보",
											template: data.car_number ,
											width: 100
										},{
											field: "customer_car_model",
											title: "차량모델",
											template: data.car_name_detail ,
											width: 100
										},{
											field: "customer_name",
											title: "고객성명",
											width: 100
										},{
											field: "customer_phone",
											title: "고객연락처",
											width: 100
										},
										// {
										// 	field: "customer_human_damage",
										// 	title: "인적피해",
										// 	width: 100
										// },{
										// 	field: "customer_damage_measure",
										// 	title: "피해정도",
										// 	width: 100
										// },
										{
											field: "customer_percent",
											title: "과실비율",
											format: "{0:##,#}%",
											width: 100
										},{
											field: "customer_compensation",
											title: "면책금액",
											width: 100,
											format: "{0:##,#}원",
										},
										{
											field: "customer_pay",
											title: "고객청구금액",
											width: 100,
											format: "{0:##,#}원",
										},
										{
											field: "customer_insurance_company",
											title: "보험회사",
											width: 100
										},{
											field: "customer_register_number",
											title: "접수번호",
											width: 100
										},{
											field: "customer_insurance_man",
											title: "담당자",
											width: 100
										},{
											field: "customer_insurance_phone",
											title: "전화번호",
											width: 100
										},{
											field: "customer_insurance_fax",
											title: "팩스번호",
											width: 100
										},{
											field: "customer_repair",
											title: "정비공업사",
											width: 100
										},
										{
											field: "customer_memo",
											title: "메모",
											width: 100
										}]
									},
									{
										title: "당사 차량 정보",
										columns: [{
											field: "target_car_number",
											title: "차량정보",
											width: 100
										},{
											field: "target_car_model",
											title: "차량모델",
											width: 100
										},{
											field: "target_name",
											title: "고객성명",
											width: 100
										},{
											field: "target_phone",
											title: "고객연락처",
											width: 100
										},
										// {
										// 	field: "target_human_damage",
										// 	title: "인적피해",
										// 	width: 100
										// },{
										// 	field: "target_damage_measure",
										// 	title: "피해정도",
										// 	width: 100
										// },
										{
											field: "target_percent",
											title: "과실비율",
											format: "{0:##,#}%",
											width: 100
										},{
											field: "target_compensation",
											title: "면책금액",
											width: 100,
											format: "{0:##,#}원",
										},{
											field: "target_insurance_company",
											title: "보험회사",
											width: 100
										},{
											field: "target_register_number",
											title: "접수번호",
											width: 100
										},{
											field: "target_insurance_man",
											title: "담당자",
											width: 100
										},{
											field: "target_insurance_phone",
											title: "전화번호",
											width: 100
										},{
											field: "target_insurance_fax",
											title: "팩스번호",
											width: 100
										},{
											field: "target_repair",
											title: "정비공업사",
											width: 100
										},
										{
											field: "target_memo",
											title: "메모",
											width: 100
										}]
									},

									]
								});
	$('#grid_history').data('kendoGrid').refresh();
}
     
function refresh_repair_history(){
		var carGrid = $("#grid").data("kendoGrid");
		var row = carGrid.select();
		var data = carGrid.dataItem(row);
				//기존 그리드 삭제
		if($('#grid_history').data().kendoGrid!=null){
				$('#grid_history').data().kendoGrid.destroy();
				$('#grid_history').empty();
			}
   	//check grid 초기화
   		$("#grid_history").kendoGrid({
					reorderable: true, //사용자가 목록 자기맘에드는대로 순서 바꿀수 있게
					resizable: true, //사용자가 컬럼 크기 맘대로 바꿀수 있게
					// selectable: "multiple, row", //선택할수 있도록
					allowCopy: true, //값 copyrksm
					height: 200, //높이????
					sortable: true, //정렬가능하도록
					// filterable: true, //필터(비교해서 정렬)가능하도록
					selectable: "row", //선택할수 있도록
					excel: {
						allPages: true,
       					fileName: "정비내역", //excel로 저장할 이름,
       					filterable: true
       				},
       				// pageable: {
       				// 	messages: {
       				// 		display: "Showing {0}-{1} from {2} data items",
       				// 		empty: "No data"
       				// 	}
       				// },
       				noRecords: {
       					template: "정비 내역이 없습니다."
       				},
       				// columnMenu: {
       				// 	sortable: false,
       				// 	messages: {
       				// 		columns: "표시할 항목 선택",
       				// 		filter: "필터",
       				// 	}
       				// },
       				// filterable: {
       				// 	operators: {
       				// 		string: {
       				// 			eq: "같음",
       				// 			contains: "포함됨",
       				// 			startswith: "시작됨",
       				// 			endswith: "끝남"
       				// 		},
       				// 		number: {
       				// 			eq: "같음",
       				// 			gte: "크거나 같음",
       				// 			gt: "큼",
       				// 			lte: "작거타 같음",
       				// 			lt: "작음",
       				// 		},
       				// 		date: {
       				// 			eq: "같음",
       				// 			gte: "(포함)이후",
       				// 			lte: "(포함)이전"
       				// 		}
       				// 	}},
       				dataSource: {
			        	transport: {
							    read: {
							      url: "http://solution.rengo.co.kr/carmanager/get_repair_history/" + data.serial,
							      dataType: "json"
							    }
					   },
					   schema: {
					   	model: {
					   		fields: {
					   			index: { type: "number" },
					   			type: { type: "string" },
					   			repair_start_date: { type: "date" },
					   			repair_end_date: { type: "date" },
					   			repair_company: { type: "string" },
					   			repair_company_phone_number: { type: "string" },
					   			repair_detail: { type: "string" },
					   			repair_pay1: { type: "number" },
					   			repair_pay2: { type: "number" },
					   			repair_pay3: { type: "number" },
					   			repair_total_pay: { type: "number" },
					   			repair_customer_name: { type: "string" },
					   			repair_charge1: { type: "number" },
					   			repair_charge2: { type: "number" },
					   			repair_total_charge: { type: "number" },
					   			memo: { type: "string" },
					   			distance: { type: "number" },
					   			}
					   		}		
					   	},
					 pageSize: 10
									 // serverPaging: true,
									 // serverFiltering: true,
									 // serverSorting: true
					},
					excelExport: function(e) {
					     e.workbook.fileName = historyExcelFilename;
					  },
					columns: [ 
						{
							field: "type",
							title: "구분",
						}, 
						{
							field: "repair_start_date",
							title: "정비시작일",
							format: "{0:yyyy년 M월 dd일}",
						},
						{
							field: "repair_end_date",
							title: "정비종료일",
							format: "{0:yyyy년 M월 dd일}",
						},
						{
							field: "repair_company",
							title: "공업사",
						},
						{
							field: "repair_detail",
							title: "정비내용",
						},
						{
							field: "repair_total_pay",
							title: "총비용",
							format: "{0:##,#}원",
						},
						{
							field: "repair_customer_name",
							title: "고객성명",
						},
						{
							field: "repair_total_charge",
							title: "총청구액",
							format: "{0:##,#}원",
						},
						{
							field: "memo",
							title: "메모",
						},
           			]
         });
   		$('#grid_history').data('kendoGrid').refresh();

}

function refresh_emergency_history(){
	var carGrid = $("#grid").data("kendoGrid");
		var row = carGrid.select();
		var data = carGrid.dataItem(row);
		//기존 그리드 삭제
		if($('#grid_history').data().kendoGrid!=null){
				$('#grid_history').data().kendoGrid.destroy();
				$('#grid_history').empty();
			}

   	//grid 새로생성
   		$("#grid_history").kendoGrid({
					reorderable: true, //사용자가 목록 자기맘에드는대로 순서 바꿀수 있게
					resizable: true, //사용자가 컬럼 크기 맘대로 바꿀수 있게
					// selectable: "multiple, row", //선택할수 있도록
					allowCopy: true, //값 copyrksm
					height: 200, //높이????
					sortable: true, //정렬가능하도록
					// filterable: true, //필터(비교해서 정렬)가능하도록
					selectable: "row", //선택할수 있도록
					excel: {
						allPages: true,
       					fileName: historyExcelFilename, //excel로 저장할 이름,
       					filterable: true
       				},
       				// pageable: {
       				// 	messages: {
       				// 		display: "Showing {0}-{1} from {2} data items",
       				// 		empty: "No data"
       				// 	}
       				// },
       				noRecords: {
       					template: "긴급출동 내역이 없습니다."
       				},
       				// columnMenu: {
       				// 	sortable: false,
       				// 	messages: {
       				// 		columns: "표시할 항목 선택",
       				// 		filter: "필터",
       				// 	}
       				// },
       				// filterable: {
       				// 	operators: {
       				// 		string: {
       				// 			eq: "같음",
       				// 			contains: "포함됨",
       				// 			startswith: "시작됨",
       				// 			endswith: "끝남"
       				// 		},
       				// 		number: {
       				// 			eq: "같음",
       				// 			gte: "크거나 같음",
       				// 			gt: "큼",
       				// 			lte: "작거타 같음",
       				// 			lt: "작음",
       				// 		},
       				// 		date: {
       				// 			eq: "같음",
       				// 			gte: "(포함)이후",
       				// 			lte: "(포함)이전"
       				// 		}
       				// 	}},
       				dataSource: {
			        	transport: {
							    read: {
							      url: "http://solution.rengo.co.kr/carmanager/get_emergency_history/"+ data.serial,
							      dataType: "json"
							    }
					   },
					   schema: {
					   	model: {
					   		fields: {
					   			index: { type: "number" },
					   			date: { type: "date" },
					   			place: { type: "string" },
					   			detail: { type: "string" },
					   			}
					   		}		
					   	},
					 pageSize: 10
									 // serverPaging: true,
									 // serverFiltering: true,
									 // serverSorting: true
					},
					excelExport: function(e) {
					     e.workbook.fileName = historyExcelFilename;
					  },
					columns: [ 
						{
							field: "date",
							title: "출동일시",
							format: "{0: yyyy년 MM월 dd일 HH시mm분}",
						}, 
						{
							field: "place",
							title: "장소",
						},
						{
							field: "detail",
							title: "내용",
						},
           			]
         	});
      
}
      

	


//점검날짜 주기 변경시
function periodChange(){
	var checkDate = $('#datetimepicker').data('date');
	if(checkDate!=null){
		var checkedYaer = checkDate.substring(0,4);
		var checkedMonth = checkDate.substring(6,8);
		var checkedDay = checkDate.substring(10,12);
		var selectDate = new Date(checkedYaer+"-"+checkedMonth+"-"+checkedDay); 
		var type = $('#check_select_type option:selected').val();
		var period = $('#check_select_period option:selected').val(); 

		//주기 글자 길이 구해서 길면 개월, 짧으면 년
		if(period.length==3){
			var month = parseInt(period.substring(0,1));
			selectDate.setMonth(selectDate.getMonth() + month);
		}else{
			var month = parseInt(period.substring(0,1))*12;
			selectDate.setMonth(selectDate.getMonth() + month);
		}
		$('#datetimepicker2').prop('disabled', false);
		$('#datetimepicker2').val(selectDate.getFullYear()+"년 "+zeroPad((selectDate.getMonth() + 1),2) + "월 " + zeroPad(selectDate.getDate() + "일"),2);
	}

}


/////////////////////////////////////////////////////////////////////// 내역 버튼들 이벤트 ///////////////////////////////////////////////////////////////




// $("#btn_history_modify").click(function() {

// 	var dialog_title;

// 	//grid에서 id 얻어오기
// 	var grid = $("#grid_history").data("kendoGrid");
// 	var row = grid.select();
// 	var data = grid.dataItem(row);
// 	if(data==null){
// 		alert("먼저 수정할 항목을 선택해 주세요.");
// 		return false;
// 	}
// 	switch(selected_tab){
// 		case 1:
// 			dialog_title = $('#check_history_dialog_title');
// 			dialog_title.text("점검 내역 수정");

// 			//값 설정
// 			var checkDate = new Date(data.check_date);
// 			var nextCheckDate = new Date(data.next_check_date);
			
// 			$('#datetimepicker').attr('data-date', checkDate.getFullYear()+"년 "+ zeroPad((checkDate.getMonth() + 1),2) + "월 " + zeroPad(checkDate.getDate(),2) + "일");
// 			$('#datetimepicker').val(checkDate.getFullYear()+"년 "+ zeroPad((checkDate.getMonth() + 1),2) + "월 " + zeroPad(checkDate.getDate(),2) + "일");
// 			$('#datetimepicker2').attr('data-date', nextCheckDate.getFullYear()+"년 "+ zeroPad((nextCheckDate.getMonth() + 1),2) + "월 " + zeroPad(nextCheckDate.getDate(),2) + "일");
// 			$('#datetimepicker2').val(nextCheckDate.getFullYear()+"년 "+zeroPad((nextCheckDate.getMonth() + 1),2) + "월 " + zeroPad(nextCheckDate.getDate(),2) + "일");
// 			$('#text_result').val(data.result);
// 			$('#text_pay').val(data.pay);
// 			// alert(data.type+","+data.period);
// 			$("#check_select_type").val(data.type).attr("selected", "selected");
// 			$("#check_select_period").val(data.period).attr("selected", "selected");
// 			//다이얼로그 보이기
// 			$('#history_check_dialog').modal(); 
// 			break;
// 		case 2:
// 			dialog_title = $('#check_supply_dialog_title');
// 			dialog_title.text("소모품교환 내역 수정");
// 			$('#history_supply_dialog').modal();  
// 			break;
// 		case 3:
// 			dialog_title = $('#check_accident_dialog_title');
// 			dialog_title.text("사고 내역 수정");
// 			$('#history_accident_dialog').modal();  
// 			break;
// 		case 4:
// 			dialog_title = $('#check_repair_dialog_title');
// 			dialog_title.text("정비 내역 수정");
// 			$('#history_repair_dialog').modal();  
// 			break;
// 		case 5:
// 			dialog_title = $('#check_emergency_dialog_title');
// 			dialog_title.text("현장출동 내역 수정");
// 			$('#history_emergency_dialog').modal();  
// 			break;
// 	}

// });




</script>