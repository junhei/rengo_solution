
<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">


<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>


<!-- 새로운 datepicker -->
<link href="/css/datepicker.min.css?<?=time();?>" rel="stylesheet" type="text/css">
<script src="/js/datepicker.js?<?=time();?>"></script>
<script src="/js/i18n_datepicker.en.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>


<?php
require("/home/apache/CodeIgniter-3.0.6/application/views/car/dialog_car_info.html");
require("/home/apache/CodeIgniter-3.0.6/application/views/car/dialog_check_input.html");
require("/home/apache/CodeIgniter-3.0.6/application/views/car/dialog_emergency_input.html");
require("/home/apache/CodeIgniter-3.0.6/application/views/car/dialog_repair_input.html");
require("/home/apache/CodeIgniter-3.0.6/application/views/car/dialog_supply_input.html");
require("/home/apache/CodeIgniter-3.0.6/application/views/car/dialog_accident_input.html");

require("/home/apache/CodeIgniter-3.0.6/application/views/car/history_grid_event.php");
require("/home/apache/CodeIgniter-3.0.6/application/views/rengo_util.html");
?>
<style type="text/css">
	html{overflow:hidden;}
</style>

<!--content-->
<div class="row">
	<div class="col-md-12">
		<div class="panel border_gray magb0">
			
			<div class="panel-heading pdt15">
				<div class="row">
					<div class="col-md-9">
						<h3 class="mgb10">차량관리</h3>
					</div>
					<div class="col-sm-3">
						<div class="col-md-12 pdl0 pdr0">
							<select class="form-control" id="company_select">
								<?php foreach($company_list as $company):?>
									<option value="<?php echo $company['serial']; ?>"> <?php echo $company['company_name']; ?></option>
								<?php endforeach;?>
							</select>
						</div>

					</div>
				</div>
			</div>
			
			<div class="panel-footer bb_ccc pdb0 pdt15">
				<div class="row">
					<div class="list_data_box">
						<div class="col-lg-1 col-md-2 col-xs-3">전체차량</div>
						<div class="col-lg-1 col-md-2 col-xs-3 text-right" id="car_div_all"><?php echo $전체 ?>대</div>
						<div class="col-lg-1 col-md-2 col-xs-3">운행차량</div>
						<div class="col-lg-1 col-md-2 col-xs-3 text-right" id="car_div_run"><?=$정상?>대</div>
						<div class="col-lg-1 col-md-2 col-xs-3">미운행차량</div>
						<div class="col-lg-1 col-md-2 col-xs-3 text-right" id="car_div_no_run"><?=$전체-$정상?>대</div>
					</div>
				</div>
			</div>
			
			<div class="panel-heading bg_fff pdt14 pdb13">
				<div class="row">
					<div class="col-md-9">
						<button id="btn_register" type="button" class="btn action_btn">차량등록</button>
						<a class="btn btn-default magl5" href="#" role="button" onclick="javascript:saveExcel();">엑셀로 저장</a>
					</div>
				</div>
			</div>

		</div>


		<div class="panel-footer section_lnb bt0">
			<ul class="nav nav-tabs">
		        <li class="active"><a data-toggle="tab" href="#car_view01" id="tab_car1">전체</a></li>
		        <li><a data-toggle="tab" href="#car_view01" id="tab_car6">경형</a></li>
		        <li><a data-toggle="tab" href="#car_view01" id="tab_car5">소형</a></li>
		        <li><a data-toggle="tab" href="#car_view01" id="tab_car4">준중형</a></li>
		        <li><a data-toggle="tab" href="#car_view01" id="tab_car3">중형</a></li>
		        <li><a data-toggle="tab" href="#car_view01" id="tab_car2">대형</a></li>
		        <li><a data-toggle="tab" href="#car_view01" id="tab_car7">SUV</a></li>
		        <li><a data-toggle="tab" href="#car_view01" id="tab_car8">승합</a></li>
		        <li><a data-toggle="tab" href="#car_view01" id="tab_car9">수입</a></li>
		        <li class="col-md-3 pull-right magt15">
					<div class="input-group custom-search-form">
						<input id="searchbox" type="text" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button">
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
		        </li>
		    </ul>
		</div>

	</div>
</div>

	


<!--content -->

<div class="tab-content">
	<div id="example magt0">
		<div id="grid"></div>
	</div>
</div>


<script>



$(window).resize(function() {
    var gridElement = $("#grid");
    var gird_position = $('#grid').position();
    var window_height = $( window ).height();
    gridElement.children(".k-grid-content").height(window_height - gird_position.top - 150);
    gridElement.height(window_height - gird_position.top - 50);
});

$(document).ready(function () {


	 var gird_position = $('#grid').position();
     var window_height = $( window ).height() - gird_position.top - 50;

     var company_serial = get_company_serial();


    $("#grid").kendoGrid({
				navigatable: true,  //키보드로 표 조작 할수 있게
				// reorderable: true, //사용자가 목록 자기맘에드는대로 순서 바꿀수 있게
				resizable: true, //사용자가 컬럼 크기 맘대로 바꿀수 있게
				selectable: "row", //선택할수 있도록
				allowCopy: true, //값 copyrksm
				height: window_height, //높이????
				sortable: true, //정렬가능하도록
				// filterable: true, //필터(비교해서 정렬)가능하도록
				excel: {
					allPages: true,
       				fileName: excelFileName, //excel로 저장할 이름,
       				filterable: true
       			},
       			pageable: 
       			// {input:true},
					{
						input: true,
						messages: {
							// display: "Showing {0}-{1} from {2} data items",
							display: "총 {2} 개의 차중에 {0}~{1}번째",
							empty: "데이타 없음"
						}
					},
				// groupable: {
				// 	messages: {
				// 		empty: "여기어 항목을 넣으면 그룹으로 정렬됩니다..."
				// 	}
				// }, //그룹으로 정렬 가능
		  //                  filterable: {
						  //     mode: "row" //row로 설정하면 각 항목에 검색할수있는 filter제공 가능
						  // },
								// columnMenu: true, //정렬 + 필터 + 필드숨기기 가능하도록
								noRecords: {
									template: "현재 등록된 차량이 없습니다."
								},
		  //                 messages: { //특정 상황에서 보여줄 메세지 지정
						  //   noRecords: "현재 보여줄 항목이 없습니다."
						  // },
						//   columnMenu: {
						//   	sortable: false,
						//   	messages: {
						//   		columns: "표시할 항목 선택",
						//   		filter: "필터",
						//   	// sortAscending: "오름차순으로 정렬",
						//   	// sortDescending: "내림차순으로 정렬"
						//   }
						// },
				// 		filterable: {
				// 			operators: {
				// 				string: {
				// 					eq: "같음",
				// 					contains: "포함됨",
				// 					startswith: "시작됨",
				// 					endswith: "끝남"
				// 				},
				// 				number: {
				// 					eq: "같음",
				// 					gte: "크거나 같음",
				// 					gt: "큼",
				// 					lte: "작거타 같음",
				// 					lt: "작음",
				// 				},
				// 				date: {
				// 					eq: "같음",
				// 					gte: "(포함)이후",
				// 					lte: "(포함)이전"
				// 				}
				// 			}},
				// pdf: { //pdf로 내보내기
				// 	allPages: true
				// },
				dataSource: {
						  	// type: "odata",
						  	transport: {
						  		read: {
						  			url: "http://solution.rengo.co.kr/carmanager/get_list/" + company_serial  ,
						  			dataType: "json"
						  		}
						  	},

					 	// data : products,
					 	schema: {
					 		model: {
					 			fields: {
					 				serial: { type: "number" ,   validation: { required: true }, defaultValue: 0 },
					 				// branch_serial :  { type: "number" },
					 				car_index:  { type: "number" },
					 				car_status: { type: "string" },
					 				car_number: { type: "string" },
					 				rent_status: { type: "string" },
					 				price_type: { type: "string" },
					 				car_type: { type: "string" },
					 				car_name_detail: { type: "string" },
					 				car_people: { type: "number" },
					 				color: { type: "string" },
					 				gear_type: { type: "string" },
					 				fuel_option: { type: "string" },
					 				smoking: { type: "string" },
					 				release_year: { type: "number" },
					 				// drive_distance: { type: "number" },
					 				// rest_fuel: { type: "number" },
					 				car_register_number: { type: "string" },
					 				// option: { type: "string" },
					 				// car_manager: { type: "string" },
					 				// temp_number: { type: "string" },
					 				insurance_company: { type: "string" },
					 				// total_insurance_money: { type: "number" },
					 				insurance_age: { type: "string" },
					 				insurance_start_date: { type: "date" },
					 				insurance_end_date: { type: "date" },
					 				insurance_text: { type: "string" },
					 				// purchase_date: { type: "date" },
					 				release_date: { type: "date" },
					 				register_date: { type: "date" },
					 				purchase_type: { type: "string" },
					 				// car_money: { type: "number" },
					 				// installment_money: { type: "number" },
					 				// installment_pay_day: { type: "string" },
					 				// installment_monthly_money: { type: "number" },
					 				// location: { type: "string" },
					 				// etc_text: { type: "string" },
					 				register_user: { type: "date" },
					 				register_datetime: { type: "string" },
					 				modify_user: { type: "string" },
					 				modify_datetime: { type: "date" },
					 				car_image: { type: "string" },
					 				option_usb : { type: "string" },
					 				option_second_driver : { type: "string" },
					 				option_international_license : { type: "string" },
					 				option_license1 : { type: "string" },
					 				minimun_drive_year : { type: "int" },
					 				option_navi : { type: "string" },
					 				option_bluetooth : { type: "string" },
					 				option_automatic_back_mirror : { type: "string" },
					 				option_hi_pass : { type: "string" },
					 				option_aux: { type: "string" },
					 				option_heatseat: { type: "string" },
					 				option_gps: { type: "string" },
					 				option_rear_sensor: { type: "string" },
					 				option_blackbox: { type: "string" },
					 				option_cdplayer: { type: "string" },
					 				option_backcamera: { type: "string" },
					 				option_sunroof: { type: "string" },
					 				option_lane_departure_warning: { type: "string" },
					 				option_automatic_seat: { type: "string" },
					 				option_front_sensor: { type: "string" },
					 				option_smart_key: { type: "string" },
					 				option_av_system: { type: "string" },
					 				option_ecm: { type: "string" },
					 				// option_week_48: { type: "string" },
					 				option_weekend_48: { type: "string" },
					 				insurance_man1_flag : { type: "string" },
					 				insurance_man2_flag : { type: "string" },
					 				insurance_object_flag : { type: "string" },
					 				insurance_children_flag : { type: "string" },
					 				insurance_self_flag : { type: "string" },
					 				insurance_self2_flag : { type: "string" },
					 				insurance_self3_flag : { type: "string" },
					 				insurance_self_price : { type: "string" },
					 				insurance_self2_price : { type: "string" },
					 				insurance_self3_price : { type: "string" },
					 				insurance_man1_compensation : { type: "string" },
					 				insurance_man2_compensation : { type: "string" },
					 				insurance_object_compensation : { type: "string" },
					 				insurance_children_compensation : { type: "string" },
					 				insurance_self_compensation : { type: "string" },
					 				insurance_self2_compensation : { type: "string" },
					 				insurance_self3_compensation : { type: "string" },
					 				insurance_man1_customer : { type: "string" },
					 				insurance_man2_customer : { type: "string" },
					 				insurance_object_customer : { type: "string" },
					 				insurance_children_customer : { type: "string" },
					 				insurance_self_customer : { type: "string" },
					 				insurance_self2_customer : { type: "string" },
					 				insurance_self3_customer : { type: "string" },
					 				week_price : { type: "number" },
					 				weekend_price : { type: "number" },
					 				week_price_6 : { type: "number" },
					 				weekend_price_6 : { type: "number" },
					 				week_price_1 : { type: "number" },
					 				weekend_price_1 :  { type: "number" },
					 				// day_price : { type: "number" },
					 				// hour_price : { type: "number" },


					 			}
					 		}
					 	},
					 	pageSize: 20
									 // serverPaging: true,
									 // serverFiltering: true,
									 // serverSorting: true
									},

					// <span class="bg_primary">예약</span>
					// 					<span class="bg_success">운행</span>
					// 					<span class="bg_info">반납</span>
					// 					<span class="bg_waming">장기</span>
					// 					<span class="bg_danger">보험</span>
					// 					<span class="bg_deep_purple">할인</span>

					columns: [ 
					// {
					// 	field: "serial",
					// 	title: "번호",
					// 					  template: "<strong>#: serial # </strong>", //해당 항목 찐하게
					// 					  width: 80
					// 					},
										{
											field: "car_status",
											title: "운행여부",
											template: "<span class='#:data.car_status#' >#:data.car_status#</span>" ,
											width: 120,
											filterable: {
												cell: {
													template: function (args) {
							                            // create a DropDownList of unique values (colors)
							                            args.element.kendoDropDownList({
							                            	dataSource: args.dataSource,
							                            	dataTextField: "car_status",
							                            	dataValueField: "car_status",
							                            	valuePrimitive: true
							                            });

							                            // or
							                            // create a ColorPicker
							                            // args.element.kendoColorPicker();
							                        },
							                        showOperators: true
							                    }
							                }
							            },
							            // {
							            // 	field: "rent_status",
							            // 	template: "<span class='#:data.rent_status#'>#:data.rent_status#</span>" ,
							            // 	title: "대여상태",
							            // 	width: 120
							            // },
							            {
							            	field: "car_image",
							            	template: "<img class='img-circle' width='50' src='https://s3.ap-northeast-2.amazonaws.com/image.rengo.co.kr/car/#:data.car_index#/#:data.car_image#'/> " ,
							            	title: "사진",
							            	width: 70,
							            	filterable: false,
							            	sortable: false
							            },
							            {

							            	field: "car_name_detail",
							            	title: "모델명",
							            	width: 140
							            }, 
							            {
							            	field: "car_number",
							            	title: "차량번호",
							            	width: 140
							            },
							            // {
							            // 	field: "price_type",
							            // 	title: "요금",
							            // 	width: 100
							            // }, 
							             {
							            	field: "car_type",
							            	title: "분류",
							            	width: 100
							            },  
							            {
							            	field: "car_people",
							            	title: "정원",
							            	width: 100
							            },  {
							            	field: "color",
							            	title: "색상",
							            	width: 100
							            },  {
							            	field: "gear_type",
							            	title: "변속",
							            	width: 100
							            },  {
							            	field: "fuel_option",
							            	title: "연료",
							            	width: 100
							            },  {
							            	field: "release_year",
							            	title: "연식",
							            	width: 100
							            }, 
							            //  {
							            // 	field: "drive_distance",
							            // 	title: "주행",
							            // 	format: "{0:##,#}",
							            // 	width: 100
							            // },  {
							            // 	field: "rest_fuel",
							            // 	title: "잔여연료",
							            // 	format: "{0:##,#}",
							            // 	width: 100
							            // },  
							            {
							            	field: "car_register_number",
							            	title: "차대번호",
							            	width: 100
							            },  
							            {
							            	field: "option",
							            	title: "장착품",
							            	width: 200
							            },  
							            // {
							            // 	field: "car_manager",
							            // 	title: "차량담당",
							            // 	width: 100
							            // },  {
							            // 	field: "temp_number",
							            // 	title: "임시번호",
							            // 	width: 100
							            // },  
							            {
							            	field: "insurance_company",
							            	title: "보험회사",
							            	width: 100
							            },  
							            // {
							            // 	field: "total_insurance_money",
							            // 	title: "총보험료",
							            // 	format: "{0:##,#}원",
							            // 	width: 100
							            // }, 
							             {
							            	field: "insurance_age",
							            	title: "보험연령",
							            	width: 100
							            },  {
							            	field: "insurance_start_date",
							            	title: "보험시작일",
							            	format: "{0:yyyy/M/dd}",
							            	width: 100
							            },  {
							            	field: "insurance_end_date",
							            	title: "보험종료일",
							            	format: "{0:yyyy/M/dd}",
							            	width: 100
							            },  {
							            	field: "insurance_text",
							            	title: "특약사항",
							            	width: 100
							            },
							            // {
							            // 	field: "purchase_date",
							            // 	title: "구입일시",
							            // 	format: "{0:yyyy/M/dd}",
							            // 	width: 100
							            // },  {
							            // 	field: "release_date",
							            // 	title: "출고일시",
							            // 	format: "{0:yyyy/M/dd}",
							            // 	width: 100
							            // },  {
							            // 	field: "register_date",
							            // 	title: "등록일시",
							            // 	format: "{0:yyyy/M/dd}",
							            // 	width: 100
							            // },  {
							            // 	field: "purchase_type",
							            // 	title: "구입방식",
							            // 	width: 100
							            // },  {
							            // 	field: "car_money",
							            // 	title: "차량대금",
							            // 	format: "{0:##,#}원",
							            // 	width: 100
							            // },  {
							            // 	field: "installment_money",
							            // 	title: "할부원금",
							            // 	format: "{0:##,#}원",
							            // 	width: 100
							            // },
							            // {
							            // 	field: "installment_pay_day",
							            // 	title: "매월지급일",
							            // 	width: 100
							            // },  {
							            // 	field: "installment_monthly_money",
							            // 	title: "매월할부금",
							            // 	format: "{0:##,#}원",
							            // 	width: 100
							            // },
							            // {
							            // 	field: "location",
							            // 	title: "차량위치",
							            // 	width: 100
							            // },
							            // {
							            // 	field: "etc_text",
							            // 	title: "비고사항",
							            // 	width: 200
							            // }, 
							            //  {
							            // 	field: "register_user",
							            // 	title: "등록자",
							            // 	width: 100
							            // },  {
							            // 	field: "register_datetime",
							            // 	title: "등록일시",
							            // 	format: "{0: yyyy-MM-dd HH:mm:ss}",
							            // 	width: 100
							            // },  {
							            // 	field: "modify_user",
							            // 	title: "수정자",
							            // 	width: 100
							            // },  {
							            // 	field: "modify_datetime",
							            // 	title: "수정일시",
							            // 	format: "{0: yyyy-MM-dd HH:mm:ss}",
							            // 	width: 100
							            // }
							            ]
							        });

});

   var is_car_modify_mode = false;  //차량이 등록모드인지 수정 모드인지 구분
   var selected_tab = 1; // 선택 된 탭의 index
   var is_history_register = true; //내역 등록인지 수정인지, 수정은 더블클릭하면 수정임

   var car_insurance_start_date =''; //보험시작일
   var car_insurance_end_date =''; //보험종료일
   var check_date = ''; //점검 날짜
   var supply_date = '';
   var accident_time = ''; //사고 시간
   var accident_insurance_date = ''; //사고 접수일
   var repair_start_date = '';
   var repair_end_date = '';
   var emergency_time = '';
   
   var option_fuel;
   var option_gear;
   var option_smoking;
   var option_navi;
   var option_bluetooth;
   // var option_automatic_back_mirror;
   var option_hi_pass;
   var option_aux;
   var option_heatseat;
   // var option_gps;
   // var option_rear_sensor;
   var option_blackbox;
   var option_cdplayer;
   var option_backcamera;
   var option_sunroof;
   // var option_lane_departure_warning;
   // var option_automatic_seat;
   // var option_front_sensor;
   var option_smart_key;
   var option_usb;
   // var option_av_system;
   // var option_ecm;
   // var option_week_48;
   var option_weekend_48;
   var option_international_license;
   var option_second_driver;
   var option_license_type;
  

   var today = new Date();
   var excelFileName = "차량리스트_" + today.toISOString().substring(0, 10);
   var historyExcelFilename = "점검내역" +  today.toISOString().substring(0, 10) + ".xlsx"; 	

   //슈퍼 마스터가 아니면 회사 선택 select 안보임
   if($("#company_select option").size()==1){
   	$("#company_select").hide();
   }else{
   	$("#company_select").show();
   }
    $(document).ready(function() {


    });
   //차량 grid 초기화
   



// 차량 그리드 더블 클릭
$("#grid").delegate("tbody>tr", "dblclick",
	function(){
				$('.modal_scroll').css('height', $(window).height()-200);  

	 			$('#car_input_dialog').modal('toggle'); //다이얼로그 출력
	 			
	 			is_car_modify_mode = true;
	 			$("#car_info_history_none").css('display','block');
				//그리드에서 선택된게 있으면
				var carGrid = $("#grid").data("kendoGrid");
				var row = carGrid.select();
				var data = carGrid.dataItem(row);

				//저장된 데이터를 다이얼로그에 입력
				$('#btn_car_delete').show(); //삭제 버튼 보이기
				$("#dialog_title").text("차량 수정");
				$("#btn_car_save").text("수정");

				//선택된 차량 정보 출력
				$("#car_input_car_number").val(data.car_number);
				$("#car_input_car_type").val(data.car_type);
				$("#car_input_car_model").val(data.car_name_detail);
				$("#car_select_status").val(data.car_status).prop("selected", true);
				$("#car_input_car_register_number").val(data.car_register_number);
				$("#car_select_car_birth").val(data.release_year);
				$("#car_select_max_people").val(data.car_people);
				$("#car_input_color").val(data.color);
				// $("#car_select_branch").val(data.branch_serial).prop("selected", true);
				//초기화
				$("#car_select_maker option:eq(0)").prop("selected", true);
				$("#car_select_car_model1 option:eq(0)").prop("selected", true);
				$("#car_select_car_model2 option:eq(0)").prop("selected", true);

				//옵션 등록
				var text = '';
				$("#car_select_fuel").val(data.fuel_option);
				
				// switch(data.fuel_option){
				// 	case "휘발유":
				// 	case "가솔린":
				// 	$("#car_radio_fuel_gasoline").prop("checked", true);
				// 	$("#car_radio_fuel_disel").prop("checked", false);
				// 	$("#car_radio_fuel_lpg").prop("checked", false);
				// 	$("#car_radio_fuel_hybrid").prop("checked", false);
				// 	$("#car_radio_fuel_electronic").prop("checked", false);

				// 	break;
				// 	case "디젤":
				// 	case "경유":
				// 	$("#car_radio_fuel_gasoline").prop("checked", false);
				// 	$("#car_radio_fuel_disel").prop("checked", true);
				// 	$("#car_radio_fuel_lpg").prop("checked", false);
				// 	$("#car_radio_fuel_hybrid").prop("checked", false);
				// 	$("#car_radio_fuel_electronic").prop("checked", false);
				// 	break;
				// 	case "LPG":
				// 	$("#car_radio_fuel_gasoline").prop("checked", false);
				// 	$("#car_radio_fuel_disel").prop("checked", false);
				// 	$("#car_radio_fuel_lpg").prop("checked", true);
				// 	$("#car_radio_fuel_hybrid").prop("checked", false);
				// 	$("#car_radio_fuel_electronic").prop("checked", false);
				// 	break;
				// 	case "하이브리드":
				// 	$("#car_radio_fuel_gasoline").prop("checked", false);
				// 	$("#car_radio_fuel_disel").prop("checked", false);
				// 	$("#car_radio_fuel_lpg").prop("checked", false);
				// 	$("#car_radio_fuel_hybrid").prop("checked", true);
				// 	$("#car_radio_fuel_electronic").prop("checked", false);
				// 	break;
				// 	case "전기":
				// 	$("#car_radio_fuel_gasoline").prop("checked", false);
				// 	$("#car_radio_fuel_disel").prop("checked", false);
				// 	$("#car_radio_fuel_lpg").prop("checked", false);
				// 	$("#car_radio_fuel_hybrid").prop("checked", false);
				// 	$("#car_radio_fuel_electronic").prop("checked", true);
				// 	break;
				// } 
				option_fuel = data.fuel_option;
				text += "<span class='label label-default'>" + data.fuel_option +"</span>  ";


				$("#car_select_gear").val(data.gear_type);
				// switch(data.gear_type){
				// 	case "수동":

				// 	$("#car_radio_gear_auto").prop("checked", false);
				// 	$("#car_radio_gear_manual").prop("checked", true);
				// 	break;
				// 	case "자동":
				// 	case "오토":
				// 	default:
				// 	$("#car_radio_gear_auto").prop("checked", true);
				// 	$("#car_radio_gear_manual").prop("checked", false);
				// 	break;
				// }
				option_gear = data.gear_type;
				if(option_gear == null || option_gear == 'null'){
					option_gear = "자동";
				}

				text += "<span class='label label-default'>" + option_gear +"</span>  ";

				switch(data.smoking){
					case "Y":
					text += "<span class='label label-default'>흡연</span>  ";
					break;
					case "N":
					default:
					text += "<span class='label label-default'>금연</span>  ";
					break;
				}
				$('#car_select_smoking').val(data.smoking);
				option_smoking = data.smoking;

				text += "<span class='label label-default'>면허취득" + data.minimun_drive_year +"년이상</span>  ";
				$('#car_select_minimun_drive_year').val(data.minimun_drive_year);
				option_minimun_drive_year = data.minimun_drive_year;

				if(data.option_license1 =="Y"){
		        	text += "<span class='label label-default'>1종 보통면허 이상</span>  ";
		        	option_license1="Y";
		        }else{
		        	text += "<span class='label label-default'>2종 보통면허 이상</span>  ";
		        	option_license1="N";
		        }
		       	$('#car_select_license_type').val(data.option_license1);


				if(data.option_navi =="Y"){
					text += "<span class='label label-default'>네비게이션</span>  ";
					$('#car_checkbox_navi').prop("checked", true);
					option_navi="Y";
				}else{
					$('#car_checkbox_navi').prop("checked", false);
				}

				if(data.option_backcamera =="Y"){
		        	text += "<span class='label label-default'>후방카메라</span>  ";
		        	$('#car_checkbox_backcamera').prop("checked", true);
		        	option_backcamera="Y";
		        }else{
		        	$('#car_checkbox_backcamera').prop("checked", false);
		        }

		        if(data.option_blackbox =="Y"){
		        	text += "<span class='label label-default'>블랙박스</span>  ";
		        	$('#car_checkbox_blackbox').prop("checked", true);
		        	option_blackbox="Y";
		        }else{
		        	$('#car_checkbox_blackbox').prop("checked", false);
		        }

		        if(data.option_hi_pass =="Y"){
					text += "<span class='label label-default'>하이패스</span>  ";
					$('#car_checkbox_hi_pass').prop("checked", true);
					option_hi_pass="Y";
				}else{
					$('#car_checkbox_hi_pass').prop("checked", false);
				}

				if(data.option_bluetooth =="Y"){
					text += "<span class='label label-default'>블루투스</span>  ";
					$('#car_checkbox_bluetooth').prop("checked", true);
					option_bluetooth="Y";
				}else{
					$('#car_checkbox_bluetooth').prop("checked", false);
				}

				if(data.option_cdplayer =="Y"){
		        	text += "<span class='label label-default'>CD</span>  ";
		        	$('#car_checkbox_cdplayer').prop("checked", true);
		        	option_cdplayer="Y";
		        }else{
		        	$('#car_checkbox_cdplayer').prop("checked", false);
		        }
				// if(data.option_automatic_back_mirror =="Y"){
				// 	text += "<span class='label label-default'>전동백미러</span>  ";
				// 	$('#car_checkbox_automatic_back_mirror').prop("checked", true);
				// 	option_automatic_back_mirror="Y";
				// }else{
				// 	$('#car_checkbox_automatic_back_mirror').prop("checked", false);
				// }
				
				if(data.option_aux =="Y"){
					text += "<span class='label label-default'>AUX</span>  ";
					$('#car_checkbox_aux').prop("checked", true);
					option_aux="Y";
				}else{
					$('#car_checkbox_aux').prop("checked", false);
				}

				if(data.option_usb =="Y"){
		        	text += "<span class='label label-default'>USB</span>  ";
		        	$('#car_checkbox_usb').prop("checked", true);
		        	option_usb="Y";
		        }else{
		        	$('#car_checkbox_usb').prop("checked", false);
		        }


				if(data.option_heatseat =="Y"){
					text += "<span class='label label-default'>열선시트</span>  ";
					$('#car_checkbox_heatseat').prop("checked", true);
					option_heatseat="Y";
				}else{
					$('#car_checkbox_heatseat').prop("checked", false);
				}
		        // if(data.option_gps =="Y"){
		        //         text += "<span class='label label-default'>GPS</span>  ";
		        //         $('#car_checkbox_gps').prop("checked", true);
		        //         option_gps="Y";
		        // }else{
		        //         $('#car_checkbox_gps').prop("checked", false);
		        // }
		        // if(data.option_rear_sensor =="Y"){
		        // 	text += "<span class='label label-default'>후방감지기</span>  ";
		        // 	$('#car_checkbox_rear_senser').prop("checked", true);
		        // 	option_rear_sensor = "Y";
		        // }else{
		        // 	$('#car_checkbox_rear_senser').prop("checked", false);
		        // }
		        
		        if(data.option_sunroof =="Y"){
		        	text += "<span class='label label-default'>썬루프</span>  ";
		        	$('#car_checkbox_sunroof').prop("checked", true);
		        	option_sunroof="Y";
		        }else{
		        	$('#car_checkbox_sunroof').prop("checked", false);
		        }
		        // if(data.option_lane_departure_warning =="Y"){
		        // 	text += "<span class='label label-default'>차선이탈</span>  ";
		        // 	$('#car_checkbox_lane_departure_warning').prop("checked", true);
		        // 	option_lane_departure_warning ="Y";
		        // }else{
		        // 	$('#car_checkbox_lane_departure_warning').prop("checked", false);
		        // }
		        // if(data.option_automatic_seat =="Y"){
		        // 	text += "<span class='label label-default'>전동시트</span>  ";
		        // 	$('#car_checkbox_automatic_seat').prop("checked", true);
		        // 	option_automatic_seat="Y";
		        // }else{
		        // 	$('#car_checkbox_automatic_seat').prop("checked", false);
		        // }
		        // if(data.option_gps =="Y"){
		        // 	text += "<span class='label label-default'>GPS</span>  ";
		        // 	$('#car_checkbox_gps').prop("checked", true);
		        // 	option_gps="Y";
		        // }else{
		        // 	$('#car_checkbox_gps').prop("checked", false);
		        // }
		        // if(data.option_front_sensor =="Y"){
		        // 	text += "<span class='label label-default'>전방감지기</span>  ";
		        // 	$('#car_checkbox_front_senser').prop("checked", true);
		        // 	option_front_sensor="Y";
		        // }else{
		        // 	$('#car_checkbox_front_senser').prop("checked", false);
		        // }
		        if(data.option_smart_key =="Y"){
		        	text += "<span class='label label-default'>스마트키</span>  ";
		        	$('#car_checkbox_smart_key').prop("checked", true);
		        	option_smart_key="Y";
		        }else{
		        	$('#car_checkbox_smart_key').prop("checked", false);
		        }
		        // if(data.option_av_system =="Y"){
		        // 	text += "<span class='label label-default'>AV시스템</span>  ";
		        // 	$('#car_checkbox_av_system').prop("checked", true);
		        // 	option_av_system="Y";
		        // }else{
		        // 	$('#car_checkbox_av_system').prop("checked", false);
		        // }
		        // if(data.option_ecm =="Y"){
		        // 	text += "<span class='label label-default'>ECM</span>  ";
		        // 	$('#car_checkbox_ecm').prop("checked", true);
		        // 	option_ecm="Y";
		        // }else{
		        // 	$('#car_checkbox_ecm').prop("checked", false);
		        // }
		        // if(data.option_week_48 =="Y"){
		        // 	text += "<span class='label label-default'>주중48시간</span>  ";
		        // 	$('#car_checkbox_week_48').prop("checked", true);
		        // 	option_week_48="Y";
		        // }else{
		        // 	$('#car_checkbox_week_48').prop("checked", false);
		        // }
		        if(data.option_weekend_48 =="Y"){
		        	text += "<span class='label label-default'>주말48시간</span>  ";
		        	$('#car_checkbox_weekend_48').prop("checked", true);
		        	option_weekend_48="Y";
		        }else{
		        	$('#car_checkbox_weekend_48').prop("checked", false);
		        }

		        

		        if(data.option_second_driver =="Y"){
		        	text += "<span class='label label-default'>제2운전자등록가능</span>  ";
		        	$('#car_checkbox_second_driver').prop("checked", true);
		        	option_second_driver="Y";
		        }else{
		        	$('#car_checkbox_second_driver').prop("checked", false);
		        }

		        if(data.option_international_license =="Y"){
		        	text += "<span class='label label-default'>국제운전면허증가능</span>  ";
		        	$('#car_checkbox_international_license').prop("checked", true);
		        	option_international_license="Y";
		        }else{
		        	$('#car_checkbox_international_license').prop("checked", false);
		        }



		        $("#car_div_option").html(text);

		        //보험회사
		        $("#car_select_insurance_company").val(data.insurance_company);
		        $("#car_select_require_age").val(data.insurance_age);


		        



		        if(data.insurance_start_date!=null && data.insurance_start_date!="0000-00-00"){
		        	 $('#car_input_insurance_start_date_text').datepicker().data('datepicker').selectDate(data.insurance_start_date);
		        }
		        if(data.insurance_end_date!=null && data.insurance_end_date!="0000-00-00"){
		        	$('#car_input_insurance_end_date_text').datepicker().data('datepicker').selectDate(data.insurance_end_date);
		        }
		        
		        

		        $("#car_input_insurance_text").val(data.insurance_text);

		        // alert(data.insurance_man1_compensation);

		        if(data.insurance_man1_flag=="Y"){
		        	$("#car_checkbox_person1").prop('checked', true).change();
		        	// alert(data.insurance_man1_compensation);
		        	$("#car_insurance_person1_compensation").val(data.insurance_man1_compensation).prop("selected", true);
		        	$("#car_insurance_person1_customer").val(data.insurance_man1_customer);
		        }else{
		        	// alert('o');
		        	$("#car_checkbox_person1").prop('checked', false).change();
		        	$("#car_insurance_person1_compensation option:eq(0)").prop("selected", true);
		        	$("#car_insurance_person1_customer").val();
		        }
		        if(data.insurance_man2_flag=="Y"){
		        	$("#car_checkbox_person2").prop("checked", true).change();
		        	$("#car_insurance_person2_compensation").val(data.insurance_man2_compensation).prop("selected", true);
		        	$("#car_insurance_person2_customer").val(data.insurance_man2_customer);
		        }else{
		        	$("#car_checkbox_person2").prop("checked", false).change();
		        	$("#car_insurance_person2_compensation option:eq(0)").prop("selected", true);
		        	$("#car_insurance_person2_customer").val();
		        }
		        if(data.insurance_object_flag=="Y"){
		        	$("#car_checkbox_object").prop("checked", true).change();
		        	$("#car_insurance_object_compensation").val(data.insurance_object_compensation).prop("selected", true);
		        	$("#car_insurance_object_customer").val(data.insurance_object_customer);
		        }else{
		        	$("#car_checkbox_object").prop("checked", false).change();
		        	$("#car_insurance_object_compensation option:eq(0)").prop("selected", true);
		        	$("#car_insurance_object_customer").val();
		        }
		        if(data.insurance_children_flag=="Y"){
		        	$("#car_checkbox_children").prop("checked", true).change();
		        	$("#car_insurance_children_compensation").val(data.insurance_children_compensation).prop("selected", true);
		        	$("#car_insurance_children_customer").val(data.insurance_children_customer);
		        }else{
		        	$("#car_checkbox_children").prop("checked", false).change();
		        	$("#car_insurance_children_compensation option:eq(0)").prop("selected", true);
		        	$("#car_insurance_children_customer").val();
		        }
		        if(data.insurance_self_flag=="Y"){
		        	$("#car_checkbox_self1").prop("checked", true).change();
		        	$("#car_insurance_self1_pay").val(data.insurance_self_price);
		        	$("#car_insurance_self1_compensation").val(data.insurance_self_compensation).prop("selected", true);
		        	$("#car_insurance_self1_customer").val(data.insurance_self_customer);
		        }else{
		        	$("#car_checkbox_self1").prop("checked", false).change();
		        	$("#car_insurance_self1_pay").val();
		        	$("#car_insurance_self1_compensation option:eq(0)").prop("selected", true);
		        	$("#car_insurance_self1_customer").val();
		        }
		        if(data.insurance_self2_flag=="Y"){
		        	$("#car_checkbox_self2").prop("checked", true).change();
		        	$("#car_insurance_self2_pay").val(data.insurance_self2_price);
		        	$("#car_insurance_self2_compensation").val(data.insurance_self2_compensation).prop("selected", true);
		        	$("#car_insurance_self2_customer").val(data.insurance_self2_customer);
		        }else{
		        	$("#car_checkbox_self2").prop("checked", false).change();
		        	$("#car_insurance_self2_pay").val();
		        	$("#car_insurance_self2_compensation option:eq(0)").prop("selected", true);
		        	$("#car_insurance_self2_customer").val();
		        }
		        if(data.insurance_self3_flag=="Y"){
		        	$("#car_checkbox_self3").prop("checked", true).change();
		        	$("#car_insurance_self3_pay").val(data.insurance_self3_price);
		        	$("#car_insurance_self3_compensation").val(data.insurance_self3_compensation).prop("selected", true);
		        	$("#car_insurance_self3_customer").val(data.insurance_self3_customer);
		        }else{
		        	$("#car_checkbox_self3").prop("checked", false).change();
		        	$("#car_insurance_self3_pay").val();
		        	$("#car_insurance_self3_compensation option:eq(0)").prop("selected", true);
		        	$("#car_insurance_self3_customer").val();
		        }

		       	//요금정보

		       	$("#car_input_week_price").val(data.week_price);
		        $("#car_input_weekend_price").val(data.weekend_price);
		        $("#car_input_week_price_6").val(data.week_price_6);
		        $("#car_input_weekend_price_6").val(data.weekend_price_6);
		        $("#car_input_week_price_1").val(data.week_price_1);
		        $("#car_input_weekend_price_1").val(data.weekend_price_1);

		        ///////////////////////////////////////////////
		   //      $("#input_6week_cal").val('');
 				// $("#input_6week_result").val('');
 				// $("#input_6weekend_cal").val('');
 				// $("#input_6weekend_result").val('');
 				// $("#input_1week_cal").val('');
 				// $("#input_1week_result").val('');
 				// $("#input_1weekend_cal").val('');
 				// $("#input_1weekend_result").val('');
		        // $("#car_input_day_price").val(data.day_price);
		        // $("#car_input_hour_price").val(data.hour_price);


				//history grid 초기화
				$("#tab_check_history").trigger({ type: "click" });
			});

//차량 리스트 검색
$("#searchbox").keyup(function () {
	var val = $('#searchbox').val();
                // alert(val);
                $("#grid").data("kendoGrid").dataSource.filter({
                	logic: "or",
                	filters: [
                	{
                		field: "car_status",
                		operator: "contains",
                		value: val
                	},
                	{
                		field: "car_number",
                		operator: "contains",
                		value: val
                	},
                	{
                		field: "rent_status",
                		operator: "contains",
                		value:val
                	},
                	{
                		field: "car_type",
                		operator: "contains",
                		value:val
                	},
                	{
                		field: "car_name_detail",
                		operator: "contains",
                		value:val
                	},
                	{
                		field: "color",
                		operator: "contains",
                		value:val
                	},
                	{
                		field: "gear_type",
                		operator: "contains",
                		value:val
                	},
                	{
                		field: "fuel_option",
                		operator: "contains",
                		value:val
                	},
                	{
                		field: "car_register_number",
                		operator: "contains",
                		value:val
                	},
                	{
                		field: "option",
                		operator: "contains",
                		value:val
                	},
                	{
                		field: "temp_number",
                		operator: "contains",
                		value:val
                	},
                	]
                });


            });





//////////////////////////////////////////////////////////////////////////////////////  버튼 이벤트 
//차 등록 버튼
$("#btn_register").click(function(){




	company_serial = get_company_serial();

	if(company_serial == 0){
		alert("업체를 먼저 선택해 주세요.");
		return false;
	}

	option_fuel ='';
	option_gear ='';
	option_smoking = '';
	option_navi ='';
	option_bluetooth ='';
	// option_automatic_back_mirror ='';
	option_hi_pass ='';
	option_aux ='';
	option_heatseat ='';
	// option_gps ='';
	// option_rear_sensor ='';		
	option_blackbox ='';
	option_cdplayer='';
	option_backcamera='';
	option_sunroof='';
	// option_lane_departure_warning='';
	// option_automatic_seat='';
	// option_front_sensor='';
	option_smart_key='';
	option_av_system='';
	// option_ecm='';
	// option_week_48='';
	option_weekend_48='';
	option_usb='';
    option_second_driver='';
    option_international_license='';
    option_minimun_drive_year='';
    option_license_type='';

	$('.modal_scroll').css('height', 630);  

	is_car_modify_mode = false;


	$('#car_input_dialog').modal('toggle'); //다이얼로그 출력
	//밑에 grid 안보여줌
	$("#car_info_history_none").css('display','none');

	$('#btn_car_delete').hide(); //삭제 버튼 보이기
	$("#dialog_title").text("차량 등록");
	$("#btn_car_save").text("저장");

	//다이얼로그 값 초기화

	//기본정보 초기화
	$("#car_input_car_number").val('');
	$("#car_input_car_type").val('');
	$("#car_input_car_model").val('');
	$("#car_select_maker option:eq(0)").prop("selected", true);
	$("#car_select_car_model1 option:eq(0)").prop("selected", true);
	$("#car_select_car_model2 option:eq(0)").prop("selected", true);
	$("#car_input_car_register_number").val('');
	$("#car_select_max_people option:eq(0)").prop("selected", true);
	$("#car_select_car_birth option:eq(0)").prop("selected", true);
	$("#car_input_color").val('');

	//세부정보 초기화
	$("#car_div_option").html('');
	$("#car_select_fuel option:eq(0)").prop("selected", true);
	$("#car_select_gear option:eq(0)").prop("selected", true);
	$("#car_select_smoking option:eq(0)").prop("selected", true);
	$("#car_select_minimun_drive_year option:eq(0)").prop("selected", true);
	$("#car_select_license_type option:eq(0)").prop("selected", true);
	//옵션들
	$("#car_checkbox_navi").prop("checked", false);
	$("#car_checkbox_backcamera").prop("checked", false);
	$("#car_checkbox_blackbox").prop("checked", false);	
	$("#car_checkbox_hi_pass").prop("checked", false);	
	$("#car_checkbox_bluetooth").prop("checked", false);
	$("#car_checkbox_cdplayer").prop("checked", false);
	$("#car_checkbox_aux").prop("checked", false);
	$("#car_checkbox_usb").prop("checked", false);
	$("#car_checkbox_heatseat").prop("checked", false);
	$("#car_checkbox_sunroof").prop("checked", false);	
	$("#car_checkbox_smart_key").prop("checked", false);
	$("#car_checkbox_weekend_48").prop("checked", false);
	$("#car_checkbox_international_license").prop("checked", false);
	$("#car_checkbox_second_driver").prop("checked", false);


	// $("#car_checkbox_av_system").prop("checked", false);
	// $("#car_checkbox_automatic_back_mirror").prop("checked", false);
	// $("#car_checkbox_automatic_seat").prop("checked", false);
	// $("#car_checkbox_gps").prop("checked", false);
	// $("#car_checkbox_rear_senser").prop("checked", false);
	// $("#car_checkbox_front_senser").prop("checked", false);
	// $("#car_checkbox_lane_departure_warning").prop("checked", false);
	// $("#car_checkbox_ecm").prop("checked", false);
	// $("#car_checkbox_week_48").prop("checked", false);

		//요금 정보 초기화
	$("#car_input_week_price").val('');
	$("#car_input_weekend_price").val('');
	$("#car_input_week_price_6").val('');
	$("#car_input_weekend_price_6").val('');
	$("#car_input_week_price_1").val('');
	$("#car_input_weekend_price_1").val('');

	//보험 정보 초기화
	$("#car_select_insurance_company option:eq(0)").prop("selected", true);
	$("#car_select_require_age option:eq(0)").prop("selected", true);
 	$('#car_input_insurance_start_date_text').datepicker().data('datepicker').date = new Date();
 	$('#car_input_insurance_start_date_text').val('');
 	$('#car_input_insurance_end_date_text').datepicker().data('datepicker').date = new Date();
 	$('#car_input_insurance_end_date_text').val('');
	$("#car_input_insurance_text").val('');

	$("#car_checkbox_person1").prop('checked', true).change();
	$("#car_checkbox_person2").prop("checked", false).change();
	$("#car_checkbox_object").prop("checked", true).change();
	$("#car_checkbox_children").prop("checked", true).change();
	$("#car_checkbox_self1").prop("checked", false).change();
	$("#car_checkbox_self2").prop("checked", false).change();
	$("#car_checkbox_self3").prop("checked", false).change();


	//보험정보
	$('#car_insurance_person1_compensation').val('');
	$('#car_insurance_person1_customer').val('');
	$('#car_insurance_person2_compensation').val('');
	$('#car_insurance_person2_customer').val('');
	$('#car_insurance_object_compensation').val('');
	$('#car_insurance_object_customer').val('');
	$('#car_insurance_children_compensation').val('');
	$('#car_insurance_children_customer').val('');
	$('#car_insurance_self1_pay').val('');
	$('#car_insurance_self1_compensation').val('');
	$('#car_insurance_self1_customer').val('');
	$('#car_insurance_self2_pay').val('');
	$('#car_insurance_self2_compensation').val('');
	$('#car_insurance_self2_customer').val('');
	$("#car_input_max_people").val('');


	$("#car_input_week_price").val('');
 	$("#car_input_weekend_price").val('');
	$("#input_6week_cal").val('');
 	$("#input_6week_result").val('');
 	$("#input_6weekend_cal").val('');
 	$("#input_6weekend_result").val('');
 	$("#input_1week_cal").val('');
 	$("#input_1week_result").val('');
 	$("#input_1weekend_cal").val('');
 	$("#input_1weekend_result").val('');




 	//modal 스크롤 제일 위로
 	// $("#modal_content").scrollTop(0);
});




function saveExcel(){
	var grid = $("#grid").data("kendoGrid");
	grid.saveAsExcel();
}


/////////////////////////////////////////////////////// tab click 이벤트



$("#tab_car1").click(function() {
	tab_click("전체");
});

$("#tab_car2").click(function() {
	tab_click("대형");
});
$("#tab_car3").click(function() {
	tab_click("중형");
});

$("#tab_car4").click(function() {
	tab_click("준중형");
});

$("#tab_car5").click(function() {
	tab_click("소형");
});

$("#tab_car6").click(function() {
	tab_click("경형");
});

$("#tab_car7").click(function() {
	tab_click("SUV");
});
$("#tab_car8").click(function() {
	tab_click("승합");
});
$("#tab_car9").click(function() {
	tab_click("수입");
});


function tab_click(type) {

	var company_serial = get_company_serial();

	// var branch_serial = $("#car_branch_select option:selected").val();
	// var branch_serial = 0;

	if(type =='전체'){
		type = '';
		$("#grid").data("kendoGrid").dataSource.filter({
			logic: "or",
			filters: [
			{
				field: "car_type",
				operator: "contains",
				value: type
			}]
		});
	}else{
		$("#grid").data("kendoGrid").dataSource.filter({
			logic: "or",
			filters: [
			{
				field: "car_type",
				operator: "eq",
				value: type
			}]
		});
	}
}




//////////////////////////////////////////////////////////////// 파트너 선택 리스너

$( "#company_select" ).change(function() {

	var selectedSerial = $(this).val();
 	//1. 그리드 refresh
 	grid_refresh(selectedSerial);
 	//3. count refresh
 	getCount();
 	

 });

function getCount(){
	var company_serial = get_company_serial();
	$.get("/carmanager/get_counts/"+company_serial,
 		function(data){
				// alert(data);
				var result = JSON.parse(data);

				$("#car_div_all").text(result.all+"대");
				$("#car_div_run").text(result.run+"대");
				$("#car_div_no_run").text(result.all-result.run+"대");
				
	});

}




function grid_refresh(company_serial, type){

	var dataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "http://solution.rengo.co.kr/carmanager/get_list/" + company_serial,
				dataType: "json"
			}
		},
		pageSize: 20
	});
	var grid = $("#grid").data("kendoGrid");
	grid.setDataSource(dataSource);

	//2. count refresh
}


function zeroPad(num, places) {
	var zero = places - num.toString().length + 1;
	return Array(+(zero > 0 && zero)).join("0") + num;
}



</script>

<script>
$('#car_input_dialog, #car_registration_option_dialog, #car_car_select_btn, #history_check_dialog, #search_vehicles_dialog, #history_supply_dialog, #history_accident_dialog, #history_repair_dialog, #history_emergency_dialog, #dialog_blacklist_input').draggable({
	handle: ".modal-header"
});



$('.modal-lg').on('hidden.bs.modal',function() {
  $('.modal_scroll').scrollTop(0);
  setTimeout(function(){
    $('.modal').css({
      'top':'0',
      'left':'0'
    });

    },30)
})
</script>  

