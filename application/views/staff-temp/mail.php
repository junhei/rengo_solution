<style type="text/css">
.mail-hr {
	border-top:1px solid #d6d6d6;
}
.wd600 {
	width:600px;
}
.hi200 {
	height:200px;
}
.mg-auto {
	margin:auto;
}
.bg-color-rengo {
	background-color: #ffe80e;
}
.mgl30 {
	margin-left:30px;
}
.pdt30 {
	padding-top:30px;
}
.pdt40 {
	padding-top:40px;
}
.pd0 {
	padding:0;
}
.mgt40 {
	margin-top: 40px; 
}
.mgb40 {
	margin-bottom: 40px;
}
.mgr30 {
	margin-right: 30px;
}
.fw100{
	font-weight: 100;
}
.footer {
   padding-top:0;
   padding-bottom:0;
}
.mail{
	width:100%;
	height:100%;
	max-width: 600px;
}
.header-mail{
	width:100%;
	max-width: 600px;
	position: relative;
}
.bg-white {
	background-color: white;
}
.mg-auto {
	padding:5%;
}	
.header-line-mail{}

@media screen and (max-width: 600px) {
.mail {
	width:100%;
}
.mg-auto {
	padding:5%;
}	
.header-line-mail{padding-bottom: 120px !important;}
}
/* footer */
#footer h2 {text-align:center; padding:60px 0 20px}
#footer h2 img {width:150px; height:auto; }
#footer ul {text-align:center;/* margin-bottom:50px; */}
#footer ul li {display:inline-block; padding:0 4px;}
#footer #foot_info {text-align:center;}
#footer #foot_info span {display:inline-block !important; position:relative !important; padding:0 12px 0 9px !important; line-height:22px !important;}
#footer #foot_info span:after {position:absolute !important; top:7px !important; right:0 !important; height:10px !important; width:1px !important; background:#888 !important; content:''}
#footer #foot_info span.bgn::after {display:none !important;}
#footer #copyright {text-align:center !important; margin-top:5px !important; }
</style>
<!DOCTYPE html>
<html>
<head>
    <title>RENGO SOLUTION</title>
</head>
<body>

    <div style="width:100%; height:100%; max-width: 600px; padding:30px; margin:0 auto;">
		<header style="background-color: #ffe80e; width:100%; max-width: 600px; position: relative;">
			<p class="header-line-mail" style="font-size:34px;padding:30px 30px 90px 30px; line-height: 1.2em; letter-spacing: -0.9px; display: block;">
				<b>새로운 기기</b> 에서<br>
				로그인 하였습니다.
			</p>
			<span style="position: absolute; right: 30px; bottom:30px;">
				<img src="http://solution.rengo.co.kr/img/L1_w.png" class="" width="60" height="60">
			</span>
		</header> 
		
		<div class="pdt40 mgb40" style="padding-top:40px; margin-bottom: 40px;">
			<hr class="mail-hr" style="border-top:1px solid #d6d6d6;">
		</div>

		<div>
			<h2 class="mgb40" style="margin-bottom: 40px;">
				<b><font color="#ffe80e">새로운 기기</font></b> 에서<br>
				로그인 하였습니다.<br>
			</h2>

			<p>
				안녕하세요. Rengo 입니다.<br>
				<b>변수</b>님의 본인 인증 번호는 [<b>변수</b>] 입니다.<br>
			</p>

			<p class="mgb40" style="margin-bottom: 40px;">
				RENGO을 이용해 주셔서 감사합니다.<br>
				더욱 편리한 서비스를 제공하기 위해 항상 최선을 다하겠습니다.<br>
			</p>			
		</div>

		<div class="" style="margin-top:40px; margin-bottom: 40px;">
			<hr style="style="border-top:1px solid #d6d6d6;"">
		</div>
		
		<div id="footer" class="section text-center bg-white" 
			style="padding:0;background-color: #fff; text-align: center; background: #fff;font-size:14px;color:#aaa;padding:60px 20px;letter-spacing:-0.5px;">
			<div id="foot_info" style="text-align: center;">
				<span>(주)렌고</span><span>대표:이승원</span><span class="bgn">사업자번호:302-81-29052</span><br>
				<span class="bgn">통신판매업신고 : 제2016-부산금정-0119호</span><br>
				<span class="bgn">개인정보담당자 : swlee@rengo.co.kr</span><br>
				<span class="bgn fc_f50">고객센터 : 1800-1090</span><br>
			</div>
			<p id="copyright">ⓒ 2016 rengo All rights reserved.</p>
			<ul style="padding: 0; margin:0 0 30px 0;">
				<li><a href="https://www.facebook.com/rengopage" target="_blank"><img src="http://rengo.co.kr/new/img/img_pc//icon_facebook.png" alt="페이스북"></a></li>								
				<li><a href="http://blog.naver.com/rengo_blog" target="_blank"><img src="http://rengo.co.kr/new/img/img_pc//icon_blog.png" alt="블로그"></a></li>
			</ul>
		</div>
    </div>

    <table style="max-width: 600px; width: 100%; margin: 0 auto; padding: 0 15px; font-family: '나눔고딕', '나눔바른고딕', '돋움', '고딕';" cellpadding="0" border="0" cellspacing="0">
    	<tr>
    		<td>
			    <table style="width: 100%;" cellpadding="0" border="0" cellspacing="0">
			    	<tr style="background-color:#ffe80e; font-family: '나눔고딕', '나눔바른고딕', '돋움', '고딕';">
			    		<td style="color: #252525; width: 100%; padding: 30px 30px 15px 30px; font-size:34px; font-family: '나눔고딕', '나눔바른고딕', '돋움', '고딕';"><b>렌고의 파트너</b>가<br /> 되신것을 환영합니다!</td>
			    	<tr>
			    </table>
		    </td>
	    </tr>
	    <tr>
	    	<td>
	    		<table style="width: 100%;" cellpadding="0" border="0" cellspacing="0">
			    	<tr style="background-color:#ffe80e; ">
			    		<td style="padding: 0 30px 30px 30px; text-align: right; font-family: '나눔고딕', '나눔바른고딕', '돋움', '고딕';">
			    			<img src="http://solution.rengo.co.kr/img/L1_w.png" class="" width="60" height="60">
			    		</td>
			    	<tr>
			    </table>
	    	</td>
	    </tr>
	    <tr>
	    	<td style="padding:40px 0 0 0; font-family: '나눔고딕', '나눔바른고딕', '돋움', '고딕';"></td>
	    </tr>
	    <tr>
	    	<td style="border-top:1px solid #d6d6d6; padding:40px 0 0 0; font-family: '나눔고딕', '나눔바른고딕', '돋움', '고딕';"></td>
	    </tr>
	    <tr>
	    	<td style="font-size:34px; color: #252525; font-weight: bold; padding: 0 0 50px 0; font-family: '나눔고딕', '나눔바른고딕', '돋움', '고딕';">
	    		<span style="color: #ffe80e;">최종 승인을</span> 하기<br /> 위해서는 아래의 url을 클릭해주세요.
	    	</td>
	    </tr>
	    <tr>
	    	<td style="font-size:14px; color: #4b4b4b; padding: 0 0 30px 0; font-family: '나눔고딕', '나눔바른고딕', '돋움', '고딕';">
	    		안녕하세요. Rengo 입니다.<br />
				<b>wogus4041@naver.com</b>님의 본인 인증 번호는 [<b>77777</b>] 입니다.
	    	</td>
	    </tr>
	    <tr>
	    	<td style="font-size:14px; color: #4b4b4b; padding: 0 0 40px 0; font-family: '나눔고딕', '나눔바른고딕', '돋움', '고딕';">
	    		RENGO을 이용해 주셔서 감사합니다.<br />
				더욱 편리한 서비스를 제공하기 위해 항상 최선을 다하겠습니다.
	    	</td>
	    </tr>
	    <tr>
	    	<td style="border-top:1px solid #d6d6d6; padding:40px 0 0 0; font-family: '나눔고딕', '나눔바른고딕', '돋움', '고딕';"></td>
	    </tr>
	    <tr>
	    	<td style="font-size:14px; color:#aaa; padding-bottom: 15px; text-align: center; font-weight: normal; font-family: '나눔고딕', '나눔바른고딕', '돋움', '고딕';">
	    		(주)렌고 | 대표:이승원 | 사업자번호:302-81-29052<br />
	    		통신판매업신고 : 제2016-부산금정-0119호<br />
	    		개인정보담당자 : swlee@rengo.co.kr<br />
	    		고객센터 : 1800-1090<br />
	    		ⓒ 2016 rengo All rights reserved.
	    	</td>
	    </tr>
	    <tr>
	    	<td style="padding-bottom: 30px;">
	    		<table style="width: 100%;">
		    		<tr>
		    			<td style="text-align: center;">
		    				<a href="https://www.facebook.com/rengopage" target="_blank"><img src="http://rengo.co.kr/new/img/img_pc//icon_facebook.png" alt="페이스북"></a>
		    				<a href="http://blog.naver.com/rengo_blog" target="_blank"><img src="http://rengo.co.kr/new/img/img_pc//icon_blog.png" alt="블로그"></a>
		    			</td>
		    		</tr>
	    		</table>
	    	</td>
	    </tr>
    </table>
</body>
</html>




