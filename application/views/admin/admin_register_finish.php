<!DOCTYPE html>
<html>
<head>
    <title>RENGO SOLUTION</title>
    <script src="/js/jquery-1.9.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap-toggle.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/rengo_intro.css?53939">
    <style type="text/css"> 

    </style>
</head>
<body>
    <header>
        <div id="header">
            <div class="header_inner">
                <h1 class="title" onclick="go_to_login()">렌고 파트너 전용 서비스</h1>
            </div>
        </div>
    </header>
    <div class="register_finish_wrap">
            <div class="content_wrap">
                <div class="row register">
                    <div class="col-lg-12">
                        <div class="ibox-title row">
                            <div>
                                <h5>신청완료</h5>
                                <h6>가입하신 정보는 회원님의 동의 없이 공개되지 않으며, 개인정보 보호정책에 의해 보호를 받습니다.</h6>
                            </div>
                        </div>
                        <div>
                            <ul class="register_tab">
                                <li>01. 약관동의</li>
                                <li>02. 정보입력</li>
                                <li class="on">03. 신청완료</li>
                            </ul>
                        </div>
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div method="get" class="form-horizontal">
                                    <div class="register_finish">
    								    <div class="register_icon01">회원가입 아이콘</div>
                                        <div class="ibox-title">
                                            <h5><span>파트너 신청이 완료</span>되었습니다.</h5>
                                        </div>
                                        <div class="hr_line"></div>
                                        <p>영업 담당자가 확인 후 </p>
                                        <p>빠르게 연락드리겠습니다.</p>
                                        <p>감사합니다.</p>
                                    </div>
                                </div>
                                <button class="finish_btn" id="btn_register">확인</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>
</html>
<script>
$("#btn_register").click(function(){
    location.replace('/');
});

function go_to_login(){
    location.replace('/');
}
</script>
