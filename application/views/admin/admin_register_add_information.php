<!DOCTYPE html>
<html>
<head>
    <title>RENGO SOLUTION</title>
    <script src="/js/jquery-1.9.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap-toggle.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    <link rel="stylesheet" href="/css/rengo_intro.css?53939">


<script type="text/javascript">
$(function () {
    $('#partner_time_st').datetimepicker({
        locale: 'ko',
        format: 'HH:mm',
        stepping: 30,
    });
});
</script>

<script type="text/javascript">
$(function () {
    $('#partner_time_end').datetimepicker({
      locale: 'ko',
      format: 'HH:mm',
      stepping: 30,
  });
});
</script>

<script language="javascript">
$(document).ready(function(){

    initModal();
});

function initModal() {
    $('.modal').on('hidden.bs.modal',function() {
        $('.modal_scroll').scrollTop(0);
        setTimeout(function(){
            $('.modal').css({
              'top':'0',
              'left':'0'
            });
        },30)
    });
//    $('.modal').draggable();
}

</script>
</head>
<body>

    <?
    if($error){
        echo $error;
    }

    ?>
    <header>
        <div id="header">
            <div class="header_inner">
                <h1 class="title" onclick="go_to_login()">렌고 파트너 전용 서비스</h1>
            </div>
        </div>
    </header>
<!-- map 모달 -->
<div class="modal" id="dialog_map" role="dialog">
	<div class="modal-dialog modal-sm" style="width:800px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 id="dialog_title" class="modal-title">지도</h4>
			</div>

				<div class="modal-body">
					<div id="naver_map" style="width:100%;height:350px;"></div>
				</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
			</div>

		</div>
	</div>
</div>

    <div class="content_wrap">
        <div class="row register">
            <div class="col-lg-12">
                <div class="ibox-title row">
                    <div>
                        <h5>정보입력</h5>
                        <h6>가입하신 정보는 회원님의 동의 없이 공개되지 않으며, 개인정보 보호정책에 의해 보호를 받습니다.</h6>
                    </div>
                </div>
                <div>
                    <ul class="register_tab">
                        <li>01. 약관동의</li>
                        <li class="on">02. 정보입력</li>
                        <li>03. 신청완료</li>
                    </ul>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div method="get" class="form-horizontal">
                         <form method="post" action="/registermanager/file_upload" enctype="multipart/form-data" id="registermanager_form">
                            <div class="form-group"><label class="col-sm-2 control-label">*아이디(이메일)</label>

                                <div class="col-sm-5"><input type="text" class="form-control input-lg" id="email" placeholder="이메일주소를 입력해 주세요."></div>
                               <!--  <div class="col-sm-1 pdl0" >
                                    <button id="email_confirm" class="bt visible-inline-block" >중복확인</button>
                                </div> -->
                                <div class="col-sm-3">
                                    <!-- <p id="email_dup_text" class="no-margins lh46"></p> -->
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">*비밀번호</label>
                                <div class="col-sm-5"><input type="password" class="form-control input-lg" id="password" maxlength="20" placeholder="6자리이상, 영문과 숫자를 사용하세요."></div>
                                <div class="col-sm-3">
                                    <!-- <p id="password_str" class="no-margins lh46"></p> -->
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">*비밀번호 확인</label>
                                <div class="col-sm-5"><input type="password" class="form-control input-lg" id="password_confirm" maxlength="20" placeholder="한번 더 입력해 주세요."></div>
                                <div class="col-sm-3">
                                    <!-- <p id="password_check" class="no-margins lh46"></p> -->
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">*파트너(상호)명</label>

                                <div class="col-sm-5"><input type="text" class="form-control input-lg" id="company_name" placeholder="예)000렌터카 00지점"></div>
                            </div>


                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">*대표번호</label>
                                <div class="col-sm-5"><input type="text" class="form-control input-lg" id="representative_number" maxlength="20" placeholder="예)18001090( - 제외)"></div>
                            </div>


                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" >*팩스번호</label>

                                <div class="col-sm-5">
                                    <ul class="call_number">
                                        <li>
                                            <select class="form-control input-lg" id="ffirst_number">
                                                <option value="02">02</option>
                                                <option value="031">031</option>
                                                <option value="032">032</option>
                                                <option value="033">033</option>
                                                <option value="051">051</option>
                                                <option value="052">052</option>
                                                <option value="053">053</option>
                                                <option value="054">054</option>
                                                <option value="055">055</option>
                                                <option value="041">041</option>
                                                <option value="042">042</option>
                                                <option value="043">043</option>
                                                <option value="061">061</option>
                                                <option value="062">062</option>
                                                <option value="063">063</option>
                                                <option value="064">064</option>
                                                <option value="064">070</option>
                                            </select>
                                        </li>
                                        <li>
                                            <input type="text" class="form-control input-lg" maxlength="4" id="fmiddle_number">        
                                        </li>
                                        <li>
                                            <input type="text" class="form-control input-lg" maxlength="4" id="flast_number">
                                        </li>
                                    </ul>
                                </div>
                            </div>


                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">*주소</label>

                                <div class="col-sm-3"><input type="text" class="form-control input-lg" id="postcode" maxlength="5" disabled placeholder="우편번호를 검색해 주세요."></div>
                                <div class="col-sm-3 pdl0">
                                    <a role="button" class="bt" type="button" onclick="execDaumPostcode()" style="margin-bottom: 5px;" >검색</a>
                                    <a id="map_show_btn" type="button" class="bt" data-toggle="modal" data-target="#dialog_map" style="margin-left:12px; margin-bottom: 5px;">지도</a>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-5 col-sm-offset-2">
                                    <input type="text" class="form-control input-lg" id="address" placeholder="주소를 입력해주세요.">
                                </div>
                            </div>



<!--                             <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">*영업시간</label>
                                <div class="col-sm-6">
                                    <ul class="no-margins no-padding no-liststyle">
                                        <li class="no-margins no-padding no-borderradius pull-left opening-hours">
                                            <div class='input-group date' id='partner_time_st'>
                                                <input type='text' id="partner_time_st_input" class="form-control input-lg" maxlength="5" placeholder="개점 시간">
                                                <span class="input-group-addon no-borderradius">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                        </li>
                                        <li class="pull-left no-margins no-padding text-center between-hours" >
                                            ~
                                        </li>
                                        <li class="no-margins no-padding no-borderradius pull-left opening-hours">
                                            <div class='input-group date' id='partner_time_end'>
                                                <input type='text' id="partner_time_end_input" class="form-control input-lg" maxlength="5" placeholder="폐점 시간">
                                                <span class="input-group-addon no-borderradius">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>


                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">*시간당 배차가능횟수</label>
                                <div class="col-sm-4 delivery">
                                    <li class="percent no-liststyle">
                                        <input type='text' id="delivery_max_count" class="form-control input-lg pdr23 text-right" placeholder="10이하의 양수" maxlength="2"  >
                                        <span>건</span>
                                    </li>
                                </div>
                            </div> -->


                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">*대표자명</label>
                                <div class="col-sm-4"><input type="text" class="form-control input-lg" id="representative" ></div>
                            </div>


                            <div class="hr-line-dashed"></div>

                            <div class="form-group"><label class="col-sm-2 control-label" >*대표자 연락처</label>

                                <div class="col-sm-5">
                                    <ul class="call_number">
                                        <li> 
                                            <select class="form-control input-lg" id="rfirst_number">
                                                <option value="010">010</option>
                                                <option value="011">011</option>
                                                <option value="016">016</option>
                                                <option value="017">017</option>
                                                <option value="019">019</option>
                                            </select>
                                        </li>
                                        <li>
                                            <input type="text" class="form-control input-lg" maxlength="4" id="rmiddle_number">        
                                        </li>
                                        <li>
                                            <input type="text" class="form-control input-lg" maxlength="4" id="rlast_number" >
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">*법인등록번호</label>
                                    <div class="col-sm-5">
                                        <ul class="call_number">
                                        <li>
                                            <input type="text" class="form-control input-lg" maxlength="6" id="corporation_first_number">
                                        </li>
                                        <li>
                                            <input type="text" class="form-control input-lg" maxlength="7" id="corporation_last_number" >
                                        </li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-2 control-label">*사업자등록번호</label>
                                        <div class="col-sm-5">
                                            <ul class="call_number">
                                                <li>
                                                    <input type="text" class="form-control input-lg" maxlength="3" id="cr_first_number">
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control input-lg" maxlength="2" id="cr_middle_number" >
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control input-lg" maxlength="5" id="cr_last_number" >
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">사업자등록증 업로드</label>
                                        <div class="filebox">
                                            <div class="col-sm-5">
                                                <input class="form-control input-lg upload-name" id="" placeholder="사본을 첨부해 주세요.(pdf, jpg, jpeg, png, gif )" disabled="disabled" value="">
                                                <input type="file" id="business_regist" name="business_regist" class="upload-hidden" >
                                            </div>
                                            <div class="col-sm-3 pdl0">
                                                <label class="bt" for="business_regist">파일첨부</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">통장사본 업로드</label>
                                        <div class="filebox">
                                            <div class="col-sm-5">
                                                <input class="form-control input-lg upload-name" id="" placeholder="사본을 첨부해 주세요.(pdf, jpg, jpeg, png, gif )" disabled="disabled" value="">
                                                <input type="file" id="bankbook" name="bankbook" class="upload-hidden">
                                            </div>
                                            <div class="col-sm-3 pdl0">
                                                <label class="bt" for="bankbook">파일첨부</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <input type="hidden" id="company_serial" name="company_serial" value="">
                                </form>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <button class="finish_btn" id="btn_register">신청하기</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    </html>

<script type="text/javascript" src="//apis.daum.net/maps/maps3.js?apikey=0f1dc0047640c3cf044e7a7af12f2869&libraries=services"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>

<script type="text/javascript">
  
var EMAIL_FLAG, authcode_test, registernumber_test, PASSWORD_FLAG, PASSWORD_FORMAT_FLAG, dup_test;
//이하 객체 변수
var $fileTarget;
var $email, $email_dup_text, $email_confirm;
var $password, $password_confirm ,$password_str, $password_check;
var $company_name;
var $representative_number;
var $ffirst_number, $fmiddle_number, $flast_number;
var $postcode, $address;
// var $start_time, $end_time;
// var $delivery_max_count;
var $representative;
var $rfirst_number, $rmiddle_number, $rlast_number;
var $corporation_first_number, $corporation_last_number;
var $cr_first_number, $cr_middle_number, $cr_last_number;
var $btn_register;
var $business_regist, $bankbook;

$(document).ready(function(){

    initVariables();
    emailConfirm();
    passwordKeyup();
    passwordConfirmKeyup();
    fileTarget();
    // deliveryKeypressCheck($delivery_max_count);
    btnRegister();
    var numcheckvars = [
        $representative_number,$fmiddle_number,$flast_number,$rmiddle_number,$rlast_number,
        $corporation_first_number,$corporation_last_number,$cr_first_number,$cr_middle_number,$cr_last_number
    ];
    // var timecheckvars = [$start_time,$end_time];
    // for(var i=0;i<numcheckvars.length;++i){ numberKeypressCheck(numcheckvars[i]); }
    // for(var i=0;i<timecheckvars.length;++i) { timeBlurCheck(timecheckvars[i]); }

}); 


function initVariables() {
    EMAIL_FLAG=false; authcode_test=true; registernumber_test=true; PASSWORD_FLAG=false; PASSWORD_FORMAT_FLAG=false;
    dup_test = [EMAIL_FLAG,authcode_test,registernumber_test];

    $fileTarget = $('.filebox .upload-hidden');
    $email=$('#email'); $email_dup_text=$('#email_dup_text'); $email_confirm=$('#email_confrim');
    $password=$('#password'); $password_confirm=$('#password_confirm'); $password_str=$('#password_str'); $password_check=$('#password_check');
    $company_name=$('#company_name');
    $representative_number=$('#representative_number');
    $ffirst_number=$('#ffirst_number option:selected'); $fmiddle_number=$('#fmiddle_number'); $flast_number=$('#flast_number');
    $postcode=$('#postcode'); $address=$('#address');
    // $start_time = $("#partner_time_st_input"); $end_time = $("#partner_time_end_input");
    // $delivery_max_count = $("#delivery_max_count");
    $representative = $('#representative');
    $rfirst_number=$('#rfirst_number option:selected'); $rmiddle_number=$('#rmiddle_number'); $rlast_number=$('#rlast_number');
    $corporation_first_number=$('#corporation_first_number'); $corporation_last_number=$('#corporation_last_number');
    $cr_first_number=$('#cr_first_number'); $cr_middle_number=$('#cr_middle_number'); $cr_last_number=$('#cr_last_number');
    $btn_register=$('#btn_register');
    $business_regist=$('#business_regist'); $bankbook=$('#bankbook');
}

function fileTarget(){
    $fileTarget.on('change', function(){
        if(window.FileReader){
            var filename = $(this)[0].files[0].name;
        } else {
            var filename = $(this).val().split('/').pop().split('\\').pop();
        }
        $(this).siblings('.upload-name').val(filename);
    });
}

function execDaumPostcode() {
    new daum.Postcode({
        oncomplete: function(data) {

            var fullAddr = ''; 
            var extraAddr = ''; 

            if (data.userSelectedType === 'R') {
                fullAddr = data.roadAddress;
            } else { 
                fullAddr = data.jibunAddress;
            }

            if(data.userSelectedType === 'R'){
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }

                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            document.getElementById('postcode').value = data.zonecode; 
            document.getElementById('address').value = fullAddr;

            document.getElementById('address').focus();
        }
    }).open();
}

function isDup(value,column) {
    if(column == 'email'){
        if(emailcheck(value)==false){
            $email_dup_text.html('사용할 수 없는 아이디 입니다.');
            $email.focus();
            return false;
        }
    }
    $.post('/registermanager/duplicate_test/',
    {
        value : value,
        column : column
    },
    function(data,status){
        var result = JSON.parse(data);
        if(result=='null') {
            if(column=='email') {
                EMAIL_FLAG=true;
                $email_dup_text.html('사용할 수 있는 아이디입니다.');
            } 
        } 
        else {
                if(column=='email') { $email_dup_text.html('사용할 수 없는 ID입니다.'); EMAIL_FLAG=false; } 
                // if(column=='auth_code') { $('#authcode_dup_text').html('중복된 번호입니다.'); authcode_test=false;} 
                // if(column=='register_number') { $('#registernumber_dup_text').html('중복된 번호입니다.'); registernumber_test=false;} 
            }
    })
}

function emailConfirm() {
    $('#email_confirm').click(function() {
        isDup($email.val(),'email');
        return false;
    })
}

function passwordKeyup(){
    $password.keyup(function() {
        if($password.val() == ''){
            $password_str.html('');
            return false;
        }
         if(checkPassword($password.val())==false){
             $password_str.html('비밀번호가 형식에 맞지 않습니다.');
             PASSWORD_FORMAT_FLAG=false;
             return false;
        }else{
            $password_str.html('안전한 비밀번호 입니다.');
            PASSWORD_FORMAT_FLAG=true;
            return true;
        }
    })
}

function passwordConfirmKeyup(){
    $password_confirm.keyup(function() {
        if($password_confirm.val() == ''){
            $password_check.html('');
            return false;
        }

        if($password.val() == $(this).val() && PASSWORD_FORMAT_FLAG){
            $password_check.html('비밀번호가 일치합니다.');
            PASSWORD_FLAG=true;
        } else if($password.val() != $(this).val() && PASSWORD_FORMAT_FLAG){
            $password_check.html('비밀번호가 일치하지 않습니다.');
            PASSWORD_FLAG=false;
        } else if($password.val() != $(this).val() && !PASSWORD_FORMAT_FLAG){
            $password_check.val('');
            PASSWORD_FLAG=false;
        }
    })
}

function btnRegister() {
    $btn_register.click(function(){
    var FLAG=true;
    // var st_token = $start_time.val().split(':');
    // var st_str = st_token[0] + st_token[1];
    // var end_token = $end_time.val().split(':');
    // var end_str = end_token[0] + end_token[1];

   
    if($email.val().length==0) {
        alert('아이디를 입력해 주세요.');
        $email.focus();
        FLAG=false;
        return false;
    }
    if($password.val().length==0) {
        alert('비밀번호를 입력해 주세요.');
        $password.focus();
        FLAG=false;
        return false;
    }

    if($company_name.val().length==0) {
        alert('파트너 (상호)명을 입력해 주세요.');
        $company_name.focus();
        FLAG=false;
        return false;
    }

    if($representative_number.val().length==0) {
        alert('대표번호를 입력해 주세요.');
        $representative_number.focus();
        FLAG=false;
        return false;
    }

    if($fmiddle_number.val().length==0 || $flast_number.val().length==0) {
        alert('팩스번호를 입력해 주세요.');
        $fmiddle_number.focus();
        FLAG=false;
        return false;
    }
    if($postcode.val().length==0) {
        alert('주소를 입력해주세요.');
        $postcode.focus();
        FLAG=false;
        return false;
    }
    

    if($representative.val().length==0) {
        alert('대표자명을 입력해주세요.');
        $representative.focus();
        FLAG=false;
        return false;
    }

    if($rmiddle_number.val().length==0 || $rlast_number.val().length==0 ) {
        alert('대표자명을 입력해주세요.');
        $rmiddle_number.focus();
        FLAG=false;
        return false;
    }



    if($corporation_first_number.val().length==0 || $corporation_last_number.val().length==0 ) {
        alert('법인등록번호를 입력해주세요.');
        $corporation_first_number.focus();
        FLAG=false;
        return false;
    }

    if($cr_first_number.val().length==0 || $cr_middle_number.val().length==0 || $cr_last_number.val().length==0 ) {
        alert('사업자등록번호를 입력해주세요.');
        $cr_first_number.focus();
        FLAG=false;
        return false;
    }


     if(emailcheck($email.val())==false){
         // alert('이메일 형식대로 입력해 주세요.');
         alert('아이디 형식을 확인해주세요.');
         $email.focus();
         FLAG=false;
         return false;
    }

    // if(!EMAIL_FLAG) {
    //     alert('이메일 중복확인을 해주세요.');
    //     $email.focus();
    //     FLAG=false;
    //     return false;
    // }

    if(checkPassword($password.val())==false){
         // alert('비밀번호가 형식에 맞지 않습니다.');
         alert('비밀번호 형식을 확인해주세요.');
         $password.focus();
         FLAG=false;
         return false;
    }

    var numcheckvars = [
        $representative_number,$fmiddle_number,$flast_number,$rmiddle_number,$rlast_number,
        $corporation_first_number,$corporation_last_number,$cr_first_number,$cr_middle_number,$cr_last_number
    ];

    for(var i=0; i<numcheckvars.length; ++i){
        if(numbercheck(numcheckvars[i].val())==false){
            alert('입력 형식을 확인해주세요.');
            numcheckvars[i].val('');
            numcheckvars[i].focus();
            FLAG=false;
            return false;
        }
    }
    
    // if(timecheck($start_time.val())==false){
    //     // alert('영업시간이 형식에 맞지 않습니다.');
    //     alert('입력 형식을 확인해주세요.');
    //     $start_time.val('');
    //     $start_time.focus();
    //     FLAG=false;
    //     return false;
    // }

    // if(timecheck($end_time.val())==false){
    //     // alert('영업시간이 형식에 맞지 않습니다.');
    //     alert('입력 형식을 확인해주세요.');
    //     $end_time.val('');
    //     $end_time.focus();
    //     FLAG=false;
    //     return false;
    // }

    if( $business_regist.val() != "" ){
        var ext = $business_regist.val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['gif','png','jpg','jpeg','pdf']) == -1) {
            alert('gif,png,jpg,jpeg,pdf 파일만 업로드 할수 있습니다.');
            FLAG=false;
            return false;
        }
    }
    // else{
    //     alert('사업자 등록증을 첨부해 주세요.');
    //     FLAG=false;
    //     return false;
    // }

    if( $bankbook.val() != "" ){
        var ext = $bankbook.val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['gif','png','jpg','jpeg','pdf']) == -1) {
        alert('gif,png,jpg,jpeg,pdf 파일만 업로드 할수 있습니다.');
        FLAG=false;
        return false;
        }
    }
    // else{
    //     alert('통장사본을 첨부해 주세요.');
    //     FLAG=false;
    //     return false;
    // }

    // if(parseInt($delivery_max_count.val())>10){
    //     alert('10건 이하만 입력가능 합니다.');
    //     FLAG=false;
    //     $delivery_max_count.focus();
    //     return false;
    // }

    if(FLAG) {
        $.post('/registermanager/company_info_register/',
        {
            email : $email.val(),
            password : $password.val(),
            company_name : $company_name.val(),
            representative_number : $representative_number.val(),
            fax : $('#ffirst_number option:selected').val()  +'-'+ $fmiddle_number.val() +'-'+ $flast_number.val(),
            postcode : $postcode.val(),
            address : $address.val(),
            // start_time : st_str,
            // end_time : end_str,
            // delivery_max_count : $delivery_max_count.val(),
            representative : $representative.val(),
            representative_tel : $('#rfirst_number option:selected').val() + $rmiddle_number.val() + $rlast_number.val(),
            corporation_number : $corporation_first_number.val()+'-'+$corporation_last_number.val(),
            cr_number : $cr_first_number.val() + '-' + $cr_middle_number.val() + '-' + $cr_last_number.val(),
        },
        function(data){
            var result = JSON.parse(data);
            if(result.code=="S01"){
                 // alert(result.serial);
                 $('#company_serial').val(result.serial);
                 if($bankbook.val() != "" || $business_regist.val() != "" ){
                    $('#registermanager_form').submit();
                 }else{
                    location.replace('/registermanager/finish');
                 }
                }else{ 
                    alert("등록중 에러가 발생했습니다.\n" + result.message);

                }
            });
    }

})    
}

function checkPassword(upw)
{
    if(!/^[a-zA-Z0-9]{6,20}$/.test(upw)) return false;

    var chk_num = upw.search(/[0-9]/g); 
    var chk_eng = upw.search(/[a-z]/ig);

    if(chk_num < 0 || chk_eng < 0) return false;
    return true;
}

function emailcheck(strValue)
{
    var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
    if(strValue.length == 0 || !strValue.match(regExp))
        {return false;}
    return true;
}

function numbercheck(value) {
    var numExp = /^[0-9]*$/;
    if(value.length==0 || !numExp.test(value)) 
        return false;
    return true;
}

function timecheck(value) {
    var timeExp=/^[0-9]{2}:[0-9]{2}$/;
    if(value.length==0 || !timeExp.test(value)) 
        return false;
    return true;
}

function numberKeypressCheck($obj){
    $obj.keypress(function(){
        var numExp = /^[0-9]*$/;
        var objtext=$obj.val();
        if(!numExp.test(objtext)){
            $obj.val(objtext.slice(0,objtext.length-1));
        }
    })
}

function timeBlurCheck($obj){
    $obj.blur(function(){
    var timeExp=/^[0-9]{2}:[0-9]{2}$/;
        var objtext=$obj.val();
        if(!timeExp.test(objtext)){
            $obj.val('');
        }
    })   
}

// function deliveryKeypressCheck($obj){
//     $obj.keypress(function(){
//         if(parseInt($(this).val())>10){
//             $(this).val(10);
//         }
//     })
// }


function go_to_login(){
    location.replace('/');
}
</script>

<script type="text/javascript" src="https://openapi.map.naver.com/openapi/naverMap.naver?ver=2.0&key=806f88faa17d724e0108f82103c91ac5"></script>
<script type="text/javascript">
var map_on_flag = 'Y';
$('#dialog_map').on('shown.bs.modal', function () {
	if($('#address').val() == ''){
		alert('주소를 입력해 주세요.');
		$('#dialog_map').modal('hide');
		return false;
	}
	if(map_on_flag == 'Y'){
		oMap = new nhn.api.map.Map('naver_map' ,{
			point : oPoint,
			zoom : <?=$_POST['zoom_level']?$_POST['zoom_level']:'12';?>,
			enableWheelZoom : true,
			enableDragPan : true,
			enableDblClickZoom : false,
			mapMode : 0,
			activateTrafficMap : false,
			activateBicycleMap : false,
			minMaxLevel : [ 5, 14 ]
		});

		oMap.addOverlay(DraggableMarker);

		//아래는 사이드에 줌 컨트롤을 추가하는 소스 입니다.
		mapZoom = new nhn.api.map.ZoomControl(); // - 줌 컨트롤 선언
		mapZoom.setPosition({left:20, bottom:40}); // - 줌 컨트롤 위치 지정
		oMap.addControl(mapZoom); // - 줌 컨트롤 추가.
	}
	moveMap($('#address').val());
	map_on_flag = 'N';
})

var oPoint = new nhn.api.map.LatLng(<?=stripslashes($_POST['lat']?$_POST['lat']:'0');?>, <?=stripslashes($_POST['lon']?$_POST['lon']:'0');?>);
nhn.api.map.setDefaultPoint('LatLng');
// - Draggable Marker 의 경우 Icon 인자는 Sprite Icon이 된다.
// - 따라서 Sprite Icon 을 사용하기 위해 기본적으로 사용되는 값을 지정한다.
// - Sprite Icon 을 사용하기 위해서 상세한 내용은 레퍼런스 페이지의 nhn.api.map.SpriteIcon 객체를 참조하면 된다.
var testdefaultSpriteIcon = {
	url:"http://static.naver.com/maps2/icons/pin_spot2.png", 
	size:{width:28, height:37},
	spriteSize:{width:28, height:37},
	imgOrder:0, 
	offset : {width: 14, height: 37}
};
// - 위에서 지정한 기본값을 이용해 실제로 SpriteIcon 을 생성한다.
var testDupSpriteIcon_first = new nhn.api.map.SpriteIcon(testdefaultSpriteIcon.url, testdefaultSpriteIcon.size, testdefaultSpriteIcon.spriteSize, 0, testdefaultSpriteIcon.offset); 

var DraggableMarker = new nhn.api.map.DraggableMarker(testDupSpriteIcon_first , {       
	title : 'first row image',
	point : oPoint,
	zIndex : 1,
	smallSrc :  testDupSpriteIcon_first});          

DraggableMarker.attach("changePosition" , function (oEvent) {
	var market_position = DraggableMarker.getPoint();
	$('#lat').val(market_position.getLat());
	$('#lon').val(market_position.getLng());
});


function moveMarker(lat, lon, level){
	var new_marker_position = new nhn.api.map.LatLng(lat, lon);
	oMap.setCenter(new_marker_position);
	DraggableMarker.setPoint(new_marker_position);
	oMap.setLevel(level);
}

function moveMap(address){
	$.get("/get_coord_from_address.php?address=" + address, function(data, status){
		var data2 = data.split('|');
		if(data2[0] == 'ok1'){
			$('#lat').val(data2[2]);
			$('#lon').val(data2[1]);
			if($('#zoom_level').val() == undefined || $('#zoom_level').val() == ''){
				level = 12;
			} else {
				level = $('#zoom_level').val();
			}
			moveMarker(data2[2], data2[1], level);
		} else {
			alert(data2);
		}
    });
}
<?
if($_POST['lat'] && $_POST['lon']){
?>
//	moveMarker(<?=$_POST['lat'];?>, <?=$_POST['lon'];?>, <?=$_POST['zoom_level'];?>)
<?
}
?>
</script>