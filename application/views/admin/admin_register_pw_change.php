<!DOCTYPE html>
<html>
<head>
    <title>RENGO SOLUTION</title>

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/rengo_intro.css">

    <script src="/js/jquery-1.9.1.min.js"></script>

    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap-toggle.min.js"></script>
</head>
<body>
    <header>
        <div id="header">
            <div class="header_inner">
                <h1 class="title" onclick="go_to_login()">렌고 파트너 전용 서비스</h1>
            </div>
        </div>
    </header>
            <div class="content_wrap">
                <div class="row register">
                    <div class="col-lg-12">
                        <div class="ibox-title">
                            <h5>비밀번호 찾기</h5>
                            <h6>가입하신 정보는 회원님의 동의 없이 공개되지 않으며, 개인정보 보호정책에 의해 보호를 받습니다.</h6>
                        </div>
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <form method="get" class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-2 control-label">아이디(이메일)</label>

                                        <div class="col-sm-4"><input type="text" id="input_id" class="form-control input-lg" placeholder="이메일주소를 입력해주세요."></div>
                                        <div class="col-sm-6 pdl0">
                                         <a role="button" id="button_get_number" class="bt" style="padding: 0 13px;">인증</a>
                                            <!--<a id="" class="bt" style="display:inline-block;">중복확인</a>-->
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                   <!--  <div class="form-group"><label class="col-sm-2 control-label">휴대폰번호</label>

                                        <div class="col-sm-6">
                                            <ul class="phone_number">
                                                <li>
                                                    <select class="form-control input-lg">
                                                        <option value="">010</option>
                                                        <option value="">011</option>
                                                        <option value="">016</option>
                                                        <option value="">017</option>
                                                        <option value="">019</option>
                                                    </select>
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control input-lg" placeholder="">        
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control input-lg" placeholder="">
                                                </li>
                                              
                                            </ul>
                                        </div>
                                    </div> 
                                    <div class="hr-line-dashed"></div> -->
                                    <div class="form-group"><label class="col-sm-2 control-label">인증번호</label>

                                    <div class="col-sm-4"><input id="input_auth" type="text" class="form-control input-lg" placeholder="인증번호를 입력해 주세요."></div>
                                   
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-2 control-label">새 비밀번호</label>

                                        <div class="col-sm-4"><input id="input_pw1" type="password" class="form-control input-lg" placeholder="6자리이상 영문와 숫자를 사용하세요."></div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-2 control-label">새 비밀번호 확인</label>

                                        <div class="col-sm-4"><input id="input_pw2" type="password" class="form-control input-lg" placeholder="한번 더 입력해 주세요."></div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div role="button" id="button_reset_password" class="finish_btn">변경하기</div>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

</body>
</html>
<script language="javascript">

$("#button_get_number").click(function(){
    check_form();
});

$("#button_reset_password").click(function(){
    change_password();
});



function check_form(){
        $.post("/registermanager2/send_auth_mail",{
            id:$('#input_id').val(),
        }, function(data){
            if(data == 'OK'){
                alert("인증번호를 전송하였습니다.");
                return true;
            } else {
                alert(data);
                return false;
            }
        });

    

}

function change_password(){

    var password = $('#input_pw1').val();
    var password2 = $('#input_pw2').val();
    var id = $('#input_id').val();


    if(emailcheck(id)==false){
         alert('아이디를 확인해 주세요.');
          $('#input_id').focus();
          return false;
    }

    if($('#input_auth').val() == ''){
        alert('인증번호를 입력해 주세요.');
        $('#input_auth').focus();
        return false;
    }

    CheckPassword( $('#input_id').val() ,password);

    if(password != password2){
        alert("비밀번호가 일치하지 않습니다.");
         return false;
    }
   
     $.post("/registermanager2/change_password",{
            id:$('#input_id').val(),
            pw:password,
            auth:$('#input_auth').val()
        }, function(data){
            if(data == 'OK'){
                alert("비밀번호 변경이 완료되었습니다.");
                location.replace("http://solution.rengo.co.kr");
                return true;
            } else {
                alert(data);
                return false;
            }
        });

}


function CheckPassword(uid, upw)

{
    if(!/^[a-zA-Z0-9]{6,20}$/.test(upw))

    { 
        alert('비밀번호는 숫자와 영문자 조합으로 6~20자리를 사용해야 합니다.'); 
        return false;
    }

  
    var chk_num = upw.search(/[0-9]/g); 
    var chk_eng = upw.search(/[a-z]/ig);

    if(chk_num < 0 || chk_eng < 0)

    { 
        alert('비밀번호는 숫자와 영문자를 혼용하여야 합니다.'); 
        return false;
    }
    
    return true;

}

function emailcheck(strValue)
{
    var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
    //입력을 안했으면
    if(strValue.lenght == 0)
    {return false;}
    //이메일 형식에 맞지않으면
    if (!strValue.match(regExp))
    {return false;}
    return true;
}

function go_to_login(){
    location.replace('/');
}
</script>