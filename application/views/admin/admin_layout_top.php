<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>RENGO SOLUTION</title>

  <script src="/js/jquery-1.9.1.min.js"></script>
  <link href="/css/font-awesome.min.css" rel="stylesheet">
  <script src="/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <script src="/js/bootstrap-toggle.min.js"></script>

  <!-- Bootstrap Material Design -->
  <link href="/css/material_design/bootstrap-material-design.css" rel="stylesheet">
  <link href="/css/material_design/ripples.min.css" rel="stylesheet">
  <script src="/js/material_design/ripples.min.js"></script>
  <script src="/js/material_design/material.min.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  
  <link rel="stylesheet" href="/css/rengo_solution.css?<?=time();?>">
  <link rel="stylesheet" href="/css/rengo_solution_menu.css">
</head>
<body style="padding-right:0 !important">

  
<?php
function confirmUrl($url){
  if(substr(getenv(REQUEST_URI), 0, strlen($url)) == $url){
    return true;
  } else {
    return false;
  }
}
?>

  <!--side_menu-->
  <div class="nav-side-menu">
    <div class="brand">
      <div class="current-user">
    <div>
			<span class="name">
      <?=$company_name;?>
             
            </span>
    </div>
    <hr>
    <div class="function">
        <div><?=$admin_id;?> </div>
        <a href="/admin/logout" class="btn btn-link btn-xs">
            <i class="fa fa-sign-out"></i>
            로그아웃
        </a>
    </div>
    
    <!-- <div>
			<span class="name">
            <?=$admin_name;?>
            </span>
    </div>
    <div class="function">
        <a href="/admin/list" class="btn btn-link btn-xs">
            <i class="fa fa-search"></i> 마스터 관리 설정
        </a>
    </div> -->
</div>
    </div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
    <div class="menu-list">
      <ul id="menu-content" class="menu-content collapse out">



      <?php
      //permission 잘라서 각 메뉴의 권한 가져오기
      $menu_permission_array = array('N','N','N','N','N','N','N');
      for($i=0; $i<strlen($permission); $i++){
          $menu_permission_array[$i] = substr($permission, $i, 1);
      }


      $menu_url=array("/rent","/car_allocation",);
      ?>
      <li<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub_menu_r\"";?> data-toggle="collapse" data-target="#rent" class="collapsed menu_r">
        <? if($menu_permission_array[0] == 'Y') echo '<a href="#"><i class="fa fa-dashboard"></i> 대여관리 <span class="arrow"></span></a>'; ?>
      </li>
      <ul<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub-menu collapse in\"";?> class="sub-menu collapse" id="rent">
        <?
        $menu_url = "/rent";
        ?>
        <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/rent">예약관리</a></li>
        <?
        $menu_url = "/car_allocation";
        ?>
        <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/car_allocation">스케줄관리</a></li>
      </ul>


      <?
      $menu_url = "/carmanager";
      ?>
      <li<?php if(confirmUrl($menu_url)) echo " class=\"side_menu_r\"";?>>
      <? if($menu_permission_array[1] == 'Y') echo '<a href="/carmanager"><i class="fa fa-car"></i> 차량관리</a>'; ?>
    </li>

  <?
  $menu_url =array("/usermanager");
  ?>
  <li <?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo "class=\"sub_menu_r\" ";?> data-toggle="collapse" data-target="#usermanager" class="collapsed menu_r">
    <? if($menu_permission_array[2] == 'Y') echo '<a href="#"><i class="fa fa-user"></i> 고객관리 <span class="arrow"></span></a>'; ?>
  </li>

  <ul<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub-menu collapse in\"";?> class="sub-menu collapse" id="usermanager">
    <?
    $menu_url = "/usermanager";
    ?>
    <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/usermanager">고객관리</a></li>
    <!-- <?
    $menu_url = "/blacklistmanager";
    ?>
    <li<?php if(confirmUrl($menu_url))  echo " class=\"sub_menu_r\"";?>><a href="/blacklistmanager">블랙리스트</a></li> -->
  </ul>

  <?php
  $menu_url=array("/periodsales","/carnumbersales","/acceptancesales");
  ?>
  <li<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub_menu_r\"";?> data-toggle="collapse" data-target="#salemanager" class="collapsed menu_r">
    <? if($menu_permission_array[3] == 'Y') echo '<a href="#"><i class="fa fa-credit-card"></i> 매출관리<span class="arrow"></span></a>'; ?>
  </li>
  <ul<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub-menu collapse in\"";?> class="sub-menu collapse" id="salemanager">
    <?
    $menu_url = "/periodsales";
    ?>
    <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/periodsales">월별 매출</a></li>
    <?
    $menu_url = "/carnumbersales";
    ?>
    <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/carnumbersales">차량별 매출</a></li>
    <?
    $menu_url = "/acceptancesales";
    ?>
    <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/acceptancesales">수납방식별 매출</a></li>
    <!-- <?
    $menu_url = "/settlementstatement";
    ?>
    <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/settlementstatement">정산내역서 발행</a></li> -->
  </ul>

<?php
$menu_url=array("/salesetting", "/areasetting","/settlementstatement", "/blacklistmanager", "/chartmanager");
?>
<li <?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub_menu_r\"";?> data-toggle="collapse" data-target="#rengomanager" class="collapsed menu_r">
  <? if($menu_permission_array[4] == 'Y') echo '<a href="#"><i class="fa fa-wrench"></i> 렌고설정 <span class="arrow"></span></a>'; ?>
</li>
<ul<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub-menu collapse in\"";?> class="sub-menu collapse" id="rengomanager">

  <?
  $menu_url = "/salesetting";
  ?>
  <li<?php if(confirmUrl($menu_url))  echo " class=\"sub_menu_r\"";?>><a href="/salesetting">판매설정</a></li>

  <?
  $menu_url = "/areasetting";
  ?>
  <li<?php if(confirmUrl($menu_url))  echo " class=\"sub_menu_r\"";?>><a href="/areasetting">지역설정</a></li>

  <?
  $menu_url = "/settlementstatement";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/settlementstatement">정산관리</a></li> 
  <?
  $menu_url = "/blacklistmanager";
  ?>
  <li<?php if(confirmUrl($menu_url))  echo " class=\"sub_menu_r\"";?>><a href="/blacklistmanager">블랙리스트</a></li>
 <!--  <?
  $menu_url = "/chartmanager";
  ?>
  <li<?php if(confirmUrl($menu_url))  echo " class=\"sub_menu_r\"";?>><a href="/chartmanager">통계</a></li> -->

</ul>

<?php
$menu_url=array("/partnersetting", "/staffmanager");
?>
<li <?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub_menu_r\"";?> data-toggle="collapse" data-target="#setting" class="collapsed menu_r">
  <? if($menu_permission_array[5] == 'Y') echo '<a href="#"><i class="fa fa-wrench"></i> 환경설정 <span class="arrow"></span></a>'; ?>
</li>
<ul<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub-menu collapse in\"";?> class="sub-menu collapse" id="setting">

  <?
  $menu_url = "/partnersetting";
  ?>
  <li<?php if(confirmUrl($menu_url))  echo " class=\"sub_menu_r\"";?>><a href="/partnersetting">파트너 설정</a></li>
  <!-- 2016-10-11 15:04 이승원 대표님 요청
  <li><a href="#">직원관리</a></li>
  -->
  <!-- <?
  $menu_url = "/salesetting";
  ?>
  <li<?php if(confirmUrl($menu_url))  echo " class=\"sub_menu_r\"";?>><a href="/salesetting">판매설정</a></li> -->

  <!-- <?
  $menu_url = "/areasetting";
  ?>
  <li<?php if(confirmUrl($menu_url))  echo " class=\"sub_menu_r\"";?>><a href="/areasetting">지역설정</a></li> -->

  <!--
  <?
  $menu_url = "/preference";
  ?>
  <li<?php if(confirmUrl($menu_url))  echo " class=\"sub_menu_r\"";?>><a href="/preference">요금설정</a></li>
  
  <?
  $menu_url = "/companymanager";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/companymanager">지점 관리</a></li> -->
  <?
  $menu_url = "/staffmanager";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/staffmanager">직원 관리</a></li>

</ul>


<?php
$menu_url=array("/partnermanager","/mastermanager","/makermanager","/carmaster","/areamanager");
?>
<li<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub_menu_r\"";?> data-toggle="collapse" data-target="#master" class="collapsed menu_r">
  <? if($menu_permission_array[6] == 'Y') echo '<a href="#"><i class="fa fa-globe"></i> 마스터 관리 <span class="arrow"></span></a>'; ?>
</li>
<ul<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub-menu collapse in\"";?> class="sub-menu collapse" id="master">
  <?
  $menu_url = "/partnermanager";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/partnermanager">파트너 관리</a></li>
  <?
  $menu_url = "/mastermanager";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/mastermanager">마스터 관리</a></li>
  <?
  $menu_url = "/makermanager";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/makermanager">제조사 관리</a></li>
  <?
  $menu_url = "/carmaster";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/carmaster">차종 관리</a></li>
  <?
  $menu_url = "/areamanager";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/areamanager">지역 관리</a></li>
<!--   <?
  $menu_url = "/chartmaster";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/chartmaster">통계</a></li> -->
  
  
</ul>
</ul>
</div>
</div>
<!--side_menu-->
<!--content-->
<div id="page-wrapper">

<script type="text/javascript">
$('#menu-content li').click(function(){

  $(this).toggleClass('sub_menu_r1');

  
  $(this).prevAll('ul .in').addClass('out');
  $(this).prevAll('ul .in').removeClass('in');
  $(this).prevAll().removeClass('sub_menu_r1');

  
  $(this).next().nextAll('ul .in').addClass('out');
  $(this).next().nextAll('ul .in').removeClass('in');
  $(this).nextAll().removeClass('sub_menu_r1');

})

</script>