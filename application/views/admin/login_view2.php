<!DOCTYPE html>
<html>
<head>
    <title>RENGO SOLUTION</title>
    <script src="/js/jquery-1.9.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap-toggle.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/rengo_intro.css">
</head>
<body>
	<header>
		<div id="header">
			<div class="header_inner">
				<h1 class="title">렌고 파트너 전용 서비스</h1>
			</div>
		</div>
	</header>
    <div class="visual">

    	<div class="inner_wrap">
    		<div class="inner">	
	    		<div class="main_txt">
		    		<h2 class="tit">가장쉬운<br />렌터카 예약, 렌고</h2>
		    		<p class="txt">이제 유휴차량에 대해 고민하지 마세요!</p>
		    	</div>

	    		<div class="login_area">
					<form name="loginForm" method="post" onsubmit="javascript:" action="<?=$redirect_uri?$redirect_uri:'/rent';?>">
					<input type="hidden" name="act" value="I">
					<h3>로그인</h3>
					<fieldset id="login_member" class="member_id">
						<div class="login_input">
							<p><input type="text" id="login_id" name="login_id" class="user_id" placeholder="아이디 입력" maxlength="20" value=""></p>
							<p><input type="password" id="login_pw" name="login_pw" class="user_pw" placeholder="비밀번호 입력" maxlength="20" ></p>
							<input type="submit" name="login" id="login" value="로그인" class="btn_login" >
						</div>
						<div class="login_set">
							<p class="chk_save">
								<input type="checkbox" id="saveId" name="saveId" value="Y">
								<label for="saveId">아이디 저장</label>
							</p>
							<p class="btn_area">
								<a href="/registermanager2" class="find">PW찾기</a> <span class="bar">ㅣ</span>
								<a href="/registermanager" class="join">회원가입</a>
							</p>
						</div>
					</fieldset>
					</form>
				</div>
			</div>
    	</div>
    </div>

    <div class="bg_yellow partners">
	    <div class="content_wrap">
	    	<h2>렌고파트너사의 특권</h2>
	    	<div>
	    		<ul class="list_partner">
	    			<li>
	    				<h5>즉각적인 매출</h5>
	    				<p>유휴차량에 대한 고민,<br /> 렌고를 통해 즉시 매출로 만들어 보세요.</p>
	    			</li>
	    			<li>
	    				<h5>원터치 판매</h5>
	    				<p>파트너스 전용 앱을 통해 언제 어디서든<br />원터치로 예약관리를 하실수 있습니다.</p>
	    			</li>
	    			<li>
	    				<h5>광고 및 마게팅 비용 “제로”</h5>
	    				<p>렌고의 모든 광고효과를 무료로<br />무제한 누리실수 있습니다.</p>
	    			</li>
	    		</ul>
	    	</div>
	    </div>
    </div>

    <div class="partners">
    	<div class="content_wrap">
    		<h2>렌고 제휴 프로세스</h2>
    		<div>
    			<ul class="list_process">
    				<li>
    					<span><img src="/img/p_i01.png" alt="제휴문의"></span>
    					<h5>제휴문의</h5>
    					<p>아래 입력란에<br />연락처를 남겨주세요.</p>
    				</li>
    				<li>
    					<span><img src="/img/p_i02.png" alt="검토 후 담당자 연결"></span>
    					<h5>검토 후 담당자 연결</h5>
    					<p>검토후 영업담<br />당자가 방문하여 자세한<br />설명을 해드립니다.</p>
    				</li>
    				<li>
    					<span><img src="/img/p_i03.png" alt="계약체결"></span>
    					<h5>계약체결</h5>
    					<p>계약서 작성</p>
    				</li>
    				<li>
    					<span><img src="/img/p_i04.png" alt="콘텐츠 제작 및 적용"></span>
    					<h5>콘텐츠 제작 및 적용</h5>
    					<p>차량등록 및 콘텐츠 제작</p>
    				</li>
    				<li>
    					<span><img src="/img/p_i05.png" alt="예약오픈"></span>
    					<h5>예약오픈</h5>
    					<p>렌고를 통해 즉각적인<br />매출을 올려보세요.</p>
    				</li>
    			</ul>
    		</div>
    	</div>
    </div>

    <div class="main_contact">
   	 <div class="wrap_section">
    	<div class="w1200">
        <div class="txt_contact">
            <h2>렌고의 파트너가 되어 주세요.</h2>
          	<p>유휴차량에 대한 고민 함께하겠습니다.</p>
        </div>
        <div class="input_contact">
            <label for="input_company"><span class="tit">업체명</span><input type="text" id="input_company"></label>
            <label for="input_charge"><span class="tit">성명/직책</span><input type="text" id="input_charge"></label>
           	<label for="input_email"><span class="tit">이메일 주소</span><input type="text" id="input_email"></label>
           	<label for="input_phone"><span class="tit">연락처</span><input type="text" id="input_phone"></label>
            <a href="javascript:;" class="btn_yellow" onclick="javascript:send_sms();">문의하기</a>
        </div>
        </div>
  	</div>
  </div>
  <div id="footer" class="section fp-auto-height">
	<!--<h2><a href="/pc/"><img src="/img/pc/logo_h1.png" alt="렌터카 당일예약 서비스 렌고" /></a></h2>-->
		<div id="foot_info">
			<span>(주)렌고</span><span>대표:이승원</span><span class="bgn">사업자번호:302-81-29052</span><br>
			<span class="bgn">통신판매업신고 : 제2016-부산금정-0119호</span><br>
			<span class="bgn">개인정보담당자 : swlee@rengo.co.kr</span><br>
			<!--<span class="bgn">부산시 금정구 금강로 271, 3층</span><br>-->
			<span class="bgn fc_f50">고객센터 : 1800-1090</span><br>
			<!--<span class="bgn">부산광역시 금정구 금강로 271,3층(장전동)</span-->
		</div>
		<p id="copyright">ⓒ 2016 rengo All rights reserved.</p>
		<h3 class="screen_out">렌고 소셜 네트워크 바로가기</h3>
		<ul>
		 <li><a href="https://www.facebook.com/rengopage" target="_blank"><img src="/img/icon_facebook.png" alt="페이스북"></a></li>
		 <li><a href="http://blog.naver.com/rengo_blog" target="_blank"><img src="/img/icon_blog.png" alt="블로그"></a></li>
		 <!--<li><a href="https://youtu.be/_j-0hB4YItE" target="_blank"><img src="/img/main/foot_youtube.png" alt="유튜브"></a></li>-->
		</ul>
	</div>
</body>
</html>
