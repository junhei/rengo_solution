<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>RENGO SOLUTION</title>

  <script src="/js/jquery-1.9.1.min.js"></script>
  <link href="/css/font-awesome.min.css" rel="stylesheet">
  <script src="/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <script src="/js/bootstrap-toggle.min.js"></script>

  <!-- Bootstrap Material Design -->
  <link href="/css/material_design/bootstrap-material-design.css" rel="stylesheet">
  <link href="/css/material_design/ripples.min.css" rel="stylesheet">
  <script src="/js/material_design/ripples.min.js"></script>
  <script src="/js/material_design/material.min.js"></script>

  <link rel="stylesheet" href="/css/rengo_solution.css">
  <link rel="stylesheet" href="/css/rengo_solution_menu.css">

  <script>
    $(function(){
      $(".menu_r").click(function(){
          $(this).toggleClass("sub_menu_r1");
      })
    })
  </script>

</head>
<body style="padding-right:0 !important">

  <!--navbar
  <nav class="navbar navbar-default">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-top-links navbar-right push">
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a7.jpg">
                                </a>
                                <div class="media-body">
                                    <small class="pull-right">46h ago</small>
                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a4.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="pull-right text-navy">5h ago</small>
                                    <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/profile.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="pull-right">23h ago</small>
                                    <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                    <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="mailbox.html">
                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="mailbox.html">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile.html">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="grid_options.html">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="notifications.html">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>


                <li>
                    <a href="/admin/logout?back_url=<?=getenv(REQUEST_URI);?>">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>
    </div>
  </nav>
  <!--navbar -->
<?php
function confirmUrl($url){
  if(substr(getenv(REQUEST_URI), 0, strlen($url)) == $url){
    return true;
  } else {
    return false;
  }
}
?>

  <!--side_menu-->
  <div class="nav-side-menu">
    <div class="brand">
      <div class="current-user">
    <div>
			<span class="name">
             <?=$admin_name;?>

            
            </span>
    </div>
    <hr>
    <div class="function">
        <div>삼천리렌트카 금정영업소</div>
        <a href="/partnersetting" class="btn btn-link btn-xs">
            <i class="fa fa-cog"></i>
            파트너 설정
        </a>
        <a href="/admin/logout?back_url=<?=getenv(REQUEST_URI);?>" class="btn btn-link btn-xs">
            <i class="fa fa-sign-out"></i>
            로그아웃
        </a>
    </div>
    
    <!-- <div>
			<span class="name">
            <?=$admin_name;?>
            </span>
    </div>
    <div class="function">
        <a href="/admin/list" class="btn btn-link btn-xs">
            <i class="fa fa-search"></i> 마스터 관리 설정
        </a>
    </div> -->
</div>
    </div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
    <div class="menu-list">
      <ul id="menu-content" class="menu-content collapse out">

      <?php
      $menu_url=array("/rent","/car_allocation",);
      ?>
      <li<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub_menu_r\"";?> data-toggle="collapse" data-target="#rent" class="collapsed menu_r">
        <a href="#"><i class="fa fa-dashboard"></i> 대여관리 <span class="arrow"></span></a>
      </li>
      <ul<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub-menu collapse in\"";?> class="sub-menu collapse" id="rent">
        <?
        $menu_url = "/rent";
        ?>
        <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/rent">에약현황</a></li>
        <?
        $menu_url = "/car_allocation";
        ?>
        <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/car_allocation">배차스케쥴</a></li>
      </ul>


      <?
      $menu_url = "/carmanager";
      ?>
      <li<?php if(confirmUrl($menu_url)) echo " class=\"side_menu_r\"";?>>
      <a href="/carmanager"><i class="fa fa-car"></i> 차량관리</a>
    </li>

  <?
  $menu_url =array("/usermanager","/blacklistmanager");
  ?>
  <li <?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo "class=\"sub_menu_r\" ";?> data-toggle="collapse" data-target="#usermanager" class="collapsed menu_r">
    <a href="#"><i class="fa fa-user"></i> 고객관리 <span class="arrow"></span></a>
  </li>

  <ul<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub-menu collapse in\"";?> class="sub-menu collapse" id="usermanager">
    <?
    $menu_url = "/usermanager";
    ?>
    <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/usermanager">고객관리</a></li>
    <?
    $menu_url = "/blacklistmanager";
    ?>
    <li<?php if(confirmUrl($menu_url))  echo " class=\"sub_menu_r\"";?>><a href="/blacklistmanager">블랙리스트</a></li>
  </ul>

  <?php
  $menu_url=array("/salemanager", "/chartmanager");
  ?>
  <li<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub_menu_r\"";?> data-toggle="collapse" data-target="#salemanager" class="collapsed menu_r">
    <a href="#"><i class="fa fa-credit-card"></i> 매출관리<span class="arrow"></span></a>
  </li>
  <ul<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub-menu collapse in\"";?> class="sub-menu collapse" id="salemanager">
    <?
    $menu_url = "/salemanager";
    ?>
    <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/salemanager">매출현황</a></li>
    <?
    $menu_url = "/chartmanager";
    ?>
    <li<?php if(confirmUrl($menu_url))  echo " class=\"sub_menu_r\"";?>><a href="/chartmanager">통계</a></li>
  </ul>


<?php
$menu_url=array("/partnersetting", "/salesetting", "/preference","/companymanager","/staffmanager");
?>
<li <?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub_menu_r\"";?> data-toggle="collapse" data-target="#setting" class="collapsed menu_r">
  <a href="#"><i class="fa fa-wrench"></i> 환경설정 <span class="arrow"></span></a>
</li>
<ul<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub-menu collapse in\"";?> class="sub-menu collapse" id="setting">

  <?
  $menu_url = "/partnersetting";
  ?>
  <li<?php if(confirmUrl($menu_url))  echo " class=\"sub_menu_r\"";?>><a href="/partnersetting">파트너 설정</a></li>
  <!-- 2016-10-11 15:04 이승원 대표님 요청
  <li><a href="#">직원관리</a></li>
  -->
  <?
  $menu_url = "/salesetting";
  ?>
  <li<?php if(confirmUrl($menu_url))  echo " class=\"sub_menu_r\"";?>><a href="/salesetting">판매설정</a></li>

  <?
  $menu_url = "/preference";
  ?>
  <li<?php if(confirmUrl($menu_url))  echo " class=\"sub_menu_r\"";?>><a href="/preference">요금설정</a></li>
  
  <?
  $menu_url = "/companymanager";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/companymanager">지점 관리</a></li>
  <?
  $menu_url = "/staffmanager";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/staffmanager">직원 관리</a></li>

</ul>


<?php
$menu_url=array("/company","/admin","/makermanager","/carmaster","/areamanager");
?>
<li<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub_menu_r\"";?> data-toggle="collapse" data-target="#master" class="collapsed menu_r">
  <a href="#"><i class="fa fa-globe"></i> 마스터 관리 <span class="arrow"></span></a>
</li>
<ul<?php for($i=0;$i<count($menu_url);++$i) if(confirmUrl($menu_url[$i])) echo " class=\"sub-menu collapse in\"";?> class="sub-menu collapse" id="master">
  <?
  $menu_url = "/company";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/company/list">파트너 관리</a></li>
  <?
  $menu_url = "/admin";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/admin/list">마스터 관리</a></li>
  <?
  $menu_url = "/makermanager";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/makermanager">제조사 관리</a></li>
  <?
  $menu_url = "/carmaster";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/carmaster">차종 관리</a></li>
  <?
  $menu_url = "/areamanager";
  ?>
  <li<?php if(confirmUrl($menu_url)) echo " class=\"sub_menu_r\"";?>><a href="/areamanager">지역 관리</a></li>
  
  
</ul>
</ul>
</div>
</div>
<!--side_menu-->
<!--content-->
<div id="page-wrapper">
