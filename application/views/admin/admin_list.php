<?
	$company_list_count = count($company_list);
	for($i=0;$i<$company_list_count;$i++){
		$company_name_list[$company_list[$i]['serial']] = $company_list[$i]['company_name'];
	}
	$admin_function_count = count($admin_function);
?>
			<table class="tablesorter" border="1" cellpadding="0" cellspacing="1" width="<?=(800+($admin_function_count*100));?>px" style="background:#fff; border-color: #ccc;">
			<thead>
			<tr>
			  <th style="text-align:center; font-size:20px; background-color:#646a6f; color: #fff; padding:15px 10px;" colspan="<?=(4+$admin_function_count);?>">관리자 리스트</th>
			</tr>
			</thead>
			<tr align="center">
				<td width="150px" rowspan="2">ID</td>
				<td width="150px" rowspan="2">이름</td>
				<td colspan="<?=$admin_function_count;?>" style="padding: 10px;">접근권한</td>
				<td width="200px" rowspan="2">파트너업체명</td>
			</tr>
			<tr align="center">
<?
	for($i=0;$i<$admin_function_count;$i++){
?>
				<td width="100px" style="padding: 10px;"><?=$admin_function[$i];?></td>
<?
	}
?>
			</tr>
<?
	for($i=0;$i<$row_count;$i++){
?>
			<tr>
				<td width="150px" style="padding: 10px; color:#5dd3b6;"><a style="color: #333;" href="/admin/edit/<?=$result[$i]['serial'];?>"><?=$result[$i]['admin_id'];?></a></td>
				<td width="150px" style="padding: 10px;"><?=$result[$i]['admin_name'];?></td>
<?
		for($j=0;$j<$admin_function_count;$j++){
?>
				<td width="100px" align="center" style="padding: 10px;"><?=@$result[$i]['permission'][$j] == "Y"?"O":"X";?></td>
<?
		}
?>
				<td width="200px" style="padding: 10px;"><?=$company_name_list[$result[$i]['company_serial']];?></td>
			</tr>
<?
	}
?>
			</table>
<!-- 
<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>
<link href="/css/datepicker.min.css?<?=time();?>" rel="stylesheet" type="text/css">
<script src="/js/datepicker.js?<?=time();?>"></script>
<script src="/js/i18n_datepicker.en.js"></script>
<style type="text/css">
html {
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel border_gray magb0">
            
            <div class="panel-heading pdt17" >
                <div class="row">
                    <div class="col-md-9">
                        <h3 class="">관리자 리스트</h3>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="tab-content">
    <div id="example" style="margin:0;">
        <div id="master_grid"></div>
    </div>
</div>


<?php 
$data=array();
for($i=0; $i<count($result); ++$i){
    $data[$i]['admin_id'] = $result[$i]['admin_id'];
    $data[$i]['admin_name'] = $result[$i]['admin_id'];
}

$permission_string = array('rent_manager','car_manager','user_manager','sale_manager','preference','master_setting');

for($i=0; $i<count($result); ++$i){
    
    if($data[$i]['admin_id'] == $result[$i]['admin_id']){
        
        $permission = $result[$i]['permission'];        
        
        for($j=0; $j<6; ++$j) {

            $flag = $permission[$j]=='Y'?'O':'X';

            $temp_data = array(
                'admin_id'=>$result[$i]['admin_id'],
                $permission_string[$j]=>$flag
            );

            $data[$i] = array_merge($data[$i],$temp_data);
        }

    }

}

for($i=0; $i<count($result); ++$i) {

    if($data[$i]['admin_id'] == $result[$i]['admin_id']){

        $temp_data = array(
            'admin_id'=>$result[$i]['admin_id'],
            'partner_name'=>$company_name_list[$result[$i]['company_serial']]
        );
        $data[$i] = array_merge($data[$i],$temp_data);
    }

}

// echo json_encode($data);
?>

<script type="text/javascript">

$(document).ready(function() {
    setGrid();
    fitGridSize();
    $(window).resize(function() {
    	fitGridSize();
	});
})

function setGrid() {
    var data = <?php echo json_encode($data)?>;
    $("#master_grid").kendoGrid({
        selectable: true, 
        allowCopy: true, 
        sortable: true, 
        groupable: false,
        resizable: true,
        excel: {
            allPages: true,
            fileName: "고객리스트.xlsx",
            filterable: true
        },
        pageable: {
            input: true,
            messages: {
                display: "총 {2} 명의 고객 중 {0}~{1}번째",
                empty: "데이타 없음"
            }
          },
        noRecords: {
            template: "등록된 고객이 없습니다."
          },
        dataSource: {
            data : data,
            dataType : "json",
            // transport: {
            //      read: {
            //        url: '/admin/list/',
            //        dataType: "json"
            //      }
            //    },
            schema: {
                model: {
                    fields: {
                        admin_id: {type: "string"},
                        admin_name: { type: "string" },
                        rent_manager: { type: "string" }, 
                        car_manager: { type: "string" },
                        user_manager : {type : "string"}, 
                        sale_manager : { type: "string" }, 
                        preference : {type:"string"},
                        master_setting : {type:"string"},
                        partner_name : {type:"string"}
                    }
                }
            },
            pageSize: 30
        },
       
        columns: [ {
                field: "admin_id",
                title: "ID",
                width: 200
            },  {
                field: "admin_name",
                title: "이름",
                width: 100
            },  {
                title: "접근권한", 
                	columns :[{
                		field: "rent_manager",
                		title: "대여관리",
                		width: 80
                	},	{
                		field: "car_manager",
                		title: "차량관리",
                		width:80
                	},	{
                		field: "user_manager",
                		title: "고객관리",
                		width:80
                	},	{
                		field: "sale_manager",
                		title: "매출관리",
                		width:80
                	},	{
                		field: "preference",
                		title: "환경설정",
                		width:80
                	},	{
                		field: "master_setting",
                		title: "마스터 설정",
                		width:80
                	}]
            },  {
                field: "partner_name",
                title: "파트너업체명",
                width: 100
            }
        ]
    });    
}


function fitGridSize() {
    var gridElement = $("#master_grid");
    var gird_position = $('#master_grid').position();
    var window_height = $( window ).height() - gird_position.top - 70;
    gridElement.children(".k-grid-content").height(window_height-100);
    gridElement.height(window_height);
}

</script>



 -->