<!-- <style type="text/css">
.backgroundImage {
	background: url(/login_img/메인04.jpg) no-repeat,
			 	url(/login_img/메인03.jpg) no-repeat,
				url(/login_img/메인02.jpg) no-repeat,
				url(/login_img/메인01.jpg) no-repeat;
	background-size : 1920px 1080px;
}
</style> -->

<!DOCTYPE html>
<html>
<head>
    <title>RENGO SOLUTION</title>
    <script src="/js/jquery-1.9.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap-toggle.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/rengo_login.css">
</head>
<body>

    <div id="bg_img" class="visual">

		<header>
			<div id="header">
				<div class="header_inner">
					<h1 class="title"></h1>
				</div>
			</div>
		</header> 

    	<div class="inner_wrap">
    		<div class="inner">	
	    		<div class="login_txt">
	    			<h2 id="login_h" class="fw300">
	    				<b>렌고</b>의 <b>파트너</b>가 되어주세요.<br><b>마지막 예약</b>까지 <b>함께</b><br>하겠습니다.
	    			</h2>
	    		</div>

	    		<div class="login_area">
					<input type="hidden" name="act" value="I">
					<h3>로그인</h3>
					<fieldset id="login_member" class="member_id">
						<div class="login_input">
							<p><input type="text" id="login_id" name="login_id" class="user_id"  maxlength="50"></p>
							<div style="border-top:1px solid #d7d7d9;">
								<p><input type="password" id="login_pw" name="login_pw" class="user_pw" maxlength="50" ></p>
							</div>
							<input type="submit" name="login" id="login" value="로그인" class="btn_login_mint" >
						</div>
						<div class="login_set">
							<p class="chk_save">
								<input type="checkbox" id="saveId" name="saveId" value="Y">
								<label for="saveId">아이디 저장</label>
							</p>
							<p class="btn_area">
								<a href="/registermanager2" class="find">비밀번호 변경</a> <span class="bar">ㅣ</span>
								<a href="/registermanager" class="join">파트너 신청</a>
							</p>
						</div>
					</fieldset>
				</div>
			</div>
    	</div>

    	<footer>
	    	<div id="foot_info">
			 	<span>(주)렌고</span><span>대표:이승원</span><span>사업자번호:302-81-29052</span><span class="bgn">통신판매업신고 : 제2016-부산금정-0119호</span>
				<br>
				<span>개인정보담당자 : swlee@rengo.co.kr</span><span class="bgn">고객센터 : 1800-1090</span>
				<br />
				<span class="bgn">ⓒ 2017 rengo All rights reserved.</span>
		 	</div>
    	</footer>

    </div>

</body>
<script type="text/javascript">

var image_list;
var bg_img=null;
var login_text;
var login_h=null;


$(document).ready(function(){	
	init();


	setInterval(function(){
		bg_slide(image_list[0]);
		image_list=circular_array(image_list);
	},5000);

	setInterval(function(){
		login_text=circular_array(login_text);
		login_h.html(login_text[0]);
	},7500);


})

function init() {
	image_list = <?=$image_list?>;
	bg_img=$('#bg_img');
	login_h=$('#login_h');
	login_text =[
	"<b>렌고</b>의 <b>파트너</b>가 되어주세요.<br><b>마지막 예약</b>까지 <b>함께</b><br>하겠습니다.",
	"<b>렌고</b>의 <b>파트너</b>가 되어<br><b>유휴차량</b>에 대한 <b>고민</b>을<br>해결하세요."
	];

	for(var i=0; i<image_list.length;++i) {
   		imagePreload( '/login_img/'+image_list[i]);
	}

	bg_img.animate({opacity:0.11},0).css('background-image','url("/login_img/'+image_list[image_list.length-1]+'")').animate({opacity: 1}, 2000);

	$('#header').fadeIn(2000);
	$('.login_area').fadeIn(2000);
	
	bg_size();
}

imagePreload = function() {
// variables
	var image_cache_array = new Array(),
    i = 0;

// termination condition
	if (!document.images) {
	  return false;
	}

	for (key in arguments) {
	  image_cache_array[i] = new Image();
	  image_cache_array[i].src = arguments[key];
	  i++;
	}

	return i;
}

function circular_array(arr) {
	var ret = arr.shift();
	arr.push(ret);
	return arr;
}

function bg_size(){
	var width=$(window).width();
	var height=$(window).height();
	bg_img.css('background-size','cover');
}

function bg_slide(url) {	
	bg_img.css('background-image','url("/login_img/'+url+'")');	
	bg_img.css('transition','background 1s linear');	
	bg_size();
}

$(window).resize(function(){
	bg_img.css('transition','background 0s linear');
	bg_size();
})


$('#login').click(function(){
	$.post("/rent",{
		login_id:$('#login_id').val(),
		login_pw:$('#login_pw').val(),
		redirect_url:'<?=$redirect_uri?$redirect_uri:"/rent";?>',
	}, function(data2){
		var data = data2.split('|');
		if(data[0] == 'OK1' && (data[1] != '' && data[1] != undefined)){
			document.location.href=data[1];
		} else {
			alert(data2);
		}
	});
});

$(".login_area input[type=text]").keypress(function(e) { 
    if (e.keyCode == 13){
        $('#login').trigger('click');
    }    
});

$(".login_area input[type=password]").keypress(function(e) { 
    if (e.keyCode == 13){
        $('#login').trigger('click');
    }    
});


</script>
</html>
