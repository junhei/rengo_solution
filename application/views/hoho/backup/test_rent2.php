<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>RENGO SOLUTION</title>
    <link rel="stylesheet" href="css/rengo_solution.css">
    <link rel="stylesheet" href="css/rengo_solution_menu.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css">
    <script src="//code.jquery.com/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/latest/js/bootstrap.min.js"></script>
  </head>
  <body>
    <!--navbar -->
    <nav class="navbar navbar-default">
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#">삼천리렌트카 금정영업소 님</a></li>
          <li><a href="#" class="bg_main">로그아웃</a></li>
          <li><a href="#">알림</a></li>
        </ul>
      </div>
    </nav>
    <!--navbar -->

    <!--side_menu-->
    <div class="nav-side-menu">
      <div class="brand"><img src="img/l2_black.png" alt="logo"></div>
      <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
      <div class="menu-list">
        <ul id="menu-content" class="menu-content collapse out">
          <li>
            <a href="#"><i class="fa fa-dashboard fa-lg"></i> 대여관리</a>
          </li>
          <li>
            <a href="#"><i class="fa fa-car fa-lg"></i> 차량관리</a>
          </li>
          <li>
            <a href="#"><i class="fa fa-user fa-lg"></i> 고객관리</a>
          </li>
          <li>
            <a href="#"><i class="fa fa-credit-card fa-lg"></i> 매출관리</a>
          </li>
          <li data-toggle="collapse" data-target="#setting" class="collapsed side_menu_r">
            <a href="#"><i class="fa fa-wrench fa-lg"></i> 환경설정 <span class="arrow"></span></a>
          </li>
          <ul class="sub-menu collapse in" id="setting">
            <li class="sub_menu_r"><a href="#">파트너 정보</a></li>
            <li><a href="#">직원관리</a></li>
            <li><a href="#">판매관리</a></li>
          </ul>
          <li data-toggle="collapse" data-target="#master" class="collapsed">
            <a href="#"><i class="fa fa-globe fa-lg"></i> 마스터 설정 <span class="arrow"></span></a>
          </li>
          <ul class="sub-menu collapse" id="master">
            <li><a href="#">파트너 관리</a></li>
            <li><a href="#">마스터 관리</a></li>
            <li><a href="#">파트너 관리</a></li>
            <li><a href="#">차량 관리</a></li>
            <li><a href="#">지역 관리</a></li>
          </ul>
        </ul>
      </div>
    </div>
    <!--side_menu-->

    <!--content-->
    <div id="page-wrapper">

    </div>
    <!--content -->

  </body>
</html>
