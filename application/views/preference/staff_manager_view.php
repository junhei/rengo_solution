 <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
 <link href="/css/rengo_login.css" rel="stylesheet">
 <link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
 <link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
 <link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
 <link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
 <link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
 <link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
 <link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">

 <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
 <script src="/telerik/js/jszip.min.js"></script>
 <script src="/telerik/js/kendo.all.min.js"></script>
 <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
 <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

 <?php
    require("/home/apache/CodeIgniter-3.0.6/application/views/rengo_util.html");
?>


 <div class="row">
  <div class="col-md-12">
    <div class="panel border_gray">
      <div class="panel-heading">
        <div class="row">
          <div class="col-md-5">
            <h3 class="mgt20 mgb10">직원 관리</h3>
          </div>
          <div class="col-md-3  col-md-offset-4 mgt20">
            <select class="form-control" id="company_select">
              <?php foreach($company_list as $company):?>
                <option value="<?php echo $company['serial']; ?>"> <?php echo $company['company_name']; ?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
      </div>

      <div class="panel-footer pdb0 pdt15">
        <div class="row">
          <div class="list_data_box">
            <div class="col-lg-1 col-md-2 col-xs-3">총 직원 수</div>
            <div class="col-lg-1 col-md-2 col-xs-3 text-right" id="count_staff">명</div>
          </div>
        </div>
      </div>


      <div class="panel-heading border_gray bg_fff pdt14 pdb13">
        <div class="row">
          <div class="col-md-1">
            <button id="add_btn" type="button" class="btn action_btn" data-toggle="modal" data-target="#branch_office">직원 추가</button>
          </div>
        </div>
      </div>
      <div class="">
        <div id="grid"> </div>                
      </div>          

    </div>              
  </div>
</div>


<!-- Modal 지역정보 등록-->
<div class="modal fade" id="branch_office" role="dialog">
  <div class="modal-dialog modal_dialog500">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button id="" type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="" class="modal-title">직원 추가</h4>
      </div>

      <div class="modal-body">
        <div class="row">

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 pdr0 pdl0 control-label text-left ">*아이디(이메일)</label>
                <div class="col-sm-10 ">
                  <input type="" class="form-control" value="" id="admin_id" placeholder="이메일 주소를 입력해주세요." maxlength="30">
                </div>
              </div>
            </form>
          </div>

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 pdr0 pdl0 control-label text-left">*비밀번호</label>
                <div class="col-sm-10 ">
                  <a class="bt" role="button"  id="change_password_btn" type="button" >변경하기</a>
                  <input type="password" style="display:none;" class="form-control" id="admin_pw" placeholder="6자리이상, 영문과 숫자를 사용하세요." maxlength="20">
                </div>
              </div>
            </form>
          </div>

          <div id="admin_pw_confirm_div">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 pdr0 pdl0 control-label text-left">*비밀번호 확인</label>
                <div class="col-sm-10 ">
                  <input type="password" class="form-control" id="admin_pw_confirm" placeholder="한번 더 입력해주세요." maxlength="20">
                </div>
              </div>
            </form>
          </div>


          <div >
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 pdr0 pdl0 control-label text-left ">*직원명</label>
                <div class="col-sm-10">
                  <input type="" class="form-control" value="" id="admin_name" maxlength="10">
                </div>
              </div>
            </form>
          </div>

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 pdr0 pdl0 control-label text-left ">연락처</label>
                <div class="col-sm-10 ">
                  <input type="" class="form-control" value="" placeholder="" id="phone_number" maxlength="11"> 
                </div>
              </div>
            </form>
          </div>

          <div class="">
            <form class="form-horizontal">
              <div class="form-group magb0">
                <label for="" class="col-sm-2 pdr0 pdl0 control-label text-left pdt0 ">접근권한</label>
                <div class="col-sm-10">
                  <div class="panel panel-default magb0">
                    <div class="panel-body vehicle_info" style="font-size:12px;">
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_rent"><span class="checkbox-material"><span class="check"></span></span> 대여관리</label></div>
                      </div>
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_car"><span class="checkbox-material"><span class="check"></span></span> 차량관리</label></div>
                      </div>
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_customer"><span class="checkbox-material"><span class="check"></span></span> 고객관리</label></div>
                      </div>
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_sales"><span class="checkbox-material"><span class="check"></span></span> 매출관리</label></div>
                      </div>
                      <!-- <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_rengo"><span class="checkbox-material"><span class="check"></span></span> 렌고설정</label></div>
                      </div>
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_preference"><span class="checkbox-material"><span class="check"></span></span> 환경설정</label></div>
                      </div> -->

                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>


        </div>
      </div>

      <div class="modal-footer">
        <button id="delete_btn" type="button" class="btn mgr7 pull-left" data-dismiss="modal">삭제</button>      
        <button id="close_btn" type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
        <button id="save_btn" type="button" class="btn btn-primary">저장</button>
        <button id="update_btn" type="button" class="btn btn-primary" style="display:none;">수정</button>

      </div>
    </div>
  </div>
</div>

<script> 

  var $grid;
  var $car_company_select;
  var $add_btn, $delete_btn ,$save_btn, $update_btn;
  var $change_password_btn;
  var $admin_id, $admin_pw, $admin_name, $admin_pw_confirm, $admin_pw_confirm_div;
  var $phone_number;
  var $branch_office;
  var $check_rent, $check_car, $check_customer, $check_sales, $check_rengo, $check_preference;

  $(document).ready(function(){
    getCount();
    initVariables();
    initModal();
    initCompanySelect();
    initGrid();
    addBtnClick();
    deleteBtnClick();
    savaBtnClick();
    updateBtnClick();
    changePasswordBtnClick();
    windowResize();
    gridDoubleClick();
    numberKeypessCheck($phone_number);

    $(window).resize(function() {
      windowResize();
    });

    $car_company_select.change(function() {
      refresh_grid();
    });
  })

  function getCount(){
    var company_serial = get_company_serial();
    $.post("/staffmanager/get_count",
      {
            company_serial : company_serial
          },
          function(data){
            var result = JSON.parse(data);
            $("#count_staff").html(result + " 명");
      });
      
  }

  function initVariables() {
    $grid=$('#grid');
    $car_company_select=$('#company_select');
    $add_btn=$('#add_btn');
    $delete_btn=$('#delete_btn');
    $save_btn=$('#save_btn');
    $update_btn=$('#update_btn');
    $change_password_btn=$("#change_password_btn");
    $admin_id=$('#admin_id');
    $admin_pw=$("#admin_pw");
    $admin_pw_confirm=$('#admin_pw_confirm');
    $admin_pw_confirm_div=$('#admin_pw_confirm_div');
    $admin_name=$('#admin_name');
    $phone_number=$('#phone_number');
    $branch_office=$('#branch_office');
    $check_rent=$('#check_rent');
    $check_car=$('#check_car');
    $check_customer=$('#check_customer');
    $check_sales=$('#check_sales');
    $check_rengo=$('#check_rengo');
    $check_preference=$('#check_preference');
  }

  function initModal() {
    $('.modal').on('hidden.bs.modal',function() {
      $('.modal_scroll').scrollTop(0);
      setTimeout(function(){
        $('.modal').css({
          'top':'0',
          'left':'0'
        });
      },30)
    });
    $('.modal').draggable();
  }

  function initCompanySelect(){
    show_company_select();  
  }



  function initGrid(){
    $grid.kendoGrid({
      selectable: true, 
      allowCopy: true, 
      sortable: true, 
      groupable: false,
      pageable: {
        input: true,
        messages: {
          display: "Showing {0}-{1} from {2} data items",
          empty: "No data"
        }
      },
      noRecords: {
        template: "등록된 직원이 없습니다."
      },
      dataSource: {
        transport: {
         read: {
           url: "/staffmanager/get_list/"+<?=$company_serial?>+"/0",
           dataType: "json"
         }
       },
       schema: {
        model: {
          fields: {
            admin_id: { type: "string" },
                            // admin_pw: { type: "string" },
                            admin_name: {type:"string"},
                            // permission: {type:"string"},
                            phone_number: { type: "number" }, 
                            serial: { type: "number" },
                            check_rent: { type: "string" },
                            check_car: { type: "string" },
                            check_customer: { type: "string" }, 
                            check_sales: { type: "string" }, 
                            check_rengo: {type:"string"},
                            check_preference: { type: "string" } 
                          }
                        }
                      },
                      pageSize: 30
                    },

                    columns: [
                    {
                      field: "admin_id",
                      title: "아이디",
                      width: 200
                    },
                    {
                      field: "admin_name",
                      title: "이름",
                      width: 200
                    },{
                      field: "phone_number",
                      title: "전화번호",
                      width: 200
                    },
                    {
                      field: "check_rent",
                      title: "대여관리",
                      width: 100
                    },{
                      field: "check_car",
                      title: "차량관리",
                      width: 100
                    },{
                      field: "check_customer",
                      title: "고객관리",
                      width: 100
                    },{
                      field: "check_sales",
                      title: "매출관리",
                      width: 100
                    },
            // {
            //   field: "check_rengo",
            //   title: "렌고관리",
            //   width: 100
            // },{
            //   field: "check_preference",
            //   title: "환경설정",
            //   width: 100
            // },
            ]
          });
  }

  function addBtnClick(){
    $add_btn.click(function(){
      $change_password_btn.hide();
      $admin_pw.show();

      $delete_btn.hide();
      $save_btn.show();
      $update_btn.hide();

      $admin_id.val('');
      $admin_id.prop('disabled', false);
      $admin_pw.val('');
      $admin_pw.prop('disabled', false);
      $admin_pw_confirm_div.show();
      $admin_name.val('');
      $phone_number.val('');
    });
  }

  function deleteBtnClick() {
    $delete_btn.click(function() {
      var grid = $grid.data("kendoGrid");
      var row = grid.select();
      var data = grid.dataItem(row);

      if(data==null){
        return false;
      }

      $.post("/staffmanager/delete",
      {
            serial : data.serial //수정사항 10/3일.
          },
          function(data, status){
            var result = JSON.parse(data);
            if(result.code=="S01"){
              refresh_grid();
              getCount();
            }else{
              alert("데이터 삭제 중 에러가 발생했습니다.");
            }
          });
    });
  }



    function savaBtnClick() {
     $save_btn.click(function(){
      var company_serial=get_company_serial();

      if(!emailcheck($admin_id.val())) {
        alert('이메일을 확인해주세요.');
        $admin_id.focus();
        return false;
      }

      if(!checkPassword($admin_pw.val())){
        alert('비밀번호를 확인해주세요.');
        $admin_pw.focus();
        return false;
      }

      if(!checkPassword($admin_pw_confirm.val())){
        alert('비밀번호 형식을 확인해주세요.');
        $admin_pw_confirm.focus();
        return false;
      }        

      if($admin_pw.val() != $admin_pw_confirm.val()){
        alert('비밀번호가 일치하지 않습니다.');
        $admin_pw_confirm.focus();
        return false;
      }

      if($admin_name.val()==''){
        alert('이름을 입력해주세요.');
        $admin_name.focus();
        return false;
      }

      // if($phone_number.val()!=''){
      //   if(!numbercheck($phone_number.val())){
      //     alert('휴대폰번호 형식을 확인해주세요.');
      //     $phone_number.focus();
      //     return false;
      //   }    
      // }

      $.post("/staffmanager/add",
      {
        company_serial : company_serial,
        admin_id: $admin_id.val() ,
        admin_pw: $admin_pw.val() ,
        admin_name : $admin_name.val(),
        phone_number : $phone_number.val(),
        check_rent: ($check_rent.is(":checked")) ? "Y" : "N" ,
        check_car: ($check_car.is(":checked")) ? "Y" : "N" ,
        check_sales: ($check_sales.is(":checked")) ? "Y" : "N" ,
        check_customer: ($check_customer.is(":checked")) ? "Y" : "N" ,
            // check_rengo: ($check_rengo.is(":checked")) ? "Y" : "N" ,
            // check_preference: ($check_preference.is(":checked")) ? "Y" : "N" ,
          },
          function(data, status){
            var result = JSON.parse(data);
            if(result.code=="S01"){
              refresh_grid();
              $branch_office.modal('toggle');
              getCount();
            }else if(result.code=="E02"){
              alert(result.message);
            }else{
              alert("데이터 입력 중 에러가 발생했습니다.");
            }
          });
    }); 
   }        

   

   function updateBtnClick() {
     $update_btn.click(function(){

      var grid = $grid.data("kendoGrid");
      var row = grid.select();
      var data = grid.dataItem(row);
      var password;

      if(!emailcheck($admin_id.val())) {
        alert('이메일을 확인해주세요.');
        $admin_id.focus();
        return false;
      }

      if($admin_pw.is(":visible")) {
        if(!checkPassword($admin_pw.val())){
          alert('비밀번호를 확인해주세요.');
          $admin_pw.focus();
          return false;
        }

        if(!checkPassword($admin_pw_confirm.val())){
          alert('비밀번호를 확인해주세요.');
          $admin_pw_confirm.focus();
          return false;
        }        

        if($admin_pw.val() != $admin_pw_confirm.val()){
          alert('비밀번호가 일치하지 않습니다.');
          $admin_pw_confirm.focus();
          return false;
        }
      }

      if($admin_name.val()==''){
        alert('이름을 입력해주세요.');
        $admin_name.focus();
        return false;
      }

      if($phone_number.val()!=''){
        if(!numbercheck($phone_number.val())){
          alert('휴대폰번호 형식을 확인해주세요.');
          $phone_number.focus();
          return false;
        }    
      }

      $.post("/staffmanager/update/",
      {
        serial : data.serial,
        admin_pw : $admin_pw.val() ,
        admin_name : $admin_name.val(),
        admin_pw:  $admin_pw.val(),
        phone_number : $phone_number.val(),
        check_rent: ($check_rent.is(":checked")) ? "Y" : "N" ,
        check_car: ($check_car.is(":checked")) ? "Y" : "N" ,
        check_sales: ($check_sales.is(":checked")) ? "Y" : "N" ,
        check_customer: ($check_customer.is(":checked")) ? "Y" : "N" ,
            // check_rengo: ($check_rengo.is(":checked")) ? "Y" : "N" ,
            // check_preference: ($check_preference.is(":checked")) ? "Y" : "N" ,
          },
          function(data, status){
            var result = JSON.parse(data);
            if(result.code=="S01"){
              alert('수정되었습니다.');
              refresh_grid();
              $branch_office.modal('toggle');
            }else if(result.code=="E02"){//이미 있는 경우
              alert("이미 등록 된 아이디 입니다.");
            }else{
              alert("데이터 입력 중 에러가 발생했습니다.");
            }
          });
    });
   }


   function gridDoubleClick(){
    $grid.delegate("tbody>tr", "dblclick", function(){
      var grid = $grid.data('kendoGrid'); 
      var row = grid.select();
      var data = grid.dataItem(row);

      $change_password_btn.show();
      $admin_pw.val('');
      $admin_pw.hide();
      $admin_pw_confirm_div.hide();

      $delete_btn.show();
      $save_btn.hide();
      $update_btn.show();

      $admin_id.val(data.admin_id);
      $admin_id.prop('disabled', true);
      $admin_pw.val('');
      $admin_pw_confirm.val('');
      $admin_name.val(data.admin_name);
      $phone_number.val(data.phone_number);

      if(data.check_rent == "O"){
        $check_rent.prop('checked', true);
      }

      if(data.check_car == "O"){
        $check_car.prop('checked', true);
      }

      if(data.check_sales == "O"){
        $check_sales.prop('checked', true);

      }

      if(data.check_customer == "O"){
        $check_customer.prop('checked', true);
      }

        // if(data.check_rengo == "O"){
        //     $check_rengo.prop('checked', true);
        // }

        // if(data.check_preference == "O"){
        //     $check_preference.prop('checked', true);
        // }

        $branch_office.modal('toggle');
      });
  }

  function refresh_grid(){

    var company_serial=get_company_serial();

    var dataSource = new kendo.data.DataSource({
     transport: {
      read: {
       url: "/staffmanager/get_list/" + company_serial + '/0' ,
       dataType: "json"
     }
   },
   pageSize: 30
 });

    var grid = $grid.data("kendoGrid");
    grid.setDataSource(dataSource);
  }

  function windowResize() {
    var gird_position = $grid.position();
    var window_height = $( window ).height() - gird_position.top - 50;
    $grid.children(".k-grid-content").height(window_height - 100);
    $grid.height(window_height);
  }

function changePasswordBtnClick(){
    $change_password_btn.click(function(){
        $change_password_btn.hide();
        $admin_pw.show();
        $admin_pw_confirm_div.show();
    });    
}



  function confirmEmail() {
    if(!emailcheck($admin_id.val())) {
      alert('이메일 형식을 확인해주세요.');
      $(this).focus();
      return false;
    }
    return true;
  }

  function checkPassword(upw)
  {
    if(!/^[a-zA-Z0-9]{6,20}$/.test(upw)) return false;

    var chk_num = upw.search(/[0-9]/g); 
    var chk_eng = upw.search(/[a-z]/ig);

    if(chk_num < 0 || chk_eng < 0) return false;
    return true;
  }

  function emailcheck(strValue)
  {
    var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
    if(strValue.length == 0 || !strValue.match(regExp))
      {return false;}
    return true;
  }

  function numbercheck(value) {
    var numExp = /^[0-9]*$/;
    if(value.length==0 || !numExp.test(value)) 
      return false;
    return true;
  }

  function numberKeypessCheck($obj){
    $obj.keypress(function(){
      var numExp = /^[0-9]*$/;
      var objtext=$obj.val();
      if(!numExp.test(objtext)){
        $obj.val(objtext.slice(0,objtext.length-1));
      }
    })
  }

</script>  

