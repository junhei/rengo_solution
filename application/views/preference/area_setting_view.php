

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<link href="/css/rengo_login.css" rel="stylesheet">

<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">


<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>
<script src="/telerik/js/cultures/kendo.culture.ko-KR.min.js"></script>
<!-- 새로운 datepicker -->
<link href="/css/datepicker.min.css?<?=time();?>" rel="stylesheet" type="text/css">
<script src="/js/datepicker.js?<?=time();?>"></script>
<script src="/js/i18n_datepicker.en.js"></script>

<style type="text/css">
html{overflow-x:hidden;}
</style>

<?php
    require("/home/apache/CodeIgniter-3.0.6/application/views/rengo_util.html");
?>

<div class="row">
  <div class="col-md-12">
    <div class="panel border_gray magb0">
      
      <div class="panel-heading">
        <div class="row">
          <div class="col-md-4">
            <div class="col-sm-3 pdr0">
              <h3 class="mgt20 mgb10">지역설정</h3>
            </div>
            
            
          </div>

          <div class="col-md-3 pull-right">
            <div class="col-md-12 mgt20">
              <select class="form-control" id="company_select">
                <?php foreach($company_list as $company):?>
                  <option value="<?php echo $company['serial']; ?>"> <?php echo $company['company_name']; ?></option>
                <?php endforeach;?>
              </select>
            </div>
          </div>

        </div>
      </div>

      <div class="panel-footer bb_ccc pdb0 pdt15">
        <div class="row">
            <div class="list_data_box">
                <div class="col-lg-2 col-md-3 col-xs-4">지하철역 선택개수</div>
                <div class="col-lg-1 col-md-2 col-xs-3 text-right" id="subway_count"></div>
                <div class="col-lg-2 col-md-3 col-xs-4">주요지역 선택개수</div>
                <div class="col-lg-1 col-md-2 col-xs-3 text-right" id="spot_count"></div>
            </div>
        </div>
    </div>

    <div class="panel-heading border_gray bg_fff pdt14 pdb13 bb0">
        <div class="row">
            <div class="col-sm-7">
                <div class="btn-group" role="group" aria-label="...">
                    <a role="button" type="button" data-toggle="tab" class="btn pdl35 pdr35 bg_main" href="#subway_setup" id="btn_subway">지하철역</a>
                    <a role="button" type="button" data-toggle="tab" class="btn pdl35 pdr35" href="#spot_city_setup" id="btn_spot">주요지역</a>
                    <script type="text/javascript">
                        $('#btn_subway').click(function(){
                            $(this).addClass('bg_main');
                            $('#btn_spot').removeClass('bg_main');
                        });
                        $('#btn_spot').click(function(){
                            $(this).addClass('bg_main');
                            $('#btn_subway').removeClass('bg_main');
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
    

    </div>

    <div class="tab-content">
      
    
      <div id ="subway_setup" class="tab-pane fade in active">
      <div class="panel-footer section_lnb">
          <ul class="nav nav-tabs border_none" id="tab_subway_city">
            
          </ul>
      </div >
      <div class="panel-footer bg_fff">
        <div class="">
        <div class="delivery_wrap">
          <div class="ibox-title">
              <h5>지하철 딜리버리 설정</h5>
              <h6>딜리버리를 하실 지하철역을 설정합니다.</h6>
          </div>
          
          <div class="ibox float-e-margins magb0">
              <div class="ibox-content" id="div_subways">
                  <!-- <div class="row">

                  </div> -->
         
                  <button class="btn action_btn pull-right" id="btn_register">저장</button>
                  
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>

    <div id="spot_city_setup" class="tab-pane fade">
     <div class="panel-footer section_lnb">
          <ul class="nav nav-tabs border_none" id="tab_spot_city">

          </ul>
      </div>
      <div class="panel-footer bg_fff">
       
        <div class="delivery_wrap">
          <div class="ibox-title">
              <h5>주요지역 딜리버리 설정</h5>
              <h6>딜리버리를 하실 주요지을 설정합니다.</h6>
          </div>
          
          <div class="ibox float-e-margins magb0">
              <div class="ibox-content" id="div_place">

              <button class="btn action_btn pull-right" id="btn_register">저장</button>

              </div>

          </div>

        </div>
      </div>
      </div>

      </div>
    <script type="text/javascript">


var subwayArray = new Array();
// var spotArray = new Array();
var company_serial = <?php echo $company_serial; ?>;


var selected_city = "수도권";
var selected_city2 = '';




get_subway_city();
get_spot_city();
get_area_count();
// get_delivery_count();

// if(company_serial != 0){
//   // getWorkTime();
// }


show_company_select();


function city_select(city_name){
   selected_city = city_name;
    get_subway(city_name);
}

function spot_city_select(city_name){
  selected_city2 = city_name;
  get_spot_place(city_name);
}

function get_subway_city(){
   $.get("/salesetting/get_subway_city",
    function(data){
      var result = JSON.parse(data);

      var text='';
      for(var i=0; i<result.length; i++){
        if(i==0){
            text += "<li class='active'>";
        }else{
            text += "<li>";
        }
        text += ("<a data-toggle='tab' href='#' id='tab" + (i+1) +"' onclick='city_select(\""+result[i].city +"\")' >" + result[i].city + " </a></li>");
      }
      $( "#tab_subway_city").html(text);

      get_subway($("#tab1").text());
      
  });
}

function get_spot_city(){
   $.get("/salesetting/get_spot_city",
    function(data){
      var result = JSON.parse(data);
      selected_city2 = result[0].city;
      var text='';
      for(var i=0; i<result.length; i++){
        if(i==0){
            text += "<li class='active'>";
        }else{
            text += "<li>";
        }
        text += ("<a data-toggle='tab' href='#' id='tab_spot_" + (i+1) +"' onclick='spot_city_select(\""+result[i].city +"\")' >" + result[i].city + " </a></li>");
      }
      $( "#tab_spot_city").html(text);

      get_spot_place($("#tab_spot_1").text());
      
  });
}

// function get_delivery_count(){
//     var company_serial = get_company_serial();
//     $.get("/salesetting/get_delivery_max_per_hour/"+company_serial,
//     function(data){
//         var result = JSON.parse(data);
//         $("#delivery_max_count").val(result[0].delivery_max_per_hour);   
//     });

// }

function get_subway(city_name){

  var company_serial = get_company_serial();
  if(company_serial==0){
      // alert("업체를 먼저 선택해 주세요.");
      return false;
  }


  $.get("/salesetting/get_delivery_subway_by_city/"+city_name+"/"+company_serial,
  function(data){

    var resultArray = JSON.parse(data);
    var subwayLineArray = new Array();
    for(i=0; i<resultArray.length; i++){
      var tempSubwayLine = resultArray[i].area_kind.split(" ");
      subwayLineArray.push(tempSubwayLine[2]);
    }

      //중복제거
      var subways = subwayLineArray.filter(function(itm, i, a){
        return i==a.indexOf(itm);
      });


      var text = '';
      subwayArray = Array();
      // alert(data);
      for(var i=0; i< subways.length; i++){
        subwayArray.push(subways[i]);
        text +=  "<div class='row'><div class='magb15 col-sm-12' ><h4 class='pull-left'>" + subways[i] + "</h4> <div class='pull-right'><div class='all_chk_btn'><button class='btn action_btn mgr7 pull-left' onclick='all_select(\""+city_name+"\", \""+subways[i]+"\")'>전체선택</button><button class='btn btn-default pull-left' onclick='all_deselect(\""+city_name+"\" , \""+subways[i]+"\")'>전체해제</button></div></div></div><div class='col-sm-12'><ul class='delivery'>";
        for(j=0; j<resultArray.length; j++){
          var tempSubwayLine = resultArray[j].area_kind.split(" ");

          if(subways[i] == tempSubwayLine[2]){

            text += "<li><label for='chk" + (j+1) +"'><span class='agree'><input type='checkbox' id='chk" + (j+1) +"' name='subway_" + subways[i] +"'  value='" + resultArray[j].serial + "'";

            // text += "<input type='checkbox' id='check_subway' name='subway_" + subways[i] + "' value='" + resultArray[j].serial + "'";
            if(resultArray[j].selected == true){
              text += " checked ";
            }
            text += "><span class='chk_txt'>" + resultArray[j].location_name_kor +"</span></span></label></li>";
          }
        }
        text += "</ul></div></div><div class='hr-line-dashed'></div>";
      }
      text += "<button class='btn action_btn pull-right' id='btn_a' onclick='save_subway()'>저장</button>";

      

      $( "#div_subways").html(text);
    });
}

function get_spot_place(city_name){
      // var company_serial = get_company_serial();
      // alert($company_serial);

  var company_serial = get_company_serial();
  if(company_serial==0){
      // alert("업체를 먼저 선택해 주세요.");
      return false;
  }



       $.get("/salesetting/get_spots/"+city_name+"/"+company_serial,
          function(data){
             // alert(data);
             var result = JSON.parse(data);
             if(result.length == 0){
                //아무 등록된 지역이 없으면
                // alert('hi');
                 var text = '';
                $( "#div_place").html(text);
             }else{
                //등록된 지역이 있으면
                // alert('no');

                var areaNameArray = new Array();
                for(i=0; i<result.length; i++){
                  var area_name = result[i].area_name;
                  areaNameArray.push(area_name);
                }

                  //중복제거
                  var areaArray = areaNameArray.filter(function(itm, i, a){
                    return i==a.indexOf(itm);
                  });
                var text = '';
                 for(var i=0; i< areaArray.length; i++){
                    text +=  "<div class='row'><div class='magb15 col-sm-12' ><h4 class='pull-left'>" + areaArray[i] + "</h4> <div class='pull-right'></div></div><div class='col-sm-12'><ul class='delivery'>";
                    for(j=0; j<result.length; j++){
                      // var tempSubwayLine = resultArray[j].area_kind.split(" ");

                      if(areaArray[i] == result[j]['area_name']){

                        text += "<li><label for='chk" + (j+1) +"'><span class='agree'><input type='checkbox' id='chk" + (j+1) +"' name='place'  value='" + result[j].serial + "'";

                         if(result[j].selected == true){
                              text += " checked ";
                          }
                        text += "><span class='chk_txt'>" + result[j].location_name_kor +"</span></span></label></li>";
                      }
                    }
                    text += "</ul></div></div><div class='hr-line-dashed'></div>";
                  }
                  text += "<button class='btn action_btn pull-right' id='btn_b' onclick='save_special_place()'>저장</button>";
                  
                  $( "#div_place").html(text);
             }

        });
}


//////////////////////////////////////////////////////////////// 파트너 선택 리스너

$( "#company_select" ).change(function() {
 company_serial = $(this).val();

  // getWorkTime();
  get_subway(selected_city);
  get_spot_place(selected_city2);
  get_area_count();
});

// $( "#car_branch_select" ).change(function() {
//   branch_serial = $(this).val();
// });




/////////////////////////////////////////////////////////////// 브런치 선택 리스너

// function getWorkTime(){

//    var company_serial = get_company_serial();

//  $.get("http://solution.rengo.co.kr/salesetting/get_start_end_time/"+company_serial,
//   function(data){
//         // alert(data);
//         result = JSON.parse(data);
//         var openTime = result[0].open_time_start;
//         var endTime = result[0].open_time_finish;


//         $('#partner_time_st_input').val(openTime.substring(0,2)+":"+openTime.substring(2,4));
//         $('#partner_time_end_input').val(endTime.substring(0,2)+":"+endTime.substring(2,4));
//       });
// }


$("#btn_register").click(function() {
  // alert("전체");
});


function save_subway(){

    var company_serial = get_company_serial();
    if(company_serial == 0){
       alert('업체를 선택해 주세요.');
       return false;
    }


    var serialArray = Array();
    //지하철역 선택 된거 serial 얻어옴
    for(var i=0; i<subwayArray.length; i++){
      var name = "subway_" + subwayArray[i];
      $("input[name='"+ name+ "']:checked").each(function() {
        var serial = $(this).val();
        serialArray.push(serial);
      });
    }

      $.post("/salesetting/save_subway",
      {
          company_serial : company_serial,
          city : selected_city, //수정사항 10/3일.
          subways : serialArray
      },
      function(data, status){
            // alert(data);
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                        alert("저장 되었습니다.");
                    }else{
                        alert("데이터 저장 중 에러가 발생했습니다.");
                    }

      });

    $('#check_subway:checked').each(function() { 
      alert($(this).val());
    });

}

function save_special_place(){

   var company_serial = get_company_serial();
   if(company_serial == 0){
       alert('업체를 선택해 주세요.');
       return false;
    }


    var serialArray = Array();
    //지하철역 선택 된거 serial 얻어옴

    $("input[name='place']:checked").each(function() {
        var serial = $(this).val();
        serialArray.push(serial);
    });


      $.post("/salesetting/save_spot",
      {
          company_serial : company_serial,
          city : selected_city2, //수정사항 10/3일.
          spot : serialArray
      },
      function(data, status){
            // alert(data);
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                        alert("저장 되었습니다.");
                    }else{
                        alert("데이터 저장 중 에러가 발생했습니다.");
                    }

      });

}


function get_area_count(){
  $company_serial = get_company_serial();
   $.post("/areamanager/get_counts",
      {
          company_serial : company_serial
      },
      function(data){
            // alert(data);
            var result = JSON.parse(data);
            $("#subway_count").text(result.count_subway+"곳");
            $("#spot_count").text(result.count_spot+"곳");
                
      });

}


function all_select(city_name, subway){
  // alert(city_name+","+subway);
  var name = "subway_" + subway;
  $("input:checkbox[name='" + name +"']").prop("checked", true);
}

function all_deselect(city_name, subway){
// alert(city_name+","+subway);
  var name = "subway_" + subway;
  $("input:checkbox[name='" + name +"']").prop("checked", false);
}



// $("#btn_time_save").click(function() {

//   var company_serial = get_company_serial();
//   if(company_serial == 0){
//     alert('업체를 선택해 주세요.');
//     return false;
//   }

//   var start_time = $("#partner_time_st_input").val();
//   var st_token = start_time.split(':');
//   var st_str = st_token[0] + st_token[1];

//   var end_time = $("#partner_time_end_input").val();
//   var end_token = end_time.split(':');
//   var end_str = end_token[0] + end_token[1];

//   var max = $("#delivery_max_count").val();

//   // alert(st_str);
//     $.post("/salesetting/save_time_and_delivery",
//       {
//           company_serial : company_serial,
//           start_time : st_str, //수정사항 10/3일.
//           end_time : end_str,
//           max : max
//       },
//       function(data, status){
//             // alert(data);
//                     var result = JSON.parse(data);
//                     if(result.code=="S01"){
//                         alert("저장 되었습니다.");
//                     }else{
//                         alert(result.message);
//                     }

//       });

// });



$('select[multiple]').multiselect({
  columns: 3,
  placeholder: '서비스 지역 선택',
  selectGroup   : true, // select entire optgroup
  minHeight  : 400,   // minimum height of option overlay
});
</script>

<script src="http://rengo.co.kr/partner/js/jquery.multiselect.js"></script>
<style>
ul,li { margin:0; padding:0; list-style:nont;}
.label { color:#000; font-size:12px;}
</style>