<!-- 사업자 등록증 사진 모달 -->
<div class="modal fade" id="dialog_business_regist" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 id="dialog_title" class="modal-title">사업자 등록증</h4>
			</div>
				<div class="modal-body">
					<div class="row" align="center" id="div_business">

					</div>
				</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
			</div>

		</div>
	</div>
</div>

<!-- 통장사본 사진 모달 -->
<div class="modal fade" id="dialog_bankbook" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 id="dialog_title" class="modal-title">통장 사본</h4>
			</div>

				<div class="modal-body">
					<div class="row" align="center" id="div_bank">

					</div>
				</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
			</div>

		</div>
	</div>
</div>

<!-- map 모달 -->
<div class="modal" id="dialog_map" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 id="dialog_title" class="modal-title">지도</h4>
			</div>

				<div class="modal-body">
					<div id="naver_map" style="width:100%;height:350px;"></div>
				</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
			</div>

		</div>
	</div>
</div>
