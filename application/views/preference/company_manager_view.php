<link href="/telerik/examples/content/shared/styles/examp름les-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">


<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>
<script src="/telerik/examples/content/shared/js/console.js"></script>

<div class="row">
  <div class="col-md-12">
    <div class="panel border_gray">
      <div class="panel-heading bb_ccc">
        <div class="row">
          <div class="col-md-2">
            <h3 class="mgt20 mgb10">지점 관리</h3>
          </div>
          <div class="col-md-3  col-md-offset-4 mgt20">
            <select class="form-control" id="car_company_select">
            <?php foreach($company_list as $company):?>
                <option value="<?php echo $company['serial']; ?>"> <?php echo $company['company_name']; ?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
      </div>

      <div class="panel-footer">
        <div class="row">
          <div class="list_data_box">
            <div class="col-lg-1 col-md-2 col-xs-3 f_전체">전체차량</div>
            <div class="col-lg-1 col-md-2 col-xs-3 text-right" ></div>
            <div class="col-lg-1 col-md-2 col-xs-3 f_미운행">미운행</div>
            <div class="col-lg-1 col-md-2 col-xs-3 text-right" ></div>
            <div class="col-lg-1 col-md-2 col-xs-3 f_검사중">검사중</div>
            <div class="col-lg-1 col-md-2 col-xs-3 text-right" ></div>
            <div class="col-lg-1 col-md-2 col-xs-3 f_정상">정상차량</div>
            <div class="col-lg-1 col-md-2 col-xs-3 text-right" ></div>
            <div class="col-lg-1 col-md-2 col-xs-3 f_수리중">수리중</div>
            <div class="col-lg-1 col-md-2 col-xs-3 text-right" ></div>
          </div>
        </div>
      </div>

      <div class="panel-footer bg_fff">
        <div class="panel-body">
          <div class="panel panel-define magb40">
            <div class="panel-heading well bb0 bg_fff" style="margin-bottom:0;">
              <div class="row">
                <h5 class="pull-left pdl15">지점관리 리스트</h5>
                <div class="pull-right mgr15">
                  <button id="branch_delete_btn" type="button" class="btn btn-defalut">삭제</button>
                  <button id="branch_add_btn" type="button" class="btn action_btn" data-toggle="modal" data-target="#branch_dialog">추가</button>
                </div>
              </div>
            </div>

          <div class="row">
            <div class="col-sm-12">
                <div id="grid">
                  
                </div>                
              </div>
            </div>
          </div>
        </div>
      </div>          

    </div>              
  </div>
</div>


<!-- Modal 지역정보 등록-->
<div class="modal fade" id="branch_dialog" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button id="" type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="" class="modal-title">지점관리 추가</h4>
      </div>

      <div class="modal-body">
        <div class="row">
          <div >
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">지점명</label>
                <div class="col-sm-10">
                  <input type="" id="branch_name" class="form-control" value="" placeholder="예) 금정영업소" id="">
                </div>
              </div>
            </form>
          </div>

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">주소</label>
                <div class="col-sm-10 ">
                  <input type="" id="branch_address" class="form-control" value="" placeholder="" id="special_memo">
                </div>
              </div>
            </form>
          </div>

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">대표번호</label>
                <div class="col-sm-10 ">
                  <input type="" id="branch_phone" class="form-control" value="" placeholder="예) 15881600" id="special_memo">
                </div>
              </div>
            </form>
          </div>

        </div>
      </div>

      <div class="modal-footer">
          <button id="branch_close_btn" type="button" class="btn btn-defalut" data-dismiss="modal">닫기</button>
          <button id="branch_save_btn" type="button" class="btn action_btn">저장</button>
        <!-- <button id="branch_update_btn" type="button" class="btn btn-primary" style="display:none;">수정</button> -->

      </div>
    </div>
  </div>
</div>
<!--/ Modal 지역정보 등록-->

<script> 

    var is_register = true;

//슈퍼 마스터가 아니면 회사 선택 select 안보임
   if($("#car_company_select option").size()==1){
    $("#car_company_select").hide();
   }else{
    $("#car_company_select").show();
   }

        // $(document).ready(function() {

   $("#grid").kendoGrid({
                navigatable: true,  //키보드로 표 조작 할수 있게
                selectable: true, //선택할수 있도록
                allowCopy: true, //값 copyrksm
                height: 400, //높이????
                sortable: true, //정렬가능하도록
                filterable: true, //필터(비교해서 정렬)가능하도록
                groupable: false, //그룹으로 정렬 가능
                pageable: {
                    input: true,
                    messages: {
                        display: "Showing {0}-{1} from {2} data items",
                        empty: "No data"
                    }
                  },
                noRecords: {
                    template: "현재 페이지에서 보여줄 내용이 없습니다."
                  },
                columnMenu: {
                    sortable: false,
                    messages: {
                        columns: "표시할 항목 선택",
                        filter: "필터",
                    }
                 },
                dataSource: {
                    transport: {
                         read: {
                           url: "/companymanager/get_branch/<?php echo $company_serial; ?>",
                           dataType: "json"
                         }
                       },
                    schema: {
                        model: {
                            fields: {
                                serial: { type: "number" },
                                company_serial: { type: "number" },
                                branch_name: {type:"string"},
                                branch_address: {type:"string"},
                                branch_phone: { type: "string" }, 
                            }
                        }
                    },
                    pageSize: 30
                },
               
                columns: [
             // {
             //      field: "admin_id",
             //      title: "아이디",
             //      width: 200
             // },
                {
                  field: "branch_name",
                  title: "지점명",
                  width: 140
              
                },
              {
                  field: "branch_address",
                  title: "주소",
                  width: 200
              },{
                  field: "branch_phone",
                  title: "대표번호",
                  width: 200
              },
          ]
});


//         var grid = $("#grid").data("kendoGrid");
//         var btn_save = $("#save_btn");
//         var btn_update = $("#update_btn");
//         var btn_delete = $("#delete_btn");
        
//         btn_delete.click(function() {
//             var grid = $("#grid").data("kendoGrid");
//             var row = grid.select();
//             var data = grid.dataItem(row);
//             if(data==null){
//                 alert("삭제할 제조사를 선택해 주세요.");
//                 return false;
//             }
// // alert(data.serial);

//             $.post("/staffmanager/delete",
//             {
//                     serial : data.serial //수정사항 10/3일.
//             },
//             function(data, status){
//                     var result = JSON.parse(data);
//                     if(result.code=="S01"){
//                         alert("삭제되었습니다.");
//                         //데이터 다시 읽어옴
//                         basic_grid_refresh();
//                     }else{
//                         alert("데이터 삭제 중 에러가 발생했습니다.");
//                     }

//             });
//             });




$("#branch_delete_btn").click(function(){

    var grid = $('#grid').data('kendoGrid'); 
    var row = grid.select();
    var data = grid.dataItem(row);


      $.post("/companymanager/delete_branch",
        {
              serial : data.serial
        },
        function(data){
             var result = JSON.parse(data);
              if(result.code=="S01"){
                      alert("삭제되었습니다.");
                      // $('#branch_dialog').modal('toggle');
                      grid_refresh();

              }else{
                      alert(result.message);
              }
        });
});

$("#branch_add_btn").click(function(){
  is_register = true;
   $('#branch_name').val('');
   $('#branch_address').val('');
   $('#branch_phone').val('');
});

$("#branch_save_btn").click(function(){

  if($("#car_company_select").is(":visible")){
    var company_serial = $("#car_company_select option:selected").val();
  }else{
    var company_serial = <?php echo $company_serial; ?>;
  }


    var grid = $('#grid').data('kendoGrid'); 
    var row = grid.select();
    var data = grid.dataItem(row);

    if(is_register){
        $.post("/companymanager/add_branch",
        {
              company_serial : company_serial ,
              branch_name : $('#branch_name').val(),
              branch_address : $('#branch_address').val(),
              branch_phone: $('#branch_phone').val(),

        },
        function(data){
             var result = JSON.parse(data);
              if(result.code=="S01"){
                      alert("등록되었습니다.");
                      $('#branch_dialog').modal('toggle');
                      grid_refresh();

              }else{
                      alert(result.message);
              }
        });
    }else{
         $.post("/companymanager/update_branch",
        {
            serial : data.serial,
            company_serial : company_serial ,
            branch_name : $('#branch_name').val(),
            branch_address : $('#branch_address').val(),
            branch_phone: $('#branch_phone').val(),
        },
        function(data){
            var result = JSON.parse(data);
              if(result.code=="S01"){
                      alert("수정되었습니다.");
                      $('#branch_dialog').modal('toggle');
                      grid_refresh();

              }else{
                      alert(result.message);
              }
        });
    }

});




$("#grid").delegate("tbody>tr", "dblclick", function(){
   is_register = false;
    var grid = $('#grid').data('kendoGrid'); 
    var row = grid.select();
    var data = grid.dataItem(row);


   $('#branch_name').val(data.branch_name);
   $('#branch_address').val(data.branch_address);
   $('#branch_phone').val(data.branch_phone);

    $('#branch_dialog').modal('toggle');

});


//////////////////////////////////////////////////////////////// 파트너 선택 리스너

$( "#car_company_select" ).change(function() {

   grid_refresh();
 });

function grid_refresh(){

  if($("#car_company_select").is(":visible")){
    var company_serial = $("#car_company_select option:selected").val();
  }else{
    var company_serial = <?php echo $company_serial; ?>;
  }

   var sourceUrl = "/companymanager/get_branch/" + company_serial;
    var dataSource = new kendo.data.DataSource({
         transport: {
          read: {
           url: sourceUrl,
           dataType: "json"
           }
        },
        pageSize: 30
    });

    var grid = $("#grid").data("kendoGrid");
    grid.setDataSource(dataSource);
}

/////////////////////////////////////////////////////////////// 브런치 선택 리스너

    $('#branch_dialog').draggable({
      handle: ".modal-header"
    });

  </script>



