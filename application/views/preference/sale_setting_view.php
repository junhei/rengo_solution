<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<link href="/css/rengo_login.css" rel="stylesheet">
<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">

<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>
<script src="/telerik/js/cultures/kendo.culture.ko-KR.min.js"></script>
<!-- 새로운 datepicker -->
<link href="/css/datepicker.min.css?<?=time();?>" rel="stylesheet" type="text/css">
<script src="/js/datepicker.js?<?=time();?>"></script>
<script src="/js/i18n_datepicker.en.js"></script>

<style type="text/css">
html{overflow-x:hidden;}
</style>
<?php
    require("/home/apache/CodeIgniter-3.0.6/application/views/rengo_util.html");
?>


<!-- <script type="text/javascript">
      $(function () {
        $('#partner_time_st').datetimepicker({
          locale: 'ko',
          format: 'HH:mm',
          stepping: 30,
      });
    });
</script>

<script type="text/javascript">
  $(function () {
    $('#partner_time_end').datetimepicker({
      locale: 'ko',
      format: 'HH:mm',
      stepping: 30,
  });
});
</script> -->

<div class="row">
  <div class="col-md-12">
    <div class="panel border_gray">
      <div class="panel-heading">
        <div class="row">
          <div class="col-md-9">
            <h3 class="mgt20 mgb10">판매설정</h3>
          </div>
          <div class="col-sm-3">
            <div class="col-md-12 mgt20">
              <select class="form-control" id="company_select">
                <?php foreach($company_list as $company):?>
                  <option value="<?php echo $company['serial']; ?>"> <?php echo $company['company_name']; ?></option>
                <?php endforeach;?>
              </select>
            </div>
           </div>
         </div>
       </div>

      <div class="panel-footer bg_fff">
       <!--  <div class="panel-body">
          <div class="panel panel-define magb40">
            <div class="panel-heading well bg_fff bb0 magb0" style="">
              <div class="row">

                <h5 class="pull-left pdl15">기본요금설정</h5>
                <div class="pull-right mgr15">
                  <button id="basic_price_add_btn" type="button" class="btn action_btn" >저장</button>
                </div>
              </div>
            </div>

            <div class="row mgr0 magl0 bg_fff well" style="padding:0;border-radius:0; ">

              <div class="col-md-12 well" style="padding:0;border-top:none; border-right:0; border-left: 0;">
                <form class="form-horizontal">
                  <div class="form-group" style="margin-bottom:0; border-radius: 0;">

                    <div class="col-sm-1"></div>
                    <div class="col-sm-11">
                      <div class="col-sm-6 br_ccc">
                        <label for="" class="col-sm-12 basic_price_font" >주중 대여료</label>
                      </div>

                      <div class="col-sm-6">
                        <label for="" class="col-sm-12 basic_price_font">주말 대여료</label>
                      </div>
                    </div>
                  </div>
                </form>
              </div>

              <div class="col-md-12">
                <form class="form-horizontal">

                  <div class="form-group">
                    <label for="" class="col-sm-1 control-label text-left pdl0 insurance_txt">24시간</label>

                    <div class="col-sm-11">
                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" value="100" disabled>
                        <span>%</span>
                      </div>
                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" value="100" disabled>
                        <span>%</span>
                      </div>
                    </div>
                  </div>

                </form>
              </div>

              <div class="col-md-12">
                <form class="form-horizontal">

                  <div class="form-group">
                    <label for="" class="col-sm-1 control-label text-left pdl0 insurance_txt">6시간</label>

                    <div class="col-sm-11">
                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" id="input_6_hour_week">
                        <span>%</span>
                      </div>

                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" id="input_6_hour_weekend">
                        <span>%</span>
                      </div>
                    </div>
                  </div>
                </form>
              </div>

              <div class="col-md-12">
                <form class="form-horizontal">

                  <div class="form-group">
                    <label for="" class="col-sm-1 control-label text-left pdl0 insurance_txt">1시간</label>

                    <div class="col-sm-11">
                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" id="input_1_hour_week">
                        <span>%</span>
                      </div>

                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" id="input_1_hour_weekend">
                        <span>%</span>
                      </div>
                    </div>
                  </div>

                </form>
              </div>

            </div>
          </div>
        </div> -->
        <div class="panel-body">
            <div class="panel panel-define magb40">
                <div class="panel-heading well bg_fff bb_ccc" style="margin-bottom:0;">
                    <div class="row bb_ccc" style="padding-bottom:19px;">
                        <h5 class="pdl15 pull-left">판매설정</h5>
                        <div class="pull-right mgr15">
                            <button id="button_setting_save" type="button" class="btn action_btn" >저장</button>
                        </div>
                    </div>

                    <div class="row" style="padding-top:35px;">
                        <div class="col-md-6">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label text-left pdl0 pdr0">영업시간</label>
                                    <div class="col-sm-10">
                                        <!-- <ul style="list-style: none; margin: 0; padding: 0;">
                                            <li style="float:left; width: 160px; margin: 0; padding: 0; border-radius: 0;">
                                                <div class='input-group date' id='partner_time_st'>
                                                    <input type='text' id="partner_time_st_input" class="form-control input-lg" maxlength="5" placeholder="개점 시간" >
                                                    <span class="input-group-addon" style="border-radius: 0;">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </li>
                                            <li style="float:left; width: 30px; margin: 0; padding: 0; text-align: center; line-height: 46px;">
                                                ~
                                            </li>
                                            <li style="float:left; width: 160px; margin: 0; padding: 0; border-radius: 0;">
                                                <div class='input-group date' id='partner_time_end'>
                                                    <input type='text' id="partner_time_end_input" class="form-control input-lg" maxlength="5" placeholder="폐점 시간">
                                                    <span class="input-group-addon" style="border-radius: 0;">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </li>
                                        </ul> -->
                                        <div class="col-sm-5">
                                            <select id='partner_time_st' class="form-control">
<!--                                                 <option>1</option> -->
                                            </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <p>~</p>
                                        </div>
                                        <div class="col-sm-5">
                                            <select id='partner_time_end' class="form-control">
                                                <!-- <option>1</option> -->
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label text-left pdl0 pdr0">시간당 배차가능 횟수</label>
                                    <div class="col-sm-10">
                                        <select id="select_max_delivery" class="form-control">
                                            <option value="1">1건</option>
                                            <option value="2">2건</option>
                                            <option value="3">3건</option>
                                            <option value="4">4건</option>
                                            <option value="5">5건</option>
                                            <option value="6">6건</option>
                                            <option value="7">7건</option>
                                            <option value="8">8건</option>
                                            <option value="9">9건</option>
                                            <option value="10">10건</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div> 
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label text-left pdl0 pdr0">예약가능기간</label>
                                    <div class="col-sm-10">
                                        <div class="col-sm-5">
                                        <select id="select_reservation_start" class="form-control">
       
                                        </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <p>~</p>
                                        </div>
                                        <div class="col-sm-5">
                                            <select id="select_reservation_end"  class="form-control">
     
                                            </select>
                                        </div>    
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

                    <!-- <div class="row pdt15">
                        <div class="col-md-4">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label text-left pdl0 pdr0">영업시간</label>
                                    <div class="col-sm-10">
                                        <ul style="list-style: none; margin: 0; padding: 0;">
                                            <li style="float:left; width: 160px; margin: 0; padding: 0; border-radius: 0;">
                                                <div class='input-group date' id='partner_time_st'>
                                                    <input type='text' id="partner_time_st_input" class="form-control input-lg" maxlength="5" placeholder="개점 시간" >
                                                    <span class="input-group-addon" style="border-radius: 0;">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </li>
                                            <li style="float:left; width: 30px; margin: 0; padding: 0; text-align: center; line-height: 46px;">
                                                ~
                                            </li>
                                            <li style="float:left; width: 160px; margin: 0; padding: 0; border-radius: 0;">
                                                <div class='input-group date' id='partner_time_end'>
                                                    <input type='text' id="partner_time_end_input" class="form-control input-lg" maxlength="5" placeholder="폐점 시간">
                                                    <span class="input-group-addon" style="border-radius: 0;">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label text-left pdl0 pdr0">시간당 배차가능 횟수</label>
                                    <div class="col-sm-8">
                                        <select class="form-control">
                                            <option>1건</option>
                                            <option>2건</option>
                                            <option>3건</option>
                                            <option>4건</option>
                                            <option>5건</option>
                                            <option>6건</option>
                                            <option>7건</option>
                                            <option>8건</option>
                                            <option>9건</option>
                                            <option>10건</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-4">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label text-left pdl0 pdr0">예약가능기간설정</label>
                                    <div class="col-sm-8">
                                        <select class="form-control">
                                            <option>1건</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div> -->

        
        <div class="panel-body">
          <div class="panel panel-define magb40">
            <div class="panel-heading well bg_fff bb0" style="margin-bottom:0;">
              <div class="row">
                <h5 class="pdl15 pull-left">장기할인</h5>
                <div class="pull-right mgr15">
                  <!-- <button id="period_price_delete" type="button" class="btn mgr7">삭제</button> -->
                  <button id="period_price_add" type="button" class="btn action_btn" data-toggle="modal" data-target="#longtime_cash">추가</button>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div id="period_price_grid"></div>                
              </div>
            </div>
          </div>
        </div>

        <div class="panel-body">
          <div class="panel panel-define">
            <div class="panel-heading well bg_fff bb0" style="margin-bottom:0;">
              <div class="row">
                <h5 class="pull-left pdl15">특정기간할인</h5>
                <div class="pull-right mgr15">
                  <!-- <button id="special_price_delete" type="button" class="btn mgr7">삭제</button> -->
                  <button id="special_price_add" type="button" class="btn action_btn" data-toggle="modal" data-target="#certain_cash">추가</button>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div id="special_price_grid"></div>                
              </div>
            </div>
          </div>
        </div>

      </div>          
    </div>              
  </div>
</div>


<script type="text/javascript">

kendo.culture("ko-KR");

  function add_div(){

    var div = document.createElement('div');
    div.innerHTML = document.getElementById('room_type').innerHTML;
    document.getElementById('field').appendChild(div);
  }

  function remove_div(obj) {
    document.getElementById('field').removeChild(obj.parentNode);
  }
</script>








<!-- Modal 장기할읹 등록-->
<div class="modal fade" id="longtime_cash" role="dialog">
  <div class="modal-dialog modal_dialog500">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button id="" type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="" class="modal-title">장기할인 설정</h4>
      </div>

      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12 pdt15 form-horizontal">
            <div class="form-group">
              <label class="col-sm-1 pdr0 pdl0 control-label text-left">기간</label>
              <div class="col-sm-11 long_time_discount"> 
                  <select class="form-control" id="period_days">
                      <option value="2">2일</option>
                      <option value="3">3일</option>
                      <option value="4">4일</option>
                      <option value="5">5일</option>
                      <option value="6">6일</option>
                      <option value="7">7일</option>
                      <option value="8">8일</option>
                      <option value="9">9일</option>
                      <option value="10">10일</option>
                      <option value="15">15일</option>
                      <option value="20">20일</option>
                      <option value="25">25일</option>
                      <option value="30">30일</option>
                      <option value="40">40일</option>
                      <option value="50">50일</option>
                      <option value="60">60일</option>
                      <option value="70">70일</option>
                      <option value="80">80일</option>
                      <option value="90">90일</option>
                   </select>
             </div> 
           </div>
          </div>
          <div class="col-sm-12 form-horizontal">
            <div class="form-group">
             <label for="" class="col-sm-1 pdl0 pdr0 control-label text-left">적용률</label>
             <div class="col-sm-11 percent">
              <input type="number" class="form-control" value=""  id="period_percent">
              <span>%</span>
            </div>
          </div>
        </div>

      </div>
    </div>

    <div class="modal-footer">
      <button type="button" class="btn pull-left" id="period_price_delete">삭제</button>
      <button id="" type="button" class="btn" data-dismiss="modal">닫기</button>
      <button id="period_price_save_btn" type="button" class="btn btn-primary">저장</button>
      <button id="period_price_update_btn" type="button" class="btn btn-primary" style="display:none;">수정</button>

    </div>
  </div>
</div>
</div>
<!--/ Modal 장기할인 등록-->


<!-- Modal 특정할인 등록-->
<div class="modal fade" id="certain_cash" role="dialog">
  <div class="modal-dialog modal_dialog500">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button id="" type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="" class="modal-title">특정기간할인 설정</h4>
      </div>

      <div class="modal-body">
        <div class="row">

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-1 control-label text-left pdr0 pdl0">날짜선택</label>
                <div class="col-sm-11 ">

                  <div class="input-daterange input-group" id="datetest">
                    <input type="text" class="form-control" id="start_date" data-date-format="yyyy년 mm월 dd일" readonly placeholder="시작일" style="background:#FFF;">
               <!--    <div class="input-daterange input-group" id="datetest">
               <input type="text" class="input-sm form-control" name="from" placeholder="날짜 선택" id ="start_date"/> -->
               <script language="javascript">
                $('#start_date').datepicker({
                  navTitles: {
                   days: 'yyyy년 mm월'
                 },
                      //  todayButton: new Date(),
                      clearButton: true,
                      closeButton: true,
                      language: 'en',
                      onSelect: function (fd, d, picker) {
                              // Do nothing if selection was cleared
                              if (!d) return;
                              startDate = d.getFullYear()+"-"+zeroPad(d.getMonth()+1,2)+"-"+zeroPad(d.getDate(),2);
                            }
                          })
                        </script>
                      <!--   <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span> -->
                        <input type="text" class="form-control" id="end_date" data-date-format="yyyy년 mm월 dd일" readonly placeholder="종료일" style="background:#FFF;">
                        <script language="javascript">
                          $('#end_date').datepicker({
                            navTitles: {
                             days: 'yyyy년 mm월'
                           },
                            //  todayButton: new Date(),
                            clearButton: true,
                            closeButton: true,
                            language: 'en',
                            onSelect: function (fd, d, picker) {
                                    // Do nothing if selection was cleared
                                    if (!d) return;
                                    endDate = d.getFullYear()+"-"+zeroPad(d.getMonth()+1,2)+"-"+zeroPad(d.getDate(),2);
                                  }
                                })
                              </script>
                              <!-- <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                              </span> -->
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div >

<!--                       <div class="col-sm-11 long_time_discount"> 
                <input type="number" class="form-control" value="" placeholder="3" id="period_days">
               <span>일 이상</span>  
             </div>  -->

                      <form class="form-horizontal">
                        <div class="form-group">
                          <label for="" class="col-sm-1 control-label text-left pdr0">적용률</label>
                          <div class="col-sm-11 percent">
                            <input type="number" class="form-control" value="" id="special_percent">
                             <span>%</span> 
                          </div>
                        </div>
                      </form>
                    </div>

                    <div>
                      <form class="form-horizontal">
                        <div class="form-group">
                          <label for="" class="col-sm-1 control-label text-left pdr0">비고</label>
                          <div class="col-sm-11 ">
                            <input type="" class="form-control" value="" placeholder="" id="special_memo">
                          </div>
                        </div>
                      </form>
                    </div>

                  </div>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn pull-left" id="special_price_delete">삭제</button>
                  <button id="" type="button" class="btn" data-dismiss="modal">닫기</button>
                  <button id="special_price_save_btn" type="button" class="btn btn-primary">저장</button>
                  <button id="special_price_update_btn" type="button" class="btn btn-primary" style="display:none;">수정</button>

                </div>
              </div>
            </div>
          </div>
          <!--/ Modal 특정할 등록-->
          <!-- 기본요금설 그리드 -->


<script> 



 //기간 grid 초기화
 $("#period_price_grid").kendoGrid({
          selectable: true, //선택할수 있도록
          height: 200, //높이????
          sortable: true, //정렬가능하도록
          pageable: false,
          // pageable: {
          //     input: true,
          //     messages: {
          //         display: "Showing {0}-{1} from {2} data items",
          //         empty: "No data"
          //     }
          //   },
          noRecords: {
            template: "기간 할인이 입력되어 있지 않습니다. [추가]를 눌러 등록해보세요."
          },
          dataSource: {
            transport: {
             read: {
              url: "/preference/get_period_price/<?php echo $company_serial; ?>",
              dataType: "json"
            }
          },
          schema: {
            model: {
              fields: {
                days: { type: "number" },
                percent: { type: "number" },
                sales: {type:"count"},
                registration_date: {type:"date"}
              }
            }
          },
          pageSize: 10
        },

        columns: [
        {
          field: "days",
          title: "기간",
          format:  "{0:##,#} 일 이상",
        },  {
          field: "percent",
          title: "적용률",
          format:  "{0:##,#}%",


        }
        ]
      });
 //특정할인 grid 초기화
 $("#special_price_grid").kendoGrid({
                navigatable: true,  //키보드로 표 조작 할수 있게
                selectable: true, //선택할수 있도록
                allowCopy: true, //값 copyrksm
                height: 200, //높이????
                sortable: true, //정렬가능하도록
                // filterable: true, //필터(비교해서 정렬)가능하도록
                pageable: false,
                noRecords: {
                  template: "특정 할인이 입력되어 있지 않습니다. [추가]를 눌러 등록해보세요."
                },
                dataSource: {
                      transport: {
                       read: {
                        url: "/preference/get_special_price/<?php echo $company_serial; ?>",         
                        dataType: "json"
                      }
                    },
                    schema: {
                      model: {
                        fields: {
                          start_date: { type: "date" },
                          end_date: { type: "date" },
                          percent: {type:"number"},
                          memo: {type:"string"},
                                  }
                                }
                              },
                          pageSize: 10
                        },

                        columns: [ 
                        {
                          field: "start_date",
                          title: "시작날짜",
                          format: "{0: yyyy년 MM월 dd일}",
                          // width: 200
                        },  {
                          field: "end_date",
                          title: "종료날짜",
                          format: "{0: yyyy년 MM월 dd일}",
                          // width: 200
                        },  {
                          field: "percent",
                          title: "적용률",
                          // width: 140,
                          format:  "{0:##,#}%",
                        },  {
                          field: "memo",
                          title: "비고",
                          // width: 100

                        }
                        ]
 });


setTimeAndCount();
getSetting();
period_grid_refresh();
special_grid_refresh();
show_company_select();

$('.modal').on('hidden.bs.modal',function() {
  $('.modal_scroll').scrollTop(0);
  setTimeout(function(){
    $('.modal').css({
      'top':'0',
      'left':'0'
    });

    },30)
})

$('.modal').draggable();






//company select 리스너
$( "#company_select" ).change(function() {
    // var selectedSerial = $(this).val();
  getSetting();
  period_grid_refresh();
  special_grid_refresh();


});




     



        // var grid = $("#period_price_grid").data("kendoGrid");
        var btn_period_price_register = $("#period_price_save_btn");
        var btn_period_price_update = $("#period_price_update_btn");
        var btn_period_price_delete = $("#period_price_delete");
        var btn_period_price_add = $("#period_price_add");
        
        btn_period_price_add.click(function() {

        //  if(<? echo $company_serial; ?> == 0){
        //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
        //   return false;
        // }

          var grid = $("#period_price_grid").data("kendoGrid");
          var row = grid.select();
          var data = grid.dataItem(row);

          $('#period_price_save_btn').show();
          $('#period_price_update_btn').hide(); 

          $('#period_days').val('');
          $('#period_percent').val('');

      });

        btn_period_price_delete.click(function() {

          // if(<? echo $company_serial; ?> == 0){
          //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
          //   return false;
          // }
          var grid = $("#period_price_grid").data("kendoGrid");
          var row = grid.select();
          var data = grid.dataItem(row);
          if(data==null){
            alert("삭제할 요금제를 선택해 주세요.");
            return false;
          }


          $.post("/preference/period_price_delete/",
          {
                    // days: $('#period_days').val() ,
                    // percent : $('#period_percent').val(),
                    serial : data.serial
                    // car_index : data.car_index //수정사항 10/3일.
                  },
                  function(data, status){
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                      alert("삭제되었습니다.");
                        //데이터 다시 읽어옴
                        $('#longtime_cash').modal('toggle');
                        period_grid_refresh();
                      }else{
                        alert("데이터 삭제 중 에러가 발생했습니다.");
                      }

                    });
        });

//기간 할인 등록
btn_period_price_register.click(function(){

  var company_serial = get_company_serial();

  if($('#period_days').val() <= 0 ){
     alert("기간은 양수만 입력 가능 합니다.");
     $('#period_days').val('');
     $('#period_days').focus();
     return false;
  }

  if($('#period_percent').val() <= 0 ||  $('#period_percent').val() > 100){
     alert("적용률은 0~100사이의 수만 입력 가능 합니다.");
     $('#period_percent').val('');
     $('#period_percent').focus();
     return false;
  }

  $.post("/preference/period_price_add/",
  {
    company_serial : company_serial,
    days: $('#period_days').val() ,
    percent : $('#period_percent').val()

  },
  function(data, status){
                      // alert ("hi2");
                      var result = JSON.parse(data);
                      if(result.code=="S01"){
                        alert("등록되었습니다.");
                          //데이터 다시 읽어옴
                          // grid.refresh();
                          // alert(result.car_index);
                          // $('#company_serial').val(result.company_serial);
                          // $('#detail_car_index').val(result.car_index);

                          // alert($('#file_upload').submit());
                          // return false;

                          period_grid_refresh();
                           $('#longtime_cash').modal('toggle');
                          // $("[data-dismiss=modal]").trigger({ type: "click" });
                          // $('.modal-backdrop').toggle();
                      }else if(result.code=="E02"){//이미 있는 경우
                        alert("이미 등록 된 기간입니다.");
                      }else{
                        alert("데이터 입력 중 에러가 발생했습니다.");
                      }

                    });

});

//기간 할인 업데이트
btn_period_price_update.click(function(){
          var grid = $("#period_price_grid").data("kendoGrid");
          var row = grid.select();
          var data = grid.dataItem(row);

          var company_serial = get_company_serial();

          if($('#period_days').val() < 0 ){
             alert("기간은 양수만 입력 가능 합니다.");
             $('#period_days').val('');
             $('#period_days').focus();
             return false;
          }

          if($('#period_percent').val() < 0 ||  $('#period_percent').val() > 100){
             alert("적용률은 0~100사이의 수만 입력 가능 합니다.");
             $('#period_percent').val('');
             $('#period_percent').focus();
             return false;
          }

          $.post("/preference/period_price_update/",
          {

            serial : data.serial,
            company_serial : company_serial,
            days: $('#period_days').val() ,
            percent : $('#period_percent').val()

          },
          function(data, status){
            // alert(data);
            var result = JSON.parse(data);

            if(result.code=="S01"){
              alert("수정되었습니다.");
                        //데이터 다시 읽어옴
                        // grid.refresh();
                        // alert(result.car_index);
                        // $('#company_serial').val(result.company_serial);
                        // $('#detail_car_index').val(result.car_index);

                        // alert($('#file_upload').submit());
                        // return false;

                        period_grid_refresh();
                        $('#longtime_cash').modal('toggle');
                        // $("[data-dismiss=modal]").trigger({ type: "click" });
                        // $('.modal-backdrop').toggle();
                    }else if(result.code=="E02"){//이미 있는 경우
                      alert("이미 등록 된 기간입니다.");
                    }else{
                      alert(result.message);
                    }

                  });

        });

        function period_grid_refresh(){

          var company_serial = get_company_serial();

          var url = "/preference/get_period_price/" + company_serial;
          var dataSource = new kendo.data.DataSource({
               transport: {
                read: {
                 url: url,
                 dataType: "json"
               }
             },
             pageSize: 10
           });
          var grid = $("#period_price_grid").data("kendoGrid");
          grid.setDataSource(dataSource);
        }

        $("#period_price_grid").delegate("tbody>tr", "dblclick", function(){
          //  if(<? echo $company_serial; ?> == 0){
          //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
          //   return false;
          // }
          var grid = $('#period_price_grid').data('kendoGrid'); 
          var row = grid.select();
          var data = grid.dataItem(row);

          $('#period_price_save_btn').hide();
          $('#period_price_update_btn').show();

          $('#period_days').val(data.days);
          $('#period_percent').val(data.percent);



          $('#longtime_cash').modal('toggle');

      });
  // });



        // $(document).ready(function() {


        // var grid = $("#special_price_grid").data("kendoGrid");
        var btn_special_price_register = $("#special_price_save_btn");
        var btn_special_price_delete = $("#special_price_delete");
        var btn_special_price_update = $("#special_price_update_btn");
        var btn_special_price_add = $("#special_price_add");
        
        btn_special_price_add.click(function() {

          // if(<? echo $company_serial; ?> == 0){
          //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
          //   return false;
          // }

          var grid = $("#special_price_grid").data("kendoGrid");
          var row = grid.select();
          var data = grid.dataItem(row);
          $('#special_price_save_btn').show();
          $('#special_price_update_btn').hide();

          $('#special_memo').val('');
          $('#special_percent').val('');

          $('#start_date').datepicker().data('datepicker').date = new Date();
          $('#end_date').datepicker().data('datepicker').date = new Date();
          $('#start_date').val('');
          $('#end_date').val('');
        });

        btn_special_price_delete.click(function() {

          // if(<? echo $company_serial; ?> == 0){
          //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
          //   return false;
          // }
          var grid = $("#special_price_grid").data("kendoGrid");
          var row = grid.select();
          var data = grid.dataItem(row);
          if(data==null){
            alert("삭제할 요금제를 선택해 주세요.");
            return false;
          }


          $.post("/preference/special_price_delete/",
          {
                    serial : data.serial //수정사항 10/3일.
                  },
                  function(data, status){
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                      alert("삭제되었습니다.");
                        //데이터 다시 읽어옴
                        special_grid_refresh();
                        $('#certain_cash').modal('toggle');
                      }else{
                        alert("데이터 삭제 중 에러가 발생했습니다.");
                      }

                    });
        });


var startDate = '';
var endDate = '';

$("#button_setting_save").click(function(){

    var company_serial = get_company_serial();

    $.post("/partnersetting/save_setting/",
    {
        company_serial : company_serial,
        start_time : $("#partner_time_st").val(),
        end_time : $("#partner_time_end").val(),
        max_count : $("#select_max_delivery").val(),
        min_period : $("#select_reservation_start").val(),
        max_period : $("#select_reservation_end").val()
    },
                  function(data, status){
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                      alert("저장되었습니다..");
                      }else{
                        alert("데이터 저장 중 에러가 발생했습니다.");
                      }

                    });

});  

btn_special_price_register.click(function(){

          $('#special_price_save_btn').show();
          $('#special_price_update_btn').hide();
                     // $('#start_date').val();
                     // $('#end_date').val();
                     // $('#special_memo').val();
                     // $('#special_percent').val();
  // alert($('#special_price_save_btn').show());

      var company_serial = get_company_serial();

      if( $('#start_date').val() > $('#end_date').val()){
        alert("종료 날짜가 과거입니다.");
        $('#end_date').val('');
        $('#end_date').focus();
        return false;
      }

      if($('#special_percent').val() <= 0){
                 alert("적용률은 0보다 커야 합니다.");
                 $('#special_percent').val('');
                 $('#special_percent').focus();
                 return false;
      }

      $.post("/preference/special_price_add/",
      {
        company_serial : company_serial,
        start_date: startDate,
        end_date : endDate,
        memo : $('#special_memo').val(),
        percent : $('#special_percent').val()

      },
      function(data, status){
                        // alert ("hi2");
                        var result = JSON.parse(data);
                        if(result.code=="S01"){
                          alert("등록되었습니다.");
                            //데이터 다시 읽어옴
                            // grid.refresh();
                            // alert(result.car_index);
                            // $('#company_serial').val(result.company_serial);
                            // $('#detail_car_index').val(result.car_index);

                            // alert($('#file_upload').submit());
                            // return false;

                            special_grid_refresh();

                            $('#certain_cash').modal('toggle');
                            // $('.modal-backdrop').toggle();
                        }else if(result.code=="E02"){//이미 있는 경우
                          alert("이미 등록 된 기간 입니다.");
                        }else{
                          alert("데이터 입력 중 에러가 발생했습니다.");
                        }

                      });

});


btn_special_price_update.click(function(){
// alert($('#start_date').val());

    var company_serial = get_company_serial();

    var grid = $("#special_price_grid").data("kendoGrid");
    var row = grid.select();
    var data = grid.dataItem(row);
    if(data==null){
      alert("수정할 요금제를 선택해 주세요.");
      return false;
    }

     if($('#special_percent').val() <= 0){
                 alert("적용률은 0보다 커야 합니다.");
                 $('#special_percent').val('');
                 $('#special_percent').focus();
                 return false;
      }


    if( $('#start_date').val() > $('#end_date').val()){
      alert("종료 날짜가 과거입니다.");
      $('#end_date').val('');
      $('#end_date').focus();
      return false;
    }




    // alert(data.serial);
        $.post("/preference/special_price_update/",
        {
          serial : data.serial,
          company_serial : company_serial,
          start_date: startDate,
          end_date : endDate,
          memo : $('#special_memo').val(),
          percent : $('#special_percent').val()

        },
        function(data, status){
                            // alert ("hi2");
                            var result = JSON.parse(data);
                            if(result.code=="S01"){
                              alert("수정되었습니다.");


                              special_grid_refresh();

                              $('#certain_cash').modal('toggle');
                                // $('.modal-backdrop').toggle();
                            }else if(result.code=="E02"){//이미 있는 경우
                              alert("이미 등록 된 기간 입니다.");
                            }else{
                              alert("데이터 입력 중 에러가 발생했습니다.");
                            }

                          });
        

});

function special_grid_refresh(){

          var company_serial = get_company_serial();

          var url = "/preference/get_special_price/" + company_serial;
          var dataSource = new kendo.data.DataSource({
           transport: {
            read: {
             url: url,
             dataType: "json"
           }
         },
         pageSize: 10,
          schema: {
                      model: {
                        fields: {
                          start_date: { type: "date" },
                          end_date: { type: "date" },
                          percent: {type:"number"},
                          memo: {type:"string"},
                          user_name: { type: "string" }, 
                          birthday: { type: "srting" }, 
                          address: { type: "string" }, 
                          phone_number1: { type: "string" },
                          phone_number2: { type: "string" },
                          corporation_name: { type: "string" }, 
                          auth_code: { type: "number" }, 
                          note: {type:"string"},
                          license_type: { type: "string" }, 
                          published_date: { type: "date" }, 
                          license_number: { type: "string" }, 
                          expiration_date: { type: "date" }, 
                                  }
                                }
                              }
       });
          var grid = $("#special_price_grid").data("kendoGrid");
          grid.setDataSource(dataSource);
}

$("#special_price_grid").delegate("tbody>tr", "dblclick", function(){

        //  if(<? echo $company_serial; ?> == 0){
        //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
        //   return false;
        // }

        var grid = $('#special_price_grid').data('kendoGrid'); 
        var row = grid.select();
        var data = grid.dataItem(row);
    // alert(data.start_date);
// alert(data.memo);
$('#special_price_save_btn').hide();
$('#special_price_update_btn').show();
      // alert($('#special_price_save_btn'));
      $('#start_date').datepicker().data('datepicker').selectDate(data.start_date);
      $('#end_date').datepicker().data('datepicker').selectDate(data.end_date);

      // $('#start_date').val(kendo.toString(data.start_date, "yyyy년 MM월 dd일"));
      // $('#end_date').val(kendo.toString(data.end_date, "yyyy년 MM월 dd일"));
      $('#special_memo').val(data.memo);
      $('#special_percent').val(data.percent);



      $('#certain_cash').modal('toggle');

    });


        // });



$('#longtime_cash, #certain_cash').draggable({
  handle: ".modal-header"
});

function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}


function setTimeAndCount(){
  //1. 영업시간
  var day = new Date('2017', '01', '01', '00', '00');
  var str = '';
  for(i=0; i<24*2; i++){
      
      tempStr = zeroPad(day.getHours(),2) +" : " + zeroPad(day.getMinutes(),2);
      str+= "<option value='" + zeroPad(day.getHours(),2) + '' + zeroPad(day.getMinutes(),2) +"'>" +tempStr + "</option>";
      day.setMinutes(day.getMinutes() + 30);
  }
  $("#partner_time_st").html(str); 

  str = '';
  for(i=0; i<24*2 ; i++){
      day.setMinutes(day.getMinutes() + 30);
      tempStr = zeroPad(day.getHours(),2) +" : " + zeroPad(day.getMinutes(),2);
      if(tempStr == "00 : 00"){
        tempStr = "24 : 00";
      }
      str+= "<option value='" + zeroPad(day.getHours(),2) + '' + zeroPad(day.getMinutes(),2) +"'>" +tempStr + "</option>";
      
  }
  $("#partner_time_end").html(str); 
  //2. 예약 가능 기간 실정
  str = '';
  for(i=2; i<24; i++){
      str+= "<option value='" + i +"'>" +i +" 시간 후 부터</option>";
  }

  for(i=1; i<=8; i++){
    str+= "<option value='" + i*24 +"'>" +i +" 일 후 부터</option>";
  }
  $("#select_reservation_start").html(str);

  str = '';
  for(i=8; i<24; i++){
      str+= "<option value='" + i +"'>" +i +" 시간 까지</option>";
  }

  for(i=1; i<=90; i++){
    str+= "<option value='" + i*24 +"'>" +i +" 일 까지</option>";
  }
  $("#select_reservation_end").html(str);
}

function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}

function getSetting(){
  var company_serial=get_company_serial();
    $.post("/partnersetting/get_setting/",
          {
              company_serial : company_serial
                    // car_index : data.car_index //수정사항 10/3일.
        },
        function(data){
          result = JSON.parse(data);
           $("#partner_time_st").val(result[0].open_time_start),
          $("#partner_time_end").val(result[0].open_time_finish),
          $("#select_max_delivery").val(result[0].delivery_max_per_hour),
          $("#select_reservation_start").val(result[0].min_reservation_period),
           $("#select_reservation_end").val(result[0].max_reservation_period)
        
    });
}

</script>





