<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>
<link href="/css/datepicker.min.css?<?=time();?>" rel="stylesheet" type="text/css">
<script src="/js/datepicker.js?<?=time();?>"></script>
<script src="/js/i18n_datepicker.en.js"></script>

<?php 
    require("/home/apache/CodeIgniter-3.0.6/application/views/master/dialog_master_manager.php");
?>

<style type="text/css">
html {
    /*overflow:hidden;*/
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel border_gray magb0">
            
            <div class="panel-heading pdt17" >
                <div class="row">
                    <div class="col-md-9">
                        <h3 class="">관리자 리스트</h3>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="tab-content">
    <div id="example" style="margin:0;">
        <div id="master_grid"></div>
    </div>
</div>

<script type="text/javascript">

$(document).ready(function() {
    setGrid();
    gridDoubleClick();
    changPasswordBtnClick();
    setBelongPartner();
    fitGridSize();
    $(window).resize(function() {
        fitGridSize();
    });
})

function setGrid() {
    var data = <?php echo json_encode($data)?>;
    $("#master_grid").kendoGrid({
        selectable: true, 
        allowCopy: true, 
        sortable: true, 
        groupable: false,
        resizable: true,
        excel: {
            allPages: true,
            fileName: "고객리스트.xlsx",
            filterable: true
        },
        pageable: {
            input: true,
            messages: {
                display: "총 {2} 명의 고객 중 {0}~{1}번째",
                empty: "데이타 없음"
            }
          },
        noRecords: {
            template: "등록된 고객이 없습니다."
          },
        dataSource: {
            transport: {
                 read: {
                   url: '/mastermanager/get_admin_list',
                   dataType: "json"
                 }
               },
            schema: {
                model: {
                    fields: {
                        serial : {type : "number"},
                        partner_serial : {type: "number"},
                        admin_id: {type: "string"},
                        admin_name: { type: "string" },
                        rent_manager: { type: "string" }, 
                        car_manager: { type: "string" },
                        user_manager : {type : "string"}, 
                        sale_manager : { type: "string" }, 
                        preference : {type:"string"},
                        master_setting : {type:"string"},
                        partner_name : {type:"string"}
                    }
                }
            },
            pageSize: 30
        },
       
        columns: [ {
                field: "admin_id",
                title: "ID",
                width: 200
            },  {
                field: "admin_name",
                title: "이름",
                width: 100
            },  {
                title: "접근권한", 
                    columns :[
                    {
                        field: "rent_manager",
                        title: "대여관리",
                        width: 50
                    },  {
                        field: "car_manager",
                        title: "차량관리",
                        width:50
                    },  {
                        field: "user_manager",
                        title: "고객관리",
                        width:50
                    },  {
                        field: "sale_manager",
                        title: "매출관리",
                        width:50
                    },  {
                        field: "rengo_manager",
                        title: "렌고설정",
                        width:50
                    },  {
                        field: "preference",
                        title: "환경설정",
                        width:50
                    },  {
                        field: "master_setting",
                        title: "마스터 설정",
                        width:50
                    }]
            },  {
                field: "partner_name",
                title: "파트너업체명",
                width: 100
            }
        ]
    });    
}

function gridDoubleClick(){
    $('#master_grid').delegate("tbody>tr", "dblclick",
      function(){
            var grid = $('#master_grid').data("kendoGrid"); 
            var row = grid.select();
            var data = grid.dataItem(row);
            
            var rent_manager = data.rent_manager == 'O'? true : false;
            var car_manager = data.car_manager  == 'O'? true : false;
            var user_manager = data.user_manager == 'O'? true : false;
            var sale_manager = data.sale_manager == 'O'? true : false;
            var rengo_manager = data.rengo_manager == 'O'? true : false;
            var preference = data.preference == 'O'? true : false;
            var master_setting = data.master_setting == 'O'? true : false;

            $('#serial').val(data.serial);
            $('#admin_id').val(data.admin_id);
            $('#admin_name').val(data.admin_name);
            $('#belong_partner').val(data.partner_serial);
            
            $('#check_rent').prop('checked',rent_manager);
            $('#check_car').prop('checked',car_manager);
            $('#check_customer').prop('checked',user_manager);
            $('#check_sales').prop('checked',sale_manager);
            $('#check_rengo').prop('checked',rengo_manager);
            $('#check_preference').prop('checked',preference);
            $('#check_master').prop('checked',master_setting);

            $('#dialog_master_manager').modal('toggle');
    });    
}

function changPasswordBtnClick() {
    $('#change_password_btn').click(function() {
        $(this).hide();
        $('#admin_pw').show();
    })
}

function setBelongPartner() {
    $.get('/mastermanager/get_partner_list',
        function(data) {
            var result = JSON.parse(data);
            for(var i=0; i<result.length; ++i){
                var option = '<option value='+result[i]['serial']+'>' + result[i]['company_name'] + '</option>';
                $('#belong_partner').append(option);     
            }      
    })
}

$('#update_btn').click(function() {
    var serial = $('#serial').val();
    var admin_id = $('#admin_id').val();
    var admin_pw = $('#admin_pw').is(':visible') ? true : false;
    
    if(admin_pw) {
        admin_pw = $('#admin_pw').val();
    }else{
        admin_pw = '';
    }

    var admin_name = $('#admin_name').val();
    var belong_partner = $('#belong_partner option:selected').val();

    var rent_manager = $('#check_rent').is(':checked') ? 'Y' : 'N';
    var car_manager = $('#check_car').is(':checked') ? 'Y' : 'N';
    var user_manager = $('#check_customer').is(':checked') ? 'Y' : 'N';
    var sale_manager = $('#check_sales').is(':checked') ? 'Y' : 'N';
    var rengo_manager = $('#check_rengo').is(':checked') ? 'Y' : 'N';
    var preference = $('#check_preference').is(':checked') ? 'Y' : 'N';
    var master_setting = $('#check_master').is(':checked') ? 'Y' : 'N';

    $.post('/mastermanager/update_admin_information',
    {
        serial : serial,
        admin_id : admin_id,
        admin_pw : admin_pw,
        admin_name : admin_name,
        company_serial : belong_partner,
        permission : rent_manager+car_manager+user_manager+sale_manager+rengo_manager+preference+master_setting
    },
    function(data) {
        var result = JSON.parse(data);
        if(result.code=="S01"){ 
            $('#dialog_master_manager').modal('toggle');
            setTimeout(function() {
                refreshGrid();                
            },300)
        }else{
            alert("error 모르게따~");
        }
    })

})

function refreshGrid() {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: '/mastermanager/get_admin_list',
                dataType: "json"
                }
           }
    });
    var grid = $("#master_grid").data("kendoGrid");

    grid.setDataSource(dataSource);
    grid.refresh();
}

function fitGridSize() {
    var gridElement = $("#master_grid");
    var gird_position = $('#master_grid').position();
    var window_height = $( window ).height() - gird_position.top - 70;
    gridElement.children(".k-grid-content").height(window_height-100);
    gridElement.height(window_height);
}

</script>



