<head>
	<meta charset="utf-8">
	<title>RENGO SOLUTION</title>

  <link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">

	<script src="/js/jquery-1.9.1.min.js"></script>
	<link href="/css/font-awesome.min.css" rel="stylesheet">
	<script src="/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<script src="/js/bootstrap-toggle.min.js"></script>

	<!-- Bootstrap Material Design -->
	<link href="/css/material_design/bootstrap-material-design.css" rel="stylesheet">
	<link href="/css/material_design/ripples.min.css" rel="stylesheet">
	<script src="/js/material_design/ripples.min.js"></script>
	<script src="/js/material_design/material.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

	<link rel="stylesheet" href="/css/rengo_solution.css?1482387553">
	<link rel="stylesheet" href="/css/rengo_solution_menu.css">
	<link href="/css/rengo_login.css" rel="stylesheet">

  <script src="/telerik/js/jszip.min.js"></script>
  <script src="/telerik/js/kendo.all.min.js"></script>
  <script src="/telerik/js/cultures/kendo.culture.ko-KR.min.js"></script>

  <link href="/css/datepicker.min.css?<?=time();?>" rel="stylesheet" type="text/css">
  <script src="/js/datepicker.js?<?=time();?>"></script>
  <script src="/js/i18n_datepicker.en.js"></script>
</head>

<div class="row mgr0 mgl0">
  <div class="col-md-12">
    <div class="panel border_gray">
<!--       <div class="panel-heading bb_ccc">
        <div class="row">
          <div class="col-md-9">
            <h3 class="mgt20 mgb10">요금설정</h3>
          </div>
          <div class="col-sm-3">
            <div class="col-md-12 mgt20">
            </div>
          </div>
        </div>
      </div> -->

<!--       <div class="panel-footer">
        <div class="row">
          <div class="list_data_box">
            <div class="col-lg-1 col-md-2 col-xs-3 f_전체">전체차량</div>
            <div class="col-lg-1 col-md-2 col-xs-3 text-right" ></div>
            <div class="col-lg-1 col-md-2 col-xs-3 f_미운행">미운행</div>
            <div class="col-lg-1 col-md-2 col-xs-3 text-right" ></div>
            <div class="col-lg-1 col-md-2 col-xs-3 f_검사중">검사중</div>
            <div class="col-lg-1 col-md-2 col-xs-3 text-right" ></div>
            <div class="col-lg-1 col-md-2 col-xs-3 f_정상">정상차량</div>
            <div class="col-lg-1 col-md-2 col-xs-3 text-right" ></div>
            <div class="col-lg-1 col-md-2 col-xs-3 f_수리중">수리중</div>
            <div class="col-lg-1 col-md-2 col-xs-3 text-right" ></div>
          </div>
        </div>
      </div> -->

      <div class="panel-footer bg_fff">
        <div class="panel-body">
          <div class="panel panel-define magb40">
            <div class="panel-heading well bg_fff bb0 magb0" style="">
              <div class="row">

                <h5 class="pull-left pdl15">기본요금설정</h5>
                <div class="pull-right mgr15">
                  <button id="basic_price_add_btn" type="button" class="btn action_btn" >저장</button>
                </div>
              </div>
            </div>

          <!-- <div class="row">
            <div class="col-sm-12">
                <div id="basic_price_grid"></div>                
              </div>
            </div> -->
            <div class="row mgr0 magl0 bg_fff well" style="padding:0;border-radius:0; ">

              <div class="col-md-12 well" style="padding:0;border-top:none; border-right:0; border-left: 0;">
                <form class="form-horizontal">
                  <div class="form-group" style="margin-bottom:0; border-radius: 0;">

                    <div class="col-sm-1"></div>
                    <div class="col-sm-11">
                      <div class="col-sm-6 br_ccc">
                        <label for="" class="col-sm-12 basic_price_font" >주중 대여료</label>
                      </div>

                      <div class="col-sm-6">
                        <label for="" class="col-sm-12 basic_price_font">주말 대여료</label>
                      </div>
                    </div>
                  </div>
                </form>
              </div>

              <div class="col-md-12">
                <form class="form-horizontal">

                  <div class="form-group">
                    <label for="" class="col-sm-1 control-label text-left pdl0 insurance_txt">24시간</label>

                    <div class="col-sm-11">
                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" value="100" disabled>
                        <span>%</span>
                      </div>
                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" value="100" disabled>
                        <span>%</span>
                      </div>
                    </div>
                  </div>

                </form>
              </div>

              <div class="col-md-12">
                <form class="form-horizontal">

                  <div class="form-group">
                    <label for="" class="col-sm-1 control-label text-left pdl0 insurance_txt">6시간</label>

                    <div class="col-sm-11">
                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" id="input_6_hour_week">
                        <span>%</span>
                      </div>

                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" id="input_6_hour_weekend">
                        <span>%</span>
                      </div>
                    </div>
                  </div>
                </form>
              </div>

              <div class="col-md-12">
                <form class="form-horizontal">

                  <div class="form-group">
                    <label for="" class="col-sm-1 control-label text-left pdl0 insurance_txt">1시간</label>

                    <div class="col-sm-11">
                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" id="input_1_hour_week">
                        <span>%</span>
                      </div>

                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" id="input_1_hour_weekend">
                        <span>%</span>
                      </div>
                    </div>
                  </div>

                </form>
              </div>

            </div>
          </div>
        </div>

        <div class="panel-body">
          <div class="panel panel-define magb40">
            <div class="panel-heading well bg_fff bb0" style="margin-bottom:0;">
              <div class="row">
                <h5 class="pdl15 pull-left">장기할인</h5>
                <div class="pull-right mgr15">
                  <button id="period_price_delete" type="button" class="btn mgr7">삭제</button>
                  <button id="period_price_add" type="button" class="btn action_btn" data-toggle="modal" data-target="#longtime_cash">추가</button>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div id="period_price_grid"></div>                
              </div>
            </div>
          </div>
        </div>

        <div class="panel-body">
          <div class="panel panel-define">
            <div class="panel-heading well bg_fff bb0" style="margin-bottom:0;">
              <div class="row">
                <h5 class="pull-left pdl15">특정할인</h5>
                <div class="pull-right mgr15">
                  <button id="special_price_delete" type="button" class="btn mgr7">삭제</button>
                  <button id="special_price_add" type="button" class="btn action_btn" data-toggle="modal" data-target="#certain_cash">추가</button>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div id="special_price_grid"></div>                
              </div>
            </div>
          </div>
        </div>

      </div>          
    </div>              
  </div>
</div>


<script type="text/javascript">

  kendo.culture("ko-KR");

  function add_div(){

    var div = document.createElement('div');
    div.innerHTML = document.getElementById('room_type').innerHTML;
    document.getElementById('field').appendChild(div);
  }

  function remove_div(obj) {
    document.getElementById('field').removeChild(obj.parentNode);
  }
</script>








<!-- Modal 장기할읹 등록-->
<div class="modal fade" id="longtime_cash" role="dialog">
  <div class="modal-dialog modal_dialog500">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button id="" type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="" class="modal-title">장기할인 설정</h4>
      </div>

      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12 pdt15 form-horizontal">
          	<div class="form-group">
          		<label class="col-sm-2 control-label text-left">기간</label>
          		<div class="col-sm-10 long_time_discount"> 
          			<input type="number" class="form-control" value="" placeholder="3" id="period_days">
               <span>일 이상</span>	
             </div>	
           </div>
            <!-- <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left">구분</label>
                <div class="col-sm-10 long_time_discount">
                  <input type="number" class="form-control" value="" placeholder="3" id="period_days">
                  <span>일 이상</span>
                </div>
              </div>
            </form> -->
          </div>
          <div class="col-sm-12 form-horizontal">
            <!-- <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left">할인율</label>
                <div class="col-sm-10 long_time_discount">
                <input type="number" class="form-control" value="" placeholder="100" id="period_percent">
                 <span>%</span>
                </div>
              </div>
            </form> -->
            <div class="form-group">
             <label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">적용률</label>
             <div class="col-sm-10 percent">
              <input type="number" class="form-control" value="" placeholder="예) 100" id="period_percent">
              <span>%</span>
            </div>
          </div>
        </div>

      </div>
    </div>

    <div class="modal-footer">
      <button id="" type="button" class="btn" data-dismiss="modal">닫기</button>
      <button id="period_price_save_btn" type="button" class="btn btn-primary">저장</button>
      <button id="period_price_update_btn" type="button" class="btn btn-primary" style="display:none;">수정</button>

    </div>
  </div>
</div>
</div>
<!--/ Modal 장기할인 등록-->


<!-- Modal 특정할인 등록-->
<div class="modal fade" id="certain_cash" role="dialog">
  <div class="modal-dialog modal_dialog500">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button id="" type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="" class="modal-title">특정할인설정</h4>
      </div>

      <div class="modal-body">
        <div class="row">

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">날짜선택</label>
                <div class="col-sm-10 ">

                  <div class="input-daterange input-group" id="datetest">
                    <input type="text" class="form-control" id="start_date" data-date-format="yyyy년 mm월 dd일" readonly placeholder="시작 날짜" style="background:#FFF;">
               <!--    <div class="input-daterange input-group" id="datetest">
               <input type="text" class="input-sm form-control" name="from" placeholder="날짜 선택" id ="start_date"/> -->
               <script language="javascript">
                $('#start_date').datepicker({
                  navTitles: {
                   days: 'yyyy년 mm월'
                 },
                      //  todayButton: new Date(),
                      clearButton: true,
                      closeButton: true,
                      language: 'en',
                      onSelect: function (fd, d, picker) {
                              // Do nothing if selection was cleared
                              if (!d) return;
                              startDate = d.getFullYear()+"-"+zeroPad(d.getMonth()+1,2)+"-"+zeroPad(d.getDate(),2);
                            }
                          })
                        </script>
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        <input type="text" class="form-control" id="end_date" data-date-format="yyyy년 mm월 dd일" readonly placeholder="종료 날짜" style="background:#FFF;">
                        <script language="javascript">
                          $('#end_date').datepicker({
                            navTitles: {
                             days: 'yyyy년 mm월'
                           },
                            //  todayButton: new Date(),
                            clearButton: true,
                            closeButton: true,
                            language: 'en',
                            onSelect: function (fd, d, picker) {
                                    // Do nothing if selection was cleared
                                    if (!d) return;
                                    endDate = d.getFullYear()+"-"+zeroPad(d.getMonth()+1,2)+"-"+zeroPad(d.getDate(),2);
                                  }
                                })
                              </script>
                              <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div >

<!--                       <div class="col-sm-10 long_time_discount"> 
                <input type="number" class="form-control" value="" placeholder="3" id="period_days">
               <span>일 이상</span>  
             </div>  -->

             <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">적용률</label>
                <div class="col-sm-10 percent">
                  <input type="number" class="form-control" value="" placeholder="100" id="special_percent">
                  <span>%</span> 
                </div>
              </div>
            </form>
          </div>

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">비고</label>
                <div class="col-sm-10 ">
                  <input type="" class="form-control" value="" placeholder="" id="special_memo">
                </div>
              </div>
            </form>
          </div>

        </div>
      </div>

      <div class="modal-footer">
        <button id="special_price_save_btn" type="button" class="btn btn-primary">저장</button>
        <button id="special_price_update_btn" type="button" class="btn btn-primary" style="display:none;">수정</button>

      </div>
    </div>
  </div>
</div>
<script> 

//company serial 얻기
function get_company_serial(){
  return <?php echo $company_serial; ?>;
}





get_basic_price();

//company select 리스너
$( "#car_company_select" ).change(function() {
    // var selectedSerial = $(this).val();
    // basic_grid_refresh();

    get_basic_price();
    period_grid_refresh();
    special_grid_refresh();


  });

// $( "#car_branch_select" ).change(function() {
//     // var selectedSerial = $(this).val();
//     // basic_grid_refresh();
//     get_basic_price();
//     period_grid_refresh();
//     special_grid_refresh();
//  });

 //기간 grid 초기화
 $("#period_price_grid").kendoGrid({
          selectable: true, //선택할수 있도록
          height: 200, //높이????
          sortable: true, //정렬가능하도록
          pageable: false,
          // pageable: {
          //     input: true,
          //     messages: {
          //         display: "Showing {0}-{1} from {2} data items",
          //         empty: "No data"
          //     }
          //   },
          noRecords: {
            template: "기간 할인이 입력되어 있지 않습니다. [추가]를 눌러 등록해보세요."
          },
          dataSource: {
            transport: {
             read: {
              url: "/preference/get_period_price/<?php echo $company_serial; ?>/0",
              dataType: "json"
            }
          },
          schema: {
            model: {
              fields: {
                days: { type: "number" },
                percent: { type: "number" },
                sales: {type:"count"},
                registration_date: {type:"date"},
                user_name: { type: "string" }, 
                birthday: { type: "srting" }, 
                address: { type: "string" }, 
                phone_number1: { type: "string" },
                phone_number2: { type: "string" },
                corporation_name: { type: "string" }, 
                auth_code: { type: "number" }, 
                note: {type:"string"},
                license_type: { type: "string" }, 
                published_date: { type: "date" }, 
                license_number: { type: "string" }, 
                expiration_date: { type: "date" }, 
              }
            }
          },
          pageSize: 30
        },

        columns: [
        {
          field: "days",
          title: "기간",
          format:  "{0:##,#} 일 이상",
        },  {
          field: "percent",
          title: "적용률",
          format:  "{0:##,#}%",


        }
        ]
      });
 //특정할인 grid 초기화
 $("#special_price_grid").kendoGrid({
                navigatable: true,  //키보드로 표 조작 할수 있게
                selectable: true, //선택할수 있도록
                allowCopy: true, //값 copyrksm
                height: 200, //높이????
                sortable: true, //정렬가능하도록
                // filterable: true, //필터(비교해서 정렬)가능하도록
                groupable: false, //그룹으로 정렬 가능
                // excel: {
                //   allPages: true,
                //   fileName: "고객리스트.xlsx",
                //   filterable: true
                // },
                pageable: false,
                // pageable: {
                //     input: true,
                //     messages: {
                //         display: "Showing {0}-{1} from {2} data items",
                //         empty: "No data"
                //     }
                //   },
                noRecords: {
                  template: "특정 할인이 입력되어 있지 않습니다. [추가]를 눌러 등록해보세요."
                },
                // columnMenu: {
                //   sortable: false,
                //   messages: {
                //     columns: "표시할 항목 선택",
                //     filter: "필터",
                //   }
                // },
                dataSource: {
                  transport: {
                   read: {
                    url: "/preference/get_special_price/<?php echo $company_serial; ?>/0",         
                    dataType: "json"
                  }
                },
                schema: {
                  model: {
                    fields: {
                      start_date: { type: "date" },
                      end_date: { type: "date" },
                      percent: {type:"number"},
                      memo: {type:"string"},
                      user_name: { type: "string" }, 
                      birthday: { type: "srting" }, 
                      address: { type: "string" }, 
                      phone_number1: { type: "string" },
                      phone_number2: { type: "string" },
                      corporation_name: { type: "string" }, 
                      auth_code: { type: "number" }, 
                      note: {type:"string"},
                      license_type: { type: "string" }, 
                      published_date: { type: "date" }, 
                      license_number: { type: "string" }, 
                      expiration_date: { type: "date" }, 
                    }
                  }
                },
                pageSize: 30
              },

              columns: [ 
              {
                field: "start_date",
                title: "시작날짜",
                format: "{0: yyyy년 MM월 dd일}",
                          // width: 200
                        },  {
                          field: "end_date",
                          title: "종료날짜",
                          format: "{0: yyyy년 MM월 dd일}",
                          // width: 200
                        },  {
                          field: "percent",
                          title: "적용률",
                          // width: 140,
                          format:  "{0:##,#}%",
                        },  {
                          field: "memo",
                          title: "비고",
                          // width: 100

                        }
                        ]
                      });





        // var grid = $("#period_price_grid").data("kendoGrid");
        var btn_period_price_register = $("#period_price_save_btn");
        var btn_period_price_update = $("#period_price_update_btn");
        var btn_period_price_delete = $("#period_price_delete");
        var btn_period_price_add = $("#period_price_add");
        
        btn_period_price_add.click(function() {

        //  if(<? echo $company_serial; ?> == 0){
        //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
        //   return false;
        // }

        var grid = $("#period_price_grid").data("kendoGrid");
        var row = grid.select();
        var data = grid.dataItem(row);

        $('#period_price_save_btn').show();
        $('#period_price_update_btn').hide(); 

        $('#period_days').val('');
        $('#period_percent').val('');

      });

        btn_period_price_delete.click(function() {

          // if(<? echo $company_serial; ?> == 0){
          //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
          //   return false;
          // }
          var grid = $("#period_price_grid").data("kendoGrid");
          var row = grid.select();
          var data = grid.dataItem(row);
          if(data==null){
            alert("삭제할 요금제를 선택해 주세요.");
            return false;
          }


          $.post("/preference/period_price_delete/",
          {
                    // days: $('#period_days').val() ,
                    // percent : $('#period_percent').val(),
                    serial : data.serial
                    // car_index : data.car_index //수정사항 10/3일.
                  },
                  function(data, status){
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                      alert("삭제되었습니다.");
                        //데이터 다시 읽어옴
                        period_grid_refresh();
                      }else{
                        alert("데이터 삭제 중 에러가 발생했습니다.");
                      }

                    });
        });

//기간 할인 등록
btn_period_price_register.click(function(){

// alert("hi");
var company_serial = get_company_serial();

if($('#period_days').val() < 0 ){
 alert("기간은 양수만 입력 가능 합니다.");
 $('#period_days').val('');
 $('#period_days').focus();
 return false;
}

if($('#period_percent').val() < 0 ||  $('#period_percent').val() > 100){
 alert("적용률은 0~100사이의 수만 입력 가능 합니다.");
 $('#period_percent').val('');
 $('#period_percent').focus();
 return false;
}

$.post("/preference/period_price_add/",
{
  company_serial : company_serial,
  days: $('#period_days').val() ,
  percent : $('#period_percent').val()

},
function(data, status){
                      // alert ("hi2");
                      var result = JSON.parse(data);
                      if(result.code=="S01"){
                        alert("등록되었습니다.");
                          //데이터 다시 읽어옴
                          // grid.refresh();
                          // alert(result.car_index);
                          // $('#company_serial').val(result.company_serial);
                          // $('#detail_car_index').val(result.car_index);

                          // alert($('#file_upload').submit());
                          // return false;

                          period_grid_refresh();

                          $("[data-dismiss=modal]").trigger({ type: "click" });
                          // $('.modal-backdrop').toggle();
                      }else if(result.code=="E02"){//이미 있는 경우
                        alert("이미 등록 된 기간입니다.");
                      }else{
                        alert("데이터 입력 중 에러가 발생했습니다.");
                      }

                    });

});

//기간 할인 업데이트
btn_period_price_update.click(function(){
  var grid = $("#period_price_grid").data("kendoGrid");
  var row = grid.select();
  var data = grid.dataItem(row);

  var company_serial = get_company_serial();

  if($('#period_days').val() < 0 ){
   alert("기간은 양수만 입력 가능 합니다.");
   $('#period_days').val('');
   $('#period_days').focus();
   return false;
 }

 if($('#period_percent').val() < 0 ||  $('#period_percent').val() > 100){
   alert("적용률은 0~100사이의 수만 입력 가능 합니다.");
   $('#period_percent').val('');
   $('#period_percent').focus();
   return false;
 }

 $.post("/preference/period_price_update/",
 {

  serial : data.serial,
  company_serial : company_serial,
  days: $('#period_days').val() ,
  percent : $('#period_percent').val()

},
function(data, status){
  var result = JSON.parse(data);

  if(result.code=="S01"){
    alert("수정되었습니다.");
                        //데이터 다시 읽어옴
                        // grid.refresh();
                        // alert(result.car_index);
                        // $('#company_serial').val(result.company_serial);
                        // $('#detail_car_index').val(result.car_index);

                        // alert($('#file_upload').submit());
                        // return false;

                        period_grid_refresh();

                        $("[data-dismiss=modal]").trigger({ type: "click" });
                        // $('.modal-backdrop').toggle();
                    }else if(result.code=="E02"){//이미 있는 경우
                      alert("이미 등록 된 기간입니다.");
                    }else{
                      alert(result.message);
                    }

                  });

});

function period_grid_refresh(){

  var company_serial = get_company_serial();

  var url = "/preference/get_period_price/" + company_serial;
  var dataSource = new kendo.data.DataSource({
   transport: {
    read: {
     url: url,
     dataType: "json"
   }
 },
 pageSize: 10
});
  var grid = $("#period_price_grid").data("kendoGrid");
  grid.setDataSource(dataSource);
}
$("#period_price_grid").delegate("tbody>tr", "dblclick", function(){
        //  if(<? echo $company_serial; ?> == 0){
        //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
        //   return false;
        // }
        var grid = $('#period_price_grid').data('kendoGrid'); 
        var row = grid.select();
        var data = grid.dataItem(row);

        $('#period_price_save_btn').hide();
        $('#period_price_update_btn').show();

        $('#period_days').val(data.days);
        $('#period_percent').val(data.percent);



        $('#longtime_cash').modal('toggle');

      });
  // });



        // $(document).ready(function() {


        // var grid = $("#special_price_grid").data("kendoGrid");
        var btn_special_price_register = $("#special_price_save_btn");
        var btn_special_price_delete = $("#special_price_delete");
        var btn_special_price_update = $("#special_price_update_btn");
        var btn_special_price_add = $("#special_price_add");
        
        btn_special_price_add.click(function() {

          // if(<? echo $company_serial; ?> == 0){
          //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
          //   return false;
          // }

          var grid = $("#special_price_grid").data("kendoGrid");
          var row = grid.select();
          var data = grid.dataItem(row);
          $('#special_price_save_btn').show();
          $('#special_price_update_btn').hide();

          $('#special_memo').val('');
          $('#special_percent').val('');

          $('#start_date').datepicker().data('datepicker').date = new Date();
          $('#end_date').datepicker().data('datepicker').date = new Date();
          $('#start_date').val('');
          $('#end_date').val('');
        });

        btn_special_price_delete.click(function() {

          // if(<? echo $company_serial; ?> == 0){
          //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
          //   return false;
          // }
          var grid = $("#special_price_grid").data("kendoGrid");
          var row = grid.select();
          var data = grid.dataItem(row);
          if(data==null){
            alert("삭제할 요금제를 선택해 주세요.");
            return false;
          }


          $.post("/preference/special_price_delete/",
          {
                    serial : data.serial //수정사항 10/3일.
                  },
                  function(data, status){
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                      alert("삭제되었습니다.");
                        //데이터 다시 읽어옴
                        special_grid_refresh();
                      }else{
                        alert("데이터 삭제 중 에러가 발생했습니다.");
                      }

                    });
        });


        var startDate = '';
        var endDate = '';

        btn_special_price_register.click(function(){

          $('#special_price_save_btn').show();
          $('#special_price_update_btn').hide();
                     // $('#start_date').val();
                     // $('#end_date').val();
                     // $('#special_memo').val();
                     // $('#special_percent').val();
  // alert($('#special_price_save_btn').show());

  var company_serial = get_company_serial();

  if( $('#start_date').val() > $('#end_date').val()){
    alert("종료 날짜가 과거입니다.");
    $('#end_date').val('');
    $('#end_date').focus();
    return false;
  }

  if($('#special_percent').val() < 0 ||  $('#special_percent').val() > 100){
   alert("적용률은 0~100사이의 수만 입력 가능 합니다.");
   $('#special_percent').val('');
   $('#special_percent').focus();
   return false;
 }

 $.post("/preference/special_price_add/",
 {
  company_serial : company_serial,
  start_date: startDate,
  end_date : endDate,
  memo : $('#special_memo').val(),
  percent : $('#special_percent').val()

},
function(data, status){
                    // alert ("hi2");
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                      alert("등록되었습니다.");
                        //데이터 다시 읽어옴
                        // grid.refresh();
                        // alert(result.car_index);
                        // $('#company_serial').val(result.company_serial);
                        // $('#detail_car_index').val(result.car_index);

                        // alert($('#file_upload').submit());
                        // return false;

                        special_grid_refresh();

                        $("[data-dismiss=modal]").trigger({ type: "click" });
                        // $('.modal-backdrop').toggle();
                    }else if(result.code=="E02"){//이미 있는 경우
                      alert("이미 등록 된 기간 입니다.");
                    }else{
                      alert("데이터 입력 중 에러가 발생했습니다.");
                    }

                  });

});

        btn_special_price_update.click(function(){
// alert($('#start_date').val());

var company_serial = get_company_serial();

var grid = $("#special_price_grid").data("kendoGrid");
var row = grid.select();
var data = grid.dataItem(row);
if(data==null){
  alert("수정할 요금제를 선택해 주세요.");
  return false;
}

if($('#special_percent').val() < 0 ||  $('#special_percent').val() > 100){
 alert("적용률은 0~100사이의 수만 입력 가능 합니다.");
 $('#special_percent').val('');
 $('#special_percent').focus();
 return false;
}

if( $('#start_date').val() > $('#end_date').val()){
  alert("종료 날짜가 과거입니다.");
  $('#end_date').val('');
  $('#end_date').focus();
  return false;
}




    // alert(data.serial);
    $.post("/preference/special_price_update/",
    {
      serial : data.serial,
      company_serial : company_serial,
      start_date: startDate,
      end_date : endDate,
      memo : $('#special_memo').val(),
      percent : $('#special_percent').val()

    },
    function(data, status){
                            // alert ("hi2");
                            var result = JSON.parse(data);
                            if(result.code=="S01"){
                              alert("수정되었습니다.");


                              special_grid_refresh();

                              $("[data-dismiss=modal]").trigger({ type: "click" });
                                // $('.modal-backdrop').toggle();
                            }else if(result.code=="E02"){//이미 있는 경우
                              alert("이미 등록 된 기간 입니다.");
                            }else{
                              alert("데이터 입력 중 에러가 발생했습니다.");
                            }

                          });


  });

        function special_grid_refresh(){

          var company_serial = get_company_serial();

          var url = "/preference/get_special_price/" + company_serial;
          var dataSource = new kendo.data.DataSource({
           transport: {
            read: {
             url: url,
             dataType: "json"
           }
         },
         pageSize: 10,
         schema: {
          model: {
            fields: {
              start_date: { type: "date" },
              end_date: { type: "date" },
              percent: {type:"number"},
              memo: {type:"string"},
              user_name: { type: "string" }, 
              birthday: { type: "srting" }, 
              address: { type: "string" }, 
              phone_number1: { type: "string" },
              phone_number2: { type: "string" },
              corporation_name: { type: "string" }, 
              auth_code: { type: "number" }, 
              note: {type:"string"},
              license_type: { type: "string" }, 
              published_date: { type: "date" }, 
              license_number: { type: "string" }, 
              expiration_date: { type: "date" }, 
            }
          }
        }
      });
          var grid = $("#special_price_grid").data("kendoGrid");
          grid.setDataSource(dataSource);
        }

        $("#special_price_grid").delegate("tbody>tr", "dblclick", function(){

        //  if(<? echo $company_serial; ?> == 0){
        //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
        //   return false;
        // }

        var grid = $('#special_price_grid').data('kendoGrid'); 
        var row = grid.select();
        var data = grid.dataItem(row);
    // alert(data.start_date);
// alert(data.memo);
$('#special_price_save_btn').hide();
$('#special_price_update_btn').show();
      // alert($('#special_price_save_btn'));
      $('#start_date').datepicker().data('datepicker').selectDate(data.start_date);
      $('#end_date').datepicker().data('datepicker').selectDate(data.end_date);

      // $('#start_date').val(kendo.toString(data.start_date, "yyyy년 MM월 dd일"));
      // $('#end_date').val(kendo.toString(data.end_date, "yyyy년 MM월 dd일"));
      $('#special_memo').val(data.memo);
      $('#special_percent').val(data.percent);



      $('#certain_cash').modal('toggle');

    });


        // });
        $("#basic_price_add_btn").click(function(){

          // if(<? echo $company_serial; ?> == 0){
          //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
          //   return false;
          // }

          //유효성 검사
          if($('#input_6_hour_week').val() >100 || $('#input_6_hour_week').val()<0){
            alert("0~100 사이의 값만 입력 가능합니다.");
            $('#input_6_hour_week').val('');
            $('#input_6_hour_week').focus();
            return false;
          }
          if($('#input_6_hour_weekend').val() >100 || $('#input_6_hour_weekend').val()<0){
            alert("0~100 사이의 값만 입력 가능합니다.");
            $('#input_6_hour_weekend').val('');
            $('#input_6_hour_weekend').focus();
            return false;
          }
          if($('#input_1_hour_week').val() >100 || $('#input_1_hour_week').val()<0){
            alert("0~100 사이의 값만 입력 가능합니다.");
            $('#input_1_hour_week').val('');
            $('#input_1_hour_week').focus();
            return false;
          }
          if($('#input_1_hour_weekend').val() >100 || $('#input_1_hour_weekend').val()<0){
            alert("0~100 사이의 값만 입력 가능합니다.");
            $('#input_1_hour_weekend').val('');
            $('#input_1_hour_weekend').focus();
            return false;
          } 

          var company_serial = get_company_serial();

          $.post("/preference/basic_price_add/",
          {
            week_6 : $('#input_6_hour_week').val(),
            week_1 : $('#input_1_hour_week').val(),
            weekend_6 : $('#input_6_hour_weekend').val(),
            weekend_1 : $('#input_1_hour_weekend').val(),
            company_serial : company_serial,
          },
          function(data){
                    // alert ("hi2");
                    // alert(data);
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                      alert("등록되었습니다.");
                    }else{
                      alert(result.message);
                    }

                  });
        });



        function get_basic_price(){

          var company_serial = get_company_serial();

  // alert(branch_serial);

  $.get("/preference/get_basic_price/"+company_serial,
    function(data){
      // alert(data);
      var result = JSON.parse(data);
      if(result.length == 0){
        // alert('no');
        $("#input_6_hour_week").val('');
        $("#input_1_hour_week").val('');
        $("#input_6_hour_weekend").val('');
        $("#input_1_hour_weekend").val('');
      }else{
        $("#input_6_hour_week").val(result[0].week_6);
        $("#input_1_hour_week").val(result[0].week_1);
        $("#input_6_hour_weekend").val(result[0].weekend_6);
        $("#input_1_hour_weekend").val(result[0].weekend_1);
      }
      // var result = JSON.parse(data);
      // var text = "";
      // for(var i=0; i< result.length; i++){
      //   text += "<option value='"+ result[i].serial+"'>" + result[i].branch_name +"</option>";
      // }
      // $( "#car_branch_select").html(text);


    });
}


$('#longtime_cash, #certain_cash').draggable({
  handle: ".modal-header"
});

function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}

</script>