<head>
    <meta charset="utf-8">
    <title>RENGO SOLUTION</title>

    <script src="/js/jquery-1.9.1.min.js"></script>
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <script src="/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <script src="/js/bootstrap-toggle.min.js"></script>

    <link href="/css/material_design/bootstrap-material-design.css" rel="stylesheet">
    <link href="/css/material_design/ripples.min.css" rel="stylesheet">
    <script src="/js/material_design/ripples.min.js"></script>
    <script src="/js/material_design/material.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript" src="//apis.daum.net/maps/maps3.js?apikey=0f1dc0047640c3cf044e7a7af12f2869&libraries=services"></script>
    <script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>

    <link rel="stylesheet" href="/css/rengo_solution.css?1482387553">
    <link rel="stylesheet" href="/css/rengo_solution_menu.css">
    <link href="/css/rengo_login.css" rel="stylesheet">

    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    <link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
    <link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
    <link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
    <link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
    <link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
    <link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
    <link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">
    <link href="/css/rengo_login.css" rel="stylesheet">

    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

    <script src="/telerik/js/jszip.min.js"></script>
    <script src="/telerik/js/kendo.all.min.js"></script>
    <script src="/telerik/examples/content/shared/js/console.js"></script>

</head>

<?php
    require("/home/apache/CodeIgniter-3.0.6/application/views/preference/dialog_partner_setting.php"); //통장사본, 사업자 등록 사진 보기 위한 팝업
?>

<script type="text/javascript">
$(function () {
    $('#partner_time_st').datetimepicker({
        locale: 'ko',
        format: 'HH:mm',
        stepping: 30,
    });
});
</script>

<script type="text/javascript">
  $(function () {
    $('#partner_time_end').datetimepicker({
      locale: 'ko',
      format: 'HH:mm',
      stepping: 30,
  });
});
</script>
    <?
    if($error){
        echo $error;
    }

    ?>
<div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div method="get" class="form-horizontal">
                         <form method="post" action="/partnersetting/file_upload2" enctype="multipart/form-data" id="registermanager_form">
                            
                            <div class="form-group"><label class="col-sm-2 control-label">승인 여부</label>
                                <div class="col-sm-5">
                                    <select id='select_status' class="form-control">
                                        <option value='W'>승인 대기중</option>
                                        <option value='Y'>정상 사용중</option>
                                        <option value='N'>사용 중지</option>
                                    </select>   
                                </div>
                                <div class="col-sm-4">
                                    <button id="btn_send_message" class="btn btn-primary">승인 메일 보내기</button>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group"><label class="col-sm-2 control-label">아이디</label>

                                <div class="col-sm-5"><input type="text" class="form-control input-lg" id="email" placeholder="Email 형식의 ID를 입력해주세요." disabled></div>
                                <!-- <div class="col-sm-1 pdl0">
                                    <button id="email_confirm" class="bt" style="display:inline-block;">중복확인</button>
                                </div> -->
                                <div class="col-sm-3">
                                    <p id="email_dup_text" style="padding-top:13px;"></p>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">비밀번호</label>
                                <div class="col-sm-5">
                                    <a class="bt" id="change_password_btn" onclick="changePasswordBtn()" type="button" >변경하기</a>
                                    <input type="password" style="display:none;" class="form-control input-lg" id="password" placeholder="변경할 영문과 숫자 포함하여 6~20자">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">파트너(상호)명</label>

                                <div class="col-sm-5"><input type="text" class="form-control input-lg" id="company_name" placeholder="사업자등록상의 상호를 입력해주세요."></div>
                            </div>


                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">대표번호</label>
                                <div class="col-sm-5"><input type="text" class="form-control input-lg" id="representative_number" maxlength="20" placeholder="숫자만 입력해주세요. 예)15661234"></div>
                            </div>


                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" >팩스번호</label>

                                <div class="col-sm-5">
                                    <ul class="call_number">
                                        <li>
                                            <select class="form-control input-lg" id="ffirst_number">
                                                <option value="02">02</option>
                                                <option value="031">031</option>
                                                <option value="032">032</option>
                                                <option value="033">033</option>
                                                <option value="051">051</option>
                                                <option value="052">052</option>
                                                <option value="053">053</option>
                                                <option value="054">054</option>
                                                <option value="055">055</option>
                                                <option value="041">041</option>
                                                <option value="042">042</option>
                                                <option value="043">043</option>
                                                <option value="061">061</option>
                                                <option value="062">062</option>
                                                <option value="063">063</option>
                                                <option value="064">064</option>
                                            </select>
                                        </li>
                                        <li>
                                            <input type="text" class="form-control input-lg" maxlength="4" id="fmiddle_number">        
                                        </li>
                                        <li>
                                            <input type="text" class="form-control input-lg" maxlength="4" id="flast_number">
                                        </li>
                                    </ul>
                                </div>
                            </div>


                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">주소</label>

                                <div class="col-sm-2"><input type="text" class="form-control input-lg" id="postcode" maxlength="5" placeholder="우편번호를 입력하세요."></div>
                                <div class="col-sm-3 pdl0">
                                    <a role="button" class="bt" style="display:inline-block;" type="button" onclick="execDaumPostcode()" >우편번호 찾기 </a>
                                    <a id="" role="button" type="button" class="bt" style="margin-left:12px;">지도 보기</a>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-5 col-sm-offset-2">
                                    <input type="text" class="form-control input-lg" id="address" placeholder="상세주소를 입력해주세요.">
                                </div>
                            </div>



                           <!--  <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">영업시간</label>
                                <div class="col-sm-6">
                                    <ul style="list-style: none; margin: 0; padding: 0;">
                                        <li style="float:left; width: 160px; margin: 0; padding: 0; border-radius: 0;">
                                            <div class='input-group date' id='partner_time_st'>
                                                <input type='text' id="partner_time_st_input" class="form-control input-lg" maxlength="5" placeholder="개점 시간" >
                                                <span class="input-group-addon" style="border-radius: 0;">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                        </li>
                                        <li style="float:left; width: 30px; margin: 0; padding: 0; text-align: center; line-height: 46px;">
                                            ~
                                        </li>
                                        <li style="float:left; width: 160px; margin: 0; padding: 0; border-radius: 0;">
                                            <div class='input-group date' id='partner_time_end'>
                                                <input type='text' id="partner_time_end_input" class="form-control input-lg" maxlength="5" placeholder="폐점 시간">
                                                <span class="input-group-addon" style="border-radius: 0;">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>


                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">시간당 배차가능횟수</label>
                                <div class="col-sm-4">
                                    <li class="percent" style="width: 350px; list-style: none; position:relative;">
                                        <input type='text' id="delivery_max_count" class="form-control input-lg" placeholder="4" maxlength="5" style="padding-right: 23px; text-align: right;">
                                        <span style="top:13px; right: 10px; position: absolute;">건</span>
                                    </li>
                                </div>
                            </div> -->


                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">대표자명</label>
                                <div class="col-sm-4"><input type="text" class="form-control input-lg" id="representative" placeholder="홍길동"></div>
                            </div>


                            <div class="hr-line-dashed"></div>

                            <div class="form-group"><label class="col-sm-2 control-label" >대표자 연락처</label>

                                <div class="col-sm-5">
                                    <ul class="call_number">
                                        <li> 
                                            <select class="form-control input-lg" id="rfirst_number">
                                                <option value="010">010</option>
                                                <option value="011">011</option>
                                                <option value="016">016</option>
                                                <option value="017">017</option>
                                                <option value="019">019</option>
                                            </select>
                                        </li>
                                        <li>
                                            <input type="text" class="form-control input-lg" maxlength="4" id="rmiddle_number">        
                                        </li>
                                        <li>
                                            <input type="text" class="form-control input-lg" maxlength="4" id="rlast_number" >
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">법인등록번호</label>
                                    <div class="col-sm-5">
                                        <ul class="call_number">
                                        <li>
                                            <input type="text" class="form-control input-lg" maxlength="6" id="corporation_first_number">
                                        </li>
                                        <li>
                                            <input type="text" class="form-control input-lg" maxlength="7" id="corporation_last_number" >
                                        </li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <div class="hr-line-dashed"></div>
                                    <div class="form-group"><label class="col-sm-2 control-label">사업자등록번호</label>
                                        <div class="col-sm-5">
                                            <ul class="call_number">
                                                <li>
                                                    <input type="text" class="form-control input-lg" maxlength="3" id="cr_first_number">
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control input-lg" maxlength="2" id="cr_middle_number" >
                                                </li>
                                                <li>
                                                    <input type="text" class="form-control input-lg" maxlength="5" id="cr_last_number" >
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">사업자등록증 업로드</label>
                                        <div class="filebox">
                                            <div class="col-sm-5">
                                                <input class="form-control input-lg upload-name" id="" placeholder="본인명의의 사업자등록증을 첨부해주세요." disabled="disabled" value="">
                                                <input type="file" id="business_regist" name="business_regist" class="upload-hidden" >
                                            </div>
                                            <div class="col-sm-3 pdl0">
                                                <label class="bt" for="business_regist">첨부하기</label>
                                                <a id="business_show_btn" onclick="business_show()" type="button" class="bt" data-toggle="modal" data-target="#dialog_business_regist" style="margin-left:12px; margin-bottom: 5px;">보기</a>   
                                            </div>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">통장사본 업로드</label>
                                        <div class="filebox">
                                            <div class="col-sm-5">
                                                <input class="form-control input-lg upload-name" id="image_bank" placeholder="본인명의의 통장사본을 첨부해주세요. gif, jpg, jpeg, png화일만 가능" disabled value="">
                                                <input type="file" id="bankbook" name="bankbook" class="upload-hidden">
                                            </div>
                                            <div class="col-sm-3 pdl0">
                                                <label class="bt" for="bankbook">첨부하기</label>
                                                <a id="bank_show_btn" onclick="bank_show()" type="button" class="bt" data-toggle="modal" data-target="#dialog_bankbook" style="margin-left:12px; margin-bottom: 5px;">보기</a>   
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="hr-line-dashed"></div> -->
                                    <input type="hidden" id="company_serial" name="company_serial" value="">
                                </form>
                            </div>

                            <div class="modal-footer">
                                <button id="btn_register" type="button" class="btn btn-primary">저장</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    </html>

<script type="text/javascript" src="//apis.daum.net/maps/maps3.js?apikey=0f1dc0047640c3cf044e7a7af12f2869&libraries=services"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>

<script type="text/javascript">
var company_serial;
var EMAIL_FLAG, authcode_test, registernumber_test, PASSWORD_FLAG, PASSWORD_FORMAT_FLAG, dup_test;
//이하 객체 변수
var $registermanager_form;
var $company_serial;
var $select_status;
var $fileTarget;
var $email, $email_dup_text, $email_confirm, $btn_send_message;
var $password, $password_confirm ,$password_str, $password_check, $change_password_btn;
var $company_name;
var $representative_number;
var $ffirst_number, $fmiddle_number, $flast_number;
var $postcode, $address;
// var $start_time, $end_time;
// var $delivery_max_count;
var $representative;
var $rfirst_number, $rmiddle_number, $rlast_number;
var $corporation_first_number, $corporation_last_number;
var $cr_first_number, $cr_middle_number, $cr_last_number;
var $btn_register;
var $business_regist, $bankbook;
var image_bank;
$(document).ready(function(){

    initVariables();
    initInformation();
    passwordKeyup();
    passwordConfirmKeyup();
    fileTarget();
    btnSendMessage();
    var numcheckvars = [
        $representative_number,$fmiddle_number,$flast_number,$rmiddle_number,$rlast_number,
        $corporation_first_number,$corporation_last_number,$cr_first_number,$cr_middle_number,$cr_last_number
    ];
    // var timecheckvars = [$start_time,$end_time];
    for(var i=0;i<numcheckvars.length;++i){ numberKeyupCheck(numcheckvars[i]); }
    // for(var i=0;i<timecheckvars.length;++i) { timeBlurCheck(timecheckvars[i]); }

}); 


function initVariables() {
    company_serial = <?php echo $company_serial; ?>;
    EMAIL_FLAG=false; /*authcode_test=true; registernumber_test=true;*/ PASSWORD_FLAG=false; PASSWORD_FORMAT_FLAG=false;
    dup_test = [EMAIL_FLAG,authcode_test,registernumber_test];

    $registermanager_form=$('#registermanager_form');
    $company_serial=$('#company_serial');
    $select_status = $('#select_status');
    $fileTarget = $('.filebox .upload-hidden');
    $email=$('#email'); $email_dup_text=$('#email_dup_text'); $email_confirm=$('#email_confrim'); $btn_send_message =$('#btn_send_message');
    $password=$('#password'); $password_confirm=$('#password_confirm'); $password_str=$('#password_str'); $password_check=$('#password_check'); $change_password_btn =$('#change_password_btn');
    $company_name=$('#company_name');
    $representative_number=$('#representative_number');
    $ffirst_number=$('#ffirst_number'); $fmiddle_number=$('#fmiddle_number'); $flast_number=$('#flast_number');
    $postcode=$('#postcode'); $address=$('#address');
    // $start_time = $("#partner_time_st_input"); $end_time = $("#partner_time_end_input");
    // $delivery_max_count = $("#delivery_max_count");
    $representative = $('#representative');
    $rfirst_number=$('#rfirst_number'); $rmiddle_number=$('#rmiddle_number'); $rlast_number=$('#rlast_number');
    $corporation_first_number=$('#corporation_first_number'); $corporation_last_number=$('#corporation_last_number');
    $cr_first_number=$('#cr_first_number'); $cr_middle_number=$('#cr_middle_number'); $cr_last_number=$('#cr_last_number');
    $btn_register=$('#btn_register');
    $business_regist=$('#business_regist'); $bankbook=$('#bankbook'); $image_bank=$('#image_bank');
}

function initInformation() {
    $.get("/partnersetting/get_initial_info/"+company_serial,
        function(data){
            var result = JSON.parse(data);
            if(result==''){
                return false;
            }

            var status = result[0].flag;
            var fax=result[0].fax.split('-');
            // var openTime = result[0].open_time_start == null ? 'asdf' : result[0].open_time_start;
            // var endTime = result[0].open_time_finish == null ? 'asdf' : result[0].open_time_finish;
            var phone = result[0].phone;

            var corporation_number = result[0].corporate_license_number.split('-');
            var cr_number = result[0].register_number.split('-');

            if(status == 'W'){
                $btn_send_message.show();
            }else{
                $btn_send_message.hide();
            }

            if(phone != null){
                var phone1 = phone.substring(0,3);
                var phone2 = phone.substring(3, phone.length-4);
                var phone3 = phone.substring(phone.length-4, phone.length);
            }else {
                var phone1, phone2, phone3 = '';                
            }

            $select_status.val(status).prop("selected", true);
            $email.val(result[0].email);
            //password
            $company_name.val(result[0].company_name);
            $representative_number.val(result[0].tel);

            $ffirst_number.val(fax[0]).prop('selected',true);
            $fmiddle_number.val(fax[1]);
            $flast_number.val(fax[2]);
            $postcode.val(result[0].postcode);
            $address.val(result[0].company_address);
            // $start_time.val(openTime.substring(0,2)+":"+openTime.substring(2,4));
            // $end_time.val(endTime.substring(0,2)+":"+endTime.substring(2,4));
            // $delivery_max_count.val(result[0].delivery_max_per_hour);
            $representative.val(result[0].owner_name);
            $rfirst_number.val(phone1).prop('selected',true);
            $rmiddle_number.val(phone2);
            $rlast_number.val(phone3);
            $corporation_first_number.val(corporation_number[0]);
            $corporation_last_number.val(corporation_number[1]);
            $cr_first_number.val(cr_number[0]);
            $cr_middle_number.val(cr_number[1]);
            $cr_last_number.val(cr_number[2]);
        });       
}

function fileTarget(){
    $fileTarget.on('change', function(){
        if(window.FileReader){
            var filename = $(this)[0].files[0].name;
        } else {
            var filename = $(this).val().split('/').pop().split('\\').pop();
        }
        $(this).siblings('.upload-name').val(filename);
    });
}

function execDaumPostcode() {
    new daum.Postcode({
        oncomplete: function(data) {

            var fullAddr = ''; 
            var extraAddr = ''; 

            if (data.userSelectedType === 'R') {
                fullAddr = data.roadAddress;
            } else { 
                fullAddr = data.jibunAddress;
            }

            if(data.userSelectedType === 'R'){
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }

                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            document.getElementById('postcode').value = data.zonecode; 
            document.getElementById('address').value = fullAddr;

            document.getElementById('address').focus();
        }
    }).open();
}

// function isDup(value,column) {
//     if(column == 'email'){
//         if(emailcheck(value)==false){
//             $email_dup_text.html('이메일 형식이 아닙니다.');
//             $email.focus();
//             return false;
//         }
//     }
//     $.post('/registermanager/duplicate_test/',
//     {
//         value : value,
//         column : column
//     },
//     function(data,status){
//         var result = JSON.parse(data);
//         if(result=='null') {
//             if(column=='email') {
//                 EMAIL_FLAG=true;
//                 $email_dup_text.html('사용 할 수 있는 ID입니다.');
//             } 
//         } 
//         else {
//                 if(column=='email') { $email_dup_text.html('사용할 수 없는 ID입니다.'); EMAIL_FLAG=false; } 
//                 // if(column=='auth_code') { $('#authcode_dup_text').html('중복된 번호입니다.'); authcode_test=false;} 
//                 // if(column=='register_number') { $('#registernumber_dup_text').html('중복된 번호입니다.'); registernumber_test=false;} 
//             }
//     })
// }

function passwordKeyup(){
    $password.keyup(function() {
        if($password.val() == ''){
            $password_str.html('');
            return false;
        }
         if(checkPassword($password.val())==false){
             $password_str.html('비밀번호가 형식에 맞지 않습니다.');
             PASSWORD_FORMAT_FLAG=false;
             return false;
        }else{
            $password_str.html('안전한 비밀번호 입니다.');
            PASSWORD_FORMAT_FLAG=true;
            return true;
        }
    })
}

function passwordConfirmKeyup(){
    $password_confirm.keyup(function() {
        if($password_confirm.val() == ''){
            $password_check.html('');
            return false;
        }

        if($password.val() == $(this).val() && PASSWORD_FORMAT_FLAG){
            $password_check.html('비밀번호가 일치합니다.');
            PASSWORD_FLAG=true;
        } else if($password.val() != $(this).val() && PASSWORD_FORMAT_FLAG){
            $password_check.html('비밀번호가 일치하지 않습니다.');
            PASSWORD_FLAG=false;
        } else if($password.val() != $(this).val() && !PASSWORD_FORMAT_FLAG){
            $password_check.val('');
            PASSWORD_FLAG=false;
        }
    })
}

function changePasswordBtn() {
    $change_password_btn.hide();
    $password.show();
}

function btnSendMessage() {
    $btn_send_message.click(function() {
        if(company_serial!=''){
            $.post("/partnermanager/send_confirm_mail",
                {
                    partner_serial : company_serial
                },
                function(data)
                {
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                        alert("메일을 보냈습니다.");
                    } else {
                        alert(result.message);
                    }
                }); 
        }else{
            alert('파트너 번호 없음');
            return false;
        }
    });    
}

function business_show() {
    var url = "/partnersetting/get_image/business/"+<?=$company_serial?>;
    $('#div_business').html("<img id='business_img' src='" + url + "' onError='this.onerror=null;default_image();' width='400' height='400'>");    
}

function bank_show() {
    var url = "/partnersetting/get_image/bank/"+<?=$company_serial?>;
    $('#div_bank').html("<img id='bankbook_img' src='" + url + "' onError='this.onerror=null;default_image();' width='400' height='400'>");    
}

function default_image() {
    $('#business_img').attr('src','/partnersetting/get_image/business/1872');
    $('#bankbook_img').attr('src','/partnersetting/get_image/business/1872');
}

// function btnRegister() {
    $('#btn_register').click(function(){

        var password = $password.val();
        var FLAG=true;
        // var st_token = $start_time.val().split(':');
        // var st_str = st_token[0] + st_token[1];
        // var end_token = $end_time.val().split(':');
        // var end_str = end_token[0] + end_token[1];

        if($password.is(":visible") && $password.val()==''){
            alert("변경할 비밀번호를 입력해 주세요.");
            $password.focus();
            return false;
        } else if($password.is(":visible") && $password.val() != ''){
            if(checkPassword(password)==false){
                alert('비밀번호가 형식에 맞지 않습니다.');
                $password.focus();
                return false;
            }
        } else if($password.is(':visible')==false){
            password='';
        } 

        if($company_name.val().length==0) {
            alert('파트너 상호명을 입력해주세요.');
            $company_name.focus();
            FLAG=false;
            return false;
        }

        var numcheckvars = [
            $representative_number,$fmiddle_number,$flast_number,$rmiddle_number,$rlast_number,
            $corporation_first_number,$corporation_last_number,$cr_first_number,$cr_middle_number,$cr_last_number
        ];

        for(var i=0; i<numcheckvars.length; ++i){
            if(numbercheck(numcheckvars[i].val())==false){
                alert('입력 형식을 확인해주세요.');
                numcheckvars[i].val('');
                numcheckvars[i].focus();
                FLAG=false;
                return false;
            }
        }
        
        // if(timecheck($start_time.val())==false){
        //     // alert('영업시간이 형식에 맞지 않습니다.');
        //     alert('입력 형식을 확인해주세요.');
        //     $start_time.val('');
        //     $start_time.focus();
        //     FLAG=false;
        //     return false;
        // }

        // if(timecheck($end_time.val())==false){
        //     // alert('영업시간이 형식에 맞지 않습니다.');
        //     alert('입력 형식을 확인해주세요.');
        //     $end_time.val('');
        //     $end_time.focus();
        //     FLAG=false;
        //     return false;
        // }

        if( $business_regist.val() != "" ){
            var ext = $business_regist.val().split('.').pop().toLowerCase();
            if($.inArray(ext, ['gif','png','jpg','jpeg','pdf']) == -1) {
                alert('gif,png,jpg,jpeg,pdf 파일만 업로드 할수 있습니다.');
                FLAG=false;
                return false;
            }
        } 
    
        if( $bankbook.val() != "" ){
            var ext = $bankbook.val().split('.').pop().toLowerCase();
            if($.inArray(ext, ['gif','png','jpg','jpeg','pdf']) == -1) {
                alert('gif,png,jpg,jpeg,pdf 파일만 업로드 할수 있습니다.');
                FLAG=false;
                return false;
            }
        } 
        if(FLAG) {
            $.post('/partnersetting/partner_info_update/',
            {
                company_serial : company_serial,
                select_status : $select_status.filter(':selected').val(),
                email : $email.val(),
                password : password,
                company_name : $company_name.val(),
                representative_number : $representative_number.val(),
                fax : $ffirst_number.filter(':selected').val() +'-'+ $fmiddle_number.val() +'-'+ $flast_number.val(),
                postcode : $postcode.val(),
                address : $address.val(),
                // start_time : st_str,
                // end_time : end_str,
                // delivery_max_count : $delivery_max_count.val(),
                representative : $representative.val(),
                representative_tel : $rfirst_number.filter(':selected').val() + $rmiddle_number.val() + $rlast_number.val(),
                corporation_number : $corporation_first_number.val()+'-'+$corporation_last_number.val(),
                cr_number : $cr_first_number.val() + '-' + $cr_middle_number.val() + '-' + $cr_last_number.val(),
            },
            function(data){
                var result = JSON.parse(data);
                if(result.code=="S01"){
                    $company_serial.val(company_serial);
                    alert("수정되었습니다.");
                    $registermanager_form.submit();
                } else {
                    alert(result.message);
                }
            });
        }
    })    
// }

function checkPassword(upw)
{
    if(!/^[a-zA-Z0-9]{6,20}$/.test(upw)) return false;

    var chk_num = upw.search(/[0-9]/g); 
    var chk_eng = upw.search(/[a-z]/ig);

    if(chk_num < 0 || chk_eng < 0) return false;
    return true;
}

function emailcheck(strValue)
{
    var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
    if(strValue.length == 0 || !strValue.match(regExp))
        {return false;}
    return true;
}

function numbercheck(value) {
    var numExp = /^[0-9]*$/;
    if(value.length==0 || !numExp.test(value)) 
        return false;
    return true;
}

function numberKeyupCheck($obj){
    $obj.keyup(function(){
        var numExp = /^[0-9]*$/;
        var objtext=$obj.val();
        if(!numExp.test(objtext)){
            $obj.val(objtext.slice(0,objtext.length-1));
        }
    })
}

function timecheck(value) {
    var timeExp=/^[0-9]{2}:[0-9]{2}$/;
    if(value.length==0 || !timeExp.test(value)) 
        return false;
    return true;
}

function timeBlurCheck($obj){
    $obj.blur(function(){
    var timeExp=/^[0-9]{2}:[0-9]{2}$/;
        var objtext=$obj.val();
        if(!timeExp.test(objtext)){
            $obj.val('');
        }
    })   
}
</script>