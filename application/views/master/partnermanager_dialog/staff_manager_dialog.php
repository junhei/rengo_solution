<head>
	<meta charset="utf-8">
	<title>RENGO SOLUTION</title>

	<script src="/js/jquery-1.9.1.min.js"></script>
	<link href="/css/font-awesome.min.css" rel="stylesheet">
	<script src="/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<script src="/js/bootstrap-toggle.min.js"></script>

	<!-- Bootstrap Material Design -->
	<link href="/css/material_design/bootstrap-material-design.css" rel="stylesheet">
	<link href="/css/material_design/ripples.min.css" rel="stylesheet">
	<script src="/js/material_design/ripples.min.js"></script>
	<script src="/js/material_design/material.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

	<link rel="stylesheet" href="/css/rengo_solution.css?1482387553">
	<link rel="stylesheet" href="/css/rengo_solution_menu.css">
	<link href="/css/rengo_login.css" rel="stylesheet">

  <link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>

	<link href="/css/datepicker.min.css?<?=time();?>" rel="stylesheet" type="text/css">
	<script src="/js/datepicker.js?<?=time();?>"></script>
	<script src="/js/i18n_datepicker.en.js"></script>
</head>

<div class="row mgr0 mgl0">
  <div class="col-md-12">
    <div class="panel border_gray">
<!--       <div class="panel-heading bb_ccc">
        <div class="row">
          <div class="col-md-2">
            <h3 class="mgt20 mgb10">직원 관리</h3>
          </div>
        </div>
      </div> -->
      <div class="panel-footer bg_fff">
        <div class="panel-body">
          <div class="panel panel-define magb40">
            <div class="panel-heading bg_mint">
              <div class="row">
                <h4 class="pull-left pdl15"><i class="fa fa-cog"></i> 직원관리 리스트</h4>
                <div class="pull-right mgr15">
                  <button id="delete_btn" type="button" class="btn btn-danger">삭제</button>
                  <button id="add_btn" type="button" class="btn btn-default" data-toggle="modal" data-target="#branch_office">추가</button>
                </div>
              </div>
            </div>

          <div class="row">
            <div class="col-sm-12">
                <div id="grid"> </div>                
              </div>
            </div>
          </div>
        </div>



      </div>          
    </div>              
  </div>
</div>


<!-- Modal 지역정보 등록-->
<div class="modal fade" id="branch_office" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button id="" type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="" class="modal-title">직원관리 추가</h4>
      </div>

      <div class="modal-body">
        <div class="row">

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">아이디</label>
                <div class="col-sm-10 ">
                  <input type="" class="form-control" value="" placeholder="" id="admin_id">
                </div>
              </div>
            </form>
          </div>

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">비밀번호</label>
                <div class="col-sm-10 ">
                   <a class="bt" id="change_password_btn" type="button" >변경하기</a>
                    <input type="password" style="display:none;" class="form-control" id="admin_pw" placeholder="변경할 영문과 숫자 포함하여 6~20자">
                </div>
              </div>
            </form>
          </div>

          <!-- <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">지점명</label>
                <div class="col-sm-10 ">
                   <select class="form-control" id="branch_select">
                            <?php foreach($branch_list as $branch):?>
                                <option value="<?php echo $branch['serial']; ?>"> <?php echo $branch['branch_name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                </div>
              </div>
            </form>
          </div> -->

          <div >
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">직원명</label>
                <div class="col-sm-10">
                  <input type="" class="form-control" value="" placeholder="예) 홍길동" id="admin_name">
                </div>
              </div>
            </form>
          </div>

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">휴대폰번호</label>
                <div class="col-sm-10 ">
                  <input type="" class="form-control" value="" placeholder="" id="phone_number">
                </div>
              </div>
            </form>
          </div>

          <div class="pdt15">
            <form class="form-horizontal">
              <div class="form-group magb0">
                <label for="" class="col-sm-2 control-label text-left pdt0">접근권한</label>
                <div class="col-sm-10">
                  <div class="panel panel-default magb0">
                    <div class="panel-body vehicle_info">
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_rent"><span class="checkbox-material"><span class="check"></span></span> 대여관리</label></div>
                      </div>
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_car"><span class="checkbox-material"><span class="check"></span></span> 차량관리</label></div>
                      </div>
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_customer"><span class="checkbox-material"><span class="check"></span></span> 고객관리</label></div>
                      </div>
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_sales"><span class="checkbox-material"><span class="check"></span></span> 매출관리</label></div>
                      </div>
<!--                       <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_setting"><span class="checkbox-material"><span class="check"></span></span> 환경설정</label></div>
                      </div> -->
                      <!-- <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_master"><span class="checkbox-material"><span class="check"></span></span> 마스터 설정</label></div>
                      
                    
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>


        </div>
      </div>

      <div class="modal-footer">
        <button id="save_btn" type="button" class="btn btn-primary">저장</button>
        <button id="update_btn" type="button" class="btn btn-primary" style="display:none;">수정</button>

      </div>
    </div>
  </div>
</div>

<script> 


            $("#grid").kendoGrid({
                navigatable: true,  //키보드로 표 조작 할수 있게
                selectable: true, //선택할수 있도록
                allowCopy: true, //값 copyrksm
                height: 600, //높이????
                sortable: true, //정렬가능하도록
                filterable: true, //필터(비교해서 정렬)가능하도록
                groupable: false, //그룹으로 정렬 가능
                excel: {
                    allPages: true,
                    fileName: "고객리스트.xlsx",
                    filterable: true
                },
                pageable: {
                    input: true,
                    messages: {
                        display: "Showing {0}-{1} from {2} data items",
                        empty: "No data"
                    }
                  },
                noRecords: {
                    template: "현재 페이지에서 보여줄 내용이 없습니다."
                  },
                columnMenu: {
                    sortable: false,
                    messages: {
                        columns: "표시할 항목 선택",
                        filter: "필터",
                    }
                 },
                dataSource: {
                    transport: {
                         read: {
                           url: "/staffmanager/get_list/"+<?=$company_serial?>+"/0",
                           dataType: "json"
                         }
                       },
                    schema: {
                        model: {
                            fields: {
                                admin_id: { type: "string" },
                                admin_pw: { type: "string" },
                                admin_name: {type:"string"},
                                permission: {type:"string"},
                                phone_number: { type: "number" }, 
                                // branch_serial: { type: "number" }, 
                                serial: { type: "number" },
                                check_rent: { type: "string" },
                                check_car: { type: "string" },
                                check_customer: { type: "string" }, 
                                check_sales: { type: "string" }, 
                                // check_setting: {type:"string"},
                                // check_master: { type: "string" } 
                                // published_date: { type: "date" }, 
                                // license_number: { type: "string" }, 
                                // expiration_date: { type: "date" }, 
                                // business_office: { type: "string" }, 
                                // point: { type: "number" }, 
                                // number_of_use: { type: "number" }, 
                                // recently_use_date: { type: "date" }, 
                                // email: { type: "string" }, 
                                // reception_message: { type: "string" }, 
                                // reception_email: { type: "string" }, 
                                // charge_date: { type: "date" }, 
                                // surtax: { type: "string" },                                        
                                // note_detail: { type: "string" }, 
                                // registrant: { type: "string" }, 
                                // registration_date: { type: "date" }, 
                                // modifier: { type: "string" }, 
                                // modification_date: { type: "date" }
                            }
                        }
                    },
                    pageSize: 30
                },
               
                columns: [
             {
                  field: "admin_id",
                  title: "아이디",
                  width: 200
             },
              {
                  field: "admin_name",
                  title: "이름",
                  width: 200
              },{
                  field: "phone_number",
                  title: "전화번호",
                  width: 200
              },
                {
                  field: "check_rent",
                  title: "대여관리",
                  width: 100
                },{
                  field: "check_car",
                  title: "차량관리",
                  width: 100
                },{
                  field: "check_customer",
                  title: "고객관리",
                  width: 100
                },{
                  field: "check_sales",
                  title: "매출관리",
                  width: 100
                },
               //  {
               //    field: "check_setting",
               //    title: "환경설정",
               //    width: 100
                // }
               //  ,{
               //    field: "check_master",
               //    title: "마스터관리",
               //    width: 200
                // }
          ]
            });
        var grid = $("#grid").data("kendoGrid");
        var btn_save = $("#save_btn");
        var btn_update = $("#update_btn");
        var btn_delete = $("#delete_btn");
        var btn_add = $("#add_btn");

        btn_add.click(function(){

            $("#change_password_btn").hide();
            $("#admin_pw").show();

            $('#save_btn').show();
            $('#update_btn').hide();

            $("#admin_id").val('');
            $('#admin_id').prop('disabled', false);
            $("#admin_pw").val('');
            $('#admin_pw').prop('disabled', false);
            $("#admin_name").val('');
            $("#phone_number").val('');
            // $("#branch_select option:eq(0)").prop("selected", true);
            $('#check_rent').prop("checked", true);
            $('#check_car').prop("checked", true);
            $('#check_customer').prop("checked", true);
            $('#check_sales').prop("checked", true);
            // $('#check_setting').prop("checked", true);
        });
        
        btn_delete.click(function() {
            var grid = $("#grid").data("kendoGrid");
            var row = grid.select();
            var data = grid.dataItem(row);
            if(data==null){
                alert("삭제할 제조사를 선택해 주세요.");
                return false;
            }
// alert(data.serial);

            $.post("/staffmanager/delete",
            {
                    serial : data.serial //수정사항 10/3일.
            },
            function(data, status){
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                        alert("삭제되었습니다.");
                        //데이터 다시 읽어옴
                        refresh_grid();
                    }else{
                        alert("데이터 삭제 중 에러가 발생했습니다.");
                    }

            });
        });

       btn_save.click(function(){
// alert($('#phone_number').val());


          var company_serial = <?php echo $company_serial; ?>;

          if($("#admin_id").val()==''){
              alert("ID를 입력해 주세요.(email 형식)");
              $("#admin_id").focus();
              return false;
          }


          if($("#admin_pw").val()==''){
                alert("비밀번호를 입력해 주세요.");
                $("#admin_pw").focus();
                return false;
          }

          if($("#admin_name").val()==''){
                alert("이름을 입력해 주세요.");
                $("#admin_name").focus();
                return false;
          }
        
                $.post("/staffmanager/add",
                {
                    admin_id: $('#admin_id').val() ,
                    admin_pw: $('#admin_pw').val() ,
                    admin_name : $('#admin_name').val(),
                    company_serial : company_serial,
                    phone_number : $('#phone_number').val(),
                    check_rent: ($('#check_rent').is(":checked")) ? "Y" : "N" ,
                    check_car: ($('#check_car').is(":checked")) ? "Y" : "N" ,
                    check_sales: ($('#check_sales').is(":checked")) ? "Y" : "N" ,
                    check_customer: ($('#check_customer').is(":checked")) ? "Y" : "N" ,
                    // check_setting: ($('#check_setting').is(":checked")) ? "Y" : "N" ,
                    // check_master: ($('#check_master').is(":checked")) ? "Y" : "N" ,
                    // branch_select : $("#car_branch_select option:selected").val()
                },
                function(data, status){
                    // alert ("hi2");
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                        alert("등록되었습니다.");
                        //데이터 다시 읽어옴
                        // alert(result.car_index);
                        // $('#company_serial').val(result.company_serial);
                        // $('#detail_car_index').val(result.car_index);
                    
                        // alert($('#file_upload').submit());
                        // return false;

                        refresh_grid();

                        $("[data-dismiss=modal]").trigger({ type: "click" });
                        // $('.modal-backdrop').toggle();
                    }else if(result.code=="E02"){//이미 있는 경우
                        alert(result.message);
                    }else{
                        alert("데이터 입력 중 에러가 발생했습니다.");
                    }

                });

        });


       btn_update.click(function(){
            
            var grid = $("#grid").data("kendoGrid");
            var row = grid.select();
            var data = grid.dataItem(row);
                           // alert(data.serial);

                    // $("#branch_select option:selected").val();


            if($("#admin_id").val()==''){
              alert("ID를 입력해 주세요.(email 형식)");
              $("#admin_id").focus();
              return false;
             }

              if($("#admin_pw").is(":visible") && $("#admin_pw").val()==''){
                  alert("변경할 비밀번호를 입력해 주세요.");
                  $("#admin_pw").focus();
                  return false;
              }else if($("#admin_pw").is(":visible") == false){
                  var password = '';
              }else{
                  var password = $('#admin_pw').val();
              }
      

          if(data.serial < '1'){
            alert('아이디를 선택해주세요.');
            return false;
          }
                $.post("/staffmanager/update/",
                {
                    // admin_id: $('#admin_id').val() ,
                    admin_pw : $('#admin_pw').val() ,
                    admin_name : $('#admin_name').val(),
                    admin_pw: password,
                    serial : data.serial,
                    phone_number : $('#phone_number').val(),
                    check_rent: ($('#check_rent').is(":checked")) ? "Y" : "N" ,
                    check_car: ($('#check_car').is(":checked")) ? "Y" : "N" ,
                    check_sales: ($('#check_sales').is(":checked")) ? "Y" : "N" ,
                    check_customer: ($('#check_customer').is(":checked")) ? "Y" : "N" ,
                    // check_setting: ($('#check_setting').is(":checked")) ? "Y" : "N" ,
                    // check_master: ($('#check_master').is(":checked")) ? "Y" : "N" ,
                    // branch_select : $("#branch_select option:selected").val()
                    
           
                },
                function(data, status){
                    // alert ("hi2");
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                        alert("수정되었습니다.");
                        //데이터 다시 읽어옴
                        // grid.refresh();
                        // alert(result.car_index);
                        // $('#company_serial').val(result.company_serial);
                        // $('#detail_car_index').val(result.car_index);
                    
                        // alert($('#file_upload').submit());
                        // return false;

                        refresh_grid();

                        $("[data-dismiss=modal]").trigger({ type: "click" });
                        // $('.modal-backdrop').toggle();
                    }else if(result.code=="E02"){//이미 있는 경우
                        alert("이미 등록 된 아이디 입니다.");
                    }else{
                        alert("데이터 입력 중 에러가 발생했습니다.");
                    }

                });

        });


//         function basic_grid_refresh(){
//             var dataSource = new kendo.data.DataSource({
//          transport: {
//           read: {
//            url: "/staffmanager/get_list/",
//            dataType: "json"
//            }
//         },
//         pageSize: 10
//     });

//     var grid = $("#grid").data("kendoGrid");
//     grid.setDataSource(dataSource);
// }

$("#grid").delegate("tbody>tr", "dblclick", function(){

    $("#change_password_btn").show();
    $("#admin_pw").val('');
    $("#admin_pw").hide();
   
    var grid = $('#grid').data('kendoGrid'); 
    var row = grid.select();
    var data = grid.dataItem(row);

  $('#save_btn').hide();
  $('#update_btn').show();
  
if(data.check_rent == "O"){
  $check_rent = "checked"
}else{
  $check_rent = ""
}

if(data.check_car == "O"){
  $check_car = "checked"
}else{
  $check_car= ""
}

if(data.check_sales == "O"){
  $check_sales = "checked"
}else{
  $check_sales = ""
}

if(data.check_customer == "O"){
  $check_customer = "checked"
}else{
  $check_customer = ""
}

// if(data.check_setting == "O"){
//   $check_setting = "checked"
// }else{
//   $check_setting = ""
// }

// if(data.check_master == "O"){
//   $check_master = "checked"
// }else{
//   $check_master = ""
// }


                    $('#admin_id').val(data.admin_id);
                    $('#admin_id').prop('disabled', true);
                    $('#admin_pw').val('');
//                    $('#admin_pw').prop('disabled', true);
                    $('#admin_name').val(data.admin_name);
                    $('#phone_number').val(data.phone_number);
                    // $('#serial').val(data.serial);
                    $("input:checkbox[id='check_rent']").prop($check_rent, true);
                    $("input:checkbox[id='check_car']").prop($check_car, true);
                    $("input:checkbox[id='check_sales']").prop($check_sales, true);
                    $("input:checkbox[id='check_customer']").prop($check_customer, true);
                    // $("input:checkbox[id='check_setting']").prop($check_setting, true);

 
    $('#branch_office').modal('toggle');

    });

function refresh_grid(){


    var company_serial = <?php echo $company_serial; ?>;
    var dataSource = new kendo.data.DataSource({
         transport: {
          read: {
           url: "/staffmanager/get_list/" + company_serial + '/0' ,
           dataType: "json"
           }
        },
        pageSize: 30
    });
    var grid = $("#grid").data("kendoGrid");
    
    grid.setDataSource(dataSource);
    // grid.refresh();
}

function windowResize() {
    var window_height = $( window ).height() - 60;
    $('#page-wrapper').height(window_height);
}

$(window).resize(function() {
    windowResize();
});

$(document).ready(function() {
  windowResize();
})

$("#change_password_btn").click(function(){
    $("#change_password_btn").hide();
    $("#admin_pw").show();
});

    </script>