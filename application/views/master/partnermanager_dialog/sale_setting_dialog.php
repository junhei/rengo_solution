
<head>
  <meta charset="utf-8">
  <title>RENGO SOLUTION</title>

  <link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">

  <script src="/js/jquery-1.9.1.min.js"></script>
  <link href="/css/font-awesome.min.css" rel="stylesheet">
  <script src="/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <script src="/js/bootstrap-toggle.min.js"></script>

  <!-- Bootstrap Material Design -->
  <link href="/css/material_design/bootstrap-material-design.css" rel="stylesheet">
  <link href="/css/material_design/ripples.min.css" rel="stylesheet">
  <script src="/js/material_design/ripples.min.js"></script>
  <script src="/js/material_design/material.min.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

  <link rel="stylesheet" href="/css/rengo_solution.css?1482387553">
  <link rel="stylesheet" href="/css/rengo_solution_menu.css">
  <link href="/css/rengo_login.css" rel="stylesheet">

  <script src="/telerik/js/jszip.min.js"></script>
  <script src="/telerik/js/kendo.all.min.js"></script>
  <script src="/telerik/js/cultures/kendo.culture.ko-KR.min.js"></script>

  <link href="/css/datepicker.min.css?<?=time();?>" rel="stylesheet" type="text/css">
  <script src="/js/datepicker.js?<?=time();?>"></script>
  <script src="/js/i18n_datepicker.en.js"></script>
</head>
<style type="text/css">
  .row {
    margin:0;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <div class="panel border_gray">
      <div class="panel-heading bb_ccc">
        <div class="row">
          <div class="col-md-9">
            <h3 class="mgt20 mgb10">판매설정</h3>
          </div>
         </div>
       </div>

       <div class="panel-body">
            <div class="panel panel-define magb40">
                <div class="panel-heading well bg_fff bb_ccc" style="margin-bottom:0;">
                    <div class="row bb_ccc" style="padding-bottom:19px;">
                        <h5 class="pdl15 pull-left">기타설정</h5>
                        <div class="pull-right mgr15">
                            <button id="" type="button" class="btn action_btn" >저장</button>
                        </div>
                    </div>

                    <div class="row" style="padding-top:35px;">
                        <div class="col-md-6">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label text-left pdl0 pdr0">영업시간</label>
                                    <div class="col-sm-10">
                                        <ul style="list-style: none; margin: 0; padding: 0;">
                                            <li style="float:left; width: 160px; margin: 0; padding: 0; border-radius: 0;">
                                                <div class='input-group date' id='partner_time_st'>
                                                    <input type='text' id="partner_time_st_input" class="form-control input-lg" maxlength="5" placeholder="개점 시간" >
                                                    <span class="input-group-addon" style="border-radius: 0;">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </li>
                                            <li style="float:left; width: 30px; margin: 0; padding: 0; text-align: center; line-height: 46px;">
                                                ~
                                            </li>
                                            <li style="float:left; width: 160px; margin: 0; padding: 0; border-radius: 0;">
                                                <div class='input-group date' id='partner_time_end'>
                                                    <input type='text' id="partner_time_end_input" class="form-control input-lg" maxlength="5" placeholder="폐점 시간">
                                                    <span class="input-group-addon" style="border-radius: 0;">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-3">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="" class="col-sm-6 control-label text-left pdl0 pdr0">시간당 배차가능 횟수</label>
                                    <div class="col-sm-6">
                                        <select class="form-control">
                                            <option>1건</option>
                                            <option>2건</option>
                                            <option>3건</option>
                                            <option>4건</option>
                                            <option>5건</option>
                                            <option>6건</option>
                                            <option>7건</option>
                                            <option>8건</option>
                                            <option>9건</option>
                                            <option>10건</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-3">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="" class="col-sm-6 control-label text-left pdl0 pdr0">예약가능기간설정</label>
                                    <div class="col-sm-6">
                                        <select class="form-control">
                                            <option>1건</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      <div class="panel-footer bg_fff">
        <div class="panel-body">
          <div class="panel panel-define magb40">
            <div class="panel-heading well bg_fff bb0 magb0" style="">
              <div class="row">

                <h5 class="pull-left pdl15">기본요금설정</h5>
                <div class="pull-right mgr15">
                  <button id="basic_price_add_btn" type="button" class="btn action_btn" >저장</button>
                </div>
              </div>
            </div>

            <div class="row mgr0 magl0 bg_fff well" style="padding:0;border-radius:0; ">

              <div class="col-md-12 well" style="padding:0;border-top:none; border-right:0; border-left: 0;">
                <form class="form-horizontal">
                  <div class="form-group" style="margin-bottom:0; border-radius: 0;">

                    <div class="col-sm-1"></div>
                    <div class="col-sm-11">
                      <div class="col-sm-6 br_ccc">
                        <label for="" class="col-sm-12 basic_price_font" >주중 대여료</label>
                      </div>

                      <div class="col-sm-6">
                        <label for="" class="col-sm-12 basic_price_font">주말 대여료</label>
                      </div>
                    </div>
                  </div>
                </form>
              </div>

              <div class="col-md-12">
                <form class="form-horizontal">

                  <div class="form-group">
                    <label for="" class="col-sm-1 control-label text-left pdl0 insurance_txt">24시간</label>

                    <div class="col-sm-11">
                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" value="100" disabled>
                        <span>%</span>
                      </div>
                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" value="100" disabled>
                        <span>%</span>
                      </div>
                    </div>
                  </div>

                </form>
              </div>

              <div class="col-md-12">
                <form class="form-horizontal">

                  <div class="form-group">
                    <label for="" class="col-sm-1 control-label text-left pdl0 insurance_txt">6시간</label>

                    <div class="col-sm-11">
                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" id="input_6_hour_week">
                        <span>%</span>
                      </div>

                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" id="input_6_hour_weekend">
                        <span>%</span>
                      </div>
                    </div>
                  </div>
                </form>
              </div>

              <div class="col-md-12">
                <form class="form-horizontal">

                  <div class="form-group">
                    <label for="" class="col-sm-1 control-label text-left pdl0 insurance_txt">1시간</label>

                    <div class="col-sm-11">
                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" id="input_1_hour_week">
                        <span>%</span>
                      </div>

                      <div class="col-sm-6 percent">
                        <input type="number" class="form-control" id="input_1_hour_weekend">
                        <span>%</span>
                      </div>
                    </div>
                  </div>

                </form>
              </div>

            </div>
          </div>
        </div>

        <div class="panel-body">
          <div class="panel panel-define magb40">
            <div class="panel-heading well bg_fff bb0" style="margin-bottom:0;">
              <div class="row">
                <h5 class="pdl15 pull-left">장기할인</h5>
                <div class="pull-right mgr15">
                  <button id="period_price_delete" type="button" class="btn mgr7">삭제</button>
                  <button id="period_price_add" type="button" class="btn action_btn" data-toggle="modal" data-target="#longtime_cash">추가</button>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12 pdr0 pdl0">
                <div id="period_price_grid"></div>                
              </div>
            </div>
          </div>
        </div>

        <div class="panel-body">
          <div class="panel panel-define">
            <div class="panel-heading well bg_fff bb0" style="margin-bottom:0;">
              <div class="row">
                <h5 class="pull-left pdl15">특정할인</h5>
                <div class="pull-right mgr15">
                  <button id="special_price_delete" type="button" class="btn mgr7">삭제</button>
                  <button id="special_price_add" type="button" class="btn action_btn" data-toggle="modal" data-target="#certain_cash">추가</button>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12 pdr0 pdl0">
                <div id="special_price_grid"></div>                
              </div>
            </div>
          </div>
        </div>

      </div>          
    </div>              
  </div>
</div>


<script type="text/javascript">

kendo.culture("ko-KR");

  function add_div(){

    var div = document.createElement('div');
    div.innerHTML = document.getElementById('room_type').innerHTML;
    document.getElementById('field').appendChild(div);
  }

  function remove_div(obj) {
    document.getElementById('field').removeChild(obj.parentNode);
  }
</script>








<!-- Modal 장기할읹 등록-->
<div class="modal fade" id="longtime_cash" role="dialog">
  <div class="modal-dialog modal_dialog500">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button id="" type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="" class="modal-title">장기할인 설정</h4>
      </div>

      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12 pdt15 form-horizontal">
            <div class="form-group">
              <label class="col-sm-1 pdr0 pdl0 control-label text-left">기간</label>
              <div class="col-sm-11 long_time_discount"> 
                <input type="number" class="form-control" value="" placeholder="3" id="period_days">
               <span>일 이상</span>  
             </div> 
           </div>
          </div>
          <div class="col-sm-12 form-horizontal">
            <div class="form-group">
             <label for="" class="col-sm-1 pdl0 pdr0 control-label text-left">적용률</label>
             <div class="col-sm-11 percent">
              <input type="number" class="form-control" value="" placeholder="예) 100" id="period_percent">
              <span>%</span>
            </div>
          </div>
        </div>

      </div>
    </div>

    <div class="modal-footer">
      <button id="" type="button" class="btn" data-dismiss="modal">닫기</button>
      <button id="period_price_save_btn" type="button" class="btn btn-primary">저장</button>
      <button id="period_price_update_btn" type="button" class="btn btn-primary" style="display:none;">수정</button>

    </div>
  </div>
</div>
</div>
<!--/ Modal 장기할인 등록-->


<!-- Modal 특정할인 등록-->
<div class="modal fade" id="certain_cash" role="dialog">
  <div class="modal-dialog modal_dialog500">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button id="" type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="" class="modal-title">특정할인설정</h4>
      </div>

      <div class="modal-body">
        <div class="row">

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-1 control-label text-left pdr0 pdl0">날짜선택</label>
                <div class="col-sm-11 ">

                  <div class="input-daterange input-group" id="datetest">
                    <input type="text" class="form-control" id="start_date" data-date-format="yyyy년 mm월 dd일" readonly placeholder="시작 날짜" style="background:#FFF;">
               <!--    <div class="input-daterange input-group" id="datetest">
               <input type="text" class="input-sm form-control" name="from" placeholder="날짜 선택" id ="start_date"/> -->
               <script language="javascript">
                $('#start_date').datepicker({
                  navTitles: {
                   days: 'yyyy년 mm월'
                 },
                      //  todayButton: new Date(),
                      clearButton: true,
                      closeButton: true,
                      language: 'en',
                      onSelect: function (fd, d, picker) {
                              // Do nothing if selection was cleared
                              if (!d) return;
                              startDate = d.getFullYear()+"-"+zeroPad(d.getMonth()+1,2)+"-"+zeroPad(d.getDate(),2);
                            }
                          })
                        </script>
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        <input type="text" class="form-control" id="end_date" data-date-format="yyyy년 mm월 dd일" readonly placeholder="종료 날짜" style="background:#FFF;">
                        <script language="javascript">
                          $('#end_date').datepicker({
                            navTitles: {
                             days: 'yyyy년 mm월'
                           },
                            //  todayButton: new Date(),
                            clearButton: true,
                            closeButton: true,
                            language: 'en',
                            onSelect: function (fd, d, picker) {
                                    // Do nothing if selection was cleared
                                    if (!d) return;
                                    endDate = d.getFullYear()+"-"+zeroPad(d.getMonth()+1,2)+"-"+zeroPad(d.getDate(),2);
                                  }
                                })
                              </script>
                              <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div >

<!--                       <div class="col-sm-11 long_time_discount"> 
                <input type="number" class="form-control" value="" placeholder="3" id="period_days">
               <span>일 이상</span>  
             </div>  -->

                      <form class="form-horizontal">
                        <div class="form-group">
                          <label for="" class="col-sm-1 control-label text-left pdr0">적용률</label>
                          <div class="col-sm-11 percent">
                            <input type="number" class="form-control" value="" placeholder="100" id="special_percent">
                             <span>%</span> 
                          </div>
                        </div>
                      </form>
                    </div>

                    <div>
                      <form class="form-horizontal">
                        <div class="form-group">
                          <label for="" class="col-sm-1 control-label text-left pdr0">비고</label>
                          <div class="col-sm-11 ">
                            <input type="" class="form-control" value="" placeholder="" id="special_memo">
                          </div>
                        </div>
                      </form>
                    </div>

                  </div>
                </div>

                <div class="modal-footer">
                  <button id="" type="button" class="btn" data-dismiss="modal">닫기</button>
                  <button id="special_price_save_btn" type="button" class="btn btn-primary">저장</button>
                  <button id="special_price_update_btn" type="button" class="btn btn-primary" style="display:none;">수정</button>

                </div>
              </div>
            </div>
          </div>
          <!--/ Modal 특정할 등록-->
          <!-- 기본요금설 그리드 -->


<!-- <div class="row">
  <div class="col-md-12">
    <div class="panel border_gray magb0">
      
      <div class="panel-heading">
        <div class="row">
          <div class="col-md-2 pdr0">
            <h3 class="mgt20 mgb10">판매 지역 설정</h3>
          </div>
          <div class="col-md-3 pdl0">
            <ul class="nav nav-pills pdt10">
              <li class="active"><a data-toggle="pill" href="#subway_setup" >지하철</a></li>
              <li><a data-toggle="pill" href="#spot_city_setup">주요지역</a></li>
            </ul>  
          </div>
        </div>
      </div>
      
    </div>

    <div class="tab-content">
      
    
      <div id ="subway_setup" class="tab-pane fade in active">
      <div class="panel-footer section_lnb">
          <ul class="nav nav-tabs border_none" id="tab_subway_city">
          </ul>
      </div >
      <div class="panel-footer bg_fff">
        <div class="">
        <div class="delivery_wrap">
          <div class="ibox-title">
              <h5>지하철 딜리버리 설정</h5>
              <h6>딜리버리를 하실 지하철역을 설정합니다.</h6>
          </div>
          
          <div class="ibox float-e-margins magb0">
              <div class="ibox-content" id="div_subways">
         
                  <button class="btn action_btn pull-right" id="btn_register">저장</button>
                  
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>

    <div id="spot_city_setup" class="tab-pane fade">
     <div class="panel-footer section_lnb">
          <ul class="nav nav-tabs border_none" id="tab_spot_city">

          </ul>
      </div>
      <div class="panel-footer bg_fff">
       
        <div class="delivery_wrap">
          <div class="ibox-title">
              <h5>주요지역 딜리버리 설정</h5>
              <h6>딜리버리를 하실 주요지을 설정합니다.</h6>
          </div>
          
          <div class="ibox float-e-margins magb0">
              <div class="ibox-content" id="div_place">

              <button class="btn action_btn pull-right" id="btn_register">저장</button>

              </div>

          </div>

        </div>
      </div>
      </div>

      </div>
      </div>
      </div> -->
          <script> 

//company serial 얻기
function get_company_serial(){
  if($("#car_company_select").is(":visible")){
    var company_serial = $("#car_company_select option:selected").val();
  }else{
    var company_serial = <?php echo $company_serial; ?>;
  }
  return company_serial;
}



//슈퍼 마스터가 아니면 회사 선택 select 안보임
if($("#car_company_select option").size()==1){
  $("#car_company_select").hide();
}else{
  $("#car_company_select").show();
}


get_basic_price();

//company select 리스너
$( "#car_company_select" ).change(function() {
    // var selectedSerial = $(this).val();
    // basic_grid_refresh();

  get_basic_price();
  period_grid_refresh();
  special_grid_refresh();


});

// $( "#car_branch_select" ).change(function() {
//     // var selectedSerial = $(this).val();
//     // basic_grid_refresh();
//     get_basic_price();
//     period_grid_refresh();
//     special_grid_refresh();
//  });

 //기간 grid 초기화
 $("#period_price_grid").kendoGrid({
          selectable: true, //선택할수 있도록
          height: 200, //높이????
          sortable: true, //정렬가능하도록
          pageable: false,
          // pageable: {
          //     input: true,
          //     messages: {
          //         display: "Showing {0}-{1} from {2} data items",
          //         empty: "No data"
          //     }
          //   },
          noRecords: {
            template: "기간 할인이 입력되어 있지 않습니다. [추가]를 눌러 등록해보세요."
          },
          dataSource: {
            transport: {
             read: {
              url: "/preference/get_period_price/<?php echo $company_serial; ?>/0",
              dataType: "json"
            }
          },
          schema: {
            model: {
              fields: {
                days: { type: "number" },
                percent: { type: "number" },
                sales: {type:"count"},
                registration_date: {type:"date"},
                user_name: { type: "string" }, 
                birthday: { type: "srting" }, 
                address: { type: "string" }, 
                phone_number1: { type: "string" },
                phone_number2: { type: "string" },
                corporation_name: { type: "string" }, 
                auth_code: { type: "number" }, 
                note: {type:"string"},
                license_type: { type: "string" }, 
                published_date: { type: "date" }, 
                license_number: { type: "string" }, 
                expiration_date: { type: "date" }, 
              }
            }
          },
          pageSize: 30
        },

        columns: [
        {
          field: "days",
          title: "기간",
          format:  "{0:##,#} 일 이상",
        },  {
          field: "percent",
          title: "적용률",
          format:  "{0:##,#}%",


        }
        ]
      });
 //특정할인 grid 초기화
 $("#special_price_grid").kendoGrid({
                navigatable: true,  //키보드로 표 조작 할수 있게
                selectable: true, //선택할수 있도록
                allowCopy: true, //값 copyrksm
                height: 200, //높이????
                sortable: true, //정렬가능하도록
                // filterable: true, //필터(비교해서 정렬)가능하도록
                groupable: false, //그룹으로 정렬 가능
                // excel: {
                //   allPages: true,
                //   fileName: "고객리스트.xlsx",
                //   filterable: true
                // },
                pageable: false,
                // pageable: {
                //     input: true,
                //     messages: {
                //         display: "Showing {0}-{1} from {2} data items",
                //         empty: "No data"
                //     }
                //   },
                noRecords: {
                  template: "특정 할인이 입력되어 있지 않습니다. [추가]를 눌러 등록해보세요."
                },
                // columnMenu: {
                //   sortable: false,
                //   messages: {
                //     columns: "표시할 항목 선택",
                //     filter: "필터",
                //   }
                // },
                dataSource: {
                      transport: {
                       read: {
                        url: "/preference/get_special_price/<?php echo $company_serial; ?>/0",         
                        dataType: "json"
                      }
                    },
                    schema: {
                      model: {
                        fields: {
                          start_date: { type: "date" },
                          end_date: { type: "date" },
                          percent: {type:"number"},
                          memo: {type:"string"},
                          user_name: { type: "string" }, 
                          birthday: { type: "srting" }, 
                          address: { type: "string" }, 
                          phone_number1: { type: "string" },
                          phone_number2: { type: "string" },
                          corporation_name: { type: "string" }, 
                          auth_code: { type: "number" }, 
                          note: {type:"string"},
                          license_type: { type: "string" }, 
                          published_date: { type: "date" }, 
                          license_number: { type: "string" }, 
                          expiration_date: { type: "date" }, 
                                  }
                                }
                              },
                          pageSize: 30
                        },

                        columns: [ 
                        {
                          field: "start_date",
                          title: "시작날짜",
                          format: "{0: yyyy년 MM월 dd일}",
                          // width: 200
                        },  {
                          field: "end_date",
                          title: "종료날짜",
                          format: "{0: yyyy년 MM월 dd일}",
                          // width: 200
                        },  {
                          field: "percent",
                          title: "적용률",
                          // width: 140,
                          format:  "{0:##,#}%",
                        },  {
                          field: "memo",
                          title: "비고",
                          // width: 100

                        }
                        ]
                      });

     



        // var grid = $("#period_price_grid").data("kendoGrid");
        var btn_period_price_register = $("#period_price_save_btn");
        var btn_period_price_update = $("#period_price_update_btn");
        var btn_period_price_delete = $("#period_price_delete");
        var btn_period_price_add = $("#period_price_add");
        
        btn_period_price_add.click(function() {

        //  if(<? echo $company_serial; ?> == 0){
        //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
        //   return false;
        // }

        var grid = $("#period_price_grid").data("kendoGrid");
        var row = grid.select();
        var data = grid.dataItem(row);

        $('#period_price_save_btn').show();
        $('#period_price_update_btn').hide(); 

        $('#period_days').val('');
        $('#period_percent').val('');

      });

        btn_period_price_delete.click(function() {

          // if(<? echo $company_serial; ?> == 0){
          //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
          //   return false;
          // }
          var grid = $("#period_price_grid").data("kendoGrid");
          var row = grid.select();
          var data = grid.dataItem(row);
          if(data==null){
            alert("삭제할 요금제를 선택해 주세요.");
            return false;
          }


          $.post("/preference/period_price_delete/",
          {
                    // days: $('#period_days').val() ,
                    // percent : $('#period_percent').val(),
                    serial : data.serial
                    // car_index : data.car_index //수정사항 10/3일.
                  },
                  function(data, status){
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                      alert("삭제되었습니다.");
                        //데이터 다시 읽어옴
                        period_grid_refresh();
                      }else{
                        alert("데이터 삭제 중 에러가 발생했습니다.");
                      }

                    });
        });

//기간 할인 등록
btn_period_price_register.click(function(){

// alert("hi");
  var company_serial = get_company_serial();

  if($('#period_days').val() < 0 ){
     alert("기간은 양수만 입력 가능 합니다.");
     $('#period_days').val('');
     $('#period_days').focus();
     return false;
  }

  if($('#period_percent').val() < 0 ||  $('#period_percent').val() > 100){
     alert("적용률은 0~100사이의 수만 입력 가능 합니다.");
     $('#period_percent').val('');
     $('#period_percent').focus();
     return false;
  }

  $.post("/preference/period_price_add/",
  {
    company_serial : company_serial,
    days: $('#period_days').val() ,
    percent : $('#period_percent').val()

  },
  function(data, status){
                      // alert ("hi2");
                      var result = JSON.parse(data);
                      if(result.code=="S01"){
                        alert("등록되었습니다.");
                          //데이터 다시 읽어옴
                          // grid.refresh();
                          // alert(result.car_index);
                          // $('#company_serial').val(result.company_serial);
                          // $('#detail_car_index').val(result.car_index);

                          // alert($('#file_upload').submit());
                          // return false;

                          period_grid_refresh();

                          $("[data-dismiss=modal]").trigger({ type: "click" });
                          // $('.modal-backdrop').toggle();
                      }else if(result.code=="E02"){//이미 있는 경우
                        alert("이미 등록 된 기간입니다.");
                      }else{
                        alert("데이터 입력 중 에러가 발생했습니다.");
                      }

                    });

});

//기간 할인 업데이트
btn_period_price_update.click(function(){
          var grid = $("#period_price_grid").data("kendoGrid");
          var row = grid.select();
          var data = grid.dataItem(row);

          var company_serial = get_company_serial();

          if($('#period_days').val() < 0 ){
             alert("기간은 양수만 입력 가능 합니다.");
             $('#period_days').val('');
             $('#period_days').focus();
             return false;
          }

          if($('#period_percent').val() < 0 ||  $('#period_percent').val() > 100){
             alert("적용률은 0~100사이의 수만 입력 가능 합니다.");
             $('#period_percent').val('');
             $('#period_percent').focus();
             return false;
          }

          $.post("/preference/period_price_update/",
          {

            serial : data.serial,
            company_serial : company_serial,
            days: $('#period_days').val() ,
            percent : $('#period_percent').val()

          },
          function(data, status){
            // alert(data);
            var result = JSON.parse(data);

            if(result.code=="S01"){
              alert("수정되었습니다.");
                        //데이터 다시 읽어옴
                        // grid.refresh();
                        // alert(result.car_index);
                        // $('#company_serial').val(result.company_serial);
                        // $('#detail_car_index').val(result.car_index);

                        // alert($('#file_upload').submit());
                        // return false;

                        period_grid_refresh();

                        $("[data-dismiss=modal]").trigger({ type: "click" });
                        // $('.modal-backdrop').toggle();
                    }else if(result.code=="E02"){//이미 있는 경우
                      alert("이미 등록 된 기간입니다.");
                    }else{
                      alert(result.message);
                    }

                  });

        });

        function period_grid_refresh(){

          var company_serial = get_company_serial();

          var url = "/preference/get_period_price/" + company_serial;
          var dataSource = new kendo.data.DataSource({
           transport: {
            read: {
             url: url,
             dataType: "json"
           }
         },
         pageSize: 10
       });
          var grid = $("#period_price_grid").data("kendoGrid");
          grid.setDataSource(dataSource);
        }
        $("#period_price_grid").delegate("tbody>tr", "dblclick", function(){
        //  if(<? echo $company_serial; ?> == 0){
        //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
        //   return false;
        // }
        var grid = $('#period_price_grid').data('kendoGrid'); 
        var row = grid.select();
        var data = grid.dataItem(row);

        $('#period_price_save_btn').hide();
        $('#period_price_update_btn').show();

        $('#period_days').val(data.days);
        $('#period_percent').val(data.percent);



        $('#longtime_cash').modal('toggle');

      });
  // });



        // $(document).ready(function() {


        // var grid = $("#special_price_grid").data("kendoGrid");
        var btn_special_price_register = $("#special_price_save_btn");
        var btn_special_price_delete = $("#special_price_delete");
        var btn_special_price_update = $("#special_price_update_btn");
        var btn_special_price_add = $("#special_price_add");
        
        btn_special_price_add.click(function() {

          // if(<? echo $company_serial; ?> == 0){
          //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
          //   return false;
          // }

          var grid = $("#special_price_grid").data("kendoGrid");
          var row = grid.select();
          var data = grid.dataItem(row);
          $('#special_price_save_btn').show();
          $('#special_price_update_btn').hide();

          $('#special_memo').val('');
          $('#special_percent').val('');

          $('#start_date').datepicker().data('datepicker').date = new Date();
          $('#end_date').datepicker().data('datepicker').date = new Date();
          $('#start_date').val('');
          $('#end_date').val('');
        });

        btn_special_price_delete.click(function() {

          // if(<? echo $company_serial; ?> == 0){
          //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
          //   return false;
          // }
          var grid = $("#special_price_grid").data("kendoGrid");
          var row = grid.select();
          var data = grid.dataItem(row);
          if(data==null){
            alert("삭제할 요금제를 선택해 주세요.");
            return false;
          }


          $.post("/preference/special_price_delete/",
          {
                    serial : data.serial //수정사항 10/3일.
                  },
                  function(data, status){
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                      alert("삭제되었습니다.");
                        //데이터 다시 읽어옴
                        special_grid_refresh();
                      }else{
                        alert("데이터 삭제 중 에러가 발생했습니다.");
                      }

                    });
        });


var startDate = '';
var endDate = '';

btn_special_price_register.click(function(){

          $('#special_price_save_btn').show();
          $('#special_price_update_btn').hide();
                     // $('#start_date').val();
                     // $('#end_date').val();
                     // $('#special_memo').val();
                     // $('#special_percent').val();
  // alert($('#special_price_save_btn').show());

  var company_serial = get_company_serial();

  if( $('#start_date').val() > $('#end_date').val()){
    alert("종료 날짜가 과거입니다.");
    $('#end_date').val('');
    $('#end_date').focus();
    return false;
  }

  if($('#special_percent').val() < 0 ||  $('#special_percent').val() > 100){
             alert("적용률은 0~100사이의 수만 입력 가능 합니다.");
             $('#special_percent').val('');
             $('#special_percent').focus();
             return false;
  }

  $.post("/preference/special_price_add/",
  {
    company_serial : company_serial,
    start_date: startDate,
    end_date : endDate,
    memo : $('#special_memo').val(),
    percent : $('#special_percent').val()

  },
  function(data, status){
                    // alert ("hi2");
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                      alert("등록되었습니다.");
                        //데이터 다시 읽어옴
                        // grid.refresh();
                        // alert(result.car_index);
                        // $('#company_serial').val(result.company_serial);
                        // $('#detail_car_index').val(result.car_index);

                        // alert($('#file_upload').submit());
                        // return false;

                        special_grid_refresh();

                        $("[data-dismiss=modal]").trigger({ type: "click" });
                        // $('.modal-backdrop').toggle();
                    }else if(result.code=="E02"){//이미 있는 경우
                      alert("이미 등록 된 기간 입니다.");
                    }else{
                      alert("데이터 입력 중 에러가 발생했습니다.");
                    }

                  });

});

btn_special_price_update.click(function(){
// alert($('#start_date').val());

    var company_serial = get_company_serial();

    var grid = $("#special_price_grid").data("kendoGrid");
    var row = grid.select();
    var data = grid.dataItem(row);
    if(data==null){
      alert("수정할 요금제를 선택해 주세요.");
      return false;
    }

    if($('#special_percent').val() < 0 ||  $('#special_percent').val() > 100){
                 alert("적용률은 0~100사이의 수만 입력 가능 합니다.");
                 $('#special_percent').val('');
                 $('#special_percent').focus();
                 return false;
      }

    if( $('#start_date').val() > $('#end_date').val()){
      alert("종료 날짜가 과거입니다.");
      $('#end_date').val('');
      $('#end_date').focus();
      return false;
    }




    // alert(data.serial);
        $.post("/preference/special_price_update/",
        {
          serial : data.serial,
          company_serial : company_serial,
          start_date: startDate,
          end_date : endDate,
          memo : $('#special_memo').val(),
          percent : $('#special_percent').val()

        },
        function(data, status){
                            // alert ("hi2");
                            var result = JSON.parse(data);
                            if(result.code=="S01"){
                              alert("수정되었습니다.");


                              special_grid_refresh();

                              $("[data-dismiss=modal]").trigger({ type: "click" });
                                // $('.modal-backdrop').toggle();
                            }else if(result.code=="E02"){//이미 있는 경우
                              alert("이미 등록 된 기간 입니다.");
                            }else{
                              alert("데이터 입력 중 에러가 발생했습니다.");
                            }

                          });
        

});

function special_grid_refresh(){

          var company_serial = get_company_serial();

          var url = "/preference/get_special_price/" + company_serial;
          var dataSource = new kendo.data.DataSource({
           transport: {
            read: {
             url: url,
             dataType: "json"
           }
         },
         pageSize: 10,
          schema: {
                      model: {
                        fields: {
                          start_date: { type: "date" },
                          end_date: { type: "date" },
                          percent: {type:"number"},
                          memo: {type:"string"},
                          user_name: { type: "string" }, 
                          birthday: { type: "srting" }, 
                          address: { type: "string" }, 
                          phone_number1: { type: "string" },
                          phone_number2: { type: "string" },
                          corporation_name: { type: "string" }, 
                          auth_code: { type: "number" }, 
                          note: {type:"string"},
                          license_type: { type: "string" }, 
                          published_date: { type: "date" }, 
                          license_number: { type: "string" }, 
                          expiration_date: { type: "date" }, 
                                  }
                                }
                              }
       });
          var grid = $("#special_price_grid").data("kendoGrid");
          grid.setDataSource(dataSource);
}

$("#special_price_grid").delegate("tbody>tr", "dblclick", function(){

        //  if(<? echo $company_serial; ?> == 0){
        //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
        //   return false;
        // }

        var grid = $('#special_price_grid').data('kendoGrid'); 
        var row = grid.select();
        var data = grid.dataItem(row);
    // alert(data.start_date);
// alert(data.memo);
$('#special_price_save_btn').hide();
$('#special_price_update_btn').show();
      // alert($('#special_price_save_btn'));
      $('#start_date').datepicker().data('datepicker').selectDate(data.start_date);
      $('#end_date').datepicker().data('datepicker').selectDate(data.end_date);

      // $('#start_date').val(kendo.toString(data.start_date, "yyyy년 MM월 dd일"));
      // $('#end_date').val(kendo.toString(data.end_date, "yyyy년 MM월 dd일"));
      $('#special_memo').val(data.memo);
      $('#special_percent').val(data.percent);



      $('#certain_cash').modal('toggle');

    });


        // });
        $("#basic_price_add_btn").click(function(){

          // if(<? echo $company_serial; ?> == 0){
          //   alert("슈퍼관리자는 요금정보는 수정, 삭제가 불가능 합니다.");
          //   return false;
          // }

          //유효성 검사
          if($('#input_6_hour_week').val() >100 || $('#input_6_hour_week').val()<0){
            alert("0~100 사이의 값만 입력 가능합니다.");
            $('#input_6_hour_week').val('');
            $('#input_6_hour_week').focus();
            return false;
          }
          if($('#input_6_hour_weekend').val() >100 || $('#input_6_hour_weekend').val()<0){
            alert("0~100 사이의 값만 입력 가능합니다.");
            $('#input_6_hour_weekend').val('');
            $('#input_6_hour_weekend').focus();
            return false;
          }
          if($('#input_1_hour_week').val() >100 || $('#input_1_hour_week').val()<0){
            alert("0~100 사이의 값만 입력 가능합니다.");
            $('#input_1_hour_week').val('');
            $('#input_1_hour_week').focus();
            return false;
          }
          if($('#input_1_hour_weekend').val() >100 || $('#input_1_hour_weekend').val()<0){
            alert("0~100 사이의 값만 입력 가능합니다.");
            $('#input_1_hour_weekend').val('');
            $('#input_1_hour_weekend').focus();
            return false;
          } 

          var company_serial = get_company_serial();

          $.post("/preference/basic_price_add/",
          {
            week_6 : $('#input_6_hour_week').val(),
            week_1 : $('#input_1_hour_week').val(),
            weekend_6 : $('#input_6_hour_weekend').val(),
            weekend_1 : $('#input_1_hour_weekend').val(),
            company_serial : company_serial,
          },
          function(data){
                    // alert ("hi2");
                    // alert(data);
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                      alert("등록되었습니다.");
                    }else{
                      alert(result.message);
                    }

                  });
        });



        function get_basic_price(){

          var company_serial = get_company_serial();

  // alert(branch_serial);

  $.get("/preference/get_basic_price/"+company_serial,
    function(data){
      // alert(data);
      var result = JSON.parse(data);
      if(result.length == 0){
        // alert('no');
        $("#input_6_hour_week").val('');
        $("#input_1_hour_week").val('');
        $("#input_6_hour_weekend").val('');
        $("#input_1_hour_weekend").val('');
      }else{
        $("#input_6_hour_week").val(result[0].week_6);
        $("#input_1_hour_week").val(result[0].week_1);
        $("#input_6_hour_weekend").val(result[0].weekend_6);
        $("#input_1_hour_weekend").val(result[0].weekend_1);
      }
      // var result = JSON.parse(data);
      // var text = "";
      // for(var i=0; i< result.length; i++){
      //   text += "<option value='"+ result[i].serial+"'>" + result[i].branch_name +"</option>";
      // }
      // $( "#car_branch_select").html(text);


    });
}


$('#longtime_cash, #certain_cash').draggable({
  handle: ".modal-header"
});

function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}

</script>

    <script type="text/javascript">


var subwayArray = new Array();
// var spotArray = new Array();
var company_serial = <?php echo $company_serial; ?>;


var selected_city = "수도권";
var selected_city2 = '';




get_subway_city();
get_spot_city();
get_delivery_count();

if(company_serial != 0){
  getWorkTime();
}


//슈퍼 마스터가 아니면 회사 선택 select 안보임
if($("#car_company_select option").size()==1){
  $("#car_company_select").hide();
}else{
  $("#car_company_select").show();
}



function city_select(city_name){
   selected_city = city_name;
    get_subway(city_name);
}

function spot_city_select(city_name){
  selected_city2 = city_name;
  get_spot_place(city_name);
}

function get_subway_city(){
   $.get("/salesetting/get_subway_city",
    function(data){
      var result = JSON.parse(data);

      var text='';
      for(var i=0; i<result.length; i++){
        if(i==0){
            text += "<li class='active'>";
        }else{
            text += "<li>";
        }
        text += ("<a data-toggle='tab' href='#' id='tab" + (i+1) +"' onclick='city_select(\""+result[i].city +"\")' >" + result[i].city + " </a></li>");
      }
      $( "#tab_subway_city").html(text);

      get_subway($("#tab1").text());
      
  });
}

function get_spot_city(){
   $.get("/salesetting/get_spot_city",
    function(data){
      var result = JSON.parse(data);
      selected_city2 = result[0].city;
      var text='';
      for(var i=0; i<result.length; i++){
        if(i==0){
            text += "<li class='active'>";
        }else{
            text += "<li>";
        }
        text += ("<a data-toggle='tab' href='#' id='tab_spot_" + (i+1) +"' onclick='spot_city_select(\""+result[i].city +"\")' >" + result[i].city + " </a></li>");
      }
      $( "#tab_spot_city").html(text);

      get_spot_place($("#tab_spot_1").text());
      
  });
}

function get_delivery_count(){
    var company_serial = get_company_serial();
    $.get("/salesetting/get_delivery_max_per_hour/"+company_serial,
    function(data){
        var result = JSON.parse(data);
        $("#delivery_max_count").val(result[0].delivery_max_per_hour);   
    });

}

function get_subway(city_name){

  var company_serial = get_company_serial();
  if(company_serial==0){
      // alert("업체를 먼저 선택해 주세요.");
      return false;
  }


  $.get("/salesetting/get_delivery_subway_by_city/"+city_name+"/"+company_serial,
  function(data){

    var resultArray = JSON.parse(data);
    var subwayLineArray = new Array();
    for(i=0; i<resultArray.length; i++){
      var tempSubwayLine = resultArray[i].area_kind.split(" ");
      subwayLineArray.push(tempSubwayLine[2]);
    }

      //중복제거
      var subways = subwayLineArray.filter(function(itm, i, a){
        return i==a.indexOf(itm);
      });


      var text = '';
      subwayArray = Array();
      // alert(data);
      for(var i=0; i< subways.length; i++){
        subwayArray.push(subways[i]);
        text +=  "<div class='row'><div class='magb15 col-sm-12' ><h4 class='pull-left'>" + subways[i] + "</h4> <div class='pull-right'><div class='all_chk_btn'><div class='pull-left' onclick='all_select(\""+city_name+"\", \""+subways[i]+"\")'>전체선택</div><div class='pull-left' onclick='all_deselect(\""+city_name+"\" , \""+subways[i]+"\")'>전체해제</div></div></div></div><div class='col-sm-12'><ul class='delivery'>";
        for(j=0; j<resultArray.length; j++){
          var tempSubwayLine = resultArray[j].area_kind.split(" ");

          if(subways[i] == tempSubwayLine[2]){

            text += "<li><label for='chk" + (j+1) +"'><span class='agree'><input type='checkbox' id='chk" + (j+1) +"' name='subway_" + subways[i] +"'  value='" + resultArray[j].serial + "'";

            // text += "<input type='checkbox' id='check_subway' name='subway_" + subways[i] + "' value='" + resultArray[j].serial + "'";
            if(resultArray[j].selected == true){
              text += " checked ";
            }
            text += "><span class='chk_txt'>" + resultArray[j].location_name_kor +"</span></span></label></li>";
          }
        }
        text += "</ul></div></div><div class='hr-line-dashed'></div>";
      }
      text += "<button class='btn action_btn pull-right' id='btn_a' onclick='save_subway()'>저장</button>";
      

      $( "#div_subways").html(text);
    });
}

function get_spot_place(city_name){
      // var company_serial = get_company_serial();
      // alert($company_serial);

  var company_serial = get_company_serial();
  if(company_serial==0){
      // alert("업체를 먼저 선택해 주세요.");
      return false;
  }

   if($("#car_company_select").is(":visible")){
    var company_serial = $("#car_company_select option:selected").val();
   }else{
    var company_serial = <?php echo $company_serial; ?>;
   } 

       $.get("/salesetting/get_spots/"+city_name+"/"+company_serial,
          function(data){
             // alert(data);
             var result = JSON.parse(data);
             if(result.length == 0){
                //아무 등록된 지역이 없으면
                // alert('hi');
                 var text = '';
                $( "#div_place").html(text);
             }else{
                //등록된 지역이 있으면
                // alert('no');

                var areaNameArray = new Array();
                for(i=0; i<result.length; i++){
                  var area_name = result[i].area_name;
                  areaNameArray.push(area_name);
                }

                  //중복제거
                  var areaArray = areaNameArray.filter(function(itm, i, a){
                    return i==a.indexOf(itm);
                  });
                var text = '';
                 for(var i=0; i< areaArray.length; i++){
                    text +=  "<div class='row'><div class='magb15 col-sm-12' ><h4 class='pull-left'>" + areaArray[i] + "</h4> <div class='pull-right'></div></div><div class='col-sm-12'><ul class='delivery'>";
                    for(j=0; j<result.length; j++){
                      // var tempSubwayLine = resultArray[j].area_kind.split(" ");

                      if(areaArray[i] == result[j]['area_name']){

                        text += "<li><label for='chk" + (j+1) +"'><span class='agree'><input type='checkbox' id='chk" + (j+1) +"' name='place'  value='" + result[j].serial + "'";

                         if(result[j].selected == true){
                              text += " checked ";
                          }
                        text += "><span class='chk_txt'>" + result[j].location_name_kor +"</span></span></label></li>";
                      }
                    }
                    text += "</ul></div></div><div class='hr-line-dashed'></div>";
                  }
                  text += "<button class='btn action_btn pull-right' id='btn_b' onclick='save_special_place()'>저장</button>";

                  $( "#div_place").html(text);
             }

        });
}


//////////////////////////////////////////////////////////////// 파트너 선택 리스너

$( "#car_company_select" ).change(function() {
 company_serial = $(this).val();

  getWorkTime();
  get_subway(selected_city);
  get_spot_place(selected_city2);
});

// $( "#car_branch_select" ).change(function() {
//   branch_serial = $(this).val();
// });




/////////////////////////////////////////////////////////////// 브런치 선택 리스너

function getWorkTime(){

   var company_serial = get_company_serial();

 $.get("http://solution.rengo.co.kr/salesetting/get_start_end_time/"+company_serial,
  function(data){
        // alert(data);
        result = JSON.parse(data);
        var openTime = result[0].open_time_start;
        var endTime = result[0].open_time_finish;


        $('#partner_time_st_input').val(openTime.substring(0,2)+":"+openTime.substring(2,4));
        $('#partner_time_end_input').val(endTime.substring(0,2)+":"+endTime.substring(2,4));
      });
}


$("#btn_register").click(function() {
  // alert("전체");
});


function save_subway(){

    var company_serial = get_company_serial();
    if(company_serial == 0){
       alert('업체를 선택해 주세요.');
       return false;
    }


    var serialArray = Array();
    //지하철역 선택 된거 serial 얻어옴
    for(var i=0; i<subwayArray.length; i++){
      var name = "subway_" + subwayArray[i];
      $("input[name='"+ name+ "']:checked").each(function() {
        var serial = $(this).val();
        serialArray.push(serial);
      });
    }

      $.post("/salesetting/save_subway",
      {
          company_serial : company_serial,
          city : selected_city, //수정사항 10/3일.
          subways : serialArray
      },
      function(data, status){
            // alert(data);
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                        alert("저장 되었습니다.");
                    }else{
                        alert("데이터 저장 중 에러가 발생했습니다.");
                    }

      });

    $('#check_subway:checked').each(function() { 
      alert($(this).val());
    });

}

function save_special_place(){

   var company_serial = get_company_serial();
   if(company_serial == 0){
       alert('업체를 선택해 주세요.');
       return false;
    }


    var serialArray = Array();
    //지하철역 선택 된거 serial 얻어옴

    $("input[name='place']:checked").each(function() {
        var serial = $(this).val();
        serialArray.push(serial);
    });


      $.post("/salesetting/save_spot",
      {
          company_serial : company_serial,
          city : selected_city2, //수정사항 10/3일.
          spot : serialArray
      },
      function(data, status){
            // alert(data);
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                        alert("저장 되었습니다.");
                    }else{
                        alert("데이터 저장 중 에러가 발생했습니다.");
                    }

      });

}



function all_select(city_name, subway){
  // alert(city_name+","+subway);
  var name = "subway_" + subway;
  $("input:checkbox[name='" + name +"']").prop("checked", true);
}

function all_deselect(city_name, subway){
// alert(city_name+","+subway);
  var name = "subway_" + subway;
  $("input:checkbox[name='" + name +"']").prop("checked", false);
}



$("#btn_time_save").click(function() {

  var company_serial = get_company_serial();
  if(company_serial == 0){
    alert('업체를 선택해 주세요.');
    return false;
  }

  var start_time = $("#partner_time_st_input").val();
  var st_token = start_time.split(':');
  var st_str = st_token[0] + st_token[1];

  var end_time = $("#partner_time_end_input").val();
  var end_token = end_time.split(':');
  var end_str = end_token[0] + end_token[1];

  var max = $("#delivery_max_count").val();

  // alert(st_str);
    $.post("/salesetting/save_time_and_delivery",
      {
          company_serial : company_serial,
          start_time : st_str, //수정사항 10/3일.
          end_time : end_str,
          max : max
      },
      function(data, status){
            // alert(data);
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                        alert("저장 되었습니다.");
                    }else{
                        alert(result.message);
                    }

      });

});


function get_company_serial(){

   if($("#car_company_select").is(":visible")){
    var company_serial = $("#car_company_select option:selected").val();
   }else{
    var company_serial = <?php echo $company_serial; ?>;
  }

  return company_serial; 
}


$('select[multiple]').multiselect({
  columns: 3,
  placeholder: '서비스 지역 선택',
  selectGroup   : true, // select entire optgroup
  minHeight  : 400,   // minimum height of option overlay
});
</script>

<script src="http://rengo.co.kr/partner/js/jquery.multiselect.js"></script>
<style>
ul,li { margin:0; padding:0; list-style:nont;}
.label { color:#000; font-size:12px;}
</style>



