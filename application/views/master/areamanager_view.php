<link href="/telerik/examples/content/shared/styles/examp름les-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">

<script src="/telerik/js/kendo.all.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<link href="/css/rengo_login.css" rel="stylesheet">

<?php
  require("/home/apache/CodeIgniter-3.0.6/application/views/master/dialog_areamanager.php");
?>


<div class="row">
  <div class="col-md-12">
    <div class="panel bb0 magb0">
      <div class="panel-heading">
        <div class="row">
          <div class="col-md-9">
            <h3 class="mgt20 mgb10">지역관리</h3>
          </div>
          <div class="col-sm-3 mgt20">
            <div class="col-md-6 pdl0">
            </div>
          </div>
        </div>
      </div>

    </div>

      <div class="panel-footer section_lnb">
          <ul class="nav nav-tabs border_none" id="tab_city">
            <li class="active"><a data-toggle="tab" href="#" id="tab1" >수도권</a></li>
            <li><a data-toggle="tab" href="#" id="tab6">부산</a></li>
          </ul>
      </div>

      <div class="panel-footer bg_fff">
        <div class="">
        <div class="delivery_wrap">
          <div class="ibox-title">
            <div class="row">
              <div class="pull-left">
                <h5>주요지역 딜리버리 추가</h5>
                <h6>딜리버리를 하실 주요지를 추가합니다.</h6>
              </div>
              <div class="pull-right">
                <div class="all_chk_btn">
                  <div class="pull-left" data-toggle="modal" id="button_register" data-target="#dialog_areamanager">추가</div>
                  <div class="pull-left" id="button_delete">삭제</div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="ibox float-e-margins magb0">
              <div class="ibox-content" id="div_place">
                </div>
              </div>
          </div>
        </div>
      </div>
          </div>
        </div>
      </div>
    </div>


<!-- Modal 지역정보 등록-->
<div class="modal fade" id="places" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button id="" type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="" class="modal-title">주요지역 추가</h4>
      </div>

      <div class="modal-body">
        <div class="row">
          
          <!--
          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">도, 시</label>
                <div class="col-sm-5">
	                <select id="car_select_status" class="form-control">
        						<option>부산</option>
        						<option>부산</option>
        					</select>
                </div>
              </div>
            </form>
          </div>

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">군, 구</label>
                <div class="col-sm-5">
                  <select id="car_select_status" class="form-control">
                    <option>강남구</option>
                    <option>서초구</option>
                  </select>
                </div>
              </div>
            </form>
          </div>
          -->


          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">지역명</label>
                <div class="col-sm-10 ">
                  <input type="" class="form-control" value="" placeholder="예) 강남호텔" id="special_memo">
                </div>
              </div>
            </form>
          </div>

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label text-left pdr0">인근지하철역</label>
                <div class="col-sm-10">
                <div class="input-group custom-search-form">
                  <input id="special_memo" type="text" class="form-control" placeholder="Search...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button" id="btn_search">
                        <i class="fa fa-search"></i>
                    </button>
                  </span>
                </div>
              </div>
              </div>
            </form>
          </div>

          <div class="row">
            <div class="col-sm-10 col-md-offset-2">
              <div class="panel panel-default">
                <div class="panel-body vehicle_info" id="">
                  <span class="label label-default">서울 1호선 강남</span>
                  <span class="label label-default">서울 1호선 서초</span>
                  <span class="label label-default">서울 1호선 부산대</span>
                  <span class="label label-default">서울 1호선 서울대</span>
                  <span class="label label-default">서울 1호선 신사</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button id="" type="button" class="btn" data-dismiss="modal">닫기</button>
        <button id="special_price_save_btn" type="button" class="btn action_btn">저장</button>
        <button id="special_price_update_btn" type="button" class="btn btn-primary" style="display:none;">수정</button>
        
      </div>
    </div>
  </div>
</div>
<!--/ Modal 지역정보 등록-->

<script> 


$('.modal').on('hidden.bs.modal',function() {
  $('.modal_scroll').scrollTop(0);
  setTimeout(function(){
    $('.modal').css({
      'top':'0',
      'left':'0'
    });

    },30)
})

$('.modal').draggable();


var selected_city = "강원도";
//도시 tab 만들기
get_city();
get_special_spot(selected_city);

//슈퍼 마스터가 아니면 회사 선택 select 안보임
// if($("#car_company_select option").size()==1){
//   $("#car_company_select").hide();
// }else{
//   $("#car_company_select").show();
// }

//1. company select

// $( "#car_company_select" ).change(function() {
//  company_serial = $(this).val();
//  branch_serial = 0;

//   //1. get branch_list
//   $.get("/carmanager/get_car_branch/"+company_serial,
//     function(data){
//       var result = JSON.parse(data);
//       var text = "";
//       for(var i=0; i< result.length; i++){
//         text += "<option value='"+ result[i].serial+"'>" + result[i].branch_name +"</option>";
//       }
//       $( "#car_branch_select").html(text);


//     });

// });

// //2. branch select
// $( "#car_branch_select" ).change(function() {
//   branch_serial = $(this).val();
// });

// function get_company_serial(){
//   if($("#car_company_select").is(":visible")){
//       var company_serial = $("#car_company_select option:selected").val();
//   }else{
//       var company_serial = <?php echo $company_serial; ?>;
//   }
//   return company_serial;
// }

// function get_branch_serial(){
//     var branch_serial = $("#car_branch_select option:selected").val();
//     return branch_serial;
// }


function get_city(){
   $.get("/areamanager/get_all_city",
    function(data){
            // alert(data);
      var result = JSON.parse(data);


      var text='';
      for(var i=0; i<result.length; i++){
        if(i==0){
            text += "<li class='active'>";
        }else{
            text += "<li>";
        }
        text += ("<a data-toggle='tab' href='#' id='tab" + (i+1) +"' onclick='city_select(\""+result[i].sido +"\")' >" + result[i].sido + " </a></li>");
      }
      $( "#tab_city").html(text);
  });
}

function city_select(city_name){
   selected_city = city_name;
   get_special_spot(city_name);
}

function get_special_spot(city_name){
      // var company_serial = get_company_serial();
      // alert($company_serial);
       $.get("/areamanager/get_place_by_city/"+city_name,
          function(data){
             // alert(data);
             var result = JSON.parse(data);
             if(result.length == 0){
                //아무 등록된 지역이 없으면
                // alert('hi');
                 var text = '';
                $( "#div_place").html(text);
             }else{
                //등록된 지역이 있으면
                // alert('no');

                var areaNameArray = new Array();
                for(i=0; i<result.length; i++){
                  var area_name = result[i].area_name;
                  areaNameArray.push(area_name);
                }

                  //중복제거
                  var areaArray = areaNameArray.filter(function(itm, i, a){
                    return i==a.indexOf(itm);
                  });
                var text = '';
                 for(var i=0; i< areaArray.length; i++){
                    // subwayArray.push(subways[i]);
                    text +=  "<div class='row'><div class='magb15 col-sm-12' ><h4 class='pull-left'>" + areaArray[i] + "</h4> <div class='pull-right'></div></div><div class='col-sm-12'><ul class='delivery'>";
                    for(j=0; j<result.length; j++){
                      // var tempSubwayLine = resultArray[j].area_kind.split(" ");

                      if(areaArray[i] == result[j]['area_name']){

                        text += "<li><label for='chk" + (j+1) +"'><span class='agree'><input type='checkbox' id='chk" + (j+1) +"' name='place'  value='" + result[j].serial + "'><span class='chk_txt'>" + result[j].location_name_kor +"</span></span></label></li>";
                      }
                    }
                    text += "</ul></div></div><div class='hr-line-dashed'></div>";
                  }

                  $( "#div_place").html(text);
             }

        });
}

//추가 버튼
$("#button_register").click(function() {
    $("#place_input").val('');
  //도시의 구/군을 가져온다.
    $.get("/areamanager/get_gu/"+selected_city,
          function(data){
             var result = JSON.parse(data);
             if(result.length == 0){

             }else{
                var text = "";
                for(var i=0; i< result.length; i++){
                    text += "<option name='select_sub_area' value='" + result[i].gugun + "'>" + result[i].gugun +"</option>";
                }
                $( "#select_place").html(text);

             }

    });

});
//삭제 버튼
$("#button_delete").click(function() {

    var serialArray = Array();
     $("input[name='place']:checked").each(function() {
        var serial = $(this).val();
        serialArray.push(serial);
      });

     if(serialArray.length==0){
      alert('삭제할 항목을 선택해 주세요.');
      return false;
     }

      $.post("/areamanager/delete_special_place",
      {
          area_list : serialArray
      },
      function(data, status){
            // alert(data);
            var result = JSON.parse(data);
            if(result.code=="S01"){
                alert("삭제 되었습니다.");
                get_special_spot(selected_city);
            }else{
                alert(result.message);
            }

      });

});

//저장 버튼
$("#button_save").click(function() {
    // var city_name = $("#select_place option:selected").val();
    var sub_name = $("#select_place option:selected").text();
    var special_name = $("#place_input").val();

     $.post("/areamanager/save_special_place",
      {
          city_name : selected_city,
          sub_name : sub_name, //수정사항 10/3일.
          location_name_kor : special_name
      },
      function(data){
            // alert(data);
                    var result = JSON.parse(data);
                    if(result.code=="S01"){
                        alert("저장 되었습니다.");
                        $('#dialog_areamanager').modal('toggle');
                        get_special_spot(selected_city);
                    }else{
                        alert(result.message);
                    }

      });

});



</script>

