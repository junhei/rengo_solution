<div class="modal fade" id="dialog_areamanager" role="dialog">
	<div class="modal-dialog modal_dialog500">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 id="dialog_title" class="modal-title">지점 등록</h4>
			</div>

			<div class="modal_scroll">
				<div class="modal-body br_ccc">
					<div class="row rent_input">
						<div class="col-sm-12">

							<!-- <div class="row">
								<div class="col-md-12"><h4>기본정보</h4></div>
							</div> -->

<!-- 							<div class="row">
								<div class="col-ms-12">
									<form class="form-horizontal">
										<div class="form-group">
											<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">시/도</label>
											<div class="col-sm-11">
												<select id="select_city">
												</select>
											</div>
										</div>
									</form>
								</div>
							</div> -->

							<div class="row">
								<div class="col-ms-12">
									<form class="form-horizontal">
										<div class="form-group">
											<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">지역</label>
											<div class="col-sm-10">
												<select class="form-control" id="select_place">
	<!-- 											  	<option value="volvo">Volvo</option>
												  	<option value="saab">Saab</option>
												  	<option value="mercedes">Mercedes</option>
											  		<option value="audi">Audi</option> -->
												</select>
											</div>
										</div>
									</form>
								</div>
							</div>

							<div class="row">
								<div class="col-ms-12">
									<form class="form-horizontal">
										<div class="form-group">
											<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">세부지역명</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="place_input" >
											</div>
										</div>
									</form>
								</div>
							</div>
						
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
				<button type="button" class="btn action_btn" id="button_save" >저장</button>
			</div>

		</div>
	</div>
</div>