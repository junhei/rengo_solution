<div class="modal fade" id="dialog_master_manager" role="dialog">
  <div class="modal-dialog modal_dialog500">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 id="" class="modal-title">관리자 수정</h4>
      </div>

      <div class="modal-body">
        <div class="row">
        <input type="hidden" class="form-control" value="" id="serial">
          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 pdr0 pdl0 control-label text-left ">*아이디(이메일)</label>
                <div class="col-sm-10 ">
                  <input type="" class="form-control" value="" id="admin_id" placeholder="이메일 주소를 입력해주세요." maxlength="30">
                </div>
              </div>
            </form>
          </div>

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 pdr0 pdl0 control-label text-left">*비밀번호</label>
                <div class="col-sm-10 ">
                    <a class="bt" role="button" id="change_password_btn" type="button" >변경하기</a>
                    <input type="password" style="display:none;" class="form-control" id="admin_pw" placeholder="6자리이상, 영문과 숫자를 사용하세요." maxlength="20">
                </div>
              </div>
            </form>
          </div>

          <div>
            <form class="form-horizontal">
              <div class="form-group">
                <label for="" class="col-sm-2 pdr0 pdl0 control-label text-left ">이름</label>
                <div class="col-sm-10 ">
                  <input type="" class="form-control" value="" id="admin_name" placeholder="" maxlength="30">
                </div>
              </div>
            </form>
          </div>

        <div>
            <form class="form-horizontal">
                <div class="form-group">
                    <label for="" class="col-sm-2 pdr0 pdl0 control-label text-left ">소속파트너</label>
                    <div class="col-sm-10">
                        <select id="belong_partner" class="form-control">

                        </select>
                    </div>
                </div>
            </form>
        </div>

          <div class="">
            <form class="form-horizontal">
              <div class="form-group magb0">
                <label for="" class="col-sm-2 pdr0 pdl0 control-label text-left pdt0 ">접근권한</label>
                <div class="col-sm-10">
                  <div class="panel panel-default magb0">
                    <div class="panel-body vehicle_info" style="font-size:12px;">
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_rent"><span class="checkbox-material"><span class="check"></span></span> 대여관리</label></div>
                      </div>
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_car"><span class="checkbox-material"><span class="check"></span></span> 차량관리</label></div>
                      </div>
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_customer"><span class="checkbox-material"><span class="check"></span></span> 고객관리</label></div>
                      </div>
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_sales"><span class="checkbox-material"><span class="check"></span></span> 매출관리</label></div>
                      </div>
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_rengo"><span class="checkbox-material"><span class="check"></span></span> 렌고설정</label></div>
                      </div>
                      <div class="col-md-4">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_preference"><span class="checkbox-material"><span class="check"></span></span>환경설정</label></div>
                      </div>
                      <div class="col-md-4 pdr0">
                        <div class="checkbox-inline"><label><input type="checkbox" id="check_master"><span class="checkbox-material"><span class="check"></span></span>마스터관리</label></div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>


        </div>
      </div>

      <div class="modal-footer">
        <!-- <button id="delete_btn" type="button" class="btn mgr7 pull-left" data-dismiss="modal">삭제</button>-->
        <button id="close_btn" type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
        <!-- <button id="save_btn" type="button" class="btn btn-primary">저장</button> -->
        <button id="update_btn" type="button" class="btn btn-primary">수정</button>

      </div>
    </div>
  </div>
</div>