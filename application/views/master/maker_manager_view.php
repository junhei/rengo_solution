<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">


	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
  
	<script src="/telerik/js/jszip.min.js"></script>
	<script src="/telerik/js/kendo.all.min.js"></script>
	<script src="/telerik/examples/content/shared/js/console.js"></script>

<!-- Modal -->
  <div class="modal fade" id="input_dialog" role="dialog">
    <div class="modal-dialog modal_dialog500">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 id="dialog_title" class="modal-title">정보 입력</h4>
        </div>

        <div class="modal-body">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="form-horizontal">
        				<div class="form-group">
							<label for="" class="col-sm-2 control-label text-left">제조사</label>
							<div class="col-sm-10">
								<input id="input_maker" type="text" class="form-control" placeholder="현대, 기아 등등" >
							</div>
						</div>	
					</div>
        		</div>
    		</div>
        	
        	<div class="row">
        		<div class="col-md-12">
        			<div class="form-horizontal">
        				<div class="form-group">
							<label for="" class="col-sm-2 control-label text-left">사용유무</label>
							<div class="col-sm-10">
								<select id="select_status" class="form-control">
									<option>사용</option>
									<option>중지</option>
								</select>
							</div>
						</div>	
					</div>	
        		</div>
        	</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-horizontal">
						<div class="form-group">
							<label for="" class="col-sm-2 control-label text-left">국산,수입</label>
							<div class="col-sm-10">
								<select id="select_country" class="form-control">
									<option>국산</option>
									<option>수입</option>
								</select>
							</div>
						</div>		
					</div>
				</div>
			</div>
			
		</div>

        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
          <button id="btn_change" type="button" class="btn action_btn">저장</button>
        </div>
      </div>
      
    </div>
  </div>


	<!--content-->
			<div class="panel border_gray magb0 bb0">
				<div class="panel-heading bg_fff bb_ccc">
					<div class="row">
						<div class="col-md-9">
							<h3 class="mgt20 mgb10">제조사 관리</h3>
						</div>
					</div>
				</div>

		      <div class="panel-heading bb0">
					<div class="row">
						<div class="col-md-5">
						 	<button id="btn_register" type="button" class="btn action_btn" data-toggle="modal" data-target="#input_dialog">등록</button>
						    <!-- <button id="btn_modify" type="button" class="btn btn-default" data-toggle="modal" data-target="#input_dialog" style="display:none;">수정</button> -->
<!-- 						    <button id="btn_delete"  type="button" class="btn btn-default">삭제</button> -->
						</div>
					</div>
				</div>
			</div>
	


	<div id="">
		<div id="grid"></div>
		<script>


$('.modal').on('hidden.bs.modal',function() {
  $('.modal_scroll').scrollTop(0);
  setTimeout(function(){
    $('.modal').css({
      'top':'0',
      'left':'0'
    });

    },30)
})

$('.modal').draggable();





			$(document).ready(function() {
				$("#grid").kendoGrid({
					// navigatable: true,  //키보드로 표 조작 할수 있게
					// reorderable: true, //사용자가 목록 자기맘에드는대로 순서 바꿀수 있게
					resizable: true, //사용자가 컬럼 크기 맘대로 바꿀수 있게
					// selectable: "multiple, row", //선택할수 있도록
					allowCopy: true, //값 copyrksm
					height: 600, //높이????
					sortable: true, //정렬가능하도록
					// filterable: true, //필터(비교해서 정렬)가능하도록
					selectable: "row", //선택할수 있도록
					excel: {
						allPages: true,
       					fileName: "제조사리스트.xlsx", //excel로 저장할 이름,
       					filterable: true
       				},
       				pageable: {
       					input: true,
       					messages: {
       						display: "총 {2}개의 제조사 중 {0}-{1}번째",
       						empty: "No data"
       					}
       				},
       				noRecords: {
       					template: "현재 페이지에서 보여줄 내용이 없습니다."
       				},
       				// columnMenu: {
       				// 	sortable: false,
       				// 	messages: {
       				// 		columns: "표시할 항목 선택",
       				// 		filter: "필터",
       				// 	}
       				// },
       				// filterable: {
       				// 	operators: {
       				// 		string: {
       				// 			eq: "같음",
       				// 			contains: "포함됨",
       				// 			startswith: "시작됨",
       				// 			endswith: "끝남"
       				// 		},
       				// 		number: {
       				// 			eq: "같음",
       				// 			gte: "크거나 같음",
       				// 			gt: "큼",
       				// 			lte: "작거타 같음",
       				// 			lt: "작음",
       				// 		},
       				// 		date: {
       				// 			eq: "같음",
       				// 			gte: "(포함)이후",
       				// 			lte: "(포함)이전"
       				// 		}
       				// 	}},
       				dataSource: {
       					// type: "odata",
			        	transport: {
							    read: {
							      url: "http://solution.rengo.co.kr/makermanager/get_list",
							      dataType: "json"
							    }
					   },

					   // data : products,
					   schema: {
					   	model: {
					   		fields: {
					   			index: { type: "number" },
					   			name: { type: "string" },
					   			status: { type: "string" },
					   			car_count: { type: "number" },
					   			country: { type: "string" }
					   				}
					   		}		
					   	},
					   	pageSize: 30
									 // serverPaging: true,
									 // serverFiltering: true,
									 // serverSorting: true
					},
					// toolbar: [
					//  { template: '<a class="k-button" href="\\#" onclick="return register()">등록</a>'},
					//  { template: '<a class="k-button" href="\\#" onclick="return modify()">수정</a>'},
					//  { template: '<a class="k-button" href="\\#" onclick="return remove()">삭제</a>'},
					//  ],
					columns: [ 
						{
							field: "name",
							title: "제조사명",

						}, 
						{
							field: "status",
							title: "사용상태",
						}, 
						{
							field: "country",
							title: "국가",
						}, 
						{
							field: "car_count",
							title: "차량 등록수",
						},
           			]
           		});

           		 fitGridSize();
			});

// //표 가로길이 자동설정
// var grid = $("#grid").data("kendoGrid");
// for (var i = 0; i < grid.columns.length; i++) {
//   grid.autoFitColumn(i);// displays "name" and then "age"
// }


// function savePdf(){
//  	var grid = $("#grid").data("kendoGrid");
//    grid.saveAsPDF();
// }

var grid = $("#grid").data("kendoGrid");
var btn_register = $("#btn_register");
        // var btn_modify =  $("#btn_modify");
        // var btn_delete =  $("#btn_delete");
var btn_change = $("#btn_change");
var text_dialog_title = $('#dialog_title');
var is_modify_mode = false;


$(document).ready(function() {
		//등록버튼
	
     

        btn_register.click(function() {
           	is_modify_mode = false;
           	text_dialog_title.text("등록");
           	btn_change.text("등록하기");
           //값 초기화
           	$('#input_maker').val('');
			$("#select_status option:eq(0)").attr("selected", "selected");
			$("#select_country option:eq(0)").attr("selected", "selected");
        });


        // btn_modify.click(function() {
        	
        // });


   //      btn_delete.click(function() {
   //      	var row = grid.select();
			// var data = grid.dataItem(row);
			// if(data==null){
			// 	alert("삭제할 제조사를 선택해 주세요.");
			// 	return false;
			// }


   //      	$.post("http://solution.rengo.co.kr/makermanager/delete/",
   //      	{
   //      			index : data.index
			// },
			// function(data, status){
			// 		var result = JSON.parse(data);
			// 		if(result.code=="S01"){
			// 			alert("삭제되었습니다.");
			// 			//데이터 다시 읽어옴
			// 			grid_refresh();
			// 		}else{
			// 			alert("데이터 삭제 중 에러가 발생했습니다.");
			// 		}

			// });

            
   //      });


        btn_change.click(function(){
        	var grid = $("#grid").data("kendoGrid");
        	var title = text_dialog_title.text();
        	var maker = $('#input_maker').val();
        	var status = $('#select_status option:selected').val();
        	var country =  $('#select_country option:selected').val();
        		// alert("메이커:"+ maker +", 상태:" + status);
        	if(is_modify_mode == false){
        		$.post("http://solution.rengo.co.kr/makermanager/register/",
        		{
					name : maker,
					status : status,
					country : country
				},
				function(data, status){
					var result = JSON.parse(data);
					if(result.code=="S01"){
						alert("등록되었습니다.");
						//데이터 다시 읽어옴
						// grid.refresh();
						grid_refresh();

						$("[data-dismiss=modal]").trigger({ type: "click" });
						// $('.modal-backdrop').toggle();
					}else if(result.code=="E02"){//이미 있는 경우
						alert("이미 등록 된 제조사 입니다.");
					}else{
						alert("데이터 입력 중 문제가 발생했습니다. 다시 시도해 주세요.");
					}

				});
        	}else{

        		//선택한것의 정보 읽어오기
        		var row = grid.select();
				var data = grid.dataItem(row);

        		$.post("http://solution.rengo.co.kr/makermanager/update/",
        		{
        			index : data.index,
					name : maker,
					status : status,
					country : country
				},
				function(data, status){
					var result = JSON.parse(data);
					if(result.code=="S01"){
						alert("수정되었습니다.");
						//데이터 다시 읽어옴
						// grid.refresh();
						grid_refresh();

						$("[data-dismiss=modal]").trigger({ type: "click" });
					}else{
						alert("데이터 수정 중 에러가 발생했습니다.");
					}

				});
        	}
        });
      
});

function grid_refresh(){
	var dataSource = new kendo.data.DataSource({
 		 transport: {
  		  read: {
   		   url: "http://solution.rengo.co.kr/makermanager/get_list/",
 		   dataType: "json"
 		   }
	  	},
	  	pageSize: 30
	});
	var grid = $("#grid").data("kendoGrid");
	grid.setDataSource(dataSource);
}

$("#grid").delegate("tbody>tr", "dblclick",
  function(){
       // $("#btn_modify").trigger("click");
       $('#input_dialog').modal('toggle'); //다이얼로그 출력
       is_modify_mode = true;
        	//선택한것의 정보 읽어오기
        	var grid = $("#grid").data("kendoGrid");
        	var row = grid.select();
			var data = grid.dataItem(row);
			if(data==null){
				alert("수정할 제조사를 선택해 주세요.");
				return false;
			}

        	text_dialog_title.text("수정");
        	btn_change.text("수정하기");


			//읽어온값 대입
			$('#input_maker').val(data.name);
			$("#select_status").val(data.status).attr("selected", "selected");
			$("#select_country").val(data.country).attr("selected", "selected");

});

function fitGridSize() {
	var gridElement = $("#grid");
    var gird_position = $('#grid').position();
    var window_height = $( window ).height() - gird_position.top - 50;
    gridElement.children(".k-grid-content").height(window_height-100);
    gridElement.height(window_height);
}

$(window).resize(function() {
    fitGridSize();
});
   
</script>
</div>
