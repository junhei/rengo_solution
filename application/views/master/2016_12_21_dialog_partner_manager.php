<!-- Modal -->
<div class="modal fade" id="dialog_partner_manager" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 id="dialog_title" class="modal-title">파트너 정보</h4>
			</div>
			<div class="modal-body">
				<div class="row rent_input">
					<!--왼쪽 -->
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12"><h4>기본정보</h4></div>
						</div>
						<div class="row pdt15">
							<div class="col-md-6">
								<form class="form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">파트너 번호</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="partner_serial" value="" disabled>
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-6">
								<form class="form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">아이디</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="partner_id" value="">
										</div>
									</div>
								</form>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<form class="form-horizontal">
									<div class="form-group magb0">
										<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">업체명</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="partner_name" value="">
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-6">
								<div class="form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">사용유무</label>
										<div class="col-sm-10">
											<label class="radio-inline">
								  				<input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="사용중" readonly="true"> 사용중
											</label>
											<label class="radio-inline">
										  		<input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="사용하지 않음" readonly="true"> 사용하지 않음
											</label>
											<label class="radio-inline">
										  		<input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="승인 대기중" readonly="true"> 승인 대기중
											</label>
											<button class="btn btn-default" id="btn_send_message">승인메일보내기</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<form class="form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">대표자이름</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="owner_name" value="" >
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-6">
								<form class="form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">본사소재지</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="company_address" value="">
										</div>
									</div>
								</form>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<form class="form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">사업자등록번호</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="register_number" name="">
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-6">
								<form class="form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">전화번호</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="tel" value="">
										</div>
									</div>
								</form>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<form class="form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">FAX</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="fax" value="">
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-6">
								<form class="form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">이메일</label>
										<div class="col-sm-10">
											<input type="" class="form-control" id="email" value="">
										</div>
									</div>
								</form>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<form class="form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">주소</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="office_address" value="">
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-6">
								<form class="form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">주소간단설명</label>
										<div class="col-sm-10">
											<input type="" class="form-control" id="address_comment" value="">
										</div>
									</div>
								</form>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<form class="form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">영업시간</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="" value="">
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-6">
								<form class="form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">영업시간</label>
										<div class="col-sm-10">
											<input type="" class="form-control" id="" value="">
										</div>
									</div>
								</form>
							</div>
						</div>

					</div>
				</div>
			</div>
			
			<div class="modal-body bt_ccc">
				<div class="row">
					<div class="col-md-12"><h4>서비스 지역</h4></div>
				</div>
				<div class="row pdt15">
					<div class="col-md-12">
						<form class="form-horizontal">
							<div class="form-group">
								<!-- <label for="" class="col-sm-2 control-label text-left pdl0 pdr0">서비스 지역</label> -->
								<div class="col-sm-12">
									<textarea type="" id="service_area" class="form-control autosize" style="height:auto;" rows="15" disabled></textarea>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>	

			<!-- <div class="modal-body bt_ccc">
				<div class="row">
					<div class="col-md-12"><h4>대여자격 및 준수사항</h4></div>
				</div>
				<div class="row pdt15">
					<div class="col-md-12">
						<form class="form-horizontal">
							<div class="form-group">
								<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">대여 자격</label>
								<div class="col-sm-10">
									<textarea type="" id="car_input_insurance_text" class="form-control" value="" rows="3"></textarea>
								</div>
							</div>
						</form>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<form class="form-horizontal">
							<div class="form-group">
								<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">임차인 준수사항</label>
								<div class="col-sm-10">
									<textarea type="" id="car_input_insurance_text" class="form-control" value="" rows="3"></textarea>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>	 -->

			<div class="modal-footer">
				<!-- <button type="button" class="btn btn-danger pull-left" id="user_delete">삭제</button> -->
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
				<button type="button" class="btn btn-primary" data-toggle="modal" id="btn_register">저장</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
// $.get("/partnermanager/get_partner_information/",
// 	function(data){
// 	    var result = JSON.parse(data);
// 	    $('#partner_serial').val(result[0].serial);	
// });
// $(function() {
//   $("textarea.autosize").keyup(function () {
//     $(this).css("height","1px").css("height",(20+$(this).prop("scrollHeight"))+"px");
//   });
// });

// var grid = $('#grid').data('kendoGrid'); 
// var row = grid.select();
// var data = grid.dataItem(row);

// $.get('/partnermanager/register_btn_flag/'+data.tel,
// 	function(data)
// 	{
// 		var result=JSON.parse(data);
// 		// if(result[0].flag=='N'){

// 		// }
// 		alert(result[0].flag);
// 	})



$('#btn_send_message').click(function() {

	var value = $(':radio[name="inlineRadioOptions"]:checked').val();
	// alert(value);
	if(value == "승인 대기중"){
		var partner_serial = $('#partner_serial').val();
		if(partner_serial!=''){
			$.post("/partnermanager/send_confirm_mail",
				{
					partner_serial : partner_serial
				},
				function(data)
				{
					var result = JSON.parse(data);
					if(result.code=="S01"){
						alert("메일을 보냈습니다.");
					} else {
						alert(result.message);
					}
				});	
		}else{
			alert('파트너 번호 없음');
		}
	}else{
		alert('승인 대기중인 상태에서만 메일을 보낼수 있습니다.');
	}
	
	
});



$('#btn_register').click(function() {
	var input_test=true;
	var partner_serial=$('#partner_serial');
	var partner_id=$('#partner_id');
	var partner_name=$('#partner_name');
	var owner_name=$('#owner_name');
	var company_address=$('#company_address');
	var register_number=$('#register_number');
	var tel=$('#tel');
	var fax=$('#fax');
	var email=$('#email');
	var office_address=$('#office_address');
	var address_comment=$('#address_comment');

	var input_confirm=[
		partner_id, partner_name, owner_name, company_address,
		register_number, tel, fax, email, office_address, address_comment
		];

	var input_name = {
		'partner_id' : '아이디',
		'partner_name' : '업체명',
		'owner_name' : '대표자이름',
		'company_address' : '본사소재지',
		'register_number' : '사업자등록번호',
		'tel' : '전화번호',
		'fax' : '팩스',
		'email' : '이메일',
		'office_address' : '주소',
		'address_comment' : '주소간단설명'
	};

	for(var i=0; i<input_confirm.length;++i){
		var id_name=input_confirm[i].attr('id');
		if(input_confirm[i].val()==''){
			alert(input_name[id_name]+'의 값을 입력해주세요.');
	      	input_confirm[i].focus();
    	  	input_test=false;
	      	break;
		}
	}

	if(input_test){
		$.post("/partnermanager/register_partner_information/"+partner_serial.val(),
			{
				id : partner_id.val(),
				partner_name : partner_name.val(),
				owner_name : owner_name.val(),
				company_address : company_address.val(),
				register_number : register_number.val(),
				tel : tel.val(),
				fax : fax.val(),
				email : email.val(),
				office_address : office_address.val(),
				address_comment : address_comment.val()
			},
			function(data,status)
			{
				var result = JSON.parse(data);
				if(result.code=="S01"){
					alert("등록되었습니다.");
					$('#dialog_partner_manager').modal('hide');
					$('#grid').data('kendoGrid').dataSource.read();
					$('#grid').refresh();
				} else {
					alert(result.message);
				}
			});		
	}
	
})
</script>