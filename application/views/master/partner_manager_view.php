<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>
<script src="/telerik/examples/content/shared/js/console.js"></script>

<?php
    require("/home/apache/CodeIgniter-3.0.6/application/views/master/dialog_partner_manager.php");
?>
<div class="row" style="margin-right:-10px;">
    <div class="col-md-12">
        <div class="panel border_gray">
            <div class="panel-heading bb_ccc">
                <div class="row" >
                    <div class="col-md-9">
                        <h3 class="mgt20 mgb10">파트너 관리</h3>
                    </div>
                </div>
            </div>

            <div class="panel-heading">
                <div class="col-md-9 pdl0">
                </div>
                <div class="input-group custom-search-form">
                    <input id="searchbox" type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </div>

            <div class="panel-footer bg_fff">
                <div id="example" style="margin:0;">
                    <div id="grid"></div>
                </div>
            </div>
        </div>
    </div>
</div>
    

    <script> 


$('.modal').on('hidden.bs.modal',function() {
  $('.modal_scroll').scrollTop(0);
  setTimeout(function(){
    $('.modal').css({
      'top':'0',
      'left':'0'
    });

    },30)
})

$('.modal').draggable();

    var selected_company_serial = 0;

        var gird_position = $('#grid').position();
        var window_height = $( window ).height() - gird_position.top - 70;
    
        $("#grid").kendoGrid({
        	navigatable: true,
		    selectable: true,
			allowCopy: true, 
  			height: window_height, 
            sortable: true, 
            filterable: true, 
            groupable: false, 
            resizable: true,
            pageable: {
                input: true,
                messages: {
                    display: "Showing {0}-{1} from {2} data items",
                    empty: "No data"
                }
			  },
            noRecords: {
                template: "현재 페이지에서 보여줄 내용이 없습니다."
              },
            columnMenu: {
                sortable: false,
                messages: {
                    columns: "표시할 항목 선택",
                    filter: "필터",
                }
             },
            dataSource: {
                transport: {
		             read: {
				       url: "/partnermanager/get_partner_information",
				       dataType: "json"
				     }
				   },
                schema: {
                    model: {
                        fields: {
                            serial: { type: "number" },
                            company_name: {type:"string"},
                            id: {type:"string"},
                            tel: { type: "string" }, 
                            phone_option1: { type: "string" },
                            phone_option2 : {type : "string"}, 
                            // service_area: { type: "string" }, 
                            email: { type: "string" },
                            status : { type: "string" },
                        }
                    }
                },
                pageSize: 30
            },
           
            columns: [ {
                    field: "serial",
                    title: "번호",
                    // locked: true,
                    width: 80
                },  {
                    field: "company_name",
                    title: "업체명",
                    // locked: true,
                    width: 100
                },  
                {
                    field: "email",
                    title: "파트너 아이디",
                    width: 150
                },
                {
                    field: "status",
                    title: "상태",
                    width: 100
                }, 
                 {
                    field: "tel",
                    title: "대표 연락처",
                    width: 100
                },  {
                    field: "phone_option1",
                    title: "담당 연락처",
                    width: 100
                }, 
                //  {
                //     field: "phone_option2",
                //     title: "담당 연락처2",
                //     width: 100
                // },  
                {
                    field: "service_time",
                    title: "영업시간",
                    width: 150
                }
            ]
        });

$(window).resize(function() {
var gridElement = $("#grid");
var gird_position = $('#grid').position();
var window_height = $( window ).height() - gird_position.top - 50;
gridElement.children(".k-grid-content").height(window_height-100);
gridElement.height(window_height);
});

var id; //dbclick function 안에 넣으면 스택이 쌓이는지 누른횟수 만큼 실행되서 안 됨.
// $('#btn_submit').click(function(){
//     $.get('/partnermanager/register_submit/'+id,
//     function(data)
//     {
//         var result = JSON.parse(data);
//         if(result.code=="S01"){
//             alert("처리되었습니다.");
//             $('#btn_submit').hide();          
//         }else{
//             alert("처리 중 에러가 발생했습니다.");
//         }
//     });
// })

$("#grid").delegate("tbody>tr", "dblclick", function(){

var grid = $('#grid').data('kendoGrid'); 
var row = grid.select();
var data = grid.dataItem(row);
id = data.id;

selected_company_serial = data.serial;

// open_in_frame("http://solution.rengo.co.kr/partner_dialog/partnersetting/" + selected_company_serial);
$('#dialog_partner_manager').modal('toggle');
//첫번째 탭 강제 클릭
$('#tab_partnersetting').trigger('click'); 
});

$("#searchbox").keyup(function () {
var val = $('#searchbox').val();
$("#grid").data("kendoGrid").dataSource.filter({
    logic: "or",
    filters: [
        // {
        //     field: "serial",
        //     operator: "contains",
        //     value: val
        // },
        {
            field: "company_name",
            operator: "contains",
            value: val
        },
        {
            field: "id",
            operator: "contains",
            value:val
        },
        {
            field: "tel",
            operator: "contains",
            value:val
        },
        {
            field: "phone_option1",
            operator: "contains",
            value:val
        },
        {
            field: "phone_option2",
            operator: "contains",
            value:val
        },
        {
            field: "email",
            operator: "contains",
            value:val
        },               
    ]
});
});
</script>
