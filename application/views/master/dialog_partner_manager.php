<!-- Modal -->
<div class="modal fade" id="dialog_partner_manager" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 id="dialog_title" class="modal-title">파트너 정보</h4>
			</div>

			<div class="panel-footer section_lnb bt0">
				<ul class="nav nav-tabs">
			        <li class="active"><a data-toggle="tab" href="" id="tab_partnersetting" aria-expanded="true">파트너 설정</a></li>
			        <li class=""><a data-toggle="tab" href="" id="tab_salesetting" aria-expanded="false">판매설정</a></li>
			        <li class=""><a data-toggle="tab" href="" id="tab_areasetting" aria-expanded="false">지역설정</a></li>
			        <li class=""><a data-toggle="tab" href="" id="tab_staffmanager" aria-expanded="false">직원관리</a></li>
			    </ul>
			</div>


			
			<div class="modal-body">
				<iframe id="dialog_frame" src="http://solution.rengo.co.kr/partner_dialog" frameborder="0" style="width:100%; height:auto;" class="modal_scroll"></iframe>
			</div>	

			<div class="modal-footer">
				<button type="button" onclick="$('.modal_scroll').scrollTop(0);" class="btn btn-default" data-dismiss="modal">닫기</button>
				
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	 $(document).ready(function(){
	 $('.modal_scroll').css('height', $(window).height() - 300 );  

}); 
</script>

<script type="text/javascript">


$("#tab_partnersetting").click(function() {
		open_in_frame("http://solution.rengo.co.kr/partner_dialog/partnersetting/" + selected_company_serial);
});
$("#tab_salesetting").click(function() {
		open_in_frame("http://solution.rengo.co.kr/partner_dialog/salesetting/"+ selected_company_serial);
});
$("#tab_areasetting").click(function() {
		open_in_frame("http://solution.rengo.co.kr/partner_dialog/areasetting/"+ selected_company_serial);
});
$("#tab_staffmanager").click(function() {
		open_in_frame("http://solution.rengo.co.kr/partner_dialog/staffmanager/"+ selected_company_serial);
});

function open_in_frame(url) {
	$('#dialog_frame').attr('src', url);
}

// $.get("/partnermanager/get_partner_information/",
// 	function(data){
// 	    var result = JSON.parse(data);
// 	    $('#partner_serial').val(result[0].serial);	
// });
// $(function() {
//   $("textarea.autosize").keyup(function () {
//     $(this).css("height","1px").css("height",(20+$(this).prop("scrollHeight"))+"px");
//   });
// });

// var grid = $('#grid').data('kendoGrid'); 
// var row = grid.select();
// var data = grid.dataItem(row);

// $.get('/partnermanager/register_btn_flag/'+data.tel,
// 	function(data)
// 	{
// 		var result=JSON.parse(data);
// 		// if(result[0].flag=='N'){

// 		// }
// 		alert(result[0].flag);
// 	})



$('#btn_send_message').click(function() {

	var value = $(':radio[name="inlineRadioOptions"]:checked').val();
	// alert(value);
	if(value == "승인 대기중"){
		var partner_serial = $('#partner_serial').val();
		if(partner_serial!=''){
			$.post("/partnermanager/send_confirm_mail",
				{
					partner_serial : partner_serial
				},
				function(data)
				{
					var result = JSON.parse(data);
					if(result.code=="S01"){
						alert("메일을 보냈습니다.");
					} else {
						alert(result.message);
					}
				});	
		}else{
			alert('파트너 번호 없음');
		}
	}else{
		alert('승인 대기중인 상태에서만 메일을 보낼수 있습니다.');
	}
	
	
});



$('#btn_register').click(function() {
	var input_test=true;
	var partner_serial=$('#partner_serial');
	var partner_id=$('#partner_id');
	var partner_name=$('#partner_name');
	var owner_name=$('#owner_name');
	var company_address=$('#company_address');
	var register_number=$('#register_number');
	var tel=$('#tel');
	var fax=$('#fax');
	var email=$('#email');
	var office_address=$('#office_address');
	var address_comment=$('#address_comment');

	var input_confirm=[
		partner_id, partner_name, owner_name, company_address,
		register_number, tel, fax, email, office_address, address_comment
		];

	var input_name = {
		'partner_id' : '아이디',
		'partner_name' : '업체명',
		'owner_name' : '대표자이름',
		'company_address' : '본사소재지',
		'register_number' : '사업자등록번호',
		'tel' : '전화번호',
		'fax' : '팩스',
		'email' : '이메일',
		'office_address' : '주소',
		'address_comment' : '주소간단설명'
	};

	for(var i=0; i<input_confirm.length;++i){
		var id_name=input_confirm[i].attr('id');
		if(input_confirm[i].val()==''){
			alert(input_name[id_name]+'의 값을 입력해주세요.');
	      	input_confirm[i].focus();
    	  	input_test=false;
	      	break;
		}
	}

	if(input_test){
		$.post("/partnermanager/register_partner_information/"+partner_serial.val(),
			{
				id : partner_id.val(),
				partner_name : partner_name.val(),
				owner_name : owner_name.val(),
				company_address : company_address.val(),
				register_number : register_number.val(),
				tel : tel.val(),
				fax : fax.val(),
				email : email.val(),
				office_address : office_address.val(),
				address_comment : address_comment.val()
			},
			function(data,status)
			{
				var result = JSON.parse(data);
				if(result.code=="S01"){
					alert("등록되었습니다.");
					$('#dialog_partner_manager').modal('hide');
					$('#grid').data('kendoGrid').dataSource.read();
					$('#grid').refresh();
				} else {
					alert(result.message);
				}
			});		
	}
	
})
</script>