	<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
	<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
	<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
	<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
	<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
	<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
	<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">
	<link href="/css/rengo_login.css" rel="stylesheet">

	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

	<script src="/telerik/js/jszip.min.js"></script>
	<script src="/telerik/js/kendo.all.min.js"></script>
	<script src="/telerik/examples/content/shared/js/console.js"></script>

	<link href="/css/material_design/bootstrap-material-design.css" rel="stylesheet">
	<link href="/css/material_design/ripples.min.css" rel="stylesheet">
	<script src="/js/material_design/ripples.min.js"></script>
	<script src="/js/material_design/material.min.js"></script>

<!-- Modal -->
  <div class="modal fade" id="input_dialog" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
      <form method="post" action="/carmaster/file_upload" enctype="multipart/form-data" id="carmaster_form">
      	
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 id="dialog_title" class="modal-title">차종 등록</h4>
        </div>
<!-- aws s3 cp /home/apache/uploads/company/1908/business_regist.png s3://private.information.rengo/1908/a.png<br /> -->
 
        <div class="modal-body">

					<div class="row">
						<div class="col-sm-12 form-horizontal">
								<div class="form-group">
									<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">제조사</label>
									<div class="col-sm-11">
										<select id="maker_index" class="form-control">
											<?php foreach($makermanager_list as $data):?>
												<option value="<?php echo $data['maker_index']; ?>"> <?php echo $data['maker_name']; ?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div>
						</div>
						<div class="col-sm-12 form-horizontal">
								<div class="form-group">
									<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">차종</label>
									<div class="col-sm-11">
										<select class="form-control" id = "car_type">
											<option>경형</option>
											<option>소형</option>
											<option>준중형</option>
											<option>중형</option>
											<option>대형</option>
											<option>SUV</option>
											<option>승합</option>
											<option>수입</option>
										</select>
									</div>
								</div>
						</div>

                     

						<div class="col-sm-12 form-horizontal">
								<div class="form-group">
									<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">모델명</label>
									<div class="col-sm-11">
										<input id="car_name" type="text" class="form-control">
									</div>
								</div>
						</div>
						<div class="col-sm-12 form-horizontal">
								<div class="form-group">
									<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">세부모델명</label>
									<div class="col-sm-11">
										<input id="car_name_detail" type="text" class="form-control">
									</div>
								</div>
						</div>
						<div class="col-sm-12 form-horizontal">
								<div class="form-group">
									<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">표준대여금액</label>
									<div class="col-sm-11 box_txt">
										<input type="number" class="form-control" id="car_normal_price" placeholder="롯데렌탈기준 표준금액을 입력하세요."  aria-describedby="inputSuccess3Status">
							      <span class="form-control-feedback" aria-hidden="true">원</span>
									</div>
								</div>
						</div>
<!-- 						<div class="col-sm-12 form-horizontal">
								<div class="form-group">
									<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">승차인원수</label>
									<div class="col-sm-11 box_txt">
										<input id="car_people" type="number" class="form-control" aria-describedby="inputSuccess3Status">
							      <span class="form-control-feedback" aria-hidden="true">인승</span>
									</div>
								</div>
						</div> -->
					
<!-- 							<div class="col-md-12">
										<div class="form-group">
										<label for="" class="col-sm-1 control-label text-left pdr0">연료</label>
										<div class="col-sm-11">
											<div class="check_box text-left pdt15 pdb15">
												<div class="row" style="font-size:11px;">
														<div class="col-md-4">
															<label>
							                  <input type="checkbox" id="car_checkbox_gasoline" name="car_checkbox"><span class="checkbox-material"><span class="check"></span></span> 가솔린
							                </label>
														</div>
														<div class="col-md-4">
															<label>
							                  <input type="checkbox" id="car_checkbox_diesel" name="car_checkbox"><span class="checkbox-material"><span class="check"></span></span> 디젤
							                </label>
														</div>
														<div class="col-md-4">
															<label>
							                  <input type="checkbox" id="car_checkbox_lpg" name="car_checkbox"><span class="checkbox-material"><span class="check"></span></span> LPG
							                </label>
														</div>
														<div class="col-md-4">
															<label>
							                  <input type="checkbox" id="car_checkbox_hybrid" name="car_checkbox"><span class="checkbox-material"><span class="check"></span></span> 하이브리드
							                </label>
														</div>
														<div class="col-md-4">
															<label>
							                  <input type="checkbox" id="car_checkbox_electricity" name="car_checkbox"><span class="checkbox-material"><span class="check"></span></span> 전기
							                </label>
														</div>
												</div>
											</div>
										</div>
									</div>
								</fo
							</div> -->
							<div class="col-sm-12 form-horizontal">
								<div class="form-group">
										<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">사용여부</label>
										<div class="col-sm-11">
											<select id="select_status" class="form-control">
												<option>사용</option>
												<option>중지</option>
											</select>
										</div>
									</div>
								</div>

						<div class="col-sm-12 form-horizontal">
							<div class="form-group">
								<label class="col-sm-1 control-label text-left pdl0 pdr0">리스트 이미지</label>
	                           	<div class="col-sm-11 filebox">
									<div class="col-sm-11 pdl0">
										<input class="form-control upload-name" placeholder="리스트 이미지를 첨부해주세요." id="car_list_image_txt" readonly value="">
										<input type="file" id="car_list_image" name="car_list_image" class="upload-hidden">
	                                </div>
	                                <div class="col-sm-1 pdr0 pdl0">
	                                 		<label class="bt" for="car_list_image">첨부하기</label>
	                                </div>
	                            </div> 
	                        </div>
                        </div>

                        <div class="col-sm-12 form-horizontal">
								<div class="form-group">
		                            <label class="col-sm-1 control-label text-left pdl0 pdr0">상세 이미지</label>
		                           	<div class="col-sm-11 filebox">
		                                <div class="col-sm-11 pdl0">
		                                    <input class="form-control upload-name" placeholder="상세 이미지를 첨부해주세요." id="car_detail_image_txt" readonly value="">
		                                    <input type="file" id="car_detail_image" name="car_detail_image" class="upload-hidden">
		                                </div>
		                                <div class="col-sm-1 pdr0 pdl0">
		       
		                                    <label class="bt" for="car_detail_image">첨부하기</label>
		                                </div>
		                            </div> 
		                        </div>
                        </div>
					</div>
					<input type="hidden" id="car_serial" name="car_serial" >
        </div>

        <div class="modal-footer">
       		<!-- <button id="delete_btn" type="button" class="btn mgr7 pull-left">삭제</button>     -->
        	<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
          	<button id="btn_change" type="button" class="btn btn-primary">저장</button>
        </div>
      </div>
      </form>
    </div>
  </div>


	<!--content-->
	
			<div class="panel border_gray magb0">
				<div class="panel-heading bg_fff bb_ccc">
					<div class="row">
						<div class="col-md-9">
							<h3 class="mgt20 mgb10">차종 관리</h3>
						</div>
					</div>
				</div>
				<div class="panel-heading">
						<div class="col-md-9 pdl0">
						 	<button id="btn_register" type="button" class="btn action_btn" data-toggle="modal" data-target="#input_dialog">등록</button>
						    <!-- <button id="btn_modify" type="button" class="btn btn-default" data-toggle="modal" data-target="#input_dialog">수정</button> -->
						    <!-- <button id="btn_delete"  type="button" class="btn btn-default">삭제</button> -->
						</div>
						<div class="input-group custom-search-form">
							<input id="searchbox" type="text" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div>
				</div>
			</div>
	


	<div id="">
		<div id="grid"></div>
		<script>

// //표 가로길이 자동설정
// var grid = $("#grid").data("kendoGrid");
// for (var i = 0; i < grid.columns.length; i++) {
//   grid.autoFitColumn(i);// displays "name" and then "age"
// }


// function savePdf(){
//  	var grid = $("#grid").data("kendoGrid");
//    grid.saveAsPDF();
// }


var is_register = true; //등록인지 수정인지
	//등록버튼
var grid = $("#grid").data("kendoGrid");
var btn_register = $("#btn_register");
        // var btn_modify =  $("#btn_modify");
var text_dialog_title = $('#dialog_title');
var btn_delete =  $("#btn_delete");
var btn_change = $("#btn_change");
var btn_check_history = $("#btn_change");
var text_dialog_title = $('#dialog_title');

$('.modal').on('hidden.bs.modal',function() {
  $('.modal_scroll').scrollTop(0);
  setTimeout(function(){
    $('.modal').css({
      'top':'0',
      'left':'0'
    });

    },30)
})


function fitGridSize() {
	var gridElement = $("#grid");
    var gird_position = $('#grid').position();
    var window_height = $( window ).height() - gird_position.top - 50;
    gridElement.children(".k-grid-content").height(window_height-100);
    gridElement.height(window_height);
}

$(window).resize(function() {
    fitGridSize();
});


$(document).ready(function() {


 var fileTarget = $('.filebox .upload-hidden');

 fileTarget.on('change', function(){
     if(window.FileReader){
         var filename = $(this)[0].files[0].name;
     } else {
         var filename = $(this).val().split('/').pop().split('\\').pop();
     }

     $(this).siblings('.upload-name').val(filename);
 });

		$("#grid").kendoGrid({
					// navigatable: true,  //키보드로 표 조작 할수 있게
					// reorderable: true, //사용자가 목록 자기맘에드는대로 순서 바꿀수 있게
					resizable: true, //사용자가 컬럼 크기 맘대로 바꿀수 있게
					// selectable: "multiple, row", //선택할수 있도록
					// allowCopy: true, //값 copyrksm
					height: 900, //높이????
					sortable: true, //정렬가능하도록
					// filterable: true, //필터(비교해서 정렬)가능하도록
					selectable: "row", //선택할수 있도록
					excel: {
						allPages: true,
       					fileName: "제조사리스트.xlsx", //excel로 저장할 이름,
       					filterable: true
       				},
       				pageable: {
       					// input: true,f
       					messages: {
       						display: "총 {2}개의 차종 중  {0}-{1} 번째",
       						empty: "No data"
       					}
       				},
       				noRecords: {
       					template: "현재 페이지에서 보여줄 내용이 없습니다."
       				},

       				dataSource: {

			        	transport: {
							    read: {
							      url: "http://solution.rengo.co.kr/carmaster/get_list",
							      dataType: "json"
							    }
					   },
					   schema: {
					   	model: {
					   		fields: {
					   			car_index: { type: "number" },
					   			maker_name: { type: "string" },
					   			car_list_image_file: { type: "string" },
					   			car_detail_image_file: { type: "string" },
					   			car_type: { type: "string" },
					   			car_name: { type: "string" },
					   			car_name_detail: { type: "string" },
					   			// car_people: { type: "number" },
					   			car_normal_price: { type: "number" },
					   			// fuel_option_gasoline: { type: "string" },
					   			// fuel_option_diesel: { type: "string" },
					   			// fuel_option_lpg: { type: "string" },
					   			// fuel_option_hybrid: { type: "string" },
					   			// fuel_option_electricity: { type: "string" },
					   			status: { type: "string" }
					   			}
					   		}
					   	},
					   	pageSize: 20
									 // serverPaging: true,
									 // serverFiltering: true,
									 // serverSorting: true
					},
					columns: [
						{
							field: "maker_name",
							title: "제조사명",
							width : 80

						},
						{
							field: "car_list_image_file",
							template: "<img class='img-circle' width='40' src='#:data.car_list_image_file#'/> " ,
							width: 100,
							title: "사진(리스트)"
						},
						{
							field: "car_detail_image_file",
							template: "<img class='img-rounded' width='70' src='#:data.car_detail_image_file#'/> " ,
							width: 100,
							title: "사진(상세)"
						},
						{
							field: "car_type",
							title: "차종",
						},
						{
							field: "car_name",
							title: "모델명",
						},
						{
							field: "car_name_detail",
							title: "세부모델명",
						},
						// {
						// 	field: "car_people",
						// 	title: "승차인원수",
						// 	format: "{0:##,#}명",
						// },
						{
							field: "car_normal_price",
							title: "표준대여금액",
							format: "{0:##,#}원",
						},
						// {
						// 	field: "fuel_option_gasoline",
						// 	title: "가솔린",
						// 	width:100
						// },
						// {
						// 	field: "fuel_option_diesel",
						// 	title: "디젤",
						// 	width:100
						// },
						// {
						// 	field: "fuel_option_lpg",
						// 	title: "LPG",
						// 	width:100
						// },
						// {
						// 	field: "fuel_option_hybrid",
						// 	title: "하이브리드",
						// 	width:100
						// },
						// {
						// 	field: "fuel_option_electricity",
						// 	title: "전기",
						// 	width:100
						// },
						{
							field: "status",
							title: "사용상태",
						}
           			]
         });


	


        btn_register.click(function() {
        	is_register = true;
           text_dialog_title.text("차종 등록");
           btn_change.text("등록하기");
           //값 초기화
           	$('#maker_name').val('');
           	$("#car_type option:eq(0)").attr("selected", "selected");

			// $("#car_checkbox_gasoline").prop("checked", false);
			// $("#car_checkbox_diesel").prop("checked", false);
			// $("#car_checkbox_hybrid").prop("checked", false);
			// $("#car_checkbox_lpg").prop("checked", false);
			// $("#car_checkbox_electricity").prop("checked", false);
			$("#car_list_image_file").val('');
			$("#car_list_image_txt").val('');
			$("#car_detail_image_file").val('');
			$("#car_detail_image_txt").val('');

			$('#car_name').val('');
			$('#car_name_detail').val('');
           	// $('#car_people').val('');
           	$('#car_normal_price').val('');
           	
			$("#select_status option:eq(0)").attr("selected", "selected");
        });


        btn_delete.click(function() {

        	var row = grid.select();
			var data = grid.dataItem(row);
			if(data==null){
				alert("삭제할 제조사를 선택해 주세요.");
				return false;
			}


        	$.post("/carmaster/delete/",
        	{
        			car_index : data.car_index //수정사항 10/3일.
			},
			function(data, status){
					var result = JSON.parse(data);
					if(result.code=="S01"){
						alert("삭제되었습니다.");
						//데이터 다시 읽어옴
						grid_refresh();
					}else{
						alert("데이터 삭제 중 에러가 발생했습니다.");
					}

			});


        });


        btn_change.click(function(){
        	var title = text_dialog_title.text();
        	var maker_index = $('#maker_index option:selected').val();
        	var status = $("#select_status option:selected").val();
			var car_type = $("#car_type option:selected").val();


			var car_name= $('#car_name').val();
			var car_name_detail= $('#car_name_detail').val();
           	var car_people= $('#car_people').val();

			car_name = car_name.replace(/^\s+/, "");
			car_name = car_name.replace(/\s+$/, "");

        		// alert("메이커:"+ car_name +", 상태:" + car_name_detail +"차종류" + selectCarKinds);
        		// return false;
        		if(car_name == ""){
	        		alert("이름을 입력하세요");
	        		return false;
        		}
        		if(car_name_detail == ""){
        			alert("상세 이름을 입력하세요");
        			return false;
        		}
        		// if(car_people == ""){
        		// alert("인원수를 입력하세요");
        		// die();	
        		// }
        		if($('#car_normal_price').val() == "" || $('#car_normal_price').val() <1 ){
        			alert("가격을 입력하세요");
        			return false;
        		}

    			// if($("#car_list_image").val()!='' ){
    			// 		alert("리스트 이미지 파일을 선택해주세요");
    			// 		return false;
    			// }

    			// if($("#car_detail_image").val()!='' ){
    			// 		alert("상세 이미지 파일을 선택해주세요");
    			// 		return false;
    			// }
        		
        	if(is_register){
        		$.post("/carmaster/register/",
        		{


					maker_index : maker_index,
					status : status,
					car_type : car_type,
					// fuel_option_gasoline: ($('#car_checkbox_gasoline').is(":checked")) ? "Y" : "N" ,
					// fuel_option_diesel : ($('#car_checkbox_diesel').is(":checked")) ? "Y" : "N" ,
					// fuel_option_hybrid : ($('#car_checkbox_hybrid').is(":checked")) ? "Y" : "N" ,
					// fuel_option_lpg : ($('#car_checkbox_lpg').is(":checked")) ? "Y" : "N" ,
					// fuel_option_electricity : ($('#car_checkbox_electricity').is(":checked")) ? "Y" : "N" ,
					car_name : car_name,
					car_name_detail : car_name_detail,
					// car_people : car_people,
					car_normal_price : $('#car_normal_price').val()
					// car_list_image_file : filename,
					// car_detail_image_file : $('car_detail_image_file').val()
				},
				function(data, status){
					// alert ("hi");
					var result = JSON.parse(data);
					if(result.code=="S01"){
						alert("등록되었습니다.");
						//데이터 다시 읽어옴
						// grid.refresh();
						$('#car_serial').val(result.car_index);
						$('#carmaster_form').submit();	
						// }

						// alert($('#file_upload').submit());

//						grid_refresh();

						$("[data-dismiss=modal]").trigger({ type: "click" });
						// $('.modal-backdrop').toggle();
					}else if(result.code=="E02"){//이미 있는 경우
						alert("이미 등록 된 차량 입니다.");
					}else{
						alert("데이터 입력 중 에러가 발생했습니다.");
					}

				});
        	}else{

        		//선택한것의 정보 읽어오기
        		var grid = $("#grid").data("kendoGrid");
        		var row = grid.select();
				var data = grid.dataItem(row);
				car_serial = data.car_index;
 
        		$.post("/carmaster/update/",
        		{
        			maker_index : $('#maker_index option:selected').val(),
					status : $("#select_status option:selected").val(),
					car_type : $("#car_type option:selected").val(),
					// fuel_option_gasoline: ($('#car_checkbox_gasoline').is(":checked")) ? "Y" : "N" ,
					// fuel_option_diesel : ($('#car_checkbox_diesel').is(":checked")) ? "Y" : "N" ,
					// fuel_option_hybrid : ($('#car_checkbox_hybrid').is(":checked")) ? "Y" : "N" ,
					// fuel_option_lpg : ($('#car_checkbox_lpg').is(":checked")) ? "Y" : "N" ,
					// fuel_option_electricity : ($('#car_checkbox_electricity').is(":checked")) ? "Y" : "N" ,
					car_name : $('#car_name').val(),
					car_name_detail : $('#car_name_detail').val(),
					// car_people : $('#car_people').val(),
					car_normal_price : $('#car_normal_price').val(),
					car_index : car_serial

				},
				function(data, status){
					var result = JSON.parse(data);
					if(result.code=="S01"){
						alert("수정되었습니다.");
						$('#car_serial').val(car_serial);
						$('#carmaster_form').submit();	
					}else{
						alert("데이터 수정 중 에러가 발생했습니다.");
					}

				});
        	}
        });

        //검색
          $("#searchbox").keyup(function () {
                var val = $('#searchbox').val();
                // alert(val);
                $("#grid").data("kendoGrid").dataSource.filter({
                    logic: "or",
                    filters: [
                        {
                            field: "maker",
                            operator: "contains",
                            value: val
                        },
                        {
                            field: "car_type",
                            operator: "contains",
                            value: val
                        },
                        {
                            field: "car_model",
                            operator: "contains",
                            value:val
                        },
                        {
                            field: "car_name_detail",
                            operator: "contains",
                            value:val
                        },
                        {
                            field: "status",
                            operator: "contains",
                            value:val
                        },
                    ]
                });


            });
           fitGridSize();

});

function grid_refresh(){
	var dataSource = new kendo.data.DataSource({
 		 transport: {
  		  read: {
   		   url: "/carmaster/get_list/",
 		   dataType: "json"
 		   }
	  	},
	  	pageSize: 20
	});
	var grid = $("#grid").data("kendoGrid");
	grid.setDataSource(dataSource);
}


//upload
// $(function() {

//   // We can attach the `fileselect` event to all file inputs on the page
//   $(document).on('change', ':file', function() {
//     var input = $(this),
//         numFiles = input.get(0).files ? input.get(0).files.length : 1,
//         label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
//     input.trigger('fileselect', [numFiles, label]);
//   });

//   // We can watch for our custom `fileselect` event like this
//   $(document).ready( function() {
//       $(':file').on('fileselect', function(event, numFiles, label) {

//           var input = $(this).parents('.input-group').find(':text'),
//               log = numFiles > 1 ? numFiles + ' files selected' : label;

//           if( input.length ) {
//               input.val(log);
//           } else {
//               if( log ) alert(log);
//           }

//       });
//   });

// });
//upload end

$('#input_dialog').draggable({
  handle: ".modal-header"
});


$("#grid").delegate("tbody>tr", "dblclick",
  function(){
  		$('#input_dialog').modal('toggle'); //다이얼로그 출력
  		is_register = false;
  		var grid = $("#grid").data("kendoGrid");
      	var row = grid.select();

			var data = grid.dataItem(row);
			if(data==null){
				alert("수정할 제조사를 선택해 주세요.");
				return false;
			}

        	text_dialog_title.text("차종 수정");
        	btn_change.text("수정하기");


			// if(data.fuel_option_gasoline == "Y"){
			// 	$gasoline = "checked"
			// }else{
			// 	$gasoline = ""
			// }

			// if(data.fuel_option_diesel == "Y"){
			// 	$diesel = "checked"
			// }else{
			// 	$diesel= ""
			// }

			// if(data.fuel_option_lpg == "Y"){
			// 	$lpg = "checked"
			// }else{
			// 	$lpg = ""
			// }

			// if(data.fuel_option_hybrid == "Y"){
			// 	$hybrid = "checked"
			// }else{
			// 	$hybrid = ""
			// }

			// if(data.fuel_option_electricity == "Y"){
			// 	$electricity = "checked"
			// }else{
			// 	$electricity = ""
			// }

			$("#car_list_image_file").val('');
			$("#car_list_image_txt").val('');
			$("#car_detail_image_file").val('');
			$("#car_detail_image_txt").val('');


		//읽어온값 대입
			$('#maker_index').val(data.maker_index);
			$("#select_status").val(data.status).attr("selected", "selected");
			$("#car_type").val(data.car_type).attr("selected", "selected");
			// $("#car_type option:selected").val(data.car_type);

			$('#car_name').val(data.car_name);
			$('#car_name_detail').val(data.car_name_detail);
           	// $('#car_people').val(data.car_people);
           	$('#car_normal_price').val(data.car_normal_price);
			// $("input:checkbox[id='car_checkbox_gasoline']").prop($gasoline, true);
			// $("input:checkbox[id='car_checkbox_diesel']").prop($diesel, true);
			// $("input:checkbox[id='car_checkbox_lpg']").prop($lpg, true);
			// $("input:checkbox[id='car_checkbox_hybrid']").prop($hybrid, true);
			// $("input:checkbox[id='car_checkbox_electricity']").prop($electricity, true);

});

</script>
</div>
