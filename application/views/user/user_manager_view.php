<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>
<!-- 새로운 datepicker -->
<link href="/css/datepicker.min.css?<?=time();?>" rel="stylesheet" type="text/css">
<script src="/js/datepicker.js?<?=time();?>"></script>
<script src="/js/i18n_datepicker.en.js"></script>
<style type="text/css">
    html{overflow:hidden;}
</style>
<?php
    require("/home/apache/CodeIgniter-3.0.6/application/views/user/dialog_user_info.html");
    require("/home/apache/CodeIgniter-3.0.6/application/views/rengo_util.html");
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel border_gray magb0">
            
            <div class="panel-heading pdt17" >
                <div class="row">
                    <div class="col-md-9">
                        <h3 class="">고객관리</h3>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12 pdl0 pdr0">
                            <select class="form-control" id="company_select">
                                <?php foreach($company_list as $company):?>
                                    <option value="<?php echo $company['serial']; ?>"> <?php echo $company['company_name']; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-footer bb_ccc pdt14 pdb0">
                <div class="row">
                    <div class="list_data_box">
                        <div class="col-lg-1 col-md-2 col-xs-3">전체고객</div>
                        <div class="col-lg-1 col-md-2 col-xs-3 text-right" id="whole"></div>
                        <div class="col-lg-1 col-md-2 col-xs-3">일반고객</div>
                        <div class="col-lg-1 col-md-2 col-xs-3 text-right" id="normal"></div>
                        <div class="col-lg-1 col-md-2 col-xs-3">불량고객</div>
                        <div class="col-lg-1 col-md-2 col-xs-3 text-right" id="bad"></div>
                        <div class="col-lg-1 col-md-2 col-xs-3">특별고객</div>
                        <div class="col-lg-1 col-md-2 col-xs-3 text-right" id="special"></div>
                    </div>
                </div>
            </div>

            <div class="panel-heading pdt13 pdb13">
                <div class="row">
                    <div class="col-md-9">
                        <a class="btn btn-default" href="#" role="button" onclick="saveExcel();">엑셀로 저장</a>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group custom-search-form">
                            <input id="searchbox" type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                              <button class="btn btn-default" type="button">
                                  <i class="fa fa-search"></i>
                              </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-footer section_lnb">
                <ul class="nav nav-tabs border_none" id="tab_active">
                    <li class="active"><a data-toggle="tab" href="#user_view01" onclick="filter_grid('whole')">전체</a></li>
                    <li><a data-toggle="tab" href="#user_view02" onclick="filter_grid('normal')">일반 고객</a></li>
                    <li><a data-toggle="tab" href="#user_view03" onclick="filter_grid('bad')">불량 고객</a></li>
                    <li><a data-toggle="tab" href="#user_view04" onclick="filter_grid('special')">특별 고객</a></li>
                </ul>
            </div>

        </div>
    </div>
</div>


<div class="tab-content">
    <div id="example" style="margin:0;">
        <div id="grid"></div>
    </div>
</div>

    <script> 

$(document).ready(function() {
    userCount();
    initModal();
    show_company_select();
    setGrid();
    fitGridSize();
    refresh_grid();

})

function initModal(){
    $('.modal').on('hidden.bs.modal',function() {
        $('.modal_scroll').scrollTop(0);
        setTimeout(function(){
            $('.modal').css({
                'top':'0',
                'left':'0'
            });
        },30)
    })
    $('.modal').draggable();
}

show_company_select();

    var published_date = '';
    var expiration_date = '';
    var company_serial = get_company_serial();


function setGrid() {
    $("#grid").kendoGrid({
        // navigatable: true,  
        selectable: true, 
        allowCopy: true, 
        sortable: true, 
        // filterable: true,
        groupable: false,
        resizable: true,
        excel: {
            allPages: true,
            fileName: "고객리스트.xlsx",
            filterable: true
        },
        pageable: {
            input: true,
            messages: {
                display: "총 {2} 명의 고객 중 {0}~{1}번째",
                empty: "데이타 없음"
            }
          },
        noRecords: {
            template: "등록된 고객이 없습니다."
          },
        // columnMenu: {
        //     sortable: false,
        //     messages: {
        //         columns: "표시할 항목 선택",
        //         filter: "필터",
        //     }
        //  },
        dataSource: {
            transport: {
                 read: {
                   url: '',
                   dataType: "json"
                 }
               },
            schema: {
                model: {
                    fields: {
                        user_serial: {type: "number"},
                        user_status: { type: "string" },
                        user_name: { type: "string" }, 
                        birthday: { type: "string" },
                        postcode : {type : "string"}, 
                        address: { type: "string" }, 
                        user_email: {type:"string"},
                        registration_date: {type:"date"},
                        phone_number1: { type: "string" },
                        license_type: { type: "string" }, 
                        license_number: { type: "string" }, 
                        published_date: { type: "date" }, 
                        expiration_date: { type: "date" },                        
                        phone_number2: { type: "string" },
                        note : { type: "string" },
                        // company: { type: "string" }, 
                        // auth_code: { type: "number" }, 
                        // note: {type:"string"},
                    }
                }
            },
            pageSize: 20
        },
       
        columns: [ {
                field: "user_status",
                title: "고객구분",
                // locked: true,
                width: 100
            },  {
                field: "user_name",
                title: "고객명",
                // locked: true,
                width: 100
            },  {
                field: "user_email",
                title: "이메일",
                width: 200
            }, {
                field: "birthday",
                title: "생년월일",
                width: 100
            },  {
                field: "postcode",
                title: "우편번호",
                width: 100
            },  {
                field: "address",
                title: "주소",
                width: 200
            },   {
                field: "phone_number1",
                title: "연락처",
                width: 140
            }, {
                field: "phone_number2",
                title: "긴급연락처",
                width: 140
            } ,{
                field: "license_type",
                title: "면허정보",
                width: 100
            },  {
                field: "license_number",
                title: "면허번호",
                width: 140
            },  {
                field: "published_date",
                title: "발행일",
                format: "{0: yyyy년 MM월 dd일}",
                width: 140
            },  {
                field: "expiration_date",
                title: "유효기간",
                format: "{0: yyyy년 MM월 dd일}",
                width: 140
            },  
            // {
            //     field: "company",
            //     title: "회사명",
            //     width: 140
            // },  {
            //     field: "auth_code",
            //     title: "사업자번호",
            //     width: 100
            // },
            
            // ,  {
            //     field: "note",
            //     title: "비고",
            //     width: 100
            // }
        ]
    });    
}
  

function saveExcel(){
    var grid = $("#grid").data("kendoGrid");
    grid.saveAsExcel();
}

function refresh_grid(){
    var company_serial = get_company_serial();
    
    var dataSource = new kendo.data.DataSource({
         transport: {
          read: {
           url: "/usermanager/get_user_information/" + company_serial,
           dataType: "json"
           }
        },
         schema: {
                model: {
                    fields: {
                        user_serial: {type: "number"},
                        user_status: { type: "string" },
                        user_name: { type: "string" }, 
                        birthday: { type: "string" },
                        postcode : {type : "string"}, 
                        address: { type: "string" }, 
                        user_email: {type:"string"},
                        registration_date: {type:"date"},
                        phone_number1: { type: "string" },
                        license_type: { type: "string" }, 
                        license_number: { type: "string" }, 
                        published_date: { type: "date" }, 
                        expiration_date: { type: "date" },                        
                        phone_number2: { type: "string" },
                        note : { type: "string" },
                    }
                }
            },
        pageSize: 30
    });
    var grid = $("#grid").data("kendoGrid");
    
    grid.setDataSource(dataSource);
    grid.refresh();
}

function filter_grid(user_status) {
    if(user_status =='whole'){
        user_status = '';
         $("#grid").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [
            {
                field: "user_status",
                operator: "contains",
                value: user_status
            }]
       });
    }else{
        var user_status_ko={
            'normal' : '일반',
            'bad' : '불량',
            'special' : '특별'
        };

         $("#grid").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [{
                field: "user_status",
                operator: "eq",
                value: user_status_ko[user_status]
            }]
       });
    }

}

function userCount() {

    var company_serial = get_company_serial();

    //전체 다 들고와서 key별로 나누어지게끔..
    $.get("/usermanager/get_user_count/"+company_serial + '/whole',
        function(data){
            var result = JSON.parse(data);
            $('#whole').text(result+"명");
    });
    $.get("/usermanager/get_user_count/"+company_serial + '/normal',
        function(data){
            var result = JSON.parse(data);
            $('#normal').text(result+"명");
    });
    $.get("/usermanager/get_user_count/"+company_serial + '/bad',
        function(data){
            var result = JSON.parse(data);
            $('#bad').text(result+"명");
    });
    $.get("/usermanager/get_user_count/"+company_serial + '/special',
        function(data){
            var result = JSON.parse(data);
            $('#special').text(result+"명");
    });    
}

function init_active() {
    var $tab_active=$('#tab_active');
    $tab_active.children().eq(0).addClass('active');
    for(var i=1;i<4;++i){
        $tab_active.children().eq(i).removeClass('active');
    }
}

$('#company_select').change(function() {
    userCount();
    init_active();
    refresh_grid();
});

$("#searchbox").keyup(function () {
        var val = $('#searchbox').val();
        $("#grid").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [
                {
                    field: "user_status",
                    operator: "contains",
                    value: val
                },
                {
                    field: "user_name",
                    operator: "contains",
                    value:val
                },
                {
                    field: "birthday",
                    operator: "contains",
                    value:val
                },
                {
                    field: "address",
                    operator: "contains",
                    value:val
                },
                {
                    field: "email",
                    operator: "contains",
                    value:val
                },
                {
                    field: "phone_number1",
                    operator: "contains",
                    value:val
                },
                {
                    field: "license_type",
                    operator: "contains",
                    value:val
                },
                {
                    field: "license_number",
                    operator: "contains",
                    value:val
                },
                {
                    field: "published_date",
                    operator: "contains",
                    value:val
                },
                {
                    field: "phone_number2",
                    operator: "contains",
                    value:val
                },
            ]
        });
    });



// var user_delete =$('#user_delete');

// user_delete.click(function(){
//     var grid = $('#grid').data('kendoGrid'); 
//     var row = grid.select();
//     var data = grid.dataItem(row);
//     if(data==null){
//         alert('삭제할 고객을 선택해 주세요.');
//         return false;
//     }
//     $.post('/usermanager/user_delete/',{
//         user_serial : data.user_serial
//     },
//     function(data,status){
//         var result = JSON.parse(data);
//         if(result.code=="S01"){
//             alert("삭제되었습니다.");
//             //데이터 다시 읽어옴
//             $('#dialog_user_input').modal('toggle');
//             grid.refresh();
//         }else{
//             alert("데이터 삭제 중 에러가 발생했습니다.");
//         }
//     });
// })

$("#grid").delegate("tbody>tr", "dblclick", function(){

    // $('#user_delete').removeClass('hidden');
    // $('#rental_receipt_div').removeClass('hidden');

    var grid = $('#grid').data('kendoGrid'); 
    var row = grid.select();
    var data = grid.dataItem(row);
    
    // var dialog_title=$('#dialog_title');    

    // dialog_title.text('고객 정보 수정');    
    
    $('#user_status').filter(':selected').val(data.user_status);
    $('#user_email').val(data.user_email);
    $('#user_name').val(data.user_name);
    if(data.birthday.length==8){
        var tempBirth = data.birthday.substring(2, data.birthday);
    }
    $('#birthday').val(tempBirth);
    $('#gender').val(data.gender);
    $('#postcode').val(data.postcode);
    $('#address').val(data.address);
    $('#phone_number1').val(data.phone_number1);
    $('#phone_number2').val(data.phone_number2);
    $('#registration_date').val(data.registration_date);
    $('#license_type').filter(':selected').val(data.license_type);
    $('#license_number').val(data.license_number);
    $('#input_etc').val(data.note);
    $('#published_date').datepicker().data('datepicker').selectDate(data.published_date);
    $('#expiration_date').datepicker().data('datepicker').selectDate(data.expiration_date);

    // var url = "/usermanager/get_license_image/"+data.user_serial;
    // $('#license_div').html("<img src='" + url +"' style='width: 500px; height:200px;' class='img-thumbnail' alt='운전면허증''>");


    var dataSource = new kendo.data.DataSource({
          transport: {
            read: {
            url: "/usermanager/get_order_information/"+data.user_serial,
            dataType: "json"
             }
           },
           schema: {
                model: {
                    fields: {
                        period_start: {type:"date"},
                        period_finish: {type:"date"},
                        car_type: {type:"string"},
                        car_name_detail: {type:"string"},
                        car_number: { type:"string"},
                        rental_price: {type:"number"}
                    }
                }
            },
            pageSize: 5
    });
    var grid = $("#rental_grid").data("kendoGrid");

    grid.setDataSource(dataSource);
    grid.refresh();

     var dataSource2 = new kendo.data.DataSource({
          transport: {
            read: {
            url: "/usermanager/get_receipt_information/"+data.user_serial,
            dataType: "json"
             }
            },
             schema: {
                        model: {
                            fields: {
                                deposite_date: { type:"date"},
                                type: {type:"string"},
                                deposite_way: {type:"string"},
                                deposite_amount: {type:"number"},
                                memo: {type:"string"},
                                handler: { type:"string"},
                                deposite_registration_date: {type:"string"}
                            }
                        }
                },
                pageSize: 5
    });
    var grid2 = $("#receipt_grid").data("kendoGrid");

    grid2.setDataSource(dataSource2);
    grid2.refresh();

    $('#dialog_user_input').modal('toggle');
});

function fitGridSize() {
    var gridElement = $("#grid");
    var gird_position = $('#grid').position();
    var window_height = $( window ).height() - gird_position.top - 50;
    gridElement.children(".k-grid-content").height(window_height-100);
    gridElement.height(window_height);
}

$(window).resize(function() {
    fitGridSize();
});

function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
}

</script>

