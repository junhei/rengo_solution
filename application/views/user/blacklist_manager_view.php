
<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>
<!-- 새로운 datepicker -->
<link href="/css/datepicker.min.css?<?=time();?>" rel="stylesheet" type="text/css">
<script src="/js/datepicker.js?<?=time();?>"></script>
<script src="/js/i18n_datepicker.en.js"></script>
<style type="text/css">
  html{overflow:hidden;}
</style>

<?php
    require("/home/apache/CodeIgniter-3.0.6/application/views/user/dialog_blacklist_info.html");
    require("/home/apache/CodeIgniter-3.0.6/application/views/rengo_util.html");
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel border_gray magb0 bb0">
      
        <div class="panel-heading bb_ccc pdt15">
            <div class="row" style="margin:0">
                <div class="col-md-2">
                    <h3 class=" mgb10">블랙리스트</h3>
                </div>
            </div>
        </div>

        <div class="panel-footer bt0 bb_ccc pdb0 pdt15">
            <div class="row">
                <div class="list_data_box">
                    <div class="col-lg-1 col-md-2 col-xs-3">등록고객</div>
                    <div class="col-lg-1 col-md-2 col-xs-3 text-right" ><?php print_r($count_black[0]['blacklist'])?>명</div>
                </div>
            </div>
        </div>

        <div class="panel-heading pdt14 pdb13">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6" style="padding-left:0;">
                      <button id="blacklist_register_btn" type="button" class="btn action_btn" data-toggle="modal" data-target="#dialog_blacklist_input">등록</button>
                    </div>
                    <div class="col-md-3 pull-right" style="padding-right:0;">
                        <div class="input-group custom-search-form">
                            <input id="searchbox" type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
  </div>
</div>

    <div id="blacklist_grid"></div>


<script type="text/javascript">


var is_register = true;
var register_day = '';
// var gird_position = $('#blacklist_grid').position();
// var window_height = $( window ).height() - gird_position.top - 50;
//객체 변수
var $blacklist_register_btn;
var $blacklist_delete_btn;
var $blacklist_save_btn;
var $blacklist_grid;
var $blacklist_select_type;
var $blacklist_input_name;
var $blacklist_input_birthday;
var $blacklist_input_phone;
var $blacklistday;
var $blacklist_input_place;
var $blacklist_input_detail;
var $blacklist_input_dialog_title;
var $dialog_blacklist_input;
var $searchbox;
var $blacklist_input_close_btn;

$(document).ready(function() {
    initVariables();
    initModalPosition();
    blacklistGrid();
    blacklistGridDoubleClick();
    blacklistRegisterBtnClick();
    searchboxKeyup();
    blacklistSaveBtnClick();
    blacklistDeleteBtnClick();
    fitGridSize();
    blacklistGirdResize();
})

function initVariables(){
    $blacklist_register_btn=$('#blacklist_register_btn');
    $blacklist_delete_btn=$('#blacklist_delete_btn');
    $blacklist_save_btn=$('#blacklist_save_btn');
    $blacklist_grid=$('#blacklist_grid');
    $blacklist_select_type=$('#blacklist_select_type');
    $blacklist_input_name=$('#blacklist_input_name');
    $blacklist_input_birthday=$('#blacklist_input_birthday');
    $blacklist_input_phone=$('#blacklist_input_phone');
    $blacklistday=$('#blacklistday');
    $blacklist_input_place=$('#blacklist_input_place');
    $blacklist_input_detail=$('#blacklist_input_detail');
    $blacklist_input_dialog_title=$('#blacklist_input_dialog_title');
    $dialog_blacklist_input=$('#dialog_blacklist_input');
    $searchbox=$('#searchbox');
    $blacklist_input_close_btn=$('#blacklist_input_close_btn');
}

function initModalPosition(){
    $('.modal').draggable();

    $('.modal').on('hidden.bs.modal',function() {
      $('.modal_scroll').scrollTop(0);
      setTimeout(function(){
        $('.modal').css({
          'top':'0',
          'left':'0'
        });

        },30)
    });
}

function blacklistGrid() {
    var gird_position = $blacklist_grid.position();
    var window_height = $( window ).height() - gird_position.top - 50;

    $blacklist_grid.kendoGrid({
        navigatable: true,  
        // reorderable: true, 
        resizable: true, 
        selectable: "row", 
        allowCopy: true, 
        height: window_height, 
        sortable: true, 
        // filterable: true, 
        
        pageable: {
            input: true,
            messages: {
                display: "총 {2}명의 유저중 {0}~{1}번째",
                empty: "데이타 없음"
            }
          },
        noRecords: {
            template: "현재 페이지에서 보여줄 내용이 없습니다."
            },
            // columnMenu: {
            //     sortable: false,
            //     messages: {
            //         columns: "표시할 항목 선택",
            //         filter: "필터",
            //        }
            // },
            // filterable: {
            //     operators: {
            //         string: {
            //             eq: "같음",
            //             contains: "포함됨",
            //             startswith: "시작됨",
            //             endswith: "끝남"
            //         },
            //         number: {
            //             eq: "같음",
            //             gte: "크거나 같음",
            //             gt: "큼",
            //             lte: "작거타 같음",
            //             lt: "작음",
            //         },
            //         date: {
            //             eq: "같음",
            //             gte: "(포함)이후",
            //             lte: "(포함)이전"
            //         }
            //     }
            // },
            dataSource: {
                transport: {
                    read: {
                        url: "http://solution.rengo.co.kr/blacklistmanager/get_blacklist",
                        dataType: "json"
                    }
                },
            schema: {
                model: {
                    fields: {
                        serial: { type: "number" ,   validation: { required: true }, defaultValue: 0 },
                        company_serial:  { type: "number" },
                        name: { type: "string" },
                        tel: { type: "string" },
                        birthday: { type: "string" }, 
                        used_date: { type: "string" }, 
                        memo: { type: "string" },                                        
                        kind: { type: "string" }, 
                        used_area: { type: "string" }, 
                        company_name: { type: "string" }, 
                        license: { type: "string" }, 
                    }
                }
            },
            pageSize: 30,
        },
        columns: [ 
            {
                field: "kind",
                title: "분류",
                width: 100
            },  {
                field: "name",
                title: "이름",
                width: 100
            },  {
                field: "birthday",
                title: "생년월일",
                width: 100
            },  {
                field: "tel",
                title: "연락처",
                width: 100
            },  {
                field: "phone",
                title: "긴급연락처",
                width: 100
            },  {

                field: "used_date",
                title: "사용일시",
                width: 100
            },  {
                field: "used_area",
                title: "발생장소",
                width: 100
            },  {
                field: "memo",
                title: "내용",
                width: 300
            },  {
                field: "company_name",
                title: "비고",
                width: 100
            }
        ]
    });    
}

function blacklistGridDoubleClick(){
    $blacklist_grid.delegate("tbody>tr", "dblclick", function(){
        $dialog_blacklist_input.modal('toggle'); 
        var row = $blacklist_grid.data('kendoGrid').select();
        var data = $blacklist_grid.data('kendoGrid').dataItem(row);

        $blacklist_input_dialog_title.text("블랙리스트 확인");

        if(data.company_serial == <?php echo $company_serial; ?> || <?php echo $company_serial; ?> == 0){
            $blacklist_save_btn.show();
            $blacklist_save_btn.text("수정");
            $blacklist_delete_btn.show();
            $blacklist_delete_btn.text("삭제");
        }else{
            $blacklist_save_btn.hide();
            $blacklist_delete_btn.hide();
            $blacklist_select_type.attr('disabled','disabled');
            $blacklist_input_name.attr('disabled','disabled');
            $blacklist_input_phone.attr('disabled','disabled');
            $blacklist_input_birthday.attr('disabled','disabled');
            $blacklistday.attr('disabled','disabled');
            $blacklist_input_place.attr('disabled','disabled');
            $blacklist_input_detail.attr('disabled','disabled');
        }

        $blacklist_select_type.val(data.kind).prop('selected',true);
        $blacklist_input_name.val(data.name);
        $blacklist_input_birthday.val(data.birthday);
        $blacklist_input_phone.val(data.tel);
        $blacklist_input_place.val(data.used_area);
        $blacklist_input_detail.val(data.memo);

        var year = data.used_date.substring(0,4);
        var month = data.used_date.substring(4,6);
        var day = data.used_date.substring(6,8);
        var selected_date = new Date(year, month-1, day);
        $blacklistday.datepicker().data('datepicker').selectDate(selected_date);
    });
}

function searchboxKeyup() {
    $searchbox.keyup(function () {
        var val = $searchbox.val();
            $blacklist_grid.data("kendoGrid").dataSource.filter({
                logic: "or",
                filters: [
                    {
                        field: "kind",
                        operator: "contains",
                        value: val
                    },
                    {
                        field: "name",
                        operator: "contains",
                        value: val
                    },
                    {
                        field: "birthday",
                        operator: "contains",
                        value:val
                    },
                    {
                        field: "used_area",
                        operator: "contains",
                        value:val
                    },
                    {
                        field: "company_name",
                        operator: "contains",
                        value:val
                    },
                    {
                        field: "memo",
                        operator: "contains",
                        value:val
                    },
                    {
                        field: "company_name",
                        operator: "contains",
                        value:val
                    },
                ]
        });
    });
}

function blacklistRegisterBtnClick(){
    $blacklist_register_btn.click(function(){
        $blacklist_select_type.removeAttr('disabled');
        $blacklist_input_name.removeAttr('disabled');
        $blacklist_input_birthday.removeAttr('disabled');
        $blacklist_input_phone.removeAttr('disabled');
        $blacklistday.removeAttr('disabled');
        $blacklist_input_place.removeAttr('disabled');
        $blacklist_input_detail.removeAttr('disabled');
        $blacklist_save_btn.text("저장");
        $blacklist_input_dialog_title.text("블랙리스트 등록");
        $blacklist_save_btn.show();
        $blacklist_delete_btn.hide();
        $("#blacklist_select_type option:eq(0)").prop("selected", true);
        $blacklist_input_name.val('');
        $blacklist_input_birthday.val('');
        $blacklist_input_phone.val('');
        $blacklist_input_place.val('');
        $blacklist_input_detail.val('');
        var selected_date = new Date(year, month-1, day);
        $blacklistday.datepicker().data('datepicker').selectDate(selected_date);
        // $blacklistday.datepicker().data('datepicker').date = new Date();
    });
}

function blacklistSaveBtnClick() {
    $blacklist_save_btn.click(function(){
        var is_register= true;

        if($blacklist_input_name.val() == null || $blacklist_input_name.val() == ''){
            alert('이름은 필수 정보입니다.');
            is_register=false;
            return false;
        }

        if($blacklist_input_birthday.val() == null || $blacklist_input_birthday.val() == ''){
            alert('연락처는 필수 정보입니다.');
            is_register=false;
            return false;
        }

        var used_date = $blacklistday.val().slice(0,4)+$blacklistday.val().slice(6,8) + $blacklistday.val().slice(10,12);
        if(is_register == true){  
            $.post("/blacklistmanager/register_black",
                {
                    kind : $blacklist_select_type.filter(':selected').val(),
                    name : $blacklist_input_name.val(),
                    tel : $blacklist_input_phone.val(),
                    birthday : $blacklist_input_birthday.val(),
                    used_date : used_date,
                    used_area :  $blacklist_input_place.val(),
                    memo :  $blacklist_input_detail.val(),
              },
              function(data, status){
                  var result = JSON.parse(data);
                  if(result.code=="S01"){
                    alert("저장되었습니다.");
                    $("#blacklist_input_close_btn").trigger({ type: "click" });
                    refresh_grid();
                    //데이터 다시 읽어옴
                  }else if(result.code=="E02"){
                    alert("이미 같은 내역이 같은 날짜에 등록되어 있습니다.");
                  }else{
                    alert(result.message);
                  }
            });
          } 

      // else {  

      //   $.post("http://solution.rengo.co.kr/blacklistmanager/update_black",
      //       {
      //         serial : data.serial,
      //         name : $('#blacklist_input_name').val(),
      //         tel : $('#blacklist_input_phone').val(),
      //         birthday : $('#blacklist_input_birthday').val(),
      //         // used_date : register_day,
      //         memo :  $('#blacklist_input_detail').val(),
      //         kind : $('#blacklist_select_type option:selected').val(),
      //         used_area :  $('#blacklist_input_place').val(),
      //         result : $('#blacklist_input_detail').val(),
      //     },
      //     function(data, status){
      //         var result = JSON.parse(data);
      //         if(result.code=="S01"){
      //           alert("수정되었습니다.");
      //           $("#blacklist_input_close_btn").trigger({ type: "click" });
      //           refresh_grid();
      //           //데이터 다시 읽어옴
      //         }else{
      //           alert("데이터 수정 중 문제가 발생했습니다.");
      //         }

      //   });

      // }

    });
}


function blacklistDeleteBtnClick() {
$blacklist_delete_btn.click(function(){
    var grid = $("#blacklist_grid").data("kendoGrid");
    var row = grid.select();
    var data = grid.dataItem(row);


  $.post("http://solution.rengo.co.kr/blacklistmanager/delete_black",
    {
        serial : data.serial
    },
    function(data, status){
        var result = JSON.parse(data);
        if(result.code=="S01"){
            alert("삭제되었습니다.");
                $("#blacklist_input_close_btn").trigger({ type: "click" });
                refresh_grid();
            } else {
                alert("데이터 저장 중 에러가 발생했습니다.");
            }

      });
    });    
}


function refresh_grid(){

    var dataSource = new kendo.data.DataSource({
         transport: {
          read: {
           url: "http://solution.rengo.co.kr/blacklistmanager/get_blacklist",
           dataType: "json"
           }
        },
        schema: {
                model: {
                    fields: {
                        serial: { type: "number" ,   validation: { required: true }, defaultValue: 0 },
                        company_serial:  { type: "number" },
                        name: { type: "string" },
                        tel: { type: "string" },
                        birthday: { type: "string" }, 
                        used_date: { type: "string" }, 
                        memo: { type: "string" },                                        
                        kind: { type: "string" }, 
                        used_area: { type: "string" }, 
                        company_name: { type: "string" }, 
                        license: { type: "string" }, 
                    }
                }
            },
        pageSize: 30,
    });
  
    var $blacklist_grid = $('#blacklist_grid').data('kendoGrid');
    $blacklist_grid.setDataSource(dataSource);
    $blacklist_grid.refresh();

}

function fitGridSize() {
    var gird_position = $blacklist_grid.position();
    var window_height = $( window ).height() - gird_position.top - 50;
    $blacklist_grid.children(".k-grid-content").height(window_height-100);
    $blacklist_grid.height(window_height);
}

function blacklistGirdResize(){
    $(window).resize(function() {
       fitGridSize();
    });
}

// $dialog_blacklist_input.draggable({
//     handle: ".modal-header"
// });


</script>

