<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker3.min.css">
<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
<style>
/*{width: 200px !important;}*/
</style>

<link rel="stylesheet" href="http://kendo.cdn.telerik.com/2016.3.914/styles/kendo.common.min.css"/>
<link rel="stylesheet" href="http://kendo.cdn.telerik.com/2016.3.914/styles/kendo.rtl.min.css"/>
<link rel="stylesheet" href="http://kendo.cdn.telerik.com/2016.3.914/styles/kendo.silver.min.css"/>
<link rel="stylesheet" href="http://kendo.cdn.telerik.com/2016.3.914/styles/kendo.mobile.all.min.css"/>

<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>
<script src="/telerik/js/cultures/kendo.culture.ko-KR.min.js"></script>
<script src="https://www.gstatic.com/charts/loader.js"></script>

<div class="row" style="margin-right:-10px;">
  <div class="col-md-12">
    <div class="magb0 b_ccc bg_fff">
    <!-- <div class="panel border_gray"> -->
      
      <div class="panel-heading pdt15">
        <div class="row">
          <div class="col-md-4">
            <h3 class="mgb10 magt0">통계</h3>
          </div>
          <div class="col-md-3 col-md-offset-5">
            <div class="col-md-12  text-right pdl0 pdr0">
              <select class="form-control" id="car_company_select">
                <?php foreach($company_list as $company):?>
                  <option value="<?php echo $company['serial']; ?>"> <?php echo $company['company_name']; ?></option>
                <?php endforeach;?>
              </select>
            </div>

          </div>
        </div>
      </div>

      <div class="panel-footer bg_fff pdt14 pdb13">
        <div class="row">
          <div class="col-sm-6 pdr0">
            <div class="col-sm-5 pdr0 pdl0 time_p">
              <div class="col-sm-6 pdr0">
                <select class="form-control " id="chart_select_start_year">
                </select>  
              </div>
              <div class="col-sm-6">
                <select class="form-control col-sm-1" id="chart_select_start_month">
                </select>  
              </div>
              <!-- <span>부터</span> -->
            </div>
            <div class="col-sm-1 pdl0 pdt5">
                부터
              </div>
            <div class="col-sm-5 pdr0 pdl0 time_p">
              <div class="col-sm-6 pdr0">
                <select class="form-control" id="chart_select_end_year">
                </select>                
              </div>
              <div class="col-sm-6">
                <select class="form-control" id="chart_select_end_month">
              </select>
              </div>
              <!-- <span class="time_p2">까지</span> -->
            </div>            
            <div class="col-sm-1 pdl0 pdr0 pdt5">
                까지
              </div>
          </div>
          <button id="chart_save_btn" class="btn action_btn">검색</button>
        </div>
      </div>

      <div class="panel-footer bg_fff">


      <!--   <div class="row pdt15">
          <div class="col-sm-6">
            <div class="panel panel-default magt5 magb40">
              <div class="panel-heading">
                  <i class="fa fa-cog"></i> 기간별 매출 현황
              </div>

              <div class="col-sm-12">
                <div id="chart_expense" style="width: 600px; height: 400px;"></div>
              </div>

              <div class="panel-body">
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="panel panel-default magt5 magb40">
              <div class="panel-heading">
                  <i class="fa fa-cog"></i> 차량별 운행기간
              </div>

              <div class="col-sm-12">
                <div id="chart_car_time" style="width: 600px; height: 400px;"></div>
              </div>

              <div class="panel-body">
              </div>
            </div>
          </div>
        </div> -->

        
        

        <div class="row pdt15">
          <div class="col-sm-6">
            <div class="panel panel-default magt5 magb40">
              <div class="panel-heading">
                  <i class="fa fa-cog"></i> 지역별 예약현황 (상위 10개순)
              </div>

              <div class="col-sm-12">
                <div id="chart_place" style="width: 600px; height: 400px;"></div>
              </div>

              <div class="panel-body">
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="panel panel-default magt5 magb40">
              <div class="panel-heading">
                  <i class="fa fa-cog"></i> 차종선호현황
              </div>

              <div class="col-sm-12">
                <div id="chart_car_preference" style="width: 600px; height: 400px;"></div>
              </div>

              <div class="panel-body">
              </div>
            </div>
          </div>
        </div>

        <div class="row pdt15">
          <div class="col-sm-6">
            <div class="panel panel-default magt5 magb40">
              <div class="panel-heading">
                  <i class="fa fa-cog"></i> 연령별 예약현황
              </div>

              <div class="col-sm-12">
                <div id="chart_age" style="width: 600px; height: 400px;"></div>
              </div>

              <div class="panel-body">
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="panel panel-default magt5 magb40">
              <div class="panel-heading">
                  <i class="fa fa-cog"></i> 성별 예약현황
              </div>

              <div class="col-sm-12">
                <div id="chart_sex" style="width: 600px; height: 400px;"></div>
              </div>

              <div class="panel-body">
              </div>
            </div>
          </div>
        </div>

      <!--   <div class="row pdt15">
          <div class="col-sm-6">
            <div class="panel panel-default magt5 magb40">
              <div class="panel-heading">
                  <i class="fa fa-cog"></i> 가격대별 예약현황
              </div>

              <div class="col-sm-12">
                <div id="chart_price" style="width: 600px; height: 400px;"></div>
              </div>

              <div class="panel-body">
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="panel panel-default magt5 magb40">
              <div class="panel-heading">
                  <i class="fa fa-cog"></i> 대여기간별별 예약현황
              </div>

              <div class="col-sm-12">
                <div id="chart_period" style="width: 600px; height: 400px;"></div>
              </div>

              <div class="panel-body">
              </div>
            </div>
          </div>
        </div>

        <div class="row pdt15">
          <div class="col-sm-6">
            <div class="panel panel-default magt5 magb40">
              <div class="panel-heading">
                  <i class="fa fa-cog"></i> 요일별 예약현황
              </div>

              <div class="col-sm-12">
                <div id="chart_day" style="width: 600px; height: 400px;"></div>
              </div>

              <div class="panel-body">
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="panel panel-default magt5 magb40">
              <div class="panel-heading">
                  <i class="fa fa-cog"></i> 시간별 예약현황
              </div>

              <div class="col-sm-12">
                <div id="chart_hour" style="width: 600px; height: 400px;"></div>
              </div>

              <div class="panel-body">
              </div>
            </div>
          </div>
        </div> -->
       


      </div>
    </div>
  </div>
</div>


 <script type="text/javascript">

  // var company_serial = <?php echo $company_serial; ?>;



 var today = new Date();
 var year = today.getFullYear();
 var yearArr = new Array();

//올해부터 5개 넣음
 for(i=0; i<4; i++){
      yearArr.push(year - i);
 }

 


  var month = today.getMonth() + 1; //월은 원래 1 더함
  var startMonth = year + zeroPad(month-1,2);
  var endMonth = startMonth;
  // var endMonth = year + zeroPad(month,2);

 var str='';
 for(i=1; i<=12; i++){
    str+= "<option value='" + zeroPad(i,2) + "'>" + i +"월</option>";
 }
 $("#chart_select_start_month").html(str);
 $("#chart_select_end_month").html(str);

 str='';
for(i=0; i<yearArr.length; i++){
    str+= "<option value='" + yearArr[i] +"'>" + yearArr[i] +"월</option>";
}
 $("#chart_select_start_year").html(str);
 $("#chart_select_end_year").html(str);

 // $("#selectBox option:eq(2)").attr("selected", "selected");

 $("#chart_select_start_month option:eq(" + (month-1) + ")").prop("selected", true);
 $("#chart_select_end_month option:eq(" + (month-1) + ")").prop("selected", true);

    //슈퍼 마스터가 아니면 회사 선택 select 안보임
   if($("#car_company_select option").size()==1){
    $("#car_company_select").hide();
   }else{
    $("#car_company_select").show();
   }




google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawCarTypeChart);
google.charts.setOnLoadCallback(drawPlaceChart);
google.charts.setOnLoadCallback(drawAgeChart);
google.charts.setOnLoadCallback(drawSexChart);
// google.charts.setOnLoadCallback(drawdailyChart);
// google.charts.setOnLoadCallback(drawHourChart);
// google.charts.setOnLoadCallback(drawPriceChart);
// google.charts.setOnLoadCallback(drawPeriodChart);
// google.charts.setOnLoadCallback(drawExpenseChart);
// google.charts.setOnLoadCallback(drawCarTimeChart);

// var dayUrl = "/chartmanager/get_daily_chart/" + year + zeroPad(month-1,2) + "/" + year + zeroPad(month,2) +"/<?php echo $company_serial; ?>";
// var hourUrl = "/chartmanager/get_hourly_chart/" + year + zeroPad(month-1,2) + "/" + year + zeroPad(month,2) + "/<?php echo $company_serial; ?>";


$("#chart_save_btn").click(function(){

  var startYear = $("#chart_select_start_year option:selected").val();
  var endYear = $("#chart_select_end_year option:selected").val();

   if(parseInt(startYear) > parseInt(endYear)){
     alert("시작 년도는 종료 년도보다 클수 없습니다.");
     return false;
   }


  var start = $("#chart_select_start_month option:selected").val();
  var end = $("#chart_select_end_month option:selected").val();
  if(parseInt(startYear + start) > parseInt(endYear + end)){
     alert("시작 기간은 종료 기간보다 늦을 수 없습니다.");
  }else{

    drawCarTypeChart();
    drawPlaceChart();
    drawAgeChart();
    drawSexChart();
    // drawdailyChart();
    // drawHourChart();
    // drawPriceChart();
    // drawPeriodChart();
    // drawExpenseChart();
    // drawCarTimeChart();
  }
});


function getStartDate(){
    var startYear = $("#chart_select_start_year option:selected").val();
    var startMonth = $("#chart_select_start_month option:selected").val();
    return startYear+""+startMonth+"01";
}

function getEndDate(){
    var endYear = $("#chart_select_end_year option:selected").val();
    var endMonth = $("#chart_select_end_month option:selected").val();

    //마지막날 구하기 위해서 1달 더하고 하루 뺌
    var end_temp_date = new Date(parseInt(endYear), parseInt(endMonth)-1,1);
    var end_date = new Date(end_temp_date.getFullYear(), end_temp_date.getMonth() + 1, end_temp_date.getDate());
    end_date.setDate(end_date.getDate() - 1);
    return end_date.getFullYear() +"" + zeroPad((end_date.getMonth()+1),2) + "" + zeroPad(end_date.getDate(),2);

}


function drawHourChart(){
 var company_serial = get_company_serial();
 var startMonth = getStartDate();
 var endMonth = getEndDate();

 var jsonData = $.ajax({
          url: "/chartmanager/get_hourly_chart/"+startMonth+"/" + endMonth +"/"+company_serial,
          dataType: "json",
          async: false
          }).responseText;

  var data = new google.visualization.DataTable(jsonData);

  var options = {
       
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_hour'));
        chart.draw(data, options);


}

function drawdailyChart(){
    var company_serial = get_company_serial();
    var startMonth = getStartDate();
    var endMonth = getEndDate();

   var jsonData = $.ajax({
          url: "/chartmanager/get_daily_chart/"+startMonth+"/" + endMonth +"/"+company_serial,
          dataType: "json",
          async: false
          }).responseText;

  var data = new google.visualization.DataTable(jsonData);

  var options = {
       
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_day'));
        chart.draw(data, options);

}

function drawSexChart() {
  var company_serial = get_company_serial();
  var startMonth = getStartDate();
  var endMonth = getEndDate();

    var jsonData = $.ajax({
          url: "/chartmanager/get_sex_chart/"+startMonth+"/" + endMonth +"/"+company_serial,
          dataType: "json",
          async: false
          }).responseText;

    var data = new google.visualization.DataTable(jsonData);

        var options = {
          legend : {alignment :'center', width:100, textStyle: {fontSize: 20}},
          pieSliceText: 'label',
          chartArea:{   width:'100%',height:'75%'},
          colors: ['#3c9a44', '#4ea058', '#88ba89', '#bfd7bf','#d6e2d6' ,],
          pieHole: 0.4,
          // is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_sex'));
        chart.draw(data, options);
      }

function drawCarTypeChart() {
  var company_serial = get_company_serial();
  var startMonth = getStartDate();
  var endMonth = getEndDate();

    var jsonData = $.ajax({
          url: "/chartmanager/get_cartype_chart/"+startMonth+"/" + endMonth +"/"+company_serial,
          dataType: "json",
          async: false
          }).responseText;

    var data = new google.visualization.DataTable(jsonData);

        var options = {
          legend : {alignment :'center', width:100, textStyle: {fontSize: 20}},
          pieSliceText: 'label',
          chartArea:{   width:'100%',height:'75%'},
          colors: ['#3c9a44', '#4ea058', '#88ba89', '#bfd7bf','#d6e2d6' ,],
          pieHole: 0.4,
          // is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_car_preference'));
        chart.draw(data, options);
      }

function drawPlaceChart() {
  var company_serial = get_company_serial();
  var startMonth = getStartDate();
  var endMonth = getEndDate();

    var jsonData = $.ajax({
          url: "/chartmanager/get_place_chart/"+startMonth+"/" + endMonth +"/"+company_serial,
          dataType: "json",
          async: false
          }).responseText;

    var data = new google.visualization.DataTable(jsonData);

        var options = {
          legend : {alignment :'center', position: 'right', textStyle: {fontSize: 20}},
          pieSliceText: 'label',
          chartArea:{ width:'100%',height:'75%'},
          
          colors: ['#004596', '#0262d2', '#0173d3', '#0181d3', '#049fe1', '#22bee5', '#57cdeb', '#94e5f9', '#bde2f5', '#dff5ff'],
          pieHole: 0.4,
          // is3D: true,
        };


        var chart = new google.visualization.PieChart(document.getElementById('chart_place'));
        chart.draw(data, options);
}

function drawAgeChart() {
  var company_serial = get_company_serial();
  var startMonth = getStartDate();
  var endMonth = getEndDate();


    var jsonData = $.ajax({
          url: "/chartmanager/get_age_chart/"+startMonth+"/" + endMonth +"/"+company_serial,
          dataType: "json",
          async: false
          }).responseText;

    var data = new google.visualization.DataTable(jsonData);

        var options = {
          legend : {alignment :'center', position: 'right', textStyle: {fontSize: 20}},
          pieSliceText: 'label',
          chartArea:{ width:'100%',height:'75%'},
          colors: ['#0262d2', '#0173d3', '#049fe1', '#57cdeb', '#bde2f5', ''],
          pieHole: 0.4,
          // is3D: true,
        };


        var chart = new google.visualization.PieChart(document.getElementById('chart_age'));

        

        chart.draw(data, options);
}

function drawPriceChart() {
  var company_serial = get_company_serial();
   var startMonth = getStartDate();
 var endMonth = getEndDate();

    var jsonData = $.ajax({
          url: "/chartmanager/get_price_chart/"+startMonth+"/" + endMonth +"/"+company_serial,
          dataType: "json",
          async: false
          }).responseText;

    var data = new google.visualization.DataTable(jsonData);

        var options = {
          legend : {alignment :'center', position: 'right', textStyle: {fontSize: 20}},
          pieSliceText: 'label',
          chartArea:{ width:'100%',height:'75%'},
          colors: ['#0262d2', '#0173d3', '#049fe1', '#57cdeb', '#bde2f5', ''],
          pieHole: 0.4,
          // is3D: true,
        };


        var chart = new google.visualization.PieChart(document.getElementById('chart_price'));

        

        chart.draw(data, options);
}

function drawPeriodChart() {
  var company_serial = get_company_serial();
   var startMonth = getStartDate();
 var endMonth = getEndDate();


    var jsonData = $.ajax({
          url: "/chartmanager/get_period_chart/"+startMonth+"/" + endMonth +"/"+company_serial,
          dataType: "json",
          async: false
          }).responseText;

    var data = new google.visualization.DataTable(jsonData);

        var options = {
          legend : {alignment :'center', position: 'right', textStyle: {fontSize: 20}},
          pieSliceText: 'label',
          chartArea:{ width:'100%',height:'75%'},
          colors: ['#0262d2', '#0173d3', '#049fe1', '#57cdeb', '#bde2f5', ''],
          pieHole: 0.4,
          // is3D: true,
        };


        var chart = new google.visualization.PieChart(document.getElementById('chart_period'));

        

        chart.draw(data, options);
}


function drawExpenseChart() {
 var company_serial = get_company_serial();
  var startMonth = getStartDate();
 var endMonth = getEndDate();

 var jsonData = $.ajax({
          url: "/chartmanager/get_expense_chart/"+startMonth+"/" + endMonth +"/"+company_serial,
          dataType: "json",
          async: false
          }).responseText;

  var data = new google.visualization.DataTable(jsonData);

  var options = {
       
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_expense'));
        chart.draw(data, options);
}

function drawCarTimeChart() {
 var company_serial = get_company_serial();
  var startMonth = getStartDate();
 var endMonth = getEndDate();

 var jsonData = $.ajax({
          url: "/chartmanager/get_car_time_chart/"+startMonth+"/" + endMonth +"/"+company_serial,
          dataType: "json",
          async: false
          }).responseText;

  var data = new google.visualization.DataTable(jsonData);

  var options = {
       
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_car_time'));
        chart.draw(data, options);
}




//////////////////////////////////////////////////////////////// 파트너 선택 리스너

$( "#car_company_select" ).change(function() {

 });

//company serial 얻기
function get_company_serial(){
  if($("#car_company_select").is(":visible")){
    var company_serial = $("#car_company_select option:selected").val();
  }else{
    var company_serial = <?php echo $company_serial; ?>;
  }
  return company_serial;
}

function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}
</script>
