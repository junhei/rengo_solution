<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>
<script src="/telerik/examples/content/shared/js/console.js"></script>
<style type="text/css">
    html{overflow:hidden;}
</style>
<?php
    require("/home/apache/CodeIgniter-3.0.6/application/views/rengo_util.html");
?>

<div class="row">
    <div class="col-md-12">
        <div class="magb0 b_ccc bg_fff">
            <div class="panel-heading pdt15">
                <div class="row">
                    <div class="col-md-4">
                        <h3 class="mgb10">정산관리</h3>
                    </div>
                    <div class="col-sm-3 col-md-offset-5">
                        <div class="col-md-12 pdl0 pdr0">
                            <select class="form-control" id="company_select">
                                <?php foreach($company_list as $company):?>
                                    <option value="<?php echo $company['serial']; ?>"> <?php echo $company['company_name']; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-footer pdb0 pdt15">
                <div class="row">
                    <div class="list_data_box">
                        <div class="col-lg-2 col-md-3 col-xs-4"></div>
                        <div class="col-lg-1 col-md-2 col-xs-3 text-right" id=""></div>
                        <div class="col-lg-2 col-md-3 col-xs-4"></div>
                        <div class="col-lg-1 col-md-2 col-xs-3 text-right" id=""></div>
                    </div>
                </div>
            </div>

            
            <div class="panel-footer bg_fff pdt14 pdb13">
                <div class="row">
                    <div class="col-sm-12 pdr0">
                        
                        <div class="col-sm-4 pdr0 pdl0 time_p">
                            <div class="col-sm-4 pdr0">
                                <select class="form-control " id="year_select">
                                </select>  
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control col-sm-1" id="month_select">
                                    <option value='1'>1월</option>
                                    <option value='2'>2월</option>
                                    <option value='3'>3월</option>
                                    <option value='4'>4월</option>
                                    <option value='5'>5월</option>
                                    <option value='6'>6월</option>
                                    <option value='7'>7월</option>
                                    <option value='8'>8월</option>
                                    <option value='9'>9월</option>
                                    <option value='10'>10월</option>
                                    <option value='11'>11월</option>
                                    <option value='12'>12월</option>
                                </select>  
                            </div>
                            <div class="col-sm-4 pdr0 pdl0 time_p">
                                <select class="form-control" id="half_select">
                                    <option value='1'>중간정산(1~15일)</option>
                                    <option value='2'>월말정산(16~말일)</option>
                                </select>                
                            </div>    
                        </div>
                        
                        <div class="col-sm-2">
                            <!-- <button id="btn_show" class="btn btn-default mgr12">검색</button> -->
                            <button id="btn_print" class="btn action_btn" >발행</button>
                        </div>
                        <div class="col-sm-1">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="tab-content">
    <div id="example" style="margin:0; ">
    <div id="grid_toolbar"></div>
        <div id="grid"></div>
    </div>
</div>  

<script type="text/javascript">
var $year_select;
var $month_select;
var $half_select;
var $company_select;
var $grid;
$(document).ready(function(){
    initVariables();
    initGrid();
    initCompanySelect();
    makeYear();
    fitGridSize();
    get_rengo_profit();

})

function initVariables() {
    $year_select=$('#year_select');
    $month_select=$('#month_select');
    $half_select=$('#half_select');
    $company_select=$('#company_select');
    $grid=$('#grid');
}

function initGrid() {
    $grid.kendoGrid({
        selectable: true,
        allowCopy: true, 
        sortable: true, 
        groupable: false, 
        resizable: true,
        pageable: {
            input: true,
            messages: {
                display: "총 {2} 데이터 중 {0}-{1}번째",
                empty: "데이타 없음"
            }
          },
        noRecords: {
            template: "해당 기간에 정산 내역이 없습니다."
          },
        dataSource: {
            transport: {
                 read: {
                   url: "",
                   dataType: "json"
                 }
               },
            schema: {
                model: {
                    fields: {
                        order_kind: {type: 'string'},
                        order_number: {type: 'string'},
                        serial: { type: "string" },
                        period_start: { type: "string" },
                        delivery_place: { type: "string" },
                        period_finish: { type: "string" },
                        pickup_place: { type: "string" },
                        car_name_detail: { type: "string" },
                        user_name: { type: "string" },
                        phone_number1: { type: "string" },
                        rental_price: { type: "number" },
                        price_off: { type: "number" },
                        insurance_price: { type: "number" },
                        option_price: { type: "number" },
                        total_price: { type: "number" }
                    }
                }
            },
            pageSize: 20
        },
        columns: [ 
             {
                field: "order_kind",
                title: "구분",
                width : 80
            }, 
            {
                field: "order_number",
                title: "예약번호",
                width: 100
            }, 
             {
                field: "period_start",
                title: "대여일시",
            }, 
             {
                field: "delivery_place",
                title: "대여장소",
            },
             {
                field: "period_finish",
                title: "반납일시",
            }, 
             {
                field: "pickup_place",
                title: "반납장소",
            },  
            {
                field: "car_name_detail",
                title: "세부모델명",
            },
            {
                field: "user_name",
                title: "고객명",
            },
            {
                field: "phone_number1",
                title: "연락처",
            },
             {
                field: "rental_price",
                title: "대여금액",
                format: "{0:##,#}원",
            },
            {
                field: "price_off",
                title: "할인금액",
                format: "{0:##,#}원",
            }, 
            {
                field: "insurance_price",
                title: "보험금액",
                format: "{0:##,#}원",
            }, 
            {
                field: "option_price",
                title: "옵션금액",
                format: "{0:##,#}원",
            },
            {
                field: "total_price",
                title: "총 결제금액",
                format: "{0:##,#}원",
            },   
           
        ]
    });    
}    



function get_rengo_profit(){

    var company_serial = get_company_serial();
    var year = $("#year_select option:selected").val();
    var month = $("#month_select option:selected").val();
    //1달 더하고 하루 뺌
    var start_date = new Date(parseInt(year), parseInt(month)-1,1);
    var end_date = new Date(start_date.getFullYear(), start_date.getMonth() + 1, start_date.getDate());
    end_date.setDate(end_date.getDate() - 1);

    var half = $("#half_select option:selected").val();
    var startStr = ''+year+zeroPad(month,2);
    var endStr = ''+year+zeroPad(month,2);
    if(half == 1){
        startStr += '010000';
        endStr += '152359';
    }else if(half == 2){
        startStr += '160000';
        endStr += zeroPad(end_date.getDate(),2) + '2359';
    }

    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/settlementstatement/get_settlementstatement/" + company_serial +"/" + startStr +"/"+ endStr,
                dataType: "json"
            }
        },
          schema: {
                model: {
                    fields: {
                        order_kind: {type: 'string'},
                        order_number: {type: 'string'},
                        serial: { type: "string" },
                        period_start: { type: "string" },
                        delivery_place: { type: "string" },
                        period_finish: { type: "string" },
                        pickup_place: { type: "string" },
                        car_name_detail: { type: "string" },
                        user_name: { type: "string" },
                        phone_number1: { type: "string" },
                        rental_price: { type: "number" },
                        price_off: { type: "number" },
                        insurance_price: { type: "number" },
                        option_price: { type: "number" },
                        total_price: { type: "number" }
                    }
                }
            },
            pageSize: 20
    });
    var grid = $("#grid").data("kendoGrid");
    grid.setDataSource(dataSource);
}

$("#btn_print").click(function(){

    var company_serial = get_company_serial();

    if(company_serial==0){
        alert('업체를 먼저 선택해 주세요.');
        return false;
    }
    var year = $("#year_select option:selected").val();
    var month = $("#month_select option:selected").val();
    //1달 더하고 하루 뺌
    var start_date = new Date(parseInt(year), parseInt(month)-1,1);
    var end_date = new Date(start_date.getFullYear(), start_date.getMonth() + 1, start_date.getDate());
    end_date.setDate(end_date.getDate() - 1);

    var half = $("#half_select option:selected").val();
    var startStr = ''+year+zeroPad(month,2);
    var endStr = ''+year+zeroPad(month,2);
    if(half == 1){
        startStr += '010000';
        endStr += '152359';
    }else if(half == 2){
        startStr += '160000';
        endStr += zeroPad(end_date.getDate(),2) + '2359';
    }

    window.open("/settlementstatement/print_calculation/" + company_serial +"/" + startStr +"/" + endStr);
        // $.post("/settlementstatement/print_calculation",
        //     {
        //             company_serial : company_serial,
        //     },
        //     function(data){
        //         alert(data);
        //             var result = JSON.parse(data);
        //             if(result.code=="S01"){
        //                 alert("정상 출력 되었습니다."); 
        //             }else{
        //                 alert(result.message);
        //             }

        // });
});

function makeYear() {
     var today = new Date();
     var year = today.getFullYear();
     var yearArr = new Array();

     for(i=0; i<15; i++){
          yearArr.push(year - i);
     }

    str='';
    for(i=0; i<yearArr.length; i++){
        str+= "<option value='" + yearArr[i] +"'>" + yearArr[i] +"년</option>";
    }
     $year_select.html(str);    
}

function initCompanySelect() {
    show_company_select();
    $company_select.change(function() {
        //
    })
}


function fitGridSize() {
    var gridElement = $grid;
    var gird_position = $grid.position();
    var window_height = $( window ).height() - gird_position.top - 50;
    gridElement.children(".k-grid-content").height(window_height-150);
    gridElement.height(window_height);
}

$(window).resize(function() {
    fitGridSize();
});


$('#year_select').change(function(){
        get_rengo_profit();
});

$('#month_select').change(function(){
        get_rengo_profit();
});

$('#half_select').change(function(){
        get_rengo_profit();
});



</script>
<!-- </body>



</html> -->
