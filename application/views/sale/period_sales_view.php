<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>
<script src="/telerik/examples/content/shared/js/console.js"></script>
<style type="text/css">
    html{overflow:hidden;}
</style>

<?php
    require("/home/apache/CodeIgniter-3.0.6/application/views/rengo_util.html");
?>

<div class="row" style="margin:0;">
    <div class="col-md-12" style="padding:0;">
        <div class="magb0 b_ccc bg_fff" style="border-bottom:none;">
            
            <div class="panel-heading pdt15 bb_ccc">
                <div class="row">
                    <div class="col-md-4">
                        <h3 class=" mgb10">월별 매출</h3>
                    </div>
                    <div class="col-sm-3 col-md-offset-5">
                        <div class="col-md-12 pdl0 pdr0">
                            <select class="form-control" id="company_select">
                                <?php foreach($company_list as $company):?>
                                    <option value="<?php echo $company['serial']; ?>"> <?php echo $company['company_name']; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel magb0 border_none ">
                <div class="panel-heading border_none pdt14 pdb13">
                    <div class="row">
                        
                        <div class="col-sm-1">
                          <a class="btn btn-default" href="#" role="button" onclick="saveExcel();">엑셀로 저장</a>
                        </div>

                        <div class="col-sm-7">
                            <div class="btn-group" role="group" aria-label="...">
                                <a role="button" type="button" class="btn pdl35 pdr35 bg_main" href="#" id="receipt_day">입금일 기준</a>
                                <a role="button" type="button" class="btn pdl35 pdr35" href="#" id="order_day">예약일 기준</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="row" style="margin:0;">
    <div class="col-md-12" style="padding:0;">
                
        <div class="panel-footer section_lnb bl_ccc br_ccc">
            <ul class="nav nav-tabs border_none" id="month_tab">
                <li name="li_year" class="pdt15 pdr15">
                    <select class="form-control" id="year_select" >
                    </select>
                </li>
                <li name="0" class="active"><a data-toggle="tab" href="#" id="tab_전체">전체</a></li>
                <li name="1"><a data-toggle="tab" href="#" id="tab_1월">1월</a></li>
                <li name="2"><a data-toggle="tab" href="#" id="tab_2월">2월</a></li>
                <li name="3"><a data-toggle="tab" href="#" id="tab_3월">3월</a></li>
                <li name="4"><a data-toggle="tab" href="#" id="tab_4월">4월</a></li>
                <li name="5"><a data-toggle="tab" href="#" id="tab_5월">5월</a></li>
                <li name="6"><a data-toggle="tab" href="#" id="tab_6월">6월</a></li>
                <li name="7"><a data-toggle="tab" href="#" id="tab_7월">7월</a></li>
                <li name="8"><a data-toggle="tab" href="#" id="tab_8월">8월</a></li>
                <li name="9"><a data-toggle="tab" href="#" id="tab_9월">9월</a></li>
                <li name="10"><a data-toggle="tab" href="#" id="tab_10월">10월</a></li>
                <li name="11"><a data-toggle="tab" href="#" id="tab_11월">11월</a></li>
                <li name="12"><a data-toggle="tab" href="#" id="tab_12월">12월</a></li>
            </ul>
        </div>

    </div>
</div>



<div class="tab-content">
    <div id="example" style="margin:0;">
        <div id="grid"></div>
    </div>
</div>

<script type="text/javascript">

var $year_select;
var $grid;
var $detail_grid;
var $company_select;
var $month_tab;
var $order_day;
var $receipt_day;
var DAYDELIMITER;

$(document).ready(function() {
    initVariables();
    makeYear();
    initCompanySelect();
    initGrid($grid);
    initDetailGrid($detail_grid);
    initVariables();
    fitGridSize();
    yearSelectChange();
    // gridDoubleClick();
    monthTabSetting();

    $(window).resize(function() {
        fitGridSize();
    });

    $order_day.click(function(){
        refreshGrid('orderday_period_sales','0');
        initTabACtive('order_day');
    });

    $receipt_day.click(function(){
        refreshGrid('period_sales','0');
        initTabACtive('receipt_day');
    });

});

function initVariables() {
    $year_select=$('#year_select');
    $grid=$('#grid');
    $detail_grid=$('#detail_grid');
    $company_select=$('#company_select');
    $month_tab=$('#month_tab');
    $order_day=$('#order_day');
    $receipt_day=$('#receipt_day');
    DAYDELIMITER='period_sales';
}

function makeYear() {
     var today = new Date();
     var year = today.getFullYear();
     var yearArr = new Array();

     for(i=0; i<15; i++){
          yearArr.push(year - i);
     }

    str='';
    for(i=0; i<yearArr.length; i++){
        str+= "<option value='" + yearArr[i] +"'>" + yearArr[i] +"년</option>";
    }
     $year_select.html(str);    
}

function yearSelectChange(){
    $('#year_select').change(function() {
        refreshGrid('period_sales',0);
    })    
}

function initTabACtive(tab) {
    $month_tab.children('.active').removeClass('active');
    $month_tab.children().eq(1).addClass('active');
    
    if(tab == 'order_day') {
        $order_day.addClass('bg_main');
        $receipt_day.removeClass('bg_main');
    } else if(tab == 'receipt_day') {
        $order_day.removeClass('bg_main');
        $receipt_day.addClass('bg_main');
    }
}

function monthTabSetting(){
    $month_tab.children().click(function(){
        var day_delimiter = $receipt_day.hasClass('bg_main') ? 'period_sales_per_month' : 'orderday_period_sales_per_month';
        var month = $(this).attr('name');
        if(month != 'li_year')
            refreshGrid(day_delimiter,month);
    });    
}

function initGrid($grid){
    $grid.kendoGrid({
        selectable: true,
        allowCopy: true, 
        sortable: true, 
        groupable: false, 
        resizable: true,
        excel: {
                allPages: true,
                fileName: "기간별 매출현황.xlsx",
                filterable: true
            },
        pageable: {
            input: true,
            messages: {
                display: "총 {2} 건의 자료 중 {0}~{1}번째",
                    empty: "No data"
                }
            },
            // noRecords: {
            //     template: "현재 페이지에서 보여줄 내용이 없습니다."
            // },
            dataSource: {
                transport: {
                        read: {
                           url: "",
                           dataType: "json"
                        }
                    },
                schema: {
                    model: {
                        fields: {
                            month: {type: 'number'},
                            classify: { type: "string" },
                            total_count: { type: "number" },
                            total_sale: {type:"number"},
                            normal_count: {type:"number"},
                            normal_sale: { type: "number" }, 
                            midterm_count: { type: "number" }, 
                            midterm_sale: { type: "number" }, 
                            longterm_count: { type: "number" },
                            longterm_sale: { type: "number" },
                            insurancebalance_count: { type: "number" }, 
                            insurancebalance_sale: { type: "number" }, 
                            note: {type:"string"},
                            license_type: { type: "string" }, 
                            rental_charge: { type: "number" }, 
                            sale_charge: { type: "number" }, 
                            period_discount : {type : "number"},    
                            special_period_discount : {type : "number"},
                            other_discount : {type : "number"},
                            car_allocation : {type : "number"},
                            car_return : {type : "number"},
                            car_allocation_return : {type : "number"},
                            insurance_self1 : {type : "number"},
                            insurance_self2 : {type : "number"},
                            insurance_self3 : {type : "number"},
                            excess_charge : { type: "number" }, 
                            refund_charge : { type: "number" }, 
                            car_seat : { type: "number" }, 
                            snow_chain : { type: "number" }, 
                            other_paid_option : { type: "number" }, 
                            repair_cost : { type: "number" }, 
                            loss_cost_billing : { type: "number" }, 
                            accident_exemption_cost : { type: "number" }, 
                            accident_compensation : { type: "number" }, 
                            fuel_cost : { type: "number" }, 
                            fine : { type: "number" }, 
                            security_deposit : { type: "number" }, 
                            others : { type: "number" },
                            outstanding_amount : {type:'number'}
                        }
                    }
                },
                pageSize: 40
            },
            columns: [{
                    field: "classify",
                    title: "구분",
                    template: "<strong>#: classify # </strong>",
                    width: 80,
                    locked: true,
                    lockable: false,
                },  {
                    title: "총 매출현황",
                        columns: [{
                            field: "total_count",
                            title: "건수",
                            width: 80    
                        }, {
                            field: "total_sale",
                            title: "금액",
                            format: "{0:n0}",
                            width: 140
                            }]
                },  {
                    title: "일반 매출현황",
                    columns: [{
                        field: "normal_count",
                        title: "건수",
                        width: 80   
                    }, {
                        field: "normal_sale",
                        title: "금액",
                        format: "{0:n0}",
                        width: 150
                    }]
                },  {
                    title: "중기 매출현황",
                    columns: [{
                        field: "midterm_count",
                        title: "건수",
                        width: 80
                    }, {
                        field: "midterm_sale",
                        title: "금액",
                        format: "{0:n0}",
                        width: 150
                    }]
                },  {
                    title: "장기 매출현황",
                    columns: [{
                        field: "longterm_count",
                        title: "건수",
                        width: 80
                    }, {
                        field: "longterm_sale",
                        title: "금액",
                        format: "{0:n0}",
                        width: 150
                    }]
                },  {
                    title: "보험대차 매출현황",
                    columns: [{
                        field: "insurancebalance_count",
                        title: "건수",
                        width: 80
                    }, {
                        field: "insurancebalance_sale",
                        title: "금액",
                        format: "{0:n0}",
                        width: 150
                    }]
                },  {
                    field: "total_sale",
                    title: "총결제금액",
                    format: "{0:n0}",
                    width: 140
                },  {
                    title: "할인금액",
                    columns: [{
                        field: "period_discount",
                        title: "대여기간할인",
                        format: "{0:n0}",
                        width: 120
                    }, {
                        field: "special_period_discount",
                        title: "특정기간할인",
                        format: "{0:n0}",
                        width: 120
                    }, {
                        field:"other_discount",
                        title:"기타할인",
                        format: "{0:n0}",
                        width: 120
                    }]
                },  {
                    title: "배회차금액",
                    columns: [{
                        field: "car_allocation",
                        title: "배차",
                        width: 100
                    }, {
                        field: "car_return",
                        title: "회차",
                        format: "{0:n0}",
                        width: 100
                    }, {
                        field:"car_allocation_return",
                        title:"배회차",
                        format: "{0:n0}",
                        width: 100
                    }]
                },  {
                    title: "보험긍액",
                    columns: [{
                        field: "insurance_self1",
                        title: "자차1",
                        width: 100
                    }, {
                        field: "insurance_self2",
                        title: "자차2",
                        format: "{0:n0}",
                        width: 100
                    }, {
                        field:"insurance_self3",
                        title:"자차3",
                        format: "{0:n0}",
                        width: 100
                    }]
                },  {
                    title: "초과/환불",
                    columns: [{
                        field: "excess_charge",
                        title: "초과",
                        width: 100
                    }, {
                        field: "refund_charge",
                        title: "환불",
                        format: "{0:n0}",
                        width: 100
                    }]
                },  {
                    title: "유료옵션",
                    columns: [{
                        field: "car_seat",
                        title: "카시트",
                        width: 100
                    }, {
                        field: "snow_chain",
                        title: "스노우체인",
                        format: "{0:n0}",
                        width: 100
                    }, {
                        field: "other_paid_option",
                        title: "기타유료옵션",
                        format: "{0:n0}",
                        width: 120
                    }]
                },  {
                    title: "사고.수리",
                    columns: [{
                        field: "repair_cost",
                        title: "수리비",
                        width: 100
                    }, {
                        field: "loss_cost_billing",
                        title: "청구손실비",
                        format: "{0:n0}",
                        width: 100
                    }, {
                        field: "accident_exemption_cost",
                        title: "사고면책금",
                        format: "{0:n0}",
                        width: 100
                    }, {
                        field: "accident_compensation",
                        title: "사고보상금",
                        format: "{0:n0}",
                        width: 100
                    }]
                },  {
                    title: "기타",
                    columns:[{
                        field: "fuel_cost",
                        title: "유류비용",
                        format: "{0:n0}",
                        width:100
                    }, {
                        field: "fine",
                        title: "범칙금",
                        format: "{0:n0}",
                        width:100
                    }, {
                        field: "security_deposit",
                        title: "보증금",
                        format: "{0:n0}",
                        width:100
                    }, {
                        field: "others",
                        title: "기타",
                        format: "{0:n0}",
                        width:100
                    }]
                },  {
                    field: "total_sale",
                    title: "총 입금금액",
                    format: "{0:n0}",
                    width: 100
                },  {
                    field: "outstanding_amount",
                    title: "총 미수금액",
                    format: "{0:n0}",
                    width: 100
                },  {
                    field: "",
                    title: "공급가액",
                    format: "{0:n0}",
                    width: 100
                },  {
                    field: "published_date",
                    title: "부가세액",
                    format: "{0:n0}",
                    width: 100
                },  {
                    field: "license_number",
                    title: "운행율",
                    format: "{0:n0}",
                    width: 100
                },  {
                    field: "",
                    title: "운행시간",
                    format: "{0:n0}",
                    width: 100
                }
            ]
        });

    refreshGrid('period_sales',0);
}

function refreshGrid(sale_way,month) {
    var company_serial=get_company_serial();
    var grid = $grid.data("kendoGrid");
    var year = $('#year_select option:selected').val();
    console.log("/salemanager/sale_way/"+company_serial+"/"+sale_way+"/"+year+"/"+month);
    var dataSource = new kendo.data.DataSource({
         transport: {
             read: {
                url: "/salemanager/sale_way/"+company_serial+"/"+sale_way+"/"+year+"/"+month,
                dataType: "json"
             }
           },
           pageSize: 32
    });

    grid.setDataSource(dataSource);
    
    setTimeout(function(){
        grid.refresh();
    },300);
}

function saveExcel(){
    $grid.data('kendoGrid').saveAsExcel();
}

function initDetailGrid($detail_grid){
    // $detail_grid.kendoGrid({
    //     navigatable: true,
    //     selectable: true,
    //     allowCopy: true, 
    //     sortable: true, 
    //     filterable: true, 
    //     groupable: false, 
    //     resizable: true,
    //     excel: {
    //         allPages: true,
    //         fileName: "월별 매출현황.xlsx",
    //         filterable: true
    //     },
    //     pageable: {
    //         input: true,
    //         messages: {
    //             display: "Showing {0}-{1} from {2} data items",
    //             empty: "No data"
    //         }
    //       },
    //     noRecords: {
    //         template: "현재 페이지에서 보여줄 내용이 없습니다."
    //       },
    //     columnMenu: {
    //         sortable: false,
    //         messages: {
    //             columns: "표시할 항목 선택",
    //             filter: "필터",
    //         }
    //      },
    //     dataSource: {
    //         transport: {
    //              read: {
    //                url: "",
    //                dataType: "json"
    //              }
    //            },
    //         schema: {
    //             model: {
    //                 fields: {
    //                     serial: {type: 'number'},
    //                     company_name: { type: "string" },
    //                     car_number: { type: "string" },
    //                     period: { type: "string" },
    //                     user_name: { type: "string" },
    //                     birthday: { type: "string" },
    //                     phone_number1: { type: "string" },
    //                     place: { type: "string" },
    //                     rental_price: { type: "number" },
    //                     insurance_price: { type: "number" },
    //                     price_off: { type: "number" },
    //                     deposite_amount: { type: "number" },
    //                     deposite_date: { type: "date" },

    //                 }
    //             }
    //         },
    //         pageSize: 30
    //     },
    //     columns: [ 
    //         {
    //             field: "serial",
    //             title: "주문번호",
    //             // width: 120
    //         },
    //         {
    //             field: "deposite_date",
    //             title: "예약일자",
    //             format: "{0:yyyy년 M월 dd일}",
    //             width: 150
    //         },    
    //         {
    //             field: "company_name",
    //             title: "파트너명",
    //             width : 120
    //         },
    //         {
    //             field: "car_number",
    //             title: "차량정보",
    //         },
    //         {
    //             field: "period",
    //             title: "이용일자",
    //             width: 300
    //         },
    //         {
    //             field: "user_name",
    //             title: "예약자명",
    //         },
    //         {
    //             field: "phone_number1",
    //             title: "전화번호",
    //         },
    //         {
    //             field: "place",
    //             title: "배회차 지역",
    //             width: 200
    //         },
    //          {
    //             field: "rental_price",
    //             title: "대여금액",
    //             format: "{0:##,#}"
    //         },
    //         {
    //             field: "insurance_price",
    //             title: "보험금액",
    //             format: "{0:##,#}"
    //         },
    //         {
    //             field: "price_off",
    //             title: "할인금액",
    //             format: "{0:##,#}"
    //         },
    //         {
    //             field: "deposite_amount",
    //             title: "결제금액",
    //             format: "{0:##,#}"
    //         },
    //     ]
    // });

}

function initCompanySelect() {
   show_company_select(); 
    $company_select.change(function() {
        refreshGrid('period_sales',0);
    })
}



function fitGridSize() {
    var gridElement = $grid;
    var gird_position = $grid.position();
    var window_height = $( window ).height() - gird_position.top - 50;
    gridElement.children(".k-grid-content").height(window_height-150);
    gridElement.height(window_height);

    var detailGridElement = $detail_grid;
    detailGridElement.children(".k-grid-content").height(window_height-150);
    detailGridElement.height(window_height);
}




function refresh_detail_grid(str){

    var company_serial = get_company_serial();

    var year = $("#year_select option:selected").val();

    var start_date = new Date(parseInt(year), parseInt(str)-1,1);

    //1달 더하고 하루 뺌
    var end_date = new Date(start_date.getFullYear(), start_date.getMonth() + 1, start_date.getDate());
    end_date.setDate(end_date.getDate() - 1);
    var start_date_str = start_date.getFullYear() +"-" + zeroPad((start_date.getMonth()+1),2) + "-" + zeroPad(start_date.getDate(),2);
    var end_date_str = end_date.getFullYear() +"-" + zeroPad((end_date.getMonth()+1),2) + "-" + zeroPad(end_date.getDate(),2);

    var url = "/salemanager/get_detail_reservation/" + company_serial+"/"+ start_date_str +"/" + end_date_str;
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json"
            }
        },
        pageSize: 30,
        schema: {
            model: {
                fields: {
                    serial: {type: 'number'},
                    company_name: { type: "string" },
                    car_number: { type: "string" },
                    period: { type: "string" },
                    user_name: { type: "string" },
                    birthday: { type: "string" },
                    phone_number1: { type: "string" },
                    place: { type: "string" },
                    rental_price: { type: "number" },
                    insurance_price: { type: "number" },
                    price_off: { type: "number" },
                    deposite_amount: { type: "number" },
                    deposite_date: { type: "date" },

                }
            }
        }
    });
    var grid = $detail_grid.data("kendoGrid");
    grid.setDataSource(dataSource);

    setTimeout(function(){
      grid.refresh();  
    },300);

}



function tab_select(str){
    // if(str == '전체'){
    //     $grid.show();
    //     $detail_grid.hide();
    // }else{
    //     $grid.hide();
    //     $detail_grid.show();
    //     refresh_detail_grid(str);
    // }
}


// function gridDoubleClick(){
//     $grid.delegate("tbody>tr", "dblclick",
//       function(){
//             var grid = $grid.data("kendoGrid");
//             var row = grid.select();
//             var data = grid.dataItem(row);

//             $('#tab_' + data.classify).trigger('click');
//     });    
// }



</script>
