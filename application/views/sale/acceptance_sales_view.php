<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>
<script src="/telerik/examples/content/shared/js/console.js"></script>
<style type="text/css">
    html{overflow:hidden;}
</style>
<?php
    require("/home/apache/CodeIgniter-3.0.6/application/views/rengo_util.html");
?>

<div class="row">
    <div class="col-md-12">
        <div class="magb0 b_ccc bg_fff bb0" >
            
            <div class="panel-heading pdt15 bb_ccc">
                <div class="row">
                    <div class="col-md-4">
                        <h3 class="mgb10">수납 방식별 매출현황</h3>
                    </div>
                    <div class="col-sm-3 col-md-offset-5">
                        <div class="col-md-12 pdl0 pdr0">
                            <select class="form-control" id="company_select">
                                <?php foreach($company_list as $company):?>
                                    <option value="<?php echo $company['serial']; ?>"> <?php echo $company['company_name']; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel magb0 border_none">
                <div class="panel-heading border_none pdt14 pdb13">
                    <div class="row">

                        <div class="col-sm-1">
                          <a class="btn btn-default" href="#" role="button" onclick="javascript:saveExcel();">엑셀로 저장</a>
                        </div>

                        <div class="col-sm-7">
                            <div class="btn-group" role="group" aria-label="...">
                                <a role="button" type="button" class="btn pdl35 pdr35 bg_main" href="#" id="receipt_day">입금일 기준</a>
                                <a role="button" type="button" class="btn pdl35 pdr35" href="#" id="order_day">예약일 기준</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
                
        <div class="panel-footer section_lnb bl_ccc br_ccc">
            <ul id="month_tab" class="nav nav-tabs border_none">
                <li class="pdt15 pdr15">
                    <select class="form-control" id="year_select" >
                    </select>
                </li>
                <li class="active"><a data-toggle="tab" href="#" id="period_sales" onclick="select_month('')">전체</a></li>
                <li><a data-toggle="tab" href="#" id="period_sales" onclick="select_month('1')">1월</a></li>
                <li><a data-toggle="tab" href="#" id="period_sales" onclick="select_month('2')">2월</a></li>
                <li><a data-toggle="tab" href="#" id="period_sales" onclick="select_month('3')">3월</a></li>
                <li><a data-toggle="tab" href="#" id="period_sales" onclick="select_month('4')">4월</a></li>
                <li><a data-toggle="tab" href="#" id="period_sales" onclick="select_month('5')">5월</a></li>
                <li><a data-toggle="tab" href="#" id="period_sales" onclick="select_month('6')">6월</a></li>
                <li><a data-toggle="tab" href="#" id="period_sales" onclick="select_month('7')">7월</a></li>
                <li><a data-toggle="tab" href="#" id="period_sales" onclick="select_month('8')">8월</a></li>
                <li><a data-toggle="tab" href="#" id="period_sales" onclick="select_month('9')">9월</a></li>
                <li><a data-toggle="tab" href="#" id="period_sales" onclick="select_month('10')">10월</a></li>
                <li><a data-toggle="tab" href="#" id="period_sales" onclick="select_month('11')">11월</a></li>
                <li><a data-toggle="tab" href="#" id="period_sales" onclick="select_month('12')">12월</a></li>
            </ul>
        </div>

    </div>
</div>


<div class="tab-content">
    <div id="example" style="margin:0; ">
    
        <div id="grid"></div>
    </div>
</div>
<script type="text/javascript">
var $grid;
var $year_select;
var $company_select;
var $order_day;
var $receipt_day;
var $month_tab;

$(document).ready(function() {
    initVariables();
    makeYear();
    initCompanySelect();
    yearSelectChange($year_select);
    initGrid($grid);
    fitGridSize();
    select_month('');

    $order_day.click(function(){
        // refreshGrid('orderday_period_sales','0');
        initTabACtive('order_day');
    });

    $receipt_day.click(function(){
        // refreshGrid('period_sales','0');
        initTabACtive('receipt_day');
    });
})

function initVariables() {
    $year_select=$('#year_select');
    $company_select=$('#company_select');
    $grid=$('#grid');
    $order_day=$('#order_day');
    $receipt_day=$('#receipt_day');
    $month_tab=$('#month_tab');
}

function makeYear() {
    var today = new Date();
    var year = today.getFullYear();
    var yearArr = new Array();

    for(i=0; i<4; i++){
        yearArr.push(year - i);
    }

    str='';
    for(i=0; i<yearArr.length; i++){
        str+= "<option value='" + yearArr[i] +"'>" + yearArr[i] +"년</option>";
    }
    $year_select.html(str);
}

function initTabACtive(tab) {
    $month_tab.children('.active').removeClass('active');
    $month_tab.children().eq(1).addClass('active');

    if(tab == 'order_day') {
        $order_day.addClass('bg_main');
        $receipt_day.removeClass('bg_main');
    } else if(tab == 'receipt_day') {
        $order_day.removeClass('bg_main');
        $receipt_day.addClass('bg_main');
    }
    
    
}

function initGrid($grid) {
    $grid.kendoGrid({
        selectable: true,
        allowCopy: true, 
        sortable: true, 
        groupable: false,
        excel: {
                allPages: true,
                fileName: "수납 방식별 매출현황.xlsx",
                filterable: true
            }, 
          pageable: {
            input: true,
            messages: {
                display: "총 {2} 명의 고객 중 {0}~{1}번째",
                    empty: "No data"
                }
            },
        noRecords: {
            template: "현재 페이지에서 보여줄 내용이 없습니다."
          },
        dataSource: {
            transport: {
                 read: {
                   url: "",
                   dataType: "json"
                 }
               },
            schema: {
                model: {
                    fields: {
                        month: {type: 'number'},
                        classify: { type: "string" },
                        total_count: { type: "number" },
                        total_sale: {type:"number"},
                        normal_count: {type:"number"},
                        normal_sale: { type: "number" }, 
                        midterm_count: { type: "number" }, 
                        midterm_sale: { type: "number" }, 
                        longterm_count: { type: "number" },
                        longterm_sale: { type: "number" },
                        insurancebalance_count: { type: "number" }, 
                        insurancebalance_sale: { type: "number" }, 
                        note: {type:"string"},
                        license_type: { type: "string" }, 
                        rental_charge: { type: "number" }, 
                        sale_charge: { type: "number" }, 
                        period_discount : {type : "number"},    
                        special_period_discount : {type : "number"},
                        other_discount : {type : "number"},
                        car_allocation : {type : "number"},
                        car_return : {type : "number"},
                        car_allocation_return : {type : "number"},
                        insurance_self1 : {type : "number"},
                        insurance_self2 : {type : "number"},
                        insurance_self3 : {type : "number"},
                        excess_charge : { type: "number" }, 
                        refund_charge : { type: "number" }, 
                        car_seat : { type: "number" }, 
                        snow_chain : { type: "number" }, 
                        other_paid_option : { type: "number" }, 
                        repair_cost : { type: "number" }, 
                        loss_cost_billing : { type: "number" }, 
                        accident_exemption_cost : { type: "number" }, 
                        accident_compensation : { type: "number" }, 
                        fuel_cost : { type: "number" }, 
                        fine : { type: "number" }, 
                        security_deposit : { type: "number" }, 
                        others : { type: "number" },
                        outstanding_amount : {type : 'number'} 
                    }
                }
            },
            pageSize: 30
        },
        columns: [{
                    field: "classify",
                    title: "구분",
                    template: "<strong>#: classify # </strong>",
                    width: 150,
                    locked: true,
                    lockable: false,
                },  {
                    title: "총 매출현황",
                        columns: [{
                            field: "total_count",
                            title: "건수",
                            width: 80    
                        }, {
                            field: "total_sale",
                            title: "금액",
                            format: "{0:n0}",
                            width: 140
                            }]
                },  {
                    title: "일반 매출현황",
                    columns: [{
                        field: "normal_count",
                        title: "건수",
                        width: 80   
                    }, {
                        field: "normal_sale",
                        title: "금액",
                        format: "{0:n0}",
                        width: 150
                    }]
                },  {
                    title: "중기 매출현황",
                    columns: [{
                        field: "midterm_count",
                        title: "건수",
                        width: 80
                    }, {
                        field: "midterm_sale",
                        title: "금액",
                        format: "{0:n0}",
                        width: 150
                    }]
                },  {
                    title: "장기 매출현황",
                    columns: [{
                        field: "longterm_count",
                        title: "건수",
                        width: 80
                    }, {
                        field: "longterm_sale",
                        title: "금액",
                        format: "{0:n0}",
                        width: 150
                    }]
                },  {
                    title: "보험대차 매출현황",
                    columns: [{
                        field: "insurancebalance_count",
                        title: "건수",
                        width: 80
                    }, {
                        field: "insurancebalance_sale",
                        title: "금액",
                        format: "{0:n0}",
                        width: 150
                    }]
                },  {
                    field: "total_sale",
                    title: "총결제금액",
                    format: "{0:n0}",
                    width: 100
                },  {
                    title: "할인금액",
                    columns: [{
                        field: "period_discount",
                        title: "대여기간할인",
                        width: 120
                    }, {
                        field: "special_period_discount",
                        title: "특정기간할인",
                        format: "{0:n0}",
                        width: 120
                    }, {
                        field:"other_discount",
                        title:"기타할인",
                        format: "{0:n0}",
                        width: 120
                    }]
                },  {
                    title: "배회차금액",
                    columns: [{
                        field: "car_allocation",
                        title: "배차",
                        width: 100
                    }, {
                        field: "car_return",
                        title: "회차",
                        format: "{0:n0}",
                        width: 100
                    }, {
                        field:"car_allocation_return",
                        title:"배회차",
                        format: "{0:n0}",
                        width: 100
                    }]
                },  {
                    title: "보험긍액",
                    columns: [{
                        field: "insurance_self1",
                        title: "자차1",
                        width: 100
                    }, {
                        field: "insurance_self2",
                        title: "자차2",
                        format: "{0:n0}",
                        width: 100
                    }, {
                        field:"insurance_self3",
                        title:"자차3",
                        format: "{0:n0}",
                        width: 100
                    }]
                },  {
                    title: "초과/환불",
                    columns: [{
                        field: "excess_charge",
                        title: "초과",
                        width: 100
                    }, {
                        field: "refund_charge",
                        title: "환불",
                        format: "{0:n0}",
                        width: 100
                    }]
                },  {
                    title: "유료옵션",
                    columns: [{
                        field: "car_seat",
                        title: "카시트",
                        width: 100
                    }, {
                        field: "snow_chain",
                        title: "스노우체인",
                        format: "{0:n0}",
                        width: 100
                    }, {
                        field: "other_paid_option",
                        title: "기타유료옵션",
                        format: "{0:n0}",
                        width: 120
                    }]
                },  {
                    title: "사고.수리",
                    columns: [{
                        field: "repair_cost",
                        title: "수리비",
                        width: 100
                    }, {
                        field: "loss_cost_billing",
                        title: "청구손실비",
                        format: "{0:n0}",
                        width: 100
                    }, {
                        field: "accident_exemption_cost",
                        title: "사고면책금",
                        format: "{0:n0}",
                        width: 100
                    }, {
                        field: "accident_compensation",
                        title: "사고보상금",
                        format: "{0:n0}",
                        width: 100
                    }]
                },  {
                    title: "기타",
                    columns:[{
                        field: "fuel_cost",
                        title: "유류비용",
                        format: "{0:n0}",
                        width:100
                    }, {
                        field: "fine",
                        title: "범칙금",
                        format: "{0:n0}",
                        width:100
                    }, {
                        field: "security_deposit",
                        title: "보증금",
                        format: "{0:n0}",
                        width:100
                    }, {
                        field: "others",
                        title: "기타",
                        format: "{0:n0}",
                        width:100
                    }]
                },  {
                    field: "total_sale",
                    title: "총 입금금액",
                    format: "{0:n0}",
                    width: 100
                },  {
                    field: "outstanding_amount",
                    title: "총 미수금액",
                    format: "{0:n0}",
                    width: 100
                },  {
                    field: "",
                    title: "공급가액",
                    format: "{0:n0}",
                    width: 100
                },  {
                    field: "published_date",
                    title: "부가세액",
                    format: "{0:n0}",
                    width: 100
                },  {
                    field: "license_number",
                    title: "운행율",
                    format: "{0:n0}",
                    width: 100
                },  {
                    field: "",
                    title: "운행시간",
                    format: "{0:n0}",
                    width: 100
                }
            ]
    });
    select_sale_way('acceptance_sales');
}

function initCompanySelect() {
    show_company_select();
    $company_select.change(function() {
        select_sale_way('acceptance_sales');
    })
}


function select_month(month) {
    var company_serial = get_company_serial();
    var year = $("#year_select option:selected").val();
    var grid = $("#grid").data("kendoGrid");

    var dataSource = new kendo.data.DataSource({
         transport: {
             read: {
               url: "/salemanager/sale_way/"+company_serial+"/acceptance_sales/"+year+"/"+month,
               dataType: "json"
             }
           }
    });
    
    grid.setDataSource(dataSource);
    
    setTimeout(function(){
      grid.refresh();  
    },300);
}

function select_sale_way(sale_way){
    var company_serial = get_company_serial();    
    var grid = $("#grid").data("kendoGrid");
    var year = $("#year_select option:selected").val();
    console.log("/salemanager/sale_way/"+company_serial+"/"+sale_way+"/"+year+"/0");
    var dataSource = new kendo.data.DataSource({
         transport: {
             read: {
               url: "/salemanager/sale_way/"+company_serial+"/"+sale_way+"/"+year+"/0",
               dataType: "json"
             }
           },
           pageSize: 30
    });

    grid.setDataSource(dataSource);

    setTimeout(function(){
      grid.refresh();  
    },300);
}

function fitGridSize() {
    var gridElement = $grid;
    var gird_position = $grid.position();
    var window_height = $( window ).height() - gird_position.top - 50;
    gridElement.children(".k-grid-content").height(window_height-150);
    gridElement.height(window_height);
}

$(window).resize(function() {
    fitGridSize();
});

function yearSelectChange($year_select){
    $year_select.change(function() {
        select_sale_way("acceptance_sales");
    })    
}

</script>
<!-- </body>



</html> -->
