<div class="modal fade" id="dialog_schedule_info" role="dialog" >
	<div class="modal-dialog modal-sm" >
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 id="dialog_title" class="modal-title">스케줄 관리</h4>
			</div>

			<div class="modal_scroll">
				<div class="modal-body br_ccc">
					<div class="row rent_input">
						<div class="col-sm-12">

						<div class="col-sm-6">

							<div class="row">
								<div class="col-ms-12">
									<form class="form-horizontal">
										<div class="form-group">
											<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">고객명</label>
											<div class="col-sm-10">
												<input type="text" class="form-control"  id="user_name_input" disabled >
											</div>
										</div>
									</form>
								</div>
							</div>

							<div class="row">
								<div class="col-ms-12">
									<form class="form-horizontal">
										<div class="form-group">
											<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">고객 전화번호</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="user_phone_input" disabled >
											</div>
										</div>
									</form>
								</div>
							</div>

							<div class="row">
								<div class="col-ms-12">
									<form class="form-horizontal">
										<div class="form-group">
											<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">구분</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="type_input" disabled >
											</div>
										</div>
									</form>
								</div>
							</div>

						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="col-ms-12">
									<form class="form-horizontal">
										<div class="form-group">
											<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">차량번호</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="car_number_input" disabled >
											</div>
										</div>
									</form>
								</div>
							</div>

							<div class="row">
								<div class="col-ms-12">
									<form class="form-horizontal">
										<div class="form-group">
											<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">날짜</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="date_input" disabled >
											</div>
										</div>
									</form>
								</div>
							</div>

							<div class="row">
								<div class="col-ms-12">
									<form class="form-horizontal">
										<div class="form-group">
											<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">지역</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="place_input" disabled >
											</div>
										</div>
									</form>
								</div>
							</div>
						
						</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
			</div>

		</div>
	</div>
</div>