<style type="text/css">
	.modal-dialog{width: 500px;}

</style>	

<div class="modal fade" id="dialog_by_day" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 id="dialog_title" class="modal-title">스케줄 관리</h4>
			</div>

				<div class="modal-body br_ccc">
					<div class="row rent_input">
						<div class="col-sm-12">
						
							<div class="row">
								
									<form class="form-horizontal">
										<div class="form-group">
											<label for="" class="col-sm-3 control-label text-left">배차유무</label>
											<div class="col-sm-9">
												<div class="checkbox">
													<div class="insurance_item pull-left">
													  <input type="checkbox" id="car_checkbox_person1">
													  <label for="car_checkbox_person1">
													  </label>
													</div>
												</div>
											</div>
										</div>
									</form>
								
							</div>

							<div class="row">
								
									<form class="form-horizontal">
										<div class="form-group">
											<label for="" class="col-sm-3 control-label text-left">시간설정</label>
											<div class="col-sm-9">
								              <ul class="operating_time">
								                <li>
								                  <div class="input-group date" id="partner_time_st">
								                    <input type="text" id="partner_time_st_input" class="form-control input-lg" placeholder="개점 시간">
								                    <span class="input-group-addon">
								                      <span class="glyphicon glyphicon-time"></span>
								                    </span>
								                  </div>
								                </li>
								                <li>~</li>
								                <li>
								                  <div class="input-group date" id="partner_time_end">
								                    <input type="text" id="partner_time_end_input" class="form-control input-lg" placeholder="폐점 시간">
								                    <span class="input-group-addon">
								                      <span class="glyphicon glyphicon-time"></span>
								                    </span>
								                  </div>
								                </li>
								              </ul>
											</div>
										</div>
									</form>
							</div>

							<div class="row">
								<form class="form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-3 control-label text-left">시간당 배차가능 횟수</label>
										<div class="col-sm-9 percent">
											<input type="number" class="form-control" id="" >
											<span>건</span>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
				<button type="button" class="btn action_btn" >저장</button>
			</div>

		</div>
	</div>
</div>
