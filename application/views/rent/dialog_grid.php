<script language="javascript">

//수납내역 grid
	$("#acceptance_grid").kendoGrid({
		selectable: true, 
		allowCopy: true, 
		height: 340, 
		sortable: true, 
		groupable: false, 
		resizable: true,
		pageable: false,
		noRecords: {
			  		template: "수납 내역이 없습니다."
		},

		dataSource: {
			 	transport: {
			 		read: {
			 			url: '',
			 			dataType: "json"
			 		}
		},
		schema: {
			 		model: {
			 			fields: {
			 				serial : { type: "number"},
			 				username : { type: "string" }, 
			 				deposite_date: { type: "date" },
			 				type: { type: "string" }, 
			 				deposite_way: { type: "string" },
			 				deposite_amount : {type : "number"}, 
			 				tax_paper: { type: "string" }, 
			 				memo: { type: "string" },
			 				handler: { type: "string" },
			 				deposite_registration_date: { type: "string" }, 
			 			}
			 		}
			 	},
			 	pageSize: 30
			 },

			 columns: [ {
			 	field: "deposite_date",
			 	title: "입금일시",
			 	format: "{0:yyyy-M-dd}",
			 	width: 120
			 },  {
			 	field: "type",
			 	title: "구분",
			 	width: 100
			 },  {
			 	field: "deposite_way",
			 	title: "수납방식",
			 	width: 100
			 },  {
			 	field: "deposite_amount",
			 	title: "입금금액",
			 	format: "{0:##,#}원",
			 	width: 100
			 },  
			 {
			 	field: "memo",
			 	title: "메모사항",
			 	width: 100
			 },  
			 {
			 	field: "tax_paper",
			 	title: "세금계산서",
			 	width: 100
			 }, 
			 {
			 	field: "handler",
			 	title: "수납담당자",
			 	width: 100
			 }, 
			 //  {
			 // 	field: "deposite_registration_date",
			 // 	title: "등록일시",
			 // 	width: 100
			 // }
			 ]
});

function refresh_depositeway_grid(){

	$.post("/receiptmanager/get_deposite_ways",{
			company_serial : company_serial,
		}, function(data){
			var result = JSON.parse(data);
			var str = '';
			for(i=0; i<result.length; i++){
				str += "<tr><th scope='row'>" + result[i].name + "</td><td>"+ result[i].memo + "</td><td><a class='btn btn-default pull-right' onclick='delete_deposite_way(" + result[i].serial +")'>삭제</a></td></tr>";
			}
			$("#receipt_way_table").html(str);
	});

	// alert("/receiptmanager/get_deposite_ways/" + company_serial);

	// var dataSource = new kendo.data.DataSource({
	// 	transport: {
	// 		read: {
	// 			url: "/receiptmanager/get_deposite_ways/" + company_serial,
	// 			dataType: "json"
	// 		}

	// 	},
	// 	schema: {
	// 		model: {
	// 			fields: {
	// 				   name: { type: "string" },
	// 				   memo: { type: "string" },
	// 			}
	// 		}
	// 	},
	// 	pageSize: 5
	// });
	// var grid = $("#deposite_way_grid").data("kendoGrid");
	// grid.setDataSource(dataSource);
	// grid.refresh();
	
}


function refresh_receipt_grid(order_id){
	var dataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "/receiptmanager/get_data/" + order_id,
				dataType: "json"
			}

		},
		schema: {
			model: {
				fields: {
					serial : { type: "number"},
					username : { type: "string" }, 
					deposite_date: { type: "date" },
					type: { type: "string" }, 
					deposite_way: { type: "string" },
					deposite_amount : {type : "number"}, 
					tax_paper: { type: "string" }, 
					memo: { type: "string" },
					handler: { type: "string" },
					deposite_registration_date: { type: "string" }, 
				}
			}
		},
	});
	var grid = $("#acceptance_grid").data("kendoGrid");
	grid.setDataSource(dataSource);
	//date bind 된 이후
	dataSource.fetch(function() {
		var total_price = 0;
		var size = dataSource.total();
		for(i=0; i<size; i++){
			total_price += dataSource.at(i).deposite_amount;
		}
		$("#input_inserted_price").val(total_price);
		var money = $("#input_total_price").val();
		$("#input_remain_price").val(money - total_price);
	});
}



$('#receipt_delete').click(function() {
	var grid = $("#acceptance_grid").data("kendoGrid");
	var row = grid.select();
	var data = grid.dataItem(row);
	if(data==null){
		alert("삭제할 항목를 선택해 주세요.");
		return false;
	}else{
		if(data.handler == "렌고예약"){
			alert("렌고예약은 삭제할 수 없습니다.");
			return false;
		}
		if(data.type == "렌고"){
			alert("렌고예약은 삭제할 수 없습니다.");
			return false;
		}
		alert(data.serial);

		$.post("/receiptmanager/delete_receipt",{
			serial : data.serial,
		}, function(data){
			var result = JSON.parse(data);
			if(result['code'] == "S01"){
				alert('삭제되었습니다.');
				refresh_receipt_grid(order_serial);
				return true;
			}else{
				alert(result['message']);
				return false;
			}
		});



	}
});

$('#receipt_save').click(function() {

	// alert(order_serial);
	var grid = $("#acceptance_grid").data("kendoGrid");
	var row = grid.select();
	var data = grid.dataItem(row);

	var username = $('#receipt_username').val();
	if(username == '' || username == null){
		alert('대여자명을 입력해 주세요.');
		$('#receipt_username').focus();
		return false;
	}
	// var receipt_date = $('#receipt_date').data("DateTimePicker").date();
	// receipt_date_str = receipt_date.format('YYYY-MM-DD');
	if(receipt_date == '' || receipt_date == null){
		alert('입금 날짜를 입력해 주세요.');
		$('#receipt_date').focus();
		return false;
	}

	var receipt_type = $("#select_receipt_type option:selected").val();


	var pay_type = $("#receipt_pay_type").val();
	if(pay_type == '' || pay_type == null){
		alert('수납방식을 입력해 주세요.');
		$('#receipt_pay_type').focus();
		return false;
	}
	var input_money =  $("#receipt_input_price").val();

	if(input_money == '' || input_money == null){
		alert('입금금액을 입력해 주세요.');
		$('#receipt_input_price').focus();
		return false;
	}
	// if(receipt_type=='대여기간할인' || receipt_type=='특정기간할인' || receipt_type=='기타할인' || receipt_type=='환불'){
	// 	input_money = -1 * parseInt(input_money);
	// }else{
		input_money = parseInt(input_money);
	// }
	var memo =  $("#receipt_memo").val();
	// if(memo == '' || memo == null){
	// 	alert('메모사항을 입력해 주세요.');
	// 	$('#receipt_memo').focus();
	// 	return false;
	// }
	var tax =  $("#receipt_tax").val();
	// if(tax == '' || tax == null){
	// 	alert('세금계산서를 입력해 주세요.');
	// 	$('#receipt_tax').focus();
	// 	return false;
	// }
	var staff =  $("#select_staff option:selected").text();

	if(is_receipt_register_mode){
		// order_serial
		$.post("/receiptmanager/register_receipt",{
			order_serial : order_serial,
			username : username,
			receipt_date :  moment(receipt_date).format('YYYY-MM-DD'),
			receipt_type : receipt_type,
			pay_type : pay_type,
			input_money : input_money,
			memo : memo,
			tax : tax,
			staff : staff,
			rent_type : $('#order_kind').val(),
		}, function(data){
			var result = JSON.parse(data);
			if(result['code'] == "S01"){
				alert('저장되었습니다.');
				$('#myModal3').modal('toggle');

				refresh_receipt_grid(order_serial);
					// $('#myModal3').modal('toggle');
					//입금 금액에 추가

					return true;
				}else{
					alert(result['message']);
					return false;
				}
			});
	}else{
		$.post("/receiptmanager/update_receipt",{
			serial : data.serial,
			username : username,
			receipt_date : moment(receipt_date).format('YYYY-MM-DD'),
			receipt_type : receipt_type,
			pay_type : pay_type,
			input_money : input_money,
			memo : memo,
			tax : tax,
			staff : staff,
			rent_type : $('#order_kind').val(),
		}, function(data){
			var result = JSON.parse(data);
			if(result['code'] == "S01"){
				alert('수정되었습니다.');
				$('#myModal3').modal('toggle');
				refresh_receipt_grid(order_serial);
					//입금 금액에 추가
					var temp_inserted_money = parseInt($('#input_inserted_price').val()) + input_money;
					$('#input_inserted_price').val(temp_inserted_money);
					var money = $('#input_total_price').val();
					$('#input_remain_price').val(money - temp_inserted_money);
					return true;
				}else{
					alert(result['message']);
					return false;
				}
			});
	}



});


$('#tab_user1').click(function(){
	selected_user_tab = 1;
});
$('#tab_user2').click(function(){
	selected_user_tab = 2;
});

//예약 종류에 따른 변경 이벤트
$('#order_kind').change(function(){
	if($('#order_kind option:selected').val() == '보험'){
		enableInsruanse(true);
	}else{
		enableInsruanse(false);
	}

});


////////////////////////////////////////////////////////////////////////////////////////  유저 검색 grid

$('#search_grid').kendoGrid({
	// navigatable: true,
	selectable: true,
	// allowCopy: true,
	// height: 300,
	sortable: true,
	// filterable: true,
	groupable: false,
	pageable: {
		// input: true,
		messages: {
			display: "총 {2}명의 고객중 {0}-{1} 번째",
			empty: "데이타 없음"
		}
	},
	noRecords: {
		template: "등록 된 고객이 없습니다."
	},
	// columnMenu: {
	// 	sortable: false,
	// 	messages: {
	// 		columns: "표시할 항목 선택",
	// 		filter: "필터",
	// 	}
	// },
	dataSource: {
		transport: {
			read: {
				url: "/rent/get_company_all_user/" + company_serial,
				dataType: "json"
			}
		},
		schema: {
			model: {
				fields: {
					user_serial: { type: "number" },
					user_email: {type:"string"},
					user_name: { type: "string" }, 
					birthday: { type: "srting" }, 
					address: { type: "string" }, 
					phone_number1: { type: "string" },
					phone_number2: { type: "string" },
					company: { type: "string" }, 
					auth_code: { type: "number" }, 
					note: {type:"string"},
					license_type: { type: "string" }, 
					published_date: { type: "date" }, 
					license_number: { type: "string" }, 
					expiration_date: { type: "date" }, 
				}
			}
		},
		pageSize: 30
	},
	columns: [ 
	{
		field: "user_name",
		title: "고객성명",
				// locked: true,
				width: 100
			},	
			{
				field: "birthday",
				title: "생년월일",

				width: 100
			},	
			{
				field: "user_email",
				title: "이메일",
				width: 100
			},	
			{
				field: "phone_number1",
				title: "연락처1",
				width: 140
			},	

			]
		});



//유저 검색어로 찾기
$("#input_user_search").keyup(function () {
	var val = $('#input_user_search').val();
	// alert(val);
	$("#search_grid").data("kendoGrid").dataSource.filter({
		logic: "or",
		filters: [
		{
			field: "user_name",
			operator: "contains",
			value:val
		},
		{
			field: "phone_number1",
			operator: "contains",
			value:val
		},
		{
			field: "birthday",
			operator: "contains",
			value:val
		},
		{
			field: "user_email",
			operator: "contains",
			value:val
		},					 
		]
	});
}); 

$('#user_select_btn').click(function() {
	var grid = $('#search_grid').data('kendoGrid'); 
	var row = grid.select();
	var data = grid.dataItem(row);
	if(data==null){
		alert('고객을 선택해 주세요.');
	}else{
		var user_email = data.user_email;
		var birthday = data.birthday;
		var gender = data.gender;

		if(selected_user_tab == "1"){

			$('#user_status').val(data.user_status).attr('selected','selected');
			$('#user_serial').val(data.user_serial);
			$('#user_email').val(user_email);
			$('#registration_date').val(data.registration_date);
			// $('#password').val('');
			// $('#confirm_password').val('');
			// $('#pass_text').html('');
			$('#user_name').val(data.user_name);
			$('#birthday').val(birthday);
			$('#gender').val(gender);
			$('#phone_number1').val(data.phone_number1);
			$('#phone_number2').val(data.phone_number2);
	//		$('#company').val(data.company);
	//		$('#auth_code').val(data.auth_code);
			$('#license_type').val(data.license_type).attr('selected','selected');
			$('#license_number').val(data.license_number);
			$('#expiration_date').datepicker().data('datepicker').selectDate(data.expiration_date);
			$('#published_date').datepicker().data('datepicker').selectDate(data.published_date);
			// $('#expiration_date').val(data.expiration_date);
			// $('#published_date').val(data.published_date);
			$('#postcode').val(data.postcode);
			$('#address').val(data.address);
		} else if(selected_user_tab == "2"){
			$('#user_status').val(data.user_status).attr('selected','selected');
			$('#second_user_serial').val(data.user_serial);
			$('#second_user_email').val(user_email);
			$('#second_registration_date').val(data.registration_date);
			// $('#password').val('');
			// $('#confirm_password').val('');
			// $('#pass_text').html('');
			$('#second_user_name').val(data.user_name);
			$('#second_birthday').val(birthday);
			$('#second_gender').val(gender);
			$('#second_phone_number1').val(data.phone_number1);
			$('#second_phone_number2').val(data.phone_number2);
	//		$('#second_company').val(data.company);
	//		$('#second_auth_code').val(data.auth_code);
			$('#second_license_type').val(data.license_type).attr('selected','selected');
			$('#second_license_number').val(data.license_number);
			$('#second_expiration_date').datepicker().data('datepicker').selectDate(data.expiration_date);
			$('#second_published_date').datepicker().data('datepicker').selectDate(data.published_date);
			// $('#second_expiration_date').val(data.expiration_date);
			// $('#second_published_date').val(data.published_date);
			$('#second_postcode').val(data.postcode);
			$('#second_address').val(data.address);
		}
		// alert(data.expiration_date);
	//	$('#license1_published_date').val(data.published_date);

	//	temp_date = new Date($('#rental_plan').data("DateTimePicker").date());
	//	$('#rental_period').data("DateTimePicker").date(temp_date);
	//	temp_date = new Date($('#return_plan').data("DateTimePicker").date());
	//	$('#return_period').data("DateTimePicker").date(temp_date);

		$('#customer_search_dialog').modal('toggle');
	}
});


//유저 선택하고 나서 이벤트 처리
$("#search_grid").delegate("tbody>tr", "dblclick", function(){
	// var grid = $('#search_grid').data('kendoGrid'); 
	// var row = grid.select();
	// var data = grid.dataItem(row);
	$('#user_select_btn').trigger('click');

});

////////////////////////////////////////////////////////////////////////////////////////////// 차량 검색 grid

 $("#grid_rent_car").kendoGrid({

	resizable: true, //사용자가 컬럼 크기 맘대로 바꿀수 있게
	selectable: "row", //선택할수 있도록
	// height: 200, //높이????
	sortable: true, //정렬가능하도록
	noRecords: {
		template: "등록 된 차량이 없거나 해당 기간에 이용가능한 차량이 없습니다."
	},
	columns: [ 
				{
					field: "car_status",
					title: "운행여부",
					template: "<span class='#:data.car_status#' >#:data.car_status#</span>" ,
					width: 80,
					},
				{

					field: "car_name_detail",
					title: "모델명",
					width: 140
				}, 
				{
					field: "car_number",
					title: "차량번호",
					width: 140
				},
				{
					field: "fuel_option",
					title: "연료",
					width: 100
				},	
			]
});

//차량 검색 grid refresh
function refresh_grid(type) {
	company_serial = get_company_serial();
	period_start = moment(period_start_date).format('YYYYMMDDHHmm'); 
	period_finish = moment(period_finish_date).format('YYYYMMDDHHmm');  
	period = period_start + '_' + period_finish;

	var dataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "carmanager/get_list/" + company_serial+"/" + type + "/" + period,
				dataType: "json"
			}
		},
		
		pageSize: 10
	});

	var grid = $("#grid_rent_car").data("kendoGrid");
	grid.setDataSource(dataSource);

}


//차량 선택 하기
$("#rent_car_select_btn").click(function() {
	if(car_select()){
		$('#rent_car_search_dialog').modal('toggle'); 
	}
});

$("#grid_rent_car").delegate("tbody>tr", "dblclick",
	function(){
		if(car_select()){
			$('#rent_car_search_dialog').modal('toggle'); 
		}
});

// 차량 선택후에 처리
function car_select(){
	company_serial = get_company_serial();
	if(car_serial >= '1' && car_flag == 'Y'){
		$.post("/rent/change_car_flag",{
			company_serial:company_serial,
			car_serial:car_serial,
			car_flag:car_flag,
		}, function(data){
			var result = JSON.parse(data);
			if(result['code'] == "E01"){
				alert(result['message']);
				return false;
			} else if(result['code'] == "S01"){
				$('#count_rengo_car_number').text(result['message'] + '대');
				return true;
			}
		});
	}


	grid = $('#grid_rent_car').data('kendoGrid'); 
	row = grid.select();
	data = grid.dataItem(row);
	if(data == null){
		alert('차량을 선택해 주십시요.');
		return false;
	}

	car_serial = data.serial;
	car_type = data.car_type;
	car_number = data.car_number;
	car_name_detail = data.car_name_detail;
	car_people = data.car_people;
	gear_type = data.gear_type?data.gear_type:'자동';
	fuel_option = data.fuel_option;
	smoking = data.smoking=="Y"?"흡연":"금연";
	car_option = '';
	

	if(car_serial < '1'){
		alert('차량을 선택해 주십시요.');
		return false;
	}

	car_option += car_people?"<span class=\"label label-default\">" + car_people + "인승</span>\n":'';
	car_option += fuel_option?"<span class=\"label label-default\">" + fuel_option + "</span>\n":'';
	car_option += gear_type?"<span class=\"label label-default\">" + gear_type + "</span>\n":'';
	car_option += smoking?"<span class=\"label label-default\">" + smoking + "</span>\n":'';

	temp_car_option = data.option.split(', ');
	for(i=0;i<temp_car_option.length;i++){
		car_option += "<span class=\"label label-default\">" + temp_car_option[i] + "</span>\n";
	}

	$('#car_type').val(car_type);
	$('#car_name_detail').val(car_name_detail);
	$('#car_number').val(car_number);
	$('#car_option_div').html(car_option);

	//차의 자차 유무에 따라 요금정보 보험에 보여줘야함														
	var tempHtml = '<option>적용안함</option>';
	
	if(data.insurance_self_flag == 'Y'){
		tempHtml += "<option>자차1</option>";
		car_insurance_self1_price = data.insurance_self_price;
	}
	if(data.insurance_self2_flag == 'Y'){
		tempHtml += "<option>자차2</option>";
		car_insurance_self2_price = data.insurance_self2_price;
	}
	if(data.insurance_self3_flag == 'Y'){
		tempHtml += "<option>자차3</option>";
		car_insurance_self3_price = data.insurance_self3_price;
	}
	$("#sel_insurance_name").html(tempHtml);

	//대여키로수에 추가
	// $("#start_km").val(data.drive_distance);
	// insurance_price = 0;

	// temp_date = new Date($('#rental_plan').data("DateTimePicker").date());
	// $('#rental_period').data("DateTimePicker").date(temp_date);
	// temp_date = new Date($('#return_plan').data("DateTimePicker").date());
	// $('#return_period').data("DateTimePicker").date(temp_date);

	// 영업소 본사,지점도 선택시켜야 함

	$.post("/rent/change_car_flag",{
		company_serial:company_serial,
		car_serial:car_serial,
	}, function(data){
		var result = JSON.parse(data);
		if(result['code'] == "E01"){
			alert(result['message']);
			return false;
		} else if(result['code'] == "S01"){
			car_flag = result['message'];
			init_rental_price();
			return true;
		}
	});


	return true;
}


//차 검색시 tab 클릭 이벤트
$("#rent_car_tab1").click(function() {
	grid_car_type = '전체';
	refresh_grid(grid_car_type); 
});

$("#rent_car_tab2").click(function() {
	grid_car_type = '대형';
	refresh_grid(grid_car_type); 
});
$("#rent_car_tab3").click(function() {
	grid_car_type = '중형';
	refresh_grid(grid_car_type); 
});

$("#rent_car_tab4").click(function() {
	grid_car_type = '준중형';
	refresh_grid(grid_car_type); 
});

$("#rent_car_tab5").click(function() {
	grid_car_type = '소형';
	refresh_grid(grid_car_type); 
});

$("#rent_car_tab6").click(function() {
	grid_car_type = '경형';
	refresh_grid(grid_car_type); 
});

$("#rent_car_tab7").click(function() {
	grid_car_type = 'SUV';
	refresh_grid(grid_car_type); 
});

$("#rent_car_tab8").click(function() {
	grid_car_type = '승합';
	refresh_grid(grid_car_type); 
});
$("#rent_car_tab9").click(function() {
	grid_car_type = '수입';
	refresh_grid(grid_car_type); 
});



var company_serial = get_company_serial();

// $('#deposite_way_grid').kendoGrid({

// 		selectable: true, 
// 		sortable: true, 
// 		groupable: false, 
// 		resizable: true,
//         pageable: {
//             messages: {
//                 display: "총 {2} 명의 고객 중 {0}~{1}번째",
//                 empty: "데이터 없음"
//             }
//           },
//         noRecords: {
//             template: "등록된 수납 방식이 없습니다."
//       	},
//         dataSource: {
//            	transport: {
// 		 		read: {
// 		 			url: '/receiptmanager/get_deposite_ways/0',
// 		 			dataType: "json"
// 		 		}
// 			},
//             schema: {
//                 model: {
//                     fields: {
//                         name: { type: "string" },
//                         memo: { type: "string" },
//                     }
//                 }
//             },
//             pageSize: 5
//         },
//         columns: [{
//         	fields: "name",
//         	title: "결제방식",
//         	width:80
//         }, {
//         	fields: "memo",
//         	title: "특이사항",
//         	width: 60
//         },
//         {
//         	title: "삭제",
//         	command : { 
//     			text : "삭제",
//     			// click: deleteDepositeWay 
//     		},
//         	width: 100
//         }]
// });

</script>