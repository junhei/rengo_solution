<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>


<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">


<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>
<script src="/telerik/js/cultures/kendo.culture.ko-KR.min.js"></script>

<!-- 새로운 datepicker -->
<link href="/css/datepicker.min.css?<?=time();?>" rel="stylesheet" type="text/css">
<script src="/js/datepicker.js?<?=time();?>"></script>
<script src="/js/i18n_datepicker.en.js"></script>

<?php
require("/home/apache/CodeIgniter-3.0.6/application/views/rengo_util.html");
?>

<style type="text/css">
  @media screen and (min-width: 1860px) {
    .schedule_data{width: 82%;}
    .rent_left_wrap{width: 18%;}
  }
  @media screen and (max-width: 1860px) {
    .schedule_data{width: 82%;}
    .rent_left_wrap{width: 18%;}
  }
  @media screen and (max-width: 1600px) {
    .schedule_data{width: 80%;}
    .rent_left_wrap{width: 20%;}
  }
  @media screen and (max-width: 1430px) {
    .schedule_data{width: 80%;}
    .rent_left_wrap{width: 20%;}
  }
  @media screen and (max-width: 1400px) {
    .schedule_data{width: 724px;}
    .rent_left_wrap{width: 315px;}
    .schedule{width: 1050px; display: block;}
    body{min-width:1393px !important;}
    #rent_management{width: 1112px;}
  }
  html{overflow:hidden;}
</style>



<script type="text/javascript">
  $(document).ready(function(){
    $('#rent_right_contents').css('height', $(window).height() - 430 );
    $('#rent_left_contents').css('height', $(window).height() - 430 );
    $('.modal_scroll').css('height', $(window).height() - 200 );  
    $(window).resize(function() {
      $('#rent_right_contents').css('height', $(window).height() - 430 );
      $('#rent_left_contents').css('height', $(window).height() - 430 );
    });


    $('.modal-lg').on('hidden.bs.modal',function() {
      $('.modal_scroll').scrollTop(0);
      setTimeout(function(){
        $('.modal').css({
          'top':'0',
          'left':'0'
        });

      },30)
    })
  }); 
</script>



<div class="row car_allocation_wrap" style="overflow-y:;">
    <div class="col-md-12">
        <div class="panel border_gray magb0">
  
            <div class="panel-heading pdt15">
                <div class="row">
                    <div class="col-md-9">
                        <h3 class="mgb10">스케줄관리</h3>
                    </div>
                    <div class="col-sm-3">
                        <div class="col-md-12 pdl0 pdr0">
                            <select class="form-control" id="company_select">
                                <?php foreach($company_list as $company):?>
                                <option value="<?php echo $company['serial']; ?>"> <?php echo $company['company_name']; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-footer pdb0 pdt15">
                <div class="row">
                    <div class="list_data_box">
<!--                         <div class="col-lg-1 col-md-2 col-xs-3">총 운행 스케줄</div>
                        <div class="col-lg-1 col-md-2 col-xs-3 text-right" id="count_total">건</div> -->
                        <div class="col-lg-1 col-md-2 col-xs-3">대여</div>
                        <div class="col-lg-1 col-md-2 col-xs-3 text-right" id="count_delivery">건</div>
                        <div class="col-lg-1 col-md-2 col-xs-3">반납</div>
                        <div class="col-lg-1 col-md-2 col-xs-3 text-right" id="count_pickup">건</div>
                    </div>
                </div>
            </div>

            <div class="panel-heading border_gray bg_fff pdt14 pdb13">
                <div class="row">
                    
                                        <div class="col-md-6">
                                            <div class="pull-left">
                                                <ul class="time_header pdl0" style="list-style: none;">
                                                    <li id="btn_datetimepicker1_prev" >
                                                        <a class="btn btn-default" href="javascript:void(0);" role="button"><i class="fa fa-angle-left fa-lg"></i></a>
                                                    </li>
                                                    <li >
                                                        <input type='text' class="form-control btn btn-default" role="button" id='datetimepicker1' data-date-format="yyyy년 mm월 dd일"  />
                                                    </li>
                                                    <li id="btn_datetimepicker1_next" >
                                                        <a class="btn btn-default" href="javascript:void(0);" role="button"><i class="fa fa-angle-right fa-lg"></i></a>
                                                    </li>
<script language="javascript">



var selected_date='';
var schedule_mode = 'date';
var order_kind = '';

var startDateStr = '';
var endDateStr = '';
var company_serial = get_company_serial();

$('#datetimepicker1').datepicker({
    navTitles: {
        days: 'yyyy년 mm월'
    },
    //  todayButton: new Date(),
    clearButton: false,
    closeButton: true,
    language: 'en',
    onHide: function(dp, animationCompleted){
        // next_day = get_current_date('YYYYMMDD');
        // refresh_schedule(next_day);
        getCount();
        refresh_schedule();
        $( "#car_allocation_return" ).click();

    },

    onSelect: function (fd, d, picker) {
    // Do nothing if selection was cleared
    if (!d) return;
        // $("datetimepicker1").val( d.getFullYear()+"년 "+zeroPad(d.getMonth()+1,2)+"월 "+zeroPad(d.getDate(),2)+"일");
        selected_date = d;
        startDateStr = selected_date.getFullYear()+zeroPad(selected_date.getMonth()+1,2)+zeroPad(selected_date.getDate(),2)+"0000";
        endDateStr = selected_date.getFullYear()+zeroPad(selected_date.getMonth()+1,2)+zeroPad(selected_date.getDate(),2)+"2359";
    }
});

</script>
                                                </ul>
                                            </div>
                                            <div>
                                                <button id="button_today" class="btn action_btn">오늘</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="panel-footer section_lnb bt0">
                                    <ul class="nav nav-tabs border_none">
                                        <li class="active"><a data-toggle="tab" id="car_allocation_return" onclick="grid_schedule_filter('')">전체</a></li>
                                        <li><a data-toggle="tab" id="car_allocation" onclick="grid_schedule_filter('대여')">대여</a></li>
                                        <li><a data-toggle="tab" id="car_return" onclick="grid_schedule_filter('반납')">반납</a></li>
                                    </ul>
                                </div>

    <div class="tab-content">
        <div id="example" style="margin:0;">
            <div id="grid_schedule">
                
            </div>
        </div>
    </div>
      

      <div class="panel-footer bg_gray">
        
      </div>

    </div>
  </div>
</div>

 <script id="event-template" type="text/x-kendo-template">
      <div><h6>#=title#</h6>
      </div>

</script>


<script>


  var start = new Date(),
  prevDay,
  startHours = 9,
  finishHours = 20;



$('#datetimepicker1').datepicker().data('datepicker').selectDate(new Date());

$(window).resize(function() {
    fitGridSize();
});

fitGridSize();
getCount();

function get_current_date(format){
                // temp_current_date = $('#datetimepicker1').data("DateTimePicker").date();
                if(selected_date == ''){
                    selected_date = new Date();
                }
                temp_current_date = selected_date;
                if(selected_date != null){
                    if(format != '' && format != undefined){
                        // current_date = temp_current_date.format(format);
                        current_date = moment(temp_current_date).format('YYYYMMDD');
                    } else {
                        current_date = temp_current_date;
                    }
                    return current_date;
                } else {
                    alert('날짜를 선택해 주십시요.');
                    return false;
                }
}

function refresh_schedule(){
  var company_serial = get_company_serial();
    var source = new kendo.data.DataSource({
            transport: {
                    read: {
                        url: "/car_allocation/get_car_schedule_detail/" + startDateStr +"/" + endDateStr + "/" + company_serial,
                        dataType: "json"
                    }
                },
          });
        
        var grid_schedule = $("#grid_schedule").data('kendoGrid');
        grid_schedule.setDataSource(source);    
}


$("#btn_datetimepicker1_next").click(function() {
            next_day = get_current_date();
            next_day.setDate(next_day.getDate()+1);

            $('#datetimepicker1').datepicker().data('datepicker').selectDate(next_day);

            startDateStr = next_day.getFullYear()+zeroPad(next_day.getMonth()+1,2)+zeroPad(next_day.getDate(),2)+"0000";
            endDateStr = next_day.getFullYear()+zeroPad(next_day.getMonth()+1,2)+zeroPad(next_day.getDate(),2)+"2359";


            next_day = get_current_date('YYYYMMDD');
            refresh_schedule();
            getCount();

            $( "#car_allocation_return" ).click();
            // refresh_schedule(next_day);
});
            
$("#btn_datetimepicker1_prev").click(function() {
            next_day = get_current_date();
            next_day.setDate(next_day.getDate()-1);
                //성환
            $('#datetimepicker1').datepicker().data('datepicker').selectDate(next_day);
                // $('#datetimepicker1').data("DateTimePicker").date(next_day);
//              $('#datetimepicker1').datetimepicker('update');
            startDateStr = next_day.getFullYear()+zeroPad(next_day.getMonth()+1,2)+zeroPad(next_day.getDate(),2)+"0000";
            endDateStr = next_day.getFullYear()+zeroPad(next_day.getMonth()+1,2)+zeroPad(next_day.getDate(),2)+"2359";

            next_day = get_current_date('YYYYMMDD');
            refresh_schedule();
            getCount();
            $( "#car_allocation_return" ).click();
            // refresh_schedule(next_day);

});

$("#button_today").click(function() {
    selected_date = new Date();
    $('#datetimepicker1').datepicker().data('datepicker').selectDate(selected_date);

    startDateStr = selected_date.getFullYear()+zeroPad(selected_date.getMonth()+1,2)+zeroPad(selected_date.getDate(),2)+"0000";
    endDateStr = selected_date.getFullYear()+zeroPad(selected_date.getMonth()+1,2)+zeroPad(selected_date.getDate(),2)+"2359";
    refresh_schedule();
    getCount();
    $( "#car_allocation_return" ).click();

});

// $("#datetimepicker1").on("dp.show", function(e) {
//                 current_date = $('#datetimepicker1').val();
// });
// $("#datetimepicker1").on("dp.hide", function(e) {
//                 if(current_date != $('#datetimepicker1').val()){

//                     next_day = get_current_date('YYYYMMDD');
//                     refresh_schedule(next_day);
//                 }
// });


$('.modal').on('hidden.bs.modal',function() {
  $('.modal_scroll').scrollTop(0);
  setTimeout(function(){
    $('.modal').css({
      'top':'0',
      'left':'0'
    });

    },30)
})

function replaceString(value) {
        return value.replace(",", "<br />");
    }

// kendo.pdf.defineFont({
//     "BMYEONSUNG"             : "/fonts/BMYEONSUNG_ttf.ttf", // this is a URL
// });






show_company_select();




//////////////////////////////////////////////////////////////// 파트너 선택 리스너

$( "#company_select" ).change(function() {
  company_serial = $(this).val();

  refresh_schedule();
  getCount();

  // var selectedDate = new Date();
  // selectedDate.setDate(selectedDate.getDate() + 6);
  // var selectedEndDateStr = selectedDate.getFullYear()+zeroPad(selectedDate.getMonth()+1,2)+zeroPad(selectedDate.getDate(),2)+"2359";
  // //전달받은 -6일부터 가져온다.
  // var selectedStartDate = selectedDate;
  // selectedStartDate.setDate(selectedDate.getDate() - 14);
  // var selectedDateStr = selectedStartDate.getFullYear()+zeroPad(selectedStartDate.getMonth()+1,2)+zeroPad(selectedStartDate.getDate(),2)+"0000";

  //         var source = new kendo.data.SchedulerDataSource({
  //           transport: {
  //             read: {
  //               url: "/car_allocation/get_car_schedule/" + selectedDateStr +"/" + selectedEndDateStr + "/" + company_serial,
  //               dataType: "json"
  //             }
  //           }
  //         });

  //       var scheduler = $("#scheduler").data("kendoScheduler");
  //       scheduler.setDataSource(source);
 });



//grid 초기화


$("#grid_schedule").kendoGrid({
          selectable: true, 
          sortable: true,
          groupable: false, 
          // pageable: {
          //     input: true,
          //     messages: {
          //         display: "총 {2} 개의 스케쥴 중 {0}-{1}번째",
          //         empty: "데이터 없음"
          //     }
          //   },
          noRecords: {
              template: "대여/반납 스케쥴이 없습니다."
            },
          dataSource: {
                transport: {
                    read: {
                        url: "/car_allocation/get_car_schedule_detail/" + startDateStr +"/" + endDateStr + "/" + company_serial,
                        dataType: "json"
                    }
                },
                schema: {
                    model: {
                        fields: {
                          serial: { type: "number" },
                          type: { type: "string" }, 
                          date: { type: "date" }, 
                          car_number: { type: "string" }, 
                          place: { type: "string" },  
                          name: { type: "string" },  
                          phone: { type: "string" },  
                          handler: { type: "string" },  
                      }
                  }
              },
              pageSize: 30
          },
         
          columns: [
             {
                  field: "type",
                  title: "구분",
                  width: 80
              },
              {
                  field: "date",
                  title: "대여/반납 일시",
                  width: 200,
                  format: "{0:yyyy년 M월 dd일 HH시mm분}",
              },
              {
                  field: "place",
                  title: "대여/반납 지역"
              },
              {
                  field: "car_name_detail",
                  title: "세부모델"
              },
               {
                  field: "car_number",
                  title: "차량번호"
              },
              {
                  field: "name",
                  title: "고객명"
              },
               {
                  field: "phone",
                  title: "연락처"
              },
              {
                  field: "handler",
                  title: "담당자"
              },
          ]
      });


$("#grid_schedule").delegate("tbody>tr", "dblclick",
  function(){
        //그리드에서 선택된게 있으면
        var grid = $("#grid_schedule").data("kendoGrid");
        var row = grid.select();
        var data = grid.dataItem(row);

        $('#myModal2').modal('toggle');

        $("#order_kind").attr("disabled",true);
        $("#status").attr("disabled",true);
        $("#rental_plan").attr("disabled",true);
        $("#return_plan").attr("disabled",true);
        $("#delivery_place").attr("disabled",true);
        $("#pickup_place").attr("disabled",true);
        $("#car_type").attr("disabled",true);
        $("#car_search_btn").attr("disabled",true);

        $("#user_name").attr("disabled",true);
        $("#birthday").attr("disabled",true);
        $("#gender").attr("disabled",true);
        $("#user_email").attr("disabled",true);
        $("#phone_number1").attr("disabled",true);
        $("#phone_number2").attr("disabled",true);
        $("#license_type").attr("disabled",true);
        $("#license_number").attr("disabled",true);
        $("#published_date").attr("disabled",true);
        $("#expiration_date").attr("disabled",true);
        $("#postcode").attr("disabled",true);
        $("#address").attr("disabled",true);

        $("#second_user_name").attr("disabled",true);
        $("#second_birthday").attr("disabled",true);
        $("#second_gender").attr("disabled",true);
        $("#second_user_email").attr("disabled",true);
        $("#second_phone_number1").attr("disabled",true);
        $("#second_phone_number2").attr("disabled",true);
        $("#second_license_type").attr("disabled",true);
        $("#second_license_number").attr("disabled",true);
        $("#second_published_date").attr("disabled",true);
        $("#second_expiration_date").attr("disabled",true);
        $("#second_postcode").attr("disabled",true);
        $("#second_address").attr("disabled",true);

        $("#input_damaged_cartype").prop("disabled", true);
        $("#input_damaged_carnumber").prop("disabled", true);
        $("#input_insurance_company").prop("disabled", true);
        $("#input_insurance_number").prop("disabled", true);
        $("#input_insurance_manager").prop("disabled", true);
        $("#input_accident_car").prop("disabled", true);
        $("#input_insurance_phone").prop("disabled", true);
        $("#input_insurance_fax").prop("disabled", true);
        $("#input_fix_company").prop("disabled", true);
        $("#input_fix_phone").prop("disabled", true);
        $("#input_falut_percnet").prop("disabled", true);
        $("#input_insurance_date").prop("disabled", true);

        $("#contract_period").prop("disabled", true);
        $("#rental_price").prop("disabled", true);
        $("#price_off").prop("disabled", true);
        $("#total_price").prop("disabled", true);
        $("#sel_delivery").prop("disabled", true);
        $("#input_delivery").prop("disabled", true);
        $("#sel_insurance_name").prop("disabled", true);
        $("#insurance_price").prop("disabled", true);
        $("#sel_overcharge").prop("disabled", true);
        $("#input_overcharge").prop("disabled", true);
        $("#sel_option").prop("disabled", true);
        $("#input_option").prop("disabled", true);
        $("#sel_re_or_acc").prop("disabled", true);
        $("#input_re_or_acc").prop("disabled", true);
        $("#sel_etc").prop("disabled", true);
        $("#input_etc").prop("disabled", true);

        $("#rental_period").prop("disabled", true);
        $("#return_period").prop("disabled", true);
        $("#delivery_place_order").prop("disabled", true);
        $("#pickup_place_order").prop("disabled", true);
        $("#start_fuel").prop("disabled", true);
        $("#end_fuel").prop("disabled", true);
        $("#start_km").prop("disabled", true);
        $("#end_km").prop("disabled", true);
        $("#drive_km").prop("disabled", true);
        $("#memo_text").prop("disabled", true);


        


        // $("#btn_save").hide();
        $("#btn_reservation_delete").hide();
        $("#receipt_register").hide();
        order_detail(data.serial);

        




});

function fitGridSize() {
    var gridElement = $('#grid_schedule');
    var gird_position = $("#grid_schedule").position();
    var window_height = $( window ).height() - gird_position.top - 50;
    gridElement.children(".k-grid-content").height(window_height-150);
    gridElement.height(window_height);
}



function getCount(){
    var company_serial = get_company_serial();
    var selectedDay = get_current_date();

    var startDateStr = selectedDay.getFullYear()+zeroPad(selectedDay.getMonth()+1,2)+zeroPad(selectedDay.getDate(),2)+"0000";
    var endDateStr = selectedDay.getFullYear()+zeroPad(selectedDay.getMonth()+1,2)+zeroPad(selectedDay.getDate(),2)+"2359";
        // alert(startDateStr);
    $.post("/car_allocation/get_count",{
           company_serial:company_serial,
           start_time : startDateStr,
           end_time : endDateStr
         }, function(data){

           var result = JSON.parse(data);
           var delivery = result['delivery'];
           var pickup = result['pickup'];
           var total = parseInt(delivery) + parseInt(pickup);
           $("#count_total").html(total+"건");
           $("#count_delivery").html(delivery+"건");
           $("#count_pickup").html(pickup+"건");
         });
}

function grid_schedule_filter(val1){
    $("#grid_schedule").data("kendoGrid").dataSource.filter({
        logic: "or",
        filters: [
            {
                field: "type",
                operator: "contains",
                value: val1
            }
        ]
    });
}

</script>

<?php

require("/home/apache/CodeIgniter-3.0.6/application/views/rent/dialog_rent_info.php");
require("/home/apache/CodeIgniter-3.0.6/application/views/rent/dialog_grid.php");
?>

<script type="text/javascript">
  //차량 검색 버튼 클릭시 날짜가 먼저 선택되어 있어야 함(왜냐하면 해당 기간에 빌릴수 있는 차를 구해야 해서)
  //차량 스케쥴에서 diable 시키기 위해서 해당 함수를 반드시 dialog_rent_info require 이후에 할것
$("#car_search_btn").click(function() {
});

//유저 찾기 버튼 이벤트
$('#user_search_btn').click(function(){
 
});
</script>
