
<!-- 큰 모달 -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h5 class="modal-title" id="myModalLabel">예약내역</h5>
			</div>

			<div class="modal_scroll">





				<div class="modal-body" id="register_header_information" style="display:none;">
					<div class="row">
						<div class="col-md-9">
							<form class="form-inline">
								<div class="input-group pdr10">
									<div class="input-group-addon">예약일시</div>
									<input type="text" id="registered_date" class="form-control" readonly>
								</div>
								<div class="input-group">
									<div class="input-group-addon">계약번호</div>
									<input type="text" id="order_number" class="form-control" readonly>
								</div>
							</form>
						</div>
					</div>
				</div>


				<div class="bg_f8f8f8 bb_ccc">
					<div class="row">
						<div class="col-sm-6 pdr0 br_ccc">


							<div class="modal-body bt_ccc">
								<div class="row rent_input">
									<!--왼쪽 -->
									<div class="col-md-12">
										<div class="row">
											<h5>예약정보</h5>
										</div>
										<div class="row pdt15">
											<div class="col-md-6 form-horizontal">

												<div class="form-group">
													<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*구분</label>
													<div class="col-sm-10">
														<select class="form-control" id="order_kind">
															<option value="일반">일반</option>
															<option value="보험">보험</option>
														</select>
													</div>
												</div>

											</div>
											<div class="col-md-6 form-horizontal">

												<div class="form-group">
													<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*계약상태</label>
													<div class="col-sm-10">
														<input type="text" class="form-control" id="status" readonly value="예약">
														</div>
													</div>

												</div>
											</div>
											<div class="row">
												<div class="col-md-6 form-horizontal">
													<div class="form-group">
														<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*대여예정</label>
														<div class="col-sm-10">
															<div class='input-group date' >
																<input type="text" class="form-control" id="rental_plan" readonly style="background:#FFF;">
				<!-- 												<span class="input-group-addon">
																	<span class="glyphicon glyphicon-calendar"></span>
																</span> -->
															</div>
															<script language="javascript">
																$('#rental_plan').datepicker({
																	navTitles: {
																		days: 'yyyy년 mm월'
																	},
//	todayButton: new Date(),
clearButton: true,
closeButton: true,
timepicker: true,
language: 'en',
startDate: start,
minHours: 9,
maxHours: 20,
minutesStep: 10,
onSelect: function (fd, d, picker) {
		// Do nothing if selection was cleared
		if (!d) return;

		var day = d.getDay();
		// Trigger only if date is changed
		// if (prevDay != undefined && prevDay == day) return;
		// prevDay = day;
		// If chosen day is Saturday or Sunday when set
		// hour value for weekends, else restore defaults
		if (day == 6 || day == 0) {
			picker.update({
				minHours: 9,
				maxHours: 20
			})
		} else {
			picker.update({
				minHours: 9,
				maxHours: 20
			})
		}
		period_start_date = d;


	},
	onHide: function(dp, animationCompleted){
		$("#sel_period_price_serial").val('').prop("selected", true);
		$("#sel_special_price_serial").val('').prop("selected", true);
		init_rental_price();
	}
})
</script>
</div>
</div>
</div>
<div class="col-md-6 form-horizontal">

	<div class="form-group">
		<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*반납예정</label>
		<div class="col-sm-10">
			<div class='input-group date'>
				<input type='text' class="form-control" id="return_plan" readonly style="background:#FFF;" />
<!-- 				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span> -->
			</div>
			<script language="javascript">
				$('#return_plan').datepicker({
					navTitles: {
						days: 'yyyy년 mm월'
					},
//	todayButton: new Date(),
clearButton: true,
closeButton: true,
timepicker: true,
language: 'en',
startDate: start,
minHours: 10,
maxHours: 21,
minutesStep: 10,
onSelect: function (fd, d, picker) {
		// Do nothing if selection was cleared
		if (!d) return;

		var day = d.getDay();
		// Trigger only if date is changed
		// if (prevDay != undefined && prevDay == day) return;
		// prevDay = day;
		// If chosen day is Saturday or Sunday when set
		// hour value for weekends, else restore defaults
		if (day == 6 || day == 0) {
			picker.update({
				minHours: 10,
				maxHours: 21
			})
		} else {
			picker.update({
				minHours: 10,
				maxHours: 21
			})
		}
		period_finish_date = d;
	},
	onHide: function(dp, animationCompleted){
		$("#sel_period_price_serial").val('').prop("selected", true);
		$("#sel_special_price_serial").val('').prop("selected", true);
		init_rental_price();
	}
})
</script>
</div>
</div>

</div>
</div>
<div class="row">
	<div class="col-md-6 form-horizontal">

		<div class="form-group">
			<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">대여지역</label>
			<div class="col-sm-10">
				<input type="" class="form-control" id="delivery_place">
			</div>
		</div>

	</div>
	<div class="col-md-6 form-horizontal">

		<div class="form-group">
			<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">반납지역</label>
			<div class="col-sm-10">
				<input type="" class="form-control" id="pickup_place">
			</div>
		</div>

	</div>
</div>
<div class="row">
	<div class="col-md-6 form-horizontal">

		<div class="form-group magb0">
			<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*차량검색</label>

			<div class="col-sm-10">
				<div class="input-group" id="car_search_btn">
					<input type="text" class="form-control text-point" id="car_type" data-target="#rent_car_search_dialog" readonly style="background:#FFF;" placeholder="차량을 검색해 주세요.">
					<div class="input-group-addon text-point" data-target="#rent_car_search_dialog"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
				</div>
			</div>
		</div>

	</div>
	<div class="col-md-6 form-horizontal">

		<div class="form-group">
			<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">차량번호</label>
			<div class="col-sm-10">
				<input type="" class="form-control" id="car_number" disabled>
			</div>
		</div>

	</div>
</div>
<div class="row">
	<div class="col-md-6 form-horizontal">

		<div class="form-group">
			<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">세부모델</label>
			<div class="col-sm-10">
				<input type="" class="form-control" id="car_name_detail" disabled>
			</div>
		</div>

	</div>
	<!-- <div class="col-md-6 form-horizontal">

		<div class="form-group">
			<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*영업소</label>
			<div class="col-sm-10">
				<select class="form-control" id="branch_serial">
					<option value="0">본사</option>`
				</select>
			</div>
		</div>

	</div> -->
</div>
<div class="row">
	<div class="form-horizontal" style="">
		<div class="form-group">
			<label for="" class="control-label text-right pdl0 pdr0 col-md-1" style="width:13.5%;">차량정보</label>
			<div class="col-md-11" style="width:86.5%; max-width: 516px;">
				<div class="panel panel-default">
					<div class="panel-body vehicle_info" id="car_option_div">
						<span>&nbsp;</span>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>
</div>
</div>
</div>

<div class="modal-body bt_ccc">
	<div class="row rent_input">
		<div class="col-md-12">
			<div class="row">
				<h5>고객정보</h5>
			</div>
			<div class="row pdt15">
				<div class="">
					<ul class="nav nav-tabs">
						<li class="active col-md-6 pdr0 pdl0" id="tab_user1"><a data-toggle="tab" href="#home">제1운전자(예약자)정보</a></li>
						<li class="col-md-6 pdr0 pdl0" id="tab_user2"><a data-toggle="tab" href="#menu1">제2운전자정보</a></li>
					</ul>
					<div class="tab-content">
						<div id="home" class="tab-pane fade in active">
							<div class="row pdt15">
								<div class="col-sm-6 form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*고객명</label>
										<div class="col-sm-10">
											<div class="input-group">
												<input type="text" class="form-control" id="user_name">
												<div class="input-group-addon text-point" data-target="#customer_search_dialog" id="user_search_btn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
											</div>
										</div>
									</div>
									<!-- <div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">회원번호</label>
										<div class="col-sm-10">
											<input type="" class="form-control" id="user_serial" value="" readonly>
										</div>
									</div> -->
								</div>
								<div class="col-sm-6 form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*생년월일</label>
										<div class="col-sm-10">
											<div class="col-sm-6 pdr0 pdl0">
												<input type="text" class="form-control" id="birthday" maxlength="6">
											</div>
											<div class="col-sm-6 pdl0 pdr0">
												<div class="col-sm-1 text-center">
													<label for="" class="col-sm-2 pdl0 pdr0 control-label"><p>-</p></label>
												</div>
												<div class="col-sm-3 pdl0 pdr0">
													<input type="number" size='1' class="form-control" id="gender" style="padding:3px 6px;" maxlength="1">
												</div>
												<div class="col-sm-3 pdl10 pdr0">
													<label for="" class="col-sm-2 pdl0 pdr0 control-label text-leftr">******</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 form-horizontal">

									<div class="form-group">
										<!-- <label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*운전자명</label>
										<div class="col-sm-10">
											<div class="input-group">
												<input type="text" class="form-control" id="user_name" placeholder="홍길동">
												<div class="input-group-addon text-point" data-toggle="modal" data-target="#customer_search_dialog" id="user_search_btn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
											</div>
										</div> -->
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">이메일</label>
										<div class="col-sm-10">
											<div class="input-group">
												<input type="text" class="form-control" id="user_email">
												<!-- <div class="input-group-addon text-point" data-toggle="modal" data-target="#rent_email_dialog">보내기</div> -->
											</div>
										</div>
									</div>

								</div>
								<div class="col-md-6 form-horizontal">

									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left" >*연락처</label>
										<div class="col-sm-10">
											<div class="input-group">
												<input type="text" class="form-control" id="phone_number1" maxlength="12">
												<!-- <div class="input-group-addon text-point" data-toggle="modal" >SMS</div> -->
											</div>
										</div>
									</div>

								</div>
							</div>

							<div class="row">
								<div class="col-sm-6 form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">긴급연락처</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="phone_number2" maxlength="12">
										</div>
									</div>
								</div>
								<div class="col-md-6 form-horizontal">

									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">면허종류</label>
										<div class="col-sm-10">
											<select class="form-control" id="license_type">
												<option value="1종 대형">1종 대형</option>
												<option value="1종 보통">1종 보통</option>
												<option value="2종 보통">2종 보통</option>
												<option value="2종 소형">2종 소형</option>
											</select>
										</div>
									</div>

								</div>
							</div>

							<div class="row">
								<div class="col-md-6 form-horizontal">

									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">면허번호</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="license_number">
										</div>
									</div>

								</div>
								<div class="col-md-6 form-horizontal">

									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">발행일</label>
		<div class="col-sm-10">
			<div class='input-group date'>
				<input type='text' class="form-control" id="published_date" />
			<!-- 	<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span> -->
			</div>
			<script language="javascript">
				$('#published_date').datepicker({
					navTitles: {
						days: 'yyyy년 mm월'
					},
//	todayButton: new Date(),
clearButton: true,
closeButton: true,
timepicker: false,
language: 'en',
startDate: start,

onSelect: function (fd, d, picker) {
		// Do nothing if selection was cleared
		if (!d) return;
	}
})
</script>
</div>
									</div>

								</div>
							</div>

							<div class="row">
								<div class="col-md-6 form-horizontal">

									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">유효기간</label>
										<div class="col-sm-10">
											<div class='input-group date'>
												<input type='text' class="form-control" id="expiration_date" />
											<!-- 	<span class="input-group-addon">
													<span class="glyphicon glyphicon-calendar"></span>
												</span> -->
											</div>
											<script language="javascript">
$('#expiration_date').datepicker({
navTitles: {
	days: 'yyyy년 mm월'
},
//	todayButton: new Date(),
clearButton: true,
closeButton: true,
timepicker: false,
language: 'en',
startDate: start,
onSelect: function (fd, d, picker) {
		// Do nothing if selection was cleared
		if (!d) return;
		
	}
})
											</script>
										</div>
									</div>

								</div>

<div class="col-md-6 form-horizontal">

	<div class="form-group">
		<label for="" class="col-sm-2 control-label text-left pdl0 pdr0" >우편번호</label>
			<div class="col-sm-6" style="width:120px">
				<input type="" class="form-control" value="" id="postcode" readonly>
			</div>
			<div class="col-sm-4 pdl0" style="width:100px">
				<button type="button" class="btn btn-default col-sm-12 text-center pdr0 pdl0" onclick="execDaumPostcode()">검색</button>
			</div>
</div>

</div>
</div>

<div class="row">
	<div class="col-md-12 form-horizontal">							
		<div class="form-group">
			<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">주소</label>
			<div class="col-sm-11">
				<input type="text" class="form-control" value="" id="address">
			</div>
		</div>
	</div>
</div>
</div>


<!-- 제 2운전자 -->
						<div id="menu1" class="tab-pane fade in" >
							<div class="row pdt15">
								<div class="col-sm-6 form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*고객명</label>
										<div class="col-sm-10">
											<div class="input-group" >
												<input type="text" class="form-control" id="second_user_name">
												<div class="input-group-addon text-point" data-target="#customer_search_dialog" id="second_user_search_btn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
											</div>
										</div>
									</div>
									<!-- <div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">회원번호</label>
										<div class="col-sm-10">
											<input type="" class="form-control" id="user_serial" value="" readonly>
										</div>
									</div> -->
								</div>
								<div class="col-sm-6 form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*생년월일</label>
										<div class="col-sm-10">
											<div class="col-sm-6 pdr0 pdl0">
												<input type="text" class="form-control" id="second_birthday" maxlength="6">
											</div>
											<div class="col-sm-6 pdl0 pdr0">
												<div class="col-sm-1 text-center">
													<label for="" class="col-sm-2 pdl0 pdr0 control-label"><p>-</p></label>
												</div>
												<div class="col-sm-3 pdl0 pdr0">
													<input type="number" size='1' class="form-control" id="second_gender" style="padding:3px 6px;" maxlength="1">
												</div>
												<div class="col-sm-3 pdl10 pdr0">
													<label for="" class="col-sm-2 pdl0 pdr0 control-label text-leftr">******</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 form-horizontal">

									<div class="form-group">
										<!-- <label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*운전자명</label>
										<div class="col-sm-10">
											<div class="input-group">
												<input type="text" class="form-control" id="user_name" placeholder="홍길동">
												<div class="input-group-addon text-point" data-toggle="modal" data-target="#customer_search_dialog" id="user_search_btn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
											</div>
										</div> -->
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">이메일</label>
										<div class="col-sm-10">
											<div class="input-group">
												<input type="text" class="form-control" id="second_user_email">
												<!-- <div class="input-group-addon text-point" data-toggle="modal" data-target="#rent_email_dialog">보내기</div> -->
											</div>
										</div>
									</div>

								</div>
								<div class="col-md-6 form-horizontal">

									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left" >*연락처</label>
										<div class="col-sm-10">
											<div class="input-group">
												<input type="text" class="form-control" id="second_phone_number1" maxlength="12">
												<!-- <div class="input-group-addon text-point" data-toggle="modal" >SMS</div> -->
											</div>
										</div>
									</div>

								</div>
							</div>

							<div class="row">
								<div class="col-sm-6 form-horizontal">
									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">긴급연락처</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="second_phone_number2" maxlength="12">
										</div>
									</div>
								</div>
								<div class="col-md-6 form-horizontal">

									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">면허종류</label>
										<div class="col-sm-10">
											<select class="form-control" id="second_license_type">
												<option value="1종 대형">1종 대형</option>
												<option value="1종 보통">1종 보통</option>
												<option value="2종 보통">2종 보통</option>
												<option value="2종 소형">2종 소형</option>
											</select>
										</div>
									</div>

								</div>
							</div>

							<div class="row">
								<div class="col-md-6 form-horizontal">

									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">면허번호</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="second_license_number">
										</div>
									</div>

								</div>
								<div class="col-md-6 form-horizontal">

									<div class="form-group">
										<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">발행일</label>
										<div class="col-sm-10">
											<div class='input-group date'>
												<input type='text' class="form-control" id="second_published_date" />
			<!-- 	<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span> -->
			</div>
			<script language="javascript">
				$('#second_published_date').datepicker({
					navTitles: {
						days: 'yyyy년 mm월'
					},
//	todayButton: new Date(),
clearButton: true,
closeButton: true,
timepicker: false,
language: 'en',
startDate: start,

onSelect: function (fd, d, picker) {
		// Do nothing if selection was cleared
		if (!d) return;
	}
})
</script>
</div>
</div>

</div>
</div>

<div class="row">
	<div class="col-md-6 form-horizontal">

		<div class="form-group">
			<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">유효기간</label>
			<div class="col-sm-10">
				<div class='input-group date'>
					<input type='text' class="form-control" id="second_expiration_date" />
											<!-- 	<span class="input-group-addon">
													<span class="glyphicon glyphicon-calendar"></span>
												</span> -->
											</div>
											<script language="javascript">
												$('#second_expiration_date').datepicker({
													navTitles: {
														days: 'yyyy년 mm월'
													},
//	todayButton: new Date(),
clearButton: true,
closeButton: true,
timepicker: false,
language: 'en',
startDate: start,
onSelect: function (fd, d, picker) {
		// Do nothing if selection was cleared
		if (!d) return;
		
	}
})
</script>
</div>
</div>

</div>

<div class="col-md-6 form-horizontal">

	<div class="form-group">
		<label for="" class="col-sm-2 control-label text-left pdl0 pdr0" >우편번호</label>
		<div class="col-sm-6" style="width:120px">
			<input type="" class="form-control" value="" id="second_postcode" readonly>
		</div>
		<div class="col-sm-4 pdl0" style="width:100px">
			<button type="button" class="btn btn-default col-sm-12 text-center pdr0 pdl0" onclick="execDaumPostcode()">검색</button>
		</div>
	</div>

</div>
</div>

<div class="row">
	<div class="col-md-12 form-horizontal">							
		<div class="form-group">
			<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">주소</label>
			<div class="col-sm-11">
				<input type="text" class="form-control" value="" id="second_address">
			</div>
		</div>
	</div>
</div>
</div>


<!-- 
<div id="menu1" class="tab-pane fade in">
	<div class="row pdt15">
		<div class="col-sm-6 form-horizontal">
			<div class="form-group">
				<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">회원번호</label>
				<div class="col-sm-10">
					<input type="" class="form-control" id="second_user_serial" value="" readonly>
				</div>
			</div>
		</div>
		<div class="col-sm-6 form-horizontal">
			<div class="form-group">
				<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">이메일</label>
				<div class="col-sm-10">
					<div class="input-group">
						<input type="text" class="form-control" id="second_user_email">
						<div class="input-group-addon text-point" data-toggle="modal" data-target="#rent_email_dialog">보내기</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 form-horizontal">

			<div class="form-group">
				<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*운전자명</label>
				<div class="col-sm-10">
					<div class="input-group">
						<input type="text" class="form-control" id="second_user_name" placeholder="홍길동">
						<div class="input-group-addon text-point" data-toggle="modal" data-target="#customer_search_dialog" id="user_search_btn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
					</div>
				</div>
			</div>

		</div>
		<div class="col-md-6 form-horizontal">

			<div class="form-group">
				<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*생년월일</label>
				<div class="col-sm-10">
					<div class="col-sm-6 pdr0 pdl0">
						<input type="text" class="form-control" id="second_birthday">
					</div>
					<div class="col-sm-6 pdl0 pdr0">
						<div class="col-sm-1 text-center">
							<label for="" class="col-sm-2 pdl0 pdr0 control-label"><p>-</p></label>
						</div>
						<div class="col-sm-3 pdl0 pdr0">
							<input type="number" size='1' class="form-control" id="second_gender" style="padding:3px 0;">
						</div>
						<div class="col-sm-3 pdl10 pdr0">
							<label for="" class="col-sm-2 pdl0 pdr0 control-label text-leftr">******</label>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 form-horizontal">
			<div class="form-group">
				<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*연락처</label>
				<div class="col-sm-10">
					<div class="input-group">
						<input type="text" class="form-control" id="second_phone_number1">
						<div class="input-group-addon text-point" data-toggle="modal" >SMS</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 form-horizontal">

			<div class="form-group">
				<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">긴급연락처</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="second_phone_number2">
				</div>
			</div>

		</div>
	</div>

	<div class="row">
		<div class="col-md-6 form-horizontal">

			<div class="form-group">
				<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">면허정보</label>
				<div class="col-sm-10">
					<select class="form-control" id="second_license_type">
						<option value="1종 보통">1종 보통</option>
						<option value="2종 보통">2종 보통</option>
					</select>
				</div>
			</div>

		</div>
		<div class="col-md-6 form-horizontal">

			<div class="form-group">
				<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">면허번호</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="second_license_number">
				</div>
			</div>

		</div>
	</div>

	<div class="row">
		<div class="col-md-6 form-horizontal">

			<div class="form-group">
				<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">유효기간</label>
				<div class="col-sm-10">
					<div class='input-group date'>
						<input type='text' class="form-control" id="second_expiration_date" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					<script language="javascript">
						$('#expiration_date').datepicker({
							navTitles: {
								days: 'yyyy년 mm월'
							},
//	todayButton: new Date(),
clearButton: true,
closeButton: true,
timepicker: false,
language: 'en',
startDate: start,
onSelect: function (fd, d, picker) {
		// Do nothing if selection was cleared
		if (!d) return;
		
	}
})
</script>
</div>
</div>

</div>
<div class="col-md-6 form-horizontal">

	<div class="form-group">
		<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">발행일</label>
		<div class="col-sm-10">
			<div class='input-group date'>
				<input type='text' class="form-control" id="second_published_date" />
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>
			</div>
			<script language="javascript">
				$('#published_date').datepicker({
					navTitles: {
						days: 'yyyy년 mm월'
					},
//	todayButton: new Date(),
clearButton: true,
closeButton: true,
timepicker: false,
language: 'en',
startDate: start,

onSelect: function (fd, d, picker) {
		// Do nothing if selection was cleared
		if (!d) return;
	}
})
</script>
</div>
</div>

</div>
</div>

<div class="row">
	<div class="col-md-6 form-horizontal">								
		<div class="form-group">
			<label for="" class="col-sm-2 control-label text-left pdl0 pdr0" >우편번호</label>
			<div class="col-sm-6" style="width:120px">
				<input type="" class="form-control" value="" id="second_postcode">
			</div>
			<div class="col-sm-4 pdl0" style="width:100px">
				<button type="button" class="btn btn-default col-sm-12 text-center pdr0 pdl0" onclick="execDaumPostcode()">검색</button>
			</div>
		</div>
	</div>
	<div class="col-md-6 form-horizontal">							
		<div class="form-group">
			<label for="" class="col-sm-2 control-label text-left pdl0 pdr0">상세주소</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" value="" id="second_address">
			</div>
		</div>
	</div>
</div>





</div> -->
</div>
</div>
</div>
</div>
</div>
</div>

<div class="modal-body bt_ccc">
	<div class="row rent_input">
		<div class="col-md-12">
			<div class="row">
				<h5>보험대차</h5>
			</div>
			<div class="row pdt15">
				<div class="col-md-6 form-horizontal">

					<div class="form-group">
						<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">피해차종</label>
						<div class="col-sm-10">
							<input id="input_damaged_cartype" class="form-control" value="">
						</div>
					</div>

				</div>
				<div class="col-md-6 form-horizontal">

					<div class="form-group">
						<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">차량번호</label>
						<div class="col-sm-10">
							<input id="input_damaged_carnumber" class="form-control" value="">
						</div>
					</div>

				</div>
			</div>
			<div class="row">
				<div class="col-md-6 form-horizontal">

					<div class="form-group">
						<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">보험사</label>
						<div class="col-sm-10">
							<input id="input_insurance_company" class="form-control" value="">
						</div>
					</div>

				</div>
				<div class="col-md-6 form-horizontal">

					<div class="form-group">
						<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">접수번호</label>
						<div class="col-sm-10">
							<input id="input_insurance_number" class="form-control" value="">
						</div>
					</div>

				</div>
			</div>
			<div class="row">
				<div class="col-md-6 form-horizontal">

					<div class="form-group">
						<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">청구일자</label>
						<div class="col-sm-10">
							<div class='input-group date'>
								<input type='text' class="form-control" id="input_insurance_date" />
								<!-- <span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span> -->
							</div>
						</div>
					</div>

				</div>
				<div class="col-md-6 form-horizontal">

					<div class="form-group">
						<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">청구차량</label>
						<div class="col-sm-10">
							<input id="input_accident_car" class="form-control" value="">
						</div>
					</div>

				</div>
			</div>
			<div class="row">
				<div class="col-md-6 form-horizontal">

					<div class="form-group">
						<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">연락처</label>
						<div class="col-sm-10">
							<input id="input_insurance_phone" class="form-control" value="">
						</div>
					</div>

				</div>
				<div class="col-md-6 form-horizontal">

					<div class="form-group">
						<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">팩스</label>
						<div class="col-sm-10">
							<input id="input_insurance_fax" class="form-control" value="">
						</div>
					</div>

				</div>
			</div>
			<div class="row">
				<div class="col-md-6 form-horizontal">

					<div class="form-group">
						<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">정비소</label>
						<div class="col-sm-10">
							<!-- <div class="input-group"> -->
							<input id="input_fix_company" type="text" class="form-control">
							<!-- 																			<div class="input-group-addon">검색</div> -->
							<!-- </div> -->
						</div>
					</div>

				</div>
				<div class="col-md-6 form-horizontal">

					<div class="form-group">
						<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">연락처</label>
						<div class="col-sm-10">
							<input id="input_fix_phone" type="" class="form-control" value="">
						</div>
					</div>

				</div>
			</div>
			<div class="row">
				<div class="col-md-6 form-horizontal">

					<div class="form-group">
						<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">과실비율</label>
						<div class="col-sm-10">
							<input id="input_falut_percnet" type="number" class="form-control" value="">
						</div>
					</div>

				</div>
				<div class="col-md-6 form-horizontal">

					<div class="form-group">
						<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">담당자</label>
						<div class="col-sm-10">
							<input id="input_insurance_manager" class="form-control" value="">
							<!-- <input id="input_insurance_date" type="" class="form-control" value=""> -->

						</div>
					</div>

				</div>
			</div>

			<script language="javascript">
// var insurance_accept_date;
$('#input_insurance_date').datepicker({
	navTitles: {
		days: 'yyyy년 mm월'
	},
//	todayButton: new Date(),
clearButton: true,
closeButton: true,
timepicker: false,
language: 'en',
startDate: start,

onSelect: function (fd, d, picker) {
		// Do nothing if selection was cleared

		if (!d) return;
		// insurance_accept_date = d;

	}
})
</script>

</div>
</div>
</div>
</div>
								<!-- </div>
							</div> -->
							<!-- </div> -->



							<div class="col-sm-6 pdl0">

								<div class="modal-body bt_ccc">
									<div class="row">
										<h5>요금정보</h5>
										<div class="rent_input">
											<div class="col-md-12">
												<div class="row pdt15">
													<div class="col-md-6 form-horizontal">

														<div class="form-group">
															<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*계약기간</label>
															<div class="col-sm-10">
																<input type="" class="form-control" id="contract_period" readonly >
															</div>
														</div>

													</div>
													<div class="col-md-6 form-horizontal">

														<div class="form-group">
															<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*대여금액</label>
															<div class="col-sm-10 price">
																<input type="" class="form-control" id="rental_price" value="" >
																<span>원</span>
															</div>
														</div>

													</div>
												</div>
												<div class="row">
													<!--<div class="col-md-6 form-horizontal">

														<div class="form-group">
															 <label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">할인선택1</label>
															<div class="col-sm-10">
																<select class="form-control" id="sel_period_price_serial">
																	<option>--------</option>
																	<option value="N">적용안함</option>
																	<?for($i=0;$i<count($period_price);$i++){?>
																	<option value="<?=$period_price[$i]['serial'];?>"><?=number_format($period_price[$i]['days']);?>일 이상 할인(-<?=(100-$period_price[$i]['percent']);?>%)</option>
																	<?}?>
																</select>
															</div> 
														</div>

													</div>-->
													<div class="col-md-6 form-horizontal">

														<div class="form-group">
															<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">할인금액</label>
															<div class="col-sm-10 price">
																<input type="" class="form-control" id="price_off" value="" >
																<span>원</span>
															</div>
														</div>

													</div>
													<div class="col-md-6 form-horizontal">

														<div class="form-group">
															<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">*총결제금액</label>
															<div class="col-sm-10 price">
																<input type="" class="form-control" id="total_price" value="" disabled style="font-weight: bold; color: blue">
																<span>원</span>
															</div>
														</div>

													</div>
												</div>
												<!--
												<div class="row">
													<div class="col-md-6 form-horizontal">

														<div class="form-group">
															<!-- <label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">할인선택2</label>
															<div class="col-sm-10">
																<select class="form-control" id="sel_special_price_serial">
																	<option>--------</option>
																	<option value="N">적용안함</option>
																	<?for($i=0;$i<count($special_price);$i++){?>
																	<option value="<?=$special_price[$i]['serial'];?>"><?=$special_price[$i]['memo'];?>(-<?=(100-$special_price[$i]['percent']);?>%)<?=date("Y년m월d일", strtotime($special_price[$i]['start_date']))."~".date("Y년m월d일", strtotime($special_price[$i]['end_date']));?></option>
																	<?}?>
																</select>
															</div> 
														</div>

													</div>
													<div class="col-md-6 form-horizontal">

														<div class="form-group">
															<!-- <label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">기타할인</label>
															<div class="col-sm-10 price">
																<input type="" class="form-control" id="price_off2" value="" placeholder="예)22000">
																<span>원</span>
															</div> 
														</div>

													</div>
												</div>-->

												<div class="row">
													<div class="col-md-6 form-horizontal" >

														<div class="form-group">
															<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left" >배회차</label>
															<div class="col-sm-5" style="width:120px">
																<select class="form-control" id="sel_delivery">
																	<option>배차</option>
																	<option>회차</option>
																	<option>배회차</option>
																</select>
															</div>
															<div class="col-sm-5 pdl0 price" style="width:100px">
																<input type="number" id="input_delivery" class="form-control" value=" " >
																<span>원</span>
															</div>
														</div>

													</div>

													<div class="col-md-6 form-horizontal" >

														<div class="form-group">
															<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left" >보험금액</label>
															<div class="col-sm-5" style="width:120px">
																<select class="form-control" id="sel_insurance_name">
																	<option>자차1</option>
																	<option>자차2</option>
																	<option>자차3</option>
																</select>
															</div>
															<div class="col-sm-5 pdl0 price" style="width:100px">
																<input type="number" class="form-control" id="insurance_price" value="">
																<span>원</span>
															</div>
														</div>

													</div>

												</div>

												<div class="row">
													<div class="col-md-6 form-horizontal">

														<div class="form-group">
															<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">초과/환불</label>
															<div class="col-sm-5" style="width:120px">
																<select class="form-control" id="sel_overcharge">
																	<option>초과(+)</option>
																	<option>환불(-)</option>
																</select>
															</div>
															<div class="col-sm-5 pdl0 price" style="width:100px">
																<input type="number" id="input_overcharge" class="form-control" value="">
																<span>원</span>
															</div>
														</div>

													</div>

													<div class="col-md-6 form-horizontal">

														<div class="form-group">
															<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">유료옵션</label>
															<div class="col-sm-5" style="width:120px">
																<select class="form-control" id="sel_option">
																	<option>O</option>
																	<option>X</option>
																</select>
															</div>
															<div class="col-sm-5 pdl0 price" style="width:100px">
																<input type="number" id="input_option" class="form-control" value=" ">
																<span>원</span>
															</div>
														</div>

													</div>
												</div>

												<div class="row">
													<div class="col-md-6 form-horizontal">

														<div class="form-group">
															<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">사고/수리</label>
															<div class="col-sm-5" style="width:120px">
																<select class="form-control" id="sel_re_or_acc">
																	<option>O</option>
																	<option>X</option>
																</select>
															</div>
															<div class="col-sm-5 pdl0 price" style="width:100px">
																<input type="number" id="input_re_or_acc" class="form-control" value="">
																<span>원</span>
															</div>
														</div>
													</div>
													<div class="col-md-6 form-horizontal">

														<div class="form-group">
															<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">기타</label>
															<div class="col-sm-5" style="width:120px">
																<select class="form-control" id="sel_etc">
																	<option>O</option>
																	<option>X</option>
																</select>
															</div>
															<div class="col-sm-5 pdl0 price" style="width:100px">
																<input type="number" id="input_etc" class="form-control" value=" ">
																<span>원</span>
															</div>
														</div>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>							




								<div class="modal-body bt_ccc">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<h5>대여정보</h5>
											</div>
											<div class="row pdt15">
												<div class="col-md-6 form-horizontal">

													<div class="form-group">
														<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">대여일시</label>
														<div class="col-sm-10">
															<div class='input-group date'>
																<input type='text' class="form-control" id='rental_period' />
<!-- 																<span class="input-group-addon">
																	<span class="glyphicon glyphicon-calendar"></span>
																</span> -->
															</div>
															<script language="javascript">
																$('#rental_period').datepicker({
																	navTitles: {
																		days: 'yyyy년 mm월'
																	},
//	todayButton: new Date(),
clearButton: true,
closeButton: true,
timepicker: true,
language: 'en',
startDate: start,
minHours: startHours,
maxHours: finishHours,
minutesStep: 10,
onSelect: function (fd, d, picker) {
		// Do nothing if selection was cleared
		if (!d) return;

		var day = d.getDay();
		// Trigger only if date is changed
		// if (prevDay != undefined && prevDay == day) return;
		// prevDay = day;
		// If chosen day is Saturday or Sunday when set
		// hour value for weekends, else restore defaults
		if (day == 6 || day == 0) {
			picker.update({
				minHours: startHours,
				maxHours: finishHours
			})
		} else {
			picker.update({
				minHours: startHours,
				maxHours: finishHours
			})
		}
		real_start_date = d;
	}
})
</script>
</div>
</div>

</div>
											<!-- <div class="col-md-6 form-horizontal">

													<div class="form-group">
														<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">대여담당자</label>
														<div class="col-sm-10">
															<select class="form-control" id="rental_staff_serial">
															<?for($i=0;$i<count($staff_list);$i++){?>
																<option value="<?=$staff_list[$i]['serial'];?>"><?=$staff_list[$i]['admin_name'];?></option>
															<?}?>
															</select>
														</div>
													</div>

												</div> -->


												<div class="col-md-6 form-horizontal">

													<div class="form-group">
														<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">반납일시</label>
														<div class="col-sm-10">
															<div class='input-group date'>
																<input type='text' class="form-control" id='return_period' />
		<!-- 														<span class="input-group-addon">
																	<span class="glyphicon glyphicon-calendar"></span>
																</span> -->
															</div>
															<script language="javascript">
																$('#return_period').datepicker({
																	navTitles: {
																		days: 'yyyy년 mm월'
																	},
//	todayButton: new Date(),
clearButton: true,
closeButton: true,
timepicker: true,
language: 'en',
startDate: start,
minHours: startHours,
maxHours: finishHours,
minutesStep: 10,
onSelect: function (fd, d, picker) {
		// Do nothing if selection was cleared
		if (!d) return;

		var day = d.getDay();
		// Trigger only if date is changed
		// if (prevDay != undefined && prevDay == day) return;
		// prevDay = day;
		// If chosen day is Saturday or Sunday when set
		// hour value for weekends, else restore defaults
		if (day == 6 || day == 0) {
			picker.update({
				minHours: startHours,
				maxHours: finishHours
			})
		} else {
			picker.update({
				minHours: startHours,
				maxHours: finishHours
			})
		}

		real_finish_date = d;
	}
})
</script>
</div>
</div>

</div>



											<!-- <div class="col-md-6 form-horizontal">

													<div class="form-group">
														<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">반납담당자</label>
														<div class="col-sm-10">
															<select class="form-control" id="return_staff_serial">
															<?for($i=0;$i<count($staff_list);$i++){?>
																<option value="<?=$staff_list[$i]['serial'];?>"><?=$staff_list[$i]['admin_name'];?></option>
															<?}?>
															</select>
														</div>
													</div>

												</div> -->
											</div>

											<div class="row">
												<div class="col-md-6 form-horizontal">

													<div class="form-group">
														<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">대여장소</label>
														<div class="col-sm-10">
															<input id="delivery_place_order" type="" class="form-control" value="" >
														</div>
													</div>

												</div>
											<!-- <div class="col-md-6 form-horizontal">

													<div class="form-group">
														<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">대여일시</label>
														<div class="col-sm-10">
															<div class='input-group date' id='rental_period'>
															<input type='text' class="form-control" />
																<span class="input-group-addon">
																	<span class="glyphicon glyphicon-calendar"></span>
																</span>
															</div>
															<script type="text/javascript">
																$(function () {
																	$('#rental_period').datetimepicker({
																		locale: 'ko',
																		stepping: 10,
																		format: 'YYYY년 MM월 DD일 HH시 mm분',
																		sideBySide: true
																	});
																});
															</script>
														</div>
													</div>

												</div> -->
												<div class="col-md-6 form-horizontal">

													<div class="form-group">
														<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">반납장소</label>
														<div class="col-sm-10">
															<input id="pickup_place_order" type="" class="form-control" value="" >
														</div>
													</div>

												</div>
											</div>


											<div class="row">
												<div class="col-md-6 form-horizontal">

													<div class="form-group">
														<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">대여담당자</label>
														<div class="col-sm-10">
															<select class="form-control" id="rental_staff_serial">
																<option value="0">----------</option>
																<?for($i=0;$i<count($staff_list);$i++){?>
																<option value="<?=$staff_list[$i]['serial'];?>"><?=$staff_list[$i]['admin_name'];?></option>
																<?}?>
															</select>
														</div>
													</div>

												</div>
												<div class="col-md-6 form-horizontal">

													<div class="form-group">
														<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">반납담당자</label>
														<div class="col-sm-10">
															<select class="form-control" id="return_staff_serial">
																<option value="0">----------</option>
																<?for($i=0;$i<count($staff_list);$i++){?>
																<option value="<?=$staff_list[$i]['serial'];?>"><?=$staff_list[$i]['admin_name'];?></option>
																<?}?>
															</select>
														</div>
													</div>

												</div>
											<!-- <div class="col-md-6 form-horizontal">

													<div class="form-group">
														<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">대여일시</label>
														<div class="col-sm-10">
															<div class='input-group date' id='rental_period'>
															<input type='text' class="form-control" />
																<span class="input-group-addon">
																	<span class="glyphicon glyphicon-calendar"></span>
																</span>
															</div>
															<script type="text/javascript">
																$(function () {
																	$('#rental_period').datetimepicker({
																		locale: 'ko',
																		stepping: 10,
																		format: 'YYYY년 MM월 DD일 HH시 mm분',
																		sideBySide: true
																	});
																});
															</script>
														</div>
													</div>

												</div> -->

											</div>
											<div class="row">
												<div class="col-md-6 form-horizontal">

													<div class="form-group">
														<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">대여주유량</label>
														<div class="col-sm-10 percent">
															<input id="start_fuel" class="form-control" value="number" maxlength="3">
															<span>%</span>
														</div>
													</div>

												</div>
												<div class="col-md-6 form-horizontal">

													<div class="form-group">
														<label for="" class="col-sm-2 pdl0 pdr0 control-label text-left">반납주유량</label>
														<div class="col-sm-10 percent">
															<input id="end_fuel" class="form-control" value="number"  maxlength="3">
															<span>%</span>
														</div>
													</div>

												</div>
											</div>

											<div class="row">
												<div class="col-md-12 form-horizontal">

													<div class="form-group">
														<label for="" class="col-sm-1 control-label text-left	pdl0 pdr0">대여키로수</label>
														<div class="col-sm-3 km" style="width:127px">
															<input id="start_km" type="number" class="form-control" value="" >
															<span>Km</span>
														</div>
														<label for="" class="col-sm-1 control-label text-left	pdl0 pdr0">반납키로수</label>
														<div class="col-sm-3 km" style="width:127px">
															<input id="end_km" type="number" class="form-control" value=""  >
															<span>Km</span>
														</div>
														<label for="" class="col-sm-1 control-label text-left	pdl0 pdr0">주행키로수</label>
														<div class="col-sm-3 km" style="width:128px">
															<input id="drive_km" type="number" class="form-control" value=""  >
															<span>Km</span>
														</div>
													</div>

												</div>
											</div>

											<div class="row">
												<div class="col-md-12 form-horizontal">

													<div class="form-group magb0">
														<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">비고사항</label>
														<div class="col-sm-11">
															<input id="memo_text" type="" class="form-control" value=" " style="height:50px">
														</div>
													</div>

												</div>
											</div>




										</div>
									</div>
								</div>

								<!-- <div class="modal-body bt_ccc"> -->
<!-- 									<div class="row">
										<div class="col-md-12 form-horizontal">

										

										</div>
									</div>

								</div> -->

								<div class="modal-body bt_ccc" style="padding-left:17px;">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<h5>수납관리</h5>
												</div>
												<div class="col-sm-6">
													<button id="receipt_register" type="button" class="btn btn-default pull-right"	data-toggle="modal" data-target="#myModal3">등록</button>
												</div>
											</div>
											<div class="row pdt15">
												<div class="col-md-12">
													<div class="panel panel-default magb0">
														<div class="panel-heading">
															<div class="row">
																<div class="col-md-12 form-horizontal">

																	<div class="form-group magb0">
																		<label for="" class="col-md-1 control-label text-left pdr0 pdl0">총액</label>
																		<div class="col-sm-3 price">
																			<input type="number" class="form-control" id="input_total_price" readonly style="background:#FFF;">
																			<span class="accep_price">원</span>
																		</div>
																		<label for="" class="col-md-1 control-label text-left pdr0 pdl0">입금</label>
																		<div class="col-sm-3 price">
																			<input type="number" class="form-control" id="input_inserted_price" readonly style="background:#FFF;">
																			<span class="accep_price">원</span>
																		</div>
																		<label for="" class="col-md-1 control-label text-left pdr0 pdl0">미수</label>
																		<div class="col-sm-3 price">
																			<input type="number" class="form-control" id="input_remain_price" readonly style="background:#FFF;">
																			<span class="accep_price">원</span>
																		</div>
																	</div>

																</div>
															</div>
															<!-- <div class="row"> -->
																<!-- <div class="col-md-4 text-right pdl0 pdt15"> -->
																	<!-- <div class="btn-group" role="group" aria-label="..."> -->
																		<!-- <button id="receipt_register" type="button" class="btn btn-default"	data-toggle="modal" data-target="#myModal3">등록</button> -->
																		<!-- <button id="receipt_update" type="button" class="btn btn-default">수정</button>
																		<button id="receipt_delete" type="button" class="btn btn-default">삭제</button> -->
																	<!-- </div> -->
																<!-- </div> -->
															<!-- </div> -->
														</div>
										<!-- <div class="panel-body p_scroll pdt0">
											<div class="row">
												<div id="table_receipt" class="cash_data">
													<ul>
														<li>입금일시</li>
														<li>타입</li>
														<li>수금방식</li>
														<li>입금금액</li>
														<li>권번</li>
														<li>메모사항</li>
														<li>처리자</li>
														<li>등록일시</li>
													</ul>


												</div>
											</div>
										</div> -->
										<div id="acceptance_grid" style="border:none;">
											
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>

			</div>
		</div>
	</div>


</div>

<div class="modal-footer">
	<!-- <button type="button" class="btn" data-dismiss="modal">계약서</button> -->
	<button type="button" class="btn pull-left" id="btn_reservation_delete">삭제</button>
	<button type="button" class="btn" data-dismiss="modal">예약내역전송</button>
	<button type="button" class="btn" data-dismiss="modal">닫기</button>
	<button type="button" class="btn action_btn" id="btn_save">저장하기</button>
</div>
</div>
</div>
</div>
<!--/큰 모달 -->


<!-- 작은 모달 -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog magt20_p modal_dialog500">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h5 class="modal-title" id="myModalLabel">수납관리</h5>
			</div>
			<div class="modal-body">
				<div class="row pdt15">
					<div class="col-md-12">
						<form class="form-horizontal">
							<div class="form-group">
								<label for="" class="col-md-2 control-label text-left pdr0">대여자명</label>
								<div class="col-md-10">
									<input id="receipt_username" type="" class="form-control" value="" disabled>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form class="form-horizontal">
							<div class="form-group">
								<label for="" class="col-md-2 control-label text-left pdr0">입금일시</label>
								<div class="col-md-10">
									<div class='input-group date' >
										<input type='text' id="receipt_date" class="form-control" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
<script type="text/javascript">
var receipt_date;
$('#receipt_date').datepicker({
	navTitles: {
		days: 'yyyy년 mm월'
	},
//	todayButton: new Date(),
clearButton: true,
closeButton: true,
language: 'en',
startDate: start,
onSelect: function (fd, d, picker) {
		// Do nothing if selection was cleared
		if (!d) return;
		receipt_date = d;


	}
});

</script>
														</div>


													</div>
												</form>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<form class="form-horizontal">
													<div class="form-group">
														<label for="" class="col-md-2 control-label text-left pdr0">입금타입</label>
														<div class="col-md-10 form-horizontal">
															<select id="select_receipt_type" class="form-control" plat>
																<!-- <option>(비용이 발생한 서비스)</option> -->
																<option>대여금액</option>
																<option>대여기간할인</option>
																<option>특정기간할인</option>
																<option>기타할인</option>
																<option>배차</option>
																<option>회차</option>
																<option>배회차</option>
																<option>자차1</option>
																<option>자차2</option>
																<option>자차3</option>
																<option>초과</option>
																<option>환불</option>
																<option>카시트</option>
																<option>스노우체인</option>
																<option>기타유료옵션</option>
																<option>수리비</option>
																<option>청구손실비</option>
																<option>사고면책금</option>
																<option>사고보상금</option>
																<option>유류비용</option>
																<option>범칙금</option>
																<option>보증금</option>
																<option>기타</option>
															</select>
														</div>
													</div>
												</form>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<form class="form-horizontal">
													<div class="form-group">
														<label for="" class="col-md-2 control-label text-left pdr0">수납방식</label>
														<div class="col-md-10 form-horizontal">
															<div class="col-sm-10 pdr0 pdl0">
																<select id="receipt_pay_type" type="" class="form-control" value="" >
																</select>
															</div>
															<div class="col-sm-2">
																<a class="btn btn-default" id="button_new_deposite_way" data-toggle="modal" data-target="#dialog_acceptance_system">추가</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<form class="form-horizontal">
													<div class="form-group">
														<label for="" class="col-md-2 control-label text-left pdr0">입금금액</label>
														<div class="col-md-10 price">
															<input id="receipt_input_price" type="" class="form-control" value="" placeholder="할인, 환불은(-)로 입력" >
															<span>원</span>
														</div>
													</div>
												</form>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<form class="form-horizontal">
													<div class="form-group">
														<label for="" class="col-md-2 control-label text-left pdr0">메모사항</label>
														<div class="col-md-10">
															<textarea id="receipt_memo" class="form-control" rows="3"></textarea>
														</div>
													</div>
												</form>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<form class="form-horizontal">
													<div class="form-group">
														<label for="" class="col-md-2 control-label text-left pdr0">세금계산서</label>
														<div class="col-md-10 form-horizontal">
															<input id="receipt_tax" type="" class="form-control" value="" placeholder="(권번, 일련번호 기재)">
														</div>
													</div>
												</form>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<form class="form-horizontal">
													<div class="form-group">
														<label for="" class="col-md-2 control-label text-left pdr0">수금담당자</label>
														<div class="col-md-10">
															<select id="select_staff" class="form-control">
																<option value="0">----------</option>
																<?for($i=0;$i<count($staff_list);$i++){?>
																<option value="<?=$staff_list[$i]['serial'];?>"><?=$staff_list[$i]['admin_name'];?></option>
																<?}?>
															</select>
														</div>
													</div>
												</form>
											</div>
										</div>

									</div>
									<div class="modal-footer">
										<button id="receipt_delete" type="button" class="btn btn-default pull-left" data-dismiss="modal">삭제</button>
										<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
										<button id="receipt_save" type="button" class="btn action_btn">저장</button>
									</div>
								</div>
							</div>
						</div>
						<!-- /작은 모달 -->



						<!-- Modal 예약가능 차량검색-->
						<div class="modal fade" id="rent_car_search_dialog" role="dialog">
							<div class="modal-dialog w700">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button id="" type="button" class="close" data-dismiss="modal">&times;</button>
										<h5 id="" class="modal-title">예약 가능 차량 검색</h5>
									</div>

									<div class="modal-body">
										<div class="row">
					<!-- 10-11 16:46 이승원 대표님 제거
					<div class="col-sm-12 pdt15">
						<form class="form-horizontal">
							<div class="form-group">
								<div class="col-sm-11">
									<label for="" class="col-sm-2 control-label text-left pdl0">대여예정</label>
									<div class="col-sm-4 pdl0">
										<input type="" class="form-control" value="2016-01-24 14:30">
									</div>
									<label for="" class="col-sm-2 control-label text-left pdl0">반납예정</label>
									<div class="col-sm-4 pdl0">
										<input type="" class="form-control" value="2016-01-24 14:30">
									</div>
								</div>
								<div class="col-sm-1">
									<button id="" type="button" class="btn action_btn pull-right">검색</button>
								</div>
							</div>
						</form>
					</div>
				-->

				<div class="col-md-12 pdt15 ">
					<ul class="nav nav-tabs border_none">
						<li class="active"><a data-toggle="tab" href="#car_view01" id="rent_car_tab1">전체</a></li>
						<li><a data-toggle="tab" href="#car_view02" id="rent_car_tab2">대형</a></li>
						<li><a data-toggle="tab" href="#car_view03" id="rent_car_tab3">중형</a></li>
						<li><a data-toggle="tab" href="#car_view04" id="rent_car_tab4">준중형</a></li>
						<li><a data-toggle="tab" href="#car_view05" id="rent_car_tab5">소형</a></li>
						<li><a data-toggle="tab" href="#car_view05" id="rent_car_tab6">경형</a></li>
						<li><a data-toggle="tab" href="#car_view05" id="rent_car_tab7">SUV</a></li>
						<li><a data-toggle="tab" href="#car_view05" id="rent_car_tab8">승합</a></li>
						<li><a data-toggle="tab" href="#car_view05" id="rent_car_tab9">수입</a></li>
					</ul>
				</div>
				<div class="col-md-12">
					<div class="tab-content">
						<div class="row">
							<div id="grid_rent_car"></div>
						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="modal-footer">
			<button id="" type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
			<button id="rent_car_select_btn" type="button" class="btn action_btn">선택</button>
		</div>

	</div>
</div>
</div>


<!-- Modal 수납방식 추가  -->
<div class="modal fade" id="dialog_acceptance_system" role="dialog">
	<div class="modal-dialog" style="width:500px;">
		<div class="modal-content">
			<div class="modal-header">
				<!-- <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button> -->
				<h5 class="modal-title" id="">수납방식관리</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">결제방식</label>
							<div class="col-sm-11">
								<input type="" id="input_deposite_way" class="form-control" value="">
							</div>
						</div>
					</form>
				</div>
				<div class="row">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">특이사항</label>
							<div class="col-sm-11">
								<input type="" id="input_deposite_memo" class="form-control" value="">
							</div>
						</div>
					</form>
				</div>
				<div class="row pdb15">
					<form class="form-horizontal">
						<!-- <div class="form-group"> -->
							<a class="btn btn-default pull-right" id="button_deposite_way_save">저장</a>
						<!-- </div> -->
					</form>
				</div>
				<div class="row">

				<!-- <table class="table table-bordered">
				  <thead>
				    <tr>
				      <th>결제방식</th>
				      <th>특이사항</th>
				      <th>삭제</th>
				    </tr>
				  </thead>
				  <tbody id="receipt_way_table">
				    <tr>
				      <th scope="row">Mark</td>
				      <td>Otto</td>
				      <td><a class="btn btn-default pull-right">삭제</a></td>
				    </tr>
				    <tr>
				      <th scope="row">Jacob</td>
				      <td>Thornton</td>
				      <td>@fat</td>
				    </tr>
				    <tr>
				      <th scope="row">Larry</td>
				      <td>the Bird</td>
				      <td>@twitter</td>
				    </tr>
				  </tbody>
				</table> -->

					<div id="deposite_way_grid">
					</div>
					<script type="text/javascript">
						$('#deposite_way_grid').kendoGrid({
					        selectable: true, 
					        allowCopy: true, 
					        sortable: true, 
					        groupable: false,
					        resizable: true,
					        pageable: {
					            input: true,
					            messages: {
					                display: "총 {2} 명의 고객 중 {0}~{1}번째",
					                empty: "데이타 없음"
					            }
					          },
					        noRecords: {
					            template: "등록된 고객이 없습니다."
					          },
					        dataSource: {
					            transport: {
					                 read: {
					                   url: '/receiptmanager/get_deposite_ways/0',
					                   dataType: "json"
					                 }
					               },
					            schema: {
					                model: {
					                    fields: {
					                        name: {type: "string"},
					                        memo: { type: "string" },
					                    }
					                }
					            },
					            pageSize: 30
					        },
					        columns: [ {
					                field: "name",
					                title: "결제방식",
					                width: 100
					            },  {
					                field: "특이사항",
					                title: "이름",
					                width: 100
					            },{
					        	title: "삭제",
					        	command : { 
					    			text : "삭제",
					    			// click: deleteDepositeWay 
					    		},
					        	width: 60
					        }]
					    });
					</script>
				</div>
			</div>

			<div class="modal-footer">
				<button id="" type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
				<!-- <button id="" type="button" class="btn action_btn">저장</button> -->
			</div>

		</div>
	</div>
</div>

<!-- Modal 예약내역발송-->
<div class="modal fade" id="rent_email_dialog" role="dialog">
	<div class="modal-dialog w700">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button id="" type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 id="" class="modal-title">예약내역발송</h5>
			</div>

			<div class="modal-body">
				<div class="row">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">이메일</label>
							<div class="col-sm-11">
								<input type="" id="" class="form-control" value="" disabled>
							</div>
						</div>
					</form>
				</div>
				<div class="row">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">연락처</label>
							<div class="col-sm-11">
								<input type="" id="" class="form-control" value="">
							</div>
						</div>
					</form>
				</div>
				<div class="row">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">연락처2</label>
							<div class="col-sm-11">
								<input type="" id="" class="form-control" value="">
							</div>
						</div>
					</form>
				</div>
				<div class="row">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">예약내역</label>
							<div class="col-sm-11">
								<div class="panel-body bg_fff b_ccc b_r4" style="font-size:12px;">
									<h5>[Web발신]</h5>
									<p>[렌고 예약내역 발송]</p>
									<p>▶︎ 예약번호 : 161006612</p>
									<p>▶︎ 예약자명 : 백승환</p>
									<p>▶︎ 생년월일 : 19810724</p>
									<p>▶︎ 연락처 : 01031089596</p>
									<p>▶︎ 차종 : K5</p>
									<p>▶︎ 대여일시 : 2016-10-06(목) 17:00부터</p>
									<p>▶︎ 대여장소 : 부산/부산역</p>
									<p>▶︎ 반납일시 : 2016-10-07(금) 17:00부터</p>
									<p>▶︎ 반납장소 : 부산/부산역</p>
									<p>▶︎ 대여금액 : 70,000원</p>
									<p>▶︎ 보험금액 : 0원(미가입)</p>
									<p>▶︎ 총 결제금액 : 70,000원</p>
									<p>▶︎ 대여지점 명 : 위드렌트카</p>
									<p>▶︎ 대여지점 주소 : 부산시 연제구 대리로6번길 72</p>
									<p class="magb20">▶︎ 대여지점연락처 : 051806000</p>

									<h5>[주의사항]</h5>
									<p>▶︎ 본 상품은 특가예약상품으로 교환과 취소 및 환불이 불가합니다.</p>
									<p>▶︎ 운전자는 운전면허증을 반드시 지참하셔야 합니다.(미 지참시 대여불가)</p>
									<p>▶︎ 차량의 상태에 따라 동급 또는 상위등급의 차량으로 변경될 가능성이 있습니다.</p>
									<p>▶︎ 교통상황에 따라 딜리버리 시간이 변경될 가능성이 있습니다.</p>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="row">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="" class="col-sm-1 control-label text-left pdl0 pdr0">발신번호</label>
							<div class="col-sm-11">
								<input type="" id="" class="form-control" value="" placeholder="예) 1800-1090">
							</div>
						</div>
					</form>
				</div>
			</div>

			<div class="modal-footer">
				<button id="" type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
				<button id="" type="button" class="btn action_btn">전송</button>
			</div>

		</div>
	</div>
</div>


<!-- Modal 예약내역발송-->
<div class="modal fade" id="customer_search_dialog" role="dialog">
	<div class="modal-dialog w700">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button id="" type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 id="" class="modal-title">고객찾기</h5>
			</div>

			<div class="modal-body">
				<div class="row">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="" class="col-sm-5 control-label text-left pdl0 pdr0">고객명/생년월일/전화번호/이메일</label>
							<div class="col-sm-7">
								<div class="input-group custom-search-form">
									<input id="input_user_search" type="text" class="form-control" placeholder="">
									<!-- <span class="input-group-btn">
										<button class="btn btn-default" type="button" id="btn_search">
												<i class="fa fa-search"></i>
										</button>
									</span> -->
								</div>
							</div>
						</div>
					</form>
					<div id="search_grid">
						
					</div>
				</div>




			</div>

			<div class="modal-footer">
				<button id="" type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
				<button id="user_select_btn" type="button" class="btn action_btn">선택</button>
			</div>

		</div>
	</div>
</div>

<script language="javascript">






//요금 정보 select change 이벤트들 --------------------------------------------------------------------------------------------------------------------
$('#sel_period_price_serial').on("change", function(e) {
	init_rental_price();
});

$('#sel_special_price_serial').on("change", function(e) {
	init_rental_price();
});

$('#sel_insurance_name').on("change", function(e) {
	if($('#sel_insurance_name').val()=="자차1"){
		$("#insurance_price").val(parseInt(car_insurance_self1_price));
		insurance_price = parseInt(car_insurance_self1_price);

	}else if($('#sel_insurance_name').val()=="자차2"){
		$("#insurance_price").val(parseInt(car_insurance_self2_price));
		insurance_price = parseInt(car_insurance_self2_price);
	}else if($('#sel_insurance_name').val()=="자차3"){ 
		$("#insurance_price").val(parseInt(car_insurance_self3_price));
		insurance_price = parseInt(car_insurance_self3_price);
	}else {
		$("#insurance_price").val(0);
		insurance_price = 0;
	}
	refresh_total_price();
});

//일반할인 가격 변경
$('#price_off').on("change", function(e) {
	if($('#price_off').val() == ''){
		discount_price1 = 0;
	}else{
		discount_price1 = parseInt($('#price_off').val());
	}
	
	refresh_total_price();
});

//기타할인 가격 변경
$('#price_off2').on("change", function(e) {
	if($('#price_off2').val() == ''){
		discount_price2 = 0;
	}else{
		discount_price2 = parseInt($('#price_off2').val());
	}
	refresh_total_price();
});

//보험가격 변경
$('#insurance_price').on("change", function(e) {
	if($('#insurance_price').val() == ''){
		insurance_price = 0;
	}else{
		insurance_price = parseInt($('#insurance_price').val());
	}
	refresh_total_price();
});

//배차가격 변경
$('#input_delivery').on("change", function(e) {

	if($('#input_delivery').val() == ''){
		delivery_price = 0;
	}else{
		delivery_price = parseInt($('#input_delivery').val());
	}
	refresh_total_price();
});

//배차가격 변경
$('#rental_price').on("change", function(e) {

	if($('#rental_price').val() == ''){
		rental_price = 0;
	}else{
		rental_price = parseInt($('#rental_price').val());
	}
	refresh_total_price();
});

//옵션가격 변경
$('#input_option').on("change", function(e) {
	if($('#input_option').val() == ''){
		option_price = 0;
	}else{
		option_price = parseInt($('#input_option').val());
	}
	
	refresh_total_price();
});

//사고가격 변경
$('#input_re_or_acc').on("change", function(e) {
	if($('#input_re_or_acc').val() == ''){
		accident_price = 0;
	}else{
		accident_price = parseInt($('#input_re_or_acc').val());
	}
	
	refresh_total_price();
});

//초과가격 변경
$('#input_overcharge').on("change", function(e) {
	if($("#sel_overcharge").val() == "초과(+)"){
		
		if($('#input_overcharge').val() == ''){
			overcharge_price = 0;
		}else{
			overcharge_price = parseInt($('#input_overcharge').val());
		}
		
		payback_price = 0;
	}else{
		overcharge_price = 0;
		if($('#input_overcharge').val() == ''){
			payback_price = 0;
		}else{
			payback_price = parseInt($('#input_overcharge').val());
		}
	}
	refresh_total_price();
});
//대여지역 입력시 자동으로 장소 추가
$('#delivery_place').on("change", function(e) {
	temp_place = $('#delivery_place').val();
	$("#delivery_place_order").val(temp_place);
});

$('#pickup_place').on("change", function(e) {
	temp_place = $('#pickup_place').val();
	$("#pickup_place_order").val(temp_place);
});


$('#sel_overcharge').change(function(){
	if($("#sel_overcharge").val() == "초과(+)"){
        $('#input_overcharge').css('color','black');
        payback_price = 0;
        if($('#input_overcharge').val() == ''){
        	overcharge_price = 0;
        }else{
        	overcharge_price = parseInt($('#input_overcharge').val());
        }

        
       
    }else{
    	$('#input_overcharge').css('color','red');
    	overcharge_price = 0;
    	 if($('#input_overcharge').val() == ''){
        	 payback_price = 0;
        }else{
        	payback_price = parseInt($('#input_overcharge').val());
        }
    	
    }
    refresh_total_price();
});

$('#sel_option').change(function(){
	if($("#sel_option").val() == "O"){
        $('#input_option').attr("readonly",false);
        option_price = parseInt($('#input_option').val());
    }else{
    	$('#input_option').val('');
    	$('#input_option').attr("readonly",true);
    	option_price = 0;
    }
    refresh_total_price();
});

$('#sel_re_or_acc').change(function(){
	if($("#sel_re_or_acc").val() == "O"){
        $('#input_re_or_acc').attr("readonly",false);
        accident_price = parseInt($('#input_re_or_acc').val());
    }else{
    	$('#input_re_or_acc').val('');
    	$('#input_re_or_acc').attr("readonly",true);
    	accident_price = 0;
    }
    refresh_total_price();
});

$('#sel_etc').change(function(){
	if($("#sel_etc").val() == "O"){
        $('#input_etc').attr("readonly",false);
        etc_price = parseInt($('#input_etc').val());
    }else{
    	$('#input_etc').val('');
    	$('#input_etc').attr("readonly",true);
    	etc_price = 0;
    }
    refresh_total_price();
});


$('#start_km').change(function(){
	start = $('#start_km').val();
	end = $('#end_km').val();
	if(start!='' && start!=0){
		if(end!='' && end!=0){	
			drive = parseInt(end) - parseInt(start);
			$("#drive_km").val(drive);
		}else{
			$("#drive_km").val('0')
		}
	}
});
$('#end_km').change(function(){
	start = $('#start_km').val();
	end = $('#end_km').val();
	if(start!='' && start!=0){
		if(end!='' && end!=0){	
			drive = parseInt(end) - parseInt(start);
			$("#drive_km").val(drive);
		}else{
			$("#drive_km").val('0')
		}
	}
});

//기타가격 변경
$('#input_etc').on("change", function(e) {
	etc_price = parseInt($('#input_etc').val());
	refresh_total_price();
});

//요금 계산 로직
function init_rental_price(){

	// period_start_date = $('#rental_plan').data("DateTimePicker").date();
	// period_finish_date = $('#return_plan').data("DateTimePicker").date();

	var company_serial = get_company_serial();
	var branch_serial = 0;
	if(period_start_date != '' && period_start_date != null
		&& period_finish_date != '' && period_finish_date != null
		&& period_start_date < period_finish_date
		&& car_serial >= '1'){

	period_start = moment(period_start_date).format('YYYYMMDDHHmm');
	period_finish = moment(period_finish_date).format('YYYYMMDDHHmm');

	$.post("/rent/get_rental_period",{
		company_serial: company_serial,
		branch_serial: branch_serial,
		car_serial:car_serial,
		period_start:period_start,
		period_finish:period_finish,
		period_price_serial:$('#sel_period_price_serial').val(),
		special_price_serial:$('#sel_special_price_serial').val(),
	}, function(data){
		var result = JSON.parse(data);
//			alert(result['day_price']);
//			alert(result['original_rental_price']);
//			alert(result['rental_price']);
		if(result['code'] == "E01"){
			alert(result['message']);
			return false;
		} else if(result['code'] == "S01"){
			contract_period = result['contract_period'];
			rental_price = parseInt(result['original_rental_price']);
			period_price_serial = result['period_price_serial'];
			special_price_serial = result['special_price_serial'];

			var temp_price = 0;
			if(result['rental_price'] =='' || result['rental_price'] == null){
				temp_price = 0;
			}else{
				temp_price = parseInt(result['rental_price']);
			}
			discount_price1 = 0; //rental_price - temp_price;
			total_rental_price = parseInt(result['rental_price']);
						// alert(total_price);
						$('#contract_period').val(contract_period);
						$('#rental_price').val(rental_price);
						$('#price_off').val(discount_price1);
						$('#total_price').val(total_rental_price);

						insurance_price = 0;
						discount_price2 = 0;


						$("#sel_period_price_serial").val(period_price_serial).prop("selected", true);
						$("#sel_special_price_serial").val(special_price_serial).prop("selected", true);

									//제일 하단에 숫자 변경


						refresh_total_price();
						return true;

			}
		});
	}
}



//예약 저장 하기(수정 포함)
$('#btn_save').click(function() {

	var company_serial = get_company_serial();

	//요효성 검사
	if(period_start_date == ''){
		alert('대여예정일을 선택해 주세요.');
		return false;
	}
	if(period_finish_date == ''){
		alert('반납예정일을 선택해 주세요.');
		return false;
	}
	// if($('#delivery_place').val() == ''){
	// 	alert('대여지역을 입력해 주세요.');
	// 	$('#delivery_place').focus();
	// 	return false;
	// }
	// if($('#pickup_place').val() == ''){
	// 	alert('반납지역을 입력해 주세요.');
	// 	$('#pickup_place').focus();
	// 	return false;
	// }
	if(car_serial == 0){
		alert('차량을 선택해 주세요.');
		$('#car_type').focus();
		return false;
	}
	if($('#contract_period').val() == ''){
		alert('계약 기간 정보가 없습니다.');
		$('#contract_period').focus();
		return false;
	}
	if($('#rental_price').val() == ''){
		alert('대여 요금 정보가 없습니다.');
		$('#rental_price').focus();
		return false;
	}
	if($('#user_name').val() == ''){
		alert('운전자명을 입력해 주세요.');
		$('#user_name').focus();
		return false;
	}
	if($('#birthday').val() == ''){
		alert('생년월일을 입력해 주세요.');
		$('#birthday').focus();
		return false;
	}
	if($('#gender').val() == ''){
		alert('주민번호 뒷자리를 입력해 주세요.');
		$('#gender').focus();
		return false;
	}
	if($('#phone_number1').val() == ''){
		alert('연락처를 입력해 주세요.');
		$('#phone_number1').focus();
		return false;
	}

	$.post("/rent/save_order",{
		serial:order_serial,
		order_kind:$('#order_kind').val(),
		status:$('#status').val(),
		rental_plan:  moment(period_start_date).format('YYYYMMDDHHmm'),  
		return_plan: moment(period_finish_date).format('YYYYMMDDHHmm'), 
		delivery_place:$('#delivery_place').val(),
		pickup_place:$('#pickup_place').val(),
		company_serial:company_serial,
		branch_serial:0,
		car_serial:car_serial,
		//요금정보
		contract_period:$('#contract_period').val(),
		rental_price: rental_price,
		period_price_serial:$('#sel_period_price_serial').val(),
		special_price_serial:$('#sel_special_price_serial').val(),
		price_off: discount_price1,
		payment_text:$('#payment_text').val(),
		total_price: total_rental_price,
		etc_price_off:discount_price2,
		insurance_name:$('#sel_insurance_name').val(),
		insurance_price:insurance_price,
		delivery_type:$('#sel_delivery').val(),
		delivery_price:delivery_price,
		option_type:$('#sel_option').val(),
		option_price:option_price,
		accident_type:$('#sel_re_or_acc').val(),
		accident_price:accident_price,
		overcharge_payback_type:$('#sel_overcharge').val(),
		overcharge_price:overcharge_price,
		payback_price:payback_price,
		etc_type:$('#sel_etc').val(),
		etc_price:etc_price,
		//대여정보
		period_start: (real_start_date =='') ? '' : moment(real_start_date).format('YYYYMMDDHHmm'),  
		period_finish: (real_finish_date == '' ) ? '' : moment(real_finish_date).format('YYYYMMDDHHmm'),  
		rental_staff_serial:$('#rental_staff_serial').val(),
		rental_staff_name:$('#rental_staff_serial option:selected').text(),
		return_staff_serial:$('#return_staff_serial').val(),
		return_staff_name:$('#return_staff_serial option:selected').text(),
		delivery_place_order:$('#delivery_place_order').val(),
		pickup_place_order:$('#pickup_place_order').val(),
		start_fuel:$('#start_fuel').val(),
		end_fuel:$('#end_fuel').val(),
		start_km:$('#start_km').val(),
		end_km:$('#end_km').val(),
		drive_km:$('#drive_km').val(),
		memo_text:$('#memo_text').val(),
		//유저정보
		username:$('#user_name').val(),
		user_mail:$('#user_email').val(),
		user_serial:$('#user_serial').val(),
		tel:$('#phone_number1').val(),
		birthday:$('#birthday').val(),
		gender:$('#gender').val(),
		phone_number2:$('#phone_number2').val(),
		license_type:$('#license_type').val(),
		license_number:$('#license_number').val(),
		published_date:$('#published_date').val(),
		expiration_date:$('#expiration_date').val(),
		postcode:$('#postcode').val(),
		address:$('#address').val(),

		second_username:$('#second_user_name').val(),
		second_user_mail:$('#second_user_email').val(),
		second_user_serial:$('#second_user_serial').val(),
		second_tel:$('#second_phone_number1').val(),
		second_birthday:$('#second_birthday').val(),
		second_gender:$('#second_gender').val(),
		second_phone_number2:$('#second_phone_number2').val(),
		second_license_type:$('#second_license_type').val(),
		second_license_number:$('#second_license_number').val(),
		second_published_date:$('#second_published_date').val(),
		second_expiration_date:$('#second_expiration_date').val(),
		second_postcode:$('#second_postcode').val(),
		second_address:$('#second_address').val(),

		//보험대차부분
		damaged_car_type:$("#input_damaged_cartype").val(),
		damaged_car_number:$("#input_damaged_carnumber").val(),
		insurance_company:$("#input_insurance_company").val(),
		insurance_number:$("#input_insurance_number").val(),
		insurance_manager:$("#input_insurance_manager").val(),
		accident_car_number:$("#input_accident_car").val(),
		accident_phone:$("#input_insurance_phone").val(),
		accident_fax:$("#input_insurance_fax").val(),
		repair_company:$("#input_fix_company").val(),
		repair_phone:$("#input_fix_phone").val(),
		accident_percent:$("#input_falut_percnet").val(),
		insurance_date:$("#input_insurance_date").val(),
	}, function(data){
		var result = JSON.parse(data);
		if(result['code'] == "E01"){
			alert(result['message']);
			return false;
		} else if(result['code'] == "S01"){
//			document.write(result['message']);
			alert("저장 되었습니다.");
			next_day = get_current_date('YYYYMMDD');
			refresh_schedule(next_day);
			$('#myModal2').modal('toggle');
			return true;
		}
	});

});


//예약 상세 정보 가져오기
function order_detail(order_number){
//	temp_date = new Date("2016-11-01 10:30:00");
//	$('#rental_period').data("DateTimePicker").date(temp_date);
	order_serial = order_number;
	alert(order_serial);
	$.post("/rent/order_detail",{
		serial:order_number,
	}, function(data){
		var result = JSON.parse(data);
		if(result['code'] == "E01"){
			alert(result['message']);
			return false;
		} else if(result['code'] == "S01"){
			car_serial = result['car_serial'];
			$('#user_name').val(result['username']);
			$('#delivery_place').val(result['delivery_place']);
			$('#pickup_place').val(result['pickup_place']);
			$('#order_kind').val(result['order_kind']);
			$('#status').val(result['status']);
	//			$('#sel_company_serial').val(result['company_serial']).prop("selected", true);
	//			$('#sel_branch_serial').val(result['branch_serial']).prop("selected", true);
			$('#car_serial').val(result['car_serial']);
			$('#contract_period').val(result['contract_period']);
			$('#rental_price').val(result['rental_price']);
			rental_price = parseInt(result['rental_price']);
			//요금정보
			$('#sel_period_price_serial').val(result['period_price_serial']).prop("selected", true);
			$('#sel_special_price_serial').val(result['special_price_serial']).prop("selected", true);
			$('#price_off').val(result['price_off']);
			if(result['price_off']== null || result['price_off']==''){
				discount_price1 = 0;
			}else{
				discount_price1 = parseInt(result['price_off']);
			}
			$('#insurance_price').val(result['insurance_price']);
			if(result['insurance_price']== null || result['insurance_price']==''){
				insurance_price = 0;
			}else{
				insurance_price = parseInt(result['insurance_price']);
			}
			$('#payment_text').val(result['payment_text']);
			$('#total_price').val(result['total_price']);
			if(result['etc_price_off']== null || result['etc_price_off']==''){
				discount_price2 = 0;
			}else{
				discount_price2 = parseInt(result['etc_price_off']);
			}
			$('#price_off2').val(result['etc_price_off']);
			$('#sel_insurance_name').val(result['insurance_name']);
			if(result['insurance_price']== null || result['insurance_price']==''){
				insurance_price = 0;
			}else{
				insurance_price = parseInt(result['insurance_price']);
			}
			$('#insurance_price').val(result['insurance_price']);
			$('#sel_delivery').val(result['delivery_type']);
			if(result['delivery_price']== null || result['delivery_price']==''){
				delivery_price = 0;
			}else{
				delivery_price = parseInt(result['delivery_price']);
			}
			$('#input_delivery').val(result['delivery_price']);
			$('#sel_option').val(result['option_type']);
			if(result['option_price']== null || result['option_price']==''){
				option_price = 0;
			}else{
				option_price = parseInt(result['option_price']);
			}
			$('#input_option').val(result['option_price']);
			$('#sel_re_or_acc').val(result['accident_type']);
			if(result['accident_price']== null || result['accident_price']==''){
				accident_price = 0;
			}else{
				accident_price = parseInt(result['accident_price']);
			}
			$('#input_re_or_acc').val(result['accident_price']);
			$('#sel_overcharge').val(result['overcharge_payback_type']);
			if(result['overcharge_payback_type'] == "초과(+)"){
				if(result['overcharge_price']== null || result['overcharge_price']==''){
					overcharge_price = 0;
				}else{
					overcharge_price = parseInt(result['overcharge_price']);
				}
				payback_price = 0;
				$('#input_overcharge').val(result['overcharge_price']);
			}else{
				overcharge_price = 0;
				if(result['payback_price']== null || result['payback_price']==''){
					payback_price = 0;
				}else{
					payback_price = parseInt(result['payback_price']);
				}
				$('#input_overcharge').val(result['payback_price']);
			}
			$('#sel_etc').val(result['etc_type']);
			if(result['etc_price']== null || result['etc_price']==''){
				etc_price = 0;
			}else{
				etc_price = parseInt(result['etc_price']);
			}
			$('#input_etc').val(result['etc_price']);
			
			//대여정보
			$('#rental_staff_serial').val(result['rental_staff_serial']).prop("selected", true);
			$('#return_staff_serial').val(result['return_staff_serial']).prop("selected", true);
			$('#delivery_place_order').val(result['delivery_place_order']);
			$('#pickup_place_order').val(result['pickup_place_order']);
			$('#start_fuel').val(result['start_fuel']);
			$('#end_fuel').val(result['end_fuel']);
			$('#start_km').val(result['start_km']);
			$('#end_km').val(result['end_km']);
			$('#drive_km').val(result['drive_km']);
			$('#memo_text').val(result['memo_text']);
			//유저정보
			// $('#user_serial').val(result['user_serial']);
			$('#user_email').val(result['user_mail']);
			$('#phone_number1').val(result['tel']);
			$('#birthday').val(result['birthday']);
			$('#gender').val(result['gender']);
			$('#phone_number2').val(result['phone_number2']);
			$('#license_type').val(result['license_type']);
			$('#license_number').val(result['license_number']);
			if(result['published_date'] != '0000-00-00'){
				$('#published_date').datepicker().data('datepicker').selectDate(new Date(result['published_date']));
			}else{
				$('#published_date').datepicker().data('datepicker').clear();
			}

			if(result['expiration_date'] != '0000-00-00'){
				$('#expiration_date').datepicker().data('datepicker').selectDate(new Date(result['expiration_date']));
			}else{
				$('#expiration_date').datepicker().data('datepicker').clear();
			}
			$('#postcode').val(result['postcode']);
			$('#address').val(result['address']);

			$('#second_user_name').val(result['second_username']);
			$('#second_user_email').val(result['second_user_mail']);
			$('#second_phone_number1').val(result['second_tel']);
			$('#second_birthday').val(result['second_birthday']);
			$('#second_gender').val(result['second_gender']);
			$('#second_phone_number2').val(result['second_phone_number2']);
			$('#second_license_type').val(result['second_license_type']);
			$('#second_license_number').val(result['second_license_number']);
			if(result['second_published_date'] != '0000-00-00'){
				$('#second_published_date').datepicker().data('datepicker').selectDate(new Date(result['second_published_date']));
			}else{
				$('#second_published_date').datepicker().data('datepicker').clear();
			}

			if(result['second_expiration_date'] != '0000-00-00'){
				$('#second_expiration_date').datepicker().data('datepicker').selectDate(new Date(result['second_expiration_date']));
			}else{
				$('#second_expiration_date').datepicker().data('datepicker').clear();
			}
			$('#second_postcode').val(result['second_postcode']);
			$('#second_address').val(result['second_address']);

			if(result['order_kind'] == "보험"){
				enableInsruanse(true);
				$.post("/rent/insurance_detail",{
					serial:order_number,
				}, function(data){
					var result = JSON.parse(data);
					$("#input_damaged_cartype").val(result[0]['damaged_car_type']);
					$("#input_damaged_carnumber").val(result[0]['damaged_car_number']);
					$("#input_insurance_company").val(result[0]['insurance_company']);
					$("#input_insurance_number").val(result[0]['insurance_number']);
					$("#input_insurance_manager").val(result[0]['insurance_manager']);
					$("#input_accident_car").val(result[0]['accident_car_number']);
					$("#input_insurance_phone").val(result[0]['accident_phone']);
					$("#input_insurance_fax").val(result[0]['accident_fax']);
					$("#input_fix_company").val(result[0]['repair_company']);
					$("#input_fix_phone").val(result[0]['repair_phone']);
					$("#input_falut_percnet").val(result[0]['accident_percent']);
					$("#input_insurance_date").val(result[0]['insurance_date']);
				});
			}else{
				enableInsruanse(false);
			}


			car_type = result['car_type'];
			car_number = result['car_number'];
			car_name_detail = result['car_name_detail'];
			car_people = result['car_people'];
			gear_type = result['gear_type']?result['gear_type']:'자동';
			fuel_option = result['fuel_option'];
			smoking = result['smoking']=="Y"?"흡연":"금연";
			car_option = '';
			
			car_option += car_people?"<span class=\"label label-default\">" + car_people + "인승</span>\n":'';
			car_option += fuel_option?"<span class=\"label label-default\">" + fuel_option + "</span>\n":'';
			car_option += gear_type?"<span class=\"label label-default\">" + gear_type + "</span>\n":'';
			car_option += smoking?"<span class=\"label label-default\">" + smoking + "</span>\n":'';


			if(result['option_navi'] =="Y"){
				car_option += "<span class='label label-default'>네비게이션</span>  ";
			}
			if(result['option_bluetooth'] =="Y"){
				car_option += "<span class='label label-default'>블루투스</span>  ";
			}
			if(result['option_automatic_back_mirror'] =="Y"){
				car_option += "<span class='label label-default'>전동백미러</span>  ";
			}
			if(result['option_hi_pass'] =="Y"){
				car_option += "<span class='label label-default'>하이패스</span>  ";
			}
			if(result['option_aux'] =="Y"){
				car_option += "<span class='label label-default'>AUX</span>  ";
			}
			if(result['option_heatseat'] =="Y"){
				car_option += "<span class='label label-default'>열선시트</span>  ";
			}
			if(result['option_rear_sensor'] =="Y"){
				car_option += "<span class='label label-default'>후방감지기</span>  ";
			}
			if(result['option_blackbox'] =="Y"){
				car_option += "<span class='label label-default'>블랙박스</span>  ";
			}
			if(result['option_cdplayer'] =="Y"){
				car_option += "<span class='label label-default'>CD플레이어</span>  ";
			}
			if(result['option_backcamera'] =="Y"){
				car_option += "<span class='label label-default'>후방카메라</span>  ";
			}
			if(result['option_sunroof'] =="Y"){
				car_option += "<span class='label label-default'>썬루프</span>  ";
			}
			if(result['option_lane_departure_warning'] =="Y"){
				car_option += "<span class='label label-default'>차선이탈</span>  ";
			}
			if(result['option_automatic_seat'] =="Y"){
				car_option += "<span class='label label-default'>전동시트</span>  ";
			}
			if(result['option_gps'] =="Y"){
				car_option += "<span class='label label-default'>GPS</span>  ";
			}
			if(result['option_front_sensor'] =="Y"){
				car_option += "<span class='label label-default'>전방감지기</span>  ";
			}
			if(result['option_smart_key'] =="Y"){
				car_option += "<span class='label label-default'>스마트키</span>  ";
			}
			if(result['option_av_system'] =="Y"){
				car_option += "<span class='label label-default'>AV시스템</span>  ";
			}
			if(result['option_ecm'] =="Y"){
				car_option += "<span class='label label-default'>ECM</span>  ";
			}




			$('#car_type').val(car_type);
			$('#car_name_detail').val(car_name_detail);
			$('#car_number').val(car_number);
			$('#car_option_div').html(car_option);

			$('#rental_plan').datepicker().data('datepicker').selectDate(new Date(result['period_start']));
			$('#return_plan').datepicker().data('datepicker').selectDate(new Date(result['period_finish']));
			if(result['period_start_order'] != '' && result['period_start_order'] != null){
				$('#rental_period').datepicker().data('datepicker').selectDate(new Date(result['period_start_order']));
			}else{
				$('#rental_period').datepicker().data('datepicker').clear();
				$('#rental_period').val('');
			}
			if(result['period_finish_order'] != '' && result['period_finish_order'] != null){
				$('#return_period').datepicker().data('datepicker').selectDate(new Date(result['period_finish_order']));
			}else{
				$('#return_period').datepicker().data('datepicker').clear();
				$('#return_period').val('');
			}

			refresh_total_price();
			return true;
		}
	});

	//수납내역 정보도 refresh
	refresh_receipt_grid(order_serial);
	$('#myModal2').modal();
}


function enableInsruanse(isInsurance){
	$("#input_damaged_cartype").prop("disabled", !isInsurance);
	$("#input_damaged_carnumber").prop("disabled", !isInsurance);
	$("#input_insurance_company").prop("disabled", !isInsurance);
	$("#input_insurance_number").prop("disabled", !isInsurance);
	$("#input_insurance_manager").prop("disabled", !isInsurance);
	$("#input_accident_car").prop("disabled", !isInsurance);
	$("#input_insurance_phone").prop("disabled", !isInsurance);
	$("#input_insurance_fax").prop("disabled", !isInsurance);
	$("#input_fix_company").prop("disabled", !isInsurance);
	$("#input_fix_phone").prop("disabled", !isInsurance);
	$("#input_falut_percnet").prop("disabled", !isInsurance);
	$("#input_insurance_date").prop("disabled", !isInsurance);
	if(isInsurance == false){
		$("#input_damaged_cartype").val('');
		$("#input_damaged_carnumber").val('');
		$("#input_insurance_company").val('');
		$("#input_insurance_number").val('');
		$("#input_insurance_manager").val('');
		$("#input_accident_car").val('');
		$("#input_insurance_phone").val('');
		$("#input_insurance_fax").val('');
		$("#input_fix_company").val('');
		$("#input_fix_phone").val('');
		$("#input_falut_percnet").val('');
		$("#input_insurance_date").val('');

	}
}

function execDaumPostcode() {
	new daum.Postcode({
		oncomplete: function(data) {

			var fullAddr = ''; 
			var extraAddr = ''; 

			if (data.userSelectedType === 'R') {
				fullAddr = data.roadAddress;
			} else { 
				fullAddr = data.jibunAddress;
			}

			if(data.userSelectedType === 'R'){
				if(data.bname !== ''){
					extraAddr += data.bname;
				}
				if(data.buildingName !== ''){
					extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
				}

				fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
			}

			document.getElementById('postcode').value = data.zonecode; 
			document.getElementById('address').value = fullAddr;

			document.getElementById('address').focus();
		}
	}).open();
}



//결제 금액 계산
function refresh_total_price(){
	// alert(rental_price+"/"+discount_price1+"/"+discount_price2+"/"+insurance_price+"/"+delivery_price+"/"+option_price+"/"+accident_price+"/"+overcharge_price+"/"+payback_price+"/"+etc_price);
	total_rental_price = rental_price - discount_price1 - discount_price2 + insurance_price + delivery_price + option_price + accident_price + overcharge_price - payback_price + etc_price;
	$("#total_price").val(total_rental_price);
	$("#input_total_price").val(total_rental_price);


	var inserted_price = 0;
	if($("#input_inserted_price").val() != ''){
		inserted_price = parseInt($("#input_inserted_price").val());
	}
	var remain = total_rental_price - inserted_price;
	$("#input_remain_price").val(remain);


}
$('#receipt_register').click(function() {
		//대여자명 등록
		is_receipt_register_mode = true;
		$('#receipt_username').val( $('#user_name').val() );
		//대여시작일 있으면 가져와서 넣기
		if($('#rental_plan').val()!=''){
			var selectedDate = $('#rental_plan').val().split(" ");
			temp_selected_date = new Date(selectedDate[0]);
			$('#receipt_date').datepicker().data('datepicker').selectDate(temp_selected_date);
		}

		//나머지는 초기화
		$("#select_receipt_type option:eq(0)").prop("selected", true);
		$("#receipt_input_price").val('');
		$("#receipt_memo").val('');
		$("#receipt_tax").val('');
		$("#select_staff option:eq(0)").prop("selected", true);

		// refresh_depositeway_grid();
		//회사마다 다른 결제방식을 가져온다.
		get_deposite_ways();
		
});

function get_deposite_ways(){
	company_serial = get_company_serial();
	$.post("/receiptmanager/get_deposite_ways",{
		company_serial: company_serial
	} , function(data){
			var result = JSON.parse(data);
			var str = '';
			for(i=0; i<result.length; i++){
				str += "<option>" + result[i].name + "</option>";
			}
			$("#receipt_pay_type").html(str);
		});
}

$("#button_deposite_way_save").click(function() {

	company_serial = get_company_serial();

	$.post("/receiptmanager/save_deposite_way",{
		company_serial: company_serial,
		name: $("#input_deposite_way").val(),
		memo: $("#input_deposite_memo").val()
	}, function(data){
		var result = JSON.parse(data);
		if(result['code'] == "S01"){
			alert("저장 되었습니다.");
			get_deposite_ways();
			$('#dialog_acceptance_system').modal('toggle');
			return true;
		} else{
			alert(result['message']);
			return false;
		} 
	});

});


$("#button_new_deposite_way").click(function() {
		 $("#input_deposite_way").val('');
		 $("#input_deposite_memo").val('');

		 refresh_depositeway_grid();
});

$("#btn_reservation_delete").click(function() {
	$.post("/rent/delete_order",{
		serial : order_serial
	}, function(data){
		var result = JSON.parse(data);
		if(result['code'] == "S01"){
			alert("삭제 되었습니다.");
			$('#myModal2').modal('toggle');
			var next_day = get_current_date('YYYYMMDD');
					refresh_schedule(next_day);
			return true;
		} else{
			alert(result['message']);
			return false;
		} 
	});
});

function delete_deposite_way(id){
	$.post("/receiptmanager/delete_deposite_way",{
		serial : id
	}, function(data){
		var result = JSON.parse(data);
		if(result['code'] == "S01"){
			alert("삭제 되었습니다.");
			$('#dialog_acceptance_system').modal('toggle');
			get_deposite_ways();
			return true;
		} else{
			alert(result['message']);
			return false;
		} 
	});

}




</script> 

