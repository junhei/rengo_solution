<link href="/telerik/examples/content/shared/styles/examples-offline.css" rel="stylesheet">
<link href="/telerik/styles/kendo.common-material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.rtl.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.material.mobile.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.min.css" rel="stylesheet">
<link href="/telerik/styles/kendo.dataviz.default.min.css" rel="stylesheet">


<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

<script src="/telerik/js/jszip.min.js"></script>
<script src="/telerik/js/kendo.all.min.js"></script>

<!-- 새로운 datepicker -->
<link href="/css/datepicker.min.css?<?=time();?>" rel="stylesheet" type="text/css">
<script src="/js/datepicker.js?<?=time();?>"></script>
<script src="/js/i18n_datepicker.en.js"></script>
<?/*<script src="/js/moment-with-locales.js"></script>*/?>

<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>

<?php
require("/home/apache/CodeIgniter-3.0.6/application/views/rengo_util.html");
?>

<script language="javascript">
	<!--
	function fixDataOnWheel(){
		if(event.wheelDelta < 0){
			DataScroll.doScroll('scrollbarDown');
		}else{
			DataScroll.doScroll('scrollbarUp');
		}
		dataOnScroll()
	}
	function dataOnScroll() {
		rent_left_contents.scrollTop = rent_right_contents.scrollTop;
		rent_right_header.scrollLeft = rent_right_contents.scrollLeft;
	}

	//--------------------------------------------------------------------------    변수 선언부
	var current_date = '';
	var schedule_mode = 'date';
	var order_kind = '';
	var scheduler_car_type = '';

	var selected_date = '';

	//요금 정보들
	var rental_price = 0; //대여금액
	var discount_price1 = 0; //할인금액
	var discount_price2 = 0; //기타 할인
	var insurance_price = 0;
	var delivery_price = 0;
	var option_price = 0;
	var accident_price = 0;
	var overcharge_price = 0; //초과/반납금액은 둘중 1개가 셋팅되면 나머지는 0임(둘이 동시에 값을 가질수 없음)
	var payback_price = 0;
	var etc_price = 0;
	var total_rental_price = 0; //총액, 총 결제금액

	var start = new Date(),
	prevDay,
	startHours = 9,
	finishHours = 20;

	var period_start_date = ''; //예약 대여 시작일
	var period_finish_date = ''; //예약 반납 종료일

	var real_start_date = ''; //실제 대여일
	var real_finish_date = ''; //실제 반납일

	var license1_expire_date = '';
	var license1_published_date = '';
	var license2_expire_date = '';
	var license2_published_date = '';

	var selected_user_tab = 1;



	var car_serial = '';
	var order_number = '';
	var company_serial = '<?=$company_serial;?>';
	var branch_serial = '<?=$branch_serial;?>';
	var period = '';
	var car_type = '';
	var car_flag = '';
	var car_insurance_self1_price = 0;
	var car_insurance_self2_price = 0;
	var car_insurance_self3_price = 0;

	var grid_car_type = '전체';
	var order_serial = 0;

	var is_receipt_register_mode = true;


	// 09:00 AM
	start.setHours(9);
	start.setMinutes(0);

	// If today is Saturday or Sunday set 10:00 AM
	if ([6, 0].indexOf(start.getDay()) != -1) {
		start.setHours(10);
		startHours = 10
	}
//-->
</script>



<style type="text/css">
  @media screen and (min-width: 1860px) {
    .schedule_data{width: 82%;}
    .rent_left_wrap{width: 18%;}
  }
	@media screen and (max-width: 1860px) {
		.schedule_data{width: 82%;}
		.rent_left_wrap{width: 18%;}
	}
	@media screen and (max-width: 1600px) {
		.schedule_data{width: 80%;}
		.rent_left_wrap{width: 20%;}
	}
	@media screen and (max-width: 1430px) {
		.schedule_data{width: 80%;}
		.rent_left_wrap{width: 20%;}
	}
	@media screen and (max-width: 1400px) {
		.schedule_data{width: 724px;}
		.rent_left_wrap{width: 315px;}
		.schedule{width: 1050px; display: block;}
		body{min-width:1393px !important;}
		#rent_management{width: 1112px;}
	}
	html{overflow:hidden;}

</style>



<script type="text/javascript">
	$(document).ready(function(){
		$('#rent_right_contents').css('height', $(window).height() - 430 );
		$('#rent_left_contents').css('height', $(window).height() - 430 );
		$('.modal_scroll').css('height', $(window).height() - 200 );  
		$(window).resize(function() {
			$('#rent_right_contents').css('height', $(window).height() - 430 );
			$('#rent_left_contents').css('height', $(window).height() - 430 );
		});


		$('.modal-lg').on('hidden.bs.modal',function() {
			$('.modal_scroll').scrollTop(0);
			setTimeout(function(){
				$('.modal').css({
					'top':'0',
					'left':'0'
				});

			},30)
		})
	}); 
</script>
<div id="rent_management" class="row" style="margin-right:0;">
	<div class="col-md-12">
		<div class="panel border_gray magb0">

			<div class="panel-heading pdt15">
				<div class="row">
					<div class="col-md-9">
						<h3 class="mgb10">예약관리</h3>
					</div>
					<div class="col-sm-3">
						<div class="col-md-12 pdl0 pdr0">
							<select class="form-control" id="company_select">
								<?php foreach($company_list as $company):?>
									<option value="<?php echo $company['serial']; ?>"> <?php echo $company['company_name']; ?></option>
								<?php endforeach;?>
							</select>
						</div>

					</div>
				</div>
			</div>

			<div class="panel-footer pdb0 pdt15">
				<div class="row">
					<div class="list_data_box">
						<div class="col-lg-1 col-md-2 col-xs-3">운행차량</div>
						<div class="col-lg-1 col-md-2 col-xs-3 text-right" id="count_registered_car_number"><?=$정상?>대</div>
						<div class="col-lg-1 col-md-2 col-xs-3">예약가능차량</div>
						<div class="col-lg-1 col-md-2 col-xs-3 text-right" id="count_available_rengo_car_number"><?=$예약가능차량?>대</div>
						<div class="col-lg-1 col-md-2 col-xs-3">예약불가차량</div>
						<div class="col-lg-1 col-md-2 col-xs-3 text-right" id="count_unavailable_rengo_car_number" ><?=$정상-$예약가능차량?>대</div>
					</div>
				</div>
			</div>


			<div class="panel-heading border_gray bg_fff pdt14 pdb13">
				<div class="row">
					<div class="col-md-1">
						<a class="btn action_btn" id="add_reservation_btn" style="width:100%;" data-toggle="modal" data-target="#myModal2" data-backdrop="static" data-keyboard="false">예약추가</a>
										<!--a class="btn btn-default" href="#" role="button">엑셀</a>
										<a class="btn btn-default" href="#" role="button">인쇄</a-->
										</div>
										<div class="col-md-6 pdl0">
											<div class="pull-left">
												<ul class="time_header pdl0" style="list-style: none;">
													<li id="btn_datetimepicker1_prev" >
														<a class="btn btn-default" href="javascript:void(0);" role="button"><i class="fa fa-angle-left fa-lg"></i></a>
													</li>
													<li >
														<input type='text' class="form-control btn btn-default" role="button" id='datetimepicker1' data-date-format="yyyy년 mm월 dd일" value="<?=$schedule_html['year'];?>년 <?=$schedule_html['month'];?>월 <?=$schedule_html['day'];?>일" />
													</li>
													<li id="btn_datetimepicker1_next" >
														<a class="btn btn-default" href="javascript:void(0);" role="button"><i class="fa fa-angle-right fa-lg"></i></a>
													</li>
<script language="javascript">
$('#datetimepicker1').datepicker({
	navTitles: {
		days: 'yyyy년 mm월'
	},
	//	todayButton: new Date(),
	clearButton: false,
	closeButton: true,
	language: 'en',
	onHide: function(dp, animationCompleted){
		next_day = get_current_date('YYYYMMDD');
		refresh_schedule(next_day);
	},

	onSelect: function (fd, d, picker) {
	// Do nothing if selection was cleared
	if (!d) return;
		// $("datetimepicker1").val( d.getFullYear()+"년 "+zeroPad(d.getMonth()+1,2)+"월 "+zeroPad(d.getDate(),2)+"일");
		selected_date = d;
	}
});
</script>
												</ul>
											</div>
											<div class="btn-group pull-left" role="group" aria-label="...">
												<button type="button" class="btn pdl35 pdr35 bg_main" id="btn_date">일별</button>
												<button type="button" class="btn pdl35 pdr35" id="btn_hour">시간별</button>
											</div>
										</div>

										<div class="col-md-5">
											<div class="rent_btn btn_box magt0 pull-right">
												<span class="bg_primary">예약</span>
												<span class="bg_success">운행</span>
												<span class="bg_info">반납</span>
												<span class="bg_중기">중기</span>
												<span class="bg_danger">장기</span>
												<span class="bg_warning">보험</span>
												<span class="bg_deep_purple">할인</span>
											</div>
										</div>
									</div>
								</div>


								<div class="panel-footer section_lnb bt0">
									<ul class="nav nav-tabs border_none">
										<li class="active"><a data-toggle="tab" href="#user_view01" id="scheduler_car_type_전체">전체</a></li>

										<li><a data-toggle="tab" href="#user_view05" id="scheduler_car_type_경형">경형</a></li>
										<li><a data-toggle="tab" href="#user_view06" id="scheduler_car_type_소형">소형</a></li>
										<li><a data-toggle="tab" href="#user_view07" id="scheduler_car_type_준중형">준중형</a></li>
										<li><a data-toggle="tab" href="#user_view08" id="scheduler_car_type_중형">중형</a></li>
										<li><a data-toggle="tab" href="#user_view09" id="scheduler_car_type_대형">대형</a></li>
										<li><a data-toggle="tab" href="#user_view10" id="scheduler_car_type_SUV">SUV</a></li>
										<li><a data-toggle="tab" href="#user_view11" id="scheduler_car_type_승합">승합</a></li>
										<li><a data-toggle="tab" href="#user_view12" id="scheduler_car_type_수입">수입</a></li>
										<li class="col-md-3 pull-right magt15">
											<div class="input-group custom-search-form">
												<input id="searchbox" type="text" class="form-control" placeholder="Search...">
												<span class="input-group-btn">
													<button class="btn btn-default" type="button" id="btn_search_start">
														<i class="fa fa-search"></i>
													</button>
												</span>
											</div>
										</li>
										<!-- <li> -->
										<!-- <div class="pull-right" data-toggle="modal" data-target="#dialog_by_day">추가</div> -->
										<!-- </li> -->
									</ul>
								</div>

							<!-- <div class="panel-footer section_lnb">
								<div class="col-md-9">
									<ul class="nav nav-tabs border_none">
					                    <li class="active"><a data-toggle="tab" href="#user_view01" id="order_kind_전체">전체</a></li>
					                    <li><a data-toggle="tab" href="#user_view02" id="order_kind_일반">일반</a></li>
					                    <li><a data-toggle="tab" href="#user_view03" id="order_kind_장기">장기</a></li>
					                    <li><a data-toggle="tab" href="#user_view04" id="order_kind_보험">보험</a></li>
					                </ul>
					                <div class="pull-right" data-toggle="modal" data-target="#dialog_by_day">추가</div>
								</div>
										
				                <div class="col-md-3 pdt15">
									<div class="input-group custom-search-form">
										<input type="text" class="form-control" id="search_keyword" placeholder="검색할 키워드를 입력해 주세요.">
										<span class="input-group-btn">
											<button class="btn btn-default" type="button" id="btn_search_start">
													<i class="fa fa-search"></i>
											</button>
										</span>
									</div>
								</div>
							</div> -->

							<div class="panel-footer bg_fff">
								<div class="row">
									<div class="col-md-12">
										<div class="schedule">
											<div class="row row_mag">



												<div class="col-md-3 pdr0 pdl0 bb_ccc rent_left_wrap">
													<div id="rent_left_header" class="col-md-12 pdr0 pdl0" >
														<div class="rent_left_header_content">
															<div class="schedule_car row bb_ccc">
																<!--div class="col-sm-2 text-center br_e6e6">렌고</div-->
																<div class="col-sm-7 text-center br_e6e6">차량모델</div>
																<div class="col-sm-5 text-center br_e6e6">차량번호</div>
															</div>

														</div>
													</div>


													<div id="rent_left_contents" onmousewheel="fixDataOnWheel()" onscroll="dataOnScroll()" style="float:left; overflow-y:hidden; overflow-x: hidden; height:500px; height:expression(100-17);">
														<div class="rent_left_contents_content bb_ccc br_e6e6">
															<div id="car_name_list">
																<?=$schedule_html['car_name_list'];?>
															</div>
														</div>
													</div>
												</div>
												<div onscroll="dataOnScroll()">
													<div class="col-md-9 pdl0 pdr0 schedule_data">
														<div id="rent_right_header" class="row" onscroll="dataOnScroll()" >
															<div class="rent_right_header_content">
																<div class="schedule_time_wrap time bb_ccc" id="class_day">
																	<div class="schedule_sale" id="special_price_list">
																		<?=$schedule_html['special_price_list'];?>
																	</div>

																	<div class="schedule_time">
																		<ul id="this_month_day_name">
																			<?=$schedule_html['this_month_day_name'];?>
																		</ul>
																		<ul id="this_month_day_number">
																			<?=$schedule_html['this_month_day_number'];?>
																		</ul>
																		<!--할인율-->
																		<ul id="this_month_day_cell">
																			<?=$schedule_html['this_month_day_cell'];?>
																		</ul>
																	</div>
																</div>
															</div>
														</div>
														<div id="rent_right_contents" class="bb_ccc" onscroll="dataOnScroll()">
															<div class="rent_right_contents_content bb_ccc">
																<div class="schedule_list" id="order_list_detail">
																	<?=$schedule_html['order_list_detail'];?>
																</div>
																<div id="this_month_day_cell_car">
																	<div class="row">
																		<div class="schedule_time_wrap day" id="class_day2">
																			<div class="schedule_time">
																				<?=$schedule_html['this_month_day_cell_car'];?>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- </div> -->
<script type="text/javascript">



show_company_select();
			

$('#searchbox').keypress(function (e) {
				var key = e.which;
				// alert(key);
				if(key == 13)  // the enter key code
				{
					if($('#searchbox').val().length < '1'){
//						alert('검색할 키워드를 입력해 주세요.');
//						$('#search_keyword').focus();
//						return false;
}
					// alert($('#searchbox').val());
					next_day = get_current_date('YYYYMMDD');
					refresh_schedule(next_day);
				}
});

function reload_js(src) {
//				$('script[src="' + src + '"]').remove();
//				$('<script>').attr('src', src).appendTo('head');
}

function get_current_date(format){
				// temp_current_date = $('#datetimepicker1').data("DateTimePicker").date();
				if(selected_date == ''){
					selected_date = new Date();
				}
				temp_current_date = selected_date;
				if(selected_date != null){
					if(format != '' && format != undefined){
						// current_date = temp_current_date.format(format);
						current_date = moment(temp_current_date).format('YYYYMMDD');
					} else {
						current_date = temp_current_date;
					}
					return current_date;
				} else {
					alert('날짜를 선택해 주십시요.');
					return false;
				}
}

function refresh_schedule(new_date2){
				company_serial = get_company_serial();
				branch_serial = 0;
				$.post('/rent/get_shcedule_html',{
					company_serial:company_serial,
					branch_serial:'',
					new_date:new_date2,
					search_keyword:$('#searchbox').val(),
					schedule_mode:schedule_mode,
					order_kind:order_kind,
					scheduler_car_type:scheduler_car_type
				}, function(data){
					var result = JSON.parse(data);
					$('#count_registered_car_number').text(result['count_registered_car_number'] + '대');
					$('#count_available_rengo_car_number').text(result['count_rengo_car_number'] + '대');
					$('#count_unavailable_rengo_car_number').text(result['count_registered_car_number'] - result['count_rengo_car_number'] + '대');
					$('#this_month_day_name').html(result['this_month_day_name']);
					$('#this_month_day_number').html(result['this_month_day_number']);
					$('#this_month_day_cell').html(result['this_month_day_cell']);
					$('#this_month_day_cell_car').html(result['this_month_day_cell_car']);
					$('#special_price_list').html(result['special_price_list']);
					$('#car_name_list').html(result['car_name_list']);
					$('#order_list_detail').html(result['order_list_detail']);
				});
}

function change_car_flag(car_serial, car_flag){

				var company_serial = get_company_serial();
/*
				if($("input:checkbox[name='" + name + "']").is(":checked")){
					car_flag = "Y";
				} else {
					car_flag = "N";
				}
*/
				$.post("/rent/change_car_flag",{
					company_serial:company_serial,
					branch_serial:0,
					car_serial:car_serial,
					car_flag:car_flag,
				}, function(data){
					var result = JSON.parse(data);
					next_day = get_current_date('YYYYMMDD');
					refresh_schedule(next_day);
					if(result['code'] == "E01"){
						alert(result['message']);
						return false;
					} else {
						$('#count_rengo_car_number').text(result['message'] + '대');
						return true;
					}
				});
}

$("#btn_datetimepicker1_next").click(function() {
			next_day = get_current_date();
			next_day.setDate(next_day.getDate()+1);

			$('#datetimepicker1').datepicker().data('datepicker').selectDate(next_day);


			next_day = get_current_date('YYYYMMDD');
			refresh_schedule(next_day);
});
			
$("#btn_datetimepicker1_prev").click(function() {
			next_day = get_current_date();
			next_day.setDate(next_day.getDate()-1);
				//성환
			$('#datetimepicker1').datepicker().data('datepicker').selectDate(next_day);
				// $('#datetimepicker1').data("DateTimePicker").date(next_day);
//				$('#datetimepicker1').datetimepicker('update');

			next_day = get_current_date('YYYYMMDD');
			refresh_schedule(next_day);

});

$("#btn_search_start").click(function() {
	if($('#searchbox').val().length < '1'){
//					alert('검색할 키워드를 입력해 주세요.');
//					$('#search_keyword').focus();
//					return false;
	}

	next_day = get_current_date('YYYYMMDD');
	refresh_schedule(next_day);

});

$("#datetimepicker1").on("dp.show", function(e) {
				current_date = $('#datetimepicker1').val();
});
$("#datetimepicker1").on("dp.hide", function(e) {
				if(current_date != $('#datetimepicker1').val()){

					next_day = get_current_date('YYYYMMDD');
					refresh_schedule(next_day);
				}
});


$("#company_select").change(function() {

				next_day = get_current_date('YYYYMMDD');
				refresh_schedule(next_day);
});

			// $("#sel_branch_serial").change(function() {

			// 	next_day = get_current_date('YYYYMMDD');
			// 	branch_serial = $('#sel_branch_serial').val();
			// 	refresh_schedule(next_day);
			// });

$("#order_kind_일반").click(function() {
				next_day = get_current_date('YYYYMMDD');
				order_kind = "일반";
				refresh_schedule(next_day);
});

$("#order_kind_장기").click(function() {
				next_day = get_current_date('YYYYMMDD');
				order_kind = "장기";
				refresh_schedule(next_day);
});

$("#order_kind_보험").click(function() {
				next_day = get_current_date('YYYYMMDD');
				order_kind = "보험";
				refresh_schedule(next_day);
});

$("#order_kind_전체").click(function() {
				next_day = get_current_date('YYYYMMDD');
				order_kind = "";
				refresh_schedule(next_day);
});


$("#scheduler_car_type_전체").click(function() {
				next_day = get_current_date('YYYYMMDD');
				order_kind = "";
				scheduler_car_type = "";
				refresh_schedule(next_day);
});

$("#scheduler_car_type_경형").click(function() {
				next_day = get_current_date('YYYYMMDD');
				order_kind = "";
				scheduler_car_type = "경형";
				refresh_schedule(next_day);
});

$("#scheduler_car_type_소형").click(function() {
				next_day = get_current_date('YYYYMMDD');
				order_kind = "";
				scheduler_car_type = "소형";
				refresh_schedule(next_day);
});

$("#scheduler_car_type_준중형").click(function() {
				next_day = get_current_date('YYYYMMDD');
				order_kind = "";
				scheduler_car_type = "준중형";
				refresh_schedule(next_day);
});

$("#scheduler_car_type_중형").click(function() {
				next_day = get_current_date('YYYYMMDD');
				order_kind = "";
				scheduler_car_type = "중형";
				refresh_schedule(next_day);
});

$("#scheduler_car_type_대형").click(function() {
				next_day = get_current_date('YYYYMMDD');
				order_kind = "";
				scheduler_car_type = "대형";
				refresh_schedule(next_day);
});

$("#scheduler_car_type_SUV").click(function() {
				next_day = get_current_date('YYYYMMDD');
				order_kind = "";
				scheduler_car_type = "SUV";
				refresh_schedule(next_day);
});

$("#scheduler_car_type_승합").click(function() {
				next_day = get_current_date('YYYYMMDD');
				order_kind = "";
				scheduler_car_type = "승합";
				refresh_schedule(next_day);
});

$("#scheduler_car_type_수입").click(function() {
				next_day = get_current_date('YYYYMMDD');
				order_kind = "";
				scheduler_car_type = "수입";
				refresh_schedule(next_day);
});


// $("#add_myModal2").on("show.bs.modal", function () {
// 				if(company_serial < '1'){
// 					alert('파트너를 확인해 주십시요.');
// 					return false;
// 				}
// 				if(car_serial >= '1'){
// 					$.post("/rent/change_car_flag",{
// 						company_serial:$('#sel_company_serial').val(),
// 						branch_serial:$('#sel_branch_serial').val(),
// 						car_serial:car_serial,
// 					}, function(data){
// 						var result = JSON.parse(data);
// 						if(result['code'] == "E01"){
// 							alert(result['message']);
// 							return false;
// 						} else if(result['code'] == "S01"){
// 							car_flag = result['message'];
// 							return true;
// 						}
// 					});

// 				} else {
// 					car_flag = '';
// 				}
// });

// $("#add_myModal2").on("hidden.bs.modal", function () {
// 				if(car_flag == 'Y'){
// 					$.post("/rent/change_car_flag",{
// 						company_serial:$('#sel_company_serial').val(),
// 						branch_serial:$('#sel_branch_serial').val(),
// 						car_serial:car_serial,
// 						car_flag:car_flag,
// 					}, function(data){
// 						var result = JSON.parse(data);
// 						if(result['code'] == "E01"){
// 							alert(result['message']);
// 							return false;
// 						} else {
// 							$('#count_rengo_car_number').text(result['message'] + '대');
// 							next_day = get_current_date('YYYYMMDD');
// 							refresh_schedule(next_day);
// 							car_flag = '';
// 							return true;
// 						}
// 					});

// 				}

// 				// put your default event here
// });

$("#btn_date").click(function() {
				$('#btn_hour').removeClass('bg_main');
				$('#btn_date').addClass('bg_main');
				$('#btn_date').blur();


				next_day = get_current_date('YYYYMMDD');
				schedule_mode = 'date';
				refresh_schedule(next_day);
});

$("#btn_hour").click(function() {
				$('#btn_date').removeClass('bg_main');
				$('#btn_hour').addClass('bg_main');
				$('#btn_hour').blur();


				next_day = get_current_date('YYYYMMDD');
				schedule_mode = 'hour';
				refresh_schedule(next_day);
});


$('#myModal2, #rent_car_search_dialog, #myModal3').draggable({
				handle: ".modal-header"
});

function close_day(date_Ymd){
//	alert(date_Ymd + ' ' + company_serial + ' ' + branch_serial);
	$.post('/rent/set_holiday',{company_serial:company_serial, holiday:date_Ymd}, function(data){
		var result = JSON.parse(data);
//		alert(result['message']);
		next_day = get_current_date('YYYYMMDD');
		refresh_schedule(next_day);
	});
}

function close_day_car(date_Ymd, car_serial){
	if(car_serial == '')return false;
//	alert(date_Ymd + ' ' + company_serial + ' ' +car_serial);
	$.post('/rent/set_holiday_car',{company_serial:company_serial, holiday:date_Ymd, car_serial:car_serial}, function(data){
		var result = JSON.parse(data);
//		alert(result['message']);
		next_day = get_current_date('YYYYMMDD');
		refresh_schedule(next_day);
	});
}


// function modalHeight() {
	// $('.modal_scroll').css('height', $(window).height() - 200 );  
// }
//성환추가



//초기화
$("#add_reservation_btn").click(function() {


	enableInsruanse(false);
	var date = new Date();
	date.setMinutes(0);
	order_serial = 0;
	user_serial = 0;
	car_serial = 0;
	period_start_date = '';
	period_finish_date = '';
	real_start_date = ''; //실제 대여일
	real_finish_date = ''; //실제 반납일


	total_rental_price = 0;
	rental_price =0;
	discount_price1 =0;
	discount_price2 =0;
	insurance_price =0;
	delivery_price =0;
	option_price =0;
	accident_price =0;
	overcharge_price =0;
	payback_price =0;
	etc_price = 0;

	//대여정보 초기화
	$("#status").val("예약");
	$("#order_kind option:eq(0)").prop("selected", true);
	$("#flag option:eq(0)").prop("selected", true);
	$('#rental_plan').datepicker().data('datepicker').clear();
	$('#rental_plan').val('');
	$('#return_plan').datepicker().data('datepicker').clear();
	$('#return_plan').val('');
	$('#delivery_place').val('');
	$('#pickup_place').val('');
	$('#car_type').val('');
	$('#car_name_detail').val('');
	$('#car_number').val('');
	$('#car_option_div').html('');
	//고객 정보
	$('#tab_user1').trigger('click');
	$('#user_email').val('');
	$('#user_name').val('');
	$('#birthday').val('');
	$('#gender').val('');
	$('#phone_number1').val('');
	$('#phone_number2').val('');
	$("#license_type option:eq(0)").prop("selected", true);
	$('#license_number').val('');
	$('#expiration_date').datepicker().data('datepicker').clear();
	$('#expiration_date').val('');
	$('#published_date').datepicker().data('datepicker').clear();
	$('#published_date').val('');
	$('#expiration_date').val('');
	$('#published_date').val('');
	$('#postcode').val('');
	$('#address').val('');

	$('#second_user_email').val('');
	$('#second_user_name').val('');
	$('#second_birthday').val('');
	$('#second_gender').val('');
	$('#second_phone_number1').val('');
	$('#second_phone_number2').val('');
	$("#second_license_type option:eq(0)").prop("selected", true);
	$('#second_license_number').val('');
	$('#second_expiration_date').datepicker().data('datepicker').clear();
	$('#second_expiration_date').val('');
	$('#second_published_date').datepicker().data('datepicker').clear();
	$('#second_published_date').val('');
	$('#second_expiration_date').val('');
	$('#second_published_date').val('');
	$('#second_postcode').val('');
	$('#second_address').val('');
	//보험대차 정보 초기화
	$('#input_damaged_cartype').val('');
	$('#input_damaged_carnumber').val('');
	$('#input_insurance_company').val('');
	$('#input_insurance_number').val('');
	$('#input_insurance_manager').val('');
	$('#input_accident_car').val('');
	$('#input_insurance_phone').val('');
	$('#input_insurance_fax').val('');
	$('#input_fix_company').val('');
	$('#input_fix_phone').val('');
	$('#input_falut_percnet').val('');
	$('#input_insurance_date').val('');
	//요금정보
	$('#contract_period').val('');
	$('#rental_price').val('');
	$("#sel_period_price_serial option:eq(0)").prop("selected", true);
	$('#price_off').val('');
	$("#sel_special_price_serial option:eq(0)").prop("selected", true);
	$('#price_off2').val('');
	$("#sel_insurance_name option:eq(0)").prop("selected", true);
	$('#insurance_price').val('');
	$("#sel_delivery option:eq(0)").prop("selected", true);
	$('#input_delivery').val('');
	$("#sel_option option:eq(0)").prop("selected", true);
	$('#input_option').val('');
	$('#input_option').attr("readonly",false);
	$("#sel_re_or_acc option:eq(0)").prop("selected", true);
	$('#input_re_or_acc').val('');
	$('#input_re_or_acc').attr("readonly",false);
	$("#sel_overcharge option:eq(0)").prop("selected", true);
	$('#input_overcharge').val('');
	$('#input_overcharge').attr("readonly",false);
	$("#sel_etc option:eq(0)").prop("selected", true);
	$('#input_etc').val('');
	$('#input_etc').attr("readonly",false);
	$('#total_price').val('');
	$("#sel_insurance_name").html("<option>자차1</option><option>자차2</option><option>자차3</option>");


	//대여정보 초기화
	// $('#rental_period').datepicker().data('datepicker').removeDate(new Date());
	// $('#return_period').datepicker().data('datepicker').removeDate(new Date());
	$('#rental_period').datepicker().data('datepicker').clear();
	$('#rental_period').val('');
	$('#return_period').datepicker().data('datepicker').clear();
	$('#return_period').val('');
	$('#rental_staff_serial option:eq(0)').prop("selected", true);
	$('#return_staff_serial option:eq(0)').prop("selected", true);
	$('#delivery_place_order').val('');
	$('#pickup_place_order').val('');
	$('#start_fuel').val('');
	$('#end_fuel').val('');
	$('#start_km').val('');
	$('#end_km').val('');
	$('#drive_km').val('');
	$('#memo_text').val('');



	//마지막 요금 초기화
	$('#input_total_price').val('');
	$('#input_inserted_price').val('');
	$('#input_remain_price').val('');

	refresh_receipt_grid(0);
});

</script>  

<?php

require("/home/apache/CodeIgniter-3.0.6/application/views/rent/dialog_rent_info.php");
require("/home/apache/CodeIgniter-3.0.6/application/views/rent/dialog_grid.php");
?>

<script type="text/javascript">
	
	//차량 검색 버튼 클릭시 날짜가 먼저 선택되어 있어야 함(왜냐하면 해당 기간에 빌릴수 있는 차를 구해야 해서)
$("#car_search_btn").click(function() {
	
	if(period_start_date == '' || period_start_date == null){
		alert('대여예정 날짜를 선택해 주십시요.');
		return false;
	}

	if(period_finish_date == '' || period_finish_date == null){
		alert('반납예정 날짜를 선택해 주십시요.');
		return false;
	}

	if(period_start_date >= period_finish_date){
		alert('대여기간을 확인해 주십시요.');
		return false;
	}
	$('#rent_car_search_dialog').modal('toggle');
	refresh_grid(grid_car_type);
});
//제 1유저 찾기 버튼 이벤트
$('#user_search_btn').click(function(){
	$('#customer_search_dialog').modal('toggle');
	var dataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "/rent/get_company_all_user/" + company_serial,
				dataType: "json"
			}
		},
		schema: {
			model: {
				fields: {
					user_serial: { type: "number" },
					user_email: {type:"string"},
					user_name: { type: "string" }, 
					birthday: { type: "srting" }, 
					address: { type: "string" }, 
					phone_number1: { type: "string" },
					phone_number2: { type: "string" },
					company: { type: "string" }, 
					auth_code: { type: "number" }, 
					note: {type:"string"},
					license_type: { type: "string" }, 
					published_date: { type: "date" }, 
					license_number: { type: "string" }, 
					expiration_date: { type: "date" }, 
				}
			}
		},
		pageSize: 10
	});
	var grid = $("#search_grid").data("kendoGrid");
	grid.setDataSource(dataSource);
	grid.refresh();
	//이름 입력 후 검색하면 일치하는 항목 보여줌(필터링 적용)
	var searchUserName = $("#user_name").val();
	if(searchUserName!= ''){
			$('#input_user_search').val(searchUserName);
			$("#search_grid").data("kendoGrid").dataSource.filter({
				logic: "or",
				filters: [
				{
					field: "user_name",
					operator: "contains",
					value:searchUserName
				},
				{
					field: "phone_number1",
					operator: "contains",
					value:searchUserName
				},
				{
					field: "birthday",
					operator: "contains",
					value:searchUserName
				},
				{
					field: "user_email",
					operator: "contains",
					value:searchUserName
				},					 
				]
			});
	}else{
			$('#input_user_search').val('');
	}
})

//제 2유저 찾기 버튼 이벤트
$('#second_user_search_btn').click(function(){
	$('#customer_search_dialog').modal('toggle');
	var dataSource = new kendo.data.DataSource({
		transport: {
			read: {
				url: "/rent/get_company_all_user/" + company_serial,
				dataType: "json"
			}
		},
		schema: {
			model: {
				fields: {
					user_serial: { type: "number" },
					user_email: {type:"string"},
					user_name: { type: "string" }, 
					birthday: { type: "srting" }, 
					address: { type: "string" }, 
					phone_number1: { type: "string" },
					phone_number2: { type: "string" },
					company: { type: "string" }, 
					auth_code: { type: "number" }, 
					note: {type:"string"},
					license_type: { type: "string" }, 
					published_date: { type: "date" }, 
					license_number: { type: "string" }, 
					expiration_date: { type: "date" }, 
				}
			}
		},
		pageSize: 10
	});
	var grid = $("#search_grid").data("kendoGrid");
	grid.setDataSource(dataSource);
	grid.refresh();
	//이름 입력 후 검색하면 일치하는 항목 보여줌(필터링 적용)
	var searchUserName = $("#second_user_name").val();
	if(searchUserName!= ''){
			$('#input_user_search').val(searchUserName);
			$("#search_grid").data("kendoGrid").dataSource.filter({
				logic: "or",
				filters: [
				{
					field: "user_name",
					operator: "contains",
					value:searchUserName
				},
				{
					field: "phone_number1",
					operator: "contains",
					value:searchUserName
				},
				{
					field: "birthday",
					operator: "contains",
					value:searchUserName
				},
				{
					field: "user_email",
					operator: "contains",
					value:searchUserName
				},					 
				]
			});
	}else{
			$('#input_user_search').val('');
	}
})

$("#acceptance_grid").delegate("tbody>tr", "dblclick",
	function(){
			var grid = $("#acceptance_grid").data("kendoGrid");
			var row = grid.select();
			var data = grid.dataItem(row);
			if(data==null){
				alert("수정할 항목를 선택해 주세요.");
				return false;
			}else{
				if(data.handler == "렌고예약"){
					alert("렌고예약은 수정할 수 없습니다.");
					return false;
				}
				
				is_receipt_register_mode = false;
				$('#myModal3').modal('toggle');

				$('#receipt_username').val(data.username);
				temp_deposite_date = new Date(data.deposite_date);
				$('#receipt_date').datepicker().data('datepicker').selectDate(temp_deposite_date);
				// $('#receipt_date').data("DateTimePicker").date(temp_deposite_date);
				$("#select_receipt_type").val(data.type);
				$("#receipt_input_price").val(data.deposite_amount);
				$("#receipt_memo").val(data.memo);
				$("#receipt_tax").val(data.tax_paper);
				$("#select_staff option:contains('" + data.handler +"')").prop("selected", true);
				// $('#receipt_date').val()
				company_serial = get_company_serial();
				//회사마다 다른 결제방식을 가져온 다음 선택하게 한다.
				get_deposite_ways();

			}

	});
</script>