

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
<meta name="format-detection" content="telephone=no">
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/solution_page.css" />
<title>렌고 파트너</title>

</head>

<body>
<style>
</style>
	<div class="rent">
		<div class="bar_date">
			<div class="control_date">
			<span class="tit_date">08월 26일(금)</span>
			<a href="javascript:;" class="prev" id="bt_prev_date">이전</a>
			<a href="javascript:;" class="next" id="bt_next_date">다음</a>
			</div>
			<a href="rent.php" class="btn_left">오늘</a>
			<a href="javascript:;" class="btn_right" id="bt_add_order">예약추가</a>
		</div>


		<!--left content st-->
		<div id="left_content">
			<div class="list_date_txt">
				<strong>날짜</strong>
			</div>
			<ul class="list_schedule_car">
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_4" onchange="javascript:change_car_flag('item_4', '4');">
						<label for="item_4">
							<strong class="txt_ellip">K5</strong>
							<span>(02하2407)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_19" onchange="javascript:change_car_flag('item_19', '19');">
						<label for="item_19">
							<strong class="txt_ellip">그랜드 스타렉스</strong>
							<span>(71호1355)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_239" onchange="javascript:change_car_flag('item_239', '239');">
						<label for="item_239">
							<strong class="txt_ellip">그랜드 스타렉스</strong>
							<span>(71하1303)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_7" onchange="javascript:change_car_flag('item_7', '7');">
						<label for="item_7">
							<strong class="txt_ellip">그랜드 스타렉스</strong>
							<span>(71하1302)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_10" onchange="javascript:change_car_flag('item_10', '10');">
						<label for="item_10">
							<strong class="txt_ellip">레이</strong>
							<span>(02하2404)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_11" onchange="javascript:change_car_flag('item_11', '11');">
						<label for="item_11">
							<strong class="txt_ellip">레이</strong>
							<span>(02하2405)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_12" onchange="javascript:change_car_flag('item_12', '12');">
						<label for="item_12">
							<strong class="txt_ellip">레이</strong>
							<span>(02하2409)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_13" onchange="javascript:change_car_flag('item_13', '13');">
						<label for="item_13">
							<strong class="txt_ellip">레이</strong>
							<span>(02하2410)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_9" onchange="javascript:change_car_flag('item_9', '9');">
						<label for="item_9">
							<strong class="txt_ellip">아반떼 MD</strong>
							<span>(02하2402)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_5" onchange="javascript:change_car_flag('item_5', '5');">
						<label for="item_5">
							<strong class="txt_ellip">아반떼 MD</strong>
							<span>(02하2401)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_14" onchange="javascript:change_car_flag('item_14', '14');">
						<label for="item_14">
							<strong class="txt_ellip">아반떼 MD</strong>
							<span>(02하2411)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_15" onchange="javascript:change_car_flag('item_15', '15');">
						<label for="item_15">
							<strong class="txt_ellip">아반떼 MD</strong>
							<span>(02하2412)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_16" onchange="javascript:change_car_flag('item_16', '16');">
						<label for="item_16">
							<strong class="txt_ellip">아반떼 MD</strong>
							<span>(02하2415)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_17" onchange="javascript:change_car_flag('item_17', '17');">
						<label for="item_17">
							<strong class="txt_ellip">아반떼 MD</strong>
							<span>(02하2416)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_8" onchange="javascript:change_car_flag('item_8', '8');">
						<label for="item_8">
							<strong class="txt_ellip">올 뉴 카니발</strong>
							<span>(02하2413)</span>
						</label>
					</div>
				</li>
				<li>
					<div class="tit_item">
						<input type="checkbox" id="item_18" onchange="javascript:change_car_flag('item_18', '18');">
						<label for="item_18">
							<strong class="txt_ellip">올 뉴 카니발</strong>
							<span>(02하2417)</span>
						</label>
					</div>
				</li>
			</ul>
		</div>
		<!--left content end-->


		<!--right content st-->
		<div id="right_content">
			<div class="content_data">
				<div class="list_date">
					<ul>
						<li>08/26(금)</li>
						<li>08/27(토)</li>
						<li>08/28(일)</li>
						<li>08/29(월)</li>
						<li>08/30(화)</li>
						<li>08/31(수)</li>
						<li>09/01(목)</li>
					</ul>
				</div>
				<ul class="list_schedule">

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '4');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '4');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '4');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '4');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '4');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '4');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '4');">
									<a href="javascript:;"></a>
								</li>
							</ul>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '19');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '19');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '19');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '19');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '19');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '19');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '19');">
									<a href="javascript:;"></a>
								</li>
							</ul>
				<a href="javascript:void();" class="inner" style="left:29.616923076923%; width:12.066153846154%; max-width:70.383076923077%" onclick="javascript:view_detail('4253');"> <!-- [24시간:33.333%, 1시간:1.388%] left:대여시각 * 1.388%, width:대여시간 * 1.388%, max-width:100%-left% -->
				<div style="text-align:left;font-size:10pt;" id="4253">
					<span>08:00 (직접방문) ~ 19:00 (직접반납)</span><br>
					<span></span>
					 					</div>
				</a>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '239');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '239');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '239');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '239');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '239');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '239');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '239');">
									<a href="javascript:;"></a>
								</li>
							</ul>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '7');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '7');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '7');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '7');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '7');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '7');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '7');">
									<a href="javascript:;"></a>
								</li>
							</ul>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '10');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '10');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '10');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '10');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '10');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '10');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '10');">
									<a href="javascript:;"></a>
								</li>
							</ul>
				<a href="javascript:void();" class="inner" style="left:0%; width:9.8723076923077%; max-width:100%" onclick="javascript:view_detail('4268');"> <!-- [24시간:33.333%, 1시간:1.388%] left:대여시각 * 1.388%, width:대여시간 * 1.388%, max-width:100%-left% -->
				<div style="text-align:left;font-size:10pt;" id="4268">
					<span>10:00 (부산/개금) ~ 16:00 (부산/개금)</span><br>
					<span>노용주</span>
					<span>(01028832889)</span> 					</div>
				</a>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '11');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '11');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '11');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '11');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '11');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '11');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '11');">
									<a href="javascript:;"></a>
								</li>
							</ul>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '12');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '12');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '12');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '12');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '12');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '12');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '12');">
									<a href="javascript:;"></a>
								</li>
							</ul>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '13');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '13');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '13');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '13');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '13');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '13');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '13');">
									<a href="javascript:;"></a>
								</li>
							</ul>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '9');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '9');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '9');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '9');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '9');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '9');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '9');">
									<a href="javascript:;"></a>
								</li>
							</ul>
				<a href="javascript:void();" class="inner" style="left:31.262307692308%; width:14.26%; max-width:68.737692307692%" onclick="javascript:view_detail('4323');"> <!-- [24시간:33.333%, 1시간:1.388%] left:대여시각 * 1.388%, width:대여시간 * 1.388%, max-width:100%-left% -->
				<div style="text-align:left;font-size:10pt;" id="4323">
					<span>09:30 (부산/신평) ~ 09:30 (부산/신평)</span><br>
					<span>박재윤</span>
					<span>(01083958796)</span> 					</div>
				</a>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '5');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '5');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '5');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '5');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '5');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '5');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '5');">
									<a href="javascript:;"></a>
								</li>
							</ul>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '14');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '14');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '14');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '14');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '14');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '14');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '14');">
									<a href="javascript:;"></a>
								</li>
							</ul>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '15');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '15');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '15');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '15');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '15');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '15');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '15');">
									<a href="javascript:;"></a>
								</li>
							</ul>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '16');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '16');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '16');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '16');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '16');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '16');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '16');">
									<a href="javascript:;"></a>
								</li>
							</ul>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '17');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '17');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '17');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '17');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '17');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '17');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '17');">
									<a href="javascript:;"></a>
								</li>
							</ul>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '8');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '8');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '8');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '8');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '8');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '8');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '8');">
									<a href="javascript:;"></a>
								</li>
							</ul>
						</div>
					</li>

					<li>
						<div class="cont_item">
							<ul class="item_blank">
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160826', '0800', '20160826', '1900', '18');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160827', '0800', '20160827', '1900', '18');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160828', '0800', '20160828', '1900', '18');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160829', '0800', '20160829', '1900', '18');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160830', '0800', '20160830', '1900', '18');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160831', '0800', '20160831', '1900', '18');">
									<a href="javascript:;"></a>
								</li>
								<li class="car_schedule_r" onclick="javascript:add_order_from_tab('20160901', '0800', '20160901', '1900', '18');">
									<a href="javascript:;"></a>
								</li>
							</ul>
				<a href="javascript:void();" class="inner" style="left:29.616923076923%; width:12.066153846154%; max-width:70.383076923077%" onclick="javascript:view_detail('4255');"> <!-- [24시간:33.333%, 1시간:1.388%] left:대여시각 * 1.388%, width:대여시간 * 1.388%, max-width:100%-left% -->
				<div style="text-align:left;font-size:10pt;" id="4255">
					<span>08:00 (직접방문) ~ 19:00 (직접반납)</span><br>
					<span></span>
					 					</div>
				</a>
				<a href="javascript:void();" class="inner" style="left:8.7753846153846%; width:14.26%; max-width:91.224615384615%" onclick="javascript:view_detail('4296');"> <!-- [24시간:33.333%, 1시간:1.388%] left:대여시각 * 1.388%, width:대여시간 * 1.388%, max-width:100%-left% -->
				<div style="text-align:left;font-size:10pt;" id="4296">
					<span>15:00 (부산/구포) ~ 15:00 (부산/구포)</span><br>
					<span>이성환</span>
					<span>(01075989956)</span> 					</div>
				</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<!--right content end-->

	</div>

	<!-- 예약정보 팝업 -->

	<div class="info_pop dimmed" style="display:none;">
		<div class="wrap_info">
			<div class="head_info">
				<h2 id="order_title">예약내역 확인</h2>
				<a href="javascript:;" class="close_info" id="close_info">닫기</a>
			</div>
			<div class="cont_info" style="overflow-y:scroll;height:100%;">
				<div class="wrapper">













					<div id="dt_order_view">
					<!-- 회원(임차인) 정보 -->
						<h3>회원(임차인) 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm">
								<dt>예약번호</dt>
								<dd id="dt_order_number"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>예약자명</dt>
								<dd id="dt_username"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>생년월일</dt>
								<dd id="dt_birthday"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>연락처</dt>
								<dd id="dt_tel"></dd>
							</dl>
						</div>
					<!-- 회원(임차인) 정보 끝 -->

					<!-- 대여 정보 -->
						<h3>대여 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm">
								<dt>대여차종</dt>
								<dd id="dt_car_name_detail"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>대여일시</dt>
								<dd id="dt_period_start"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>대여지역</dt>
								<dd id="dt_delivery_place"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>반납일시</dt>
								<dd id="dt_period_finish"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>반납지역</dt>
								<dd id="dt_pickup_place"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>자차보험</dt>
								<dd id="dt_insurance_name"></dd>
							</dl>
						</div>
					<!-- 대여 정보 끝 -->

					<!-- 결제 정보 -->
						<h3>결제 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm">
								<dt>대여금액</dt>
								<dd id="dt_rental_price"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>보험금액</dt>
								<dd id="dt_insurance_price"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>할인금액</dt>
								<dd id="dt_coupon_price"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>결제총액</dt>
								<dd id="dt_total_price"></dd>
							</dl>
						</div>
					<!-- 대여 정보 끝 -->

					<!-- 메모 -->
						<h3>메모</h3>
						<div class="box_cont">
								<span class="cont_memo" id="dt_memo"></span><!-- 메모 내용 없을 시 '메모 없음' 표출 -->
						</div>
					<!-- 메모 끝 -->

					<!-- 대여지점 정보 -->
						<h3>대여지점 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm">
								<dt>대여지점</dt>
								<dd id="dt_company_name"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>지점주소</dt>
								<dd id="dt_office_address"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>전화번호</dt>
								<dd id="dt_office_phone"></dd>
							</dl>
						</div>
					<!-- 대여지점 정보 끝 -->

						<div class="bdn">
							<dl class="dl_comm dl_txt bdn">
								<dd class="bdn">
						 <button id="bt_order_edit" class="btn_regist" style="width:100%;">수정</button><!-- 수정 시 '확인'으로 변경 -->

</dd>
								<dd class="bdn" id="view_delete_button" style="display:none;">
<a href="javascript:;" class="btn_regist" id="button_delete_order" style="background-color:#DDD;">삭제</a>								</dd>
							</dl>

						</div>
					</div>

















					<div id="dt_order_edit" style="display:none;">
					<!-- 예약자 정보 수정 -->
						<h3>예약자 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm">
								<dt>예약번호</dt>
								<dd id="et_order_number"></dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>예약자명</dt>
								<dd class="wrap_ipt">
									<input type="text" class="ipt_comm" value="" id="et_username" placeholder="예)홍길동">
								</dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>생년월일</dt>
								<dd class="wrap_ipt">
									<input type="tel" class="ipt_comm" value="" id="et_birthday" placeholder="예)19880515">
								</dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>연락처</dt>
								<dd class="wrap_ipt">
									<input type="tel" class="ipt_comm" placeholder="예)01012345678" value="" id="et_tel">
								</dd>
							</dl>
						</div>
					<!-- 예약자 정보 끝 -->

					<!-- 대여 정보 - 수정	-->
						<h3>대여 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm dl_slt">
								<dt>대여차종</dt>
								<dd class="wrap_slt">
									<select class="slt_comm gradient" id="et_car_serial">
										<option value=""></option>
<option value="4">K5 02하2407</option>
<option value="19">그랜드 스타렉스 71호1355</option>
<option value="239">그랜드 스타렉스 71하1303</option>
<option value="7">그랜드 스타렉스 71하1302</option>
<option value="10">레이 02하2404</option>
<option value="11">레이 02하2405</option>
<option value="12">레이 02하2409</option>
<option value="13">레이 02하2410</option>
<option value="9">아반떼 MD 02하2402</option>
<option value="5">아반떼 MD 02하2401</option>
<option value="14">아반떼 MD 02하2411</option>
<option value="15">아반떼 MD 02하2412</option>
<option value="16">아반떼 MD 02하2415</option>
<option value="17">아반떼 MD 02하2416</option>
<option value="8">올 뉴 카니발 02하2413</option>
<option value="18">올 뉴 카니발 02하2417</option>
									</select>
								</dd>
							</dl>
							<dl class="dl_comm dl_slt">
								<dt>대여일시</dt>
								<dd class="wrap_slt">
								<select name="start_period_day" id="et_start_period_day" class="slt_comm gradient">
								<option value="20160726">07월 26일(화)</option>
								<option value="20160727">07월 27일(수)</option>
								<option value="20160728">07월 28일(목)</option>
								<option value="20160729">07월 29일(금)</option>
								<option value="20160730">07월 30일(토)</option>
								<option value="20160731">07월 31일(일)</option>
								<option value="20160801">08월 01일(월)</option>
								<option value="20160802">08월 02일(화)</option>
								<option value="20160803">08월 03일(수)</option>
								<option value="20160804">08월 04일(목)</option>
								<option value="20160805">08월 05일(금)</option>
								<option value="20160806">08월 06일(토)</option>
								<option value="20160807">08월 07일(일)</option>
								<option value="20160808">08월 08일(월)</option>
								<option value="20160809">08월 09일(화)</option>
								<option value="20160810">08월 10일(수)</option>
								<option value="20160811">08월 11일(목)</option>
								<option value="20160812">08월 12일(금)</option>
								<option value="20160813">08월 13일(토)</option>
								<option value="20160814">08월 14일(일)</option>
								<option value="20160815">08월 15일(월)</option>
								<option value="20160816">08월 16일(화)</option>
								<option value="20160817">08월 17일(수)</option>
								<option value="20160818">08월 18일(목)</option>
								<option value="20160819">08월 19일(금)</option>
								<option value="20160820">08월 20일(토)</option>
								<option value="20160821">08월 21일(일)</option>
								<option value="20160822">08월 22일(월)</option>
								<option value="20160823">08월 23일(화)</option>
								<option value="20160824">08월 24일(수)</option>
								<option value="20160825">08월 25일(목)</option>
								<option value="20160826">08월 26일(금)</option>
								<option value="20160827">08월 27일(토)</option>
								<option value="20160828">08월 28일(일)</option>
								<option value="20160829">08월 29일(월)</option>
								<option value="20160830">08월 30일(화)</option>
								<option value="20160831">08월 31일(수)</option>
								<option value="20160901">09월 01일(목)</option>
								<option value="20160902">09월 02일(금)</option>
								<option value="20160903">09월 03일(토)</option>
								<option value="20160904">09월 04일(일)</option>
								<option value="20160905">09월 05일(월)</option>
								<option value="20160906">09월 06일(화)</option>
								<option value="20160907">09월 07일(수)</option>
								<option value="20160908">09월 08일(목)</option>
								<option value="20160909">09월 09일(금)</option>
								<option value="20160910">09월 10일(토)</option>
								<option value="20160911">09월 11일(일)</option>
								<option value="20160912">09월 12일(월)</option>
								<option value="20160913">09월 13일(화)</option>
								<option value="20160914">09월 14일(수)</option>
								<option value="20160915">09월 15일(목)</option>
								<option value="20160916">09월 16일(금)</option>
								<option value="20160917">09월 17일(토)</option>
								<option value="20160918">09월 18일(일)</option>
								<option value="20160919">09월 19일(월)</option>
								<option value="20160920">09월 20일(화)</option>
								<option value="20160921">09월 21일(수)</option>
								<option value="20160922">09월 22일(목)</option>
								<option value="20160923">09월 23일(금)</option>
								<option value="20160924">09월 24일(토)</option>
								<option value="20160925">09월 25일(일)</option>
								<option value="20160926">09월 26일(월)</option>
								<option value="20160927">09월 27일(화)</option>
								<option value="20160928">09월 28일(수)</option>
								<option value="20160929">09월 29일(목)</option>
								<option value="20160930">09월 30일(금)</option>
								<option value="20161001">10월 01일(토)</option>
								<option value="20161002">10월 02일(일)</option>
								<option value="20161003">10월 03일(월)</option>
								<option value="20161004">10월 04일(화)</option>
								<option value="20161005">10월 05일(수)</option>
								</select></dd>
								<dt></dt>
								<br />
								<dd class="wrap_slt">
								<select name="start_period_hour" id="et_start_period_hour" class="slt_comm gradient">
								<option value="0000">00시 00분</option>
								<option value="0030">00시 30분</option>
								<option value="0100">01시 00분</option>
								<option value="0130">01시 30분</option>
								<option value="0200">02시 00분</option>
								<option value="0230">02시 30분</option>
								<option value="0300">03시 00분</option>
								<option value="0330">03시 30분</option>
								<option value="0400">04시 00분</option>
								<option value="0430">04시 30분</option>
								<option value="0500">05시 00분</option>
								<option value="0530">05시 30분</option>
								<option value="0600">06시 00분</option>
								<option value="0630">06시 30분</option>
								<option value="0700">07시 00분</option>
								<option value="0730">07시 30분</option>
								<option value="0800">08시 00분</option>
								<option value="0830">08시 30분</option>
								<option value="0900">09시 00분</option>
								<option value="0930">09시 30분</option>
								<option value="1000">10시 00분</option>
								<option value="1030">10시 30분</option>
								<option value="1100">11시 00분</option>
								<option value="1130">11시 30분</option>
								<option value="1200">12시 00분</option>
								<option value="1230">12시 30분</option>
								<option value="1300">13시 00분</option>
								<option value="1330">13시 30분</option>
								<option value="1400">14시 00분</option>
								<option value="1430">14시 30분</option>
								<option value="1500">15시 00분</option>
								<option value="1530">15시 30분</option>
								<option value="1600">16시 00분</option>
								<option value="1630">16시 30분</option>
								<option value="1700">17시 00분</option>
								<option value="1730">17시 30분</option>
								<option value="1800">18시 00분</option>
								<option value="1830">18시 30분</option>
								<option value="1900">19시 00분</option>
								<option value="1930">19시 30분</option>
								<option value="2000">20시 00분</option>
								<option value="2030">20시 30분</option>
								<option value="2100">21시 00분</option>
								<option value="2130">21시 30분</option>
								<option value="2200">22시 00분</option>
								<option value="2230">22시 30분</option>
								<option value="2300">23시 00분</option>
								<option value="2330">23시 30분</option>
								</select>
								</dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>대여지역</dt>
								<dd class="wrap_ipt">
									<input type="text" class="ipt_comm" placeholder="예)부산대" value="" id="et_delivery_place">
								</dd>
							</dl>
							<dl class="dl_comm dl_slt">
								<dt>반납일시</dt>
								<dd class="wrap_slt">
								<select name="finish_period_day" id="et_finish_period_day" class="slt_comm gradient">
								<option value="20160726">07월 26일(화)</option>
								<option value="20160727">07월 27일(수)</option>
								<option value="20160728">07월 28일(목)</option>
								<option value="20160729">07월 29일(금)</option>
								<option value="20160730">07월 30일(토)</option>
								<option value="20160731">07월 31일(일)</option>
								<option value="20160801">08월 01일(월)</option>
								<option value="20160802">08월 02일(화)</option>
								<option value="20160803">08월 03일(수)</option>
								<option value="20160804">08월 04일(목)</option>
								<option value="20160805">08월 05일(금)</option>
								<option value="20160806">08월 06일(토)</option>
								<option value="20160807">08월 07일(일)</option>
								<option value="20160808">08월 08일(월)</option>
								<option value="20160809">08월 09일(화)</option>
								<option value="20160810">08월 10일(수)</option>
								<option value="20160811">08월 11일(목)</option>
								<option value="20160812">08월 12일(금)</option>
								<option value="20160813">08월 13일(토)</option>
								<option value="20160814">08월 14일(일)</option>
								<option value="20160815">08월 15일(월)</option>
								<option value="20160816">08월 16일(화)</option>
								<option value="20160817">08월 17일(수)</option>
								<option value="20160818">08월 18일(목)</option>
								<option value="20160819">08월 19일(금)</option>
								<option value="20160820">08월 20일(토)</option>
								<option value="20160821">08월 21일(일)</option>
								<option value="20160822">08월 22일(월)</option>
								<option value="20160823">08월 23일(화)</option>
								<option value="20160824">08월 24일(수)</option>
								<option value="20160825">08월 25일(목)</option>
								<option value="20160826">08월 26일(금)</option>
								<option value="20160827">08월 27일(토)</option>
								<option value="20160828">08월 28일(일)</option>
								<option value="20160829">08월 29일(월)</option>
								<option value="20160830">08월 30일(화)</option>
								<option value="20160831">08월 31일(수)</option>
								<option value="20160901">09월 01일(목)</option>
								<option value="20160902">09월 02일(금)</option>
								<option value="20160903">09월 03일(토)</option>
								<option value="20160904">09월 04일(일)</option>
								<option value="20160905">09월 05일(월)</option>
								<option value="20160906">09월 06일(화)</option>
								<option value="20160907">09월 07일(수)</option>
								<option value="20160908">09월 08일(목)</option>
								<option value="20160909">09월 09일(금)</option>
								<option value="20160910">09월 10일(토)</option>
								<option value="20160911">09월 11일(일)</option>
								<option value="20160912">09월 12일(월)</option>
								<option value="20160913">09월 13일(화)</option>
								<option value="20160914">09월 14일(수)</option>
								<option value="20160915">09월 15일(목)</option>
								<option value="20160916">09월 16일(금)</option>
								<option value="20160917">09월 17일(토)</option>
								<option value="20160918">09월 18일(일)</option>
								<option value="20160919">09월 19일(월)</option>
								<option value="20160920">09월 20일(화)</option>
								<option value="20160921">09월 21일(수)</option>
								<option value="20160922">09월 22일(목)</option>
								<option value="20160923">09월 23일(금)</option>
								<option value="20160924">09월 24일(토)</option>
								<option value="20160925">09월 25일(일)</option>
								<option value="20160926">09월 26일(월)</option>
								<option value="20160927">09월 27일(화)</option>
								<option value="20160928">09월 28일(수)</option>
								<option value="20160929">09월 29일(목)</option>
								<option value="20160930">09월 30일(금)</option>
								<option value="20161001">10월 01일(토)</option>
								<option value="20161002">10월 02일(일)</option>
								<option value="20161003">10월 03일(월)</option>
								<option value="20161004">10월 04일(화)</option>
								<option value="20161005">10월 05일(수)</option>
								</select></dd>
								<dt></dt>
								<br />
								<dd class="wrap_slt">
								<select name="finish_period_hour" id="et_finish_period_hour" class="slt_comm gradient">
								<option value="0000">00시 00분</option>
								<option value="0030">00시 30분</option>
								<option value="0100">01시 00분</option>
								<option value="0130">01시 30분</option>
								<option value="0200">02시 00분</option>
								<option value="0230">02시 30분</option>
								<option value="0300">03시 00분</option>
								<option value="0330">03시 30분</option>
								<option value="0400">04시 00분</option>
								<option value="0430">04시 30분</option>
								<option value="0500">05시 00분</option>
								<option value="0530">05시 30분</option>
								<option value="0600">06시 00분</option>
								<option value="0630">06시 30분</option>
								<option value="0700">07시 00분</option>
								<option value="0730">07시 30분</option>
								<option value="0800">08시 00분</option>
								<option value="0830">08시 30분</option>
								<option value="0900">09시 00분</option>
								<option value="0930">09시 30분</option>
								<option value="1000">10시 00분</option>
								<option value="1030">10시 30분</option>
								<option value="1100">11시 00분</option>
								<option value="1130">11시 30분</option>
								<option value="1200">12시 00분</option>
								<option value="1230">12시 30분</option>
								<option value="1300">13시 00분</option>
								<option value="1330">13시 30분</option>
								<option value="1400">14시 00분</option>
								<option value="1430">14시 30분</option>
								<option value="1500">15시 00분</option>
								<option value="1530">15시 30분</option>
								<option value="1600">16시 00분</option>
								<option value="1630">16시 30분</option>
								<option value="1700">17시 00분</option>
								<option value="1730">17시 30분</option>
								<option value="1800">18시 00분</option>
								<option value="1830">18시 30분</option>
								<option value="1900">19시 00분</option>
								<option value="1930">19시 30분</option>
								<option value="2000">20시 00분</option>
								<option value="2030">20시 30분</option>
								<option value="2100">21시 00분</option>
								<option value="2130">21시 30분</option>
								<option value="2200">22시 00분</option>
								<option value="2230">22시 30분</option>
								<option value="2300">23시 00분</option>
								<option value="2330">23시 30분</option>
								</select>
								</dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>반납지역</dt>
								<dd class="wrap_ipt">
									<input type="text" class="ipt_comm" placeholder="예)직접 방문" value="" id="et_pickup_place">
								</dd>
							</dl>
							<dl class="dl_comm">
								<dt>자차보험</dt>
								<dd class="wrap_ipt">
								<dd id="et_insurance_name"></dd>
								</dd>
							</dl>
						</div>


					<!-- 결제 정보 -->
						<h3>결제 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm">
								<dt>대여금액</dt>
								<dd id="et_rental_price"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>보험금액</dt>
								<dd id="et_insurance_price"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>할인금액</dt>
								<dd id="et_coupon_price"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>결제총액</dt>
								<dd id="et_total_price"></dd>
							</dl>
						</div>
					<!-- 대여 정보 끝 -->

					<!-- 메모 수정 -->
						<h3>메모</h3>
						<div class="box_cont">
								<textarea class="ipt_comm" placeholder="예)고객님 연장문의 물어보기" id="et_memo"></textarea>
						</div>
					<!-- 메모 끝 -->

					<!-- 대여지점 정보 -->
						<h3>대여지점 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm">
								<dt>대여지점</dt>
								<dd id="et_company_name"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>지점주소</dt>
								<dd id="et_office_address"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>전화번호</dt>
								<dd id="et_office_phone"></dd>
							</dl>
						</div>
					<!-- 대여지점 정보 끝 -->
						<a href="javascript:;" class="btn_regist" id="button_save_edit_order">저장</a><!-- 수정 시 '확인'으로 변경 -->

					</div>





















					<div id="dt_order_add" style="display:none;">
					<!-- 예약자 정보 추가 -->
						<h3>예약자 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm dl_txt">
								<dt>예약자명</dt>
								<dd class="wrap_ipt">
									<input type="text" class="ipt_comm" value="" id="ad_username" placeholder="예)홍길동">
								</dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>생년월일</dt>
								<dd class="wrap_ipt">
									<input type="tel" class="ipt_comm " value="" id="ad_birthday" placeholder="예)19880515">
								</dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>연락처</dt>
								<dd class="wrap_ipt">
									<input type="tel" class="ipt_comm" placeholder="예)01012345678" value="" id="ad_tel">
								</dd>
							</dl>
						</div>
					<!-- 예약자 정보 끝 -->

					<!-- 대여 정보 - 추가	-->
						<h3>대여 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm dl_slt">
								<dt>대여차종</dt>
								<dd class="wrap_slt">
									<select class="slt_comm gradient" id="ad_car_serial">
										<option value=""></option>
<option value="4">K5 02하2407</option>
<option value="19">그랜드 스타렉스 71호1355</option>
<option value="239">그랜드 스타렉스 71하1303</option>
<option value="7">그랜드 스타렉스 71하1302</option>
<option value="10">레이 02하2404</option>
<option value="11">레이 02하2405</option>
<option value="12">레이 02하2409</option>
<option value="13">레이 02하2410</option>
<option value="9">아반떼 MD 02하2402</option>
<option value="5">아반떼 MD 02하2401</option>
<option value="14">아반떼 MD 02하2411</option>
<option value="15">아반떼 MD 02하2412</option>
<option value="16">아반떼 MD 02하2415</option>
<option value="17">아반떼 MD 02하2416</option>
<option value="8">올 뉴 카니발 02하2413</option>
<option value="18">올 뉴 카니발 02하2417</option>
									</select>
								</dd>
							</dl>
							<dl class="dl_comm dl_slt">
								<dt>대여일시</dt>
								<dd class="wrap_slt">
								<select name="start_period_day" id="ad_start_period_day" class="slt_comm gradient">
								<option value="20160726">07월 26일(화)</option>
								<option value="20160727">07월 27일(수)</option>
								<option value="20160728">07월 28일(목)</option>
								<option value="20160729">07월 29일(금)</option>
								<option value="20160730">07월 30일(토)</option>
								<option value="20160731">07월 31일(일)</option>
								<option value="20160801">08월 01일(월)</option>
								<option value="20160802">08월 02일(화)</option>
								<option value="20160803">08월 03일(수)</option>
								<option value="20160804">08월 04일(목)</option>
								<option value="20160805">08월 05일(금)</option>
								<option value="20160806">08월 06일(토)</option>
								<option value="20160807">08월 07일(일)</option>
								<option value="20160808">08월 08일(월)</option>
								<option value="20160809">08월 09일(화)</option>
								<option value="20160810">08월 10일(수)</option>
								<option value="20160811">08월 11일(목)</option>
								<option value="20160812">08월 12일(금)</option>
								<option value="20160813">08월 13일(토)</option>
								<option value="20160814">08월 14일(일)</option>
								<option value="20160815">08월 15일(월)</option>
								<option value="20160816">08월 16일(화)</option>
								<option value="20160817">08월 17일(수)</option>
								<option value="20160818">08월 18일(목)</option>
								<option value="20160819">08월 19일(금)</option>
								<option value="20160820">08월 20일(토)</option>
								<option value="20160821">08월 21일(일)</option>
								<option value="20160822">08월 22일(월)</option>
								<option value="20160823">08월 23일(화)</option>
								<option value="20160824">08월 24일(수)</option>
								<option value="20160825">08월 25일(목)</option>
								<option value="20160826">08월 26일(금)</option>
								<option value="20160827">08월 27일(토)</option>
								<option value="20160828">08월 28일(일)</option>
								<option value="20160829">08월 29일(월)</option>
								<option value="20160830">08월 30일(화)</option>
								<option value="20160831">08월 31일(수)</option>
								<option value="20160901">09월 01일(목)</option>
								<option value="20160902">09월 02일(금)</option>
								<option value="20160903">09월 03일(토)</option>
								<option value="20160904">09월 04일(일)</option>
								<option value="20160905">09월 05일(월)</option>
								<option value="20160906">09월 06일(화)</option>
								<option value="20160907">09월 07일(수)</option>
								<option value="20160908">09월 08일(목)</option>
								<option value="20160909">09월 09일(금)</option>
								<option value="20160910">09월 10일(토)</option>
								<option value="20160911">09월 11일(일)</option>
								<option value="20160912">09월 12일(월)</option>
								<option value="20160913">09월 13일(화)</option>
								<option value="20160914">09월 14일(수)</option>
								<option value="20160915">09월 15일(목)</option>
								<option value="20160916">09월 16일(금)</option>
								<option value="20160917">09월 17일(토)</option>
								<option value="20160918">09월 18일(일)</option>
								<option value="20160919">09월 19일(월)</option>
								<option value="20160920">09월 20일(화)</option>
								<option value="20160921">09월 21일(수)</option>
								<option value="20160922">09월 22일(목)</option>
								<option value="20160923">09월 23일(금)</option>
								<option value="20160924">09월 24일(토)</option>
								<option value="20160925">09월 25일(일)</option>
								<option value="20160926">09월 26일(월)</option>
								<option value="20160927">09월 27일(화)</option>
								<option value="20160928">09월 28일(수)</option>
								<option value="20160929">09월 29일(목)</option>
								<option value="20160930">09월 30일(금)</option>
								<option value="20161001">10월 01일(토)</option>
								<option value="20161002">10월 02일(일)</option>
								<option value="20161003">10월 03일(월)</option>
								<option value="20161004">10월 04일(화)</option>
								<option value="20161005">10월 05일(수)</option>
								</select></dd>
								<dt></dt>
								<br />
								<dd class="wrap_slt">
								<select name="start_period_hour" id="ad_start_period_hour" class="slt_comm gradient">
								<option value="0000">00시 00분</option>
								<option value="0030">00시 30분</option>
								<option value="0100">01시 00분</option>
								<option value="0130">01시 30분</option>
								<option value="0200">02시 00분</option>
								<option value="0230">02시 30분</option>
								<option value="0300">03시 00분</option>
								<option value="0330">03시 30분</option>
								<option value="0400">04시 00분</option>
								<option value="0430">04시 30분</option>
								<option value="0500">05시 00분</option>
								<option value="0530">05시 30분</option>
								<option value="0600">06시 00분</option>
								<option value="0630">06시 30분</option>
								<option value="0700">07시 00분</option>
								<option value="0730">07시 30분</option>
								<option value="0800">08시 00분</option>
								<option value="0830">08시 30분</option>
								<option value="0900">09시 00분</option>
								<option value="0930">09시 30분</option>
								<option value="1000">10시 00분</option>
								<option value="1030">10시 30분</option>
								<option value="1100">11시 00분</option>
								<option value="1130">11시 30분</option>
								<option value="1200">12시 00분</option>
								<option value="1230">12시 30분</option>
								<option value="1300">13시 00분</option>
								<option value="1330">13시 30분</option>
								<option value="1400">14시 00분</option>
								<option value="1430">14시 30분</option>
								<option value="1500">15시 00분</option>
								<option value="1530">15시 30분</option>
								<option value="1600">16시 00분</option>
								<option value="1630">16시 30분</option>
								<option value="1700">17시 00분</option>
								<option value="1730">17시 30분</option>
								<option value="1800">18시 00분</option>
								<option value="1830">18시 30분</option>
								<option value="1900">19시 00분</option>
								<option value="1930">19시 30분</option>
								<option value="2000">20시 00분</option>
								<option value="2030">20시 30분</option>
								<option value="2100">21시 00분</option>
								<option value="2130">21시 30분</option>
								<option value="2200">22시 00분</option>
								<option value="2230">22시 30분</option>
								<option value="2300">23시 00분</option>
								<option value="2330">23시 30분</option>
								</select>
								</dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>대여지역</dt>
								<dd class="wrap_ipt">
									<input type="text" class="ipt_comm" placeholder="예)부산대" value="" id="ad_delivery_place">
								</dd>
							</dl>
							<dl class="dl_comm dl_slt">
								<dt>반납일시</dt>
								<dd class="wrap_slt">
								<select name="finish_period_day" id="ad_finish_period_day" class="slt_comm gradient">
								<option value="20160726">07월 26일(화)</option>
								<option value="20160727">07월 27일(수)</option>
								<option value="20160728">07월 28일(목)</option>
								<option value="20160729">07월 29일(금)</option>
								<option value="20160730">07월 30일(토)</option>
								<option value="20160731">07월 31일(일)</option>
								<option value="20160801">08월 01일(월)</option>
								<option value="20160802">08월 02일(화)</option>
								<option value="20160803">08월 03일(수)</option>
								<option value="20160804">08월 04일(목)</option>
								<option value="20160805">08월 05일(금)</option>
								<option value="20160806">08월 06일(토)</option>
								<option value="20160807">08월 07일(일)</option>
								<option value="20160808">08월 08일(월)</option>
								<option value="20160809">08월 09일(화)</option>
								<option value="20160810">08월 10일(수)</option>
								<option value="20160811">08월 11일(목)</option>
								<option value="20160812">08월 12일(금)</option>
								<option value="20160813">08월 13일(토)</option>
								<option value="20160814">08월 14일(일)</option>
								<option value="20160815">08월 15일(월)</option>
								<option value="20160816">08월 16일(화)</option>
								<option value="20160817">08월 17일(수)</option>
								<option value="20160818">08월 18일(목)</option>
								<option value="20160819">08월 19일(금)</option>
								<option value="20160820">08월 20일(토)</option>
								<option value="20160821">08월 21일(일)</option>
								<option value="20160822">08월 22일(월)</option>
								<option value="20160823">08월 23일(화)</option>
								<option value="20160824">08월 24일(수)</option>
								<option value="20160825">08월 25일(목)</option>
								<option value="20160826">08월 26일(금)</option>
								<option value="20160827">08월 27일(토)</option>
								<option value="20160828">08월 28일(일)</option>
								<option value="20160829">08월 29일(월)</option>
								<option value="20160830">08월 30일(화)</option>
								<option value="20160831">08월 31일(수)</option>
								<option value="20160901">09월 01일(목)</option>
								<option value="20160902">09월 02일(금)</option>
								<option value="20160903">09월 03일(토)</option>
								<option value="20160904">09월 04일(일)</option>
								<option value="20160905">09월 05일(월)</option>
								<option value="20160906">09월 06일(화)</option>
								<option value="20160907">09월 07일(수)</option>
								<option value="20160908">09월 08일(목)</option>
								<option value="20160909">09월 09일(금)</option>
								<option value="20160910">09월 10일(토)</option>
								<option value="20160911">09월 11일(일)</option>
								<option value="20160912">09월 12일(월)</option>
								<option value="20160913">09월 13일(화)</option>
								<option value="20160914">09월 14일(수)</option>
								<option value="20160915">09월 15일(목)</option>
								<option value="20160916">09월 16일(금)</option>
								<option value="20160917">09월 17일(토)</option>
								<option value="20160918">09월 18일(일)</option>
								<option value="20160919">09월 19일(월)</option>
								<option value="20160920">09월 20일(화)</option>
								<option value="20160921">09월 21일(수)</option>
								<option value="20160922">09월 22일(목)</option>
								<option value="20160923">09월 23일(금)</option>
								<option value="20160924">09월 24일(토)</option>
								<option value="20160925">09월 25일(일)</option>
								<option value="20160926">09월 26일(월)</option>
								<option value="20160927">09월 27일(화)</option>
								<option value="20160928">09월 28일(수)</option>
								<option value="20160929">09월 29일(목)</option>
								<option value="20160930">09월 30일(금)</option>
								<option value="20161001">10월 01일(토)</option>
								<option value="20161002">10월 02일(일)</option>
								<option value="20161003">10월 03일(월)</option>
								<option value="20161004">10월 04일(화)</option>
								<option value="20161005">10월 05일(수)</option>
								</select></dd>
								<dt></dt>
								<br />
								<dd class="wrap_slt">
								<select name="finish_period_hour" id="ad_finish_period_hour" class="slt_comm gradient">
								<option value="0000">00시 00분</option>
								<option value="0030">00시 30분</option>
								<option value="0100">01시 00분</option>
								<option value="0130">01시 30분</option>
								<option value="0200">02시 00분</option>
								<option value="0230">02시 30분</option>
								<option value="0300">03시 00분</option>
								<option value="0330">03시 30분</option>
								<option value="0400">04시 00분</option>
								<option value="0430">04시 30분</option>
								<option value="0500">05시 00분</option>
								<option value="0530">05시 30분</option>
								<option value="0600">06시 00분</option>
								<option value="0630">06시 30분</option>
								<option value="0700">07시 00분</option>
								<option value="0730">07시 30분</option>
								<option value="0800">08시 00분</option>
								<option value="0830">08시 30분</option>
								<option value="0900">09시 00분</option>
								<option value="0930">09시 30분</option>
								<option value="1000">10시 00분</option>
								<option value="1030">10시 30분</option>
								<option value="1100">11시 00분</option>
								<option value="1130">11시 30분</option>
								<option value="1200">12시 00분</option>
								<option value="1230">12시 30분</option>
								<option value="1300">13시 00분</option>
								<option value="1330">13시 30분</option>
								<option value="1400">14시 00분</option>
								<option value="1430">14시 30분</option>
								<option value="1500">15시 00분</option>
								<option value="1530">15시 30분</option>
								<option value="1600">16시 00분</option>
								<option value="1630">16시 30분</option>
								<option value="1700">17시 00분</option>
								<option value="1730">17시 30분</option>
								<option value="1800">18시 00분</option>
								<option value="1830">18시 30분</option>
								<option value="1900">19시 00분</option>
								<option value="1930">19시 30분</option>
								<option value="2000">20시 00분</option>
								<option value="2030">20시 30분</option>
								<option value="2100">21시 00분</option>
								<option value="2130">21시 30분</option>
								<option value="2200">22시 00분</option>
								<option value="2230">22시 30분</option>
								<option value="2300">23시 00분</option>
								<option value="2330">23시 30분</option>
								</select>
								</dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>반납지역</dt>
								<dd class="wrap_ipt">
									<input type="text" class="ipt_comm" placeholder="예)직접방문" value="" id="ad_pickup_place">
								</dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>자차보험</dt>
								<dd><input type="text" class="ipt_comm" placeholder="예)일반자차" value="" id="ad_insurance_name"></dd>
							</dl>
						</div>


					<!-- 결제 정보 -->
						<h3>결제 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm dl_txt">
								<dt>대여금액</dt>
								<dd class="input_cost"><div class="wrap_ipt">
									<input type="tel" class="ipt_comm" value="" onkeyup="javascript:chk_total_price('ad');" onclick="this.value='';chk_total_price('ad');" placeholder="예)500000" id="ad_rental_price" name="ad_rental_price">
								<span>원</span>
								</div></dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>보험금액</dt>
								<dd class="input_cost"><div class="wrap_ipt">
									<input type="tel" class="ipt_comm" value="" onkeyup="javascript:chk_total_price('ad');" onclick="this.value='';chk_total_price('ad');" placeholder="예)500000" id="ad_insurance_price" name="ad_insurance_price">
								<span>원</span>
								</div></dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>할인금액</dt>
								<dd class="input_cost"><div class="wrap_ipt">
									<input type="tel" class="ipt_comm" value="" onkeyup="javascript:chk_total_price('ad');" onclick="this.value='';chk_total_price('ad');" placeholder="예)500000" id="ad_coupon_price" name="ad_coupon_price">
								<span>원</span>
								</div></dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>결제총액</dt>
								<dd class="input_cost"><div class="wrap_ipt">
									<input type="tel" class="ipt_comm" value="" placeholder="예)500000" id="ad_total_price" name="ad_total_price">
								<span>원</span>
								</div></dd>
							</dl>
						</div>
					<!-- 대여 정보 끝 -->

					<!-- 메모 수정 -->
						<h3>메모</h3>
						<div class="box_cont">
								<textarea class="ipt_comm" placeholder="예)고객님 연장문의 물어보기" id="ad_memo"></textarea>
						</div>
					<!-- 메모 끝 -->

					<!-- 대여지점 정보 -->
						<h3>대여지점 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm">
								<dt>대여지점</dt>
								<dd id="ad_company_name"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>지점주소</dt>
								<dd id="ad_office_address"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>전화번호</dt>
								<dd id="ad_office_phone"></dd>
							</dl>
						</div>
					<!-- 대여지점 정보 끝 -->
						<a href="javascript:;" class="btn_regist" id="button_save_new_order">저장</a><!-- 수정 시 '확인'으로 변경 -->
					</div>















					<div id="pt_order_edit" style="display:none;">
					<!-- 파트너 예약자 정보 수정 -->
						<h3>예약자 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm">
								<dt>예약번호:</dt>
								<dd id="pt_order_number"></dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>예약자명</dt>
								<dd class="wrap_ipt">
									<input type="text" class="ipt_comm" value="" id="pt_username" placeholder="예)홍길동">
								</dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>생년월일</dt>
								<dd class="wrap_ipt">
									<input type="tel" class="ipt_comm" value="" id="pt_birthday" placeholder="예)19880515">
								</dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>연락처</dt>
								<dd class="wrap_ipt">
									<input type="tel" class="ipt_comm" placeholder="예)01012345678" value="" id="pt_tel">
								</dd>
							</dl>
						</div>
					<!-- 예약자 정보 끝 -->

					<!-- 대여 정보 - 수정	-->
						<h3>대여 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm dl_slt">
								<dt>대여차종</dt>
								<dd class="wrap_slt">
									<select class="slt_comm gradient" id="pt_car_serial">
										<option value=""></option>
<option value="4">K5 02하2407</option>
<option value="19">그랜드 스타렉스 71호1355</option>
<option value="239">그랜드 스타렉스 71하1303</option>
<option value="7">그랜드 스타렉스 71하1302</option>
<option value="10">레이 02하2404</option>
<option value="11">레이 02하2405</option>
<option value="12">레이 02하2409</option>
<option value="13">레이 02하2410</option>
<option value="9">아반떼 MD 02하2402</option>
<option value="5">아반떼 MD 02하2401</option>
<option value="14">아반떼 MD 02하2411</option>
<option value="15">아반떼 MD 02하2412</option>
<option value="16">아반떼 MD 02하2415</option>
<option value="17">아반떼 MD 02하2416</option>
<option value="8">올 뉴 카니발 02하2413</option>
<option value="18">올 뉴 카니발 02하2417</option>
									</select>
								</dd>
							</dl>
							<dl class="dl_comm dl_slt">
								<dt>대여일시</dt>
								<dd class="wrap_slt">
								<select name="start_period_day" id="pt_start_period_day" class="slt_comm gradient">
								<option value="20160726">07월 26일(화)</option>
								<option value="20160727">07월 27일(수)</option>
								<option value="20160728">07월 28일(목)</option>
								<option value="20160729">07월 29일(금)</option>
								<option value="20160730">07월 30일(토)</option>
								<option value="20160731">07월 31일(일)</option>
								<option value="20160801">08월 01일(월)</option>
								<option value="20160802">08월 02일(화)</option>
								<option value="20160803">08월 03일(수)</option>
								<option value="20160804">08월 04일(목)</option>
								<option value="20160805">08월 05일(금)</option>
								<option value="20160806">08월 06일(토)</option>
								<option value="20160807">08월 07일(일)</option>
								<option value="20160808">08월 08일(월)</option>
								<option value="20160809">08월 09일(화)</option>
								<option value="20160810">08월 10일(수)</option>
								<option value="20160811">08월 11일(목)</option>
								<option value="20160812">08월 12일(금)</option>
								<option value="20160813">08월 13일(토)</option>
								<option value="20160814">08월 14일(일)</option>
								<option value="20160815">08월 15일(월)</option>
								<option value="20160816">08월 16일(화)</option>
								<option value="20160817">08월 17일(수)</option>
								<option value="20160818">08월 18일(목)</option>
								<option value="20160819">08월 19일(금)</option>
								<option value="20160820">08월 20일(토)</option>
								<option value="20160821">08월 21일(일)</option>
								<option value="20160822">08월 22일(월)</option>
								<option value="20160823">08월 23일(화)</option>
								<option value="20160824">08월 24일(수)</option>
								<option value="20160825">08월 25일(목)</option>
								<option value="20160826">08월 26일(금)</option>
								<option value="20160827">08월 27일(토)</option>
								<option value="20160828">08월 28일(일)</option>
								<option value="20160829">08월 29일(월)</option>
								<option value="20160830">08월 30일(화)</option>
								<option value="20160831">08월 31일(수)</option>
								<option value="20160901">09월 01일(목)</option>
								<option value="20160902">09월 02일(금)</option>
								<option value="20160903">09월 03일(토)</option>
								<option value="20160904">09월 04일(일)</option>
								<option value="20160905">09월 05일(월)</option>
								<option value="20160906">09월 06일(화)</option>
								<option value="20160907">09월 07일(수)</option>
								<option value="20160908">09월 08일(목)</option>
								<option value="20160909">09월 09일(금)</option>
								<option value="20160910">09월 10일(토)</option>
								<option value="20160911">09월 11일(일)</option>
								<option value="20160912">09월 12일(월)</option>
								<option value="20160913">09월 13일(화)</option>
								<option value="20160914">09월 14일(수)</option>
								<option value="20160915">09월 15일(목)</option>
								<option value="20160916">09월 16일(금)</option>
								<option value="20160917">09월 17일(토)</option>
								<option value="20160918">09월 18일(일)</option>
								<option value="20160919">09월 19일(월)</option>
								<option value="20160920">09월 20일(화)</option>
								<option value="20160921">09월 21일(수)</option>
								<option value="20160922">09월 22일(목)</option>
								<option value="20160923">09월 23일(금)</option>
								<option value="20160924">09월 24일(토)</option>
								<option value="20160925">09월 25일(일)</option>
								<option value="20160926">09월 26일(월)</option>
								<option value="20160927">09월 27일(화)</option>
								<option value="20160928">09월 28일(수)</option>
								<option value="20160929">09월 29일(목)</option>
								<option value="20160930">09월 30일(금)</option>
								<option value="20161001">10월 01일(토)</option>
								<option value="20161002">10월 02일(일)</option>
								<option value="20161003">10월 03일(월)</option>
								<option value="20161004">10월 04일(화)</option>
								<option value="20161005">10월 05일(수)</option>
								</select></dd>
								<dt></dt>
								<br />
								<dd class="wrap_slt">
								<select name="start_period_hour" id="pt_start_period_hour" class="slt_comm gradient">
								<option value="0000">00시 00분</option>
								<option value="0030">00시 30분</option>
								<option value="0100">01시 00분</option>
								<option value="0130">01시 30분</option>
								<option value="0200">02시 00분</option>
								<option value="0230">02시 30분</option>
								<option value="0300">03시 00분</option>
								<option value="0330">03시 30분</option>
								<option value="0400">04시 00분</option>
								<option value="0430">04시 30분</option>
								<option value="0500">05시 00분</option>
								<option value="0530">05시 30분</option>
								<option value="0600">06시 00분</option>
								<option value="0630">06시 30분</option>
								<option value="0700">07시 00분</option>
								<option value="0730">07시 30분</option>
								<option value="0800">08시 00분</option>
								<option value="0830">08시 30분</option>
								<option value="0900">09시 00분</option>
								<option value="0930">09시 30분</option>
								<option value="1000">10시 00분</option>
								<option value="1030">10시 30분</option>
								<option value="1100">11시 00분</option>
								<option value="1130">11시 30분</option>
								<option value="1200">12시 00분</option>
								<option value="1230">12시 30분</option>
								<option value="1300">13시 00분</option>
								<option value="1330">13시 30분</option>
								<option value="1400">14시 00분</option>
								<option value="1430">14시 30분</option>
								<option value="1500">15시 00분</option>
								<option value="1530">15시 30분</option>
								<option value="1600">16시 00분</option>
								<option value="1630">16시 30분</option>
								<option value="1700">17시 00분</option>
								<option value="1730">17시 30분</option>
								<option value="1800">18시 00분</option>
								<option value="1830">18시 30분</option>
								<option value="1900">19시 00분</option>
								<option value="1930">19시 30분</option>
								<option value="2000">20시 00분</option>
								<option value="2030">20시 30분</option>
								<option value="2100">21시 00분</option>
								<option value="2130">21시 30분</option>
								<option value="2200">22시 00분</option>
								<option value="2230">22시 30분</option>
								<option value="2300">23시 00분</option>
								<option value="2330">23시 30분</option>
								</select>
								</dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>대여지역</dt>
								<dd class="wrap_ipt">
									<input type="text" class="ipt_comm" placeholder="직접 방문" value="" id="pt_delivery_place">
								</dd>
							</dl>
							<dl class="dl_comm dl_slt">
								<dt>반납일시</dt>
								<dd class="wrap_slt">
								<select name="finish_period_day" id="pt_finish_period_day" class="slt_comm gradient">
								<option value="20160726">07월 26일(화)</option>
								<option value="20160727">07월 27일(수)</option>
								<option value="20160728">07월 28일(목)</option>
								<option value="20160729">07월 29일(금)</option>
								<option value="20160730">07월 30일(토)</option>
								<option value="20160731">07월 31일(일)</option>
								<option value="20160801">08월 01일(월)</option>
								<option value="20160802">08월 02일(화)</option>
								<option value="20160803">08월 03일(수)</option>
								<option value="20160804">08월 04일(목)</option>
								<option value="20160805">08월 05일(금)</option>
								<option value="20160806">08월 06일(토)</option>
								<option value="20160807">08월 07일(일)</option>
								<option value="20160808">08월 08일(월)</option>
								<option value="20160809">08월 09일(화)</option>
								<option value="20160810">08월 10일(수)</option>
								<option value="20160811">08월 11일(목)</option>
								<option value="20160812">08월 12일(금)</option>
								<option value="20160813">08월 13일(토)</option>
								<option value="20160814">08월 14일(일)</option>
								<option value="20160815">08월 15일(월)</option>
								<option value="20160816">08월 16일(화)</option>
								<option value="20160817">08월 17일(수)</option>
								<option value="20160818">08월 18일(목)</option>
								<option value="20160819">08월 19일(금)</option>
								<option value="20160820">08월 20일(토)</option>
								<option value="20160821">08월 21일(일)</option>
								<option value="20160822">08월 22일(월)</option>
								<option value="20160823">08월 23일(화)</option>
								<option value="20160824">08월 24일(수)</option>
								<option value="20160825">08월 25일(목)</option>
								<option value="20160826">08월 26일(금)</option>
								<option value="20160827">08월 27일(토)</option>
								<option value="20160828">08월 28일(일)</option>
								<option value="20160829">08월 29일(월)</option>
								<option value="20160830">08월 30일(화)</option>
								<option value="20160831">08월 31일(수)</option>
								<option value="20160901">09월 01일(목)</option>
								<option value="20160902">09월 02일(금)</option>
								<option value="20160903">09월 03일(토)</option>
								<option value="20160904">09월 04일(일)</option>
								<option value="20160905">09월 05일(월)</option>
								<option value="20160906">09월 06일(화)</option>
								<option value="20160907">09월 07일(수)</option>
								<option value="20160908">09월 08일(목)</option>
								<option value="20160909">09월 09일(금)</option>
								<option value="20160910">09월 10일(토)</option>
								<option value="20160911">09월 11일(일)</option>
								<option value="20160912">09월 12일(월)</option>
								<option value="20160913">09월 13일(화)</option>
								<option value="20160914">09월 14일(수)</option>
								<option value="20160915">09월 15일(목)</option>
								<option value="20160916">09월 16일(금)</option>
								<option value="20160917">09월 17일(토)</option>
								<option value="20160918">09월 18일(일)</option>
								<option value="20160919">09월 19일(월)</option>
								<option value="20160920">09월 20일(화)</option>
								<option value="20160921">09월 21일(수)</option>
								<option value="20160922">09월 22일(목)</option>
								<option value="20160923">09월 23일(금)</option>
								<option value="20160924">09월 24일(토)</option>
								<option value="20160925">09월 25일(일)</option>
								<option value="20160926">09월 26일(월)</option>
								<option value="20160927">09월 27일(화)</option>
								<option value="20160928">09월 28일(수)</option>
								<option value="20160929">09월 29일(목)</option>
								<option value="20160930">09월 30일(금)</option>
								<option value="20161001">10월 01일(토)</option>
								<option value="20161002">10월 02일(일)</option>
								<option value="20161003">10월 03일(월)</option>
								<option value="20161004">10월 04일(화)</option>
								<option value="20161005">10월 05일(수)</option>
								</select></dd>
								<dt></dt>
								<br />
								<dd class="wrap_slt">
								<select name="finish_period_hour" id="pt_finish_period_hour" class="slt_comm gradient">
								<option value="0000">00시 00분</option>
								<option value="0030">00시 30분</option>
								<option value="0100">01시 00분</option>
								<option value="0130">01시 30분</option>
								<option value="0200">02시 00분</option>
								<option value="0230">02시 30분</option>
								<option value="0300">03시 00분</option>
								<option value="0330">03시 30분</option>
								<option value="0400">04시 00분</option>
								<option value="0430">04시 30분</option>
								<option value="0500">05시 00분</option>
								<option value="0530">05시 30분</option>
								<option value="0600">06시 00분</option>
								<option value="0630">06시 30분</option>
								<option value="0700">07시 00분</option>
								<option value="0730">07시 30분</option>
								<option value="0800">08시 00분</option>
								<option value="0830">08시 30분</option>
								<option value="0900">09시 00분</option>
								<option value="0930">09시 30분</option>
								<option value="1000">10시 00분</option>
								<option value="1030">10시 30분</option>
								<option value="1100">11시 00분</option>
								<option value="1130">11시 30분</option>
								<option value="1200">12시 00분</option>
								<option value="1230">12시 30분</option>
								<option value="1300">13시 00분</option>
								<option value="1330">13시 30분</option>
								<option value="1400">14시 00분</option>
								<option value="1430">14시 30분</option>
								<option value="1500">15시 00분</option>
								<option value="1530">15시 30분</option>
								<option value="1600">16시 00분</option>
								<option value="1630">16시 30분</option>
								<option value="1700">17시 00분</option>
								<option value="1730">17시 30분</option>
								<option value="1800">18시 00분</option>
								<option value="1830">18시 30분</option>
								<option value="1900">19시 00분</option>
								<option value="1930">19시 30분</option>
								<option value="2000">20시 00분</option>
								<option value="2030">20시 30분</option>
								<option value="2100">21시 00분</option>
								<option value="2130">21시 30분</option>
								<option value="2200">22시 00분</option>
								<option value="2230">22시 30분</option>
								<option value="2300">23시 00분</option>
								<option value="2330">23시 30분</option>
								</select>
								</dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>반납지역</dt>
								<dd class="wrap_ipt">
									<input type="text" class="ipt_comm" placeholder="직접 방문" value="" id="pt_pickup_place">
								</dd>
							</dl>
							<dl class="dl_comm">
								<dt>자차보험</dt>
								<dd class="wrap_ipt">
									<input type="text" class="ipt_comm" placeholder="예)일반자차" value="" id="pt_insurance_name">
								</dd>
							</dl>
						</div>


					<!-- 결제 정보 -->
						<h3>결제 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm dl_txt">
								<dt>대여금액</dt>
								<dd class="input_cost"><div class="wrap_ipt">
									<input type="tel" class="ipt_comm" value="" onkeyup="javascript:chk_total_price('pt');" onclick="this.value='';chk_total_price('pt');" placeholder="예)500000" id="pt_rental_price" name="pt_rental_price">
								<span>원</span>
								</div></dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>보험금액</dt>
								<dd class="input_cost"><div class="wrap_ipt">
									<input type="tel" class="ipt_comm" value="" onkeyup="javascript:chk_total_price('pt');" onclick="this.value='';chk_total_price('pt');" placeholder="예)500000" id="pt_insurance_price" name="pt_insurance_price">
								<span>원</span>
								</div></dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>할인금액</dt>
								<dd class="input_cost"><div class="wrap_ipt">
									<input type="tel" class="ipt_comm" value="" onkeyup="javascript:chk_total_price('ad');" onclick="this.value='';chk_total_price('pt');" placeholder="예)500000" id="pt_coupon_price" name="pt_coupon_price">
								<span>원</span>
								</div></dd>
							</dl>
							<dl class="dl_comm dl_txt">
								<dt>결제총액</dt>
								<dd class="input_cost"><div class="wrap_ipt">
									<input type="tel" class="ipt_comm" value="" placeholder="예)500000" id="pt_total_price" name="pt_total_price">
								<span>원</span>
								</div></dd>
							</dl>
						</div>
					<!-- 대여 정보 끝 -->

					<!-- 메모 수정 -->
						<h3>메모</h3>
						<div class="box_cont">
								<textarea class="ipt_comm" placeholder="예)고객님 연장문의 물어보기" id="pt_memo"></textarea>
						</div>
					<!-- 메모 끝 -->

					<!-- 대여지점 정보 -->
						<h3>대여지점 정보</h3>
						<div class="box_cont">
							<dl class="dl_comm">
								<dt>대여지점</dt>
								<dd id="pt_company_name"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>지점주소</dt>
								<dd id="pt_office_address"></dd>
							</dl>
							<dl class="dl_comm">
								<dt>전화번호</dt>
								<dd id="pt_office_phone"></dd>
							</dl>
						</div>
					<!-- 대여지점 정보 끝 -->
						<a href="javascript:;" class="btn_regist" id="button_save_partner_edit_order">저장</a><!-- 수정 시 '확인'으로 변경 -->
					</div>





				</div>

			</div>
		</div>
	</div>
	<!-- 예약정보 팝업 끝 -->


	<script>
$(document).ready(function(e) {
	$('.menu').click(function(){
		$('body').bind('touchmove', function(e){e.preventDefault()}); //스크롤방지
		$('.side').fadeIn('fast');
		$('.wrap_side').animate({'margin-left':0},'fast')
	})
	$('.close_side').click(function(){
		$("body").unbind('touchmove'); //스크롤 시작
		$('.side').fadeOut('fast');
		$('.wrap_side').animate({'margin-left':-230},'fast')
	})
});
</script>
<div class="side dimmed">
	<div class="wrap_side">
		<div class="head_side">
			<strong class="company">삼천리렌트카 금정영업소</strong>
			<span>0515813002</span>
			<a href="javascript:;" class="close_side">닫기</a>
		</div>
		<ul class="cont_side">
			<li class=""><a href="info.php">파트너 정보</a></li>
			<li class="on"><a href="rent.php">대여관리</a></li>
			<li class=""><a href="car.php">차량관리</a></li>
			<li class=""><a href="sales.php">매출관리</a></li>
			<li class=""><a href="blacklist.php">블랙리스트</a></li>
			<!--li class=""><a href="notice.php">공지사항</a></li-->
			<li class=""><a href="logout.php">로그아웃</a></li>
		</ul>
	</div>
	<a href="javascript:;" class="close_side"></a>
</div>
</body>
<script language="javascript">
dt_order_number = new Array();
dt_username = new Array();
dt_birthday = new Array();
et_birthday = new Array();
pt_birthday = new Array();
dt_tel = new Array();
dt_car_name_detail = new Array();
dt_period_start = new Array();
et_period_start_day = new Array();
et_period_start_hour = new Array();
pt_period_start_day = new Array();
pt_period_start_hour = new Array();
dt_delivery_place = new Array();
dt_period_finish = new Array();
et_period_finish_day = new Array();
et_period_finish_hour = new Array();
pt_period_finish_day = new Array();
pt_period_finish_hour = new Array();
dt_pickup_place = new Array();
dt_rental_price = new Array();
dt_insurance_name = new Array();
dt_insurance_price = new Array();
pt_insurance_price = new Array();
dt_coupon_price = new Array();
dt_total_price = new Array();
dt_company_name = new Array();
dt_office_address = new Array();
dt_office_phone = new Array();
dt_memo = new Array();
et_memo = new Array();
et_car_serial = new Array();
et_order_flag = new Array();

var od_serial = 0;


dt_order_number['4253'] = '160823624';
dt_username['4253'] = '';
dt_birthday['4253'] = '..';
et_birthday['4253'] = '';
dt_tel['4253'] = '';
dt_car_name_detail['4253'] = '그랜드 스타렉스(71호1355)';
dt_period_start['4253'] = '2016-08-28(일) 08:00 부터';
et_period_start_day['4253'] = '20160828';
et_period_start_hour['4253'] = '0800';
dt_delivery_place['4253'] = '직접방문';
dt_period_finish['4253'] = '2016-08-28(일) 19:00 까지';
et_period_finish_day['4253'] = '20160828';
et_period_finish_hour['4253'] = '1900';
dt_pickup_place['4253'] = '직접반납';
dt_rental_price['4253'] = '0원';
pt_rental_price['4253'] = '0';
dt_insurance_price['4253'] = '0원';
pt_insurance_price['4253'] = '0';
dt_insurance_name['4253'] = '미가입';
dt_coupon_price['4253'] = '0원';
et_coupon_price['4253'] = '0';
dt_total_price['4253'] = '0원';
dt_company_name['4253'] = '삼천리렌트카 금정영업소';
dt_office_address['4253'] = '부산시 금정구 부곡동 268-2';
dt_office_phone['4253'] = '01071571011';
dt_memo['4253'] = '메모 없음';
et_memo['4253'] = '';
et_car_serial['4253'] = '19';
et_order_flag['4253'] = 'P';


dt_order_number['4268'] = '160823639';
dt_username['4268'] = '노용주';
dt_birthday['4268'] = '1992.10.14';
et_birthday['4268'] = '19921014';
dt_tel['4268'] = '01028832889';
dt_car_name_detail['4268'] = '레이(02하2404)';
dt_period_start['4268'] = '2016-08-24(수) 10:00 부터';
et_period_start_day['4268'] = '20160824';
et_period_start_hour['4268'] = '1000';
dt_delivery_place['4268'] = '부산/개금';
dt_period_finish['4268'] = '2016-08-26(금) 16:00 까지';
et_period_finish_day['4268'] = '20160826';
et_period_finish_hour['4268'] = '1600';
dt_pickup_place['4268'] = '부산/개금';
dt_rental_price['4268'] = '130,000원';
pt_rental_price['4268'] = '130000';
dt_insurance_price['4268'] = '0원';
pt_insurance_price['4268'] = '0';
dt_insurance_name['4268'] = '미가입';
dt_coupon_price['4268'] = '0원';
et_coupon_price['4268'] = '0';
dt_total_price['4268'] = '130,000원';
dt_company_name['4268'] = '삼천리렌트카 금정영업소';
dt_office_address['4268'] = '부산시 금정구 부곡동 268-2';
dt_office_phone['4268'] = '01071571011';
dt_memo['4268'] = '메모 없음';
et_memo['4268'] = '';
et_car_serial['4268'] = '10';
et_order_flag['4268'] = 'F';


dt_order_number['4323'] = '160825622';
dt_username['4323'] = '박재윤';
dt_birthday['4323'] = '1991.06.21';
et_birthday['4323'] = '19910621';
dt_tel['4323'] = '01083958796';
dt_car_name_detail['4323'] = '아반떼 MD(02하2402)';
dt_period_start['4323'] = '2016-08-28(일) 09:30 부터';
et_period_start_day['4323'] = '20160828';
et_period_start_hour['4323'] = '0930';
dt_delivery_place['4323'] = '부산/신평';
dt_period_finish['4323'] = '2016-08-29(월) 09:30 까지';
et_period_finish_day['4323'] = '20160829';
et_period_finish_hour['4323'] = '0930';
dt_pickup_place['4323'] = '부산/신평';
dt_rental_price['4323'] = '60,000원';
pt_rental_price['4323'] = '60000';
dt_insurance_price['4323'] = '10,000원';
pt_insurance_price['4323'] = '10000';
dt_insurance_name['4323'] = '일반자차';
dt_coupon_price['4323'] = '0원';
et_coupon_price['4323'] = '0';
dt_total_price['4323'] = '70,000원';
dt_company_name['4323'] = '삼천리렌트카 금정영업소';
dt_office_address['4323'] = '부산시 금정구 부곡동 268-2';
dt_office_phone['4323'] = '01071571011';
dt_memo['4323'] = '메모 없음';
et_memo['4323'] = '';
et_car_serial['4323'] = '9';
et_order_flag['4323'] = 'F';


dt_order_number['4255'] = '160823626';
dt_username['4255'] = '';
dt_birthday['4255'] = '..';
et_birthday['4255'] = '';
dt_tel['4255'] = '';
dt_car_name_detail['4255'] = '올 뉴 카니발(02하2417)';
dt_period_start['4255'] = '2016-08-28(일) 08:00 부터';
et_period_start_day['4255'] = '20160828';
et_period_start_hour['4255'] = '0800';
dt_delivery_place['4255'] = '직접방문';
dt_period_finish['4255'] = '2016-08-28(일) 19:00 까지';
et_period_finish_day['4255'] = '20160828';
et_period_finish_hour['4255'] = '1900';
dt_pickup_place['4255'] = '직접반납';
dt_rental_price['4255'] = '0원';
pt_rental_price['4255'] = '0';
dt_insurance_price['4255'] = '0원';
pt_insurance_price['4255'] = '0';
dt_insurance_name['4255'] = '미가입';
dt_coupon_price['4255'] = '0원';
et_coupon_price['4255'] = '0';
dt_total_price['4255'] = '0원';
dt_company_name['4255'] = '삼천리렌트카 금정영업소';
dt_office_address['4255'] = '부산시 금정구 부곡동 268-2';
dt_office_phone['4255'] = '01071571011';
dt_memo['4255'] = '메모 없음';
et_memo['4255'] = '';
et_car_serial['4255'] = '18';
et_order_flag['4255'] = 'P';


dt_order_number['4296'] = '160824619';
dt_username['4296'] = '이성환';
dt_birthday['4296'] = '1993.04.27';
et_birthday['4296'] = '19930427';
dt_tel['4296'] = '01075989956';
dt_car_name_detail['4296'] = '올 뉴 카니발(02하2417)';
dt_period_start['4296'] = '2016-08-26(금) 15:00 부터';
et_period_start_day['4296'] = '20160826';
et_period_start_hour['4296'] = '1500';
dt_delivery_place['4296'] = '부산/구포';
dt_period_finish['4296'] = '2016-08-27(토) 15:00 까지';
et_period_finish_day['4296'] = '20160827';
et_period_finish_hour['4296'] = '1500';
dt_pickup_place['4296'] = '부산/구포';
dt_rental_price['4296'] = '110,000원';
pt_rental_price['4296'] = '110000';
dt_insurance_price['4296'] = '20,000원';
pt_insurance_price['4296'] = '20000';
dt_insurance_name['4296'] = '일반자차';
dt_coupon_price['4296'] = '0원';
et_coupon_price['4296'] = '0';
dt_total_price['4296'] = '130,000원';
dt_company_name['4296'] = '삼천리렌트카 금정영업소';
dt_office_address['4296'] = '부산시 금정구 부곡동 268-2';
dt_office_phone['4296'] = '01071571011';
dt_memo['4296'] = '메모 없음';
et_memo['4296'] = '';
et_car_serial['4296'] = '18';
et_order_flag['4296'] = 'F';


$(document).ready(function(e) {
	document.body.style.overflow = "visible";
	$('.close_info').click(function(){
//		$("body").unbind('touchmove'); //스크롤 시작
		$('body').css('overflow', 'scroll');
//		$('body').css('position', 'absolute`');
		$('.info_pop').css('display', 'none');
		$('.info_pop').css('overflow', 'hidden');
		$('#dt_order_view').css('display', 'block');
		$('#dt_order_edit').css('display', 'none');
		$('#dt_order_add').css('display', 'none');
		$('#pt_order_edit').css('display', 'none');
	})
});

function view_detail(order_serial){

//		$('body').bind('touchmove', function(e){e.preventDefault()}); //스크롤방지
		$('body').css('overflow', 'hidden');
//		$('body').css('position', 'fixed');
		$('.info_pop').css('display', 'block');
		$('.info_pop').css('overflow', 'scroll');

		$('#order_title').text('예약내역 확인');
		$('#dt_order_number').text(dt_order_number[order_serial]);
		$('#dt_username').text(dt_username[order_serial]);
		$('#dt_birthday').text(dt_birthday[order_serial]);
		$('#dt_tel').text(dt_tel[order_serial]);
		$('#dt_car_name_detail').text(dt_car_name_detail[order_serial]);
		$('#dt_period_start').text(dt_period_start[order_serial]);
		$('#dt_delivery_place').text(dt_delivery_place[order_serial]);
		$('#dt_period_finish').text(dt_period_finish[order_serial]);
		$('#dt_pickup_place').text(dt_pickup_place[order_serial]);

		$('#dt_rental_price').text(dt_rental_price[order_serial]);
		$('#dt_insurance_price').text(dt_insurance_price[order_serial]);
		$('#dt_insurance_name').text(dt_insurance_name[order_serial]);
		$('#dt_coupon_price').text(dt_coupon_price[order_serial]);
		$('#dt_total_price').text(dt_total_price[order_serial]);

		$('#dt_company_name').text(dt_company_name[order_serial]);
		$('#dt_office_address').text(dt_office_address[order_serial]);
		$('#dt_office_phone').text(dt_office_phone[order_serial]);
		$('#dt_memo').html(dt_memo[order_serial]);
		od_serial = order_serial;
		if(et_order_flag[od_serial] == "P"){
			$('#view_delete_button').css('display', 'block');
		} else {
			$('#view_delete_button').css('display', 'none');
		}
}

function edit_detail(){
		$('#order_title').text('예약 수정');
		$('#et_order_number').text(dt_order_number[od_serial]);
		$('#et_username').val(dt_username[od_serial]);
		$('#et_birthday').val(et_birthday[od_serial]);
		$('#et_tel').val(dt_tel[od_serial]);
		$('#et_car_serial').val(et_car_serial[od_serial]);
		$('#et_start_period_day').val(et_period_start_day[od_serial]);
		$('#et_start_period_hour').val(et_period_start_hour[od_serial]);
		$('#et_finish_period_day').val(et_period_finish_day[od_serial]);
		$('#et_finish_period_hour').val(et_period_finish_hour[od_serial]);
		$('#et_delivery_place').val(dt_delivery_place[od_serial]);
		$('#et_pickup_place').val(dt_pickup_place[od_serial]);
		$('#et_rental_price').text(dt_rental_price[od_serial]);
		$('#et_insurance_price').text(dt_insurance_price[od_serial]);
		$('#et_insurance_name').text(dt_insurance_name[od_serial]);
		$('#et_coupon_price').text(dt_coupon_price[od_serial]);
		$('#et_total_price').text(dt_total_price[od_serial]);

		$('#et_company_name').text(dt_company_name[od_serial]);
		$('#et_office_address').text(dt_office_address[od_serial]);
		$('#et_office_phone').text(dt_office_phone[od_serial]);

		$('#et_memo').val(et_memo[od_serial]);
}

function add_order(start_date, start_hour, finish_date, finish_hour, car_serial){
		$('#ad_start_period_day').val(start_date);
		$('#ad_start_period_hour').val(start_hour);
		$('#ad_finish_period_day').val(finish_date);
		$('#ad_finish_period_hour').val(finish_hour);
		if(parseInt(car_serial) >= '1'){
			$('#ad_car_serial').val(car_serial);
		}
		$('#ad_company_name').text('삼천리렌트카 금정영업소');
		$('#ad_office_address').text('부산시 금정구 부곡동 268-2');
		$('#ad_office_phone').text('0515813002');
}

function add_order_from_tab(start_date, start_hour, finish_date, finish_hour, car_serial){
//	$('body').bind('touchmove', function(e){e.preventDefault()}); //스크롤방지
	$('body').css('overflow', 'hidden');
//	$('body').css('position', 'fixed');
	$('.info_pop').css('display', 'block');
	$('.info_pop').css('overflow', 'scroll');
	$('#order_title').text('예약 추가');

	add_order(start_date, start_hour, finish_date, finish_hour, car_serial)
	$('#dt_order_view').css('display', 'none');
	$('#dt_order_add').css('display', 'block');
}

function partner_edit_detail(){
		$('#order_title').text('예약 수정');
		$('#pt_order_number').text(dt_order_number[od_serial]);
		$('#pt_username').val(dt_username[od_serial]);
		$('#pt_birthday').val(et_birthday[od_serial]);
		$('#pt_tel').val(dt_tel[od_serial]);
		$('#pt_car_serial').val(et_car_serial[od_serial]);
		$('#pt_start_period_day').val(et_period_start_day[od_serial]);
		$('#pt_start_period_hour').val(et_period_start_hour[od_serial]);
		$('#pt_finish_period_day').val(et_period_finish_day[od_serial]);
		$('#pt_finish_period_hour').val(et_period_finish_hour[od_serial]);
		$('#pt_delivery_place').val(dt_delivery_place[od_serial]);
		$('#pt_pickup_place').val(dt_pickup_place[od_serial]);
		$('#pt_rental_price').val(pt_rental_price[od_serial]);
		$('#pt_insurance_price').val(pt_insurance_price[od_serial]);
		$('#pt_coupon_price').val(et_coupon_price[od_serial]);
		$('#pt_insurance_name').val(dt_insurance_name[od_serial]);
		$('#pt_total_price').val( number_format(parseInt($('#pt_rental_price').val()?$('#pt_rental_price').val():0) + parseInt($('#pt_insurance_price').val()?$('#pt_insurance_price').val():0) ));

		$('#pt_company_name').text(dt_company_name[od_serial]);
		$('#pt_office_address').text(dt_office_address[od_serial]);
		$('#pt_office_phone').text(dt_office_phone[od_serial]);

		$('#pt_memo').val(et_memo[od_serial]);
}

$( "#bt_order_edit" ).click(function() {
	if(et_order_flag[od_serial] == "P"){
		partner_edit_detail();
		$('#dt_order_view').css('display', 'none');
		$('#pt_order_edit').css('display', 'block');
	} else {
		edit_detail();
		$('#dt_order_view').css('display', 'none');
		$('#dt_order_edit').css('display', 'block');
	}
});

$( "#bt_add_order" ).click(function() {
//	$('body').bind('touchmove', function(e){e.preventDefault()}); //스크롤방지
	$('body').css('overflow', 'hidden');
//	$('body').css('position', 'fixed');
	$('.info_pop').css('display', 'block');
	$('.info_pop').css('overflow', 'scroll');
	$('#order_title').text('예약 추가');

	add_order('20160826', '0800', '20160826', '1900')
	$('#dt_order_view').css('display', 'none');
	$('#dt_order_add').css('display', 'block');
});

$( "#bt_prev_date" ).click(function() {
	top.location.href='rent.php?period_date=20160825';
});

$( "#bt_next_date" ).click(function() {
	top.location.href='rent.php?period_date=20160827';
});

function change_car_flag(id_number, car_serial){
	if($("input:checkbox[id="+ id_number +"]").is(":checked")){
		var flag = "Y";
	} else {
		var flag = "N";
	}
	if(car_serial >= '1'){
		$.post("./change_partner_car_flag.php",{
			car_serial:car_serial,
			flag:flag,
		}, function(data2){
			var data = data2.split('|');
			if(data[0] == 'OK1'){
//				alert(data[1]);
//				document.location.reload();
			} else {
				alert(data2);
			}
		});
	} else {
		alert('차량정보를 확인해 주세요.');
		return false;
	}
}

$( "#button_save_new_order" ).click(function() {
	var period_start_date = $('#ad_start_period_day option:selected').val() + $('#ad_start_period_hour option:selected').val();
	var period_finish_date = $('#ad_finish_period_day option:selected').val() + $('#ad_finish_period_hour option:selected').val();
	var car_serial = $('#ad_car_serial option:selected').val();
	if(car_serial){
		$.post("./save_order_process.php",{
			username:$('#ad_username').val(),
			birthday:$('#ad_birthday').val(),
			tel:$('#ad_tel').val(),
			car_serial:car_serial,
			period_start:period_start_date,
			delivery_place:$('#ad_delivery_place').val(),
			period_finish:period_finish_date,
			pickup_place:$('#ad_pickup_place').val(),
			insurance_name:$('#ad_insurance_name').val(),
			rental_price:$('#ad_rental_price').val(),
			insurance_price:$('#ad_insurance_price').val(),
			insurance_name:$('#ad_insurance_name').val(),
			coupon_price:$('#ad_coupon_price').val(),
			total_price:parseInt($('#ad_rental_price').val()) + parseInt($('#ad_insurance_price').val()) - parseInt($('#ad_coupon_price').val()),
			comment:$('#ad_memo').val(),
		}, function(data2){
			var data = data2.split('|');
			if(data[0] == 'OK1'){
				alert('추가되었습니다.');
				document.location.reload();
			} else {
				alert(data2);
			}
		});
	} else {
		alert('차량선택를 확인해 주세요.');
		return false;
	}
});

$( "#button_save_partner_edit_order" ).click(function() {
	var period_start_date = $('#pt_start_period_day option:selected').val() + $('#pt_start_period_hour option:selected').val();
	var period_finish_date = $('#pt_finish_period_day option:selected').val() + $('#pt_finish_period_hour option:selected').val();
	var car_serial = $('#pt_car_serial option:selected').val();
	if(car_serial){
		$.post("./save_order_process.php",{
			username:$('#pt_username').val(),
			birthday:$('#pt_birthday').val(),
			tel:$('#pt_tel').val(),
			car_serial:car_serial,
			period_start:period_start_date,
			delivery_place:$('#pt_delivery_place').val(),
			period_finish:period_finish_date,
			pickup_place:$('#pt_pickup_place').val(),
			insurance_name:$('#pt_insurance_name').val(),
			rental_price:$('#pt_rental_price').val(),
			insurance_price:$('#pt_insurance_price').val(),
			insurance_name:$('#pt_insurance_name').val(),
			coupon_price:$('#pt_coupon_price').val(),
			total_price:parseInt($('#pt_rental_price').val()) + parseInt($('#pt_insurance_price').val()) - parseInt($('#pt_coupon_price').val()),
			comment:$('#pt_memo').val(),
			order_number:od_serial,
		}, function(data2){
			var data = data2.split('|');
			if(data[0] == 'OK1'){
				alert('추가되었습니다.');
				document.location.reload();
			} else {
				alert(data2);
			}
		});
	} else {
		alert('차량선택를 확인해 주세요.');
		return false;
	}
});

function number_format(num) {
	var reg = /(^[+-]?\d+)(\d{3})/;
	num += '';

	while (reg.test(num))
		num = num.replace(reg, '$1' + ',' + '$2');

	return num;
}

function chk_total_price(mode_flag){
	if(mode_flag == 'ad'){
		$('#ad_total_price').val( (parseInt($('#ad_rental_price').val()?$('#ad_rental_price').val():0) + parseInt($('#ad_insurance_price').val()?$('#ad_insurance_price').val():0) - parseInt($('#ad_coupon_price').val()?$('#ad_coupon_price').val():0) ) );

	} else if(mode_flag == 'pt'){
		$('#pt_total_price').tval( (parseInt($('#pt_rental_price').val()?$('#pt_rental_price').val():0) + parseInt($('#pt_insurance_price').val()?$('#pt_insurance_price').val():0) - parseInt($('#pt_coupon_price').val()?$('#pt_coupon_price').val():0) ) );
	}
}

$( "#button_delete_order" ).click(function() {
	if(od_serial){
		$.post("./delete_order_process.php",{
			order_number:od_serial,
		}, function(data2){
			var data = data2.split('|');
			if(data[0] == 'OK1'){
				alert('삭제되었습니다.');
				document.location.reload();
			} else {
				alert(data2);
			}
		});
	} else {
		alert('예약번호를 확인해 주세요.');
		return false;
	}
});


$( "#button_save_edit_order" ).click(function() {

	var period_start_date = $('#et_start_period_day option:selected').val() + $('#et_start_period_hour option:selected').val();
	var period_finish_date = $('#et_finish_period_day option:selected').val() + $('#et_finish_period_hour option:selected').val();
	var car_serial = $('#et_car_serial option:selected').val();
	if(car_serial){
		$.post("./save_order_process.php",{
			username:$('#et_username').val(),
			birthday:$('#et_birthday').val(),
			tel:$('#et_tel').val(),
			car_serial:car_serial,
			period_start:period_start_date,
			delivery_place:$('#et_delivery_place').val(),
			period_finish:period_finish_date,
			pickup_place:$('#et_pickup_place').val(),
			comment:$('#et_memo').val(),
			order_number:od_serial,
		}, function(data2){
			var data = data2.split('|');
			if(data[0] == 'OK1'){
				alert('수정되었습니다.');
				document.location.reload();
			} else {
				alert(data2);
			}
		});
	} else {
		alert('차량선택를 확인해 주세요.');
		return false;
	}
});

</script>
</html>
