<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Rengo_encryption {

    function encryption($value){
    		//a~z까지 5개까지 random하게 붙여서 왼쪽, 4개 뽑아서 오른쪽에 붙임
    	$left_str = $this->get_rand_alphanumeric(5);
    	$right_str = $this->get_rand_alphanumeric(4);
    	$value = $left_str.$value.$right_str;
    	//base64로 인코딩
    	// echo $value;
    	return $this->base64encode($value);

    }

    function decription($encoded_value){
    	//base64 디코딩
    	$str = $this->base64decode($encoded_value);
    	//왼쪽에서 5개 짜르고, 거기서 오른쪽에서 4개 짜름
    	// echo $str;
    	$value = substr($str,5);
    	$length = strlen($value) - 4;
    	return substr($value, 0, $length);

    }

	function get_rand_alphanumeric($length) {
    	$str = '';
    	for($i=0; $i<$length; $i++){
    		$letter = chr(65+rand(0,23));
    		$str.= $letter;  
    	}
    	return $str;
    	
	}

	 function base64encode($string) {  
	    $data = str_replace(array('+','/','='),array('-','_',''),base64_encode($string));  
	    return $data;  
	 }  

	 function base64decode($string) {  
	    $data = str_replace(array('-','_'),array('+','/'),$string);  
	    $mod4 = strlen($data) % 4;  
	    if ($mod4) {  
	        $data .= substr('====', $mod4);  
	    }  
	    return base64_decode($data);  
	 }  
}

/* End of file Someclass.php */