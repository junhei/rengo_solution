<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 require FCPATH.'vendor/autoload.php';

class Partnersetting extends CI_Controller {


	function __construct() { 

		parent::__construct();

		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		// $this->load->helper(array('form', 'url'));
		$this->load->model('admin/Admin_model', 'admin');
		$this->load->model('Company_model','company');
		$this->load->library('Rengo_encryption');
		$this->load->library('upload');
		// 관리자 메뉴 접근 퍼미션 체크
		$menu_permission = 5;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y")
			$this->admin->admin_logout(); 

	}

	function _view($url, $data = ''){

		$data['admin_id'] = $this->session->userdata('admin_id');
		$this->load->view("admin/admin_layout_top", $data);
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$company_serial = $this->session->userdata('company_serial');
		$data['permission'] = $this->session->userdata('admin_permission');
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$data['company_list'] = $this->company->get_company($company_serial);
		$this->_view("preference/partner_setting_view", $data); 
	}

	// onwer_name, password는 admin에 저장, 나머지는 company
	function partner_info_update(){
		
		$select_status = urldecode($this->input->post('select_status',TRUE));
		$email = urldecode($this->input->post('email',TRUE));
		$company_serial = intval($this->input->post('company_serial',TRUE));
		$password=urldecode($this->input->post('password',TRUE));
		$company_name=urldecode($this->input->post('company_name',TRUE));
		$representative_number=$this->input->post('representative_number',TRUE);
		$fax=$this->input->post('fax',TRUE);
		$postcode=$this->input->post('postcode',TRUE);
		$address=urldecode($this->input->post('address',TRUE));
		$start_time=$this->input->post('start_time',TRUE);
		$end_time=$this->input->post('end_time',TRUE);
		$delivery_max_count=$this->input->post('delivery_max_count',TRUE);
		$representative=urldecode($this->input->post('representative',TRUE));
		$representative_tel=$this->input->post('representative_tel',TRUE);
		$corporation_number=$this->input->post('corporation_number',TRUE);
		$cr_number=$this->input->post('cr_number',TRUE);
		
		$write_data = array (
			'email' => $email,
			'flag' => $select_status,
			'password'=>$password,
			'company_name'=>$company_name,
			'tel'=>$representative_number,
			'fax'=>$fax,
			'postcode'=>$postcode,
			'company_address'=>$address,
			// 'open_time_start'=>$start_time,
			// 'open_time_finish'=>$end_time,
			// 'delivery_max_per_hour'=>$delivery_max_count,
			'owner_name'=>$representative,
			'phone'=>$representative_tel,
			'corporate_license_number'=>$corporation_number,
			'register_number'=>$cr_number,
			);

		$result=$this->company->modify_company($company_serial, $write_data);
		echo json_encode($result);
	}

	function get_initial_info($company_serial) {
		$result = $this->company->get_company($company_serial);
		echo json_encode($result);
	}

	function save_setting(){

		$company_serial = $this->input->post('company_serial',TRUE);
		$start_time = $this->input->post('start_time',TRUE);
		$end_time = $this->input->post('end_time',TRUE);
		$max_count = $this->input->post('max_count',TRUE);
		$min_period = $this->input->post('min_period',TRUE);
		$max_period = $this->input->post('max_period',TRUE);

		$result = $this->company->save_time_and_delivery($company_serial, $start_time, $end_time, $max_count, $min_period, $max_period);
		echo json_encode($result);
	}

	function get_setting(){
		$company_serial = $this->input->post('company_serial',TRUE);
		$result = $this->company->get_setting($company_serial);
		echo json_encode($result);
	}




	function get_image($type, $company_serial) {

		$s3 = new Aws\S3\S3Client([
				    'version' => 'latest',
				    'region'  => 'ap-northeast-2',
				    'credentials' => [
				    	'key' => 'AKIAIMJGNFUPDENK47JA',
						'secret' => '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'
					]
		]);


		$refer = $_SERVER['HTTP_REFERER'];
		$filename= $this->company->get_image_file($company_serial, $type);

		$result = $s3->getObject([
			    'Bucket' => 'private.information.rengo',
			    'Key'    => $company_serial."/".$filename
		]);

		// // Print the body of the result by indexing into the result object.
		echo $result['Body'];
	}

	function get_password($company_serial) {
		$result=$this->company->password($company_serial);
		echo json_encode($result);
	}

	function file_upload() //이미지 업로드용
	{	

		$s3 = new Aws\S3\S3Client([
				    'version' => 'latest',
				    'region'  => 'ap-northeast-2',
				    'credentials' => [
				    	'key' => 'AKIAIMJGNFUPDENK47JA',
						'secret' => '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'
					]
		]);

		$company_serial = $this->input->post('company_serial',TRUE);
		$config['upload_path'] = '/home/apache/uploads/company/'.$company_serial."/";

		if(!is_dir($config['upload_path'])){
			$result = @mkdir($config['upload_path']);
			if($result == false){
				$error = array('error' => '폴더 생성 오류. 다시 시도해 주세요.');
				$this->index();
				return;
			}
		}

		$filename = array('business_regist','bankbook');

		$config['allowed_types'] = 'pdf|gif|jpg|png|jpeg';
		$config['max_size']	= '10000';
		$config['max_width']  = '1024000';
		$config['max_height']  = '768000';


		$upload_file_information['serial'] = $company_serial;

		foreach($filename as $fn){
			if($_FILES[$fn]['name'] == "" || $_FILES[$fn]['size'] < '1')continue;
			$temp = explode(".", $_FILES[$fn]['name']);
			$확장자 = $temp[(count($temp)-1)];
//			$확장자 = "jpg";
			$new_file = $config['upload_path'].$fn.'.'.$확장자;
//			$확장자 = "jpg";
			//파일 이름 암호화
			$encry_file_name = $this->rengo_encryption->encryption($fn);
			$new_file = $config['upload_path'].$encry_file_name.'.'.$확장자;


			if(is_file($new_file)){
		//			@unlink($new_file);
				@rename($new_file, $new_file."_".date("YmdHis"));
			}
			
			// $config['file_name'] = $fn.'.'.$확장자;
			$config['file_name'] = $encry_file_name.'.'.$확장자;
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload($fn)){
				$error = array('error' => $this->upload->display_errors());
			}	
			else {
				$data = array('upload_data' => $this->upload->data());
				$upload_file_information['file_'.$fn] = $config['file_name'];
				$this->company->register_company_file($upload_file_information);
				// Send a PutObject request and get the result object.
				$result = $s3->putObject([
						'Bucket' => 'private.information.rengo',
					    'Key'    => $company_serial."/".$config['file_name'],
						'Body'   => fopen( $config['upload_path'].$config['file_name'], 'r')
				 ]);
			}	
		}

		if($error){
			$this->index();
		} else {
			$this->index();
		}
	}
	function file_upload2() //이미지 업로드용
	{	
		$company_serial = $this->input->post('company_serial',TRUE);
		
		$config['upload_path'] = '/home/apache/uploads/company/'.$company_serial."/";
		if(!is_dir($config['upload_path'])){
			$result = @mkdir($config['upload_path']);
			if($result == false){
				$error = array('error' => '폴더 생성 오류. 다시 시도해 주세요.');

				$data['error'] = $error;
				$this->load->view("master/partnermanager_dialog/partner_setting_dialog", $data); 
				return;
			}
		}

		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '10000';
		$config['max_width']  = '1024000';
		$config['max_height']  = '768000';

		$filename = array('business_regist','bankbook');

		$this->load->library('upload');
		$upload_file_information['serial'] = $company_serial;

		foreach($filename as $fn){

			

			if($_FILES[$fn]['name'] == "" || $_FILES[$fn]['size'] < '1')continue;

			

			$uploaded_file = $config['upload_path'].$_FILES[$fn]['name'];
			$temp = explode(".", $_FILES[$fn]['name']);
			$확장자 = $temp[(count($temp)-1)];
//			$확장자 = "jpg";
			$new_file = $config['upload_path'].$fn.'.'.$확장자;


			if(is_file($new_file)){
		//			@unlink($new_file);
				@rename($new_file, $new_file."_".date("YmdHis"));
			}

		
			$config['file_name'] = $fn.'.'.$확장자;
			$this->upload->initialize($config);


			if ( ! $this->upload->do_upload($fn, $fn))
			{
				$error = array('error' => $this->upload->display_errors());
			}	
			else
			{
				$data = array('upload_data' => $this->upload->data());
				$upload_file_information['file_'.$fn] = $config['file_name'];

				$this->company->register_company_file($upload_file_information);
			}	
		}
		if($error){
			$data['error'] = $error;
			$this->load->view("master/partnermanager_dialog/partner_setting_dialog", $data); 
		} else {
			$data['company_serial'] = $company_serial;
			$this->load->view("master/partnermanager_dialog/partner_setting_dialog", $data); 
		}
	}
}
?>