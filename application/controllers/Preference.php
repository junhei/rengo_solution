<?php
class Preference extends CI_Controller {
	function __construct() { 
		parent::__construct();
		$this->load->model('admin/Admin_model', 'admin');
		$this->load->model('Company_model', 'company');
		$this->load->model('Fare_scheme_model', 'fare_scheme');

		// 관리자 메뉴 접근 퍼미션 체크

		$menu_permission = 4;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y")
			$this->admin->admin_logout(); 
		

		if($_SERVER['REMOTE_ADDR'] == "122.37.189.47"){
			//개발자 사무실 아이피면 프로필 표시
//			$this->output->enable_profiler(TRUE);
		}

	} 

	function index(){
		$this->_view("preference/fare_scheme_view");
	}

	function _view($url, $data = ''){
		$company_serial = $this->session->userdata('company_serial');
		$data['company_serial'] = $company_serial;
		$data['permission'] = $this->session->userdata('admin_permission');
		$data['admin_id'] = $this->session->userdata('admin_id');
		$data['company_name'] = $this->session->userdata('company_name');
		$data['company_list'] = $this->company->get_company($company_serial);
		$this->load->view("admin/admin_layout_top", $data); 
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}


	// function get_basic_price($company_serial){
	// 	$price_data_array = $this->fare_scheme->get_basic_price($company_serial);

	// 	if($price_data_array == null){
	// 		$price_data_array = array();
	// 		echo json_encode($price_data_array);
	// 	}else{
	// 		echo json_encode($price_data_array);
	// 	}

	// }
	function get_special_price($company_serial){
		
		$cartype_data_array = $this->fare_scheme->get_special_price($company_serial);

		foreach($cartype_data_array as $cartype_data){
			$str_date = $cartype_data["start_date"];
			$year = substr($str_date, 0,4);
			$month = substr($str_date,4,2);
			$day = substr($str_date,6,2);
			
			$aa = $cartype_data["end_date"];
			$end_year = substr($aa, 0,4);
			$end_month = substr($aa,4,2);
			$end_day = substr($aa,6,2);
			
			$cartype_send_data[] = array(
					"serial" => intval($cartype_data['serial']),
					"start_date" => $year."-".$month."-".$day,
					"end_date" => $end_year."-".$end_month."-".$end_day,
					"percent" => $cartype_data['percent'],
					"memo" =>  $cartype_data['memo']
				);
		}		

		
		
		if($cartype_data_array == null){
			$car_send_array = array();
			echo json_encode($car_send_array);
		}else{
			echo json_encode($cartype_send_data);
		}
	}

	function get_period_price($company_serial){
		$cartype_data_array = $this->fare_scheme->get_period_price($company_serial);
		
		if($cartype_data_array == null){
			$car_send_array = array();
			echo json_encode($car_send_array);
		}else{
			echo json_encode($cartype_data_array);
		}
		

	}

	function basic_price_add(){//등록
		
		$write_data = array(
		
			'company_serial' => intval($this->input->post('company_serial', TRUE)),
			'week_6' => intval($this->input->post('week_6', TRUE)),
			'week_1' => intval($this->input->post('week_1', TRUE)),
			'weekend_6' => intval($this->input->post('weekend_6', TRUE)),
			'weekend_1' => intval($this->input->post('weekend_1', TRUE))
			

		);

		$result = $this->fare_scheme->basic_price_add($write_data);
		echo json_encode($result);


	}

	// function basic_price_update(){
		
	// 	$company_serial = $this->session->userdata('company_serial');

	// 	$write_data = array(
		
	// 		'company_serial' => $company_serial,
	// 		'branch_serial' => $branch_serial, 
	// 		'week_6' => $this->input->post('week_6', TRUE),
	// 		'week_1' => $this->input->post('week_1', TRUE),
	// 		'weekend_6' => $this->input->post('weekend_6', TRUE),
	// 		'weekend_1' => $this->input->post('weekend_1', TRUE)
		
	// 	);
	// 	$result=$this->fare_scheme->basic_price_update($write_data);
	// 	echo json_encode($result);


		
	// }

	// function basic_price_delete(){
		
	// 	$company_serial = $this->session->userdata('company_serial');

	// 	$result=$this->fare_scheme->basic_price_delete($company_serial);
	// 	echo json_encode($result);


		
	// }
	function period_price_add(){//등록
		
		// $company_serial = $this->session->userdata('company_serial');
		$write_data = array(
		
			'company_serial' => $this->input->post('company_serial', TRUE),
			'days' => $this->input->post('days', TRUE),
			// 'memo' => $this->input->post('memo', TRUE),
			'percent' => $this->input->post('percent', TRUE)
			
		);

		$result = $this->fare_scheme->period_price_add($write_data);
		echo json_encode($result);


	}

	function period_price_update(){
		
		// $company_serial = $this->session->userdata('company_serial');
		$serial = intval($this->input->post('serial', TRUE));
		$write_data = array(
			'company_serial' => $this->input->post('company_serial', TRUE),
			'days' => $this->input->post('days', TRUE),
			'percent' => $this->input->post('percent', TRUE)
		);


		$result=$this->fare_scheme->period_price_update($serial, $write_data);
		echo json_encode($result);
	}


	function period_price_delete(){
		
		// $company_serial = $this->session->userdata('company_serial');

		$write_data = array(
		
			// 'company_serial' => $company_serial,
			'serial' => $this->input->post('serial', TRUE)
			// 'days' => $this->input->post('days', TRUE)
		
		);
		$result=$this->fare_scheme->period_price_delete($write_data);
		echo json_encode($result);


		
	}
	function special_price_add(){//등록
		$start_date = $this->input->post('start_date', TRUE);
		$start_date = date("Ymd",strtotime($start_date))."0000";

		$end_date = $this->input->post('end_date', TRUE);
		$end_date = date("Ymd",strtotime($end_date))."2400";

		// $company_serial = $this->session->userdata('company_serial');
		$write_data = array(
		
			'company_serial' => $this->input->post('company_serial', TRUE),
			// 'start_date' => $this->input->post('start_date', TRUE),
			'start_date' => $start_date,
			'end_date' => $end_date,
			'memo' => $this->input->post('memo', TRUE),
			'percent' => $this->input->post('percent', TRUE)
		
		);


		$result = $this->fare_scheme->special_price_add($write_data);
		echo json_encode($result);


	}

	function special_price_update(){
		
		// $company_serial = $this->session->userdata('company_serial');
		// $test = $this->input->post('start_date', TRUE);
		$start_date = $this->input->post('start_date', TRUE); 
		$year = substr($start_date, 0,4);
		$month = substr($start_date,5,2);
		$day = substr($start_date,8,2);
		
		$end_date = $this->input->post('end_date', TRUE);
		$end_year = substr($end_date, 0,4);
		$end_month = substr($end_date,5,2);
		$end_day = substr($end_date,8,2);

		$serial =  $this->input->post('serial', TRUE);

		$write_data = array(
			'company_serial' => $this->input->post('company_serial', TRUE),
			'start_date' => $year.$month.$day."0000", 
			'end_date' => $end_year.$end_month.$end_day."2400",
			'memo' => $this->input->post('memo', TRUE),
			'percent' => $this->input->post('percent', TRUE)
			
		
		);
		// echo $write_data['start_date'];
		// die();
		// echo json_encode($write_data);
		// die();
		$result=$this->fare_scheme->special_price_update($serial, $write_data);
		echo json_encode($result);
	}

	function special_price_delete(){
		
		// $company_serial = $this->session->userdata('company_serial');

		$write_data = array(
			'serial' => $this->input->post('serial', TRUE)
		);
		$result=$this->fare_scheme->special_price_delete($write_data);
		echo json_encode($result);


		
	}
	

	
	

}
?>