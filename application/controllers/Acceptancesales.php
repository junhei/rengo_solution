<?php
class Acceptancesales extends CI_Controller {
	private $template_file = "admin/index";
	private $admin_function, $request_uri;

	function __construct() { 
		parent::__construct();
	    $this->load->model('admin/Admin_model', 'admin');
	    $this->load->model('Salemanager_model','salemanager');
	    $this->load->model('Company_model','company');
  		$menu_permission = 3;
  		$permission = $this->admin->_check_permission($menu_permission);
  		if($permission != "Y")
   		$this->admin->admin_logout(); 

	} 
	function _view($url, $data = ''){
		$data['permission'] = $this->session->userdata('admin_permission');
  		$data['admin_id'] = $this->session->userdata('admin_id');
		$this->load->view("admin/admin_layout_top", $data); 
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$company_serial = $this->session->userdata('company_serial');
		
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$data['company_list'] = $this->company->get_company($company_serial);
		$this->_view("sale/acceptance_sales_view", $data); 
	}

}
?>