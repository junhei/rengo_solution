<?php
class Salesetting extends CI_Controller {


	function __construct() { 

		parent::__construct();

		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		
		$this->load->model('admin/Admin_model', 'admin');
		$this->load->model('Makermanager_model','makermanager');
		$this->load->model('Company_model','company');
		$this->load->model('Area_model','area');
		$this->load->model('Preference_model','preference');
		// 관리자 메뉴 접근 퍼미션 체크
		$menu_permission = 4;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y"){
			echo "sdafjlkdsjlfkjsadlkfjklsdajflksajdlkfjsald";
			// $this->admin->admin_logout(); 
		}

	

//			$this->output->enable_profiler(TRUE);
	}

	function _view($url, $data = ''){

		$data['admin_id'] = $this->session->userdata('admin_id');
		$data['city_list'] = $this->company->get_city();
		$this->load->view("admin/admin_layout_top", $data);
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$company_serial = $this->session->userdata('company_serial');
		$data['company_name'] = $this->session->userdata('company_name');
		$data['company_serial'] = $company_serial;
		$data['permission'] = $this->session->userdata('admin_permission');
		$data['admin_name'] = $this->admin->get_admin_name();
		$data['company_list'] = $this->company->get_company($company_serial);
		$this->_view("preference/sale_setting_view", $data); 
	}

	function get_start_end_time($company_serial){
		echo json_encode($this->company->get_start_end_time($company_serial));
	}

	function save_time_and_delivery(){
		$company = intval($this->input->post('company_serial', TRUE));
		$start_time = urldecode($this->input->post('start_time', TRUE));
		$end_time = urldecode($this->input->post('end_time', TRUE));
		$max = intval($this->input->post('max', TRUE));

		echo json_encode($this->company->save_time_and_delivery($company, $start_time, $end_time, $max));

	}


	//도시 얻기
	function get_subway_city(){
		echo json_encode($this->area->get_subway_city());
	}

	function get_spot_city(){
		echo json_encode($this->area->get_spot_city());
	}

	// function get_delivery_subway($company_serial){
	// 	echo json_encode($this->area->get_delivery_subway($company_serial));
	// }

	function get_spots($city, $company_serial){
		echo json_encode($this->area->get_selected_spot_by_city($city, $company_serial));
	}

	function get_delivery_subway_by_city($city, $company_serial){
		echo json_encode($this->area->get_delivery_subway_by_city($city, $company_serial));
	}

	function get_delivery_max_per_hour($company_serial){
		echo json_encode($this->company->get_delivery_max_per_hour($company_serial));
	}

	

	// function get_subway_information($city_name) {
	// 	$city_name=urldecode($city_name);
	// 	$lines=$this->preference->line_information($city_name);
	// 	$stations=$this->preference->station_information($city_name);

	// 	$ret=$lines;
	// 	foreach($lines as $li) {

	// 		// if(isset($temp)) unset($temp);
	// 		foreach($stations as $st) {

	// 			if($li == $st['area_kind']){
	// 				// array_push($temp,$st['location_name_kor']);
	// 			}
	// 		}
	// 		// print_r($temp);
	// 		// $ret[$li]=$temp;
	// 		// echo json_encode($ret[$li]);
	// 	}	

	// 	echo json_encode($ret);
	// }

	function save_subway(){
		$company = intval($this->input->post('company_serial', TRUE));
		$city = urldecode($this->input->post('city', TRUE));
		$subways = $this->input->post('subways', TRUE);

		echo json_encode($this->area->save_subway($company, $city, $subways));
	}

	function save_spot(){
		$company = intval($this->input->post('company_serial', TRUE));
		$city = urldecode($this->input->post('city', TRUE));
		$spots = $this->input->post('spot', TRUE);

		echo json_encode($this->area->save_spot($company, $city, $spots));
	}

	function save_delivery_max(){
		$company = intval($this->input->post('company_serial', TRUE));
		$max = intval($this->input->post('max', TRUE));

		echo json_encode($this->company->save_delivery_max_per_hour($company, $max));
	}

}
?>