<?php
class Partner_dialog extends CI_Controller {

	function __construct()
	 { 
		parent::__construct();

	    $this->load->model('admin/Admin_model', 'admin');
  		$menu_permission = 5;
  		$permission = $this->admin->_check_permission($menu_permission);
  		if($permission != "Y")
	   		$this->admin->admin_logout(); 

	} 

	function _view($url, $data = '')
	{
		$data['permission'] = $this->session->userdata('admin_permission');
  		$data['admin_id'] = $this->session->userdata('admin_id');
		$this->load->view($url, $data); 
	}

	function index()
	{
		$data['company_serial'] = $this->session->userdata('company_serial');
		$data['company_name'] = $this->session->userdata('company_name');
		$this->_view("master/partnermanager_dialog/partner_setting_dialog", $data); 
		
	}

	function partnersetting($company_serial = '0'){
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$this->_view("master/partnermanager_dialog/partner_setting_dialog", $data); 
	}

	function salesetting($company_serial = '0'){
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$this->_view("master/partnermanager_dialog/sale_setting_dialog", $data); 
	}

	function fairsetting($company_serial = '0'){
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$this->_view("master/partnermanager_dialog/fair_setting_dialog", $data); 
	}

	function staffmanager($company_serial = '0'){
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$this->_view("master/partnermanager_dialog/staff_manager_dialog", $data); 
	}

	function areasetting($company_serial = '0'){
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$this->_view("master/partnermanager_dialog/area_setting_dialog", $data); 
	}


}
