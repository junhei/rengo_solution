<?
$admin_id = $this->input->post('admin_id');
$admin_pw = $this->input->post('admin_pw');
$admin_level = $this->input->post('admin_level');
$admin_name = $this->input->post('admin_name');
$permission = $this->input->post('permission');
$company_serial = $this->input->post('company_serial');
$serial = $this->input->post('serial');

$data['po_admin_id'] = $admin_id;
$data['po_admin_pw'] = $admin_pw;
$data['po_admin_level'] = $admin_level;
$data['po_admin_name'] = $admin_name;
$data['po_permission'] = $permission;
$data['po_company_serial'] = $company_serial;
$data['po_serial'] = $serial;

if($admin_id && $admin_pw && $admin_level && $admin_name){

	$sql = "
	SELECT 
		*
	FROM 
		admin_information
	WHERE 
		admin_id = '".$admin_id."'
	";
	$result = $this->db->fReadSql($sql);
	$permission_value = '';
	for($i=0;$i<$permission_count;$i++){
		if(@$permission[$i] == "Y")
			$permission_value .= "Y";
		else
			$permission_value .="N";
	}

	// 관리자 아이디 정보 추가
	if($result[0]['serial'] >= '1'){
		$data['error_message']['admin_id'] = "이미 있는 아이디입니다.";
	}
	else {
		// 관리자 아이디의 serial 번호가 없다면 디비에 추가
		$this->db->flush_cache();
		$this->db->set('admin_id', $admin_id);
		$this->db->set('admin_pw', $admin_pw);
		$this->db->set('admin_level', $admin_level);
		$this->db->set('admin_name', $admin_name);
		$this->db->set('admin_flag', 'Y');
		$this->db->set('company_serial', '0');
		$this->db->set('permission', $permission_value);
		$this->db->set('company_serial', $company_serial);
		$this->db->set('registered_date', 'now()', FALSE);
		$this->db->set('registered_ip', $_SERVER['REMOTE_ADDR']);
		$this->db->set('edited_date', 'now()', FALSE);
		$this->db->set('edited_ip', $_SERVER['REMOTE_ADDR']);
		$this->db->insert('admin_information');

//		redirect('/admin/list/');
	}
} else if($admin_level >= '1'){
	if(!$admin_id){
		$data['error_message']['admin_id'] = "입력해 주세요.";
	}
	if(!$admin_pw){
		$data['error_message']['admin_pw'] = "입력해 주세요.";
	}
	if(!$admin_name){
		$data['error_message']['admin_name'] = "입력해 주세요.";
	}
}
?>