<?
$sql = "
SELECT 
	serial,
	admin_id,
	admin_pw,
	admin_level,
	admin_name,
	permission,
	company_serial,
	registered_date,
	registered_ip,
	edited_date,
	edited_ip
FROM 
	admin_information
ORDER BY 
	admin_id 
";
$data['result'] = $this->db->fReadSql($sql);
$data['row_count'] = count($data['result']);
$data['permission'] = $this->session->userdata('admin_permission');
$data['admin_id'] = $this->session->userdata('admin_id');
$data['company_name'] = $this->session->userdata('company_name');
?>