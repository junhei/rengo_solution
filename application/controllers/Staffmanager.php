<?php
class Staffmanager extends CI_Controller {


	function __construct() { 

		parent::__construct();
		$this->load->model('admin/Admin_model', 'admin');
		$this->load->model('Staffmanager_model','staffmanager');
		$this->load->model('Company_model','company');
		// 관리자 메뉴 접근 퍼미션 체크
		$menu_permission = 5;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y")
			$this->admin->admin_logout(); 
	}

	function _view($url, $data = ''){

		$data['admin_id'] = $this->session->userdata('admin_id');
		$this->load->view("admin/admin_layout_top", $data);
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$company_serial = $this->session->userdata('company_serial');
		$data['permission'] = $this->session->userdata('admin_permission');
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$data['admin_level'] = $this->session->userdata('admin_level');
		$data['company_list'] = $this->company->get_company($company_serial);
		$this->_view("preference/staff_manager_view", $data); 


	}


	function get_list($company_serial){

		if($company_serial == 0){
			return json_encode(array());
		}
		$cartype_data_array = $this->staffmanager->get_list($company_serial);

		foreach($cartype_data_array as $cartype_data){
			$check_rent = substr($cartype_data['permission'], 0,1);
			if($check_rent == "Y"){
				$check_rent = "O";
			}else{
				$check_rent = "X";
			}

			$check_car = substr($cartype_data['permission'], 1,1);
			if($check_car == "Y"){
				$check_car = "O";
			}else{
				$check_car = "X";
			}

			$check_customer = substr($cartype_data['permission'], 2,1);
			if($check_customer == "Y"){
				$check_customer = "O";
			}else{
				$check_customer = "X";
			}

			$check_sales = substr($cartype_data['permission'], 3,1);
			if($check_sales == "Y"){
				$check_sales = "O";
			}else
			{
				$check_sales = "X";
			}

			$check_rengo = substr($cartype_data['permission'], 4,1);
			if($check_rengo == "Y"){
				$check_rengo = "O";
			}else
			{
				$check_rengo = "X";
			}

			$check_preference = substr($cartype_data['permission'], 5,1);
			if($check_preference == "Y"){
				$check_preference = "O";
			}else
			{
				$check_preference = "X";
			}

			$car_send_array[] = array(
					"admin_id" => $cartype_data['admin_id'],
					"admin_name" => $cartype_data['admin_name'],
					"permission" => $cartype_data['permission'],
					"phone_number" => $cartype_data['phone_number'],
					"serial" => $cartype_data['serial'],
					"check_rent" => $check_rent,
                    "check_car" => $check_car,
                    "check_customer" => $check_customer,
                    "check_sales" => $check_sales,
                    "check_rengo" => $check_rengo,
                    "check_preference" => $check_preference,
				);
		}

		if($car_send_array == null){
			$car_send_array = array();
		}

		echo json_encode($car_send_array);
		
// echo $car_send_array;
// 		die();

	}

	function get_count(){
		$company_serial = $this->input->post('company_serial', TRUE);
		$result = $this->staffmanager->get_count($company_serial);
		echo json_encode($result);
	}
	
	function add(){//등록
		
		$check_rent = $this->input->post('check_rent', TRUE);
		$check_car = $this->input->post('check_car', TRUE);
		$check_customer = $this->input->post('check_customer', TRUE);
		$check_sales = $this->input->post('check_sales', TRUE);
		$check_rengo = $this->input->post('check_rengo', TRUE);
		$check_preference = $this->input->post('check_preference', TRUE);
		// $check_setting = "N";
		// $check_master = "N";

		$write_data = array(
			'company_serial' => $this->input->post('company_serial', TRUE),
			'admin_id' => $this->input->post('admin_id', TRUE),
			'admin_name' => $this->input->post('admin_name', TRUE),
			'permission' => $check_rent.$check_car.$check_customer.$check_sales.$check_rengo.$check_preference,
			'phone_number'	=> $this->input->post('phone_number', TRUE)
		);

		$admin_pw = $this->input->post('admin_pw', TRUE);
		$result = $this->staffmanager->add($write_data, $admin_pw);
		echo json_encode($result);

	}
	function delete(){
		
		$write_data = array(
			'serial' => $this->input->post('serial', TRUE)
		);
		$result=$this->staffmanager->delete($write_data);
		echo json_encode($result);
		
}

	function update(){
		$serial = $this->input->post('serial', TRUE);
		$check_rent = $this->input->post('check_rent', TRUE);
		$check_car = $this->input->post('check_car', TRUE);
		$check_customer = $this->input->post('check_customer', TRUE);
		$check_sales = $this->input->post('check_sales', TRUE);
		$check_rengo = $this->input->post('check_rengo', TRUE);
		$check_preference = $this->input->post('check_preference', TRUE);

		// $company_serial = $this->session->userdata('company_serial');

		if($serial < '1'){
			$response['code'] ="E01";
			$response['message'] = "직원아이디를 선택해 주세요.";
			echo json_encode($response);
			die();
		}

		$write_data = array(
		
			// 'company_serial' => $company_serial,
			'serial' => $this->input->post('serial', TRUE),
			'admin_name' => $this->input->post('admin_name', TRUE),
			'permission' => $check_rent.$check_car.$check_customer.$check_sales.$check_rengo.$check_preference,
			'phone_number'	=> $this->input->post('phone_number', TRUE)
		);
		// echo json_encode($write_data);
		// die();
		$admin_pw = $this->input->post('admin_pw', TRUE);
		$result = $this->staffmanager->update($write_data, $admin_pw);
		echo json_encode($result);
		
	}

}
?>