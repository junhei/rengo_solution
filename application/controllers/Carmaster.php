<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 require FCPATH.'vendor/autoload.php';

class Carmaster extends CI_Controller {


	function __construct() { 

		parent::__construct();
		$this->load->model('admin/Admin_model', 'admin');
		$this->load->model('Carmaster_model','carmaster');
		$this->load->model('Makermanager_model','makermanager');
		$this->load->library('Rengo_encryption');
		$this->load->library('upload');
		
		// 관리자 메뉴 접근 퍼미션 체크
		$menu_permission = 6;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y")
			$this->admin->admin_logout(); 

	// error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

//			$this->output->enable_profiler(TRUE);
	}

	function _view($url, $data = ''){

		$data['admin_id'] = $this->session->userdata('admin_id');
		$data['company_name'] = $this->session->userdata('company_name');
		$data['permission'] = $this->session->userdata('admin_permission');
		$this->load->view("admin/admin_layout_top", $data);
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$data['makermanager_list'] = $this->makermanager->get_active_list();
		$this->_view("master/car_master_view", $data); 
	}


	function get_list(){
		$cartype_data_array = $this->carmaster->get_list();
		foreach($cartype_data_array as $cartype_data){
			// if($cartype_data['fuel_option_gasoline']=='Y'){
			// 	$gasoline = "Y";
			// }else{
			// 	$gasoline = "N";
			// }
			// if($cartype_data['fuel_option_diesel']=='Y'){
			// 	$diesel = "Y";
			// }else{
			// 	$diesel = "N";
			// }
			// if($cartype_data['fuel_option_lpg']=='Y'){
			// 	$lpg = "Y";
			// }else{
			// 	$lpg = "N";
			// }
			// if($cartype_data['fuel_option_hybrid']=='Y'){
			// 	$hybrid = "Y";
			// }else{
			// 	$hybrid = "N";
			// }
			// if($cartype_data['fuel_option_electricity']=='Y'){
			// 	$electricity = "Y";
			// }else{
			// 	$electricity = "N";
			// }
			if($cartype_data['flag']=='Y'){
				$status = "사용";
			}else{
				$status = "중지";
			}

			$car_send_array[] = array(
					"car_index" => intval($cartype_data['car_index']),
					"maker_index" => $cartype_data['maker_index'],
					"maker_name" => $cartype_data['maker_name'],
					"car_list_image_file" => "https://s3.ap-northeast-2.amazonaws.com/image.rengo.co.kr/car/".$cartype_data['car_index']."/".$cartype_data['car_list_image_file'],
					"car_detail_image_file" => "https://s3.ap-northeast-2.amazonaws.com/image.rengo.co.kr/car/".$cartype_data['car_index']."/".$cartype_data['car_detail_image_file'],
					"car_type" => $cartype_data['car_type'],
					"car_name" => $cartype_data['car_name'],
					"car_name_detail" => $cartype_data['car_name_detail'],
					// "car_people" => intval($cartype_data['car_people']),
					"car_normal_price" => intval($cartype_data['car_normal_price']),
					// "fuel_option_gasoline" => $gasoline,
					// "fuel_option_diesel" => $diesel,
					// "fuel_option_lpg" => $lpg,
					// "fuel_option_hybrid" => $hybrid,
					// "fuel_option_electricity" => $electricity,
					"status" => $status
				);
		}
		

		if($car_send_array == null){
			$car_send_array = array();
		}

		echo json_encode($car_send_array);
		$car_index = "index";
// echo $car_send_array;
// 		die();

	}

	function register(){//등록
		if(!$this->input->post('car_name', TRUE)){
			alert('차량명을 입력해 주세요.');
			exit;
		}

		$status = $this->input->post('status', TRUE);
		$flag = "N";
		if($status == "사용" || $status == "사용중"){
			$flag = "Y";
		}else{
			$flag = "N";
		}
		
		// $gasoline = $this->input->post('fuel_option_gasoline', TRUE);
		//  $fuel_option_gasoline = "N";
		//  if($gasoline == "Y"){
		//  $fuel_option_gasoline = "Y";
		//  }else{
		//  	 $fuel_option_gasoline = "N";
		//  }


		$write_data = array(
			'maker_index' => $this->input->post('maker_index',TRUE),
			'car_name' => $this->input->post('car_name', TRUE),
			'car_name_detail' => $this->input->post('car_name_detail', TRUE),
			'car_type' => $this->input->post('car_type', TRUE),
			// 'fuel_option_gasoline' => $this->input->post('fuel_option_gasoline', TRUE),
			// 'fuel_option_diesel' => $this->input->post('fuel_option_diesel', TRUE),
			// 'fuel_option_lpg' => $this->input->post('fuel_option_lpg', TRUE),
			// 'fuel_option_hybrid' => $this->input->post('fuel_option_hybrid', TRUE),
			// 'fuel_option_electricity' => $this->input->post('fuel_option_electricity', TRUE),
			// 'car_people' =>$this->input->post('car_people',TRUE),
			'car_normal_price' =>$this->input->post('car_normal_price',TRUE),
			'flag' => $flag,			
			// 'status' =>$this->input->post($status,TRUE)
			'car_list_image_file' => $this->input->post('car_list_image_file', TRUE),
			'car_detail_image_file' => $this->input->post('car_detail_image_file', TRUE)
			// 'flag' => $flag

		);

		
		$result = $this->carmaster->add($write_data);
		echo json_encode($result);


	}
	//

	function update(){//수정

		
		if(!$this->input->post('car_name', TRUE)){
			alert('차량명을 입력해 주세요.');
			exit;
		}

		$status = $this->input->post('status', TRUE);
		$flag = "N";
		if($status == "사용" || $status == "사용중"){
			$flag = "Y";
		}else{
			$flag = "N";
		}

		$write_data = array(
			'maker_index' => $this->input->post('maker_index',TRUE),
			'car_name' => $this->input->post('car_name', TRUE),
			'car_name_detail' => $this->input->post('car_name_detail', TRUE),
			'car_type' => $this->input->post('car_type', TRUE),
			// 'fuel_option_gasoline' => $this->input->post('fuel_option_gasoline', TRUE),
			// 'fuel_option_diesel' => $this->input->post('fuel_option_diesel', TRUE),
			// 'fuel_option_lpg' => $this->input->post('fuel_option_lpg', TRUE),
			// 'fuel_option_hybrid' => $this->input->post('fuel_option_hybrid', TRUE),
			// 'fuel_option_electricity' => $this->input->post('fuel_option_electricity', TRUE),
			// 'car_people' =>$this->input->post('car_people',TRUE),
			'car_normal_price' =>$this->input->post('car_normal_price',TRUE),
			'flag' => $flag,			
			// 'status' =>$this->input->post($status,TRUE),
			'car_list_image_file' => $this->input->post('car_list_image_file', TRUE),
			'car_detail_image_file' => $this->input->post('car_detail_image_file', TRUE)
		);
		$car_index=$this->input->post('car_index', TRUE);
		// alert($car_index);
		$result = $this->carmaster->update($car_index,$write_data);
		echo json_encode($result);
	}

	function delete(){

		$car_index=$this->input->post('car_index',TRUE);
		// alert($car_index);
		
		$result=$this->carmaster->delete($car_index);
		echo json_encode($result);


		// if(!$this->input->post('index', TRUE)){
		// 	alert('제조사 id가 없습니다.');
		// 	exit;
		// }
		
		// $result = $this->carmaster->delete($this->input->post($car_index, TRUE));
		// echo json_encode($result);
	}

// 	function do_upload() //작은 이미지 업로드용
// 	{

// 		$dirRoot = $_SERVER["DOCUMENT_ROOT"];
// 		$car_index = $this->input->post('car_index', TRUE);

// 		$filename = $_FILES['car_list_image_file']['name'];
// 		$ext = explode(".", strtolower($filename)); 
// 		$cnt = count($ext)-1; 

//  // 		if($ext[$cnt] === ""){ 
//  //    	if(@ereg($ext[$cnt-1], "php|php3|php4|htm|inc|html")){ 
//  //        echo "죄송합니다. php, html 파일은 업로드가 제한됩니다."; 
//  //    } 
//  // } else if(@ereg($ext[$cnt], "php|php3|php4|htm|inc|html")){ 
//  //        echo "죄송합니다. php, html 파일은 업로드가 제한됩니다."; 
//  // } 

				
// 		if(!is_dir($dirRoot."/car/"))@mkdir($dirRoot."/car/");
// 		if(!is_dir($dirRoot."/car/".$car_index))@mkdir($dirRoot."/car/".$car_index);
		
				


// 		if($_FILES['car_list_image_file']['name'] && $_FILES['car_list_image_file']['size'] >= '1'){
// 			if(!copy($_FILES['car_list_image_file']['tmp_name'], $dirRoot."/car/".$car_index."/"."l".$car_index.".".$ext[$cnt])){
// 				$_error_file_upload_list_image = "error";


// 			}

// 			if($_error_file_upload_list_image == "error"){
// 				echo "리스트 이미지 에러";
// 				die();
// 			}

// 			$sql_list_image_file = "car_list_image_file = '".$_FILES['car_list_image_file']['name']."'";
// 				alert($sql_list_image_file);

// 			$sql = "
// 			UPDATE
// 				car_master
// 			SET
// 				".$sql_list_image_file."
// 			WHERE
// 				car_index = '".intval($car_index)."'
// 			";
// 			mysql_query($sql);

// 		}


// 		if($_FILES['car_detail_image_file']['name'] && $_FILES['car_detail_image_file']['size'] >= '1'){
// 			if(!copy($_FILES['car_list_image_file']['tmp_name'], $dirRoot."/car/".$car_index."/"."d".$car_index.".".$ext[$cnt])){
// 				$_error_file_upload_detail_image = "error";
// 			}

// 			if($_error_file_upload_detail_image == "error"){
// 				echo "상세 이미지 에러";
// 				die();
// 			}


// 			$sql_detail_image_file = "car_detail_image_file = '".$_FILES['car_detail_image_file']['name']."'";
// 			$sql = "
// 			UPDATE
// 				car_master
// 			SET
// 				".$sql_detail_image_file."
// 			WHERE
// 				car_index = '".intval($car_index)."'
// 			";
// 			mysql_query($sql);

// 		}
// 		// echo json_encode("upload_success");
// 			$data['makermanager_list'] = $this->makermanager->get_list();
// // echo $data;
// // die();
// 		$this->_view("car/car_master_view", $data); 
// // 			 if($ext == "png"){ 
    
// //  } else{
// //     alert ("죄송합니다. png파일만 등록 가능합니다."); 
// // $this->load->view('car_master_view');
// //   }


// 	}

	function file_upload() //이미지 업로드용
	{	

		$s3 = new Aws\S3\S3Client([
				    'version' => 'latest',
				    'region'  => 'ap-northeast-2',
				    'credentials' => [
				    	'key' => 'AKIAIMJGNFUPDENK47JA',
						'secret' => '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'
					]
		]);

		$car_serial = $this->input->post('car_serial',TRUE);	
		$config['upload_path'] = '/home/apache/uploads/car/'.$car_serial."/";

		if(!is_dir($config['upload_path'])){
			$result = @mkdir($config['upload_path']);
			if($result == false){
				echo '폴더 생성 오류. 다시 시도해 주세요.';
				$error = array('error' => '폴더 생성 오류. 다시 시도해 주세요.');
				// $this->index();
				return;
			}
		}

		$filename = array('car_list_image','car_detail_image');

		$config['allowed_types'] = 'pdf|gif|jpg|png|jpeg';
		$config['max_size']	= '10000';
		$config['max_width']  = '1024000';
		$config['max_height']  = '768000';

		$upload_file_information['serial'] = $car_serial;

		$now_temp = date('YmdHis');

		foreach($filename as $fn){


			if($_FILES[$fn]['name'] == "" || $_FILES[$fn]['size'] < '1')continue;
			$uploaded_file = $config['upload_path'].$_FILES[$fn]['name'];
			$temp = explode(".", $_FILES[$fn]['name']);
			$확장자 = $temp[(count($temp)-1)];

			if($fn=='car_list_image'){
				$car_file_name = "l".sprintf("%03d",$car_serial)."_".$now_temp;
			}else{
				$car_file_name = "d".sprintf("%03d",$car_serial)."_".$now_temp;
			}
			$new_file = $config['upload_path'].$car_file_name.'.'.$확장자;


			if(is_file($new_file)){
				@rename($new_file, $new_file."_".$now_temp);
			}

			$config['file_name'] = $car_file_name.'.'.$확장자;
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload($fn)) {
				$error = array('error' => $this->upload->display_errors());
			}	
			else {
				$data = array('upload_data' => $this->upload->data());
				$upload_file_information[$fn] = $config['file_name'];

				$this->carmaster->save_image_file($upload_file_information);
				// Send a PutObject request and get the result object.
				$result = $s3->putObject([
						'Bucket' => 'image.rengo.co.kr',
					    'Key'    => "car/".$car_serial."/".$config['file_name'],
						'Body'   => fopen( $config['upload_path'].$config['file_name'], 'r')
				 ]);
			}	
		}
?>
<script language="javascript">
top.location.replace('/carmaster');
</script>
<?
//		 $this->index();
	}


}
?>