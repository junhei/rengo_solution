<?php
class Admin extends CI_Controller {
	private $template_file = "admin/login_view";
	private $request_uri;

	function __construct() { 
		parent::__construct();
		$this->load->model('admin/Admin_model', 'admin');

		$menu_permission = 6;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y")
			$this->admin->admin_logout(); 
		// 관리자 메뉴 접근 퍼미션 체크

//			$this->output->enable_profiler(TRUE);

		if($_SERVER['REMOTE_ADDR'] == "122.37.189.47"){
			//개발자 사무실 아이피면 프로필 표시
//			$this->output->enable_profiler(TRUE);
		}

	} 

	function index($mode = '', $option = ''){
		$data['admin_function'] = $this->admin->get_admin_function();
		$permission_count = count($data['admin_function']);

		switch($mode){
			case "logout":
				$this->session->sess_destroy(); 
				if($_GET['back_url']){
					redirect($_GET['back_url']); 
					die();
				}
				redirect('/'); 
				die();
				break;
			case "list":
				// ADMIN 리스트
			case "edit":
				// ADMIN 수정
//				if(intval($option) < '1')
//					redirect("/admin/list");
				$partner_status_flag = "Y";
				$data['company_list'] = $this->admin->get_partner_list($partner_status_flag);
			case "add":
				// ADMIN 추가
				$this->template_file = "admin/admin_".$mode;
				include("admin/admin_".$mode.".php");
				break;

			default :
				HEADER("Location: /rent");
				die();
				break;
		}
		$this->_view($data);
	}

	function _view($data = ''){
		$data['permission'] = $this->session->userdata('admin_permission');
		$data['admin_name'] = $this->admin->get_admin_name();
		$this->load->view("admin/admin_layout_top", $data); 
		// $this->load->view("admin/admin_status", $data); 
		// $this->load->view("admin/layout_left", $data); 
		$this->load->view($this->template_file, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

}
?>