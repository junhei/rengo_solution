<?php
class Areamanager extends CI_Controller {


	function __construct() { 

		parent::__construct();
		$this->load->model('admin/Admin_model', 'admin');
		$this->load->model('Company_model', 'company');
		$this->load->model('Area_model','area');
		// 관리자 메뉴 접근 퍼미션 체크
		$menu_permission = 6;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y")
			$this->admin->admin_logout(); 
	}

	function _view($url, $data = ''){
		$company_serial = $this->session->userdata('company_serial');
		$data['admin_id'] = $this->session->userdata('admin_id');
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$data['permission'] = $this->session->userdata('admin_permission');
		$data['company_list'] = $this->company->get_company($company_serial);
		$this->load->view("admin/admin_layout_top", $data);
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$data['company_name'] = $this->session->userdata('company_name');
		
		$this->_view("master/areamanager_view", $data); 

	}

	function get_all_city(){
		echo json_encode($this->area->get_all_city());


	}

	function get_gu($city){
		echo json_encode($this->area->get_gu_by_city(urldecode($city)));
	}

	function get_place_by_city($city){
		echo json_encode($this->area->get_place_by_city($city));
	}

	function get_sub_area($city){
		echo json_encode($this->area->get_sub_area($city));
	}

	function save_special_place(){

		$write_data = array(
			'area_kind' => "spot ".urldecode($this->input->post('city_name', TRUE)),
			'city_name' => urldecode($this->input->post('city_name', TRUE)),
			'area_name' => urldecode($this->input->post('sub_name', TRUE)),
			'location_name_kor' => urldecode($this->input->post('location_name_kor', TRUE)),
			'flag' => 'Y'
		);
		echo json_encode($this->area->save_etc_place($write_data));
	}

	function delete_special_place(){

		$area_list = $this->input->post('area_list', TRUE);
		echo json_encode($this->area->delete_etc_place($area_list));
	}

	function get_counts(){
		$company_serial = $this->input->post('company_serial', TRUE);
		echo json_encode($this->area->get_count($company_serial));
	}


	






}
?>