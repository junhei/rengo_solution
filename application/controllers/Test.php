<?php
class Test extends CI_Controller {


	function __construct() { 

		parent::__construct();
		$this->load->model('Usermanager_model','user');
	}

	function index(){
		$this->load->view("admin/admin_layout_bottom");
	}


	
	function a(){
		echo 'a';
	}

function search_user(){
		try{
			$user_name = urldecode($this->input->post('user_name',TRUE));
			$company_serial = urldecode($this->input->post('company_serial',TRUE));

			if($company_serial=='' ){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			if($user_name == null || $user_name == ''){
				$user_name = '';
			}

			$result = $this->user->m_get_user($company_serial, $user_name);
			$response['code'] = 'S01';
			$response['value'] = $result;
			echo json_encode($response);

		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}
	}


}
?>