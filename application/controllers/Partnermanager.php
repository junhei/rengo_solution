<?php
class Partnermanager extends CI_Controller {
	private $template_file = "admin/index";
	private $admin_function, $request_uri;

	function __construct()
	 { 
		parent::__construct();

	    $this->load->model('admin/Admin_model', 'admin');
	    $this->load->model('Company_model','company');
	    $this->load->model('Partnermanager_model','partnermanager');
	    $this->load->library('Rengo_encryption');
  		$menu_permission = 6;
  		$permission = $this->admin->_check_permission($menu_permission);
  		if($permission != "Y")
	   		$this->admin->admin_logout(); 

	} 

	function _view($url, $data = '')
	{
		$data['permission'] = $this->session->userdata('admin_permission');
  		$data['admin_id'] = $this->session->userdata('admin_id');
  		$data['company_serial'] = $this->session->userdata('company_serial');
		$this->load->view("admin/admin_layout_top", $data); 
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index()
	{
		$company_serial = $this->session->userdata('company_serial');
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
	
		$data['company_list'] = $this->company->get_company($company_serial);
		$this->_view("master/partner_manager_view", $data); 
		
	}


	function get_partner_information($partner_serial='') {
		$partner_info=$this->partnermanager->partner_information($partner_serial);
		$result_area=$this->partnermanager->service_area($partner_serial);

		for($i=0;$i<count($result_area);$i++){
   			$호선명 = substr($result_area[$i]['area_kind'], (strpos($result_area[$i]['area_kind'], " ")+1));
		   	if($result_area[($i-1)]['company_serial'] != $result_area[$i]['company_serial'] || $호선명 != substr($result_area[($i-1)]['area_kind'], (strpos($result_area[($i-1)]['area_kind'], " ")+1))){
	      		if($result_area[($i-1)]['company_serial'] != $result_area[$i]['company_serial'])
	         		$service_area_list[$result_area[$i]['company_serial']] .= $result_area[$i]['city_name']."-".$호선명."\n";
		      	else if($호선명 != substr($result_area[($i-1)]['area_kind'], (strpos($result_area[($i-1)]['area_kind'], " ")+1))){
	         		$service_area_list[$result_area[$i]['company_serial']] .= "\n\n".$result_area[$i]['city_name']."-".$호선명."\n";
		      	}
		   	}
		   $service_area_list[$result_area[$i]['company_serial']] .= $result_area[$i]['location_name_kor']." ";
		}


		for($i=0;$i<count($partner_info);++$i) {
			$ps=$partner_info[$i]['serial'];
			$partner_info[$i]['service_area'] = $service_area_list[$ps];
			//파트너 정보 추가
			if($partner_info[$i]['flag']=='Y'){
				$partner_info[$i]['status'] = "사용중";
			}else if($partner_info[$i]['flag']=='W'){
				$partner_info[$i]['status'] = "승인 대기중";
			}else{
				$partner_info[$i]['status'] = "사용하지 않음";
			}

			if($partner_info[$i]['phone_option2']!=''){
				$partner_info[$i]['phone_option1'] = $partner_info[$i]['phone_option1'].", ".$partner_info[$i]['phone_option2'];
			}

			if($partner_info[$i]['open_time_start']!=''){
				$partner_info[$i]['service_time'] = substr($partner_info[$i]['open_time_start'], 0, 2).":".substr($partner_info[$i]['open_time_start'], 2, 2);
			}
			if($partner_info[$i]['open_time_finish']!=''){
				$partner_info[$i]['service_time'] .= " ~ ".substr($partner_info[$i]['open_time_finish'], 0, 2).":".substr($partner_info[$i]['open_time_finish'], 2, 2);
			}


			//
		}
		sort($partner_info);
		echo json_encode($partner_info);
	}

	function register_partner_information($partner_serial) {
		$id=$this->input->post('id',TRUE);
		$partner_name=$this->input->post('partner_name',TRUE);
		$owner_name=$this->input->post('owner_name',TRUE);
		$company_address=$this->input->post('company_address',TRUE);
		$register_number=$this->input->post('register_number',TRUE);
		$tel=$this->input->post('tel',TRUE);
		$fax=$this->input->post('fax',TRUE);
		$email=$this->input->post('email',TRUE);
		$office_address=$this->input->post('office_address',TRUE);
		$address_comment=$this->input->post('address_comment',TRUE);

		$write_data = array (
			'id'=>$id,
			'company_name'=>$partner_name,
			'owner_name'=>$owner_name,
			'company_address'=>$company_address,
			'register_number'=>$register_number,
			'tel'=>$tel,
			'fax'=>$fax,
			'email'=>$email,
			'office_address'=>$office_address,
			'address_comment'=>$address_comment
			);
	
		$result=$this->partnermanager->register_partner_information($partner_serial,$write_data);

		echo json_encode($result);
	}

	// function register_btn_flag($id) {
	// 	// $id=$this->input->post('id',TRUE);
	// 	$result=$this->partnermanager->register_btn_flag($id);
	// 	echo json_encode($result);
	// }

	function register_submit($id) {
		$this->partnermanager->update_flag($id);
		// die();
		$company_info=$this->partnermanager->get_company_info($id);

	 	$write_data=array(
	 		'admin_id'=>$company_info[0]['id'],
	 		'admin_pw'=>$company_info[0]['password'],
	 		'admin_level'=>3,
	 		'admin_name'=>$company_info[0]['owner_name'],
	 		// 'permission'=>
	 		// 'registered_date'=>$now,
	 		// 'registered_ip'=>
	 		'company_serial'=>$company_info[0]['serial'],
	 		'admin_flag'=>'Y',
	 		'phone_number'=>$company_info[0]['phone'],
	 		'auth_code'=>$company_info[0]['auth_code']
	 		);

	 	$result=$this->partnermanager->write_admin_info($write_data);
	 	echo json_encode($result);	
	}

	function send_confirm_mail(){
		$company_serial=$this->input->post('partner_serial',TRUE);

		$company_info=$this->partnermanager->get_company_info(intval($company_serial));
		if($company_info[0]['email']!=null && $company_info[0]['email']!=''){

				// 메일발송 시작
			    $nameFrom  = "렌고";
			    $mailFrom = "rengo@co.kr";
			    $nameTo  = $company_info[0]['company_name'];
			    $mailTo = $company_info[0]['email'];
			    // $cc = "참조";
			    // $bcc = "숨은참조";
				$subject ="렌고 파트너스 최종 승인 링크를 보내드립니다!";
				//암호화 로직 추가해야 함
				$encodeStr = $this->rengo_encryption->encryption($company_serial);
				$url ="http://solution.rengo.co.kr/registermanager/auth/".$encodeStr;
				$message = '

<!DOCTYPE html>
<html>
<head>
    <title>RENGO SOLUTION</title>
</head>
<body>
 <table style="max-width: 600px; width: 100%; margin: 0 auto; padding: 0 15px; font-family: "나눔고딕", "나눔바른고딕", "돋움", "고딕"; font-size:14px; line-height:1.5em; color:#252525;" cellpadding="0" border="0" cellspacing="0">
      <tr>
        <td style="padding:0 30px;">안녕하세요. 렌고입니다. </td>
      </tr>
      <tr>
        <td style="padding:0 30px;">파트너 신청 승인이 완료되었습니다.</td>
      </tr>
      <tr>
        <td style="height:18px;"></td>
      </tr>
      <tr>
        <td style="padding:0 30px;">[파트너 신청 내역]</td>
      </tr>
      <tr>
        <td style="padding:0 30px;">아이디(이메일) : <span style="color:#252525">'.$company_info[0]['email'].'</span></td>
      </tr>
        <td style="padding:0 30px;">파트너명 : '.$company_info[0]['company_name'].'</td>
      <tr>
        <td style="padding:0 30px;">주소 : '.$company_info[0]['company_address'].'</td>
      </tr>
      <tr>
        <td style="padding:0 30px;">성명/직책 : '.$company_info[0]['owner_name'].'/점장</td>
      </tr>
      <tr>
        <td style="padding:0 30px;">휴대전화 : '.$company_info[0]['tel'].'</td>
      </tr>

      <tr>
        <td style="height:18px;"></td>
      </tr>
      <tr>
        <td style="padding:0 30px;">아래 링크를 통해 로그인을 진행 해 주세요.</td>
      </tr>
      <tr>
        <td style="height:15px;">'.$url.'</td>
      </tr>
      <tr>
        <td style="padding:0 30px;">감사합니다.</td>
      </tr>
      <tr>
        <td>
          <table style="background:#ffe80e; width:100%; padding:30px; margin-top:30px; margin-bottom:30px;">
            <tr>
              <td></td>
            </tr>
            <tr>
              <td>
                (주)렌고<br />
                대표 : 이승원 I 사업자번호 : 302-81-29052<br />
                통신판매업신고 : 제2016-부산금정-0119호<br />
                개인정보담당자 : swlee@rengo.co.kr<br />
                고객센터 : 1800-1090<br />
                ⓒ 2017 rengo All rights reserved.
              </td>
              <td style="text-align:right; vertical-align:bottom;"><img src="http://solution.rengo.co.kr/img/L1_black.png" class="" width="40" height="40"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
</body>
</html>';
	    
			    $charset = "UTF-8";


			    $nameFrom   = "=?$charset?B?".base64_encode($nameFrom)."?=";
			    $nameTo   = "=?$charset?B?".base64_encode($nameTo)."?=";
			    $subject = "=?$charset?B?".base64_encode($subject)."?=";

			    $header  = "Content-Type: text/html; charset=utf-8\r\n";
			    $header .= "MIME-Version: 1.0\r\n";

			    $header .= "Return-Path: <". $mailFrom .">\r\n";
			    $header .= "From: ". $nameFrom ." <". $mailFrom .">\r\n";
			    $header .= "Reply-To: <". $mailFrom .">\r\n";
			    if ($cc)  $header .= "Cc: ". $cc ."\r\n";
			    if ($bcc) $header .= "Bcc: ". $bcc ."\r\n";

			    $result = mail($mailTo, $subject, $message, $header, $mailFrom);
			    if(!$result){
			    	$response['code'] = 'E01';
					$response['message'] = 'mail 전송 실패!';
					echo json_encode($response);		
			    }else{
			    	$response['code'] = 'S01';
					echo json_encode($response);
			    }
		}else{
			$response['code'] = 'E01';
			$response['message'] = '파트너의 email정보가 없습니다.';
			echo json_encode($response);
		}
		
	}

	

}
