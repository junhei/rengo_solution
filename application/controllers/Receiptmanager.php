<?php
class Receiptmanager extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('Receipt_model','receipt');
	 
	} 

	function _view($url, $data = ''){
		$this->load->view($url, $data); 
	}

	function index(){
		// $data['bad']=$this->blacklist->get_list();
		$this->_view("/admin/admin_register"); 
	}


	function get_data($id){
		// $id=$this->input->post('serial',TRUE);
		echo json_encode($this->receipt->get_receipt_data($id));
	}

	function register_receipt() {
		$order_serial=$this->input->post('order_serial',TRUE);
		$username=$this->input->post('username',TRUE);
		$receipt_date=$this->input->post('receipt_date',TRUE);
		$receipt_type=$this->input->post('receipt_type',TRUE);
		$pay_type=$this->input->post('pay_type',TRUE);
		$input_money=$this->input->post('input_money',TRUE);
		$memo=$this->input->post('memo',TRUE);
		$tax=$this->input->post('tax',TRUE);
		$staff=$this->input->post('staff',TRUE);
		$rent_type=$this->input->post('rent_type',TRUE);
		if($rent_type=='중기'){
				$midterm_flag = 'Y';
				$longterm_flag = 'N';
				$normal_flag = 'N';
				$insurance_flag = 'N';
		}
		else if($rent_type=='장기'){
				$longterm_flag = 'Y';
				$normal_flag = 'N';
				$insurance_flag = 'N';
				$midterm_flag = 'N';
		}else if($rent_type=='보험'){
				$longterm_flag = 'N';
				$normal_flag = 'N';
				$insurance_flag = 'Y';
				$midterm_flag = 'N';
		}else{
				$longterm_flag = 'N';
				$normal_flag = 'Y';
				$insurance_flag = 'N';
				$midterm_flag = 'N';
		}

		$tempResult = $this->receipt->get_order_data(intval($order_serial));
		$order_data = $tempResult[0];

		// echo json_encode($order_data); 

		$write_data = array(
			'order_serial' => intval($order_serial),
			'user_serial' => intval($order_data['user_serial']),
			'company_serial' => intval($order_data['company_serial']),
			'car_serial' =>  intval($order_data['car_serial']),
			'username' => $username,
			'deposite_date' => $receipt_date,
			'type' => $receipt_type,
			'deposite_way' => $pay_type,
			'deposite_amount' => intval($input_money),
			'memo' => $memo,
			'tax_paper' => $tax,
			'handler' => $staff,
			'normal_sale' => $normal_flag,
			'midterm_sale' => $midterm_flag,
			'longterm_sale' => $longterm_flag,
			'insurancebalance_sale' => $insurance_flag,
			'flag' => 'Y'
			);

		$result=$this->receipt->add_receipt($write_data);
		echo json_encode($result);
	}

	function update_receipt() {
		$serial=$this->input->post('serial',TRUE);
		$username=$this->input->post('username',TRUE);
		$receipt_date=$this->input->post('receipt_date',TRUE);
		$receipt_type=$this->input->post('receipt_type',TRUE);
		$pay_type=$this->input->post('pay_type',TRUE);
		$input_money=$this->input->post('input_money',TRUE);
		$memo=$this->input->post('memo',TRUE);
		$tax=$this->input->post('tax',TRUE);
		$staff=$this->input->post('staff',TRUE);
		$rent_type=$this->input->post('rent_type',TRUE);
		if($rent_type=='중기'){
				$midterm_flag = 'Y';
				$longterm_flag = 'N';
				$normal_flag = 'N';
				$insurance_flag = 'N';
		}
		else if($rent_type=='장기'){
				$longterm_flag = 'Y';
				$normal_flag = 'N';
				$insurance_flag = 'N';
				$midterm_flag = 'N';
		}else if($rent_type=='보험'){
				$longterm_flag = 'N';
				$normal_flag = 'N';
				$insurance_flag = 'Y';
				$midterm_flag = 'N';
		}else{
				$longterm_flag = 'N';
				$normal_flag = 'Y';
				$insurance_flag = 'N';
				$midterm_flag = 'N';
		}



		// echo json_encode($order_data); 

		$write_data = array(
			'username' => $username,
			'deposite_date' => $receipt_date,
			'type' => $receipt_type,
			'deposite_way' => $pay_type,
			'deposite_amount' => intval($input_money),
			'memo' => $memo,
			'tax_paper' => $tax,
			'handler' => $staff,
			'normal_sale' => $normal_flag,
			'midterm_sale' => $midterm_flag,
			'longterm_sale' => $longterm_flag,
			'insurancebalance_sale' => $insurance_flag,
			'flag' => 'Y'
			);

		$result=$this->receipt->update_receipt($serial, $write_data);
		echo json_encode($result);
	}

	function delete_receipt() {
		$serial=$this->input->post('serial',TRUE);
		$result=$this->receipt->delete_receipt($serial);
		echo json_encode($result);
	}

	function get_deposite_ways($company_serial){
		// $serial=$this->input->post('company_serial',TRUE);
		$result=$this->receipt->get_deposite_ways($company_serial);
		echo json_encode($result);
	}

	function save_deposite_way(){
		$name=$this->input->post('name',TRUE);
		$company_serial=$this->input->post('company_serial',TRUE);
		$memo=$this->input->post('memo',TRUE);
		$data = array(
			'name' => $name,
			'company_serial' => $company_serial,
			'memo' => $memo,
			'flag' => 'Y'
			);
		echo json_encode($this->receipt->add_deposite_way($data));
	}

	function delete_deposite_way(){
		$serial=$this->input->post('serial',TRUE);
		$result=$this->receipt->delete_deposite_way($serial);
		echo json_encode($result);
	}

}
?>