<?php



class Settlementstatement extends CI_Controller {

	function __construct() { 
		parent::__construct();

		$this->load->model('admin/Admin_model', 'admin');
	    $this->load->model('Salemanager_model','sale');
	    $this->load->model('Company_model','company');
	    require_once("/home/apache/mpdf60/mpdf.php");
  		$menu_permission = 4;
  		$permission = $this->admin->_check_permission($menu_permission);
  		if($permission != "Y")
   		$this->admin->admin_logout(); 
	} 

	function _view($url, $data = ''){
		$data['permission'] = $this->session->userdata('admin_permission');
		$data['admin_id'] = $this->session->userdata('admin_id');
		$data['admin_name'] = $this->admin->get_admin_name();
  		$data['company_serial'] = $this->session->userdata('company_serial');
		$data['company_name'] = $this->session->userdata('company_name');
		$this->load->view("admin/admin_layout_top", $data); 
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$company_serial = $this->session->userdata('company_serial');
		$data['company_serial'] = $company_serial;
		$data['company_list'] = $this->company->get_company($company_serial);
		$this->_view("sale/settlement_statement_view",$data); 
	}

	function get_settlementstatement($company_serial, $start_date, $end_date){

		$response = $this->sale->get_settlementstatement($company_serial, $start_date, $end_date);
		echo json_encode($response);

	}

	function print_calculation($company_serial, $start_date, $end_date){


			// create new PDF document
    $company_data = $this->company->get_company($company_serial);
    $order_data = $this->sale->get_settlementstatement($company_serial, $start_date, $end_date);

    if(count($order_data)==0){
       echo "해당 기간에 출력할 정산 내역이 없습니다.";
       die();
    }

   


	 

$today = date("Y-m-d");
$start_date_str = substr($start_date, 0, 4)."년".substr($start_date, 4, 2)."월".substr($start_date, 6, 2)."일";
$end_date_str = substr($end_date, 0, 4)."년".substr($end_date, 4, 2)."월".substr($end_date, 6, 2)."일";

// set some text to print

$html = '
<html>
  <head>
    <title>파트너 정산내역서</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1">
  </head>
  <style>
    @charset "UTF-8";
    /* CSS Document */

    body{margin: 0; padding: 0; font-family:"나눔고딕"; color:#252525; font-size: 14px;}
    ul, li, dd, dl{margin: 0; padding: 0;}
    strong{font-weight: bold;}
    .pdt20{padding-top:20px;}
    .cash_dd{text-align: right;}
    .body_wrap{width: 800px; padding: 30px 30px 0 30px; margin:0 auto;}
    .body_wrap>.header{background: #ffe80e; padding: 30px; margin-bottom:60px;}
    .body_wrap>.header>.logo_w{background:url(http://solution.rengo.co.kr/img/L1_w.png) right bottom no-repeat; background-size: auto 60px;}
    .body_wrap>.header>.logo_w>h1{color#252525; font-size: 34px; font-weight: normal; margin: 0; padding: 0; line-height: 1.3em; letter-spacing: -0.4px; margin-bottom: 100px;}
    .body_wrap>.content>.box_txt{margin-bottom:60px;}
    .body_wrap>.content>.box_txt>h1{font-size: 34px; font-weight: normal; color: #d6d6d6; padding:0 30px; margin: 0;}
    .body_wrap>.content>.box_txt>hr{border-top: 1px solid #d6d6d6; margin:30px 0 20px;}
    .body_wrap>.content>.box_txt>p{font-size: 14px; color: #252525; padding:0 30px; margin: 0;}
    .body_wrap>.content>.box_txt>dl{border-bottom: 1px solid #d6d6d6; padding-bottom: 20px; margin-bottom:20px;}
    .body_wrap>.content>.box_txt>dl>dt{width: 200px; font-weight: bold; display: inline-block; padding: 0 0 0 30px;}
    .body_wrap>.content>.box_txt>dl>dd{width:536px; display: inline-block; padding: 0 30px 0 0;}
    .sales_history{padding:0; border-spacing:0px; border:0; border-collapse:collapse; padding: 0; margin:30px 0 0 0; border-top:1px solid #d6d6d6; font-size:12px;}
    .sales_history tr{border-bottom: 1px solid #d6d6d6;}
    .sales_history th{background: #f0f0f0; font-weight: bold; padding:0; margin: 0; border: none; vertical-align: middle; }
    .sales_history td{padding: 20px 0; margin: 0; vertical-align: middle; text-align: center; font-weight: normal;}

  </style>
  <body>
    <table style="max-width: 1000px; width: 100%; margin: 0 auto; font-family:"나눔고딕"; " cellpadding="0" border="0" cellspacing="0">

      <!--header
      <tbody>
        <tr>
          <td colspan="2">
            <table style="width: 100%;" cellpadding="0" border="0" cellspacing="0">
              <tbody>
                <tr style="background-color:#ffe80e;">
                  <td style="color#252525; font-size: 34px; font-weight: normal; margin: 0; padding: 30px 30px 60px 30px; line-height: 1.3em; letter-spacing: -0.4px; text-align:left;" colspan="2">
                    렌고 파트너 <b>정산내역서</b><br>'.$company_data[0]['company_name'].'
                  </td>
                </tr>
                <tr style="background:#ffe80e;">
                  <td style="text-align:left; vertical-align:bottom; font-size: 14px; color: #252525; padding:0 30px 30px 30px; margin: 0;">
                    발행일자 : '.$today.'
                  </td>
                  <td style="text-align:right; vertical-align:bottom; padding:0 30px 30px 30px;">
                    <img src="http://solution.rengo.co.kr/img/sale_logo_w.jpg" class="" width="60" height="60">
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
      <!--/header-->

      <!--header-->
      <tbody>
        <tr>
          <td style="font-size:24px; color:#d6d6d6; text-align:left; padding:15px 15px 20px 20px; width:50%;">
            렌고 파트너 정산내역서
          </td>
          <td style="font-size:12px; text-align:right; padding:15px 15px 20px 20px; width:50%;">
            정산일 2016-12-06
          </td>
        </tr>
      </tbody>
      <!--/header-->

      <!--보완관리요청-->
      <tbody>
        <tr>
          <td style="font-size:24px; font-weight:normal; color:#d6d6d6; padding:20px 20px 0 20px; text-align:left;" colspan="2">
            보안관리요청
          </td>
        </tr>
        <tr>
          <td style="height:15px;" colspan="2">
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6;" colspan="2"></td>
        </tr>
        <tr>
          <td style="height:15px;" colspan="2">
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 20px 20px; text-align:left;" colspan="2">
            본 정산내역서는 귀사와 ㈜렌고 간 업무제휴를 위해 [제 3자에게 임의 제공 또는 누설하여서는 아니 되는 정보]입니다.<br />
            따라서, 본 정산내역서가 제3자에게 유출되지 않도록 주의해주시기를 당부드립니다.
          </td>
        </tr>
      </tbody>
      <!--/보완관리요청-->

      <!--파트너정보-->
      <tbody>
        <tr>
          <td style="font-size:24px; font-weight:normal; color:#d6d6d6; padding:20px 20px 0 20px; text-align:left;" colspan="2">
            파트너 정보
          </td>
        </tr>
        <tr>
          <td style="height:15px;" colspan="2">
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6;" colspan="2"></td>
        </tr>
        <tr>
          <td style="height:15px;" colspan="2">
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 15px 20px; text-align:left; width:170px; vertical-align:middle;">
            파트너명
          </td>
          <td style="text-align:left; width:540px; padding:0 20px 15px 0; vertical-align:middle; font-size:12px; color:#252525;">
            '.$company_data[0]['company_name'].'
          </td>
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6; height:15px;" colspan="2"></td>
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 15px 20px; text-align:left; width:170px; vertical-align:middle;">
            사업자등록번호
          </td>
          <td style="text-align:left; width:540px; padding:0 20px 15px 0; vertical-align:middle; font-size:12px; color:#252525;">
            '.$company_data[0]['register_number'].'
          </td>
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6; height:15px;" colspan="2"></td>
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 15px 20px; text-align:left; width:170px; vertical-align:middle;">
            대표자명
          </td>
          <td style="text-align:left; width:540px; padding:0 20px 15px 0; vertical-align:middle; font-size:12px; color:#252525;">
            '.$company_data[0]['owner_name'].'
          </td>
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6; height:15px;" colspan="2"></td>
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 15px 20px; text-align:left; width:170px; vertical-align:middle;">
            계좌번호
          </td>
          <td style="text-align:left; width:540px; padding:0 20px 15px 0; vertical-align:middle; font-size:12px; color:#252525;">
            '.$company_data[0]['bank_name'].' '.$company_data[0]['bank_account_number'].'
          </td>
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6; height:15px;" colspan="2"></td>
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 15px 20px; text-align:left; width:170px; vertical-align:middle;">
            이메일
          </td>
          <td style="text-align:left; width:540px; padding:0 20px 15px 0; vertical-align:middle; font-size:12px; color:#252525;">
            '.$company_data[0]['email'].'
          </td>
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6; height:20px;" colspan="2"></td>
        </tr>
      </tbody>
      <!--/파트너정보-->

      <!--정산내역-->
      <tbody>
        <tr>
          <td style="font-size:24px; font-weight:normal; color:#d6d6d6; padding:20px 20px 0 20px; text-align:left;" colspan="2">
            정산기간
          </td>
        </tr>
        <tr>
          <td style="height:15px;" colspan="2">
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6;" colspan="2"></td>
        </tr>
        <tr>
          <td style="height:15px;" colspan="2">
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 15px 20px; text-align:left; width:170px; vertical-align:middle;">
            판매일자
          </td>
          <td style="text-align:left; width:540px; padding:0 20px 15px 0; vertical-align:middle; font-size:12px; color:#252525;">
            '.$start_date_str.' 부터 '.$end_date_str.'
          </td>
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6; height:20px;" colspan="2"></td>
        </tr>
      </tbody>
	  </table>
      <!--/정산내역-->

      <!--판매내역-->
    <table class="sales_history" style="width:100%;">
      <tbody>
        <tr style="margin-bottom:8px;">
          <td style=" font-size:19px;padding-top:20px;padding-right:15px;padding-left:18px;color:#d6d6d6;text-align:left;" colspan="6">
            정산내역
          </td>
        </tr>
        <tr>
          <th style="font-weight:normal; padding:15px 5px; font-size:12px; width:55px;">NO</th>
          <th style="font-weight:normal; padding:15px 5px; font-size:12px; width:80px;">주문번호</th>
          <th style="font-weight:normal; padding:15px 5px; font-size:12px; width:80px;">차량정보</th>
          <th style="font-weight:normal; padding:15px 5px; font-size:12px; width:80px;">이용날짜</th>
          <th style="font-weight:normal; padding:15px 5px; font-size:12px; width:80px;">유저정보</th>
          <th style="font-weight:normal; padding:15px 5px; font-size:12px; width:80px;">결제금액</th>
        </tr>';

      for($i=0; $i<count($order_data); $i++){
      $html .= '
          <tr>
            <td style="border-bottom:1px solid #d6d6d6; font-size:12px;">'.($i+1).'</td>
            <td style="border-bottom:1px solid #d6d6d6; font-size:12px;">'.$order_data[$i]['order_number'].'</td>
            <td style="border-bottom:1px solid #d6d6d6; font-size:12px;">'.$order_data[$i]['car_name_detail'].'</td>
            <td style="border-bottom:1px solid #d6d6d6; font-size:11px;">'.$order_data[$i]['rent_date'].'</td>
            <td style="border-bottom:1px solid #d6d6d6; font-size:12px;">'.$order_data[$i]['user'].'</td>
            <td style="border-bottom:1px solid #d6d6d6; font-size:12px; text-align:right; padding-right:20px;">'.number_format ($order_data[$i]['deposite_amount']).'원</td>
          </tr>
              ';
      }

      $html .=
            '
	   </tbody>
	  </table>

    <table style="max-width: 1000px; width: 100%; margin: 0 auto; font-family:"나눔고딕"; " cellpadding="0" border="0" cellspacing="0">
        <tr>
          <td style="height:15px;" colspan="2">
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 15px 20px; text-align:left; width:170px; vertical-align:middle;">
            총정산대상금액
          </td>
          <td style="text-align:right; width:540px; padding:0 20px 15px 0; vertical-align:middle; font-size:12px; color:#252525;">
            '.number_format($order_data[0]['total_price']).'원
          </td>
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6; height:15px;" colspan="2"></td>
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 15px 20px; text-align:left; width:170px; vertical-align:middle;">
            서비스 수수료
          </td>
          <td style="text-align:right; width:540px; padding:0 20px 15px 0; vertical-align:middle; font-size:12px; color:#252525;">
            '.number_format($order_data[0]['service_fee']).'원
          </td>
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6; height:15px;" colspan="2"></td>
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 15px 20px; text-align:left; width:170px; vertical-align:middle;">
            정산 금액
          </td>
          <td style="text-align:right; width:540px; padding:0 20px 15px 0; vertical-align:middle; font-size:12px; color:#252525;">
            '.number_format($order_data[0]['calcuration_fee']).'원
          </td>
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6; height:20px;" colspan="2"></td>
        </tr>
      </tbody>
      <!--/판매내역-->

      <!--세금계산서 발행에 관한 건-->
      <tbody>
        <tr>
          <td style="font-size:24px; font-weight:normal; color:#d6d6d6; padding:20px 15px 0 20px; text-align:left;" colspan="2">
            세금계산서 발행에 관한 건
          </td>
        </tr>
        <tr>
          <td style="height:15px;" colspan="2">
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6;" colspan="2"></td>
        </tr>
        <tr>
          <td style="height:15px;" colspan="2">
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 20px 20px; text-align:left;" colspan="2">
            귀사는 ㈜렌고를 상대로 세금계산서를 발행하실 필요가 없으며, 렌고와 제휴에 대한매출분 (총정산대상금액)은 현금매출분으로 집계하시어 부가가치세 신고시 반영하시면 됩니다.<br />
            업체마다 부가가치세 신고기준이 다르므로 세무대리인과 상의하시어 신고하시기 바랍니다.
          </td>
        </tr>
      </tbody>
      <!--/세금계산서 발행에 관한 건-->

      <!--서비스 수수료 세금계산서 발행내역-->
      <tbody>
        <tr>
          <td style="font-size:24px; font-weight:normal; color:#d6d6d6; padding:20px 15px 0 20px; text-align:left;" colspan="2">
            수수료 세금계산서 발행내역
          </td>
        </tr>
        <tr>
          <td style="height:15px;" colspan="2">
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6;" colspan="2"></td>
        </tr>
        <tr>
          <td style="height:15px;" colspan="2">
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 15px 20px; text-align:left; width:170px; vertical-align:middle;">
            작성일자
          </td>
          <td style="text-align:left; width:540px; padding:0 20px 15px 0; vertical-align:middle; font-size:12px; color:#252525;">
            '.$today.'
          </td>
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6; height:15px;" colspan="2"></td>
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 15px 20px; text-align:left; width:170px; vertical-align:middle;">
            공급가액
          </td>
          <td style="text-align:left; width:540px; padding:0 20px 15px 0; vertical-align:middle; font-size:12px; color:#252525;">
            '.number_format($order_data[0]['profit']).'원
          </td>
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6; height:15px;" colspan="2"></td>
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 15px 20px; text-align:left; width:170px; vertical-align:middle;">
            부가가치세
          </td>
          <td style="text-align:left; width:540px; padding:0 20px 15px 0; vertical-align:middle; font-size:12px; color:#252525;">
            '.number_format($order_data[0]['add_price']).'원
          </td>
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6; height:15px;" colspan="2"></td>
        </tr>
        <tr>
          <td style="font-size:12px; color:#252525; padding:0 20px 15px 20px; text-align:left; width:170px; vertical-align:middle;">
            공급대가
          </td>
          <td style="text-align:left; width:540px; padding:0 20px 15px 0; vertical-align:middle; font-size:12px; color:#252525;">
            '.number_format($order_data[0]['service_fee']).'원
          </td>
        </tr>
        <tr>
          <td style="border-top:1px solid #d6d6d6; height:20px;" colspan="2"></td>
        </tr>
      </tbody>
      <!--/서비스 수수료 세금계산서 발행내역-->
      <tbody>
        <tr>
          <td style="height:20p"></td>
        </tr>
        <tr>
          <td style="text-align:left; font-size:12px; color:#252525; padding-left:20px;" colspan="2">
            ㈜렌고 부산시 금정구 금강로 271 3층(장전동)<br />
            rengo.co.kr / TEL : 1800-1090 / FAX : 051-581-3007
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>';

$mpdf=new mPDF('ko+aCJK', 'A4');
$mpdf->allow_charset_conversion=true;
$mpdf->WriteHTML($html);
$mpdf->Output();



// print a block of text using Write()
// output the HTML content
// $pdf->writeHTML($html, true, false, true, false, '');

// ---------------------------------------------------------

//Close and output PDF document
// $file_name = $company_data[0]['company_name']."_정산내역서.pdf";
// $pdf->Output('rengo_calcuration.pdf', 'I');

		
	}

}
?>