<?php
class Salemanager extends CI_Controller {
	private $template_file = "admin/index";
	private $admin_function, $request_uri;

	function __construct() { 
		parent::__construct();
	    $this->load->model('admin/Admin_model', 'admin');
	    $this->load->model('Salemanager_model','salemanager');
	    $this->load->model('Company_model','company');
  		$menu_permission = 3;
  		$permission = $this->admin->_check_permission($menu_permission);
  		if($permission != "Y")
   		$this->admin->admin_logout(); 

	} 
	function _view($url, $data = ''){
		$data['permission'] = $this->session->userdata('admin_permission');
  		$data['admin_id'] = $this->session->userdata('admin_id');
		$this->load->view("admin/admin_layout_top", $data); 
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$company_serial = $this->session->userdata('company_serial');
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$data['company_list'] = $this->company->get_company($company_serial);
		$data['branch_list'] = $this->company->get_branch($company_serial);
		$this->_view("sale/sale_manager_view", $data); 
	}


	//차량번호, 수납방식, 기간, 월별에 대해 기준이 되는 column 의 값을 설정해준다. 이 것이 없으면 배열을 합치는 작업이 어렵다.
	function _get_classifies($flag,$year,$month){
		$ret = array();
		if($flag=='carnumber_sales' || $flag=='orderday_carnumber_sales') {
			$ret=$this->salemanager->get_carnumbers($company_serial);	
		} else if($flag=='acceptance_sales') {
			$ret=$this->salemanager->get_acceptances($company_serial);	
		} else if($flag=='period_sales' || $flag=='orderday_period_sales') {
			for($i=1;$i<13;++$i){
				$ret[$i-1]=array('classify'=>strval($i));	
			}
		} else if($flag=='period_sales_per_month' || $flag == 'orderday_period_sales_per_month') {
			$last_day=date('t',mktime(0,0,1,$month,1,$year));
			for($i=1;$i<=$last_day;++$i){
				$ret[$i-1]=array('classify'=>strval($i));	
			}
		}
		return $ret;
	}

	//매출 방식에 대한 column과 카테고리(단기, 중기, 장기, 보험대차)에 대한 배열을 만든다.
	function _get_category_sales($company_serial,$category,$by_way,$year,$month){
		if($by_way == 'period_sales' || $by_way =='carnumber_sales' || $by_way == 'acceptance_sales' || $by_way == 'period_sales_per_month' 
			|| $by_way == 'orderday_period_sales' || $by_way == 'orderday_period_sales_per_month' || $by_way == 'orderday_carnumber_sales')
		{	
			if($category=='') { 
				$category_data=$this->salemanager->$by_way($company_serial,$category,$year,$month);
				$category='total';
			} else {
				$category_data=$this->salemanager->$by_way($company_serial,$category.'_sale',$year,$month);
			}

			if($by_way=='carnumber_sales' || $by_way=='orderday_carnumber_sales'){
				foreach($category_data as $sale_data)
				{
					$ret[] = array(
						"car_name_detail"=>$sale_data['car_name_detail'],
						"classify"=>$sale_data['classify'],
						$category."_count"=>intval($sale_data['count']),
						$category."_sale"=>intval($sale_data['sales'])
					);
				}
			} else {
				foreach($category_data as $sale_data)
				{
					$ret[] = array(
						"classify"=>$sale_data['classify'],
						$category."_count"=>intval($sale_data['count']),
						$category."_sale"=>intval($sale_data['sales'])
					);
				}
			}
			return $ret;
		}
		else 
		{
			die();
		}	
	}

	//??
	function _merge_classify($send_data,$company_serial,$flag) {	
		if($flag=='carnumber') {
			$ret=$this->salemanager->get_carnumbers($company_serial);	
		} else if($flag=='acceptance') {
			$ret=$this->salemanager->get_acceptances($company_serial);	
		}
		
		if(count($send_data)!=0){
			for($i=0;$i<count($ret);++$i){
				for($j=0;$j<count($send_data);++$j){
					if($ret[$i]['classify']==$send_data[$j]['classify']){
						$ret[$i] = array_merge($ret[$i],$send_data[$j]);
					}
				}
			}
		}
		return $ret;
	}

	//type에 따라 칼럼을 만들고 기준 칼럼과 합친다.
	function _make_type_data($company_serial,$type,$by_way,$year,$month) {

		if($by_way=='period_sales' || $by_way=='orderday_period_sales') {
			for($i=1;$i<13;++$i){
				$origin[$i-1]=array('classify'=>strval($i));	
			}
		} else if($by_way=='carnumber_sales' || $by_way=='orderday_carnumber_sales') {
			$origin=$this->salemanager->get_carnumbers($company_serial);
		} else if($by_way=='acceptance_sales') {
			$origin=$this->salemanager->get_acceptances($company_serial);	
		} else if($by_way=='period_sales_per_month' || $by_way=='orderday_period_sales_per_month') {
			$last_day=date('t',mktime(0,0,1,$month,1,$year));
			for($i=1;$i<=$last_day;++$i){
				$origin[$i-1]=array('classify'=>strval($i));	
			}
		}

		$type_price_key = array(
				'대여금액'=>'rental_charge',
				'대여기간할인'=>'period_discount',
				'특정기간할인'=>'special_period_discount',
				'기타할인'=>'other_discount',
				'배차'=>'car_allocation',
				'회차'=>'car_return',
				'배회차'=>'car_allocation_return',
				'자차1'=>'insurance_self1',
				'자차2'=>'insurance_self2',
				'자차3'=>'insurance_self3',
				'초과'=>'excess_charge',
				'환불'=>'refund_charge',
				'카시트'=>'car_seat',
				'스노우체인'=>'snow_chain',
				'기타유료옵션'=>'other_paid_option',
				'수리비'=>'repair_cost',
				'청구손실비'=>'loss_cost_billing',
				'사고면책금'=>'accident_exemption_cost',
				'사고보상금'=>'accident_compensation',
				'유류비용'=>'fuel_cost',
				'범칙금'=>'fine',
				'보증금'=>'security_deposit',
				'기타'=>'others',
				'총미수액'=>'outstanding_amount'
			);

		$type_data=$this->salemanager->type_data($company_serial,$type,$by_way,$year,$month);		

		for($i=0;$i<count($origin);++$i){
			$zero_data = array(
				'classify'=>$origin[$i]['classify'],
				$type_price_key[$type]=>0
			);
			$origin[$i]=array_merge($origin[$i],$zero_data);
		}

		for($i=0;$i<count($type_data);++$i){
			$carnum=$type_data[$i]['classify'];
			for($j=0;$j<count($origin);++$j){
				if($origin[$j]['classify']==$carnum){
					$origin[$j]=$type_data[$i];
				}
			}
		}

		sort($origin);
		return $origin;
	}
	//두 배열을 하나의 키 값(칼럼) 에 대해 합친다.
	function _merge_category_type($send_data,$type_data){

		for($i=0;$i<count($send_data);++$i){
			$send_data[$i] = array_merge($send_data[$i],$type_data[$i]);
		}	
		return $send_data;
	}

	
	//두 배열을 하나로 합치는데 그 값이 뭐냐면... 복잡허다 일단
	function _merge_caterogy_data($company_serial,$categorise,$by_way,$year,$month,$send_data){
		foreach($categorise as $cat){
			$category_data=$this->_get_category_sales($company_serial,$cat,$by_way,$year,$month);

			for($j=0;$j<count($send_data);++$j){
				for($k=0;$k<count($category_data);++$k){
					if($send_data[$j]['classify']==$category_data[$k]['classify']){
						$send_data[$j] = array_merge($send_data[$j],$category_data[$k]);
					}
				}
			}
		}
		return $send_data;
	}

	function _add_month_korean($send_data){
		for($i=0;$i<count($send_data);++$i) {
			if($send_data[$i]['classify']==0) {
				array_shift($send_data);
			}
			$send_data[$i]['classify'] = $send_data[$i]['classify']."월";

		}
		return $send_data;
	}

	function _add_day_korean($send_data){
		for($i=0;$i<count($send_data);++$i) {
			if($send_data[$i]['classify']==0) {
				array_shift($send_data);
			}
			$send_data[$i]['classify'] = $send_data[$i]['classify']."일";

		}
		return $send_data;
	}

	function _add_zero_data($categorise,$send_data){
		array_push($categorise,'total');

		for($i=0;$i<count($send_data);++$i) {
			foreach($categorise as $cat){
				if(!isset($send_data[$i][$cat."_sale"])){
					$zero_data = array(
						$cat."_count"=>0,
						$cat."_sale"=>0
					);
					$send_data[$i] = array_merge($send_data[$i],$zero_data);
				}
			}
		}
		return $send_data;
	}

	function _make_total_sum($send_data){
		foreach($send_data as $s){
			$sum_total_count += $s['total_count'];
			$sum_total_sale += $s['total_sale'];
			$sum_normal_count += $s['normal_count'];
			$sum_normal_sale += $s['normal_sale'];
			$sum_midterm_count += $s['midterm_count'];
			$sum_midterm_sale += $s['midterm_sale'];
			$sum_longterm_count += $s['longterm_count'];
			$sum_longterm_sale += $s['longterm_sale'];
			$sum_insurancebalance_count += $s['insurancebalance_count'];
			$sum_insurancebalance_sale += $s['insurancebalance_sale'];
			$sum_rental_charge += $s['rental_charge'];
			$sum_sale_charge += $s['sale_charge'];
			$sum_period_discount += $s['period_discount'];
			$sum_special_period_discount += $s['special_period_discount'];
			$sum_other_discount += $s['other_discount'];
			$sum_car_allocation += $s['car_allocation'];
			$sum_car_return += $s['car_return'];
			$sum_car_allocation_return += $s['car_allocation_return'];
			$sum_insurance_self1 += $s['insurance_self1'];
			$sum_insurance_self2 += $s['insurance_self2'];
			$sum_insurance_self3 += $s['insurance_self3'];
			$sum_excess_charge += $s['excess_charge'];
			$sum_refund_charge += $s['refund_charge'];
			$sum_car_seat += $s['car_seat'];
			$sum_snow_chain += $s['snow_chain'];
			$sum_other_paid_option += $s['other_paid_option'];
			$sum_repair_cost += $s['repair_cost'];
			$sum_loss_cost_billing += $s['loss_cost_billing'];
			$sum_accident_exemption_cost += $s['accident_exemption_cost'];
			$sum_accident_compensation += $s['accident_compensation'];
			$sum_fuel_cost += $s['fuel_cost'];
			$sum_fine += $s['fine'];
			$sum_security_deposit += $s['security_deposit'];
			$sum_others += $s['others'];
			$sum_outstanding_amount += $s['outstanding_amount'];
		}

		$sum_data = array(
			'classify'=>'합계',
			'total_count'=>$sum_total_count,
			'total_sale'=>$sum_total_sale,
			'normal_count'=>$sum_normal_count,
			'normal_sale'=>$sum_normal_sale,
			'midterm_count'=>$sum_midterm_count,
			'midterm_sale'=>$sum_midterm_sale,
			'longterm_count'=>$sum_longterm_count,
			'longterm_sale'=>$sum_longterm_sale,
			'insurancebalance_count'=>$sum_insurancebalance_count,
			'insurancebalance_sale'=>$sum_insurancebalance_sale,
			'rental_charge'=>$sum_rental_charge,
			'sale_charge'=>$sum_sale_charge,
			'period_discount'=>$sum_period_discount,
			'special_period_discount'=>$sum_special_period_discount,
			'other_discount'=>$sum_other_discount,
			'car_allocation'=>$sum_car_allocation,
			'car_return'=>$sum_car_return,
			'car_allocation_return'=>$sum_car_allocation_return,
			'insurance_self1'=>$sum_insurance_self1,
			'insurance_self2'=>$sum_insurance_self2,
			'insurance_self3'=>$sum_insurance_self3,
			'excess_charge'=>$sum_excess_charge,
			'refund_charge'=>$sum_refund_charge,
			'car_seat'=>$sum_car_seat,
			'snow_chain'=>$sum_snow_chain,
			'other_paid_option'=>$sum_other_paid_option,
			'repair_cost'=>$sum_repair_cost,
			'loss_cost_billing'=>$sum_loss_cost_billing,
			'accident_exemption_cost'=>$sum_accident_exemption_cost,
			'accident_compensation'=>$sum_accident_compensation,
			'fuel_cost'=>$sum_fuel_cost,
			'fine'=>$sum_fine,
			'security_deposit'=>$sum_security_deposit,
			'others'=>$sum_others,
			'outstanding_amount'=>$sum_outstanding_amount
		);	
		//배열 맨 마지막에 합계정보 추가
		array_push($send_data,$sum_data);
		return $send_data;
	}
	
	function _fill_remain_month($send_data) {
		$months=array();	
		foreach($send_data as $sd){
			array_push($months,$sd['classify']);
		}
		for($i=1;$i<13;++$i){
			if(!in_array($i,$months)){
			 	$temp = array('classify'=>$i);
			 	array_push($send_data, $temp);
			}
		}
		return $send_data;
	}

	function _fill_remain_day($send_data, $year, $month) {
		$last_day=date('t',mktime(0,0,1,$month,1,$year));
		$days=array();	
		foreach($send_data as $sd){
			array_push($days,$sd['classify']);
		}
		for($i=1;$i<=$last_day;++$i){
			if(!in_array($i,$days)){
			 	$temp = array('classify'=>$i);
			 	array_push($send_data, $temp);
			}
		}
		return $send_data;
	}

	function sale_way($company_serial,$by_way,$year,$month){

		$send_data = $this->_get_classifies($by_way,$year,$month);
		// echo json_encode($send_data);die();
		$totalsum_data=$this->_get_category_sales($company_serial,'',$by_way,$year,$month);
		// echo json_encode($totalsum_data);die();
		$idx=0;
		for($cnt=0;$cnt<count($send_data);++$cnt){
			if($totalsum_data[$idx]['classify']==$send_data[$cnt]['classify']){
				$send_data[$cnt]=array_merge($send_data[$cnt],$totalsum_data[$idx++]);
			}
		}

		if($by_way=='carnumber_sales' || $by_way=='orderday_carnumber_sales') {
			$send_data=$this->_merge_classify($send_data,$company_serial,'carnumber');
		} else if ($by_way=='acceptance_sales') {
			$send_data=$this->_merge_classify($send_data,$company_serial,'acceptance');
		}

		$token=explode('_',$by_way);

		$categorise = $toekn[0] == 'orderday'? array('일반','중기', '장기', '보험') : array('normal','midterm', 'longterm', 'insurancebalance');
		
		$send_data=$this->_merge_caterogy_data($company_serial,$categorise,$by_way,$year,$month,$send_data);

		if($by_way=='period_sales' || $by_way=='orderday_period_sales') $send_data=$this->_fill_remain_month($send_data);
		if($by_way=='period_sales_per_month' || $by_way == 'orderday_period_sales_per_month') $send_data=$this->_fill_remain_day($send_data,$year,$month);

		$send_data=$this->_add_zero_data($categorise,$send_data);
		sort($send_data);
		
		$type_array=
		array(
				'대여금액','대여기간할인','특정기간할인','기타할인',
				'배차','회차','배회차','자차1','자차2',
				'자차3','초과','환불','카시트','스노우체인',
				'기타유료옵션','수리비','청구손실비','사고면책금','사고보상금',
				'유류비용','범칙금','보증금',	'기타','총미수액'
			);

		for($i=0;$i<count($type_array); ++$i) {
			$type_data=$this->_make_type_data($company_serial,$type_array[$i],$by_way,$year,$month);			
			$send_data=$this->_merge_category_type($send_data,$type_data);
		}


		if($by_way=='period_sales' || $by_way=='orderday_period_sales') $send_data=$this->_add_month_korean($send_data);
		if($by_way=='period_sales_per_month' || $by_way == 'orderday_period_sales_per_month') $send_data=$this->_add_day_korean($send_data);

		//합계정보 만들기	
		$send_data=$this->_make_total_sum($send_data);
		
		echo json_encode($send_data);
	}

	function get_detail_reservation($company_serial, $start, $end){
		$result = $this->salemanager->detail_reservaion($company_serial, $start, $end);
		echo json_encode($result);
	}
	

}
?>