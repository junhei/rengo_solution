<?php
class Areasetting extends CI_Controller {


	function __construct() { 

		parent::__construct();
		$this->load->model('admin/Admin_model', 'admin');
		$this->load->model('Company_model', 'company');
		// 관리자 메뉴 접근 퍼미션 체크
		$menu_permission = 4;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y"){
			echo "sdafjlkdsjlfkjsadlkfjklsdajflksajdlkfjsald";
			// $this->admin->admin_logout(); 
		}
	}

	function _view($url, $data = ''){
		$company_serial = $this->session->userdata('company_serial');
		$data['admin_id'] = $this->session->userdata('admin_id');
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$data['permission'] = $this->session->userdata('admin_permission');
		$data['company_list'] = $this->company->get_company($company_serial);
		$this->load->view("admin/admin_layout_top", $data);
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$data['company_name'] = $this->session->userdata('company_name');
		$this->_view("preference/area_setting_view", $data); 
	}

}
?>