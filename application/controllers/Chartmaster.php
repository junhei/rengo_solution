<?php
class Chartmaster extends CI_Controller {


	function __construct() { 

		parent::__construct();
		$this->load->model('admin/Admin_model', 'admin');
		$this->load->model('Company_model','company');
		$this->load->model('Chart_model','chart');
		// 관리자 메뉴 접근 퍼미션 체크
		$menu_permission = 5;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y")
			$this->admin->admin_logout(); 

	

//			$this->output->enable_profiler(TRUE);
	}

	function _view($url, $data = ''){

		// $data['admin_name'] = $this->admin->get_admin_name();
		$this->load->view("admin/admin_layout_top", $data);
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$company_serial = $this->session->userdata('company_serial');
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$data['permission'] = $this->session->userdata('admin_permission');
		$data['admin_id'] = $this->session->userdata('admin_id');
		$data['company_list'] = $this->company->get_company(0);
		$this->_view("master/chart_view_master", $data); 
	}

	function get_daily_chart($start, $end, $company_serial){
		echo json_encode($this->chart->get_daily_data($start, $end, $company_serial));
	}

	function get_hourly_chart($start, $end, $company_serial){
		echo json_encode($this->chart->get_hourly_data($start, $end, $company_serial));
	}

	function get_cartype_chart($start, $end, $company_serial){
		echo json_encode($this->chart->get_cartype_data($start, $end, $company_serial));
	}

	function get_place_chart($start, $end, $company_serial){
		echo json_encode($this->chart->get_place_data($start, $end, $company_serial));
	}

	function get_age_chart($start, $end, $company_serial){
		echo json_encode($this->chart->get_age_data($start, $end, $company_serial));
	}

	function get_sex_chart($start, $end, $company_serial){
		echo json_encode($this->chart->get_sex_data($start, $end, $company_serial));
	}

	function get_price_chart($start, $end, $company_serial){
		echo json_encode($this->chart->get_price_data($start, $end, $company_serial));
	}

	function get_period_chart($start, $end, $company_serial){
		echo json_encode($this->chart->get_period_data($start, $end, $company_serial));
	}

	function get_expense_chart($start, $end, $company_serial){
		echo json_encode($this->chart->get_expense_data($start, $end, $company_serial));
	}

	function get_car_time_chart($start, $end, $company_serial){
		echo json_encode($this->chart->get_car_time_data($start, $end, $company_serial));
	}

}
?>