<?php
class Car_allocation extends CI_Controller {
	function __construct() { 
		parent::__construct();
		$this->load->model('admin/Admin_model', 'admin');
		$this->load->model('Company_model','company');
		$this->load->model('Car_model','carmanager');
		$this->load->model('Car_schedule_model','carschedule');
		$this->load->model('Rent_model','rent');
		$this->load->model('Staffmanager_model','staff');
		$menu_permission = 0;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y")
			$this->admin->admin_logout(); 
		// 관리자 메뉴 접근 퍼미션 체크


		if($_SERVER['REMOTE_ADDR'] == "122.37.189.47"){
			//개발자 사무실 아이피면 프로필 표시
//			$this->output->enable_profiler(TRUE);
		}

	} 

	function index(){
		$company_serial = $this->session->userdata('company_serial');
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$data['company_list'] = $this->company->get_company($company_serial);
		$data['staff_list'] = $this->staff->get_list($company_serial);
		$this->_view("rent/car_allocation_view", $data);
	}

	function _view($url, $data = ''){
		$data['permission'] = $this->session->userdata('admin_permission');
		$data['admin_id'] = $this->session->userdata('admin_id');
		$data['schedule_html'] = $this->rent->get_schedule_html($schedule_html_data);

		$this->load->view("admin/admin_layout_top", $data); 
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function get_car_schedule($start, $end, $company_serial){
		echo json_encode($this->carschedule->get_car_schedule($start, $end, $company_serial));
	}

	function get_car_schedule_detail($start, $end, $company_serial){
		echo json_encode($this->carschedule->get_car_schedule_detail($start, $end, $company_serial));
	}

	function get_count(){
		$start = $this->input->post('start_time', TRUE);
		$end = $this->input->post('end_time', TRUE);
		$company_serial = $this->input->post('company_serial', TRUE);
		echo json_encode($this->carschedule->get_count_delivery_pickup($start, $end, $company_serial));
	}
}
?>