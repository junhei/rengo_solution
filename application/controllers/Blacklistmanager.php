<?php
class Blacklistmanager extends CI_Controller {
	private $template_file = "admin/index";
	private $admin_function, $request_uri;

	function __construct() { 
		parent::__construct();

	    $this->load->model('admin/Admin_model', 'admin');
	    $this->load->model('Blacklist_model','blacklist');
  		$menu_permission = 4;
  		$permission = $this->admin->_check_permission($menu_permission);
  		if($permission != "Y")
   		$this->admin->admin_logout(); 
	} 

	function _view($url, $data = ''){
		$data['permission'] = $this->session->userdata('admin_permission');
  		$data['admin_id'] = $this->session->userdata('admin_id');
  		$data['company_serial'] = $this->session->userdata('company_serial');
		$data['company_name'] = $this->session->userdata('company_name');
		$data['count_black'] = $this->blacklist->count_black();
		$this->load->view("admin/admin_layout_top", $data); 
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		// $data['bad']=$this->blacklist->get_list();
		$this->_view("user/blacklist_manager_view"); 
	}

	function get_blacklist(){
		// $company_serial = $this->session->userdata('company_serial');
		$received_array = $this->blacklist->get_list();
		// var_dump($car_data_array);

		foreach($received_array as $data){
		
			$send_array[] = array(
				"serial" => intval($data['serial']),
				"company_serial" => intval($data['company_serial']),
				"name" => $data['name'],
				"tel" => $data['tel'],
				"birthday" => $data['birthday'],
				"used_date" => $data['used_date'],
				"memo" => $data['memo'],
				"kind" => $data['kind'],
				"used_area" => $data['used_area'],
				"company_name" => $data['company_name'],
				"license" => $data['license'],
			);
		}
		if($send_array == null){
			$send_array = array();
		}

		echo json_encode($send_array);
	}
	function register_black(){
		if(!$this->input->post('name', TRUE) || !$this->input->post('birthday', TRUE)){
			alert('필수항목을 입력해 주세요.');
			exit;
		}

		$company_serial = $this->session->userdata('company_serial');
		$company_name = $this->session->userdata('company_name');
		$admin_name = $this->session->userdata('admin_name');

		$write_data = array(
			'kind' => urldecode($this->input->post('kind', TRUE)),
			'name' => urldecode($this->input->post('name', TRUE)),
			'birthday' => urldecode($this->input->post('birthday', TRUE)),
			'tel' => urldecode($this->input->post('tel', TRUE)),
			'used_date' => urldecode($this->input->post('used_date', TRUE)),
			'memo' => urldecode($this->input->post('memo', TRUE)),
			'used_area' => urldecode($this->input->post('used_area', TRUE)),
			'company_serial' => $company_serial,
			'writer' => $admin_name,
			'company_name' => $company_name
		);


		$result = $this->blacklist->add($write_data);
		echo json_encode($result);
	}

	function update_black(){
		if(!$this->input->post('name', TRUE) || !$this->input->post('birthday', TRUE)){
			alert('필수항목을 입력해 주세요.');
			exit;
		}

		$company_serial = $this->session->userdata('company_serial');
		$admin_name = $this->session->userdata('admin_name');

		$write_data = array(
			'serial' =>intval($this->input->post('serial', TRUE)),
			'company_serial' => $company_serial,
			'name' => urldecode($this->input->post('name', TRUE)),
			'tel' => urldecode($this->input->post('tel', TRUE)),
			'birthday' => urldecode($this->input->post('birthday', TRUE)),
			'used_date' => urldecode($this->input->post('used_date', TRUE)),
			'memo' => urldecode($this->input->post('result', TRUE)),
			'kind' => urldecode($this->input->post('kind', TRUE)),
			'used_area' => urldecode($this->input->post('used_area', TRUE)),
			'writer' => $admin_name,
		);

		$result = $this->blacklist->update($write_data);
		echo json_encode($result);
	}

	function delete_black(){

		if(!$this->input->post('serial', TRUE)){
			alert('블랙리스트 id가 없습니다.');
			exit;
		}
		
		$result = $this->blacklist->delete($this->input->post('serial', TRUE));
		echo json_encode($result);
	}



}
?>