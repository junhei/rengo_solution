<?php
class Carnumbersales extends CI_Controller {
	private $template_file = "admin/index";
	private $admin_function, $request_uri;

	function __construct() { 
		parent::__construct();
	    $this->load->model('admin/Admin_model', 'admin');
	    $this->load->model('Salemanager_model','salemanager');
	    $this->load->model('Company_model','company');
  		$menu_permission = 3;
  		$permission = $this->admin->_check_permission($menu_permission);
  		if($permission != "Y")
   		$this->admin->admin_logout(); 

	} 
	function _view($url, $data = ''){
		$data['permission'] = $this->session->userdata('admin_permission');
  		$data['admin_id'] = $this->session->userdata('admin_id');
		$this->load->view("admin/admin_layout_top", $data); 
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$company_serial = $this->session->userdata('company_serial');
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$data['company_list'] = $this->company->get_company($company_serial);
		$this->_view("sale/carnumber_sales_view", $data); 
	}

	// function _get_category_sales($company_serial,$branch_serial,$category,$by_way,$month){
	// 	if($by_way == 'period_sales' || $by_way =='carnumber_sales' || $by_way == 'acceptance_sales')
	// 	{
	// 		if($category=='') { 
	// 			$category_data=$this->salemanager->$by_way($company_serial,$branch_serial,$category,$month);
	// 			$category='total';
	// 		} else {
	// 			$category_data=$this->salemanager->$by_way($company_serial,$branch_serial,$category.'_sale',$month);
	// 		}

	// 		foreach($category_data as $sale_data)
	// 		{
	// 			$ret[] = array(
	// 				// "month"=>$sale_data['month'],
	// 				"classify"=>$sale_data['classify'],
	// 				$category."_count"=>intval($sale_data['count']),
	// 				$category."_sale"=>intval($sale_data['sales'])
	// 			);
	// 		}
	// 		return $ret;
	// 	}
	// 	else 
	// 	{
	// 		die();
	// 	}
		
	// }

	// function sale_way($company_serial,$branch_serial=0,$by_way,$month=''){
	// 	// $company_serial=$this->session->userdata('company_serial');
	// 	$categorise=array('normal','midterm', 'longterm', 'insurancebalance');
	// 	//총매출로 배열 먼저 만들기. 안 그러면 merge가 안 됨
	// 	$send_data=$this->_get_category_sales($company_serial,$branch_serial,'',$by_way,$month);
		
	// 	//가져온 배열 정보 합치기
	// 	foreach($categorise as $cat){
	// 		$category_data=$this->_get_category_sales($company_serial,$branch_serial,$cat,$by_way,$month);

	// 		for($j=0;$j<count($send_data);++$j){
	// 			for($k=0;$k<count($category_data);++$k){
	// 				if($send_data[$j]['classify']==$category_data[$k]['classify']){
	// 					$send_data[$j] = array_merge($send_data[$j],$category_data[$k]);
	// 				}
	// 			}
	// 		}
	// 	}

		

	// 	$months=array();
	// 	if($by_way=='period_sales'){
	// 		foreach($send_data as $sd){
	// 			array_push($months,$sd['classify']);
	// 		}
	// 		for($i=1;$i<13;++$i){
	// 			if(!in_array($i,$months)){
	// 				 $temp = array('classify'=>$i);
	// 				 array_push($send_data, $temp);
	// 			}
	// 		}
	// 	}
	// 	//값 없는 필드 0으로 채우기
	// 	array_push($categorise,'total');
	// 	for($i=0;$i<count($send_data);++$i) {
	// 		foreach($categorise as $cat){
	// 			if(!isset($send_data[$i][$cat."_sale"])){
	// 				$zero_data = array(
	// 					$cat."_count"=>0,
	// 					$cat."_sale"=>0
	// 				);
	// 				$send_data[$i] = array_merge($send_data[$i],$zero_data);
	// 			}
	// 		}
	// 	}

	// 	//정렬
	// 	sort($send_data);

	// 	//정렬 후 각 숫자에 "월" string 붙이기
	// 	if($by_way=='period_sales'){
	// 		for($i=0;$i<count($send_data);++$i) {
	// 			if($send_data[$i]['classify']==0) {
	// 				array_shift($send_data);
	// 			}
	// 			$send_data[$i]['classify'] = $send_data[$i]['classify']."월";
	// 		}
	// 	}

	// 	//합계정보 만들기
	// 	foreach($send_data as $s){
	// 		$sum_total_count += $s['total_count'];
	// 		$sum_total_sale += $s['total_sale'];
	// 		$sum_normal_count += $s['normal_count'];
	// 		$sum_normal_sale += $s['normal_sale'];
	// 		$sum_midterm_count += $s['midterm_count'];
	// 		$sum_midterm_sale += $s['midterm_sale'];
	// 		$sum_longterm_count += $s['longterm_count'];
	// 		$sum_longterm_sale += $s['longterm_sale'];
	// 		$sum_insurancebalance_count += $s['insurancebalance_count'];
	// 		$sum_insurancebalance_sale += $s['insurancebalance_sale'];
	// 	}

	// 	$sum_data = array(
	// 		'classify'=>'합계',
	// 		'total_count'=>$sum_total_count,
	// 		'total_sale'=>$sum_total_sale,
	// 		'normal_count'=>$sum_normal_count,
	// 		'normal_sale'=>$sum_normal_sale,
	// 		'midterm_count'=>$sum_midterm_count,
	// 		'midterm_sale'=>$sum_midterm_sale,
	// 		'longterm_count'=>$sum_longterm_count,
	// 		'longterm_sale'=>$sum_longterm_sale,
	// 		'insurancebalance_count'=>$sum_insurancebalance_count,
	// 		'insurancebalance_sale'=>$sum_insurancebalance_sale
	// 	);	

	// 	//배열 맨 마지막에 합계정보 추가
	// 	array_push($send_data,$sum_data);

	// 	echo json_encode($send_data);
	// }

	// function get_branch($company_serial)
	// {
	// 	echo json_encode($this->company->get_branch($company_serial));
	// }

}
?>