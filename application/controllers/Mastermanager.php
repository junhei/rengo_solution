<?php
class Mastermanager extends CI_Controller {

	function __construct() { 

		parent::__construct();

		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		
		$this->load->model('admin/Admin_model', 'admin');
		$this->load->model('Mastermanager_model', 'master');
		$menu_permission = 5;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y")
			$this->admin->admin_logout(); 
	}

	function _view($url, $data = ''){
		$data['permission'] = $this->session->userdata('admin_permission');
		$data['admin_name'] = $this->admin->get_admin_name();

		$this->load->view("admin/admin_layout_top", $data); 
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$data['company_name'] = $this->session->userdata('company_name');
		$this->_view("master/master_manager_view", $data); 
	}

	function _init_admin_data($admin_list){
		$data=array();

		for($i=0; $i<count($admin_list); ++$i){
			$data[$i]['serial'] = $admin_list[$i]['serial'];
			$data[$i]['partner_serial'] = $admin_list[$i]['company_serial'];
		    $data[$i]['admin_id'] = $admin_list[$i]['admin_id'];
		    $data[$i]['admin_name'] = $admin_list[$i]['admin_name'];
		}

		return $data;
	}

	function _make_permission($admin_list,$data) {
		$permission_string = array('rent_manager','car_manager','user_manager','sale_manager','rengo_manager','preference','master_setting');

		for($i=0; $i<count($admin_list); ++$i){
		    
		    if($data[$i]['admin_id'] == $admin_list[$i]['admin_id']){
		        
		        $permission = $admin_list[$i]['permission'];
		        
		        for($j=0; $j<count($permission_string); ++$j) {

		            $flag = $permission[$j]=='Y'?'O':'X';

		            $temp_data = array(
		                'admin_id'=>$admin_list[$i]['admin_id'],
		                $permission_string[$j]=>$flag
		            );

		            $data[$i] = array_merge($data[$i],$temp_data);
		        }

		    }

		}
		return $data;
	}

	function _make_partner_name($company_name_list,$data) {

		for($i=0; $i<count($data); ++$i) {
			for($j=0; $j<count($company_name_list); ++$j){
				if($data[$i]['partner_serial'] == $company_name_list[$j]['serial']){
					$temp_data = array(
			            'partner_name'=>$company_name_list[$j]['company_name']
			        );

			        $data[$i] = array_merge($data[$i],$temp_data);		
				}
			}
	        
		}
		return $data;
	}

	function get_admin_list(){
		$admin_list=$this->master->get_admin_information();

		$company_name_list = $this->admin->get_partner_list("Y");

		$data = $this->_init_admin_data($admin_list);

		$data = $this->_make_permission($admin_list,$data);
		
		$data = $this->_make_partner_name($company_name_list,$data);

		echo json_encode($data);
	}

	function get_partner_list() {


		$result = $this->admin->get_partner_list('Y');
		array_unshift($result, array( 
				'serial' => 0,
				'company_name' => '파트너 마스터'));
		
		echo json_encode($result);
	}

	function update_admin_information() {
		$serial = $this->input->post('serial');
		$admin_id = $this->input->post('admin_id');
		$admin_pw = $this->input->post('admin_pw');
		$admin_name = $this->input->post('admin_name');
		$permission = $this->input->post('permission');
		$company_serial = $this->input->post('company_serial');

		$admin_info = array(
			'serial'=>$serial,
			'admin_id'=>$admin_id,
			'admin_pw'=>$admin_pw,
			'admin_name'=>$admin_name,
			'permission'=>$permission,
			'company_serial'=>$company_serial
			);	

		// echo json_encode($admin_info);die();
		$result = $this->master->update_admin_information($admin_info);

		echo json_encode($result);
	}
}
?>