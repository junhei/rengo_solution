<?php
class Rent extends CI_Controller {
	private $template_file = "rent/rent";
	private $request_uri;

	function __construct() { 
		parent::__construct();
		$this->load->model('admin/Admin_model', 'admin');
		$this->load->model('Rent_model','rent');
		$this->load->model('Company_model','company');
		$this->load->model('Car_model','carmanager');
		$this->load->model('Fare_scheme_model','fare');
		$this->load->model('Staffmanager_model','staff');

		$menu_permission = 0;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y")
			$this->admin->admin_logout(); 
		// 관리자 메뉴 접근 퍼미션 체크


		if($_SERVER['REMOTE_ADDR'] == "122.37.189.47"){
			//개발자 사무실 아이피면 프로필 표시
//			$this->output->enable_profiler(TRUE);
		}

	} 

	function index($mode = '', $option = ''){
		$this->_view($data);
	}

	function _view($data = ''){
		$company_serial = $this->session->userdata('company_serial');
		// $branch_serial = $this->session->userdata('branch_serial');

		$data['permission'] = $this->session->userdata('admin_permission');
		$data['admin_id'] = $this->session->userdata('admin_id');
		$data['admin_name'] = $this->admin->get_admin_name();
		$data['company_name'] = $this->session->userdata('company_name');
		$data['company_list'] = $this->company->get_company($company_serial);
		// $data['branch_list'] = $this->company->get_branch($company_serial);
		$data['staff_list'] = $this->staff->get_list($company_serial);

		$schedule_html_data['company_serial'] = $company_serial;
		$schedule_html_data['year'] = date("Y");
		$schedule_html_data['month'] = date("m");
		$schedule_html_data['day'] = date("d");
		$schedule_html_data['schedule_mode'] = "date";

		$data['schedule_html'] = $this->rent->get_schedule_html($schedule_html_data);
		$data['company_serial'] = $company_serial;
		// $data['branch_serial'] = $branch_serial;
		$data['period_price'] = $this->fare->get_period_price($company_serial);
		$data['special_price'] = $this->fare->get_special_price($company_serial);
		$data['schedule_mode'] = $this->fare->get_special_price($company_serial);
		$data['정상'] = $this->carmanager->get_count($company_serial, '정상');
		$data['예약가능차량'] = $this->rent->get_count($company_serial);
		// $data['deposite_way'] = $this->rent->get_deposite_ways($company_serial);
		$this->load->view("admin/admin_layout_top", $data); 
//		$this->load->view("admin/admin_status", $data); 
//		$this->load->view("admin/layout_left", $data); 
		$this->load->view($this->template_file, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function search_user_information() {
		$search_string = $this->input->get('search_string',TRUE);
		$result = $this->rent->get_search_user_information($search_string);

		echo json_encode($result);
	}

	function get_company_all_user($company_serial){
		$result = $this->rent->get_company_user($company_serial);
		echo json_encode($result);
	}

	function get_branch_list() {
		$company_serial = $this->input->post('company_serial');
		$branch_list = $this->company->get_branch($company_serial);

		echo json_encode($branch_list);
	}

	function get_shcedule_html(){
		$company_serial = $this->input->post('company_serial');
		$branch_serial = $this->input->post('branch_serial');
		$new_date = $this->input->post('new_date');
		$schedule_mode = $this->input->post('schedule_mode');
		$search_keyword = $this->input->post('search_keyword');
		$order_kind = $this->input->post('order_kind');
		$scheduler_car_type = $this->input->post('scheduler_car_type');

		$schedule_html_data['company_serial'] = $company_serial;
		$schedule_html_data['branch_serial'] = $branch_serial;
		$schedule_html_data['search_keyword'] = $search_keyword;
		$schedule_html_data['year'] = substr($new_date, 0, 4);
		$schedule_html_data['month'] = substr($new_date, 4, 2);
		$schedule_html_data['day'] = substr($new_date, 6, 2);
		$schedule_html_data['schedule_mode'] = $schedule_mode;
		$schedule_html_data['order_kind'] = $order_kind;
		$schedule_html_data['scheduler_car_type'] = $scheduler_car_type;

		$schedule_html = $this->rent->get_schedule_html($schedule_html_data);
		echo json_encode($schedule_html);
	}

	function get_rental_period(){
		$company_serial = $this->input->post('company_serial');
		$branch_serial = $this->input->post('branch_serial');
		$car_serial = $this->input->post('car_serial');
		$period_start = $this->input->post('period_start');
		$period_finish = $this->input->post('period_finish');
		$period_price_serial = $this->input->post('period_price_serial');
		$special_price_serial = $this->input->post('special_price_serial');

		for($i=0;$period_finish>=date("YmdHi", strtotime($period_start." +".($i+1)." days"));$i++){
			$day = date("w", strtotime($period_start." +".$i." days"));

			if($day < '1' || $day > '5'){
				//평일
				$weekend_count ++;
			} else {
				//주말
				$week_count ++;
			}

		}



		$last_date = date("YmdHi", strtotime($period_start." +".$i." days"));
		$result['code'] = "S01";
		$day_count = $week_count + $weekend_count;
		$hour_count = ceil(gmdate('Hi', strtotime($period_finish) - strtotime($last_date)) / 100);



		$result['contract_period'] = number_format($week_count + $weekend_count)."일 ".number_format($hour_count)."시간";
		$car_information = $this->carmanager->get_car_information($company_serial, $car_serial);
		$day_price = $car_information['0']['week_price'];
		$original_day_price = $day_price;

		$basic_price = $this->fare->get_basic_price($company_serial);

		$result['period_price_serial'] = $period_price_serial;
		$result['special_price_serial'] = $special_price_serial;

		if($special_price_serial != 'N')
		{
			$sql = "
			SELECT
				*
			FROM
				special_price
			WHERE
				company_serial = '".$company_serial."'
				AND flag = 'Y'
				AND start_date <= '".$period_finish."'
				AND end_date >= '".$period_start."'
			";
			$result_special_price = $this->db->fReadSql($sql);
		}

		$week_price_24 = $car_information['0']['week_price'];
		$week_price_6 = $basic_price['0']['week_6'];
		$week_price_1 = $basic_price['0']['week_1'];
		$weekend_price_24 = $car_information['0']['weekend_price'];
		$weekend_price_6 = $basic_price['0']['weekend_6'];
		$weekend_price_1 = $basic_price['0']['weekend_1'];

		if($day_count < '1' && $hour_count >= '1'){
			//하루미만 렌탈
			//특정일 할인요금 적용
			for($i=0;$i<count($result_special_price);$i++){
				$sp_start_date = $result_special_price[$i]['start_date'];
				$sp_finish_date = $result_special_price[$i]['end_date'];

				if($period_start >= $sp_start_date && $period_start <= $sp_start_date){
					$special_price = ($result_special_price[$i]['percent'] / 100);
					$result['special_price_serial'] = $result_special_price[$i]['serial'];
				}
			}

			$day = date("w", strtotime($period_start));
			if($day < '1' || $day > '5'){
				//주말요금 적용
				$price_24 = $weekend_price_24;
				$price_6 = $weekend_price_24 * ($weekend_price_6 / 100);
				$price_1 = $weekend_price_24 * ($weekend_price_1 / 100);
			} else {
				//평일요금 적용
				$price_24 = $week_price_24;
				$price_6 = $week_price_24 * ($week_price_6 / 100);
				$price_1 = $week_price_24 * ($week_price_1 / 100);
			}

			if($hour_count <= '6'){
				$result['original_rental_price'] = $price_6;
				$result['rental_price'] = $result['original_rental_price'] * $special_price;
			} else {
				$result['original_rental_price'] = $price_6 + ( $price_1 * ($hour_count - 6) );
				$result['rental_price'] = $result['original_rental_price'] * $special_price;
				if($result['original_rental_price'] >= $price_24 ){
					$result['original_rental_price'] = $price_24;
				}
				if($result['rental_price'] >= ($price_24 * $special) ){
					$result['rental_price'] = $price_24 * $special;
				}

			}
		} else {
			//하루이상 렌탈

			//특정일 할인요금 적용

			for($i=0;$i<count($result_special_price);$i++){
				$sp_start_date = $result_special_price[$i]['start_date'];
				$sp_finish_date = $result_special_price[$i]['end_date'];


				for($j=0;$sp_finish_date>=date("YmdHi", strtotime($sp_start_date." +".($j+1)." days"));$j++){
					$check_date = date("Ymd", strtotime($sp_start_date." +".($j+1)." days"))."0000"; //체크할 날짜
					$day = date("w", strtotime($sp_start_date." +".($j+1)." days")); //체크할 요일
					if($check_date >= $period_start && $check_date <= $period_finish){ // 특정일 할인요금 기간 내라면,
						$result['special_price_serial'] = $result_special_price[$i]['serial']; // 특정일 할인요금제 시리얼번호
						$special_price = ($result_special_price[$i]['percent'] / 100);
						if($day < '1' || $day > '5'){
							//주말
							$weekend_count_sp ++;
						} else {
							//평일
							$week_count_sp ++;
						}
					}
				}

			}

			$result['original_rental_price'] = $week_count * $week_price_24;
			$result['original_rental_price'] += ($weekend_count * $weekend_price_24);
			$result['rental_price'] = ( ($week_count - $week_count_sp) + ($week_count_sp * $special_price) ) * $week_price_24 ;
			$result['rental_price'] += ( ($weekend_count - $weekend_count_sp) + ($weekend_count_sp * $special_price) ) * $week_price_24 ;


			if($hour_count >= '1'){
				//특정일 할인요금 적용
				for($i=0;$i<count($result_special_price);$i++){
					$sp_start_date = $result_special_price[$i]['start_date'];
					$sp_finish_date = $result_special_price[$i]['end_date'];

					if($period_finish >= $sp_start_date && $period_finish <= $sp_finish_date){
						$special_price = $result_special_price[$i]['percent'] / 100;
					}
				}

				$day = date("w", strtotime($period_finish));
				if($day < '1' || $day > '5'){
					//주말요금 적용
					$price_24 = $weekend_price_24;
					$price_6 = $weekend_price_24 * ($weekend_price_6 / 100);
					$price_1 = $weekend_price_24 * ($weekend_price_1 / 100);
				} else {
					//평일요금 적용
					$price_24 = $week_price_24;
					$price_6 = $week_price_24 * ($week_price_6 / 100);
					$price_1 = $week_price_24 * ($week_price_1 / 100);
				}

				$original_hour_price =  $price_1 * $hour_count;
				if($original_hour_price >= $price_24 ){
					//일별요금계산후 남은 시간에 대한 계산
					$original_hour_price = $price_24;
				}

				$hour_price =  $original_hour_price * $special_price;
				if($hour_price >= $price_24 * $special_price){
					//일별요금계산후 남은 시간에 대한 계산
					$hour_price = $price_24 * $special_price;
				}

				
				$result['rental_price'] += $hour_price;
				$result['original_rental_price'] += $original_hour_price;
			}
		}
		if($period_price_serial >= '1' && $period_price_serial != 'N'){
			$sql = "
			SELECT
				*
			FROM
				period_price
			WHERE
				serial = '".$period_price_serial."'
				AND company_serial = '".$company_serial."'
				AND flag = 'Y'
			";
			$result_period_price = $this->db->fReadSql($sql);
		} else if($period_price_serial != 'N'){
			$sql = "
			SELECT
				*
			FROM
				period_price
			WHERE
				company_serial = '".$company_serial."'
				AND flag = 'Y'
				AND days <= '".$day_count."'
			ORDER BY
				days DESC
			LIMIT 0,1
			";
			$result_period_price = $this->db->fReadSql($sql);
		}
		if($result_period_price['0']['percent']){
			$result['period_price_serial'] =$result_period_price['0']['serial'];
			$result['rental_price'] = $result['rental_price'] * ($result_period_price['0']['percent'] / 100);
		}

		echo json_encode($result);
	}

	function change_car_flag(){
		$company_serial = $this->input->post('company_serial');
		// $branch_serial = $this->input->post('branch_serial');
		$car_serial = $this->input->post('car_serial');
		$car_flag = $this->input->post('car_flag');

		$user = $this->session->userdata('admin_name');

		if($car_flag){
			$write_data = array(
				'company_serial' => $company_serial,
				'serial' => $car_serial,
				'flag' => $car_flag,
				'edited_ip' => $_SERVER['REMOTE_ADDR'],
				'edited_user' => $user,

			);

			$result = $this->carmanager->update_car($write_data);
			if($result['code'] == "S01"){
				$rengo_count = $this->carmanager->get_rengo_count($company_serial, $branch_serial);
				$result['message'] = $rengo_count;
			}
			echo json_encode($result);
		} else {
			//대여 관리 예약 등록에서 차량선택시 홀딩을 위해 원래 flag 값이 Y였다면 flag를 N으로 변경하고,  모달 윈도우를 내릴 때, 홀딩했던 차량을 원래대로 돌려놓기 위해서 return에 Y를 보냄
			$original_car_flag = $this->rent->get_car_flag($car_serial);
			if($original_car_flag == "Y"){
				$write_data = array(
					'company_serial' => $company_serial,
					'serial' => $car_serial,
					'flag' => 'N',
					'edited_ip' => $_SERVER['REMOTE_ADDR'],
					'edited_user' => $user,

				);

				$result = $this->carmanager->update_car($write_data);
				if($result['code'] == "S01"){
					$result['message'] = "Y";
				}
				echo json_encode($result);
			} else {
				$result['code'] = "S01";
				$result['message'] = "N";
				echo json_encode($result);
			}

		}
	}

	function set_holiday_car(){
		$holiday = $this->input->post('holiday');
		$company_serial = $this->input->post('company_serial');
		$car_serial = $this->input->post('car_serial');

		$result = $this->rent->set_holiday($holiday, $company_serial, $car_serial);
		echo json_encode($result);
	}


	function set_holiday(){
		$holiday = $this->input->post('holiday');
		$company_serial = $this->input->post('company_serial');

		$result = $this->rent->set_holiday($holiday, $company_serial);
		echo json_encode($result);
	}

	function save_order(){
		$result = $this->rent->save_order();
		echo json_encode($result);
	}

	function order_detail(){
		$serial = $this->input->post('serial');
		$result = $this->rent->order_detail($serial);
		echo json_encode($result);
	}

	function delete_order(){
		$serial = $this->input->post('serial');
		$result = $this->rent->delete_order($serial);
		echo json_encode($result);
	}
	//보험대차 정보 얻기
	function insurance_detail(){
		$serial = $this->input->post('serial');
		$result = $this->rent->insurance_detail($serial);
		echo json_encode($result);
	}

}
?>
