<?php
class Registerpending extends CI_Controller {

	function __construct()
	 { 
		parent::__construct();

	    $this->load->model('admin/Admin_model', 'admin');
	    // $this->load->model('Usermanager_model','usermanager');
	    // $this->load->model('Company_model','company');
  		$menu_permission = 2;
  		$permission = $this->admin->_check_permission($menu_permission);
  		if($permission != "Y")
   		$this->admin->admin_logout(); 

	} 

	function _view($url, $data = '')
	{
		// $data['permission'] = $this->admin->get_menu_permission();
  // 		$data['admin_name'] = $this->admin->get_admin_name();
  // 		$data['company_serial'] = $this->session->userdata('company_serial');
		// $data['company_name'] = $this->session->userdata('company_name');
		$this->load->view("admin/admin_layout_top", $data); 
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index()
	{
		// $company_serial = $this->session->userdata('company_serial');
		// $data['company_serial'] = $company_serial;
		// $data['company_list'] = $this->company->get_company($company_serial);
		// $data['branch_list'] = $this->company->get_branch($company_serial);
		$this->_view("admin/Registerpending_view", $data); 
		
	}

}
?>