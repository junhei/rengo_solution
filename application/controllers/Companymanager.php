<?php
class Companymanager extends CI_Controller {


	function __construct() { 

		parent::__construct();
		$this->load->model('admin/Admin_model', 'admin');
		$this->load->model('Company_model','company');
		// 관리자 메뉴 접근 퍼미션 체크
		$menu_permission = 5;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y")
			$this->admin->admin_logout(); 
	}

	function _view($url, $data = ''){

		$data['admin_id'] = $this->session->userdata('admin_id');
		$data['permission'] = $this->session->userdata('admin_permission');
		$this->load->view("admin/admin_layout_top", $data);
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$company_serial = $this->session->userdata('company_serial');
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
	
		$data['company_list'] = $this->company->get_company($company_serial);
		$data['branch_list'] = $this->company->get_branch($company_serial);
		$this->_view("preference/company_manager_view", $data); 
	}


	
	function get_branch($company_serial){
		echo json_encode($this->company->get_branch($company_serial,"option"));
	}

	function add_branch(){
		$write_data = array(
			'company_serial' => intval($this->input->post('company_serial', TRUE)),
			'branch_name' => urldecode($this->input->post('branch_name', TRUE)),
			'branch_address' => urldecode($this->input->post('branch_address', TRUE)),
			'branch_phone' => urldecode($this->input->post('branch_phone', TRUE)), //차량번호
			'flag' => 'Y', //차량상태
		);
		$result = $this->company->add_branch($write_data);
		echo json_encode($result);
	}

	function update_branch(){
		$write_data = array(
			'serial' => intval($this->input->post('serial', TRUE)),
			'branch_name' => urldecode($this->input->post('branch_name', TRUE)),
			'branch_address' => urldecode($this->input->post('branch_address', TRUE)),
			'branch_phone' => urldecode($this->input->post('branch_phone', TRUE)), //차량번호
		);
		$result = $this->company->update_branch($write_data);
		echo json_encode($result);
	}

	function delete_branch(){
		$result = $this->company->delete_branch($this->input->post('serial', TRUE));
		echo json_encode($result);
	}

	





}
?>