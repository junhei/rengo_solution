<?php
class Makermanager extends CI_Controller {


	function __construct() { 

		parent::__construct();

		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		
		$this->load->model('admin/Admin_model', 'admin');
		$this->load->model('Makermanager_model','makermanager');
		// 관리자 메뉴 접근 퍼미션 체크
		$menu_permission = 6;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y")
			$this->admin->admin_logout(); 

	

//			$this->output->enable_profiler(TRUE);
	}

	function _view($url, $data = ''){

		$data['admin_id'] = $this->session->userdata('admin_id');
		$data['permission'] = $this->session->userdata('admin_permission');
		$this->load->view("admin/admin_layout_top", $data);
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$data['company_name'] = $this->session->userdata('company_name');
		$this->_view("master/maker_manager_view", $data); 
	}

	function get_list(){
		$maker_data_array = $this->makermanager->get_list();
		foreach($maker_data_array as $maker_data){
			if($maker_data['flag']=='Y'){
				$status = "사용";
			}else{
				$status = "중지";
			}
			$car_count = $this->makermanager->get_car_count(intval($maker_data['maker_index']));
			$maker_send_array[] = array(
					"index" => intval($maker_data['maker_index']),
					"name" => $maker_data['maker_name'],
					"status" => $status,
					"car_count" => $car_count,
					"country" =>  $maker_data['country']
				);
		}
		
		if($maker_send_array == null){
			$maker_send_array = array();
		}

		echo json_encode($maker_send_array);
	}

	function get_active_list(){
		$maker_data_array = $this->makermanager->get_active_list();
		echo json_encode($maker_send_array);
	}
	function register(){

		if(!$this->input->post('name', TRUE)){
			alert('제조사 이름을 입력해 주세요.');
			exit;
		}

		$status = $this->input->post('status', TRUE);
		$flag = "N";
		if($status == "사용" || $stats =="사용중"){
			$flag = "Y";
		}else{
			$flag = "N";
		}

		$write_data = array(
			'maker_name' => $this->input->post('name', TRUE),
			'country' => $this->input->post('country', TRUE),
			'flag' => $flag
		);

		$result = $this->makermanager->add($write_data);
		echo json_encode($result);
	}
	//
	function update(){

		if(!$this->input->post('index', TRUE)){
			alert('제조사 id가 없습니다.');
			exit;
		}
		if(!$this->input->post('name', TRUE)){
			alert('제조사 이름을 입력해 주세요.');
			exit;
		}

		$status = $this->input->post('status', TRUE);
		$flag = "N";
		if($status == "사용" || $stats =="사용중"){
			$flag = "Y";
		}else{
			$flag = "N";
		}

		$write_data = array(
			'maker_index' => $this->input->post('index', TRUE),
			'maker_name' => $this->input->post('name', TRUE),
			'country' => $this->input->post('country', TRUE),
			'flag' => $flag
		);

		$result = $this->makermanager->update($write_data);
		echo json_encode($result);
	}

	function delete(){

		if(!$this->input->post('index', TRUE)){
			alert('제조사 id가 없습니다.');
			exit;
		}
		
		$result = $this->makermanager->delete($this->input->post('index', TRUE));
		echo json_encode($result);
	}
	
}
?>