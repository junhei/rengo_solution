<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 require FCPATH.'vendor/autoload.php';

class Registermanager extends CI_Controller {

	function __construct() { 
		parent::__construct();
		// $this->load->model('Registermanager_model','registermanager');
		$this->load->model('Company_model','company');
	 	$this->load->library('Rengo_encryption');
		$this->load->library('upload');
	} 

	function _view($url, $data = ''){
		$this->load->view($url, $data); 
	}

	function index(){
		// $data['bad']=$this->blacklist->get_list();
		$this->_view("/admin/admin_register_agree"); 
	}

	function file_upload() //이미지 업로드용
	{	

		$s3 = new Aws\S3\S3Client([
				    'version' => 'latest',
				    'region'  => 'ap-northeast-2',
				    'credentials' => [
				    	'key' => 'AKIAIMJGNFUPDENK47JA',
						'secret' => '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'
					]
		]);

		$company_serial = $this->input->post('company_serial',TRUE);	
		$config['upload_path'] = '/home/apache/uploads/company/'.$company_serial."/";

		if(!is_dir($config['upload_path'])){
			$result = @mkdir($config['upload_path']);
			if($result == false){
				$error = array('error' => '폴더 생성 오류. 다시 시도해 주세요.');
				$this->load->view("/admin/admin_register_add_information", $error);
				return;
			}
		}

		$filename = array('business_regist','bankbook');

		$config['allowed_types'] = 'pdf|gif|jpg|png|jpeg';
		$config['max_size']	= '10000';
		$config['max_width']  = '1024000';
		$config['max_height']  = '768000';

		$upload_file_information['serial'] = $company_serial;

		foreach($filename as $fn){
			if($_FILES[$fn]['name'] == "" || $_FILES[$fn]['size'] < '1')continue;
			$uploaded_file = $config['upload_path'].$_FILES[$fn]['name'];
			$temp = explode(".", $_FILES[$fn]['name']);
			$확장자 = $temp[(count($temp)-1)];
//			$확장자 = "jpg";
			// $new_file = $config['upload_path'].$fn.'.'.$확장자;
			$encry_file_name = $this->rengo_encryption->encryption($fn);
			$new_file = $config['upload_path'].$encry_file_name.'.'.$확장자;


			if(is_file($new_file)){
		//			@unlink($new_file);
				@rename($new_file, $new_file."_".date("YmdHis"));
			}

			// $config['file_name'] = $fn.'.'.$확장자;
			$config['file_name'] = $encry_file_name.'.'.$확장자;
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload($fn)) {
				$error = array('error' => $this->upload->display_errors());
			}	
			else {
				$data = array('upload_data' => $this->upload->data());
				$upload_file_information['file_'.$fn] = $config['file_name'];

				$this->company->register_company_file($upload_file_information);
				// Send a PutObject request and get the result object.
				$result = $s3->putObject([
						'Bucket' => 'private.information.rengo',
					    'Key'    => $company_serial."/".$config['file_name'],
						'Body'   => fopen( $config['upload_path'].$config['file_name'], 'r')
				 ]);
			}	
		}
		if($error){
			$this->load->view("/admin/admin_register_add_information", $error);
		} else {
			//회원가입후 파일 업로드 성공
			$this->_send_email($company_serial);
			$this->_view('/admin/admin_register_finish', $data);

		}
	}

	function duplicate_test() {
		$test_value=$this->input->post('value',TRUE);
		$test_column=$this->input->post('column',TRUE);
		$result=$this->company->duplicate_test($test_value,$test_column);

		if($result==null){
			$data='null';
		} else {
			$data=$result;
		}
		echo json_encode($data);
	}



	function company_info_register() {
		$email=$this->input->post('email',TRUE);
		$password=$this->input->post('password',TRUE);
		$company_name=$this->input->post('company_name',TRUE);
		$representative_number=$this->input->post('representative_number',TRUE);
		$fax=$this->input->post('fax',TRUE);
		$postcode=$this->input->post('postcode',TRUE);
		$address=$this->input->post('address',TRUE);
		$start_time=$this->input->post('start_time',TRUE);
		$end_time=$this->input->post('end_time',TRUE);
		$delivery_max_count=$this->input->post('delivery_max_count',TRUE);
		$representative=$this->input->post('representative',TRUE);
		$representative_tel=$this->input->post('representative_tel',TRUE);
		$corporation_number=$this->input->post('corporation_number',TRUE);
		$cr_number=$this->input->post('cr_number',TRUE);
		// $corporate_license_number=$this->input->post('corporate_license_number',TRUE);
		// $register_number=$this->input->post('register_number',TRUE);
		// $owner_name=$this->input->post('owner_name',TRUE);
		// $phone=$this->input->post('phone',TRUE);
		// $tel=$this->input->post('tel',TRUE);
		// $company_address=$this->input->post('company_address',TRUE);
		

		$write_data = array(
			'email' => $email,
			'company_name' => $company_name,
			'password' => $password,
			'tel' => $representative_number,
			'fax' => $fax,
			'postcode' => $postcode,
			'company_address' => $address,
			'open_time_start' => $start_time,
			'open_time_finish' => $end_time,
			'delivery_max_per_hour' => $delivery_max_count,
			'owner_name' => $representative,
			'phone' => $representative_tel,
			'corporate_license_number' => $corporation_number,
			'register_number' => $cr_number,
			);

		$result=$this->company->register_company($write_data);
		echo json_encode($result);
	}

	function agreement(){
		$agree = $this->input->post('agree',TRUE);
		if($agree == 'Y'){
			$this->_view("/admin/admin_register_add_information");
			// echo ''; 
		}else{
			$this->_view("/admin/admin_register_agree"); 
			// echo ''; 
		}

	}

	function finish(){
		$this->_view('/admin/admin_register_finish', $data);
	}

	function auth($encoded_code){
		//code 복호화 로직 필요함
		$company_serial = $this->rengo_encryption->decription($encoded_code);
		$result = $this->company->add_new_admin($company_serial);
		echo $result;

	}

	function _send_email($company_serial){
		// 메일발송 시작
	    $nameFrom  = "렌고";
	    $mailFrom = "rengo@co.kr";
	    //todo 관리자 메일로 변경
	    $nameTo  = "노성환";
	    $mailTo = "kkarynossang@gmail.com";
	    // $cc = "참조";
	    // $bcc = "숨은참조";
		$subject ="파트너 승인 요청!";
		$message = "파트너 승인을 요청합니다.</br>".$company_serial." 번 파트너의 계좌 정보 및 기타 장보를 확인하시고 인증 메일을 보내주세요.</br>더욱 편리한 서비스를 제공하기 위해 최선을 다하겠습니다.";

	    
	    $charset = "UTF-8";


	    $nameFrom   = "=?$charset?B?".base64_encode($nameFrom)."?=";
	    $nameTo   = "=?$charset?B?".base64_encode($nameTo)."?=";
	    $subject = "=?$charset?B?".base64_encode($subject)."?=";

	    $header  = "Content-Type: text/html; charset=utf-8\r\n";
	    $header .= "MIME-Version: 1.0\r\n";

	    $header .= "Return-Path: <". $mailFrom .">\r\n";
	    $header .= "From: ". $nameFrom ." <". $mailFrom .">\r\n";
	    $header .= "Reply-To: <". $mailFrom .">\r\n";
	    if ($cc)  $header .= "Cc: ". $cc ."\r\n";
	    if ($bcc) $header .= "Bcc: ". $bcc ."\r\n";

	    $result = mail($mailTo, $subject, $message, $header, $mailFrom);
	    if(!$result){
	    	// echo 'fail';
	    		// json_encode("fail");
	    }else{
	    	// echo 'success';
	    		// json_encode("success");
	    }
	}

}
?>