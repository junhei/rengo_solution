<?php
// include_once "/home/apache/sms/xmlrpc.inc.php";
// include_once "/home/apache/sms/class.EmmaSMS.php";

class Registermanager2 extends CI_Controller {


	function __construct() { 
		parent::__construct();
		// $this->load->model('admin/Admin_model', 'admin');
		// $this->load->model('Company_model','company');	 
	} 

	function _view($url, $data = ''){
		$this->load->view($url, $data); 
	}

	function index(){
		$this->_view("admin/admin_register_pw_change",$data); 
	}

	function change_password(){
		$id = $_POST['id'];
		$pw = str_replace("-", "", trim($_POST['pw']));
		$auth = str_replace("-", "", trim($_POST['auth']));

		$sql = "UPDATE
				admin_information
			SET
				admin_pw = PASSWORD('".$pw."')
			WHERE
				admin_id = '".$id."'
			AND 
				auth_code = '".$auth."'";

		$result = $this->db->query($sql, '');
		$result_row_count = $this->db->affected_rows();
		if($result_row_count > 0){
			echo "OK";
		}else{
			echo "비밀번호 변경에 실패하였습니다.";
		}

	}

	function send_auth_mail(){


		$id = $_POST['id'];

		$sql = "
		SELECT
			A.serial, A.phone_number, A.admin_name, A.admin_id, A.registered_date, C.company_name
		FROM
			admin_information AS A, company_information AS C
		WHERE
			A.admin_id='".$id."'
		AND
		    A.company_serial = C.serial";

		$result = $this->db->fReadSql($sql);
		if($result['0']['serial'] >= '1') {
			//랜덤 인증 코드 생성 알고리즘
			$auth_code_temp = explode(" ",microtime());
			$auth_code = intval(substr($auth_code_temp[0], strpos($auth_code_temp[0], ".")+1)+$auth_code_temp[1]);
			$length_auth_code = strlen($auth_code);
			$rand_string = '0123456789'; 
			$strlen_string = strlen($rand_string); 

			for($i=0;$i<$length_auth_code;$i++){
				$new_auth_code .= $rand_string[rand(0,$strlen_string)].substr($auth_code, $i, rand(0,3));
			}
			$new_auth_code = substr($new_auth_code, 0, 4);

			// $sms_id = "seungw251";
			// $sms_passwd = "seungw251";
			// $sms_type = "L";
			// $sms_to = $result['0']['phone_number'];
			// $sms_from = "01099048875"; // 11월19일 추가
			// $sms_date = '';
			// $sms_msg = $new_auth_code;

			// $sms_subject = "[렌고 파트너 인증코드]";

			// $sms = new EmmaSMS();
			// $sms->login($sms_id, $sms_passwd);

			// $ret = $sms->send($sms_to, $sms_from, $sms_msg, $sms_date, $sms_type, $sms_subject);
			// // echo "1";
			// if($ret){
			// 	$sql = "
			// 	UPDATE
			// 		admin_information
			// 	SET
			// 		auth_code = '".$new_auth_code."'
			// 	WHERE
			// 		serial = ".intval($result['0']['serial']);
			// 	echo "OK";
			// 	$this->db->query($sql);
			// }
			// else {
			// 	$return_message = $sms->errMsg;
			// 	echo $return_message;
			// }

			// log_sms($sms_to, $sms_from, $sms_msg, $sms_date, $sms_type, $return_message, $sms_subject);

				// 메일발송 시작
			    $nameFrom  = "렌고";
			    $mailFrom = "rengo@co.kr";
			    $nameTo  = $result[0]['company_name'];
			    $mailTo = $id;
			    // $cc = "참조";
			    // $bcc = "숨은참조";
				$subject ="비밀번호 변경을 위한 인증번호를 전송하였습니다.";
				//암호화 로직 추가해야 함
				$message = '

<!DOCTYPE html>
<html>
<head>
    <title>RENGO SOLUTION</title>
</head>
<body>

  <p class="height:30px;"></p>

    <table style="max-width: 600px; width: 100%; margin: 0 auto; padding: 0 15px; font-family: "나눔고딕", "나눔바른고딕", "돋움", "고딕"; font-size:14px; line-height:1.5em; color:#252525;" cellpadding="0" border="0" cellspacing="0">
      <tr>
        <td style="padding:0 30px;">안녕하세요. 렌고입니다. </td>
      </tr>
      <tr>
        <td style="padding:0 30px;">요청하신 비밀번호 변경을 위한 인증번호 발송메일 입니다. </td>
      </tr>
      <tr>
        <td style="height:18px;"></td>
      </tr>
      <tr>
        <td style="padding:0 30px;">[비밀번호 변경 내역]</td>
      </tr>
      <tr>
        <td style="padding:0 30px;">아이디(이메일) : <span style="color:#252525">'.$id.'</span></td>
      </tr>
        <td style="padding:0 30px;">파트너명 : '.$result[0]['company_name'].'</td>
      <tr>
        <td style="padding:0 30px;">발급일시 : '.$result[0]['registered_date'].'</td>
      </tr>
      <tr>
        <td style="height:18px;"></td>
      </tr>
      <tr>
        <td style="padding:0 30px;">발급받으신 인증번호를 입력후 비밀번호를 변경해 주세요.</td>
      </tr>
      <tr>
        <td style="padding:0 30px;">인증번호 :</td>
      </tr>
      <tr>
        <td style="height:15px;"></td>
      </tr>
      <tr>
        <td style="padding:0 30px;"><span style="padding:20px; font-size:18px; background:#ffe80e; display:inline-block; text-align:center;">'.$new_auth_code.'</span></td>
      </tr>
      <tr>
        <td style="height:30px;"></td>
      </tr>
      <tr>
        <td style="padding:0 30px;">감사합니다.</td>
      </tr>
      <tr>
        <td>
          <table style="background:#ffe80e; width:100%; padding:30px; margin-top:30px; margin-bottom:30px;">
            <tr>
              <td></td>
            </tr>
            <tr>
              <td>
                (주)렌고<br />
                대표 : 이승원 I 사업자번호 : 302-81-29052<br />
                통신판매업신고 : 제2016-부산금정-0119호<br />
                개인정보담당자 : swlee@rengo.co.kr<br />
                고객센터 : 1800-1090<br />
                ⓒ 2017 rengo All rights reserved.
              </td>
              <td style="text-align:right; vertical-align:bottom;"><img src="http://solution.rengo.co.kr/img/L1_black.png" class="" width="40" height="40"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
</body>
</html>';
	    
			    $charset = "UTF-8";


			    $nameFrom   = "=?$charset?B?".base64_encode($nameFrom)."?=";
			    $nameTo   = "=?$charset?B?".base64_encode($nameTo)."?=";
			    $subject = "=?$charset?B?".base64_encode($subject)."?=";

			    $header  = "Content-Type: text/html; charset=utf-8\r\n";
			    $header .= "MIME-Version: 1.0\r\n";

			    $header .= "Return-Path: <". $mailFrom .">\r\n";
			    $header .= "From: ". $nameFrom ." <". $mailFrom .">\r\n";
			    $header .= "Reply-To: <". $mailFrom .">\r\n";
			    if ($cc)  $header .= "Cc: ". $cc ."\r\n";
			    if ($bcc) $header .= "Bcc: ". $bcc ."\r\n";

			    $res = mail($mailTo, $subject, $message, $header, $mailFrom);
			    if(!$res){
				    echo 'mail 전송이 실패하였습니다.';		
			    }else{
			    	$sql = "
						UPDATE
							admin_information
						SET
							auth_code = '".$new_auth_code."'
						WHERE
							serial = '".$result['0']['serial']."'";

					$re = $this->db->query($sql);
					if($re){
						echo 'OK';
					}else{
						echo $this->db->error();
					}
					
			    }


		}else{
			echo "아이디를 확인해 주세요.";
		}
	}
}
?>