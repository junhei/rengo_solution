<?php
class Login extends CI_Controller {

	function __construct() { 
		parent::__construct();
	} 

	function _view($url, $data = ''){
		$this->load->view($url, $data); 
	}

	function index(){
		$data['redirect_uri'] = $this->input->get('redirect_uri');
		$data['image_list'] = $this->get_image_name();
		$this->session->sess_destroy(); 
		$this->_view("admin/login_view",$data); 
	}

	function dot($var) {
		return $var!='.';
	}

	function get_image_name() {
		$dir_handle=opendir('/home/apache/htdocs/login_img');
		$i=0;
		while($file=readdir($dir_handle)){
			$fname=$file;
			if($fname == '..') 
				continue;
			$image_list[$i++]=$fname;
		}

		closedir($dir_handle);
		sort($image_list);
		array_shift($image_list);
		return json_encode($image_list);
	}
}
?>