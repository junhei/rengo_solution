<?php
class Carmanager extends CI_Controller {


	function __construct() { 

		parent::__construct();
		$this->load->model('admin/Admin_model', 'admin');
		$this->load->model('Car_model','carmanager');
		$this->load->model('Carmaster_model','carmaster');
		$this->load->model('Company_model','company');
		$this->load->model('Makermanager_model','makermanager');
		// 관리자 메뉴 접근 퍼미션 체크
		$menu_permission = 1;
		$permission = $this->admin->_check_permission($menu_permission);
		if($permission != "Y")
			$this->admin->admin_logout(); 

	

//			$this->output->enable_profiler(TRUE);
	}

	function _view($url, $data = ''){

		$this->load->view("admin/admin_layout_top", $data);
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index(){
		$company_serial = $this->session->userdata('company_serial');
		$data['company_serial'] = $company_serial;
		$data['company_name'] = $this->session->userdata('company_name');
		$data['permission'] = $this->session->userdata('admin_permission');
		$data['admin_id'] = $this->session->userdata('admin_id');
		$data['company_list'] = $this->company->get_company($company_serial);
		$data['전체'] = $this->carmanager->get_count($company_serial, "전체");
		$data['정상'] = $this->carmanager->get_count($company_serial, '정상');
		$maker_list = $this->makermanager->get_list();
		$data['maker_list'] = $maker_list;
		$car_name_list = $this->carmaster->get_model_list_by_maker($maker_list[0]['maker_index']);
		$data['carmodel_list'] = $car_name_list;
		$data['carmodel_detail_list'] = $this->carmaster->get_detail_list_by_name(urldecode($car_name_list[0]['car_name']));
		
		$this->_view("car/car_manager_view", $data); 
	}

	function get_car_model($car_index){
		echo json_encode($this->carmaster->get($car_index));
	}

	function get_car_model_detail_list($car_name){
		echo json_encode($this->carmaster->get_detail_list_by_name(urldecode($car_name)));
	}

	//제조사 선택시 차량 모델 이름 가져오기
	function get_car_model_list($maker_index){
		echo json_encode($this->carmaster->get_model_list_by_maker($maker_index));
	}

	//branch id별로 차들 구하기
	// function get_car_branch($id){
	// 	echo json_encode($this->company->get_branch($id));
	// }

	//업체 선택할때 count 구하기
	function get_counts($company_serial){
			$count_array = array(
					'all' => $this->carmanager->get_count($company_serial,  "전체"),
					'run' => $this->carmanager->get_count($company_serial,  "정상")

				);
		echo json_encode($count_array);
	}

	//차량 정보 리스트
	function get_list($company_serial, $type = '', $period = ''){

		if($period != ''){
			$car_data_array = $this->carmanager->get_list_period($company_serial, $type, $period);
		} else {
			$car_data_array = $this->carmanager->get_list($company_serial, $type);
		}		

		// $company_serial = $this->session->userdata('company_serial');
		// $car_data_array = $this->carmanager->get_list($company_serial, 0, $type);
		// var_dump($car_data_array);

		foreach($car_data_array as $car_data){
			//옵션 읽어오기
			$options = "";
			if($car_data["option_navi"]=="Y" || $car_data["option_navi"]=="y"){
				
				$options = $options."네비게이션, ";
			}
			if($car_data["option_bluetooth"]=="Y" || $car_data["option_bluetooth"]=="y"){
				$options = $options."블루투스, ";
			}
			if($car_data["option_automatic_back_mirror"]=="Y" || $car_data["option_automatic_back_mirror"]=="y"){
				$options = $options."자동백미러, ";
			}
			if($car_data["option_hi_pass"]=="Y" || $car_data["option_hi_pass"]=="y"){
				$options = $options."하이패스, ";
			}
			if($car_data["option_aux"]=="Y" || $car_data["option_aux"]=="y"){
				$options = $options."AUX, ";
			}
			if($car_data["option_heatseat"]=="Y" || $car_data["option_heatseat"]=="y"){
				$options = $options."히트시트, ";
			}
			if($car_data["option_gps"]=="Y" || $car_data["option_gps"]=="y"){
				$options = $options."GPS, ";
			}
			if($car_data["option_rear_sensor"]=="Y" || $car_data["option_rear_sensor"]=="y"){
				$options = $options."후방센서, ";
			}
			if($car_data["option_blackbox"]=="Y" || $car_data["option_blackbox"]=="y"){
				$options = $options."블랙박스, ";
			}
			if($car_data["option_cdplayer"]=="Y" || $car_data["option_cdplayer"]=="y"){
				$options = $options."CD플레이어, ";
			}
			if($car_data["option_backcamera"]=="Y" || $car_data["option_backcamera"]=="y"){
				$options = $options."후방카메라, ";
			}
			if($car_data["option_sunroof"]=="Y" || $car_data["option_sunroof"]=="y"){
				$options = $options."썬루프, ";
			}
			if($car_data["option_lane_departure_warning"]=="Y" || $car_data["option_lane_departure_warning"]=="y"){
				$options = $options."차선감지센서, ";
			}
			if($car_data["option_automatic_seat"]=="Y" || $car_data["option_automatic_seat"]=="y"){
				$options = $options."자동좌석, ";
			}
			if($car_data["option_front_sensor"]=="Y" || $car_data["option_front_sensor"]=="y"){
				$options = $options."전방센서, ";
			}
			if($car_data["option_smart_key"]=="Y" || $car_data["option_smart_key"]=="y"){
				$options = $options."스마트키, ";
			}
			if($car_data["option_av_system"]=="Y" || $car_data["option_av_system"]=="y"){
				$options = $options."AV시스템, ";
			}
			if($car_data["option_ecm"]=="Y" || $car_data["option_ecm"]=="y"){
				$options = $options."ECM, ";
			}

			if(strlen($options)>0){
				//뒤에 2개 짜른다.
				$options = substr($options, 0, -2);
			}


			$car_send_array[] = array(
				"serial" => intval($car_data['serial']),
				"car_index" => intval($car_data['car_index']),
				"car_status" => $car_data['car_status'],
				"car_number" => $car_data['car_number'],
				"rent_status" => $car_data['rent_status'],
				"price_type" => $car_data['price_type'],
				"car_type" => $car_data['car_type'],
				"car_name_detail" => $car_data['car_name_detail'],
				"car_people" => intval($car_data['max_people']),
				"color" => $car_data['color'],
				"gear_type" => $car_data['gear_type'],
				"fuel_option" => $car_data['fuel_option'],
				"smoking" => $car_data['smoking'],
				"release_year" => intval($car_data['release_year']),
				"drive_distance" => intval($car_data['drive_distance']),
				"rest_fuel" => intval($car_data['rest_fuel']),
				"car_register_number" => $car_data['car_register_number'],
				"option" => $options,
				"car_manager" => $car_data['car_manager'],
				"temp_number" => $car_data['temp_number'],
				"insurance_company" => $car_data['insurance_company'],
				// "total_insurance_money" => intval($car_data['insurance_man1_price']) + intval($car_data['insurance_man2_price']) + intval($car_data['insurance_object_price']) + intval($car_data['insurance_children_price']) + intval($car_data['insurance_self_price']) + intval($car_data['insurance_self2_price']),
				"insurance_age" => intval($car_data['requirement_age']),
				"insurance_start_date" => $car_data['insurance_start_date'],
				"insurance_end_date" => $car_data['insurance_end_date'],
				"insurance_text" => $car_data['insurance_text'],
				"purchase_date" => $car_data['car_purchase_date'],
				"release_date" => $car_data['car_release_date'],
				"register_date" => $car_data['car_register_date'],
				"purchase_type" => $car_data['purchase_type'],
				"car_money" => intval($car_data['car_price']),
				"installment_money" => intval($car_data['installment_money']),
				"installment_pay_day" => $car_data['installment_pay_day'],
				"installment_monthly_money" => intval($car_data['installment_monthly_money']),
				"location" => $car_data['location'],
				"etc_text" => $car_data['etc_text'],
				"register_user" => $car_data['registered_user'],
				"register_datetime" => $car_data['registered_date'],
				"modify_user" => $car_data['edited_user'],
				"modify_datetime" => $car_data['edited_date'],
				"car_image" => $car_data['car_list_image_file'],
				"option_usb" => $car_data['option_usb'],
				"option_second_driver" => $car_data['option_second_driver'], 
				"option_international_license" => $car_data['option_international_license'], 
				"option_license1" => $car_data['option_license1'], 
				"minimun_drive_year" => $car_data['minimun_drive_year'], 
				"option_navi" => $car_data["option_navi"],
				"option_bluetooth" => $car_data["option_bluetooth"],
				// "option_automatic_back_mirror" => $car_data["option_automatic_back_mirror"],
				"option_hi_pass" => $car_data["option_hi_pass"],
				"option_aux" => $car_data["option_aux"],
				"option_heatseat" => $car_data["option_heatseat"],
				// "option_gps" => $car_data["option_gps"],
				// "option_rear_sensor" => $car_data["option_rear_sensor"],
				"option_blackbox" => $car_data["option_blackbox"],
				"option_cdplayer" => $car_data["option_cdplayer"],
				"option_backcamera" => $car_data["option_backcamera"],
				"option_sunroof" => $car_data["option_sunroof"],
				// "option_lane_departure_warning" => $car_data["option_lane_departure_warning"],
				"option_automatic_seat" => $car_data["option_automatic_seat"],
				"option_front_sensor" => $car_data["option_front_sensor"],
				"option_smart_key" => $car_data["option_smart_key"],
				"option_av_system" => $car_data["option_av_system"],
				// "option_ecm" => $car_data["option_ecm"],
				// 'option_week_48' => $car_data["option_week_48"],
				'option_weekend_48' => $car_data["option_weekend_48"],
				'insurance_man1_flag' => $car_data["insurance_man1_flag"],
				'insurance_man2_flag' => $car_data["insurance_man2_flag"],
				'insurance_object_flag' => $car_data["insurance_object_flag"],
				'insurance_children_flag' => $car_data["insurance_children_flag"],
				'insurance_self_flag' => $car_data["insurance_self_flag"],
				'insurance_self2_flag' => $car_data["insurance_self2_flag"],
				'insurance_self3_flag' => $car_data["insurance_self3_flag"],
				'insurance_self_price' => $car_data["insurance_self_price"],
				'insurance_self2_price' => $car_data["insurance_self2_price"],
				'insurance_self3_price' => $car_data["insurance_self3_price"],
				'insurance_man1_compensation' => $car_data["insurance_man1_compensation"],
				'insurance_man2_compensation' => $car_data["insurance_man2_compensation"],
				'insurance_object_compensation' => $car_data["insurance_object_compensation"],
				'insurance_children_compensation' => $car_data["insurance_children_compensation"],
				'insurance_self_compensation' => $car_data["insurance_self_compensation"],
				'insurance_self2_compensation' => $car_data["insurance_self2_compensation"],
				'insurance_self3_compensation' => $car_data["insurance_self3_compensation"],
				'insurance_man1_customer' => $car_data["insurance_man1_customer"],
				'insurance_man2_customer' => $car_data["insurance_man2_customer"],
				'insurance_object_customer' => $car_data["insurance_object_customer"],
				'insurance_children_customer' => $car_data["insurance_children_customer"],
				'insurance_self_customer' => $car_data["insurance_self_customer"],
				'insurance_self2_customer' => $car_data["insurance_self2_customer"],
				'insurance_self3_customer' => $car_data["insurance_self3_customer"],
				'week_price' => $car_data["week_price"],
				'weekend_price' => $car_data["weekend_price"],
				'week_price_6' => $car_data["week_price_6"],
				'weekend_price_6' => $car_data["weekend_price_6"],
				'week_price_1' => $car_data["week_price_1"],
				'weekend_price_1' => $car_data["weekend_price_1"]
				// 'day_price' => $car_data["day_price"],
				// 'hour_price' => $car_data["hour_price"],
			);
		}
		if($car_send_array == null){
			$car_send_array = array();
		}
		echo json_encode($car_send_array);
	}



	//점검 내역 불러오기
	function get_check_history($car_serial = ''){
		if($car_serial==''){
			echo "차량을 선택해 주세요.";
		}

		$check_history = $this->carmanager->get_check_list($car_serial);
		echo json_encode($check_history);
	}

	//소모품 교환 내역 불러오기
	function get_supply_history($car_serial = ''){
		if($car_serial==''){
			echo "차량을 선택해 주세요.";
		}

		$history = $this->carmanager->get_supply_list($car_serial);
		echo json_encode($history);
	}

	//정비 내역 불러오기
	function get_repair_history($car_serial = ''){
		if($car_serial==''){
			echo "차량을 선택해 주세요.";
		}

		$history = $this->carmanager->get_repair_list($car_serial);
		echo json_encode($history);
	}

	//긴급출동 내역 불러오기
	function get_emergency_history($car_serial = ''){
		if($car_serial==''){
			echo "차량을 선택해 주세요.";
		}

		$history = $this->carmanager->get_emergency_list($car_serial);
		echo json_encode($history);
	}

	//사고 내역 불러오기
	function get_accident_history($car_serial = ''){
		if($car_serial==''){
			echo "차량을 선택해 주세요.";
		}

		$history = $this->carmanager->get_accident_list($car_serial);
		echo json_encode($history);
	}

	function register_car(){


		$user = $this->session->userdata('admin_name');
		$write_data = array(
			'company_serial' => intval($this->input->post('company_serial', TRUE)),
			'rent_status' => "대기중",
			'flag' => "N", //N : 렌고 off, Y: 렌고 on
			'car_index' => intval($this->input->post('car_index', TRUE)),
			'maker_index' => intval($this->input->post('maker_index', TRUE)),
			'car_number' => urldecode($this->input->post('car_number', TRUE)), //차량번호
			'car_status' => urldecode($this->input->post('car_status', TRUE)), //차량상태
			'car_register_number' => urldecode($this->input->post('car_register_number', TRUE)), //차대번호
			'max_people' => intval($this->input->post('max_people', TRUE)),
			'smoking' => urldecode($this->input->post('smoking', TRUE)), //흡연여부
			'gear_type' => urldecode($this->input->post('gear_type', TRUE)), //기어타입
			'fuel_option' => urldecode($this->input->post('fuel_option', TRUE)), //사용연료
			'release_year' => intval($this->input->post('release_year', TRUE)), //연식
			// 'drive_distance' => intval($this->input->post('drive_distance', TRUE)), //주행거리
			// 'rest_fuel' => intval($this->input->post('rest_fuel', TRUE)), //잔여연료
			'color' => urldecode($this->input->post('color', TRUE)), //색상
			// 'temp_number' => urldecode($this->input->post('temp_number', TRUE)), //임시번호
			// 'car_manager' => urldecode($this->input->post('car_manager', TRUE)), //임시번호
			//옵션
			'minimun_drive_year' => intval($this->input->post('minimun_drive_year', TRUE)), 
			'option_license1' => urldecode($this->input->post('option_license1', TRUE)), 
			'option_navi' => urldecode($this->input->post('option_navi', TRUE)), 
			'option_bluetooth' => urldecode($this->input->post('option_bluetooth', TRUE)),
			// 'option_automatic_back_mirror' => urldecode($this->input->post('option_automatic_back_mirror', TRUE)),
			'option_hi_pass' => urldecode($this->input->post('option_hi_pass', TRUE)),
			'option_heatseat' => urldecode($this->input->post('option_heatseat', TRUE)),
			// 'option_gps' => urldecode($this->input->post('option_gps', TRUE)),
			// 'option_rear_sensor' => urldecode($this->input->post('option_rear_sensor', TRUE)),
			'option_blackbox' => urldecode($this->input->post('option_blackbox', TRUE)),
			'option_cdplayer' => urldecode($this->input->post('option_cdplayer', TRUE)),
			'option_backcamera' => urldecode($this->input->post('option_backcamera', TRUE)),
			'option_sunroof' => urldecode($this->input->post('option_sunroof', TRUE)),
			// 'option_lane_departure_warning' => urldecode($this->input->post('option_lane_departure_warning', TRUE)),
			// 'option_automatic_seat' => urldecode($this->input->post('option_automatic_seat', TRUE)),
			// 'option_front_sensor' => urldecode($this->input->post('option_front_sensor', TRUE)),
			'option_smart_key' => urldecode($this->input->post('option_smart_key', TRUE)),
			// 'option_av_system' => urldecode($this->input->post('option_av_system', TRUE)),
			'option_aux' => urldecode($this->input->post('option_aux', TRUE)),
			// 'option_ecm' => urldecode($this->input->post('option_ecm', TRUE)),
			// 'option_week_48' => urldecode($this->input->post('option_week_48', TRUE)),
			'option_weekend_48' => urldecode($this->input->post('option_weekend_48', TRUE)),
			'option_usb' => urldecode($this->input->post('option_usb', TRUE)),
			'option_second_driver' => urldecode($this->input->post('option_second_driver', TRUE)), 
			'option_international_license' => urldecode($this->input->post('option_international_license', TRUE)),  
			//요금제
			'week_price' => intval($this->input->post('week_price', TRUE)),
			'weekend_price' => intval($this->input->post('weekend_price', TRUE)),
			'week_price_6' => intval($this->input->post('week_price_6', TRUE)),
			'weekend_price_6' => intval($this->input->post('weekend_price_6', TRUE)),
			'week_price_1' => intval($this->input->post('week_price_1', TRUE)),
			'weekend_price_1' => intval($this->input->post('weekend_price_1', TRUE)),

			//보험사
			'insurance_company' => urldecode($this->input->post('insurance_company', TRUE)),
			'requirement_age' => intval($this->input->post('requirement_age', TRUE)),
			'insurance_start_date' => urldecode($this->input->post('insurance_start_date', TRUE)),
			'insurance_end_date' => urldecode($this->input->post('insurance_end_date', TRUE)),
			'insurance_text' => urldecode($this->input->post('agreement_text', TRUE)),
			'insurance_man1_flag' => urldecode($this->input->post('insurance_man1_flag', TRUE)),
			'insurance_man2_flag' => urldecode($this->input->post('insurance_man2_flag', TRUE)),
			'insurance_object_flag' => urldecode($this->input->post('insurance_object_flag', TRUE)),
			'insurance_children_flag' => urldecode($this->input->post('insurance_children_flag', TRUE)),
			'insurance_self_flag' => urldecode($this->input->post('insurance_self_flag', TRUE)),
			'insurance_self2_flag' => urldecode($this->input->post('insurance_self2_flag', TRUE)),
			'insurance_self3_flag' => urldecode($this->input->post('insurance_self3_flag', TRUE)),
			'insurance_self_price' => intval($this->input->post('insurance_self_price', TRUE)),
			'insurance_self2_price' => intval($this->input->post('insurance_self2_price', TRUE)),
			'insurance_self3_price' => intval($this->input->post('insurance_self3_price', TRUE)),
			'insurance_man1_compensation' => urldecode($this->input->post('insurance_man1_compensation', TRUE)),
			'insurance_man2_compensation' => urldecode($this->input->post('insurance_man2_compensation', TRUE)),
			'insurance_object_compensation' => urldecode($this->input->post('insurance_object_compensation', TRUE)),
			'insurance_children_compensation' => urldecode($this->input->post('insurance_children_compensation', TRUE)),
			'insurance_self_compensation' => urldecode($this->input->post('insurance_self_compensation', TRUE)),
			'insurance_self2_compensation' => urldecode($this->input->post('insurance_self2_compensation', TRUE)),
			'insurance_self3_compensation' => urldecode($this->input->post('insurance_self3_compensation', TRUE)),
			'insurance_man1_customer' => urldecode($this->input->post('insurance_man1_customer', TRUE)),
			'insurance_man2_customer' => urldecode($this->input->post('insurance_man2_customer', TRUE)),
			'insurance_object_customer' => urldecode($this->input->post('insurance_object_customer', TRUE)),
			'insurance_children_customer' => urldecode($this->input->post('insurance_children_customer', TRUE)),
			'insurance_self_customer' => urldecode($this->input->post('insurance_self_customer', TRUE)),
			'insurance_self2_customer' => urldecode($this->input->post('insurance_self2_customer', TRUE)),
			'insurance_self3_customer' => urldecode($this->input->post('insurance_self3_customer', TRUE)),
			'registered_ip' => $_SERVER['REMOTE_ADDR'],
			'registered_user' => $user

		);


		$result = $this->carmanager->add_car($write_data);
		echo json_encode($result);
	}

	function update_car(){


		$user = $this->session->userdata('admin_name');
		$write_data = array(
			'serial' => intval($this->input->post('serial', TRUE)),
			// 'company_serial' => $this->session->userdata('company_serial'),
						// 'price_type' => urldecode($this->input->post('price_type', TRUE)),
			// 'maker_index' => intval($this->input->post('maker_index', TRUE)), //필요 없음
			'car_number' => urldecode($this->input->post('car_number', TRUE)), //차량번호
			'car_status' => urldecode($this->input->post('car_status', TRUE)), //차량상태
			// 'branch_serial' => intval($this->input->post('branch_serial', TRUE)),
			'car_register_number' => urldecode($this->input->post('car_register_number', TRUE)), //차대번호
			'max_people' => intval($this->input->post('max_people', TRUE)),
			'smoking' => urldecode($this->input->post('smoking', TRUE)), //흡연여부
			'gear_type' => urldecode($this->input->post('gear_type', TRUE)), //기어타입
			'fuel_option' => urldecode($this->input->post('fuel_option', TRUE)), //사용연료
			'release_year' => intval($this->input->post('release_year', TRUE)), //연식
			// 'drive_distance' => intval($this->input->post('drive_distance', TRUE)), //주행거리
			// 'rest_fuel' => intval($this->input->post('rest_fuel', TRUE)), //잔여연료
			'color' => urldecode($this->input->post('color', TRUE)), //색상
			// 'temp_number' => urldecode($this->input->post('temp_number', TRUE)), //임시번호
			// 'car_manager' => urldecode($this->input->post('car_manager', TRUE)), //임시번호
			//옵션
			'minimun_drive_year' => intval($this->input->post('minimun_drive_year', TRUE)), 
			'option_license1' => urldecode($this->input->post('option_license1', TRUE)), 
			'option_navi' => urldecode($this->input->post('option_navi', TRUE)), 
			'option_bluetooth' => urldecode($this->input->post('option_bluetooth', TRUE)),
			'option_automatic_back_mirror' => urldecode($this->input->post('option_automatic_back_mirror', TRUE)),
			'option_hi_pass' => urldecode($this->input->post('option_hi_pass', TRUE)),
			'option_heatseat' => urldecode($this->input->post('option_heatseat', TRUE)),
			'option_gps' => urldecode($this->input->post('option_gps', TRUE)),
			'option_rear_sensor' => urldecode($this->input->post('option_rear_sensor', TRUE)),
			'option_blackbox' => urldecode($this->input->post('option_blackbox', TRUE)),
			'option_cdplayer' => urldecode($this->input->post('option_cdplayer', TRUE)),
			'option_backcamera' => urldecode($this->input->post('option_backcamera', TRUE)),
			'option_sunroof' => urldecode($this->input->post('option_sunroof', TRUE)),
			// 'option_lane_departure_warning' => urldecode($this->input->post('option_lane_departure_warning', TRUE)),
			'option_automatic_seat' => urldecode($this->input->post('option_automatic_seat', TRUE)),
			'option_front_sensor' => urldecode($this->input->post('option_front_sensor', TRUE)),
			'option_smart_key' => urldecode($this->input->post('option_smart_key', TRUE)),
			// 'option_av_system' => urldecode($this->input->post('option_av_system', TRUE)),
			'option_aux' => urldecode($this->input->post('option_aux', TRUE)),
			// 'option_ecm' => urldecode($this->input->post('option_ecm', TRUE)),
			// 'option_week_48' => urldecode($this->input->post('option_week_48', TRUE)),
			'option_weekend_48' => urldecode($this->input->post('option_weekend_48', TRUE)),
			'option_usb' => urldecode($this->input->post('option_usb', TRUE)), 
			'option_second_driver' => urldecode($this->input->post('option_second_driver', TRUE)), 
			'option_international_license' => urldecode($this->input->post('option_international_license', TRUE)), 
			//요금제
			'week_price' => intval($this->input->post('week_price', TRUE)),
			'weekend_price' => intval($this->input->post('weekend_price', TRUE)),
			'week_price_6' => intval($this->input->post('week_price_6', TRUE)),
			'weekend_price_6' => intval($this->input->post('weekend_price_6', TRUE)),
			'week_price_1' => intval($this->input->post('week_price_1', TRUE)),
			'weekend_price_1' => intval($this->input->post('weekend_price_1', TRUE)),
			//보험사
			'insurance_company' => urldecode($this->input->post('insurance_company', TRUE)),
			'requirement_age' => intval($this->input->post('requirement_age', TRUE)),
			'insurance_start_date' => urldecode($this->input->post('insurance_start_date', TRUE)),
			'insurance_end_date' => urldecode($this->input->post('insurance_end_date', TRUE)),
			'insurance_text' => urldecode($this->input->post('agreement_text', TRUE)),
			'insurance_man1_flag' => urldecode($this->input->post('insurance_man1_flag', TRUE)),
			'insurance_man2_flag' => urldecode($this->input->post('insurance_man2_flag', TRUE)),
			'insurance_object_flag' => urldecode($this->input->post('insurance_object_flag', TRUE)),
			'insurance_children_flag' => urldecode($this->input->post('insurance_children_flag', TRUE)),
			'insurance_self_flag' => urldecode($this->input->post('insurance_self_flag', TRUE)),
			'insurance_self2_flag' => urldecode($this->input->post('insurance_self2_flag', TRUE)),
			'insurance_self3_flag' => urldecode($this->input->post('insurance_self3_flag', TRUE)),
			'insurance_self_price' => intval($this->input->post('insurance_self_price', TRUE)),
			'insurance_self2_price' => intval($this->input->post('insurance_self2_price', TRUE)),
			'insurance_self3_price' => intval($this->input->post('insurance_self3_price', TRUE)),
			'insurance_man1_compensation' => urldecode($this->input->post('insurance_man1_compensation', TRUE)),
			'insurance_man2_compensation' => urldecode($this->input->post('insurance_man2_compensation', TRUE)),
			'insurance_object_compensation' => urldecode($this->input->post('insurance_object_compensation', TRUE)),
			'insurance_children_compensation' => urldecode($this->input->post('insurance_children_compensation', TRUE)),
			'insurance_self_compensation' => urldecode($this->input->post('insurance_self_compensation', TRUE)),
			'insurance_self2_compensation' => urldecode($this->input->post('insurance_self2_compensation', TRUE)),
			'insurance_self3_compensation' => urldecode($this->input->post('insurance_self3_compensation', TRUE)),
			'insurance_man1_customer' => urldecode($this->input->post('insurance_man1_customer', TRUE)),
			'insurance_man2_customer' => urldecode($this->input->post('insurance_man2_customer', TRUE)),
			'insurance_object_customer' => urldecode($this->input->post('insurance_object_customer', TRUE)),
			'insurance_children_customer' => urldecode($this->input->post('insurance_children_customer', TRUE)),
			'insurance_self_customer' => urldecode($this->input->post('insurance_self_customer', TRUE)),
			'insurance_self2_customer' => urldecode($this->input->post('insurance_self2_customer', TRUE)),
			'insurance_self3_customer' => urldecode($this->input->post('insurance_self3_customer', TRUE)),
			'edited_ip' => $_SERVER['REMOTE_ADDR'],
			'edited_user' => $user,

		);


		$result = $this->carmanager->update_car($write_data);
		echo json_encode($result);
	}



	//점검 내역 등록
	function register_check_history(){

		if(!$this->input->post('car_serial', TRUE) || !$this->input->post('type', TRUE) || !$this->input->post('check_date', TRUE)){
			alert('필수항목을 입력해 주세요.');
			exit;
		}

		$write_data = array(
			'car_serial' => intval($this->input->post('car_serial', TRUE)),
			'type' => urldecode($this->input->post('type', TRUE)),
			'period' => urldecode($this->input->post('period', TRUE)),
			'check_date' => $this->input->post('check_date', TRUE),
			'next_check_date' => $this->input->post('next_check_date', TRUE),
			'pay' => intval($this->input->post('pay', TRUE)),
			'result' => urldecode($this->input->post('result', TRUE)),
		);


		$result = $this->carmanager->add_check($write_data);
		echo json_encode($result);
	}
	//점건 내역 수정
	function update_check_history(){

		if(!$this->input->post('serial', TRUE)  || !$this->input->post('car_serial', TRUE) || !$this->input->post('type', TRUE) || !$this->input->post('check_date', TRUE)){
			alert('필수항목을 입력해 주세요.');
			exit;
		}

		$id =  intval($this->input->post('serial', TRUE));
		$write_data = array(
		
			'car_serial' => intval($this->input->post('car_serial', TRUE)),
			'type' => urldecode($this->input->post('type', TRUE)),
			'period' => urldecode($this->input->post('period', TRUE)),
			'check_date' => $this->input->post('check_date', TRUE),
			'next_check_date' => $this->input->post('next_check_date', TRUE),
			'pay' => intval($this->input->post('pay', TRUE)),
			'result' => urldecode($this->input->post('result', TRUE)),
		);


		$result = $this->carmanager->update_check($id, $write_data);
		echo json_encode($result);
	}
	//점검 내역 삭제
	function delete_check_history(){

		if(!$this->input->post('index', TRUE)){
			alert('점검내역 id가 없습니다.');
			exit;
		}
		
		$result = $this->carmanager->delete_check($this->input->post('index', TRUE));
		echo json_encode($result);
	}


	//소모품교환 내역 등록
	function register_supply_history(){

		if(!$this->input->post('car_serial', TRUE) || !$this->input->post('type', TRUE) || !$this->input->post('check_date', TRUE)){
			alert('필수항목을 입력해 주세요.');
			exit;
		}

		$write_data = array(
			'car_serial' => intval($this->input->post('car_serial', TRUE)),
			'type' => urldecode($this->input->post('type', TRUE)),
			'period' => intval($this->input->post('period', TRUE)),
			'check_date' => $this->input->post('check_date', TRUE),
			'check_distance' => intval($this->input->post('check_distance', TRUE)),
			'next_check_distance' => intval($this->input->post('next_check_distance', TRUE)),
			'pay' => intval($this->input->post('pay', TRUE)),
			'memo' => urldecode($this->input->post('memo', TRUE)),
		);


		$result = $this->carmanager->add_supply($write_data);
		echo json_encode($result);
	}

	//소모품교환 내역 등록
	function update_supply_history(){

		if(!$this->input->post('serial', TRUE) || !$this->input->post('car_serial', TRUE) || !$this->input->post('type', TRUE) || !$this->input->post('check_date', TRUE)){
			alert('필수항목을 입력해 주세요.');
			exit;
		}

		$id =  intval($this->input->post('serial', TRUE));

		$write_data = array(
			'car_serial' => intval($this->input->post('car_serial', TRUE)),
			'type' => urldecode($this->input->post('type', TRUE)),
			'period' => intval($this->input->post('period', TRUE)),
			'check_date' => $this->input->post('check_date', TRUE),
			'check_distance' => intval($this->input->post('check_distance', TRUE)),
			'next_check_distance' => intval($this->input->post('next_check_distance', TRUE)),
			'pay' => intval($this->input->post('pay', TRUE)),
			'memo' => urldecode($this->input->post('memo', TRUE)),
		);


		$result = $this->carmanager->update_supply($id, $write_data);
		echo json_encode($result);
	}

	//소모품교환 내역 삭제
	function delete_supply_history(){

		if(!$this->input->post('index', TRUE)){
			alert('소모품교환 id가 없습니다.');
			exit;
		}
		
		$result = $this->carmanager->delete_supply($this->input->post('index', TRUE));
		echo json_encode($result);
	}

	//사고 내역 등록
	function register_accident_history(){

		if(!$this->input->post('car_serial', TRUE) || !$this->input->post('accident_date', TRUE)){
			alert('필수항목을 입력해 주세요.');
			exit;
		}

		$write_data = array(
			'car_serial' => intval($this->input->post('car_serial', TRUE)),
			'status' => urldecode($this->input->post('status', TRUE)),
			'accident_date' => $this->input->post('accident_date', TRUE),
			'accident_place' => urldecode($this->input->post('accident_place', TRUE)),
			'customer_memo' => urldecode($this->input->post('customer_memo', TRUE)),
			'customer_name' => urldecode($this->input->post('customer_name', TRUE)),
			'customer_phone' => urldecode($this->input->post('customer_phone', TRUE)),
			'customer_company' => urldecode($this->input->post('customer_company', TRUE)),
			'customer_mobile' => urldecode($this->input->post('customer_mobile', TRUE)),
			'customer_percent' => intval($this->input->post('customer_percent', TRUE)),
			'customer_compensation' => intval($this->input->post('customer_compensation', TRUE)),
			'customer_pay' => intval($this->input->post('customer_pay', TRUE)),
			'customer_insurance_company' => urldecode($this->input->post('customer_insurance_company', TRUE)),
			'customer_insurance_date' => $this->input->post('customer_insurance_date', TRUE),
			'customer_register_number' => urldecode($this->input->post('customer_register_number', TRUE)),
			'customer_insurance_man' => urldecode($this->input->post('customer_insurance_man', TRUE)),
			'customer_insurance_phone' => urldecode($this->input->post('customer_insurance_phone', TRUE)),
			'customer_insurance_fax' => urldecode($this->input->post('customer_insurance_fax', TRUE)),
			'customer_repair' => urldecode($this->input->post('customer_repair', TRUE)),
			'target_car_number' => urldecode($this->input->post('target_car_number', TRUE)),
			'target_car_model' => urldecode($this->input->post('target_car_model', TRUE)),
			'target_name' => urldecode($this->input->post('target_name', TRUE)),
			'target_phone' => urldecode($this->input->post('target_phone', TRUE)),
			'target_mobile' => urldecode($this->input->post('target_mobile', TRUE)),
			'target_percent' => intval($this->input->post('target_percent', TRUE)),
			'target_company' => urldecode($this->input->post('target_company', TRUE)),
			'target_compensation' => intval($this->input->post('target_compensation', TRUE)),
			'target_insurance_company' => urldecode($this->input->post('target_insurance_company', TRUE)),
			'target_register_number' => urldecode($this->input->post('target_register_number', TRUE)),
			'target_insurance_man' => urldecode($this->input->post('target_insurance_man', TRUE)),
			'target_insurance_phone' => urldecode($this->input->post('target_insurance_phone', TRUE)),
			'target_insurance_fax' => urldecode($this->input->post('target_insurance_fax', TRUE)),
			'target_repair' => urldecode($this->input->post('target_repair', TRUE)),
			'target_memo' => urldecode($this->input->post('target_memo', TRUE)),

		);


		$result = $this->carmanager->add_accident($write_data);
		echo json_encode($result);
	}

	//소모품교환 내역 등록
	function update_accident_history(){

		// if(!$this->input->post('serial', TRUE) || !$this->input->post('accident_date', TRUE)){
		// 	alert('필수항목을 입력해 주세요.');
		// 	exit;
		// }

		$id =  intval($this->input->post('serial', TRUE));

		$write_data = array(
			'status' => urldecode($this->input->post('status', TRUE)),
			'accident_date' => $this->input->post('accident_date', TRUE),
			'accident_place' => urldecode($this->input->post('accident_place', TRUE)),
			'customer_memo' => urldecode($this->input->post('customer_memo', TRUE)),
			'customer_name' => urldecode($this->input->post('customer_name', TRUE)),
			'customer_phone' => urldecode($this->input->post('customer_phone', TRUE)),
			'customer_company' => urldecode($this->input->post('customer_company', TRUE)),
			'customer_mobile' => urldecode($this->input->post('customer_mobile', TRUE)),
			'customer_percent' => intval($this->input->post('customer_percent', TRUE)),
			'customer_compensation' => intval($this->input->post('customer_compensation', TRUE)),
			'customer_pay' => intval($this->input->post('customer_pay', TRUE)),
			'customer_insurance_company' => urldecode($this->input->post('customer_insurance_company', TRUE)),
			'customer_insurance_date' => $this->input->post('customer_insurance_date', TRUE),
			'customer_register_number' => urldecode($this->input->post('customer_register_number', TRUE)),
			'customer_insurance_man' => urldecode($this->input->post('customer_insurance_man', TRUE)),
			'customer_insurance_phone' => urldecode($this->input->post('customer_insurance_phone', TRUE)),
			'customer_insurance_fax' => urldecode($this->input->post('customer_insurance_fax', TRUE)),
			'customer_repair' => urldecode($this->input->post('customer_repair', TRUE)),
			'target_car_number' => urldecode($this->input->post('target_car_number', TRUE)),
			'target_car_model' => urldecode($this->input->post('target_car_model', TRUE)),
			'target_name' => urldecode($this->input->post('target_name', TRUE)),
			'target_phone' => urldecode($this->input->post('target_phone', TRUE)),
			'target_mobile' => urldecode($this->input->post('target_mobile', TRUE)),
			'target_percent' => intval($this->input->post('target_percent', TRUE)),
			'target_company' => urldecode($this->input->post('target_company', TRUE)),
			'target_compensation' => intval($this->input->post('target_compensation', TRUE)),
			'target_insurance_company' => urldecode($this->input->post('target_insurance_company', TRUE)),
			'target_register_number' => urldecode($this->input->post('target_register_number', TRUE)),
			'target_insurance_man' => urldecode($this->input->post('target_insurance_man', TRUE)),
			'target_insurance_phone' => urldecode($this->input->post('target_insurance_phone', TRUE)),
			'target_insurance_fax' => urldecode($this->input->post('target_insurance_fax', TRUE)),
			'target_repair' => urldecode($this->input->post('target_repair', TRUE)),
			'target_memo' => urldecode($this->input->post('target_memo', TRUE)),


		);

		$result = $this->carmanager->update_accident($id, $write_data);
		echo json_encode($result);
	}

	//사고 내역 삭제
	function delete_accident_history(){


		// if(!$this->input->post('index', TRUE)){
		// 	alert('사고내역 id가 없습니다.');
		// 	exit;
		// }
		
		$result = $this->carmanager->delete_accident($this->input->post('index', TRUE));
		echo json_encode($result);
	}


	//정비 내역 등록
	function register_repair_history(){

		if(!$this->input->post('car_serial', TRUE) || !$this->input->post('type', TRUE) || !$this->input->post('repair_start_date', TRUE) || !$this->input->post('repair_end_date', TRUE) ){
			alert('필수항목을 입력해 주세요.');
			exit;
		}

		$write_data = array(
			'car_serial' => intval($this->input->post('car_serial', TRUE)),
			'type' => urldecode($this->input->post('type', TRUE)),
			'repair_start_date' => $this->input->post('repair_start_date', TRUE),
			'repair_end_date' => $this->input->post('repair_end_date', TRUE),
			'repair_company' => urldecode($this->input->post('repair_company', TRUE)),
			'repair_company_phone_number' => urldecode($this->input->post('repair_company_phone_number', TRUE)),
			'repair_detail' => urldecode($this->input->post('repair_detail', TRUE)),
			'repair_pay1' => intval($this->input->post('repair_pay1', TRUE)),
			'repair_pay2' => intval($this->input->post('repair_pay2', TRUE)),
			'repair_pay3' => intval($this->input->post('repair_pay3', TRUE)),
			'repair_total_pay' => intval($this->input->post('repair_total_pay', TRUE)),
			'repair_customer_name' => urldecode($this->input->post('repair_customer_name', TRUE)),
			'repair_charge1' => intval($this->input->post('repair_charge1', TRUE)),
			'repair_charge2' => intval($this->input->post('repair_charge2', TRUE)),
			'repair_total_charge' => intval($this->input->post('repair_total_charge', TRUE)),
			'distance' => intval($this->input->post('distance', TRUE)),
			'memo' => urldecode($this->input->post('memo', TRUE)),
		);


		$result = $this->carmanager->add_repair($write_data);
		echo json_encode($result);
	}

	//수리 내역 등록
	function update_repair_history(){

		if(!$this->input->post('serial', TRUE) || !$this->input->post('car_serial', TRUE)  || !$this->input->post('type', TRUE) || !$this->input->post('repair_start_date', TRUE) || !$this->input->post('repair_end_date', TRUE) ){
			alert('필수항목을 입력해 주세요.');
			exit;
		}

		$id =  intval($this->input->post('serial', TRUE));

			$write_data = array(
			'car_serial' => intval($this->input->post('car_serial', TRUE)),
			'type' => urldecode($this->input->post('type', TRUE)),
			'repair_start_date' => $this->input->post('repair_start_date', TRUE),
			'repair_end_date' => $this->input->post('repair_end_date', TRUE),
			'repair_company' => urldecode($this->input->post('repair_company', TRUE)),
			'repair_company_phone_number' => urldecode($this->input->post('repair_company_phone_number', TRUE)),
			'repair_detail' => urldecode($this->input->post('repair_detail', TRUE)),
			'repair_pay1' => intval($this->input->post('repair_pay1', TRUE)),
			'repair_pay2' => intval($this->input->post('repair_pay2', TRUE)),
			'repair_pay3' => intval($this->input->post('repair_pay3', TRUE)),
			'repair_total_pay' => intval($this->input->post('repair_total_pay', TRUE)),
			'repair_customer_name' => urldecode($this->input->post('repair_customer_name', TRUE)),
			'repair_charge1' => intval($this->input->post('repair_charge1', TRUE)),
			'repair_charge2' => intval($this->input->post('repair_charge2', TRUE)),
			'repair_total_charge' => intval($this->input->post('repair_total_charge', TRUE)),
			'distance' => intval($this->input->post('distance', TRUE)),
			'memo' => urldecode($this->input->post('memo', TRUE)),
		);


		$result = $this->carmanager->update_repair($id, $write_data);
		echo json_encode($result);
	}

	//수리 내역 삭제
	function delete_repair_history(){

		if(!$this->input->post('index', TRUE)){
			alert('소모품교환 id가 없습니다.');
			exit;
		}
		
		$result = $this->carmanager->delete_repair($this->input->post('index', TRUE));
		echo json_encode($result);
	}

	//출동 내역 등록
	function register_emergency_history(){

		if(!$this->input->post('car_serial', TRUE) || !$this->input->post('date', TRUE) ){
			alert('필수항목을 입력해 주세요.');
			exit;
		}

		$write_data = array(
			'car_serial' => intval($this->input->post('car_serial', TRUE)),
			'place' => urldecode($this->input->post('place', TRUE)),
			'date' => $this->input->post('date', TRUE),
			'detail' => urldecode($this->input->post('detail', TRUE)),
		);

		// echo json_decode($wri)


		$result = $this->carmanager->add_emergency($write_data);
		echo json_encode($result);
	}

	//출동 내역 등록
	function update_emergency_history(){

		if(!$this->input->post('serial', TRUE) || !$this->input->post('car_serial', TRUE)  || !$this->input->post('date', TRUE) ){
			alert('필수항목을 입력해 주세요.');
			exit;
		}

		$id =  intval($this->input->post('serial', TRUE));

		$write_data = array(
			'car_serial' => intval($this->input->post('car_serial', TRUE)),
			'place' => urldecode($this->input->post('place', TRUE)),
			'date' => $this->input->post('date', TRUE),
			'detail' => urldecode($this->input->post('detail', TRUE)),
		);


		$result = $this->carmanager->update_emergency($id, $write_data);
		echo json_encode($result);
	}

	//출동 내역 삭제
	function delete_emergency_history(){

		if(!$this->input->post('index', TRUE)){
			alert('소모품교환 id가 없습니다.');
			exit;
		}
		
		$result = $this->carmanager->delete_emergency($this->input->post('index', TRUE));
		echo json_encode($result);
	}




	//차량 삭제
	function delete_car(){

		if(!$this->input->post('index', TRUE)){
			alert('차량 id가 없습니다.');
			exit;
		}
		
		$result = $this->carmanager->delete_car($this->input->post('index', TRUE));
		echo json_encode($result);
	}



}
?>