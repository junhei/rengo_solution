<?php
class Usermanager extends CI_Controller {
	private $template_file = "admin/index";
	private $admin_function, $request_uri;

	function __construct()
	 { 
		parent::__construct();

	    $this->load->model('admin/Admin_model', 'admin');
	    $this->load->model('Usermanager_model','usermanager');
	    $this->load->model('Company_model','company');
  		$menu_permission = 2;
  		$permission = $this->admin->_check_permission($menu_permission);
  		if($permission != "Y")
   		$this->admin->admin_logout(); 

	} 

	function _view($url, $data = '')
	{
		$data['permission'] = $this->session->userdata('admin_permission');
  		$data['admin_id'] = $this->session->userdata('admin_id');
  		$data['company_serial'] = $this->session->userdata('company_serial');
		$data['company_name'] = $this->session->userdata('company_name');
		$this->load->view("admin/admin_layout_top", $data); 
		$this->load->view($url, $data); 
		$this->load->view("admin/admin_layout_bottom");
	}

	function index()
	{
		$company_serial = $this->session->userdata('company_serial');
		$data['company_serial'] = $company_serial;
		$data['company_list'] = $this->company->get_company($company_serial);
		// $data['branch_list'] = $this->company->get_branch($company_serial);
		$this->_view("user/user_manager_view", $data); 
		
	}

	function get_user_count($company_serial, $user_status) { //user_status : 정상, 불량..
		$user_count = $this->usermanager->get_user_count($company_serial, $user_status);
		
		echo json_encode($user_count);
	}

	function get_user_information($company_serial)
	{
		$user_info=$this->usermanager->get_user_information($company_serial);
	 	echo json_encode($user_info);
	 }

	 function get_order_information($user_serial)
	 {

	 	$order_data_array = $this->usermanager->get_order_information($user_serial);
	 	foreach($order_data_array as $order_data){

	 		$date_start = date_create($order_data['period_start']);
	 		$date_finish = date_create($order_data['period_finish']);

			$return_array[] = array(
				'period_start' => date_format($date_start, 'Y-m-d H:i'),
				'period_finish' => date_format($date_finish, 'Y-m-d H:i'),
				'car_type'=> $order_data['car_type'],
				'car_name_detail' => $order_data['car_name_detail'],
				'car_number' => $order_data['car_number'],
				'rental_price' => intval($order_data['rental_price'])
				);

		}

		if($return_array == null){
			$return_array = array();
		}

		echo json_encode($return_array);
	 }

	 function get_receipt_information($user_serial){


	 	$receipt_data_array = $this->usermanager->get_receipt_information($user_serial);

		foreach($receipt_data_array as $receipt_data){

			$return_array[] = array(
				'deposite_date' => $receipt_data['deposite_date'],
				'type' => $receipt_data['type'],
				'deposite_way'=> $receipt_data['deposite_way'],
				'deposite_amount' => intval($receipt_data['deposite_amount']),
				'memo' => $receipt_data['memo'],
				'handler' => $receipt_data['handler'],
				'deposite_registration_date' => $receipt_data['deposite_registration_date'],
				);

		}

		if($return_array == null){
			$return_array = array();
		}

		echo json_encode($return_array);
	 }

	function user_info_register()
	{
		$user_serial=$this->input->post('user_serial', TRUE);
		$user_status=$this->input->post('user_status',TRUE);
		$user_name=$this->input->post('user_name', TRUE);
		$user_email=$this->input->post('user_email',TRUE);
		$birthday=$this->input->post('birthday', TRUE);
		$gender=$this->input->post('gender',TRUE);
		$postcode=$this->input->post('postcode',TRUE);
		$address=$this->input->post('address', TRUE);
		$phone_number1=$this->input->post('phone_number1',TRUE);
		$phone_number2=$this->input->post('phone_number2',TRUE);
		$license_type=$this->input->post('license_type', TRUE);
		$license_number=$this->input->post('license_number', TRUE);
		$published_date=$this->input->post('published_date', TRUE);
		$expiration_date=$this->input->post('expiration_date', TRUE);
		$note=$this->input->post('note', TRUE);
		$company_serial=$this->session->userdata('company_serial');

		$user_information_data = array(
			'user_status'=>$user_status,
			'user_name'=>$user_name,
			'user_email'=>$user_email,
			'birthday'=>$birthday,
			'gender'=>$gender,
			'postcode'=>$postcode,
			'address'=>$address,
			'phone_number1'=>$phone_number1,
			'phone_number2'=>$phone_number2,
			'license_type'=>$license_type,
			'license_number'=>$license_number,
			'published_date'=>$published_date,
			'expiration_date'=>$expiration_date,
			'company_serial'=>$company_serial,
			'note'=>$note
		);

	
		$result1 = $this->usermanager->user_info_update($user_serial,$user_information_data);

		echo json_encode($result1);
	}

	function user_delete()
	{
		$user_serial=$this->input->post('user_serial',TRUE);
		$result=$this->usermanager->user_delete($user_serial);
		echo json_encode($result);
	}
		
	// function do_upload()
	// {
	// 	$config['upload_path'] = './uploads/';
	// 	$config['allowed_types'] = 'gif|jpg|png|jpeg';
	// 	$config['max_size']	= '100000000000';
	// 	$config['max_width']  = '10240';
	// 	$config['max_height']  = '7680';
		
	// 	$this->load->library('upload', $config);
	
	// 	if ( ! $this->upload->do_upload())
	// 	{
	// 		$error = array('error' => $this->upload->display_errors());
			
	// 		$this->load->view('upload_form', $error);
	// 	}	
	// 	else
	// 	{
	// 		$data = array('upload_data' => $this->upload->data());
			
	// 		$this->load->view('upload_success', $data);
	// 	}
	// }
	// function get_branch($company_serial)
	// {
	// 	echo json_encode($this->company->get_branch($company_serial));
	// }

	function get_license_image($user_serial)
	{
		// $result = $this->usermanager->get_license_image($user_serial);
		// echo $result['0']['license_image'];
		// echo '/home/apache/uploads/company/'.$company_serial."/";

		$refer = $_SERVER['HTTP_REFERER'];
//		if($refer != "http://solution.rengo.co.kr/partnersetting/") die();

		$file = "/home/apache/uploads/user/".$user_serial."/license.png";
		$type = "image/*";
		header("Content-type: $type");
		header('Content-Length: ' . filesize($file));
		readfile($file);
		exit();
	}

}
?>