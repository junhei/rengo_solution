<?php
class Api extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('Company_model','company');
		$this->load->model('Car_model','car');
		$this->load->model('Rent_model','rent');
		$this->load->model('Blacklist_model','blacklist');
		$this->load->model('Salemanager_model','sale');
		$this->load->model('Car_allocation_model','schedule');
		$this->load->model('Staffmanager_model','staff');
		$this->load->model('Usermanager_model','user');
	} 


	function index(){
		// $data['bad']=$this->blacklist->get_list();
		$this->load->view("index.html"); 
	}

	//로그인
	function check_login(){
		try{
			$email = urldecode($this->input->post('email',TRUE));
			$password = urldecode($this->input->post('password',TRUE));

			if($email==''){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			if($password==''){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			$result = $this->company->m_check_login($email, $password);
			echo json_encode($result);
		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}
	}

	//업체가 가지고 있는 차량 전송(수리, 매각 포함)
	function car_list(){

		try{
			$company_serial = urldecode($this->input->post('company_serial',TRUE));

			if($company_serial==''){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			$result = $this->car->m_get_list($company_serial);
			echo json_encode($result);
		}catch(Exception $e) {
    		$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
    	}

	}

	//예약현황 가져오기
	function get_reservation(){

		try{
			$company_serial = urldecode($this->input->post('company_serial',TRUE));
			$today = urldecode($this->input->post('today',TRUE));

			if($company_serial=='' || $today == ''){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			$todayDate = date('Ymd', mktime(0,0,0, substr($today,4,2), substr($today,6,2) ,  substr($today, 0,4)));
	    	$endDate = date('Ymd', strtotime($todayDate. ' + 6 days'));

			$result = $this->rent->m_get_all_order($company_serial, $todayDate."0000", $endDate."2359");
			echo json_encode($result);
		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}

	}

	//하루치 차량 스케쥴 가져오기
	function get_car_schedule_daily(){

		try{
			$company_serial = urldecode($this->input->post('company_serial',TRUE));
			$today = urldecode($this->input->post('today',TRUE));

			if($company_serial=='' || $today == ''){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			$todayDate = date('Ymd', mktime(0,0,0, substr($today,4,2), substr($today,6,2) ,  substr($today, 0,4)));
			$result = $this->schedule->get_car_schedule_detail($todayDate."0000", $todayDate."2359", $company_serial);
			$response['code'] = 'S01';
			$response['value'] = $result;
			echo json_encode($response);

		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}

	}

	//예약현황 가져오기
	function get_reservation_detail(){

		try{
			$order_serial = urldecode($this->input->post('order_serial',TRUE));

			if($order_serial==''){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			$result = $this->rent->m_get_order_detail($order_serial);
			echo json_encode($result);
		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}

	}






	function add_reservation(){
		try{


			$order_data = array(
				'company_serial' => urldecode($this->input->post('company_serial',TRUE)),
				'car_serial' => urldecode($this->input->post('car_serial',TRUE)),
				//고객정보
				'user_mail' => urldecode($this->input->post('user_mail',TRUE)),
				'username' => urldecode($this->input->post('username',TRUE)),
				'birthday' => urldecode($this->input->post('birthday',TRUE)),
				'tel' => urldecode($this->input->post('tel',TRUE)),
				//대여정보
				'period_start' => urldecode($this->input->post('period_start',TRUE)),
				'period_finish' => urldecode($this->input->post('period_finish',TRUE)),
				'delivery_place' =>  urldecode($this->input->post('delivery_place',TRUE)),
				'pickup_place' =>  urldecode($this->input->post('pickup_place',TRUE)),
				//요금정보
				'rental_price' =>  urldecode($this->input->post('rental_price',TRUE)),
				// 'option_price' =>  urldecode($this->input->post('option_price',TRUE)),
				'insurance_price' =>  urldecode($this->input->post('insurance_price',TRUE)),
				'etc_price' => urldecode($this->input->post('etc_price',TRUE)),
				'total_price' =>  urldecode($this->input->post('total_price',TRUE)),
				'price_off' =>  urldecode($this->input->post('price_off',TRUE)),
				//대여정보
				'rental_staff_serial' => urldecode($this->input->post('rental_staff_serial',TRUE)),
				'return_staff_serial' => urldecode($this->input->post('return_staff_serial',TRUE)),
				'memo_text' => urldecode($this->input->post('memo_text',TRUE))
			);

			// //2개 항목은 매출에 저장
			$pay_data = array();
			
			// $pay_data = array(
			// 	'type' => '대여요금',
			// 	'handler' => 'app',
			// 	'flag' => 'Y',
			// 	'normal_sale' => 'Y',
			// 	'car_serial' => urldecode($this->input->post('car_serial',TRUE)),
			// 	'deposite_amount' =>  urldecode($this->input->post('deposite_amount',TRUE)),
			// 	'deposite_way' =>  urldecode($this->input->post('deposite_way',TRUE))
			// );


			if($order_data['company_serial']=='' || $order_data['car_serial'] == '' || $order_data['username'] == '' || $order_data['birthday'] == '' || $order_data['tel'] == '' || $order_data['period_start'] == '' ||  $order_data['period_finish'] == ''){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}


			$result = $this->rent->m_add_order($order_data, $pay_data);
			echo json_encode($result);

		}catch(Exception $e) {
				$response['code'] = 'E01';
				$response['message'] = $e->getMessage();
				echo json_encode($response);
		}

	}

	// function update_reservation(){
	// 	try{

	// 		$order_serial = urldecode($this->input->post('order_serial',TRUE));
	// 		$order_data = array(
	// 			'company_serial' => urldecode($this->input->post('company_serial',TRUE)),
	// 			'car_serial' => urldecode($this->input->post('car_serial',TRUE)),
	// 			'user_mail' => urldecode($this->input->post('user_mail',TRUE)),
	// 			'username' => urldecode($this->input->post('username',TRUE)),
	// 			'birthday' => urldecode($this->input->post('birthday',TRUE)),
	// 			'tel' => urldecode($this->input->post('tel',TRUE)),
	// 			'period_start' => urldecode($this->input->post('period_start',TRUE)),
	// 			'period_finish' => urldecode($this->input->post('period_finish',TRUE)),
	// 			'delivery_place' =>  urldecode($this->input->post('delivery_place',TRUE)),
	// 			'pickup_place' =>  urldecode($this->input->post('pickup_place',TRUE)),
	// 			'rental_price' =>  urldecode($this->input->post('rental_price',TRUE)),
	// 			'option_price' =>  urldecode($this->input->post('option_price',TRUE)),
	// 			'insurance_price' =>  urldecode($this->input->post('insurance_price',TRUE)),
	// 			'total_price' =>  urldecode($this->input->post('total_price',TRUE)),
	// 			'price_off' =>  urldecode($this->input->post('price_off',TRUE))
	// 		);

	// 		// //2개 항목은 매출에 저장
	// 		$pay_data = array(
	// 			'deposite_amount' =>  urldecode($this->input->post('deposite_amount',TRUE)),
	// 			'deposite_way' =>  urldecode($this->input->post('deposite_way',TRUE))
	// 		);


	// 		if($order_data['company_serial']=='' || $order_data['car_serial'] == '' || $order_data['username'] == '' || $order_data['birthday'] == '' || $order_data['tel'] == '' || $order_data['period_start'] == '' ||  $order_data['period_finish'] == ''){
	// 			$response['code'] = 'E02';
	// 			$response['message'] = 'input data error';
	// 			echo json_encode($response);
	// 			die();
	// 		}


	// 		$result = $this->rent->m_add_order($order_data, $pay_data);
	// 		echo json_encode($result);

	// 	}catch(Exception $e) {
	// 			$response['code'] = 'E01';
	// 			$response['message'] = $e->getMessage();
	// 			echo json_encode($response);
	// 	}

	// }

	

	function get_car_schedule(){
		try{
			$company_serial = urldecode($this->input->post('company_serial',TRUE));
			$start = urldecode($this->input->post('start',TRUE));
			$end = urldecode($this->input->post('end',TRUE));

			if($company_serial=='' || $start == '' || $end == ''){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			$result = $this->schedule->get_car_schedule($start, $end, $company_serial);
			$response['code'] = 'S01';
			$response['value'] = $result;
			echo json_encode($response);

		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}

	}
	//page 기능 추가해야함
	function get_blacklist(){
		try{
			$result = $this->blacklist->m_get_list();
			$response['code'] = 'S01';
			$response['value'] = $result;
			echo json_encode($response);
		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}

	}

	function get_income(){
		try{
			$company_serial = urldecode($this->input->post('company_serial',TRUE));
			$date = urldecode($this->input->post('date',TRUE));

			if($company_serial=='' || $date == ''){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			$start_date = date('Y-m-d', mktime(0,0,0, substr($date,4,2), 1,  substr($date, 0,4)));
			$end_date = date("Y-m-t",  mktime(0,0,0, substr($date,4,2), 1,  substr($date, 0,4)));

			$result = $this->sale->detail_reservaion($company_serial, $start_date, $end_date);
			$response['code'] = 'S01';
			$response['value'] = $result;
			echo json_encode($response);

		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}
	}

	function get_rengo_money(){
		try{
			$company_serial = urldecode($this->input->post('company_serial',TRUE));
			$date = urldecode($this->input->post('date',TRUE));

			if($company_serial=='' || $date == ''){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			$start_date = date('Ymd', mktime(0,0,0, substr($date,4,2), 1,  substr($date, 0,4)));
			$end_date = date("Ymt",  mktime(0,0,0, substr($date,4,2), 1,  substr($date, 0,4)));

			

			$result = $this->sale->m_get_rengo_money($company_serial, $start_date."0000", $end_date."2359");
			$response['code'] = 'S01';
			$response['value'] = $result;
			echo json_encode($response);

		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}
		
	}


	function get_staff(){
		try{
			$company_serial = urldecode($this->input->post('company_serial',TRUE));

			if($company_serial=='' ){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			$result = $this->staff->m_get_list($company_serial);
			$response['code'] = 'S01';
			$response['value'] = $result;
			echo json_encode($response);

		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}


	}

	function search_user(){
		try{
			$user_name = urldecode($this->input->post('user_name',TRUE));
			$company_serial = urldecode($this->input->post('company_serial',TRUE));

			if($company_serial=='' ){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			if($user_name == null || $user_name == ''){
				$user_name = '';
			}

			$result = $this->user->m_get_user($company_serial, $user_name);
			$response['code'] = 'S01';
			$response['value'] = $result;
			echo json_encode($response);

		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}
	}

	


function get_available_car(){
		try{
			$company_serial = urldecode($this->input->post('company_serial',TRUE));
			$start = urldecode($this->input->post('start_time',TRUE));
			$end = urldecode($this->input->post('end_time',TRUE));

			if($company_serial=='' || $start== '' || $end == '' ){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			$period = $start."_".$end;

			$result = $this->car->get_list_period($company_serial, "전체", $period);
			if($result==null || count($result)==0){
				$result = array();
			}
			$response['code'] = 'S01';
			$response['value'] = $result;
			echo json_encode($response);

		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}
	}


 
function get_rental_period(){

	try{
			$company_serial = $this->input->post('company_serial');
			$car_serial = $this->input->post('car_serial');
			$period_start = $this->input->post('start_time');
			$period_finish = $this->input->post('end_time');

			for($i=0;$period_finish>=date("YmdHi", strtotime($period_start." +".($i+1)." days"));$i++){
				$day = date("w", strtotime($period_start." +".$i." days"));

				if($day < '1' || $day > '5'){
					//평일
					$weekend_count ++;
				} else {
					//주말
					$week_count ++;
				}

			}


			$last_date = date("YmdHi", strtotime($period_start." +".$i." days"));

			$day_count = $week_count + $weekend_count;
			$hour_count = ceil(gmdate('Hi', strtotime($period_finish) - strtotime($last_date)) / 100);



			$result['contract_period'] = number_format($week_count + $weekend_count)."일 ".number_format($hour_count)."시간";
			$car_information = $this->car->get_car_information($company_serial, $car_serial);
			$day_price = $car_information['0']['week_price'];
			$original_day_price = $day_price;


			$week_price_24 = $car_information['0']['week_price'];
			$week_price_6 = $car_information['0']['week_price_6'];
			$week_price_1 = $car_information['0']['week_price_1'];
			$weekend_price_24 = $car_information['0']['weekend_price'];
			$weekend_price_6 = $car_information['0']['weekend_price_6'];
			$weekend_price_1 = $car_information['0']['weekend_price_1'];

			if($day_count < '1' && $hour_count >= '1'){
				//하루미만 렌탈

				$day = date("w", strtotime($period_start));
				if($day < '1' || $day > '5'){
					//주말요금 적용
					$price_24 = $weekend_price_24;
					$price_6 = $weekend_price_6;
					$price_1 = $weekend_price_1;
				} else {
					//평일요금 적용
					$price_24 = $week_price_24;
					$price_6 = $week_price_6;
					$price_1 = $week_price_1;
				}

				if($hour_count <= '6'){
					$result['original_rental_price'] = $price_6;
					$result['rental_price'] = $result['original_rental_price'] ;
				} else {
					$result['original_rental_price'] = $price_6 + ( $price_1 * ($hour_count - 6) );
					$result['rental_price'] = $result['original_rental_price'];
					if($result['original_rental_price'] >= $price_24 ){
						$result['original_rental_price'] = $price_24;
					}
					if($result['rental_price'] >= $price_24 ){
						$result['rental_price'] = $price_24;
					}

				}
			} else {
				//하루이상 렌탈

				$result['original_rental_price'] = $week_count * $week_price_24;
				$result['original_rental_price'] += $weekend_count * $weekend_price_24;
				$result['rental_price'] = $week_count * $week_price_24 ;
				$result['rental_price'] += $weekend_count * $weekend_price_24 ;


				if($hour_count >= '1'){


					$day = date("w", strtotime($period_finish));
					if($day < '1' || $day > '5'){
						//주말요금 적용
						$price_24 = $weekend_price_24;
						$price_6 = $weekend_price_6;
						$price_1 = $weekend_price_1;
					} else {
						//평일요금 적용
						$price_24 = $week_price_24;
						$price_6 = $week_price_6;
						$price_1 = $week_price_1;
					}

					$original_hour_price =  $price_1 * $hour_count;
					if($original_hour_price >= $price_24 ){
						//일별요금계산후 남은 시간에 대한 계산
						$original_hour_price = $price_24;
					}

					$hour_price =  $original_hour_price;
					if($hour_price >= $price_24){
						//일별요금계산후 남은 시간에 대한 계산
						$hour_price = $price_24;
					}

					
					$result['rental_price'] += $hour_price;
					$result['original_rental_price'] += $original_hour_price;
				}
			}

			$response['code'] = 'S01';
			$response['value'] = $result;
			echo json_encode($response);
		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}
	}

	function change_car_on_off(){
		try{
			$car_serial = $this->input->post('car_serial');
			$flag = $this->input->post('flag');

			if($car_serial=='' || $flag== ''){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			if($flag == 'Y' || $flag == 'N'){
				$result = $this->car->m_change_flag($car_serial, $flag);
				echo json_encode($result);
			}else{
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}
			
			
		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}
	}

	function change_day_on_off(){
		try{
			$company_serial = $this->input->post('company_serial');
			$flag = $this->input->post('flag');
			$holiday = $this->input->post('holiday');


			if($company_serial=='' || $flag== '' || $holiday == ''){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}

			if($flag == 'Y' || $flag == 'N'){
				$result = $this->rent->m_change_day_flag($company_serial, $holiday, $flag);
				echo json_encode($result);
			}else{
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}
			
			
		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}
	}

	function send_auth_email(){
		try{
			$id = $this->input->post('email', TRUE);

			if($id==''){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
			}
			
			$sql = "
			SELECT
				A.serial, A.phone_number, A.admin_name, A.admin_id, A.registered_date, C.company_name
			FROM
				admin_information AS A, company_information AS C
			WHERE
				A.admin_id='".$id."'
			AND
		    	A.company_serial = C.serial";

			$result = $this->db->fReadSql($sql);
			if($result[0]['serial'] >= '1') {
				//랜덤 인증 코드 생성 알고리즘
				$auth_code_temp = explode(" ",microtime());
				$auth_code = intval(substr($auth_code_temp[0], strpos($auth_code_temp[0], ".")+1)+$auth_code_temp[1]);
				$length_auth_code = strlen($auth_code);
				$rand_string = '0123456789'; 
				$strlen_string = strlen($rand_string); 

				for($i=0;$i<$length_auth_code;$i++){
					$new_auth_code .= $rand_string[rand(0,$strlen_string)].substr($auth_code, $i, rand(0,3));
				}
				$new_auth_code = substr($new_auth_code, 0, 4);


					// 메일발송 시작
				$nameFrom  = "렌고";
				$mailFrom = "rengo@co.kr";
				$nameTo  = $result[0]['company_name'];
				$mailTo = $id;
				    // $cc = "참조";
				    // $bcc = "숨은참조";
				$subject ="비밀번호 변경을 위한 인증번호를 전송하였습니다.";
					//암호화 로직 추가해야 함
				$message = '

	<!DOCTYPE html>
	<html>
	<head>
	    <title>RENGO SOLUTION</title>
	</head>
	<body>

	  <p class="height:30px;"></p>

	    <table style="max-width: 600px; width: 100%; margin: 0 auto; padding: 0 15px; font-family: "나눔고딕", "나눔바른고딕", "돋움", "고딕"; font-size:14px; line-height:1.5em; color:#252525;" cellpadding="0" border="0" cellspacing="0">
	      <tr>
	        <td style="padding:0 30px;">안녕하세요. 렌고입니다. </td>
	      </tr>
	      <tr>
	        <td style="padding:0 30px;">요청하신 비밀번호 변경을 위한 인증번호 발송메일 입니다. </td>
	      </tr>
	      <tr>
	        <td style="height:18px;"></td>
	      </tr>
	      <tr>
	        <td style="padding:0 30px;">[비밀번호 변경 내역]</td>
	      </tr>
	      <tr>
	        <td style="padding:0 30px;">아이디(이메일) : <span style="color:#252525">'.$id.'</span></td>
	      </tr>
	        <td style="padding:0 30px;">파트너명 : '.$result[0]['company_name'].'</td>
	      <tr>
	        <td style="padding:0 30px;">발급일시 : '.$result[0]['registered_date'].'</td>
	      </tr>
	      <tr>
	        <td style="height:18px;"></td>
	      </tr>
	      <tr>
	        <td style="padding:0 30px;">발급받으신 인증번호를 입력후 비밀번호를 변경해 주세요.</td>
	      </tr>
	      <tr>
	        <td style="padding:0 30px;">인증번호 :</td>
	      </tr>
	      <tr>
	        <td style="height:15px;"></td>
	      </tr>
	      <tr>
	        <td style="padding:0 30px;"><span style="padding:20px; font-size:18px; background:#ffe80e; display:inline-block; text-align:center;">'.$new_auth_code.'</span></td>
	      </tr>
	      <tr>
	        <td style="height:30px;"></td>
	      </tr>
	      <tr>
	        <td style="padding:0 30px;">감사합니다.</td>
	      </tr>
	      <tr>
	        <td>
	          <table style="background:#ffe80e; width:100%; padding:30px; margin-top:30px; margin-bottom:30px;">
	            <tr>
	              <td></td>
	            </tr>
	            <tr>
	              <td>
	                (주)렌고<br />
	                대표 : 이승원 I 사업자번호 : 302-81-29052<br />
	                통신판매업신고 : 제2016-부산금정-0119호<br />
	                개인정보담당자 : swlee@rengo.co.kr<br />
	                고객센터 : 1800-1090<br />
	                ⓒ 2017 rengo All rights reserved.
	              </td>
	              <td style="text-align:right; vertical-align:bottom;"><img src="http://solution.rengo.co.kr/img/L1_black.png" class="" width="40" height="40"></td>
	            </tr>
	          </table>
	        </td>
	      </tr>
	    </table>
	</body>
	</html>';
		    
				    $charset = "UTF-8";


				    $nameFrom   = "=?$charset?B?".base64_encode($nameFrom)."?=";
				    $nameTo   = "=?$charset?B?".base64_encode($nameTo)."?=";
				    $subject = "=?$charset?B?".base64_encode($subject)."?=";

				    $header  = "Content-Type: text/html; charset=utf-8\r\n";
				    $header .= "MIME-Version: 1.0\r\n";

				    $header .= "Return-Path: <". $mailFrom .">\r\n";
				    $header .= "From: ". $nameFrom ." <". $mailFrom .">\r\n";
				    $header .= "Reply-To: <". $mailFrom .">\r\n";
				    if ($cc)  $header .= "Cc: ". $cc ."\r\n";
				    if ($bcc) $header .= "Bcc: ". $bcc ."\r\n";

				    $res = mail($mailTo, $subject, $message, $header, $mailFrom);
				    if(!$res){
				    	$response['code'] = 'E01';
						$response['message'] = 'mail 전송이 실패하였습니다.';
						echo json_encode($response);	
				    }else{
				    	$sql = "
							UPDATE
								admin_information
							SET
								auth_code = '".$new_auth_code."'
							WHERE
								serial = '".$result[0]['serial']."'";

						$re = $this->db->query($sql);
						if($re){
								$response['code'] = 'S01';
								$response['value'] = array();
								echo json_encode($response);	
						}else{
							$response['code'] = 'E01';
							$response['message'] = $e->getMessage();
							echo json_encode($response);
						}
						
				    }


			}else{
					$response['code'] = 'E02';
					$response['message'] = "등록된 유저가 없습니다.";
					echo json_encode($response);
			}
			
			
		}catch(Exception $e) {
			$response['code'] = 'E01';
			$response['message'] = $e->getMessage();
			echo json_encode($response);
		}
	}

	function change_password(){
		$id = $this->input->post('email', TRUE);
		$pw = str_replace("-", "", trim($this->input->post('pw', TRUE)));
		$auth = str_replace("-", "", trim($this->input->post('auth', TRUE)));


		if($id=='' || $pw == '' || $auth == ''){
				$response['code'] = 'E02';
				$response['message'] = 'input data error';
				echo json_encode($response);
				die();
		}

		echo json_encode($this->user->m_change_pw($id, $pw, $auth));

	}


}
?>